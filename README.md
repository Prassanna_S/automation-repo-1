## This is the Repository that is used for Automation perpose

This repository may be extended based on the requirments and memory of the repo

1. This contains the code for Aria 6 and Crescendo Automation.
2. Some files for pre and post automation executions that assist the testers.
3. The projects can be commited only after proper permissions.

This Repo is a Private Property of [Aria Systems](https://www.ariasystems.com/) If you have landed here without proper permission please leave the page immediately.