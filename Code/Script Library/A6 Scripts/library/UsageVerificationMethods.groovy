package library

import java.awt.geom.Arc2D.Double;
import java.lang.reflect.Method
import java.sql.ResultSet;
import java.sql.ResultSetMetaData
import java.sql.SQLException
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import groovy.xml.StreamingMarkupBuilder
import org.apache.ivy.core.module.descriptor.ExtendsDescriptor;
import library.Constants.Constant;
import library.Constants.ExpValueConstants;
import library.Constants.ExPathRpcEnc;
import library.Constants.ExPathRpc;
import library.Constants.Constant.RESULT_TYPE;
import library.VerificationMethods;
import library.ReadData

public class UsageVerificationMethods extends  VerificationMethods {
	LogResult logi = new LogResult(Constant.logFilePath)
	static ConnectDB db = null;
	public DecimalFormat df=new DecimalFormat()
	public static HashMap<String,String> acct_months_hm = new HashMap<String,String>()
	public static final HashMap<String,String> accumutemp = new HashMap<String,String>()
	public UsageVerificationMethods(ConnectDB db) {

		super(db)
	}
	/**
	 * Getting the recorded usage details from record_usage api
	 * @param tcid
	 * @return LinkedHashMap
	 */
	LinkedHashMap md_RECORD_USAGE_DETAILS_EXP(String tcid) {
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=tcid.split("-")[2]
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no
		String senior_acct_no
		String res_level_cd
		ConnectDB db = new ConnectDB()
		acct_no= getValueFromRequest(apiname,"//acct_no")
		def usage_rec_no=getValueFromResponse(apiname,ExPathRpcEnc.RECORD_USAGE_USAGERECNO1)
		if(acct_no.equals("NoVal"))
			acct_no=db.executeQueryP2("Select acct_no from ariacore.usage where rec_no="+usage_rec_no+" and client_no=" +client_no)
		String actual_amt
		HashMap input_hash = new HashMap()
		LinkedHashMap expected_hash = new LinkedHashMap()
		String xpath="//*:record_usage//*"
		List testCaseCmb = Constant.lhmAllInputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllInputResponseAsXML_Global.values().asList()

		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().equals(tcid)) {
				
				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
					def responseXml = new XmlSlurper().parseText(holder.getXml())
					def rootNode = responseXml.Body.record_usage
					rootNode.children().each() {
						input_hash.put(it.name(),rootNode[it.name()])
					}
				}
			}
		}
		logi.logInfo( "Input hash from record_usage api"+input_hash)

		String acct_no_qry = "Select senior_acct_no,resp_level_cd from ariacore.acct where acct_no="+acct_no
		ResultSet rs_acct_no_qry = db.executePlaQuery(acct_no_qry);
		while(rs_acct_no_qry.next()) {
			senior_acct_no = rs_acct_no_qry.getString(1)
			res_level_cd = rs_acct_no_qry.getString(2)
		}

		if ((senior_acct_no != "null" && res_level_cd == "3") || (senior_acct_no != "null" && res_level_cd == "4")) {
			logi.logInfo("this is child account")
			acct_no = senior_acct_no
		}
		expected_hash.put("acct_no",acct_no)
		expected_hash.put("usage_type",input_hash.get("usage_type"))
		expected_hash.put("usage_units",input_hash.get("usage_units"))

		if(input_hash.get("billable_units").toString().isEmpty()==true) {
			expected_hash.put("billable_units",d.format(input_hash.get("usage_units").toDouble()).toString())
		}
		else {
			expected_hash.put("billable_units",d.format(input_hash.get("billable_units").toDouble()).toString())
		}
		//Setting Exclude from billing
		if((input_hash.get("exclude_from_billing").toString().isEmpty()==true) ||(input_hash.get("exclude_from_billing").toString().toUpperCase()=="FALSE" ))
			expected_hash.put("exclude_ind","0")
		else if(input_hash.get("exclude_from_billing").toString().toUpperCase()=="TRUE" )
			expected_hash.put("exclude_ind","1")
		logi.logInfo("expected_hash now1# "+expected_hash.toString())
		String current_time=db.executeQueryP2("SELECT  TO_CHAR (ARIACORE.ARIAVIRTUALTIME("+client_no+"), 'YYYY-MM-DD')  FROM DUAL")
		if(input_hash.get("usage_date").toString().isEmpty()==true )
		{
			expected_hash.put("usage_date",current_time)
		}
		else
		{
			expected_hash.put("usage_date",input_hash.get("usage_date"))
		}

		String formated_date = expected_hash.get("usage_date").toString()
		if(formated_date.contains("/"))
		{
			formated_date = formated_date.replaceAll("/", "-")
			logi.logInfo("formated_date"+formated_date)
			expected_hash.put("usage_date", formated_date)
		}
		if(input_hash.get("exclusion_comments").toString().isEmpty()==true || expected_hash.get("exclude_ind")=="0")
		{
			expected_hash.put("exclusion_comments","null")
		}
		else
		{
			expected_hash.put("exclusion_comments",input_hash.get("exclusion_comments"))
		}

		if(input_hash.get("telco_from").toString().isEmpty()==true )
		{
			expected_hash.put("telco_from","null")
		}
		else
		{
			expected_hash.put("telco_from",input_hash.get("telco_from"))
		}
		if(input_hash.get("telco_to").toString().isEmpty()==true )
		{
			expected_hash.put("telco_to","null")
		}
		else
		{
			expected_hash.put("telco_to",input_hash.get("telco_to"))
		}

		if(input_hash.get("qualifier_1").toString().isEmpty()==true )
		{
			expected_hash.put("qualifier_1","null")
		}
		else
		{
			expected_hash.put("qualifier_1",input_hash.get("qualifier_1"))
		}
		if(input_hash.get("qualifier_2").toString().isEmpty()==true )
		{
			expected_hash.put("qualifier_2","null")
		}
		else
		{
			expected_hash.put("qualifier_2",input_hash.get("qualifier_2"))
		}
		if(input_hash.get("qualifier_3").toString().isEmpty()==true )
		{
			expected_hash.put("qualifier_3","null")
		}
		else
		{
			expected_hash.put("qualifier_3",input_hash.get("qualifier_3"))
		}
		if(input_hash.get("qualifier_4").toString().isEmpty()==true )
		{
			expected_hash.put("qualifier_4","null")
		}
		else
		{
			expected_hash.put("qualifier_4",input_hash.get("qualifier_4"))
		}
		if(input_hash.get("parent_usage_rec_no").toString().isEmpty()==true )
		{
			expected_hash.put("parent_rec_no","null")
		}
		else
		{
			expected_hash.put("parent_rec_no",input_hash.get("parent_usage_rec_no"))
		}

		String rate_scle_qry = "Select param_val from ariacore.all_client_params where param_name ='USAGE_RATE_PER_UNIT_SCALE' and  client_no="+client_no
		ResultSet rs_rate_scle_qry = db.executePlaQuery(rate_scle_qry)
		int rate_rnd
		int amt_rnd
		while(rs_rate_scle_qry.next())
		{
			rate_rnd=rs_rate_scle_qry.getInt(1)
			logi.logInfo("rate should be rounded off to:"+rate_rnd)
		}

		String amt_write_qry = "Select param_val from ariacore.all_client_params where param_name ='USAGE_RECORD_WRITE_SCALE' and  client_no="+client_no
		ResultSet rs_amt_write_qry = db.executePlaQuery(amt_write_qry)
		while(rs_amt_write_qry.next())
		{
			amt_rnd=rs_amt_write_qry.getInt(1)
			logi.logInfo("amt should be rounded off to:"+amt_rnd)
		}


		HashMap<String, ArrayList<String>> usage_rateamt_hm = new HashMap<String, ArrayList<String>>()

		boolean auto_rate_flag = get_Auto_Rate_Unrated_Usage_Flag()
		logi.logInfo("Auto rate flag is :: "+auto_rate_flag)

		logi.logInfo("expected_hash now1 "+expected_hash.toString())
		String rate_per_unit


		if(!auto_rate_flag){

			logi.logInfo("Inside auto rate flag is equal to false loop")

			List plan_no=getAllUsedPlanNoP2(acct_no,expected_hash.get("usage_type").toString())
			logi.logInfo "plan_no's in md_RECORD_USAGE_DETAILS_EXP "+plan_no.toString()
			if(!plan_no.isEmpty())
			{
				for(int pln=0; pln<plan_no.size(); pln++){
					logi.logInfo("Executing the loop for "+plan_no.get(pln))

					if (input_hash.get("rate").toString().isEmpty()!=true && input_hash.get("amt").toString().isEmpty()!=true )
					{
						logi.logInfo "rate and amount are given"
						expected_hash.put("amt",d.format(input_hash.get("amt").toString().toDouble().round(amt_rnd)).toString())
						actual_amt=d.format(input_hash.get("amt").toString().toDouble()).toString()
						expected_hash.put("rate",d.format(input_hash.get("rate").toString().toDouble().round(rate_rnd)).toString())
						actual_amt = "calc:"+actual_amt
					}

					else if(input_hash.get("rate").toString().isEmpty()!=true || input_hash.get("amt").toString().isEmpty()!=true ) //rate or amount given
					{
						logi.logInfo "rate or amount is given"
						if(input_hash.get("amt").toString().isEmpty()!=true)
						{
							logi.logInfo("amt is given")
							expected_hash.put("amt",d.format(input_hash.get("amt").toString().toDouble().round(amt_rnd)).toString())
							actual_amt=expected_hash.get("amt").toString()
							rate_per_unit=( expected_hash.get("amt").toString().toDouble()/expected_hash.get("billable_units").toString().toDouble()).toString()
							expected_hash.put("rate",d.format(rate_per_unit.toDouble().round(rate_rnd)).toString())
							actual_amt = "calc:"+actual_amt

						}
						if(input_hash.get("rate").toString().isEmpty()!=true)
						{
							logi.logInfo("rate is given")
							expected_hash.put("rate",d.format(input_hash.get("rate").toString().toDouble().round(rate_rnd)).toString())
							String calc_amt=(expected_hash.get("rate").toString().toDouble()*expected_hash.get("billable_units").toString().toDouble()).round(2).toString()
							actual_amt=d.format(calc_amt.toDouble()).toString()
							expected_hash.put("amt",d.format(calc_amt.toDouble().round(amt_rnd)).toString())
							actual_amt = "calc:"+actual_amt
						}


					}
					else //neither amt or rate given
					{

						List service_nos = md_GET_USAGE_SERVICE_NO(acct_no.toString(), input_hash.get("usage_type").toString(), plan_no.get(pln).toString())
						logi.logInfo("service_no in md_RECORD_USAGE_DETAILS_EXP "+service_nos)
						usage_rateamt_hm=md_USAGE_AMT_RATE_EXP(acct_no.toString(),service_nos,expected_hash.get("billable_units").toString(),expected_hash.get("usage_type").toString(),plan_no.get(pln).toString())
						logi.logInfo("usage_rateamt_hm :: " +usage_rateamt_hm.toString())
						if (service_nos.size() >0)
						{
							for(service in service_nos)
							{
								String r=usage_rateamt_hm.get(service)[0].toString()
								String amt=usage_rateamt_hm.get(service)[1].toString()
								logi.logInfo("Calculated rate_per_unit"+r)
								logi.logInfo("Calculated amt"+amt)
								actual_amt=amt

								if(md_get_tier_pricing_type( service,acct_no,expected_hash.get("usage_type").toString() ).toString().equals("1"))
								{

									if(!auto_rate_flag)
									{
										expected_hash.put("amt","null")
										expected_hash.put("rate","null")
									}

									else
									{
										expected_hash.put("rate",d.format(r.toDouble().round(rate_rnd)).toString())
										expected_hash.put("amt",d.format(amt.toDouble().round(amt_rnd)).toString())
									}

								}
								else
								{
									expected_hash.put("amt","null")
									expected_hash.put("rate","null")
									break;
								}


							}
						}
						else
						{
							expected_hash.put("amt","null")
							expected_hash.put("rate","null")

						}
						logi.logInfo "actual_amt# "+actual_amt
					}

					if(expected_hash.get("exclude_ind")=="1")
						actual_amt="0"

					logi.logInfo "actual_amt "+actual_amt
					String rec_no=getValueFromResponse(apiname,"//usage_rec_no")

					Constant.recordedusageamounts.put(tcid+"-"+acct_no+"-"+rec_no+"-"+expected_hash.get("usage_type")+"-"+plan_no.get(pln),actual_amt.toString())
					logi.logInfo("All recorded usages hash" +Constant.recordedusageamounts.toString() )
				}
			}
			else
			{
				expected_hash.put("amt","null")
				expected_hash.put("rate","null")
			}


		} else {

			logi.logInfo("Inside else loop of auto rate unrated flag true")

			if (input_hash.get("rate").toString().isEmpty()!=true && input_hash.get("amt").toString().isEmpty()!=true )
			{
				logi.logInfo "rate and amount are given"
				expected_hash.put("amt",d.format(input_hash.get("amt").toString().toDouble().round(amt_rnd)).toString())
				actual_amt=d.format(input_hash.get("amt").toString().toDouble()).toString()
				expected_hash.put("rate",d.format(input_hash.get("rate").toString().toDouble().round(rate_rnd)).toString())
				actual_amt = "calc:"+actual_amt
			}

			else if(input_hash.get("rate").toString().isEmpty()!=true || input_hash.get("amt").toString().isEmpty()!=true ) //rate or amount given
			{
				logi.logInfo "rate or amount is given"
				if(input_hash.get("amt").toString().isEmpty()!=true)
				{
					logi.logInfo("amt is given")
					expected_hash.put("amt",d.format(input_hash.get("amt").toString().toDouble().round(amt_rnd)).toString())
					actual_amt=expected_hash.get("amt").toString()
					rate_per_unit=( expected_hash.get("amt").toString().toDouble()/expected_hash.get("billable_units").toString().toDouble()).toString()
					expected_hash.put("rate",d.format(rate_per_unit.toDouble().round(rate_rnd)).toString())
					actual_amt = "calc:"+actual_amt

				}
				if(input_hash.get("rate").toString().isEmpty()!=true)
				{
					expected_hash.put("rate",d.format(input_hash.get("rate").toString().toDouble().round(rate_rnd)).toString())
					String calc_amt=(expected_hash.get("rate").toString().toDouble()*expected_hash.get("billable_units").toString().toDouble()).round(2).toString()
					actual_amt=d.format(calc_amt.toDouble()).toString()
					expected_hash.put("amt",d.format(calc_amt.toDouble().round(amt_rnd)).toString())
					actual_amt = "calc:"+actual_amt
				}

			}
			else //neither amt or rate given
			{
				List service_nos=md_GET_USAGE_SERVICE_NO(acct_no.toString(), input_hash.get("usage_type").toString())
				logi.logInfo("service_no in md_RECORD_USAGE_DETAILS_EXP "+service_nos)
				usage_rateamt_hm=md_USAGE_AMT_RATE_EXP(acct_no.toString(),service_nos,expected_hash.get("billable_units").toString(),expected_hash.get("usage_type").toString())
				logi.logInfo("usage_rateamt_hm :: " +usage_rateamt_hm.toString())
				if (service_nos.size() >0)
				{
					for(service in service_nos)
					{
						String r=usage_rateamt_hm.get(service)[0].toString()
						String amt=usage_rateamt_hm.get(service)[1].toString()
						logi.logInfo("Calculated rate_per_unit"+r)
						logi.logInfo("Calculated amt"+amt)
						actual_amt=amt

						if(md_get_tier_pricing_type( service,acct_no,expected_hash.get("usage_type").toString() ).toString().equals("1"))
						{
							if(!auto_rate_flag)
							{
								expected_hash.put("amt","null")
								expected_hash.put("rate","null")
							}

							else
							{
								expected_hash.put("rate",d.format(r.toDouble().round(rate_rnd)).toString())
								expected_hash.put("amt",d.format(amt.toDouble().round(amt_rnd)).toString())
							}
						}
						else
						{
							expected_hash.put("amt","null")
							expected_hash.put("rate","null")
							break;
						}


					}
				}
				else
				{
					expected_hash.put("amt","null")
					expected_hash.put("rate","null")

				}
				logi.logInfo "actual_amt# "+actual_amt

			}
			if(expected_hash.get("exclude_ind")=="1")
				actual_amt="0"

			//Adding usage amount in hash for further verification
			logi.logInfo "actual_amt "+actual_amt
			String rec_no=getValueFromResponse(apiname,"//usage_rec_no")
			String plan_no=getUsedPlanNoP2(acct_no,expected_hash.get("usage_type").toString())
			logi.logInfo "plan_no in md_RECORD_USAGE_DETAILS_EXP "+plan_no
			Constant.recordedusageamounts.put(tcid+"-"+acct_no+"-"+rec_no+"-"+expected_hash.get("usage_type")+"-"+plan_no,actual_amt.toString())
			logi.logInfo("All recorded usages hash" +Constant.recordedusageamounts.toString() )
		}
		//Set all zero values to null as in DB
		if (expected_hash.get("rate").toString()=="0")
			expected_hash.put("rate","null")

		if (expected_hash.get("amt").toString()=="0")
			expected_hash.put("amt","null")



		return expected_hash.sort()
	}

	/**
	 * Getting plan no of the account
	 * @param accountNo
	 * @param usagetype
	 * @return String
	 * @throws SQLException
	 */
	String getUsedPlanNoP2(String accountNo,String usagetype) throws SQLException{
		logi.logInfo "Calling getUsedPlanNoP2 accountNo => "+accountNo
		ConnectDB db = new ConnectDB();
		String planNo,masterplanNo,suppplanNo
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		if(usagetype.equals(""))
		{
			String master_plan_query="select plan_no from ariacore.all_acct_active_plans where acct_no="+accountNo+" and supp_plan_ind=0 and  client_no="+clientNo+" and plan_no  in ( SELECT plan_no FROM ariacore.plan_services WHERE service_no IN  (SELECT service_no   FROM ariacore.services   WHERE usage_type IN  (SELECT usage_type FROM ariacore.usage WHERE acct_no = "+accountNo+" )) ) "
			String supp_plan_query=" select min(plan_no) from ariacore.all_acct_active_plans where acct_no="+accountNo+" and supp_plan_ind=1 and  client_no="+clientNo+" and plan_no in ( SELECT plan_no FROM ariacore.plan_services WHERE service_no IN  (SELECT service_no  FROM ariacore.services  WHERE usage_type IN (SELECT usage_type FROM ariacore.usage WHERE acct_no = "+accountNo+" ))) and acct_no ="+accountNo
			masterplanNo=db.executeQueryP2(master_plan_query)
			suppplanNo=db.executeQueryP2(supp_plan_query)
		}
		else
		{
			String master_plan_query="select plan_no from ariacore.all_acct_active_plans where acct_no="+accountNo+" and supp_plan_ind=0 and  client_no="+clientNo+" and plan_no  in ( SELECT plan_no FROM ariacore.plan_services WHERE service_no IN  (SELECT service_no   FROM ariacore.services   WHERE usage_type ="+usagetype+") )"
			String supp_plan_query="select min(plan_no) from ariacore.all_acct_active_plans where acct_no="+accountNo+" and supp_plan_ind=1 and  client_no="+clientNo+" and plan_no in ( SELECT plan_no FROM ariacore.plan_services WHERE service_no IN  (SELECT service_no  FROM ariacore.services  WHERE usage_type ="+usagetype+")) and acct_no ="+accountNo
			masterplanNo=db.executeQueryP2(master_plan_query)
			suppplanNo=db.executeQueryP2(supp_plan_query)
		}

		logi.logInfo("master plan no:"+masterplanNo)
		logi.logInfo("supp plan no:"+suppplanNo)

		String ser_order = Constant.lhmCaseVTADDoneList.get(Constant.TESTCASEID).toString()
		logi.logInfo("ser_order:"+ser_order)

		if(masterplanNo != null && suppplanNo != null && ( ser_order.contains("VTA_AD") || ser_order.contains(", AD") ) )
		{
			logi.logInfo("if loop")
			planNo=masterplanNo
		}
		else if(masterplanNo != null && ( ser_order.contains("VTA_AD") || ser_order.contains(", AD") ) )
		{
			logi.logInfo("else if loop")
			planNo=masterplanNo
		}
		else if(suppplanNo == null)
		{
			logi.logInfo("supp plan is null")
			planNo=masterplanNo
		}
		else
		{
			logi.logInfo("else loop")
			planNo=suppplanNo
		}
		logi.logInfo 'Plan No : : ' + planNo

		return planNo
	}

	/**
	 * Getting the expected usage amount
	 * @param accountNo
	 * @param invoiceNo
	 * @return String
	 */
	public String calculateUsageAmount(String accountNo,String invoiceNo){
		logi.logInfo("calling calculateUsageAmount")
		DecimalFormat d = new DecimalFormat("#.##########");
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		double totalUsgAmt=0.0
		String amount
		ConnectDB db = new ConnectDB();

		if(invoiceNo.contains("not updated")== true)
			invoiceNo=0
		List usagetypes=[]

		String usagetypeqry="SELECT distinct usage_type FROM ariacore.usage where acct_no="+accountNo+" and exclude_ind = 0 and (invoice_no="+invoiceNo+" or invoice_no is null)"
		ResultSet rs =db.executePlaQuery(usagetypeqry)
		while(rs.next())
		{
			usagetypes.add(rs.getString(1))
		}
		logi.logInfo "usagetype recorded : "+usagetypes

		for(usagetype in usagetypes )
		{
			if(usagetype!=null)
			{
				List uservices = md_GET_USAGE_SERVICE_NO(accountNo,usagetype)
				String billable_units=db.executeQueryP2("SELECT sum(billable_units) FROM ariacore.usage where usage_type="+usagetype+" and acct_no="+accountNo+" and invoice_no="+invoiceNo)
				HashMap<String, ArrayList<String>> usage_amt_hm = new HashMap<String, ArrayList<String>>()
				usage_amt_hm=md_USAGE_AMT_RATE_EXP(accountNo, uservices,billable_units,usagetype)
				for (uservice in uservices)
					totalUsgAmt=totalUsgAmt+usage_amt_hm.get(uservice)[1].toString().toDouble()
			}

		}

		double creditamt=db.executeQueryP2("select NVL(sum(credit),0) from ariacore.gl where acct_no="+accountNo+" and client_no="+clientNo+" and invoice_no="+invoiceNo).toString().toDouble()
		logi.logInfo("Total Usage plan amount without any credit"+totalUsgAmt.toString())
		totalUsgAmt=totalUsgAmt-creditamt

		logi.logInfo("Total Usage plan amount"+totalUsgAmt.toString())
		return d.format(totalUsgAmt).toString()
	}
	/**
	 * Getting the recorded usage details from usage table
	 * @param tcid
	 * @return LinkedHashMap
	 */
	LinkedHashMap md_RECORD_USAGE_DETAILS_DB(String tcid)
	{

		logi.logInfo "Calling md_RECORD_USAGE_DETAILS_DB"
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=tcid.split("-")[2]
		def acct_no
		String senior_acct_no
		String res_level_cd
		ConnectDB db = new ConnectDB()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')

		def invoice_No = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		def usage_rec_no=getValueFromResponse(apiname,ExPathRpcEnc.RECORD_USAGE_USAGERECNO1)
		logi.logInfo "usage_rec_no : "+usage_rec_no


		acct_no = getValueFromRequest(apiname,"//acct_no")
		String acct_no_qry = "Select senior_acct_no,resp_level_cd from ariacore.acct where acct_no="+acct_no
		ResultSet rs_acct_no_qry = db.executePlaQuery(acct_no_qry);
		while(rs_acct_no_qry.next())
		{
			senior_acct_no = rs_acct_no_qry.getString(1)
			res_level_cd = rs_acct_no_qry.getString(2)
		}

		if ((senior_acct_no != "null" && res_level_cd == "3") || (senior_acct_no != "null" && res_level_cd == "4"))
		{
			logi.logInfo("this is child account")
			acct_no = senior_acct_no
		}

		if (acct_no.equals ("NoVal"))
			acct_no=db.executeQueryP2("Select acct_no from ariacore.usage where rec_no="+usage_rec_no+" and client_no=" +clientNo)

		String usage_detail_query="select acct_no,usage_type,usage_units ,billable_units,exclude_ind,amt,rate,plan_no,TO_CHAR (usage_date, 'YYYY-MM-DD') as usage_date ,telco_from,telco_to ,exclude_comments ,qualifier_1,qualifier_2,qualifier_3,qualifier_4,parent_rec_no from ariacore.usage where acct_no="+acct_no+" and rec_no="+usage_rec_no
		ResultSet rs = db.executePlaQuery(usage_detail_query);
		LinkedHashMap record_usage_details = new LinkedHashMap()
		while(rs.next())
		{
			record_usage_details.put("acct_no",rs.getString("acct_no"))
			record_usage_details.put("usage_type",rs.getString("usage_type"))
			record_usage_details.put("usage_units",rs.getString("usage_units"))
			record_usage_details.put("billable_units",rs.getString("billable_units"))
			record_usage_details.put("exclude_ind",rs.getString("exclude_ind"))
			
			if(rs.getString("rate")!= null)
				record_usage_details.put("rate",d.format(rs.getString("rate").toDouble()).toString())
			else
				record_usage_details.put("rate",rs.getString("rate"))
				
	
			
			record_usage_details.put("amt",rs.getString("amt"))
			record_usage_details.put("usage_date",rs.getString("usage_date"))
			record_usage_details.put("exclusion_comments",rs.getString("exclude_comments"))
			record_usage_details.put("telco_from",rs.getString("telco_from"))
			record_usage_details.put("telco_to",rs.getString("telco_to"))
			record_usage_details.put("qualifier_1",rs.getString("qualifier_1"))
			record_usage_details.put("qualifier_2",rs.getString("qualifier_2"))
			record_usage_details.put("qualifier_3",rs.getString("qualifier_3"))
			record_usage_details.put("qualifier_4",rs.getString("qualifier_4"))
			record_usage_details.put("parent_rec_no",rs.getString("parent_rec_no"))

		}
		rs.close()
		logi.logInfo  "record_usage_details DB Final"+record_usage_details.toString()
		return record_usage_details.sort()
	}
	/**
	 * Getting the service numbers of the usage type
	 * @param acct_no
	 * @param usagetype
	 * @return List
	 */
	List md_GET_USAGE_SERVICE_NO(String acct_no,String usagetype)
	{
		logi.logInfo( "calling md_GET_USAGE_SERVICE_NO acct_no => "+acct_no+" : usagetype=> "+usagetype)
		List<String> ser_no=[]
		String plan_no
		String client_no
		ConnectDB db = new ConnectDB()

		client_no =Constant.mycontext.expand('${Properties#Client_No}')
		plan_no=getUsedPlanNoP2(acct_no,usagetype)
		logi.logInfo("Plan no from getUsedPlanNoP2"+plan_no)

		if(plan_no!="null")
		{
			String ser_no_qry = "SELECT DISTINCT service_no FROM ariacore.plan_services WHERE service_no IN (SELECT DISTINCT service_no FROM ariacore.services WHERE usage_type="+usagetype+" AND usage_based =1) AND plan_no ="+plan_no
			ResultSet rs=db.executePlaQuery(ser_no_qry)
			while(rs.next())
			{
				ser_no.add(rs.getString(1))
			}
		}

		logi.logInfo("service_no list"+ser_no)
		return ser_no
	}
	/**
	 * Getting the service numbers of the usage type in a plan
	 * @param acct_no
	 * @param usagetype
	 * @param plan_no
	 * @return List
	 */
	List md_GET_USAGE_SERVICE_NO(String acct_no,String usagetype,String plan_no)
	{
		logi.logInfo( "calling md_GET_USAGE_SERVICE_NO acct_no => "+acct_no+" : usagetype=> "+usagetype+" : plan_no"+plan_no)
		List<String> ser_no=[]
		String client_no
		ConnectDB db = new ConnectDB()
		client_no =Constant.mycontext.expand('${Properties#Client_No}')

		String ser_no_qry = "SELECT DISTINCT service_no FROM ariacore.plan_services WHERE service_no IN (SELECT DISTINCT service_no FROM ariacore.services WHERE usage_type="+usagetype+" AND usage_based =1) AND plan_no ="+plan_no
		ResultSet rs=db.executePlaQuery(ser_no_qry)
		while(rs.next())
		{
			ser_no.add(rs.getString(1))
		}
		logi.logInfo("service_no list"+ser_no)
		return ser_no
	}

	public String md_Is_Usage_Billed_a(String tcid)
	{
		return md_Is_Usage_Billed("a")
	}
	public String md_Is_Usage_Billed_b(String tcid)
	{
		return md_Is_Usage_Billed("b")
	}
	public String md_Is_Usage_Billed_c(String tcid)
	{
		return md_Is_Usage_Billed("c")
	}
	public String md_Is_Usage_Billed_d(String tcid)
	{
		return md_Is_Usage_Billed("d")
	}

	/**
	 * Verifying usage is billed or not
	 * @param suffix
	 * @return String
	 */
	public String md_Is_Usage_Billed(String suffix)
	{
		String api_name="record_usage"+"."+suffix
		boolean uflag = false
		def acct_no = getValueFromRequest(api_name,"//acct_no")
		String client_no =Constant.mycontext.expand('${Properties#Client_No}')
		String usage_type=getValueFromRequest(api_name,"//usage_type").toString()
		String recno=getValueFromResponse(api_name,"//usage_rec_no")

		if(recno != "NoVal"){
			logi.logInfo( " api_name => " +api_name+" usage_type => "+usage_type)
			ConnectDB db = new ConnectDB()
			String invoiceno=getInvoiceNoVT(client_no, acct_no).toString();
			String unbilled_flag =db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name = 'INVOICE_UNBILLED_USAGE_DURING_PRORATION' and client_no="+client_no)
			String usage_line_items_count
			boolean auto_RateFlag = get_Auto_Rate_Unrated_Usage_Flag()
			if(unbilled_flag == "TRUE" )
			{
				usage_line_items_count=db.executeQueryP2("select count(*) from ariacore.usage where acct_no="+acct_no+"  and exclude_ind=0 and rec_no="+recno+" and ( invoice_no ="+invoiceno+ " or invoice_no is null )")

			}else

			{
				usage_line_items_count=db.executeQueryP2("select count(*) from ariacore.usage where acct_no="+acct_no+"  and exclude_ind=0 and rec_no="+recno+" and ( invoice_no ="+invoiceno +")")
			}

			logi.logInfo( "usage_line_items_count "+usage_line_items_count)
			if(usage_line_items_count.toInteger()>=1)
			{
				uflag=true
			}
		}
		return uflag.toString().toUpperCase()
	}
	/**
	 * Getting the expected usage amount and rate per unit
	 * @param acct_no
	 * @param service_nos
	 * @param usage_units
	 * @param usagetype
	 * @return HashMap
	 */
	HashMap md_USAGE_AMT_RATE_EXP(String acct_no,List service_nos,String usage_units,String usagetype)
	{

		logi.logInfo("calling md_USAGE_AMT_RATE_EXP acct_no=>"+acct_no+" service_nos=>"+service_nos.toString()+"usage_units=> "+usage_units)
		String apiname = Constant.SERVICENAME
		boolean flaag=true; 
		HashMap<String, ArrayList<String>> usage_hm = new HashMap<String, ArrayList<String>>()
		LinkedHashMap<String,String> hm_unit_rates=new LinkedHashMap<String, String>();

		String client_no =Constant.mycontext.expand('${Properties#Client_No}')
		String pooling_enabled
		String plan_no=getUsedPlanNoP2(acct_no,usagetype)
		int acc_cnt=0
		String price_rule
		double input_units=usage_units.toDouble();
		int rate_rnd,lag_days
		double already_recorded_units
		String acc_months
		Date acct_created_date,usg_date,virtual_date
		ConnectDB db = new ConnectDB()
		String allow_acc= db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name='ALLOW_USAGE_ACCUMULATION' and client_no="+client_no).toString()
		String poolingcnt= db.executeQueryP2("Select count(param_val) from ariacore.acct_plan_params where param_name='ENABLE_USAGE_POOLING' and acct_no="+acct_no+" and plan_no="+plan_no).toString()
		if(poolingcnt.equals("0"))
		{
			 pooling_enabled = "FALSE"
		}
		else
		{
			 pooling_enabled = db.executeQueryP2("Select param_val from ariacore.acct_plan_params where param_name='ENABLE_USAGE_POOLING' and acct_no="+acct_no+" and plan_no="+plan_no).toString()
		}
		
		if(acct_months_hm.containsKey(acct_no+"-"+plan_no) == true)
		{
			acc_months = acct_months_hm.get(acct_no+"-"+plan_no)
			logi.logInfo("Reterieved accumulation for that acct:"+acc_months)
	
		}
		else
		{
			acc_months = db.executeQueryP2("SELECT param_val from ariacore.acct_plan_params where param_name='USAGE_ACCUMULATION_RESET_MONTHS' and acct_no= "+acct_no+" and plan_no="+plan_no+" and client_no="+client_no).toString()
			if(!acc_months.equalsIgnoreCase("null"))
			{
				String firInvFlg = db.executeQueryP2("SELECT param_val from ariacore.aria_client_params where param_name='INCLUDE_USAGE_CHARGES_ON_FIRST_INVOICE' and client_no="+client_no)
				if(firInvFlg.equalsIgnoreCase("TRUE"))
				{
					acc_months = (acc_months.toInteger()-1).toString()
				}
				acct_months_hm.put(acct_no+"-"+plan_no,acc_months)
				logi.logInfo("storing acct no and reset months:"+acct_months_hm.get(acct_no+"-"+plan_no).toString())
			}
		}
		if(!acct_months_hm.get(acct_no+"-"+plan_no).toString().equalsIgnoreCase("null"))
		{
			acc_months = db.executeQueryP2("SELECT param_val from ariacore.acct_plan_params where param_name='USAGE_ACCUMULATION_RESET_MONTHS' and acct_no= "+acct_no+" and plan_no="+plan_no+" and client_no="+client_no).toString()
			if(acc_months.toInteger()>0)
			{
				flaag=true;
			}
			else if(acc_months.equals("0"))
			{
				acc_cnt++;
				if(acc_cnt==1)
				{
					
				if(!accumutemp.containsKey(acct_no))
				{
				accumutemp.put(acct_no,apiname)
				flaag = true;
				logi.logInfo("Storing apiname in the accumutemp: "+accumutemp.get(acct_no).toString())
				}
				
				}
			}
		}
		if(accumutemp.containsKey(acct_no))
		{
			if(!accumutemp.get(acct_no).equals(apiname))
			{
			
			flaag=false;
			}
		}
		
//		//		if(acct_months_hm.containsKey(acct_no+"-"+plan_no) == true)
////		{
////			acc_months = acct_months_hm.get(acct_no+"-"+plan_no)
////			logi.logInfo("Reterieved accumulation for that acct:"+acc_months)
////			acc_cnt--;
////			if(acc_cnt == 0)
////			{
////				acc_cnt=0;
////			}
////		}
////		else
////		{
//			
//			acc_months1 = db.executeQueryP2("SELECT param_val from ariacore.acct_plan_params where param_name='USAGE_ACCUMULATION_RESET_MONTHS' and acct_no= "+acct_no+" and plan_no="+plan_no+" and client_no="+client_no).toString()
//			
//			if(!acc_months1.equalsIgnoreCase("null"))
//			{
//				if(acc_months1.toInteger()>0)
//				{
//					acc_months = acc_months1
//				}
//				String firInvFlg = db.executeQueryP2("SELECT param_val from ariacore.aria_client_params where param_name='INCLUDE_USAGE_CHARGES_ON_FIRST_INVOICE' and client_no="+client_no)
//				if(firInvFlg.equalsIgnoreCase("TRUE"))
//				{
//					
//					if(acc_months1.equals("0"))
//					{
//						acc_cnt++;
//					}
//					logi.logInfo("Include usgae charge on first invoice flg TRUE")
//					//acc_months = (acc_months.toInteger()-1).toString()
//					logi.logInfo("acc_months: "+acc_months)
//					
//				}
//			}
//			acct_months_hm.put(acct_no+"-"+plan_no, acc_months)
//			logi.logInfo("storing acct no and reset months:"+acct_months_hm.get(acct_no).toString())
//	//	}
//		logi.logInfo("Acc Cnter Value: "+acc_cnt)
		
		rate_rnd = db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name ='USAGE_RATE_PER_UNIT_SCALE' and  client_no="+client_no).toInteger()
		int amt_write_scale = db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name ='USAGE_RECORD_WRITE_SCALE' and  client_no="+client_no).toString().toInteger()
		
		

		if(pooling_enabled.equalsIgnoreCase("TRUE"))
		{
			logi.logInfo("pooling enabled acct")
			already_recorded_units =  db.executeQueryP2("SELECT CASE WHEN TOTAL_UNITS = 0 THEN (SELECT NVL(SUM(billable_units),0) AS TOTAL_UNITS FROM ariacore.usage WHERE exclude_ind  =0 and acct_no="+acct_no+" AND (USAGE_DATE > (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+" and bill_DATE < (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+") ) and USAGE_DATE < (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+"))) ELSE TOTAL_UNITS  END AS FINAL_TOTAL_UNITS FROM ( SELECT NVL(SUM(billable_units),0) AS TOTAL_UNITS FROM ariacore.usage WHERE exclude_ind  =0 AND acct_no ="+acct_no+" AND (USAGE_DATE > (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+ " AND client_no ="+client_no+")))").toString().toDouble()
		}

		else if(!acct_months_hm.get(acct_no+"-"+plan_no).toString().equalsIgnoreCase("null") && allow_acc.equalsIgnoreCase("TRUE") && flaag==true)
		{
			logi.logInfo("accumalation enabled acct")
			String created_date_qry = "select created from ariacore.acct where acct_no="+acct_no
			ResultSet rs_created_date_qry = db.executePlaQuery(created_date_qry)
			while(rs_created_date_qry.next())
			{
				acct_created_date = rs_created_date_qry.getDate(1)
			}
			int bill_interval =db.executeQueryP2("SELECT distinct billing_interval FROM ariacore.client_plan WHERE plan_no="+plan_no+" AND client_no="+client_no).toInteger()

			if(bill_interval == 1)
			{
				logi.logInfo("Monthly plan")
				lag_days = db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name= 'MONTHLY_BILL_LAG_DAYS' and client_no="+client_no).toInteger()
			}
			else if(bill_interval == 3)
			{
				logi.logInfo("Quartely plan")
				lag_days = db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name= 'QUARTERLY_BILL_LAG_DAYS' and client_no="+client_no).toInteger()
			}
			else if(bill_interval == 6)
			{
				logi.logInfo("Semi annulaly plan")
				lag_days = db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name= 'SEMI_ANNUAL_BILL_LAG_DAYS' and client_no="+client_no).toInteger()
			}
			logi.logInfo("bill lag_days:"+lag_days)
			logi.logInfo("acct_created_date:"+acct_created_date.toString())
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")
			Calendar now = Calendar.getInstance();
			now.setTime(acct_created_date);
			now.add(Calendar.MONTH,acc_months.toInteger());
			now.add(Calendar.DAY_OF_MONTH,lag_days)
			String accu_valid_str = sdf.format(now.getTime())
			logi.logInfo("acc_valid_date in String :"+accu_valid_str)
			Date accu_valid_date = sdf.parse(accu_valid_str)
			logi.logInfo("acc_valid_date :"+accu_valid_date.toString())
			String usage_date_qry = "select usage_date from ariacore.usage where usage_type="+usagetype+ "and exclude_ind=0 and acct_no="+acct_no+ "and usage_units="+usage_units

			ResultSet rs = db.executePlaQuery(usage_date_qry)
			while(rs.next())
			{
				usg_date = rs.getDate(1)
			}

			logi.logInfo("usg date :"+usg_date.toString())

			String vt_qry = "Select ARIACORE.ARIAVIRTUALTIME("+client_no+") from DUAL"
			ResultSet rs_vt_qry = db.executePlaQuery(vt_qry)

			while(rs_vt_qry.next())
			{
				virtual_date=rs_vt_qry.getDate(1)
			}

			if(virtual_date <= accu_valid_date)
			{
				Date start_date = acct_created_date
				Date end_date = accu_valid_date+1
				logi.logInfo("start_date:"+start_date.toString()+" and End date:"+end_date.toString())
				already_recorded_units=db.executeQueryP2("select NVL(sum(billable_units),0) from ariacore.usage where usage_type="+usagetype+" and exclude_ind=0 and acct_no="+acct_no+" and  TO_CHAR(usage_date,'yyyy-MM-dd') BETWEEN '"+start_date+"' and '"+end_date+"'").toString().toDouble()
			}
			else if(virtual_date > accu_valid_date)
			{
				logi.logInfo("Accumalation has been Reset")
				Date start_date = accu_valid_date+1
				now.setTime(accu_valid_date);
				now.add(Calendar.MONTH,acc_months.toInteger());
				accu_valid_str = sdf.format(now.getTime())
				accu_valid_date = sdf.parse(accu_valid_str)
				Date end_date = accu_valid_date+1
				logi.logInfo("start_date:"+start_date.toString())
				logi.logInfo("End date:"+end_date.toString())
				already_recorded_units=db.executeQueryP2("select NVL(sum(billable_units),0) from ariacore.usage where usage_type="+usagetype+" and exclude_ind=0 and acct_no="+acct_no+" and  TO_CHAR(usage_date,'yyyy-MM-dd') BETWEEN '"+start_date+"' and '"+end_date+"'").toString().toDouble()
			}


		}
		else
		{
			already_recorded_units=db.executeQueryP2("SELECT CASE WHEN TOTAL_UNITS = 0 THEN (SELECT NVL(SUM(billable_units),0) AS TOTAL_UNITS FROM ariacore.usage WHERE usage_type ="+usagetype+" AND exclude_ind  =0 AND acct_no="+acct_no+" AND (USAGE_DATE > (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+" and bill_DATE < (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+") ) and USAGE_DATE < (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+"))) ELSE TOTAL_UNITS  END AS FINAL_TOTAL_UNITS FROM ( SELECT NVL(SUM(billable_units),0) AS TOTAL_UNITS FROM ariacore.usage WHERE usage_type ="+usagetype+" AND exclude_ind  =0 AND acct_no ="+acct_no+" AND (USAGE_DATE > (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+ " AND client_no ="+client_no+")))").toString().toDouble()
		}
		for(int flg=0;flg<service_nos.size();flg++)
		{
			price_rule=md_get_tier_pricing_type(service_nos.get(flg),acct_no,usagetype)

			ArrayList<String> rate_amt = []
			hm_unit_rates = GET_PRICE_TIER(acct_no,service_nos.get(flg),plan_no)
			if(price_rule.equals("1"))
			{

				double rem_units=input_units;
				double sum=0.0;
				double unit_rate;

				logi.logInfo "Rate tier hash from DB standard"+hm_unit_rates.toString()
				List<String> keyList = new ArrayList<String>(hm_unit_rates.keySet());
				List<String> valList = new ArrayList<String>(hm_unit_rates.values());
				double lb;
				if(hm_unit_rates.size()==1)
				{
					sum=input_units*(hm_unit_rates.get("UBN").toDouble())
				}

				else
				{
					for(int i=0;i<keyList.size();)
					{
						if(rem_units>0.0)
						{
							int j=i-1;
							double ub;

							double max_units;

							if(j<0)
							{
								logi.logInfo "already_recorded_units now"+already_recorded_units
								lb=already_recorded_units-usage_units.toDouble();
								logi.logInfo "new lb"+lb
							}
							else
							{
								lb=(keyList.get(j).split("UB")[1]).toDouble()
							}

							if(keyList.get(i).equals("UBN"))
							{
								max_units=rem_units;
							}

							else
							{
								ub=(keyList.get(i).split("UB")[1]).toDouble()
								max_units=ub-lb;
							}


							logi.logInfo("max units"+max_units);
							logi.logInfo("current i"+i);

							if(max_units<0.0)
							{

								logi.logInfo "max units less than 0"
								while(max_units<0.0)
								{
									logi.logInfo("max:"+max_units)
									logi.logInfo "inside while"
									if(i<keyList.size())
									{
										logi.logInfo("i value:"+i)
										logi.logInfo("keyList.get(i) value:"+keyList.get(i).toString())
										if(keyList.get(i).equals("UBN"))
										{
											max_units=rem_units;
										}
										else
										{
											if(keyList.get(i+1).equals("UBN"))
											{
												max_units=rem_units;
											}
											else
											{
												ub=(keyList.get(i+1).split("UB")[1]).toDouble()
												max_units=ub-lb;
											}
										}
										i++

									}
									else
									{
										i=i-1
									}
								}
							}



							if(rem_units>=max_units)
							{
								rem_units=rem_units-max_units;
								unit_rate=max_units*(valList.get(i).toDouble());
								sum=sum+unit_rate;
								logi.logInfo("sum"+sum);
								logi.logInfo("rem_units"+rem_units);
							}

							else
							{
								unit_rate=rem_units*(valList.get(i).toDouble());
								sum=sum+unit_rate;
								logi.logInfo("else sum"+sum);
								break;
							}

							i++
						}
						else
						{
							break;
						}
					}
				}
				logi.logInfo("total usgae rate"+sum);
				logi.logInfo("rate per unit"+(sum/input_units));

				rate_amt.add(((sum.toDouble().round(amt_write_scale))/input_units).toDouble().round(rate_rnd).toString())
				rate_amt.add(sum.toString())
				usage_hm.put(service_nos.get(flg), rate_amt)
				logi.logInfo("standard:"+usage_hm.toString())
				logi.logInfo "lb now"+lb


			}
			else if(price_rule.equals("2"))
			{
				int sum=0;

				logi.logInfo "Rate tier hash from DB volume discount"+hm_unit_rates.toString()
				List<String> keyList = new ArrayList<String>(hm_unit_rates.keySet());
				List<String> valList = new ArrayList<String>(hm_unit_rates.values());
				if(input_units > 0)
				{
					if(hm_unit_rates.size()==1)
					{
						sum=input_units*(hm_unit_rates.get("UBN").toInteger())
					}
					else
					{
						for(int i=0;i<keyList.size();i++)
						{
							if(keyList.get(i)!="UBN")
							{
								int max_units = (keyList.get(i).split("UB")[1]).toInteger()
								logi.logInfo("vd max units"+max_units)
								if(input_units<=max_units)
								{
									sum=input_units*valList.get(i).toInteger()
									break;
								}
							}

							else
							{
								sum=input_units*valList.get(i).toInteger()
								break;
							}

						}
					}
				}
				logi.logInfo("total usgae rate"+sum);
				logi.logInfo("rate per unit"+(sum/input_units));


				rate_amt.add(((sum.toDouble().round(amt_write_scale))/input_units).toDouble().round(rate_rnd).toString())
				rate_amt.add(sum.toString())
				usage_hm.put(service_nos.get(flg), rate_amt)
				logi.logInfo("Volume Discount:"+usage_hm.toString())

			}

			else if(price_rule.equals("3"))
			{
				int sum;

				logi.logInfo "Rate tier hash from DB flat"+hm_unit_rates.toString()
				List<String> keyList = new ArrayList<String>(hm_unit_rates.keySet());
				List<String> valList = new ArrayList<String>(hm_unit_rates.values());
				if(input_units)
				{
					if(hm_unit_rates.size()==1)
					{
						sum=1*(hm_unit_rates.get("UBN").toInteger())

					}
					else
					{
						for(int i=0;i<keyList.size();i++)
						{
							if(keyList.get(i)!="UBN")
							{
								int max_units = (keyList.get(i).split("UB")[1]).toInteger()

								if(input_units<=max_units)
								{

									sum=(1*valList.get(i).toString().toInteger())
									break
								}
							}

							else
							{
								sum=1*valList.get(i).toInteger()
								break;
							}
						}
					}
				}
				logi.logInfo("total usgae rate"+sum);
				logi.logInfo("rate per unit"+sum/1);


				rate_amt.add((sum/1).toString())
				rate_amt.add(sum.toString())
				usage_hm.put(service_nos.get(flg), rate_amt)
				logi.logInfo("Flat:"+usage_hm.toString())
			}
		}
		return usage_hm
	}
	/**
	 * Getting the pricing rule no from plan_services table
	 * @param service_no
	 * @param acct_no
	 * @param usagetype
	 * @return String
	 */
	public String md_get_tier_pricing_type(String service_no,String acct_no,String usagetype)
	{
		logi.logInfo( "Calling md_get_tier_pricing_type")
		String plan_no=getUsedPlanNoP2(acct_no,usagetype)
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		String query="SELECT TIERED_PRICING_RULE_NO from (SELECT plan_no,  service_no,  TIERED_PRICING_RULE_NO FROM ariacore.plan_services WHERE plan_no IN   ("+plan_no+"  ) AND client_no   = "+client_no+" AND service_no IN   (SELECT service_no   FROM ariacore.services   WHERE custom_to_client_no = "+client_no+"   AND usage_based = 1) ORDER BY service_no)where service_no="+service_no
		ConnectDB db = new ConnectDB()
		String price_type=db.executeQueryP2(query)
		logi.logInfo "Price type for service"+service_no+" : " +price_type

		return price_type
	}
	//Overloaded with new parameter plan_no
	public String md_get_tier_pricing_type(String service_no,String acct_no,String usagetype,String plan_no)
	{
		logi.logInfo( "Calling md_get_tier_pricing_type")
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		String query="SELECT TIERED_PRICING_RULE_NO from (SELECT plan_no,  service_no,  TIERED_PRICING_RULE_NO FROM ariacore.plan_services WHERE plan_no IN   ("+plan_no+"  ) AND client_no   = "+client_no+" AND service_no IN   (SELECT service_no   FROM ariacore.services   WHERE custom_to_client_no = "+client_no+"   AND usage_based = 1) ORDER BY service_no)where service_no="+service_no
		ConnectDB db = new ConnectDB()
		String price_type=db.executeQueryP2(query)
		logi.logInfo "Price type for service"+service_no+" : " +price_type

		return price_type
	}
	/**
	 * Getting the price tier (to unit), rate for the respective service of the plan
	 * @param acct_no
	 * @param ser_no
	 * @param plan_no
	 * @return LinkedHashMap
	 */
	LinkedHashMap GET_PRICE_TIER(String acct_no,String ser_no,String plan_no)
	{
		logi.logInfo "Calling GET_PRICE_TIER "+acct_no+" :: "+ser_no+" :: "+plan_no
		LinkedHashMap<String,String> hm_rates=new LinkedHashMap<String, String>();
		DecimalFormat d = new DecimalFormat("#.##########");
		String client_no,threshold,multiplier,multiplier_units
		ConnectDB db = new ConnectDB()

		String cln_qry="Select client_no from ariacore.acct where acct_no="+acct_no
		ResultSet rs_cln_qry=db.executePlaQuery(cln_qry)
		while(rs_cln_qry.next())
		{
			client_no = rs_cln_qry.getString(1)

		}

		String adv_bill_qry = "Select unbillable_usg_threshold,calc_usage_using_recur_factor from ariacore.plan_services where service_no="+ser_no+" and plan_no="+plan_no+" and client_no="+client_no
		ResultSet rs_adv= db.executePlaQuery(adv_bill_qry)

		while(rs_adv.next())
		{
			threshold = rs_adv.getString(1)
			multiplier = rs_adv.getString(2)
		}

		logi.logInfo("threshold :"+threshold +" and multiplier: "+multiplier)
		if(multiplier.equals("1"))
		{
			logi.logInfo("Adv billing option - Usage Tier Multiplier enabled:")
			multiplier_units = db.executeQueryP2("Select recurring_factor from ariacore.all_acct_recurring_factors where acct_no="+acct_no+" and plan_no="+plan_no).toString()
		}

		String unit_rate_qry = "SELECT NVL(to_unit,0)as to_unit,rate_per_unit FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SERVICE_NO="+ser_no+" AND SCHEDULE_NO in (SELECT RATE_SCHEDULE_NO FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO  ="+acct_no+" AND PLAN_NO  ="+plan_no+" AND SERVICE_NO ="+ser_no+" AND CLIENT_NO  ="+client_no+") AND CLIENT_NO   ="+client_no
		ResultSet rs_unit_rate_qry=db.executePlaQuery(unit_rate_qry)
		while(rs_unit_rate_qry.next())
		{
			String unit = rs_unit_rate_qry.getString(1)
			String rate=rs_unit_rate_qry.getString(2)

			if(multiplier.equals("1"))
			{
				unit = d.format((unit.toDouble() * multiplier_units.toDouble()).toDouble().round(2)).toString()
			}

			if(unit.equals("0"))
			{
				unit="N"
			}

			hm_rates.put("UB"+unit,rate)
			logi.logInfo( "hm_rates "+hm_rates.toString())
		}

		return hm_rates
	}
	/**
	 * Getting parent account no from create_acct_complete api
	 * @param testid
	 * @return String
	 */
	public String md_ActualInvoice_Amt(String testid) {

		String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
		if(accountNo == 'NoVal')
		{
			 accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		return md_ActualInvoice_Amt(accountNo,testid);
	}
	/**
	 * Getting child account no from create_acct_complete.a api
	 * @param testid
	 * @return String
	 */
	public String md_ActualInvoice_Amt_child(String testid) {

		String accountNo=getValueFromResponse('create_acct_complete.a',"//acct_no")
		return md_ActualInvoice_Amt(accountNo,testid);
	}
	/**
	 * Getting the latest invoice amount of the account
	 * @param accountNo
	 * @param testCaseId
	 * @return String
	 */
	public String md_ActualInvoice_Amt(String accountNo,String testCaseId) {
		String invoiceNo=""
		logi.logInfo( "Invoice number md_ActualInvoice_Amttttttttttttttttttttttttt " +invoiceNo)
		DecimalFormat d = new DecimalFormat("#.##########");
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		//String service  = Constant.SERVICENAME
		logi.logInfo 'Calling md_ActualInvoice accountNo=> '+accountNo
		String amount="0"
		invoiceNo = getInvoiceNoVT(clientNo, accountNo).toString();
		logi.logInfo( "Invoice number md_ActualInvoice_Amt " +invoiceNo)
		if(invoiceNo.contains("not updated")!= true) //Got invoice number without any error
		{
			String query = "SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo
			if(accountNo == "NoVal" || invoiceNo == "NoVal") {
				amount = "No Val ret"
			} else {
				ConnectDB dbActInvoice = new ConnectDB();
				ResultSet resultSetActInvoice = dbActInvoice.executePlaQuery(query)
				amount= ""
				if (resultSetActInvoice.next())
					amount = resultSetActInvoice.getString(1)
				resultSetActInvoice.close()
			}
		}else
		{
			amount="Invoice not updated in DB "
		}
		return d.format(amount.toDouble()).toString();
	}

	/**
	 * Getting the expected invoice amount of the account
	 * @param accountNo
	 * @param testCaseId
	 * @return String
	 */
	public String md_ExpInvoice_Amt(String testcaseid,String accountNo){
		DecimalFormat d = new DecimalFormat("#.##########");
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = database.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time : "+ currentVirtualTime
		//int useddays=database.executeQueryP2("SELECT (TRUNC(next_bill_date) -  trunc(plan_date)) AS days FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo).toInteger()
		int useddays=database.executeQueryP2("SELECT TRUNC(LAST_BILL_THRU_DATE+1) - (select CASE WHEN  (trunc(plan_date) >= trunc(last_recur_bill_date) or last_recur_bill_date is null ) THEN trunc(plan_date) ELSE trunc(last_recur_bill_date)  END 	FROM ariacore.acct WHERE acct_no = "+accountNo+")from ariacore.acct WHERE acct_no = "+accountNo+"  AND client_no ="+clientNo).toInteger()

		Map<String, HashMap<String,String>> hash = getPlanServiceAndTieredPricing()

		Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
		resultHash.putAll(hash)

		logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()

		Iterator iterator = resultHash.entrySet().iterator()
		logi.logInfo "Before Key setting "

		def invoice = 0
		double proration_factor

		while (iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) iterator.next()
			String planNo = pairs.getKey().toString()
			logi.logInfo "Execution for the plan :  "+ planNo
			String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
			String planStatusQuery, planStatusDataQuery

			int planTypeInd = Integer.parseInt(database.executeQueryP2(planTypeQuery))

			logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo


			if(planTypeInd==1) {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT_SUPP_PLAN_MAP AC WHERE ACCT_NO   ="+accountNo+" AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date from ariacore.ACCT_SUPP_PLAN_MAP where acct_no = "+ accountNo +"AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo

			}
			else {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+" AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+ " AND CLIENT_NO= " + clientNo

			}

			int planStatus = Integer.parseInt(database.executeQueryP2(planStatusQuery))
			logi.logInfo "Plan Status : "+planStatus+ " plan : " +planNo
			logi.logInfo "Plan planStatusDataQuery : "+planStatusDataQuery+ " plan : " +planNo


			if(planStatus!=0){
				String statusDateDB = database.executeQueryP2(planStatusDataQuery)
				logi.logInfo "Status date from DB: "+statusDateDB
				Date statusDate = simpleFormat.parse(statusDateDB)

				// Stores the current virtual time in the specified format
				Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
				logi.logInfo "planStatus : "+planStatus+ " plan : " +planNo
				logi.logInfo "statusDate.compareTo(currentVTDate) : "+statusDate.compareTo(currentVTDate)+ " plan : " +planNo


				if((planStatus==1 && statusDate.compareTo(currentVTDate)<=0)|| (planTypeInd==0)){

					Map<String,String> valueMap = pairs.getValue()
					Map<String,String> value = new TreeMap<String,String> ()
					value.putAll(valueMap)

					int billingInterval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planNo+ "and client_no="+clientNo))
					Date nextbilldate
					ResultSet rs=database.executePlaQuery("SELECT TRUNC(LAST_BILL_THRU_DATE+1) FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo)
					rs.next()
					nextbilldate=rs.getDate(1)
					Date virtualstartdate = subractMonthsFromGivenDate(nextbilldate,billingInterval)

					logi.logInfo "virtualstartdate "+virtualstartdate
					logi.logInfo "nextbilldate " +nextbilldate
					int durationOfPlan = getDaysBetween(virtualstartdate,nextbilldate )
					logi.logInfo "Duration of a plan : "+planNo + " :: "+durationOfPlan
					logi.logInfo "DaysInvoice "+useddays
					logi.logInfo "daysInvoice % durationOfPlan Value : "+ (useddays % durationOfPlan )
					int tmp_days = durationOfPlan-useddays
					if(tmp_days<=0)
						tmp_days = durationOfPlan
					proration_factor=useddays/durationOfPlan.toDouble()
					logi.logInfo "proration_factor "+proration_factor
					Iterator service = value.entrySet().iterator()
					logi.logInfo "Invoice calculation for a plan : "+planNo
					String unitsquery="select recurring_factor from ariacore.all_acct_recurring_factors where ACCT_NO ="+accountNo+" and plan_no="+planNo
					int noofUnits =database.executeQueryP2(planStatusQuery).toInteger()

					logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo

					while(service.hasNext()){
						Map.Entry servicePairs = (Map.Entry) service.next()
						String serviceNo = servicePairs.getKey().toString()
						logi.logInfo "Invoice calculation for a service : "+serviceNo
						int seqNo=1;
						String tieredPricing = servicePairs.getValue().toString()
						int tieredPricingInt = Integer.parseInt(tieredPricing)
						logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
						String altFee = "NoVal"
						def serviceInvoice
						String query
						switch(tieredPricingInt){
							case 1:
								logi.logInfo "Case 1 "
								if(!altFee.equals("NoVal"))
								{
									query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
								}
								else
									query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
								serviceInvoice = database.executeQueryP2(query).toDouble()
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								invoice  = invoice+serviceInvoice
								logi.logInfo "After adding with invoice " + invoice
								seqNo++;
								break;
							case 2 :
								logi.logInfo "Case 2"
								if(!altFee.equals("NoVal"))
								{
									query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
								}
								else
									query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"

								serviceInvoice = database.executeQueryP2(query).toDouble()
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								invoice  = invoice+serviceInvoice
								logi.logInfo "After adding with invoice " + invoice
								break;
							case 3 :
								logi.logInfo "Case 3"
								query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
								serviceInvoice = database.executeQueryP2(query).toDouble()
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								invoice  = invoice+serviceInvoice
								logi.logInfo "After adding with invoice " + invoice
								break;
						}
					}

				}
			}

		}

		double tinvoice=(invoice*proration_factor.toDouble()).round(2)

		return d.format(tinvoice).toString()
	}


	public def md_BULK_RECORD_USAGE_DETAILS_DB1(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_DB(testCaseId,"1")
	}
	public def md_BULK_RECORD_USAGE_DETAILS_DB2(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_DB(testCaseId,"2")
	}
	public def md_BULK_RECORD_USAGE_DETAILS_DB3(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_DB(testCaseId,"3")
	}
	public def md_BULK_RECORD_USAGE_DETAILS_DB4(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_DB(testCaseId,"4")
	}

	public def md_BULK_RECORD_USAGE_DETAILS_DB5(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_DB(testCaseId,"5")
	}
	public def md_BULK_RECORD_USAGE_DETAILS_DB6(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_DB(testCaseId,"6")
	}
	public def md_BULK_RECORD_USAGE_DETAILS_DB7(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_DB(testCaseId,"7")
	}
	public def md_BULK_RECORD_USAGE_DETAILS_DB8(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_DB(testCaseId,"8")
	}

	public def md_BULK_RECORD_USAGE_DETAILS_EXP1(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_EXP(testCaseId,"1")
	}
	public def md_BULK_RECORD_USAGE_DETAILS_EXP2(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_EXP(testCaseId,"2")
	}
	public def md_BULK_RECORD_USAGE_DETAILS_EXP3(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_EXP(testCaseId,"3")
	}
	public def md_BULK_RECORD_USAGE_DETAILS_EXP4(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_EXP(testCaseId,"4")
	}

	public def md_BULK_RECORD_USAGE_DETAILS_EXP5(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_EXP(testCaseId,"5")
	}
	public def md_BULK_RECORD_USAGE_DETAILS_EXP6(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_EXP(testCaseId,"6")
	}
	public def md_BULK_RECORD_USAGE_DETAILS_EXP7(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_EXP(testCaseId,"7")
	}
	public def md_BULK_RECORD_USAGE_DETAILS_EXP8(String testCaseId)
	{
		return md_BULK_RECORD_USAGE_DETAILS_EXP(testCaseId,"8")
	}
	/**
	 * Getting the recorded usage (done through bulk_record_usage api) details from usage table
	 * @param tcid
	 * @param seq_no
	 * @return HashMap
	 */
	HashMap md_BULK_RECORD_USAGE_DETAILS_DB(String tcid,String seq_no)
	{
		String apiname=tcid.split("-")[2]
		String senior_acct_no
		String res_level_cd
		String acct_no = getValueFromRequest(apiname,"//usage_records_row["+seq_no+"]/acct_no").toString()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		
		String acct_no_qry = "Select senior_acct_no,resp_level_cd from ariacore.acct where acct_no="+acct_no
		ResultSet rs_acct_no_qry = db.executePlaQuery(acct_no_qry);
		while(rs_acct_no_qry.next())
		{
			senior_acct_no = rs_acct_no_qry.getString(1)
			res_level_cd = rs_acct_no_qry.getString(2)
		}

		if ((senior_acct_no != "null" && res_level_cd == "3") || (senior_acct_no != "null" && res_level_cd == "4"))
		{
			logi.logInfo("this is child account")
			acct_no = senior_acct_no
		}

		String usage_detail_query="select acct_no,usage_type,usage_units ,billable_units,amt,rate,exclude_ind,exclude_comments,TO_CHAR(usage_date,'yyyy-MM-dd') as usage_date,telco_from,telco_to,qualifier_1,qualifier_2,qualifier_3,qualifier_4 from (SELECT acct_no,usage_type,usage_units,billable_units,exclude_ind,exclude_comments,usage_date,telco_from,telco_to,qualifier_1,qualifier_2,qualifier_3,qualifier_4,amt,rate,rec_no,row_number() over(order by rec_no ASC) as s_no FROM ariacore.usage WHERE ctrl_id = (Select max(ctrl_id) from  ariacore.usage_ctrl where client_no="+clientNo+") )WHERE acct_no="+acct_no+" and s_no="+seq_no
		ResultSet rs = db.executePlaQuery(usage_detail_query);
		HashMap bulk_rec_hm = new HashMap()
		
		while(rs.next())
		{
			bulk_rec_hm.put("acct_no",rs.getString("acct_no"))
			bulk_rec_hm.put("usage_type",rs.getString("usage_type"))
			bulk_rec_hm.put("usage_units",rs.getString("usage_units"))
			bulk_rec_hm.put("billable_units",rs.getString("billable_units"))
			bulk_rec_hm.put("amt",rs.getString("amt"))
			bulk_rec_hm.put("rate",rs.getString("rate"))
			bulk_rec_hm.put("exclude_ind",rs.getString("exclude_ind"))
			bulk_rec_hm.put("exclude_comments",rs.getString("exclude_comments"))
			bulk_rec_hm.put("usage_date",rs.getString("usage_date"))
			bulk_rec_hm.put("telco_from",rs.getString("telco_from"))
			bulk_rec_hm.put("telco_to",rs.getString("telco_to"))
			bulk_rec_hm.put("qualifier_1",rs.getString("qualifier_1"))
			bulk_rec_hm.put("qualifier_2",rs.getString("qualifier_2"))
			bulk_rec_hm.put("qualifier_3",rs.getString("qualifier_3"))
			bulk_rec_hm.put("qualifier_4",rs.getString("qualifier_4"))


		}
		
		rs.close()
		logi.logInfo("md_BULK_RECORD_USAGE_DETAILS_DB Hash:"+bulk_rec_hm.toString())
		return bulk_rec_hm.sort()
	}


	/**
	 * Getting the recorded usage details from bulk_record_usage api
	 * @param tcid
	 * @param line_no
	 * @return HashMap
	 */
	HashMap md_BULK_RECORD_USAGE_DETAILS_EXP(String tcid,String line_no)
	{
		logi.logInfo("calling md_BULK_RECORD_USAGE_DETAILS_EXP")
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=tcid.split("-")[2]
		HashMap bulk_rec_exp = new HashMap()
		String rate_per_unit
		String calc_amt
		int rate_rnd
		int amt_rnd
		String bulk_act_amt
		double amt_sum = 0.0
		String senior_acct_no
		String res_level_cd
		def client_no = getValueFromRequest(apiname,"//client_no")

		String acct_no = getValueFromRequest(apiname,"//usage_records_row["+line_no+"]/acct_no").toString()
		ConnectDB db = new ConnectDB()
		String acct_no_qry = "Select senior_acct_no,resp_level_cd from ariacore.acct where acct_no="+acct_no
		ResultSet rs_acct_no_qry = db.executePlaQuery(acct_no_qry);
		while(rs_acct_no_qry.next())
		{
			senior_acct_no = rs_acct_no_qry.getString(1)
			res_level_cd = rs_acct_no_qry.getString(2)
		}

		if ((senior_acct_no != "null" && res_level_cd == "3") || (senior_acct_no != "null" && res_level_cd == "4"))
		{
			logi.logInfo("this is child account")
			acct_no = senior_acct_no
		}
		bulk_rec_exp.put("acct_no",acct_no)

		String rec_no_query="select rec_no from (SELECT rec_no,row_number() over(order by ctrl_id desc) as s_no ,acct_no FROM ariacore.usage WHERE acct_no="+acct_no+" and ctrl_id IS NOT NULL order by ctrl_id desc, rec_no asc ) WHERE acct_no="+acct_no+" and s_no="+line_no

		String rec_no = db.executeQueryP2(rec_no_query)

		def usage_type= getValueFromRequest(apiname,"//usage_records_row["+line_no+"]/usage_type")
		bulk_rec_exp.put("usage_type", usage_type)

		def usage_units = getValueFromRequest(apiname,"//usage_records_row["+line_no+"]/usage_units")
		bulk_rec_exp.put("usage_units", usage_units)

		String billable_units = getValueFromRequest(apiname,"//usage_records_row["+line_no+"]/billable_units")
		if(billable_units.equalsIgnoreCase("NoVal"))
		{
			bulk_rec_exp.put("billable_units", usage_units)
		}

		else
		{
			bulk_rec_exp.put("billable_units", billable_units)
		}

		String exclude_ind = getValueFromRequest(apiname,"//usage_records_row["+line_no+"]/exclude_from_billing")

		if(exclude_ind.equalsIgnoreCase("NoVal") || exclude_ind.equalsIgnoreCase("false") )
		{
			bulk_rec_exp.put("exclude_ind", "0")
		}
		else if(exclude_ind.equalsIgnoreCase("true"))
		{
			bulk_rec_exp.put("exclude_ind", "1")
		}

		String exclude_comments = getValueFromRequest(apiname,"//usage_records_row["+line_no+"]/exclusion_comments")

		if(exclude_comments.equalsIgnoreCase("NoVal") || bulk_rec_exp.get("exclude_ind") == "0")
		{
			bulk_rec_exp.put("exclude_comments", "null")
		}
		else
		{
			bulk_rec_exp.put("exclude_comments", exclude_comments)
		}

		String usage_date = getValueFromRequest(apiname,"//usage_records_row["+line_no+"]/usage_date")

		if(usage_date.equalsIgnoreCase("NoVal"))
		{
			String current_time=db.executeQueryP2("SELECT  TO_CHAR (ARIACORE.ARIAVIRTUALTIME("+client_no+"), 'YYYY-MM-DD')  FROM DUAL")
			bulk_rec_exp.put("usage_date", current_time)
		}
		else
		{
			bulk_rec_exp.put("usage_date", usage_date)
		}
		String formated_date = bulk_rec_exp.get("usage_date").toString()
		if(formated_date.contains("/"))
		{
			logi.logInfo("usgae date contains /")
			Date tradeDate = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH).parse(formated_date)
			String krwtrDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(tradeDate)
			bulk_rec_exp.put("usage_date", krwtrDate)
		}
		else if(!formated_date.contains("-"))
		{
			logi.logInfo("usgae date with no separator")
			Date tradeDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(formated_date)
			logi.logInfo("usgae date without separator:"+formated_date)
			String krwtrDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(tradeDate)
			logi.logInfo("with separator now:"+krwtrDate)
			bulk_rec_exp.put("usage_date", krwtrDate)
		}
		String telco_frm = getValueFromRequest(apiname,"//usage_records_row["+line_no+"]/telco_from")
		if(telco_frm.equalsIgnoreCase("NoVal"))
		{
			bulk_rec_exp.put("telco_from", "null")
		}
		else
		{
			bulk_rec_exp.put("telco_from", telco_frm)
		}

		String telco_to = getValueFromRequest(apiname,"//usage_records_row["+line_no+"]/telco_to")

		if(telco_to.equalsIgnoreCase("NoVal"))
		{
			bulk_rec_exp.put("telco_to", "null")
		}
		else
		{
			bulk_rec_exp.put("telco_to", telco_to)
		}

		for(int q=1;q<=4;q++)
		{
			String qualifier = getValueFromRequest(apiname,"//usage_records_row["+line_no+"]/qualifier_"+q)
			if(qualifier.equalsIgnoreCase("NoVal"))
			{
				bulk_rec_exp.put("qualifier_"+q, "null")
			}
			else
			{
				bulk_rec_exp.put("qualifier_"+q, qualifier)
			}
		}

		String rate = getValueFromRequest(apiname,"//usage_records_row["+line_no+"]/rate")
		String amount = getValueFromRequest(apiname,"//usage_records_row["+line_no+"]/amt")
		
		logi.logInfo("rate:"+rate)
		logi.logInfo("amount:"+amount)


		String rate_scle_qry = "Select param_val from ariacore.all_client_params where param_name ='USAGE_RATE_PER_UNIT_SCALE' and  client_no="+client_no
		ResultSet rs_rate_scle_qry = db.executePlaQuery(rate_scle_qry)
		while(rs_rate_scle_qry.next())
		{
			rate_rnd=rs_rate_scle_qry.getInt(1)
		}

		String amt_write_qry = "Select param_val from ariacore.all_client_params where param_name ='USAGE_RECORD_WRITE_SCALE' and  client_no="+client_no
		ResultSet rs_amt_write_qry = db.executePlaQuery(amt_write_qry)
		while(rs_amt_write_qry.next())
		{
			amt_rnd=rs_amt_write_qry.getInt(1)
		}

		HashMap<String, ArrayList<String>> usage_rateamt_hm = new HashMap<String, ArrayList<String>>()
		boolean auto_rate_flag = get_Auto_Rate_Unrated_Usage_Flag()
		logi.logInfo("Auto rate flag is in Bulk record exp :: "+auto_rate_flag)

		if(!auto_rate_flag)
		{
			logi.logInfo("Auto rate flag is false")

			List plan_no=getAllUsedPlanNoP2(acct_no,bulk_rec_exp.get("usage_type").toString())
			logi.logInfo "plan_no's in md_BULK_RECORD_USAGE_DETAILS_EXP "+plan_no.toString()
			if(!plan_no.isEmpty())
			{
				logi.logInfo("first iffffffffffffffffffffffffffff")
				for(int pln=0; pln<plan_no.size(); pln++)
				{
					logi.logInfo("Now the plan is "+plan_no.get(pln))
					if((amount.equalsIgnoreCase("NoVal")== false) && (rate.equalsIgnoreCase("NoVal")== false))
					{
						logi.logInfo("second iffffffffffffffffffffffffffff")
						logi.logInfo("both amt and rate given")
					//editttttt	
						if(rate==0 && amount==0)
						{
							bulk_rec_exp.put("amt",null)
							bulk_rec_exp.put("rate",null)
						}
						else
						{
							bulk_rec_exp.put("rate",d.format(rate.toDouble().round(rate_rnd)).toString())
							bulk_rec_exp.put("amt",d.format(amount.toDouble().round(amt_rnd)).toString())
						}
						
									
						

						List ser_list=md_GET_USAGE_SERVICE_NO(acct_no.toString(), bulk_rec_exp.get("usage_type").toString())
						logi.logInfo("Services no for the usgae type "+ser_list)
						for(service in ser_list)
						{
							amt_sum = amt_sum + bulk_rec_exp.get("amt").toDouble()
						}
						bulk_act_amt = d.format(amt_sum).toString()
						if(bulk_rec_exp.get("exclude_ind").toString() == "1")
						{
							logi.logInfo("third iffffffffffffffffffffffffffff")
							bulk_act_amt = "0.0"
							logi.logInfo("bulk_act_amt:::::"+bulk_act_amt)
						}
						bulk_act_amt = "calc:"+bulk_act_amt
						logi.logInfo("bulk_act_amt:"+bulk_act_amt)
					}
					else if((amount.equalsIgnoreCase("NoVal")== false) || (rate.equalsIgnoreCase("NoVal")== false))
					{
						logi.logInfo("first else iffffffffffffffffffffffffffff")
						if(rate.equalsIgnoreCase("NoVal")== false)
						{
							logi.logInfo("first oneeelse iffffffffffffffffffffffffffff")
							logi.logInfo("rate given")
							bulk_rec_exp.put("rate",d.format(rate.toDouble().round(rate_rnd)).toString())
							String tot_amt= (rate.toDouble() * bulk_rec_exp.get("billable_units").toDouble()).toString()
							bulk_rec_exp.put("amt",d.format(tot_amt.toDouble().round(amt_rnd)).toString())

							List ser_list=md_GET_USAGE_SERVICE_NO(acct_no.toString(), bulk_rec_exp.get("usage_type").toString())
							logi.logInfo("Services no for the usgae type "+ser_list)
							for(service in ser_list)
							{
								amt_sum = amt_sum + bulk_rec_exp.get("amt").toDouble()
							}
							bulk_act_amt = d.format(amt_sum).toString()
							if(bulk_rec_exp.get("exclude_ind").toString() == "1")
							{
								bulk_act_amt = "0.0"
								logi.logInfo("bulk_act_amt:::::"+bulk_act_amt)
							}
							bulk_act_amt = "calc:"+bulk_act_amt
							logi.logInfo("bulk_act_amt::"+bulk_act_amt)
						}

						else if (amount.equalsIgnoreCase("NoVal")== false)
						{
							logi.logInfo("second else iffffffffffffffffffffffffffff")
							logi.logInfo("amount given")
							bulk_rec_exp.put("amt",d.format(amount.toDouble().round(amt_rnd)).toString())
							String rpu= (amount.toDouble()/bulk_rec_exp.get("billable_units").toDouble()).toString()
							bulk_rec_exp.put("rate",d.format(rpu.toDouble().round(rate_rnd)).toString())

							List ser_list=md_GET_USAGE_SERVICE_NO(acct_no.toString(), bulk_rec_exp.get("usage_type").toString())
							logi.logInfo("Services no for the usgae type "+ser_list)
							for(service in ser_list)
							{
								amt_sum = amt_sum + bulk_rec_exp.get("amt").toDouble()
							}

							bulk_act_amt = d.format(amt_sum).toString()
							if(bulk_rec_exp.get("exclude_ind").toString() == "1")
							{
								bulk_act_amt = "0.0"
								logi.logInfo("bulk_act_amt:::::"+bulk_act_amt)
							}
							bulk_act_amt = "calc:"+bulk_act_amt
							logi.logInfo("bulk_act_amt:::"+bulk_act_amt)
						}

					}
					else
					{
						logi.logInfo("first else iffffffffffffffffffffffffffff")
						List ser_list=md_GET_USAGE_SERVICE_NO(acct_no.toString(), bulk_rec_exp.get("usage_type").toString(),plan_no.get(pln).toString())
						logi.logInfo("Services no for the usgae type "+ser_list)
						String amtt = "0.0"
						usage_rateamt_hm=md_USAGE_AMT_RATE_EXP(acct_no.toString(),ser_list,bulk_rec_exp.get("billable_units").toString(),bulk_rec_exp.get("usage_type").toString(),plan_no.get(pln).toString())
						logi.logInfo("usage_rateamt_hm :: " +usage_rateamt_hm.toString())
						if(!ser_list.isEmpty())
						{
							logi.logInfo("inside else iffffffffffffffffffffffffffff")
							logi.logInfo("ser_list is not Empty")
							for(service in ser_list)
							{
								String r=usage_rateamt_hm.get(service)[0].toString()
								String amt=usage_rateamt_hm.get(service)[1].toString()
								logi.logInfo("cal rate:"+r+" and cal amt:"+amt)
								amtt = (amtt.toDouble() + amt.toDouble()).toString()
								bulk_act_amt = d.format(amtt.toDouble().round(amt_rnd)).toString()
								logi.logInfo("bulk_act_amt::::"+bulk_act_amt)
								if(bulk_rec_exp.get("exclude_ind").toString() == "1")
								{
									bulk_act_amt = "0.0"
									logi.logInfo("bulk_act_amt:::::"+bulk_act_amt)
								}

								if(md_get_tier_pricing_type(service,acct_no,bulk_rec_exp.get("usage_type").toString()).toString().equals("1"))
								{
									logi.logInfo(service+"belongs to pricing rule 1")
									if(!auto_rate_flag)
									{
										bulk_rec_exp.put("amt","null")
										bulk_rec_exp.put("rate","null")
									}

									else
									{
										logi.logInfo("d.format(r.toDouble().round(rate_rnd)).toString()"+d.format(r.toDouble().round(rate_rnd)).toString())
										if(d.format(r.toDouble().round(rate_rnd)).toString()==0 && d.format(amt.toDouble().round(amt_rnd)).toString()==0)
										{
										bulk_rec_exp.put("amt","null")
										bulk_rec_exp.put("rate","null")
										}else{
										bulk_rec_exp.put("rate",d.format(r.toDouble().round(rate_rnd)).toString())
										bulk_rec_exp.put("amt",d.format(amt.toDouble().round(amt_rnd)).toString())
										}
									}
								}

								else
								{
									bulk_rec_exp.put("amt","null")
									bulk_rec_exp.put("rate","null")
									break;
								}
							}
						}
						else
						{ logi.logInfo("ser_list is Empty")
							bulk_rec_exp.put("amt","null")
							bulk_rec_exp.put("rate","null")
						}

					}
					logi.logInfo "plan_no in md_BULK_RECORD_USAGE_DETAILS_EXP if loop "+plan_no.get(pln)
					Constant.recordedusageamounts.put(tcid+"-"+acct_no+"-"+rec_no+ "-"+bulk_rec_exp.get("usage_type")+"-"+plan_no.get(pln),bulk_act_amt.toString())

				}
			}
			else
			{
				bulk_rec_exp.put("amt","null")
				bulk_rec_exp.put("rate","null")
			}
		}

		else
		{
			if((amount.equalsIgnoreCase("NoVal")== false) && (rate.equalsIgnoreCase("NoVal")== false))
			{
				logi.logInfo("third else iffffffffffffffffffffffffffff")
				logi.logInfo("both amt and rate given")
				bulk_rec_exp.put("rate",d.format(rate.toDouble().round(rate_rnd)).toString())
				bulk_rec_exp.put("amt",d.format(amount.toDouble().round(amt_rnd)).toString())

				List ser_list=md_GET_USAGE_SERVICE_NO(acct_no.toString(), bulk_rec_exp.get("usage_type").toString())
				logi.logInfo("Services no for the usgae type "+ser_list)
				for(service in ser_list)
				{
					amt_sum = amt_sum + bulk_rec_exp.get("amt").toDouble()
				}
				bulk_act_amt = d.format(amt_sum).toString()
				if(bulk_rec_exp.get("exclude_ind").toString() == "1")
				{
					bulk_act_amt = "0.0"
					logi.logInfo("bulk_act_amt:::::"+bulk_act_amt)
				}
				bulk_act_amt = "calc:"+bulk_act_amt
				logi.logInfo("bulk_act_amt:"+bulk_act_amt)
			}
			else if((amount.equalsIgnoreCase("NoVal")== false) || (rate.equalsIgnoreCase("NoVal")== false))
			{
				logi.logInfo("fourth elseif iffffffffffffffffffffffffffff")
				if(rate.equalsIgnoreCase("NoVal")== false)
				{
					logi.logInfo("rate given")
					bulk_rec_exp.put("rate",d.format(rate.toDouble().round(rate_rnd)).toString())
					String tot_amt= (rate.toDouble() * bulk_rec_exp.get("billable_units").toDouble()).toString()
					bulk_rec_exp.put("amt",d.format(tot_amt.toDouble().round(amt_rnd)).toString())

					List ser_list=md_GET_USAGE_SERVICE_NO(acct_no.toString(), bulk_rec_exp.get("usage_type").toString())
					logi.logInfo("Services no for the usgae type "+ser_list)
					for(service in ser_list)
					{
						amt_sum = amt_sum + bulk_rec_exp.get("amt").toDouble()
					}
					bulk_act_amt = d.format(amt_sum).toString()
					if(bulk_rec_exp.get("exclude_ind").toString() == "1")
					{
						bulk_act_amt = "0.0"
						logi.logInfo("bulk_act_amt:::::"+bulk_act_amt)
					}
					bulk_act_amt = "calc:"+bulk_act_amt
					logi.logInfo("bulk_act_amt::"+bulk_act_amt)
				}

				else if (amount.equalsIgnoreCase("NoVal")== false)
				{
					logi.logInfo("fifth else iffffffffffffffffffffffffffff")
					logi.logInfo("amount given")
					bulk_rec_exp.put("amt",d.format(amount.toDouble().round(amt_rnd)).toString())
					String rpu= (amount.toDouble()/bulk_rec_exp.get("billable_units").toDouble()).toString()
					bulk_rec_exp.put("rate",d.format(rpu.toDouble().round(rate_rnd)).toString())

					List ser_list=md_GET_USAGE_SERVICE_NO(acct_no.toString(), bulk_rec_exp.get("usage_type").toString())
					logi.logInfo("Services no for the usgae type "+ser_list)
					for(service in ser_list)
					{
						amt_sum = amt_sum + bulk_rec_exp.get("amt").toDouble()
					}

					bulk_act_amt = d.format(amt_sum).toString()
					if(bulk_rec_exp.get("exclude_ind").toString() == "1")
					{
						bulk_act_amt = "0.0"
						logi.logInfo("bulk_act_amt:::::"+bulk_act_amt)
					}
					bulk_act_amt = "calc:"+bulk_act_amt
					logi.logInfo("bulk_act_amt:::"+bulk_act_amt)
				}

			}
			else
			{
				logi.logInfo("fourth else iffffffffffffffffffffffffffff")
				List ser_list=md_GET_USAGE_SERVICE_NO(acct_no.toString(), bulk_rec_exp.get("usage_type").toString())
				logi.logInfo("Services no for the usgae type "+ser_list)
				String amtt = "0.0"
				usage_rateamt_hm=md_USAGE_AMT_RATE_EXP(acct_no.toString(),ser_list,bulk_rec_exp.get("billable_units").toString(),bulk_rec_exp.get("usage_type").toString())
				logi.logInfo("usage_rateamt_hm :: " +usage_rateamt_hm.toString())
				if(!ser_list.isEmpty())
				{
					logi.logInfo("ser_list is not Empty")
					for(service in ser_list)
					{
						String r=usage_rateamt_hm.get(service)[0].toString()
						String amt=usage_rateamt_hm.get(service)[1].toString()
						logi.logInfo("cal rate:"+r+" and cal amt:"+amt)
						amtt = (amtt.toDouble() + amt.toDouble()).toString()
						bulk_act_amt = d.format(amtt.toDouble().round(amt_rnd)).toString()
						logi.logInfo("bulk_act_amt::::"+bulk_act_amt)
						if(bulk_rec_exp.get("exclude_ind").toString() == "1")
						{
							bulk_act_amt = "0.0"
							logi.logInfo("bulk_act_amt:::::"+bulk_act_amt)
						}

						if(md_get_tier_pricing_type(service,acct_no,bulk_rec_exp.get("usage_type").toString()).toString().equals("1"))
						{
							logi.logInfo(service+"belongs to pricing rule 1")
							logi.logInfo("d.format(r.toDouble().round(rate_rnd)).toString()siva"+d.format(r.toDouble().round(rate_rnd)).toString())
							if(!auto_rate_flag)
							{
								bulk_rec_exp.put("amt","null")
								bulk_rec_exp.put("rate","null")
							}

							else
							{
								if(d.format(r.toDouble().round(rate_rnd)).toString()=="0" && d.format(amt.toDouble().round(amt_rnd)).toString()=="0")
										{
										bulk_rec_exp.put("amt","null")
										bulk_rec_exp.put("rate","null")
										}else{
										bulk_rec_exp.put("rate",d.format(r.toDouble().round(rate_rnd)).toString())
										bulk_rec_exp.put("amt",d.format(amt.toDouble().round(amt_rnd)).toString())
										}
							}
						}

						else
						{
							bulk_rec_exp.put("amt","null")
							bulk_rec_exp.put("rate","null")
							break;
						}
					}
				}
				else
				{ logi.logInfo("ser_list is Empty")
					bulk_rec_exp.put("amt","null")
					bulk_rec_exp.put("rate","null")
				}

			}

			String plan_no=getUsedPlanNoP2(acct_no,bulk_rec_exp.get("usage_type").toString())
			logi.logInfo "plan_no in md_BULK_RECORD_USAGE_DETAILS_EXP else loop "+plan_no
			Constant.recordedusageamounts.put(tcid+"-"+acct_no+"-"+rec_no+ "-"+bulk_rec_exp.get("usage_type")+"-"+plan_no,bulk_act_amt.toString())
		}
		logi.logInfo("All bulk recorded usages hash" +Constant.recordedusageamounts.toString() )

		logi.logInfo("md_BULK_RECORD_USAGE_DETAILS_EXP Hash:"+bulk_rec_exp.toString())
		return bulk_rec_exp.sort()

	}
	/**
	 * Getting the recorded usage details based on USAGE_RECORD_WRITE_SCALE/USAGE_RECORD_READ_SCALE parameters
	 * @param tcid
	 * @return LinkedHashMap
	 */
	LinkedHashMap md_RECORD_USAGE_PRECISION_EXP(String tcid)
	{
		logi.logInfo("md_RECORD_USAGE_PRECISION_EXP")
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=tcid.split("-")[2]
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		String actual_amt
		double amt_sum = 0.0
		int rate_rnd
		int amt_rnd

		def acct_no = getValueFromRequest(apiname,"//acct_no")
		HashMap input_hash = new HashMap()
		LinkedHashMap expected_hash = new LinkedHashMap()
		String xpath="//*:record_usage//*"
		List testCaseCmb = Constant.lhmAllInputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllInputResponseAsXML_Global.values().asList()

		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().equals(tcid)) {

				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
					def responseXml = new XmlSlurper().parseText(holder.getXml())
					def rootNode = responseXml.Body.record_usage
					rootNode.children().each() {
						input_hash.put(it.name(),rootNode[it.name()])
					}
				}
			}
		}
		logi.logInfo( "Input hash from record_usage api"+input_hash)

		expected_hash.put("acct_no",input_hash.get("acct_no"))
		expected_hash.put("usage_type",input_hash.get("usage_type"))
		expected_hash.put("usage_units",input_hash.get("usage_units"))

		if(input_hash.get("billable_units").toString().isEmpty()==true) {
			expected_hash.put("billable_units",d.format(input_hash.get("usage_units").toDouble()).toString())
		}
		else {
			expected_hash.put("billable_units",d.format(input_hash.get("billable_units").toDouble()).toString())
		}
		//Setting Exclude from billing
		if((input_hash.get("exclude_from_billing").toString().isEmpty()==true) ||(input_hash.get("exclude_from_billing").toString().toUpperCase()=="FALSE" ))
			expected_hash.put("exclude_ind","0")
		else if(input_hash.get("exclude_from_billing").toString().toUpperCase()=="TRUE" )
			expected_hash.put("exclude_ind","1")

		ConnectDB db = new ConnectDB()

		String current_time=db.executeQueryP2("SELECT  TO_CHAR (ARIACORE.ARIAVIRTUALTIME("+client_no+"), 'YYYY-MM-DD')  FROM DUAL")

		if(input_hash.get("usage_date").toString().isEmpty()==true )
		{
			expected_hash.put("usage_date",current_time)
		}
		else
		{
			expected_hash.put("usage_date",input_hash.get("usage_date"))
		}


		if(input_hash.get("exclusion_comments").toString().isEmpty()==true || expected_hash.get("exclude_ind")=="0")
		{
			expected_hash.put("exclusion_comments","null")
		}
		else
		{
			expected_hash.put("exclusion_comments",input_hash.get("exclusion_comments"))
		}

		if(input_hash.get("telco_from").toString().isEmpty()==true )
		{
			expected_hash.put("telco_from","null")
		}
		else
		{
			expected_hash.put("telco_from",input_hash.get("telco_from"))
		}

		if(input_hash.get("telco_to").toString().isEmpty()==true )
		{
			expected_hash.put("telco_to","null")
		}
		else
		{
			expected_hash.put("telco_to",input_hash.get("telco_to"))
		}

		if(input_hash.get("qualifier_1").toString().isEmpty()==true )
		{
			expected_hash.put("qualifier_1","null")
		}
		else
		{
			expected_hash.put("qualifier_1",input_hash.get("qualifier_1"))
		}
		if(input_hash.get("qualifier_2").toString().isEmpty()==true )
		{
			expected_hash.put("qualifier_2","null")
		}
		else
		{
			expected_hash.put("qualifier_2",input_hash.get("qualifier_2"))
		}
		if(input_hash.get("qualifier_3").toString().isEmpty()==true )
		{
			expected_hash.put("qualifier_3","null")
		}
		else
		{
			expected_hash.put("qualifier_3",input_hash.get("qualifier_3"))
		}
		if(input_hash.get("qualifier_4").toString().isEmpty()==true )
		{
			expected_hash.put("qualifier_4","null")
		}
		else
		{
			expected_hash.put("qualifier_4",input_hash.get("qualifier_4"))
		}
		if (input_hash.get("parent_rec_no").toString().isEmpty()==true)
		{
			expected_hash.put("parent_rec_no","null")
		}
		else
		{
			expected_hash.put("parent_rec_no",input_hash.get("parent_rec_no"))
		}

		logi.logInfo("expected_hash after enter till qualifier "+expected_hash.toString())


		String rate_scle_qry = "Select param_val from ariacore.all_client_params where param_name ='USAGE_RATE_PER_UNIT_SCALE' and  client_no="+client_no
		ResultSet rs_rate_scle_qry = db.executePlaQuery(rate_scle_qry)
		while(rs_rate_scle_qry.next())
		{
			rate_rnd=rs_rate_scle_qry.getInt(1)
			logi.logInfo("rate should be rounded off to:"+rate_rnd)
		}

		String amt_write_qry = "Select param_val from ariacore.all_client_params where param_name ='USAGE_RECORD_WRITE_SCALE' and  client_no="+client_no
		ResultSet rs_amt_write_qry = db.executePlaQuery(amt_write_qry)
		while(rs_amt_write_qry.next())
		{
			amt_rnd=rs_amt_write_qry.getInt(1)
			logi.logInfo("amt should be rounded off to:"+amt_rnd)
		}



		HashMap<String, ArrayList<String>> usage_rateamt_hm = new HashMap<String, ArrayList<String>>()

		if(input_hash.get("rate").toString().isEmpty()!=true && input_hash.get("amt").toString().isEmpty()!=true)//rate and amount given
		{
			logi.logInfo("rate,amt has been provided")
			expected_hash.put("rate",d.format(input_hash.get("rate").toString().toDouble().round(rate_rnd)).toString())
			expected_hash.put("amt",d.format(input_hash.get("amt").toString().toDouble().round(amt_rnd)).toString())
			List service_nos=md_GET_USAGE_SERVICE_NO(acct_no.toString(), input_hash.get("usage_type").toString())
			logi.logInfo("service_no in md_RECORD_USAGE_PRECISION_EXP "+service_nos)

			for(service in service_nos)
			{
				amt_sum = amt_sum + expected_hash.get("amt").toString().toDouble()
			}

			actual_amt = d.format(amt_sum.round(amt_rnd)).toString()
			actual_amt = "calc:"+actual_amt

		}

		else if(input_hash.get("rate").toString().isEmpty()!=true || input_hash.get("amt").toString().isEmpty()!=true ) //rate or amount given
		{
			String rate_per_unit
			if(input_hash.get("rate").toString().isEmpty()!=true)
			{
				logi.logInfo("rate provided")
				expected_hash.put("rate",d.format(input_hash.get("rate").toString().toDouble().round(rate_rnd)).toString())
				logi.logInfo("rate given:"+expected_hash.get("rate").toString())
				String calc_amt=(expected_hash.get("rate").toString().toDouble()*expected_hash.get("billable_units").toString().toDouble()).toString()
				logi.logInfo("calc amount"+calc_amt)
				expected_hash.put("amt",d.format(calc_amt.toDouble().round(amt_rnd)).toString())
				List service_nos=md_GET_USAGE_SERVICE_NO(acct_no.toString(), input_hash.get("usage_type").toString())
				logi.logInfo("service_no in md_RECORD_USAGE_PRECISION_EXP "+service_nos)

				for(service in service_nos)
				{
					amt_sum = amt_sum + expected_hash.get("amt").toString().toDouble()
				}

				actual_amt = d.format(amt_sum.round(amt_rnd)).toString()
				actual_amt = "calc:"+actual_amt

			}
			else if(input_hash.get("amt").toString().isEmpty()!=true)
			{
				logi.logInfo("amount provided")
				expected_hash.put("amt",d.format(input_hash.get("amt").toString().toDouble().round(amt_rnd)).toString())

				rate_per_unit=( expected_hash.get("amt").toString().toDouble()/expected_hash.get("billable_units").toString().toDouble()).toString()
				expected_hash.put("rate",d.format(rate_per_unit.toDouble().round(rate_rnd)).toString())
				List service_nos=md_GET_USAGE_SERVICE_NO(acct_no.toString(), input_hash.get("usage_type").toString())
				logi.logInfo("service_no in md_RECORD_USAGE_PRECISION_EXP "+service_nos)

				for(service in service_nos)
				{
					amt_sum = amt_sum + expected_hash.get("amt").toString().toDouble()
				}

				actual_amt = d.format(amt_sum.round(amt_rnd)).toString()
				actual_amt = "calc:"+actual_amt
			}



		}

		else //neither amt or rate given
		{
			logi.logInfo("neither amt or rate given")
			List service_nos=md_GET_USAGE_SERVICE_NO(acct_no.toString(), input_hash.get("usage_type").toString())
			logi.logInfo("service_no in md_RECORD_USAGE_DETAILS_EXP "+service_nos)
			String amount = "0.0"
			usage_rateamt_hm=md_USAGE_AMT_RATE_EXP(acct_no.toString(),service_nos,expected_hash.get("billable_units").toString(),expected_hash.get("usage_type").toString())
			logi.logInfo("usage_rateamt_hm :: " +usage_rateamt_hm.toString())
			for(service in service_nos)
			{
				String r=usage_rateamt_hm.get(service)[0].toString()
				String amt=usage_rateamt_hm.get(service)[1].toString()

				logi.logInfo("Calculated amount"+amt)
				logi.logInfo("Calculated rate_per_unit"+r)
				amount = (amount.toDouble() + amt.toDouble()).toString()
				actual_amt = d.format(amount.toDouble().round(amt_rnd)).toString()
				logi.logInfo("actual amt:"+actual_amt)
				if(md_get_tier_pricing_type(service,acct_no,expected_hash.get("usage_type").toString()).toString().equals("1"))
				{
					logi.logInfo("price tier 1")
					expected_hash.put("rate",d.format(r.toDouble().round(rate_rnd)).toString())
					expected_hash.put("amt",d.format(amt.toDouble().round(amt_rnd)).toString())


				}
				else
				{
					expected_hash.put("amt","null")
					expected_hash.put("rate","null")
					break;
				}


			}
		}

		if(expected_hash.get("exclude_ind")=="1")
		{
			actual_amt="0.0"
		}

		//Adding usage amount in hash for further verification
		logi.logInfo "actual_amt "+actual_amt
		String plan_no=getUsedPlanNoP2(acct_no,expected_hash.get("usage_type").toString())
		String rec_no= db.executeQueryP2("select rec_no from ariacore.usage where acct_no="+acct_no+" and usage_type="+input_hash.get("usage_type"))
		Constant.recordedusageamounts.put(tcid+"-"+acct_no+"-"+rec_no+"-"+ expected_hash.get("usage_type")+"-"+plan_no,actual_amt.toString())
		logi.logInfo("All recorded usages hash in precision" +Constant.recordedusageamounts.toString() )
		return expected_hash.sort()

	}
	/**
	 * Getting the Master plan rate schedule number
	 * @param tcid
	 * @return String
	 */
	String md_PLAN_RATE_SCHEDULE_master(String tcid)
	{
		String apiname=tcid.split("-")[2]
		String plan_no=getValueFromRequest(apiname,"//master_plan_no")
		String curr_cd=getValueFromRequest(apiname,"//currency_cd")
		String client = getValueFromRequest(apiname,"//client_no")
		return md_PLAN_RATE_SCHEDULE(plan_no,curr_cd,client)
	}
	/**
	 * Getting the Supplemental plan rate schedule number
	 * @param tcid
	 * @return String
	 */
	String md_PLAN_RATE_SCHEDULE_sp1(String tcid)
	{
		String apiname=tcid.split("-")[2]
		String supp_plan_no=getValueFromRequest(apiname,"//supp_plans_row[1]/supp_plans")
		String curr_cd=getValueFromRequest(apiname,"//currency_cd")
		String client = getValueFromRequest(apiname,"//client_no")
		return md_PLAN_RATE_SCHEDULE(supp_plan_no,curr_cd,client)
	}
	/**
	 *  Getting the plan rate schedule number
	 * @param plan_num
	 * @param currency_cd
	 * @param clientNo
	 * @return String
	 */
	String md_PLAN_RATE_SCHEDULE(String plan_num,String currency_cd,String clientNo)
	{
		ConnectDB db = new ConnectDB()
		String sch_qry="Select schedule_no from ariacore.client_plan_dflt_rate_scheds where plan_no="+plan_num+" and currency_cd='"+currency_cd+"' and client_no="+clientNo
		String sch_num
		ResultSet rs_sch_qry=db.executePlaQuery(sch_qry)

		while(rs_sch_qry.next())
		{
			sch_num=rs_sch_qry.getString(1)
		}

		return sch_num
	}
	/**
	 * Getting Master plan no from create_acct_complete api
	 * @param tcid
	 * @return String
	 */
	String md_ACCT_RATE_SCHEDULE_EXP_master(String tcid)
	{
		String apiname=tcid.split("-")[2]
		String acct_no = getValueFromResponse(apiname,ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String plan_no = getValueFromRequest(apiname,"//master_plan_no")

		return md_ACCT_RATE_SCHEDULE_EXP(acct_no,plan_no)
	}
	/**
	 * Getting Supplemental plan no from create_acct_complete api
	 * @param tcid
	 * @return String
	 */
	String md_ACCT_RATE_SCHEDULE_EXP_sp1(String tcid)
	{
		String apiname=tcid.split("-")[2]
		String acct_no = getValueFromResponse(apiname,ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String supp_plan_no = getValueFromRequest(apiname,"//supp_plans_row[1]/supp_plans")

		return md_ACCT_RATE_SCHEDULE_EXP(acct_no,supp_plan_no)
	}
	/**
	 * Getting rate schedule number
	 * @param acct_no
	 * @param plan_num
	 * @return String
	 */
	String md_ACCT_RATE_SCHEDULE_EXP(String acct_no,String plan_num)
	{
		logi.logInfo("md_ACCT_RATE_SCHEDULE_EXP")
		String rate_sch
		ConnectDB db = new ConnectDB()
		String rate_sch_qry = "Select DISTINCT rate_schedule_no from ariacore.acct_rates where acct_no="+acct_no+" and plan_no="+plan_num
		ResultSet rs_rate_sch_qry = db.executePlaQuery(rate_sch_qry)

		while(rs_rate_sch_qry.next())
		{
			rate_sch = rs_rate_sch_qry.getString(1)
		}

		return rate_sch
	}

	public def md_GET_USAGE_HISTORY_API1(String tcid)
	{
		return md_GET_USAGE_HISTORY_API(tcid,"1")
	}
	public def md_GET_USAGE_HISTORY_API2(String tcid)
	{
		return md_GET_USAGE_HISTORY_API(tcid,"2")
	}
	public def md_GET_USAGE_HISTORY_API3(String tcid)
	{
		return md_GET_USAGE_HISTORY_API(tcid,"3")
	}
	/**
	 * Getting recorded usage details from GET_USAGE_HISTORY_API response
	 * @param acct_no
	 * @param plan_num
	 * @return HashMap
	 */
	HashMap md_GET_USAGE_HISTORY_API(String tcid,String line_no)
	{
		logi.logInfo("md_GET_USAGE_HISTORY_API")
		int tmp=1
		LinkedHashMap histy_api=new LinkedHashMap()
		String xpath="//*:get_usage_historyResponse"
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()

		for(int j=0; j<testCaseCmb.size(); j++) {

			if(testCaseCmb[j].toString().equals(tcid))
			{

				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
					def responseXml = new XmlSlurper().parseText(holder.getXml())
					def rootNode = responseXml.Body.get_usage_historyResponse.usage_history_records
					rootNode.children().each()
					{
						def nme=it.name()+"["+tmp+"]"
						logi.logInfo("nme:"+nme.toString())

						if(nme.equals("usage_history_records_row["+line_no+"]"))
						{
							it.children().each()
							{
								logi.logInfo(it.name())
								logi.logInfo(it.text())
								histy_api.put(it.name().toUpperCase(), it.text())
							}
						}

						tmp++

					}
				}
			}

		}

		histy_api.remove("USAGE_TIME")
		if(histy_api.get("IS_EXCLUDED").toString().equals("false"))
			histy_api.put("EXCLUDE_REASON_CD","null")

		if(histy_api.containsKey("CLIENT_RECORD_ID"))
			histy_api.remove("CLIENT_RECORD_ID")
		if(histy_api.containsKey("INVOICE_TRANSACTION_ID"))
			histy_api.remove("INVOICE_TRANSACTION_ID")

		if(histy_api.containsKey("EXCLUSION_COMMENTS"))
			histy_api.remove("EXCLUSION_COMMENTS")

		if(histy_api.containsKey("QUALIFIER_1"))
			histy_api.remove("QUALIFIER_1")

		if(histy_api.containsKey("QUALIFIER_2"))
			histy_api.remove("QUALIFIER_2")

		if(histy_api.containsKey("QUALIFIER_3"))
			histy_api.remove("QUALIFIER_3")

		if(histy_api.containsKey("QUALIFIER_4"))
			histy_api.remove("QUALIFIER_4")

		return histy_api.sort()
	}

	public def md_GET_USAGE_HISTORY_DB1(String tcid)
	{
		return md_GET_USAGE_HISTORY_DB(tcid,"1")
	}
	public def md_GET_USAGE_HISTORY_DB2(String tcid)
	{
		return md_GET_USAGE_HISTORY_DB(tcid,"2")
	}
	public def md_GET_USAGE_HISTORY_DB3(String tcid)
	{
		return md_GET_USAGE_HISTORY_DB(tcid,"3")
	}
	/**
	 * Getting recorded usage details from usage table
	 * @param acct_no
	 * @param seq_no
	 * @return HashMap
	 */
	HashMap md_GET_USAGE_HISTORY_DB(String tcid,String seq_no)
	{
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=tcid.split("-")[2]
		LinkedHashMap<String,String> histy_db=new LinkedHashMap()
		logi.logInfo("apiname"+apiname)
		def acct_no = getValueFromRequest(apiname,"//acct_no")
		def start_date = getValueFromRequest(apiname,"//date_range_start")
		def end_date = getValueFromRequest(apiname,"//date_range_end")
		def usage_type= getValueFromRequest(apiname,"//specified_usage_type_no")
		def usage_type_code1= getValueFromRequest(apiname,"//specified_usage_type_code")
		String unit_type
		def client_no = getValueFromRequest(apiname,"//client_no")

		if(usage_type!='NoVal' && end_date=='NoVal')
		{
			logi.logInfo "Inside if - usage type and end_date"
			String usg_qry = "SELECT acct_no as billable_acct_no,usage_type AS usage_type_no,TO_CHAR(usage_date,'yyyy-MM-dd') AS usage_date,usage_units AS units,billable_units AS recorded_units,exclude_ind  AS is_excluded,exclude_reason_cd AS exclude_reason_cd,NVL(rate,0) AS pre_rated_rate,amt AS specific_record_charge_amount,rec_no AS usage_rec_no FROM(Select acct_no,usage_type, usage_date,usage_units,billable_units,exclude_ind,exclude_reason_cd,rate,amt,rec_no,row_number() over(order by usage_date asc) as s_no FROM ariacore.usage WHERE acct_no="+acct_no+" AND TO_CHAR(usage_date,'yyyy-MM-dd') >= '"+start_date+"' AND usage_type="+usage_type+" AND CLIENT_NO="+client_no+") where s_no="+seq_no
			ResultSet resultSet = db.executePlaQuery(usg_qry);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();

			while (resultSet.next()){
				for(int i=1; i<=columns; i++){
					histy_db.put(md.getColumnName(i),resultSet.getObject(i));
				}

			}

		}

		else if(usage_type!='NoVal' && end_date!='NoVal' && start_date!='NoVal')
		{
			logi.logInfo "Inside elseif - usage type,start_date end_date"
			String usg_qry = "SELECT acct_no as billable_acct_no,usage_type AS usage_type_no,TO_CHAR(usage_date,'yyyy-MM-dd') AS usage_date,usage_units AS units,billable_units AS recorded_units,exclude_ind  AS is_excluded,exclude_reason_cd AS exclude_reason_cd,NVL(rate,0) AS pre_rated_rate,amt AS specific_record_charge_amount,rec_no AS usage_rec_no FROM(Select acct_no,usage_type, usage_date,usage_units,billable_units,exclude_ind,exclude_reason_cd,rate,amt,rec_no,row_number() over(order by rec_no desc,usage_date asc) as s_no FROM ariacore.usage WHERE acct_no="+acct_no+" AND TO_CHAR(usage_date,'yyyy-MM-dd') BETWEEN '"+start_date+"' AND '"+end_date+"' AND usage_type="+usage_type+" AND CLIENT_NO="+client_no+") where s_no="+seq_no
			ResultSet resultSet = db.executePlaQuery(usg_qry);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();

			while (resultSet.next()){
				for(int i=1; i<=columns; i++){
					histy_db.put(md.getColumnName(i),resultSet.getObject(i));
				}

			}
		}

		else if(usage_type_code1!='NoVal'&& start_date!='NoVal' && end_date!='NoVal')
		{
			logi.logInfo "Inside if - usage typecode,start_date and end_date"
			String usg_qry = "SELECT a.acct_no as billable_acct_no,a.usage_type AS usage_type_no,TO_CHAR(a.usage_date,'yyyy-MM-dd') AS usage_date,a.usage_units AS units,a.billable_units AS recorded_units,a.exclude_ind  AS is_excluded,a.exclude_reason_cd AS exclude_reason_cd,NVL(a.rate,0) AS pre_rated_rate,a.amt AS specific_record_charge_amount,a.rec_no AS usage_rec_no FROM(Select USG.acct_no,USG.usage_type, USG.usage_date,USG.usage_units,USG.billable_units,USG.exclude_ind,USG.exclude_reason_cd,USG.rate,USG.amt,USG.rec_no,UST.usage_type_code,row_number() over(order by USG.rec_no desc,USG.usage_date asc) as s_no FROM ariacore.usage USG JOIN ariacore.usage_types UST ON USG.usage_type=UST.usage_type WHERE USG.acct_no="+acct_no+" AND TO_CHAR(USG.usage_date,'yyyy-MM-dd') BETWEEN '"+start_date+"' AND '"+end_date+"' AND USG.CLIENT_NO="+client_no+" AND UST.CUSTOM_TO_CLIENT_NO="+client_no+" AND UST.usage_type_code='"+usage_type_code1+"')a where s_no="+seq_no
			ResultSet resultSet = db.executePlaQuery(usg_qry);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();

			while (resultSet.next()){
				for(int i=1; i<=columns; i++){
					histy_db.put(md.getColumnName(i),resultSet.getObject(i));
				}

			}
		}
		else
		{
			logi.logInfo " Inside else"
			String usg_qry = "SELECT acct_no as billable_acct_no,usage_type AS usage_type_no,TO_CHAR(usage_date,'yyyy-MM-dd') AS usage_date,usage_units AS units,billable_units AS recorded_units,exclude_ind  AS is_excluded,exclude_reason_cd AS exclude_reason_cd,NVL(rate,0) AS pre_rated_rate,amt AS specific_record_charge_amount,rec_no AS usage_rec_no FROM(Select acct_no,usage_type, usage_date,usage_units,billable_units,exclude_ind,exclude_reason_cd,rate,amt,rec_no,row_number() over(order by rec_no desc,usage_date asc) as s_no FROM ariacore.usage WHERE acct_no="+acct_no+" AND CLIENT_NO="+client_no+" AND TO_CHAR(usage_date,'yyyy-MM-dd') BETWEEN '"+start_date+"' AND '"+end_date+"') where s_no="+seq_no
			ResultSet resultSet = db.executePlaQuery(usg_qry);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();

			while (resultSet.next()){
				for(int i=1; i<=columns; i++){
					histy_db.put(md.getColumnName(i),resultSet.getObject(i));
				}

			}
		}

		if(histy_db.get("IS_EXCLUDED").toString().equals("0"))
		{

			histy_db.put("IS_EXCLUDED","false")

		}
		else if(histy_db.get("IS_EXCLUDED").toString().equals("1"))
		{

			histy_db.put("IS_EXCLUDED","true")
		}

		if(histy_db.get("IS_EXCLUDED").toString().equals("true"))
			histy_db.put("PRE_RATED_RATE","0")

		String unit_read_qry = "Select param_val from ariacore.all_client_params where param_name ='USAGE_RECORD_READ_SCALE' and  client_no="+client_no
		ResultSet rs_unit_read = db.executePlaQuery(unit_read_qry)
		while(rs_unit_read.next())
		{

			int un_rnd=rs_unit_read.getInt(1)
			histy_db.put("UNITS",d.format(histy_db.get("UNITS").toString().toDouble().round(un_rnd)).toString())
			histy_db.put("RECORDED_UNITS",d.format(histy_db.get("RECORDED_UNITS").toString().toDouble().round(un_rnd)).toString())
			if(histy_db.get("SPECIFIC_RECORD_CHARGE_AMOUNT").toString() != "null")
			{
				histy_db.put("SPECIFIC_RECORD_CHARGE_AMOUNT",d.format(histy_db.get("SPECIFIC_RECORD_CHARGE_AMOUNT").toString().toDouble().round(un_rnd)).toString())
			}
			else
			{
				histy_db.remove("SPECIFIC_RECORD_CHARGE_AMOUNT")
			}
		}


		String des_qry = "Select description,usage_unit_type,usage_type_code from ARIACORE.usage_types where usage_type="+histy_db.get("USAGE_TYPE_NO")
		ResultSet rs_des_qry = db.executePlaQuery(des_qry)
		while(rs_des_qry.next())
		{
			String unit_des = rs_des_qry.getString(1)
			unit_type = rs_des_qry.getString(2)
			String usage_type_code=rs_des_qry.getString(3)
			histy_db.put("USAGE_TYPE_DESCRIPTION", unit_des)
			histy_db.put("USAGE_TYPE_CODE", usage_type_code)
		}

		String type_qry = "Select description from ariacore.usage_UNIT_TYPES where usage_unit_type="+unit_type
		ResultSet rs_type_qry = db.executePlaQuery(type_qry)
		while(rs_type_qry.next())
		{
			String type = rs_type_qry.getString(1)
			histy_db.put("UNITS_DESCRIPTION",type.concat("s"))
		}

		return histy_db.sort()
	}

	String md_GET_USAGE_HISTORY_LINE_ITEMS_COUNT(String tcid)
	{
		String apiname=tcid.split("-")[2]
		logi.logInfo("Calling md_GET_USAGE_HISTORY_LINE_ITEMS_COUNT")
		return getNodeCountFromResponse(apiname,"//*/*:usage_history_records_row").toString()

	}
	/**
	 * Getting latest invoice_no
	 * @param clientNo
	 * @param accountNo
	 * @return String
	 * @throws ParseException
	 * @throws SQLException
	 */
	String getInvoiceNoVT(String clientNo, String accountNo) throws ParseException, SQLException{
		logi.logInfo "Calling getInvoiceNoVT"
		ConnectDB db = new ConnectDB();
		logi.logInfo "Callingggggggggggggg"
		String invoiceNo

		logi.logInfo("current VT "+db.executeQueryP2("select trunc(ariacore.ariavirtualtime("+clientNo+")) from dual"))
		String currentVirtualTime = getStoredVirtualTime(clientNo)
		logi.logInfo("stored VT "+currentVirtualTime)

		ResultSet rs=db.executePlaQuery("SELECT max(invoice_no) FROM ARIACORE.GL Where ACCT_NO ="+accountNo+" and client_no="+clientNo)

		if(rs.next())
			invoiceNo=rs.getString(1)
		else
			invoiceNo = "Invoice not updated in DB "

		logi.logInfo 'Invoice No : : ' + invoiceNo
		return invoiceNo;
	}
	/**
	 * Getting the total no of invoices of the child account
	 * @param tcid
	 * @return String
	 */
	public String md_GET_CHILD_INVOICE_COUNT(String tcid)
	{
		ConnectDB db = new ConnectDB();
		String child_acct_no= getValueFromResponse('create_acct_complete.a',"//acct_no")
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoice_count=db.executeQueryP2("SELECT COUNT(INVOICE_NO) FROM ARIACORE.gl WHERE CLIENT_NO="+clientNo+" AND ACCT_NO="+child_acct_no)
		return invoice_count
	}
	/**
	 * Getting the expected bill thru date of the account
	 * @param testCaseId
	 * @return String
	 */
	public String md_verify_bill_thru_date_for_acct(String testCaseId){

		boolean returnType = false
		ReadData objRD =new ReadData()
		ConnectDB db = new ConnectDB()

		String apiname = testCaseId.split("-")[2]
		String localTcId = testCaseId.split("-")[1]
		String account_number = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String client_number = Constant.mycontext.expand('${Properties#Client_No}')

		String billDate
		String createdDate
		String billDayQry = "select bill_day from ariacore.acct where acct_no = "+account_number
		String createdQry = "select created from ariacore.acct where acct_no = "+account_number

		String altBillDayFromCAC = objRD.getTestDataMap("create_acct_complete",localTcId).get("alt_bill_day")
		if(altBillDayFromCAC.equals("")){
			altBillDayFromCAC = "0"
		}
		logi.logInfo(" Alt bill day is :: "+altBillDayFromCAC )

		billDate = db.executeQueryP2(billDayQry)
		logi.logInfo(" billDate :: "+billDate )

		Date date = new Date();
		Calendar now = Calendar.getInstance();

		ResultSet resultSet = db.executeQuery(createdQry);
		while(resultSet.next()){
			date = resultSet.getDate(1);
		}

		now.setTime(date);
		int day = now.get(Calendar.DAY_OF_MONTH);
		logi.logInfo(" day :: "+day )

		int tempval = day + Integer.parseInt(altBillDayFromCAC)
		logi.logInfo(" Added value :: "+tempval )

		if(Integer.parseInt(billDate) == tempval){
			returnType = true
		}
		return returnType.toString().toUpperCase()
	}

	/**
	 * Getting the discarded usage index value
	 * @param testcaseid
	 * @return String
	 */
	def md_DiscardUsageExcludeIndCheck(String testcaseid)
	{
		logi.logInfo "Entered into  md_DiscardUsageExcludeIndCheck "
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo "Got account number from response "+acct_no
		def rec_no= getValueFromRequest("discard_usage","//usage_record_no")
		logi.logInfo "Got rec no from discard_usage request "+rec_no
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo "client no from TD "+client_no
		ConnectDB dbqa =new ConnectDB()
		String finalindex

		if(!rec_no.equals("NoVal"))
		{
			String exclude_ind_query="SELECT EXCLUDE_IND FROM ARIACORE.USAGE WHERE ACCT_NO="+acct_no+" AND REC_NO="+rec_no+" AND CLIENT_NO="+client_no+""
			String exclude_ind=dbqa.executeQueryP2(exclude_ind_query)
			finalindex= exclude_ind
		}
		else
		{
			def client_record_id= getValueFromRequest("discard_usage","//client_record_id")
			String rec_no_query="SELECT rec_no FROM ARIACORE.USAGE WHERE ACCT_NO="+acct_no+" AND CLIENT_RECORD_ID='"+client_record_id+"' AND CLIENT_NO="+client_no+""
			rec_no=dbqa.executeQueryP2(rec_no_query)
			logi.logInfo " Got client record id from the discard usage request"
			logi.logInfo " Rec no has not been provided in the discard usage.Considering the client id"
			String exclude_ind_q="SELECT EXCLUDE_IND FROM ARIACORE.USAGE WHERE ACCT_NO="+acct_no+" AND CLIENT_RECORD_ID='"+client_record_id+"' AND CLIENT_NO="+client_no+""
			String exclude_ind1=dbqa.executeQueryP2(exclude_ind_q)
			finalindex= exclude_ind1
		}

		String tempkey
		logi.logInfo( "Recorded usage hash before discard" +Constant.recordedusageamounts.toString() )
		Constant.recordedusageamounts.each{ k,v ->
			if(k.split("-")[4].equals(rec_no)==true)
			{
				tempkey=k
				logi.logInfo("tempkey"+tempkey)
			}
		}
		if(finalindex.equals("1"))
			Constant.recordedusageamounts.remove(tempkey)

		logi.logInfo( "Recorded usage hash after discard" +Constant.recordedusageamounts.toString() )

		return finalindex

	}
	/**
	 * Returns true/false value based on the failed records response in 'discard_usage' api
	 * @param tcid
	 * @return boolean
	 */
	def md_DiscardUsageFailedRecordsAbsenceCheck(String testcaseid)
	{
		boolean StatusFlag = false
		logi.logInfo "Entered into md_DiscardUsageFailedRecordsAbsenceCheck "
		def failed_records = getValueFromResponse('discard_usage',ExPathRpcEnc.DISCARD_FAILED_RECORDS)
		if(failed_records.equals("NoVal"))
		{
			StatusFlag = true
		}

		return StatusFlag.toString().toUpperCase()
	}
	/**
	 * Getting the total billed units after discarding the usage
	 * @param tcid
	 * @return String
	 */
	def md_GetBilledUnitsAftDiscardUsage(String testcaseid)
	{
		logi.logInfo "Entered into main method - md_GetBilledUnitsAftDiscardUsage"
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo "client no from TD "+client_no
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo "account number from response "+acct_no
		ConnectDB database =new ConnectDB()
		String invoice_no_query="SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO="+acct_no+" AND ROWNUM=1 AND CLIENT_NO="+client_no+"ORDER BY 1 DESC"
		String invoice_no=database.executeQueryP2(invoice_no_query)
		logi.logInfo"Invoice no from DB "+invoice_no
		String billed_units_query="SELECT NVL(SUM(USAGE_UNITS),0) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+" AND USAGE_TYPE IS NOT NULL AND CLIENT_NO="+client_no+""
		String billed_units=database.executeQueryP2(billed_units_query)
		logi.logInfo "Billed units from DB "+billed_units
		return billed_units
	}
	/**
	 * Getting the total billed units amount after discarding the usage
	 * @param testcaseid
	 * @return String
	 */
	def md_GetBilledAmountAftDiscardUsage(String testcaseid)
	{
		logi.logInfo "Entered into main method - md_GetBilledAmountAftDiscardUsage"
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo "client no from TD "+client_no
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo "account number from response "+acct_no
		ConnectDB database =new ConnectDB()
		String invoice_no_query="SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO="+acct_no+" AND ROWNUM=1 AND CLIENT_NO="+client_no+"ORDER BY 1 DESC"
		String invoice_no=database.executeQueryP2(invoice_no_query)
		logi.logInfo"Invoice no from DB "+invoice_no
		String billed_amount_query="SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+" AND USAGE_TYPE IS NOT NULL AND CLIENT_NO="+client_no+""
		String billed_amount=database.executeQueryP2(billed_amount_query)
		logi.logInfo "Billed Amount from DB "+billed_amount
		return billed_amount
	}
	/**
	 * Getting the rec_no of  the discarded usage
	 * @param testcaseid
	 * @return String
	 */
	def md_GetDiscardUsageFailedRecNumber(String testcaseid)
	{
		logi.logInfo "Entered into  md_GetDiscardUsageFailedRecNumber "
		def rec_no= getValueFromRequest("discard_usage","//usage_record_no")
		logi.logInfo "Got rec no from discard_usage request "+rec_no
		return rec_no
	}
	
	def md_ExcludeIndCheckForRetainedUsage_a(String testcaseid)
	{
		logi.logInfo "Entered into md_ExcludeIndCheckForRetainedUsage_a"
		return md_ExcludeIndCheckForRetainedUsage("a",testcaseid)

	}

	def md_ExcludeIndCheckForRetainedUsage_b(String testcaseid)
	{
		logi.logInfo "Entered into md_ExcludeIndCheckForRetainedUsage_b"
		return md_ExcludeIndCheckForRetainedUsage("b",testcaseid)

	}
	/**
	 * Getting EXCLUDE_IND value for the usage type from USAGE table
	 * @param type
	 * @param testcaseid
	 * @return String 
	 */
	def md_ExcludeIndCheckForRetainedUsage(String type,String testcaseid)
	{
		logi.logInfo "Entered into main method - md_ExcludeIndCheckForRetainedUsage "
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo "Got account number from response "+acct_no
		def rec_no=getValueFromResponse("record_usage."+type,ExPathRpcEnc.RECORD_USAGE_USAGERECNO1)
		logi.logInfo "Got rec no from record_usage response "+rec_no
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo "client no from TD "+client_no
		ConnectDB dbqa =new ConnectDB()
		String exclude_ind_query="SELECT EXCLUDE_IND FROM ARIACORE.USAGE WHERE ACCT_NO="+acct_no+" AND REC_NO="+rec_no+" AND CLIENT_NO="+client_no+""
		String exclude_ind=dbqa.executeQueryP2(exclude_ind_query)
		//dbqa.closeconnection();
		return exclude_ind

	}
	/**
	 * Getting the expected billed units
	 * @param testcaseid
	 * @return String
	 */
	def md_ExpBilledUnits(String testcaseid)
	{
		logi.logInfo "Entered into main method - md_ExpBilledUnits "
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo "Got account number from response "+acct_no
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo "client no from TD "+client_no
		ConnectDB database =new ConnectDB()
		String invoice_no_query="SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO="+acct_no+" AND ROWNUM=1 AND CLIENT_NO="+client_no+" ORDER BY 1 DESC"
		String invoice_no=database.executeQueryP2(invoice_no_query)
		logi.logInfo"Invoice no from DB "+invoice_no
		String exp_billed_units_q="SELECT NVL(SUM(USAGE_UNITS),0) FROM ARIACORE.USAGE WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no+" AND INVOICE_NO="+invoice_no+" AND EXCLUDE_IND=0"
		String exp_billed_units=database.executeQueryP2(exp_billed_units_q)
		logi.logInfo"Expected Billed units from DB "+exp_billed_units
		return exp_billed_units
	}

	/**
	 * Getting the parent account no from create_acct_complete api
	 * @param testid
	 * @return String
	 */
	public String md_ActualUsage_Amt(String testid) {

		String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
		if(accountNo == 'NoVal')
		{
			 accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		return md_ActualUsage_Amt(accountNo,testid);
	}
	/**
	 * Getting the child account no from create_acct_complete.a api
	 * @param testid
	 * @return String
	 */
	public String md_ActualUsage_Amt_Child(String testid) {

		String accountNo=getValueFromResponse('create_acct_complete.a',"//acct_no")
		if(accountNo == 'NoVal')
		{
			 accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		return md_ActualUsage_Amt(accountNo,testid);
	}
	/**
	 * Getting the actual usage amount from the latest invoice
	 * @param accountNo
	 * @param testcaseid
	 * @return String
	 */
	String md_ActualUsage_Amt(String accountNo,String testcaseid)
	{
		logi.logInfo "Calling md_ActualUsage_Amt for acct_no :  "+accountNo
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String amount="0"
		
		logi.logInfo "checkinggggggggggggggggggggg :  "+amount
		invoiceNo = getInvoiceNoVT(clientNo, accountNo).toString();
		logi.logInfo( "Invoice number from getInvoiceNoVT " +invoiceNo)
		if(invoiceNo.contains("not updated")!= true) //Got invoice number without any error
		{
			String query = "select NVL(sum(debit),0)  FROM ariacore.gl_detail WHERE client_no="+clientNo+" AND service_no IN (SELECT service_no FROM ariacore.services WHERE usage_based=1 and custom_to_client_no="+clientNo+" ) AND invoice_no IN ("+invoiceNo+")"
			logi.logInfo "yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy :  "+amount
			
			if(accountNo == "NoVal" || invoiceNo == "NoVal") {
				
				logi.logInfo "intolooppppppppppppiffffff :  "+accountNo
				amount = "No Val ret"
			} else {
			
			logi.logInfo "intolooppppppppppppelseeeee :  "+accountNo
				ConnectDB dbActInvoice = new ConnectDB();
				ResultSet resultSetActInvoice = dbActInvoice.executePlaQuery(query)
				amount= ""
				if (resultSetActInvoice.next())
					amount = resultSetActInvoice.getString(1)
				resultSetActInvoice.close()
			}
			
		}else
		{
			logi.logInfo "outerrrrrrintolooppppppppppppelseeeee :  "+accountNo
			amount="Invoice not updated in DB "
		}
		logi.logInfo "Exit Test tttttt :  "+amount
		return amount;
	}


	String md_ExpUsage_Amt(String testcaseid)
	{
		def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		return md_ExpUsage_Amt(testcaseid,acct_no)
	}

	String md_ExpUsage_Amt_Child(String testcaseid)
	{
		def acct_no = getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		return md_ExpUsage_Amt(testcaseid,acct_no)
	}
	String md_ExpInvoiceWithUsage_Amt(String testcaseid)
	{
		def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		return md_ExpInvoiceWithUsage_Amt(testcaseid,acct_no)
	}

	String md_ExpInvoiceWithUsage_Amt_Child(String testcaseid)
	{
		def acct_no = getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		return md_ExpInvoiceWithUsage_Amt(testcaseid,acct_no)
	}

	String md_ExpInvoiceWithoutUsage_Amt(String testcaseid)
	{
		def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		return md_ExpInvoiceWithoutUsage_Amt(testcaseid,acct_no)
	}

	String md_ExpInvoiceWithoutUsage_Amt_Child(String testcaseid)
	{
		def acct_no = getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		return md_ExpInvoiceWithoutUsage_Amt(testcaseid,acct_no)
	}
	/**
	 * Getting the expected invoice and usage amount
	 * @param testCaseId
	 * @param acct_no
	 * @return String 
	 */
	public String md_ExpInvoiceWithUsage_Amt(String testCaseId,String acct_no){		
		logi.logInfo("callingthismethod ")
		logi.logInfo("calling md_ExpInvoiceWithUsage_Amt")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=testCaseId.split("-")[2]
		double totalInvoice=0.0
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no).toString();
		logi.logInfo( "invoiceNo in md_ExpInvoiceWithUsage_Amt method " +invoiceNo)
		double exp = md_ExpInvoice_Amt(testCaseId,acct_no).toDouble()
		logi.logInfo("expInvoice "+exp.toString())

		logi.logInfo "Incoming acct"+acct_no

		List<String> usagekeys=[]
		Constant.recordedusageamounts.each
		{ k,v ->
			logi.logInfo "key now in hash "+k.split("-")[3]
			if(k.split("-")[3].equals(acct_no)==true)

			{
				//usg=usg+v.toString().toDouble()
				usagekeys.add(k)
			}

		}
		logi.logInfo("usagekeys list  "+usagekeys.toString())

		boolean auto_rateFlg = get_Auto_Rate_Unrated_Usage_Flag()
		String usg
		if(!auto_rateFlg){
			usg = md_ExpUsage_Amt(testCaseId,acct_no)
			//md_ExpUsage_Amt_With_Auto_Rate(testCaseId)
		} else
			usg=md_ExpUsage_Amt(testCaseId,acct_no)
		logi.logInfo("usgInvoice "+usg.toString())
		double creditamt=db.executeQueryP2("select NVL(sum(credit),0) from ariacore.gl where acct_no="+acct_no+" and client_no="+clientNo+" and invoice_no="+invoiceNo).toString().toDouble()

		String resp_level_cd = db.executeQueryP2("select resp_level_cd from ariacore.acct where acct_no="+acct_no+" and client_no="+clientNo).toString()
		logi.logInfo("callingthiscollectiongroupsss "+usg.toString())
		if(resp_level_cd == '2' || !getValueFromRequest("create_acct_complete","//collections_acct_groups").equals("NoVal") || !getValueFromRequest("create_acct_complete","//coupon_codes").equals("NoVal"))
			creditamt = 0.0
			
		totalInvoice =  exp+ (usg.toDouble().round(2))-creditamt.round(2)
		logi.logInfo("totalInvoice "+totalInvoice)
		//Constant.recordedusageamounts.clear()

		logi.logInfo "All recorded usage hash before "+testCaseId.split("-")[1] + " => "+Constant.recordedusageamounts.toString()

		/*Clearing usage hash for bill lag day usage amount calculation*/
		ReadData objRD =new ReadData()
		logi.logInfo " tc id " +testCaseId
		String localTcId = testCaseId.split("-")[1]
		logi.logInfo " local tc id " +localTcId
		String ClearHashVal = objRD.getTestDataMap("create_acct_complete",localTcId).get("Clear_UsageHash").toString()
		logi.logInfo "Usage Clear Hash Value " + ClearHashVal.toString()
		if(!ClearHashVal.equalsIgnoreCase("No"))
		{
			logi.logInfo "Gonna clear usage hash"
			for (key in usagekeys){
				Constant.recordedusageamounts.remove(key)
			}
		}

		logi.logInfo "All recorded usage hash after "+testCaseId.split("-")[1] + " => "+Constant.recordedusageamounts.toString()
		return d.format(totalInvoice).toString()
	}
	/**
	 * Getting the expected invoice amount without usage amount
	 * @param testCaseId
	 * @param acct_no
	 * @return String 
	 */
	public String md_ExpInvoiceWithoutUsage_Amt(String testCaseId, String acct_no)
	{

		logi.logInfo("calling md_ExpInvoiceWithoutUsage_Amt")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		double usg = 0.0
		double cal_usg
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=testCaseId.split("-")[2]
		double totalInvoice=0.0
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no).toString();
		logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		double exp = md_ExpInvoice_Amt(testCaseId,acct_no).toDouble()
		logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		double creditamt=db.executeQueryP2("select NVL(sum(credit),0) from ariacore.gl where acct_no="+acct_no+" and client_no="+clientNo+" and invoice_no="+invoiceNo).toString().toDouble()
		totalInvoice =  exp-creditamt.round(2)
		logi.logInfo("totalInvoice in md_ExpInvoiceWithoutUsage_Amt method "+totalInvoice)
		return d.format(totalInvoice).toString()

	}
	/**
	 * Getting the expected usage amount
	 * @param testCaseId
	 * @param acct_no
	 * @return String
	 */
	public String md_ExpUsage_Amt(String testCaseId,String acct_no){
		logi.logInfo("calling md_ExpUsage_Amt for acct_no:"+acct_no)
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo("Client No ukuk "+clientNo)
		double usg = 0.0
		logi.logInfo("usggggggggggggggggggggggg "+usg)
		ConnectDB db = new ConnectDB()
		logi.logInfo("dbbbbbbbbbbbbbbbbbb "+db)
		DecimalFormat d = new DecimalFormat("#.##########");
		logi.logInfo("DecimalFormattttttttttttttt "+testCaseId)
		String apiname=testCaseId.split("-")[2]
		logi.logInfo("apinameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee "+testCaseId)
		logi.logInfo("apinameeeeeeeeeeeeeeeeeeeeeeeeeee "+apiname)
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no).toString();
		logi.logInfo( "invoiceNo in md_ExpUsage_Amt method " +invoiceNo)
		HashMap<String, ArrayList<String>> usagetypeplans= new HashMap<String, ArrayList<String>>()
		HashMap<String, ArrayList<String>> usage_rateamt_hm = new HashMap<String, ArrayList<String>>()
		String unbilled_flag =db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name = 'INVOICE_UNBILLED_USAGE_DURING_PRORATION' and client_no="+clientNo)

		logi.logInfo("recordedusageamounts - in md exp usage amt method "+Constant.recordedusageamounts.toString())
		String allow_pooling = db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name='ALLOW_USAGE_POOLING' and client_no="+clientNo).toString()
		String is_pool_enabled = db.executeQueryP2("Select param_val from ariacore.acct_plan_params where param_name='ENABLE_USAGE_POOLING' and acct_no="+acct_no).toString()

		if( is_pool_enabled.equalsIgnoreCase("TRUE")  && allow_pooling.equalsIgnoreCase("TRUE"))
		{
			logi.logInfo("pooling has been enabled for this account")
			Constant.recordedusageamounts.each
			{ k,v ->
				if(k.split("-")[3].equals(acct_no)==true)
				{
					String plan_no = k.split("-")[6].toString()
					String pooling_enabled_plan=db.executeQueryP2("Select plan_no from ariacore.acct_plan_params where param_name='ENABLE_USAGE_POOLING' and acct_no="+acct_no).toString()
					if(pooling_enabled_plan.equals(plan_no) == true)
					{
						String amt = v.toString()
						logi.logInfo("calculated amt for this pooled plan:"+amt)
						if(amt.contains("calc") == false)
						{
							logi.logInfo("inside the ifffff:"+amt)
							Constant.recordedusageamounts.put(k, "calc:"+amt)
						}
					}
				}
			}
		}

		logi.logInfo("now the Constant.recordedusageamounts hash is:"+Constant.recordedusageamounts.toString())
		if(unbilled_flag == "TRUE")
		{
			int utcount=1;
			Constant.recordedusageamounts.each
			{ k,v ->
				if(k.split("-")[3].equals(acct_no)==true)
				{
					String plan_no = k.split("-")[6].toString()
					String amt = v.toString()
					if (amt.contains("calc") == false)
					{
						logi.logInfo("inside if calc false loop")
						if (usagetypeplans.containsKey(k.split("-")[5]))
						{
							logi.logInfo("inside if calc false loop - if " + usagetypeplans.get(k.split("-")[5]).toString().split(",")[1].toString() +" , amt : "+amt)
							double tmpamtt = usagetypeplans.get(k.split("-")[5]).toString().split(",")[1].toString().toDouble() + amt.toDouble()
							logi.logInfo("Calculated amt: " + tmpamtt.toString())
							usagetypeplans.put(k.split("-")[5],plan_no+","+tmpamtt.toString())
						}
						else
							usagetypeplans.put(k.split("-")[5],plan_no+","+amt)
					}
					else
					{
						logi.logInfo("inside calc loop")
						usagetypeplans.put(k.split("-")[5]+ ","+utcount.toString(),plan_no+","+amt )
						utcount++;
					}
					logi.logInfo("utcount :: "+utcount)
					//usagetypeplans.put(k.split("-")[5]+ ","+utcount.toString(),plan_no+","+amt)



				}
			}
		}

		else if(unbilled_flag == "FALSE")
		{
			int utcount=1;
			List res_plan_nos=[]
			boolean auto_rate_flag = get_Auto_Rate_Unrated_Usage_Flag()
			if(!auto_rate_flag)
			{
				res_plan_nos = getAllUsedPlanNoP2(acct_no,"")
			}
			else
			{
				String bill_plan_no = getUsedPlanNoP2(acct_no,"")
				res_plan_nos.add(bill_plan_no)
			}

			logi.logInfo("usage should be billed for the plan:"+res_plan_nos.toString())
			for(plan in res_plan_nos)
			{
				logi.logInfo("the Plan is :"+plan.toString())
				Constant.recordedusageamounts.each
				{ k,v ->
					if(k.split("-")[3].equals(acct_no)==true && k.split("-")[6].toString() == plan.toString())
					{
						String plan_no = k.split("-")[6].toString()
						String amt = v.toString()
						if (amt.contains("calc") == false)
						{
							logi.logInfo("inside if calc false loop")
							if (usagetypeplans.containsKey(k.split("-")[5]))
							{
								logi.logInfo("inside if calc false loop - if " + usagetypeplans.get(k.split("-")[5]).toString().split(",")[1].toString() +" , amt : "+amt)
								double tmpamtt = usagetypeplans.get(k.split("-")[5]).toString().split(",")[1].toString().toDouble() + amt.toDouble()
								logi.logInfo("Calculated amt: " + tmpamtt.toString())
								usagetypeplans.put(k.split("-")[5]+","+plan_no,plan_no+","+tmpamtt.toString())
							}
							else
								usagetypeplans.put(k.split("-")[5]+","+plan_no,plan_no+","+amt)


						}
						else
						{
							logi.logInfo("inside calc loop")
							usagetypeplans.put(k.split("-")[5]+ ","+utcount.toString(),plan_no+","+amt )
							utcount++;
						}
						logi.logInfo("utcount :: "+utcount)
					}
				}

			}
		}

		logi.logInfo( "usagetypeplans in md_ExpUsage_Amt method " +usagetypeplans.toString())

		usagetypeplans.each
		{ k,v ->
			logi.logInfo( "Inside usagetype set loop")
			String[] karray = k.split(",")
			logi.logInfo ("Key val :: "+karray[0]);
			String[] varray = v.split(",")
			if(varray[1].toString().contains("calc:") == true)
			{
				logi.logInfo ("verifying the iffff:: "+karray[0]);
				double calc_amt = varray[1].split("calc:")[1].toString().toDouble().round(2)
				usg = (usg + calc_amt).toDouble()
			}
			else
			{
				String totalunits=db.executeQueryP2("SELECT NVL(sum(billable_units),0) FROM ariacore.usage u JOIN ariacore.acct act ON act.acct_no=u.acct_no WHERE act.acct_no = "+acct_no+" AND u.usage_type ="+karray[0]+" AND (u.invoice_no ="+invoiceNo+" or u.invoice_no  is null) and u.exclude_ind=0 and u.client_rated=0")
				List service_nos=md_GET_USAGE_SERVICE_NO(acct_no.toString(),karray[0],varray[0])
				
				logi.logInfo( "Inside usagetype set loopjhdfgvasudfgkausydgfahsgdfa")
				for(service in service_nos)
				{
					String threshold = db.executeQueryP2("Select unbillable_usg_threshold from ariacore.plan_services where service_no="+service+" and plan_no="+varray[0]+" and client_no="+clientNo).toString()
					if(!threshold.equalsIgnoreCase("null"))
					{
						logi.logInfo("usage recorded for Adv billing Threshold enabled service")
						String tot_units=db.executeQueryP2("SELECT NVL(sum(usage_units),0) FROM ariacore.usage u JOIN ariacore.acct act ON act.acct_no=u.acct_no WHERE act.acct_no = "+acct_no+" AND u.usage_type ="+karray[0]+" AND (u.invoice_no ="+invoiceNo+" or u.invoice_no  is null) and u.exclude_ind=0 and u.client_rated=0")
						totalunits = d.format((tot_units.toDouble() - threshold.toDouble()).toDouble()).toString()
						if(totalunits.toDouble() < 0)
						{
							totalunits = "0"
						}
					}
					logi.logInfo("service_no in md_ExpUsage_Amt "+service_nos)
					usage_rateamt_hm=md_USAGE_AMT_RATE_EXP(acct_no.toString(),service_nos,totalunits,karray[0],varray[0])
					logi.logInfo("usage_rateamt_hm in md_ExpUsage_Amt "+usage_rateamt_hm)
					String amt=usage_rateamt_hm.get(service)[1].toString()
					usg=usg+amt.toString().toDouble()
					logi.logInfo( "Inside usagetype set sdfasdfasdfwerfasdfasdfvasdfasdfasdfasdf")
				}
			}
		}

		return d.format(usg.round(2)).toString()
	}
	/**
	 * Getting the usage count from 'get usage history' api
	 * @param testcaseid
	 * @return String
	 */
	public def md_GetUsageHistoryLineCount(String testcaseid)
	{
		def Line_count
		logi.logInfo "Entered into md_Get_UsageHistory_Line_Count"
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo "Got account number from response "+acct_no
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo "client no from TD "+client_no
		def usage_type=getValueFromRequest("get_usage_history","//specified_usage_type_no")
		logi.logInfo "Got Usage type from the get usage history request "+usage_type
		def usage_type_code=getValueFromRequest("get_usage_history","//specified_usage_type_code")
		logi.logInfo "Got Usage type code from the get usage history request "+usage_type_code
		ConnectDB database =new ConnectDB()
		if(!usage_type.equals("NoVal"))
		{
			String line_count_query="SELECT COUNT(*) FROM ARIACORE.USAGE WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no+" AND USAGE_TYPE="+usage_type
			Line_count=database.executeQueryP2(line_count_query)
		}
		else if(!usage_type_code.equals("NoVal"))
		{
			String line_count_query2="SELECT COUNT(*) FROM ARIACORE.USAGE USG JOIN ARIACORE.USAGE_TYPES UST ON USG.USAGE_TYPE=UST.USAGE_TYPE WHERE USG.ACCT_NO="+acct_no+" AND USG.CLIENT_NO="+client_no+" AND UST.CUSTOM_TO_CLIENT_NO="+client_no+" AND UST.USAGE_TYPE_CODE='"+usage_type_code+"'"
			Line_count=database.executeQueryP2(line_count_query2)
		}
		else
		{
			String line_count_query1="SELECT COUNT(*) FROM ARIACORE.USAGE WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no
			Line_count=database.executeQueryP2(line_count_query1)
		}
		return Line_count
	}

	public String md_Is_Proration_Applied(String tcid)
	{
		def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		return md_Is_Proration_Applied(tcid,acct_no)

	}
	/**
	 * Returns true/false value based on proration factor
	 * @param tcid
	 * @param acct_no
	 * @return String
	 */
	public String md_Is_Proration_Applied(String tcid,String acct_no)
	{
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no).toString();
		logi.logInfo( "invoiceNo in md_Is_Proration_Applied method " +invoiceNo)
		ConnectDB db = new ConnectDB()
		String flag
		int a=db.executeQueryP2("select count(*) from ariacore.gl_detail where invoice_no="+invoiceNo+" and proration_factor is not  null and   proration_factor!=1 and orig_credit_id is null").toString().toInteger()
		if(a>=1)
			flag="true"
		else
			flag="false"
		return flag.toUpperCase()

	}
	/**
	 * Getting the default_rate_schedule number for the account
	 * @param testCaseId
	 * @return String
	 */
	public String md_verify_Default_Rate_Tier_For_Acct(String testCaseId)
	{
		logi.logInfo( "Calling md_verify_Default_Rate_Tier_For_Acct")
		ConnectDB db = new ConnectDB()
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String plan_no=getUsedPlanNoP2(acct_no,"")
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')

		String query="select distinct(RATE_SCHEDULE_NO) from ariacore.acct_rates where acct_no = "+acct_no+" and plan_no = "+plan_no+" and client_no ="+client_no
		String default_rate_schedule = db.executeQueryP2(query)

		return default_rate_schedule
	}
	/**
	 * Getting the expected rate_schedule number for the account
	 * @param testCaseId
	 * @return String 
	 */
	public String md_verify_Default_Rate_Tier_Expected(String testCaseId)
	{
		logi.logInfo( "Calling md_verify_Default_Rate_Tier_For_Acct")
		ConnectDB db = new ConnectDB()
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def currency_cd = getValueFromRequest('create_acct_complete',"//currency_cd")
		String plan_no=getUsedPlanNoP2(acct_no,"")
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')

		//String query="select DEFAULT_RATE_SCHED_NO from ariacore.client_plan where plan_no = "+plan_no+" and client_no ="+client_no
		String query = "SELECT min(schedule_no) FROM ariacore.new_rate_schedules WHERE schedule_no IN (SELECT DISTINCT rate_schedule_no FROM ariacore.acct_rates WHERE plan_no = "+plan_no+" AND client_no ="+client_no+") AND client_no  ="+client_no+" AND currency_cd='"+currency_cd+"'"
		String default_rate = db.executeQueryP2(query)
		return default_rate
	}
	/**
	 * Getting the alt rate schedule number from update_acct_complete api
	 * @param testCaseId
	 * @return String
	 */
	public String md_verify_Default_Rate_Tier_BackDate_Expected(String testCaseId)
	{
		logi.logInfo( "Calling md_verify_Default_Rate_Tier_For_Acct")
		def default_rate=getValueFromRequest('update_acct_complete',"//master_plan_alt_rate_sched_no")
		return default_rate
	}
	/**
	 * Getting all the active plan numbers of the account
	 * @param accountNo
	 * @param usagetype
	 * @return List plan nos
	 * @throws SQLException
	 */
	List getAllUsedPlanNoP2(String accountNo,String usagetype) throws SQLException{
		logi.logInfo "Calling getAllUsedPlanNoP2 accountNo => "+accountNo
		ConnectDB db = new ConnectDB();
		List planNo = []
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String master_plan_query
		String supp_plan_query
		String masterplanNo
		if(usagetype.equals(""))
		{
			master_plan_query="select plan_no from ariacore.all_acct_active_plans where acct_no="+accountNo+" and supp_plan_ind=0 and  client_no="+clientNo+" and plan_no  in ( SELECT plan_no FROM ariacore.plan_services WHERE service_no IN  (SELECT service_no   FROM ariacore.services   WHERE usage_type IN  (SELECT usage_type FROM ariacore.usage WHERE acct_no = "+accountNo+" )) ) "
			supp_plan_query=" select plan_no from ariacore.all_acct_active_plans where acct_no="+accountNo+" and supp_plan_ind=1 and  client_no="+clientNo+" and plan_no in ( SELECT plan_no FROM ariacore.plan_services WHERE service_no IN  (SELECT service_no  FROM ariacore.services  WHERE usage_type IN (SELECT usage_type FROM ariacore.usage WHERE acct_no = "+accountNo+" ))) and acct_no ="+accountNo
		}
		else
		{
			master_plan_query="select plan_no from ariacore.all_acct_active_plans where acct_no="+accountNo+" and supp_plan_ind=0 and  client_no="+clientNo+" and plan_no  in ( SELECT plan_no FROM ariacore.plan_services WHERE service_no IN  (SELECT service_no   FROM ariacore.services   WHERE usage_type ="+usagetype+") )"
			supp_plan_query="select plan_no from ariacore.all_acct_active_plans where acct_no="+accountNo+" and supp_plan_ind=1 and  client_no="+clientNo+" and plan_no in ( SELECT plan_no FROM ariacore.plan_services WHERE service_no IN  (SELECT service_no  FROM ariacore.services  WHERE usage_type ="+usagetype+")) and acct_no ="+accountNo
		}
		masterplanNo = db.executeQueryP2(master_plan_query)
		String[] suppplanNo = db.executeQueryReturnValuesAsArray(supp_plan_query)
		logi.logInfo "Supp plan no's :: "+suppplanNo.toString()
		logi.logInfo "Supp plan no's size  :: "+suppplanNo.size()

		if(masterplanNo != null && suppplanNo.length < 1)
		{
			planNo.add(masterplanNo)
			logi.logInfo "Supp plan no's length :: 1 "+planNo
		}
		else if (masterplanNo != null && suppplanNo.length >= 1)
		{
			planNo.add(masterplanNo)
			planNo.addAll(suppplanNo)
			logi.logInfo "Supp plan no's length :: 2 "+planNo
		}
		else
		{
			planNo.addAll(suppplanNo)
			logi.logInfo "Supp plan no's length :: 3 "+planNo
		}
		logi.logInfo 'Plan No are : : ' + planNo.toString()

		return planNo
	}
	/**
	 * Getting 'AUTO_RATE_UNRATED_USAGE_AT_LOAD_TIME' flag value for the client
	 * @return boolean
	 */
	public boolean get_Auto_Rate_Unrated_Usage_Flag(){
		boolean autoRateFlag = false

		ConnectDB db = new ConnectDB()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')

		String autoRateQry = "select param_val from ariacore.aria_client_params where client_no = "+clientNo+" and param_name = 'AUTO_RATE_UNRATED_USAGE_AT_LOAD_TIME'"
		String flag = db.executeQueryP2(autoRateQry)
		if(flag.equals("TRUE")){
			autoRateFlag = true
		}

		logi.logInfo "Auto rate unrated usage flag for the client : "+clientNo+" is : "+autoRateFlag
		return autoRateFlag
	}
	public String md_Service_Type_Wise_Billing(){

	}
	//Overloaded method with plan_no as additional parameter
	HashMap md_USAGE_AMT_RATE_EXP(String acct_no,List service_nos,String usage_units,String usagetype,String plan_no)
	{
		
		String apiname = Constant.SERVICENAME
		logi.logInfo("md_USAGE_AMT_RATE_EXP apiname is :"+apiname)
		
		logi.logInfo("calling md_USAGE_AMT_RATE_EXP acct_no=>"+acct_no+" service_nos=>"+service_nos.toString()+"usage_units=> "+usage_units+" plan_no=> "+plan_no)

		HashMap<String, ArrayList<String>> usage_hm = new HashMap<String, ArrayList<String>>()
		LinkedHashMap<String,String> hm_unit_rates=new LinkedHashMap<String, String>();

		String client_no =Constant.mycontext.expand('${Properties#Client_No}')
		int acc_cnt=0;
		
		String price_rule
		String pooling_enabled
		double input_units=usage_units.toDouble();
		int rate_rnd,lag_days
		double already_recorded_units
		String acc_months1
		String acc_months
		Date acct_created_date,usg_date,virtual_date
		
		boolean flaag=true;
		ConnectDB db = new ConnectDB()
		String allow_acc= db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name='ALLOW_USAGE_ACCUMULATION' and client_no="+client_no).toString()
		String allow_pooling = db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name='ALLOW_USAGE_POOLING' and client_no="+client_no).toString()
		String poolingcnt= db.executeQueryP2("Select count(param_val) from ariacore.acct_plan_params where param_name='ENABLE_USAGE_POOLING' and acct_no="+acct_no+" and plan_no="+plan_no).toString()
		if(poolingcnt.equals("0"))
		{
			pooling_enabled = "FALSE" 
		}
		else
		{
			pooling_enabled = db.executeQueryP2("Select param_val from ariacore.acct_plan_params where param_name='ENABLE_USAGE_POOLING' and acct_no="+acct_no+" and plan_no="+plan_no).toString()
		}
		if(acct_months_hm.containsKey(acct_no+"-"+plan_no) == true)
		{
			acc_months = acct_months_hm.get(acct_no+"-"+plan_no)
			logi.logInfo("Reterieved accumulation for that acct:"+acc_months)
	
		}
		else
		{
			acc_months = db.executeQueryP2("SELECT param_val from ariacore.acct_plan_params where param_name='USAGE_ACCUMULATION_RESET_MONTHS' and acct_no= "+acct_no+" and plan_no="+plan_no+" and client_no="+client_no).toString()
			if(!acc_months.equalsIgnoreCase("null"))
			{
			
				acct_months_hm.put(acct_no+"-"+plan_no,acc_months)
				logi.logInfo("storing acct no and reset months:"+acct_months_hm.get(acct_no+"-"+plan_no).toString())
			}
		}
		if(!acct_months_hm.get(acct_no+"-"+plan_no).toString().equalsIgnoreCase("null"))
		{
			acc_months = db.executeQueryP2("SELECT param_val from ariacore.acct_plan_params where param_name='USAGE_ACCUMULATION_RESET_MONTHS' and acct_no= "+acct_no+" and plan_no="+plan_no+" and client_no="+client_no).toString()
			if(acc_months.toInteger()>0)
			{
				flaag=true;
			}
			else if(acc_months.equals("0"))
			{
				acc_cnt++;
				if(acc_cnt==1)
				{
					
				if(!accumutemp.containsKey(acct_no))
				{
				accumutemp.put(acct_no,apiname)
				flaag = true;
				logi.logInfo("Storing apiname in the accumutemp: "+accumutemp.get(acct_no).toString())
				}
				
				}
			}
		}
//		else
//		{
//			
//			acc_months = db.executeQueryP2("SELECT param_val from ariacore.acct_plan_params where param_name='USAGE_ACCUMULATION_RESET_MONTHS' and acct_no= "+acct_no+" and plan_no="+plan_no+" and client_no="+client_no).toString()
//			if(!acc_months1.equalsIgnoreCase("null"))
//			{
//				if(acc_months1.toInteger()>0)
//				{
//					acc_months = acc_months1
//				}
//				String firInvFlg = db.executeQueryP2("SELECT param_val from ariacore.aria_client_params where param_name='INCLUDE_USAGE_CHARGES_ON_FIRST_INVOICE' and client_no="+client_no)
//				if(firInvFlg.equalsIgnoreCase("TRUE"))
//				{
//					if(acc_months1.toInteger()>0)
//					{
//						flaag=true;
//					}
//					else if(acc_months1.equals("0"))
//					{
//						acc_cnt++;
//						if(acc_cnt==1)
//						{
//						temp.put(acct_no,apiname)
//						}
//					}	
//					logi.logInfo("Include usgae charge on first invoice flg TRUE")
//					//acc_months = (acc_months.toInteger()-1).toString()
//					
//					
//				}
//			}
//			
//			
//			logi.logInfo("acc_months: "+acc_months)
//			logi.logInfo("flaag: "+flaag)
//			logi.logInfo("allow_acc: "+allow_acc)
//			acct_months_hm.put(acct_no+"-"+plan_no,acc_months)
//			logi.logInfo("storing acct no and reset months:"+acct_months_hm.get(acct_no+"-"+plan_no).toString())
//		}
		
		if(accumutemp.containsKey(acct_no))
		{
			if(!accumutemp.get(acct_no).equals(apiname))
			{
			
			flaag=false;
			}
		}
		
		logi.logInfo("Acc Cnter Value is: "+acc_cnt)
		rate_rnd = db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name ='USAGE_RATE_PER_UNIT_SCALE' and  client_no="+client_no).toInteger()
		
		if(pooling_enabled.equalsIgnoreCase("TRUE") && allow_pooling.equalsIgnoreCase("TRUE") )
		{
			logi.logInfo("pooling enabled acct")
			already_recorded_units =  db.executeQueryP2("SELECT CASE WHEN TOTAL_UNITS = 0 THEN (SELECT NVL(SUM(billable_units),0) AS TOTAL_UNITS FROM ariacore.usage WHERE exclude_ind  =0 and acct_no="+acct_no+" AND (USAGE_DATE > (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+" and bill_DATE < (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+") ) and USAGE_DATE < (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+"))) ELSE TOTAL_UNITS  END AS FINAL_TOTAL_UNITS FROM ( SELECT NVL(SUM(billable_units),0) AS TOTAL_UNITS FROM ariacore.usage WHERE exclude_ind  =0 AND acct_no ="+acct_no+" AND (USAGE_DATE > (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+ " AND client_no ="+client_no+")))").toString().toDouble()
			String threshold
			String ser_nos = service_nos.toString().replace("[", "")
			ser_nos = ser_nos.replace("]", "")
			if(!ser_nos.equals(""))
				threshold = db.executeQueryP2("Select unbillable_usg_threshold from ariacore.plan_services where service_no IN ("+ser_nos+") and plan_no="+plan_no+" and client_no="+client_no).toString()
			else
				threshold = "null"

			if(!threshold.equalsIgnoreCase("null"))
			{
				logi.logInfo("pooling recorded for Adv billing Threshold enabled service")
				already_recorded_units = already_recorded_units - threshold.toDouble()
				usage_units=  (usage_units.toDouble() -  threshold.toDouble() ).toString()
				input_units = usage_units.toDouble()
			}
			if(already_recorded_units < 0)
			{
				already_recorded_units =0
			}
		}
		else if(!acct_months_hm.get(acct_no+"-"+plan_no).toString().equalsIgnoreCase("null") && allow_acc.equalsIgnoreCase("TRUE") && flaag==true)
		{
			logi.logInfo("This is accumalation enabled acct")
			String created_date_qry = "select created from ariacore.acct where acct_no="+acct_no
			ResultSet rs_created_date_qry = db.executePlaQuery(created_date_qry)
			while(rs_created_date_qry.next())
			{
				acct_created_date = rs_created_date_qry.getDate(1)
			}
			int bill_interval =db.executeQueryP2("SELECT distinct billing_interval FROM ariacore.client_plan WHERE plan_no="+plan_no+" AND client_no="+client_no).toInteger()

			if(bill_interval == 1)
			{
				logi.logInfo("Monthly plan")
				lag_days = db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name= 'MONTHLY_BILL_LAG_DAYS' and client_no="+client_no).toInteger()
			}
			else if(bill_interval == 3)
			{
				logi.logInfo("Quartely plan")
				lag_days = db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name= 'QUARTERLY_BILL_LAG_DAYS' and client_no="+client_no).toInteger()
			}
			else if(bill_interval == 6)
			{
				logi.logInfo("Semi annulaly plan")
				lag_days = db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name= 'SEMI_ANNUAL_BILL_LAG_DAYS' and client_no="+client_no).toInteger()
			}
			logi.logInfo("bill lag_days:"+lag_days)
			logi.logInfo("acct_created_date:"+acct_created_date.toString())
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")
			Calendar now = Calendar.getInstance();
			now.setTime(acct_created_date);
			now.add(Calendar.MONTH,acct_months_hm.get(acct_no+"-"+plan_no).toString().toInteger());
			now.add(Calendar.DAY_OF_MONTH,lag_days)
			String accu_valid_str = sdf.format(now.getTime())
			logi.logInfo("acc_valid_date in String :"+accu_valid_str)
			Date accu_valid_date = sdf.parse(accu_valid_str)
			logi.logInfo("acc_valid_date :"+accu_valid_date.toString())
			String usage_date_qry = "select usage_date from ariacore.usage where usage_type="+usagetype+ "and exclude_ind=0 and acct_no="+acct_no+ "and usage_units="+usage_units

			ResultSet rs = db.executePlaQuery(usage_date_qry)
			while(rs.next())
			{
				usg_date = rs.getDate(1)
			}

			logi.logInfo("usg date :"+usg_date.toString())

			String vt_qry = "Select ARIACORE.ARIAVIRTUALTIME("+client_no+") from DUAL"
			ResultSet rs_vt_qry = db.executePlaQuery(vt_qry)

			while(rs_vt_qry.next())
			{
				virtual_date=rs_vt_qry.getDate(1)
			}

			if(virtual_date <= accu_valid_date)
			{
				Date start_date = acct_created_date
				Date end_date = accu_valid_date+1
				logi.logInfo("start_date:"+start_date.toString()+" and End date:"+end_date.toString())
				String ser_nos = service_nos.toString().replace("[", "")
				ser_nos = ser_nos.replace("]", "")
				String threshold = db.executeQueryP2("Select unbillable_usg_threshold from ariacore.plan_services where service_no IN ("+ser_nos+") and plan_no="+plan_no+" and client_no="+client_no).toString()


				if(!threshold.equalsIgnoreCase("null"))
				{
					logi.logInfo("usage recorded for Adv billing Threshold enabled service")
					int inv_cnt=db.executeQueryP2("select count(distinct invoice_no) from ariacore.usage where usage_type="+usagetype+" and exclude_ind=0 and acct_no="+acct_no+" and  TO_CHAR(usage_date,'yyyy-MM-dd') BETWEEN '"+start_date+"' and '"+end_date+"'").toString().toInteger()
					if(inv_cnt >= 0)
					{
						double tot_units = db.executeQueryP2("select NVL(sum(usage_units),0) from ariacore.usage where usage_type="+usagetype+" and exclude_ind=0 and acct_no="+acct_no+" and  TO_CHAR(usage_date,'yyyy-MM-dd') BETWEEN '"+start_date+"' and '"+end_date+"'").toString().toDouble()
						already_recorded_units = (tot_units - (inv_cnt * threshold.toDouble())).toDouble()
					}
					if(already_recorded_units < 0)
					{
						already_recorded_units =0
					}

				}
				else
				{
					already_recorded_units=db.executeQueryP2("select NVL(sum(billable_units),0) from ariacore.usage where usage_type="+usagetype+" and exclude_ind=0 and acct_no="+acct_no+" and  TO_CHAR(usage_date,'yyyy-MM-dd') BETWEEN '"+start_date+"' and '"+end_date+"'").toString().toDouble()
				}

			}
			else if(virtual_date > accu_valid_date)
			{
				logi.logInfo("Accumalation has been Reset")
				Date start_date = accu_valid_date+1
				now.setTime(accu_valid_date);
				now.add(Calendar.MONTH,acc_months.toInteger());
				accu_valid_str = sdf.format(now.getTime())
				accu_valid_date = sdf.parse(accu_valid_str)
				Date end_date = accu_valid_date+1
				logi.logInfo("start_date:"+start_date.toString())
				logi.logInfo("End date:"+end_date.toString())

				String ser_nos = service_nos.toString().replace("[", "")
				ser_nos = ser_nos.replace("]", "")
				String threshold = db.executeQueryP2("Select unbillable_usg_threshold from ariacore.plan_services where service_no IN ("+ser_nos+") and plan_no="+plan_no+" and client_no="+client_no).toString()

				if(!threshold.equalsIgnoreCase("null"))
				{
					logi.logInfo("usage recorded for Adv billing Threshold enabled service")
					int inv_cnt=db.executeQueryP2("select count(distinct invoice_no) from ariacore.usage where usage_type="+usagetype+" and exclude_ind=0 and acct_no="+acct_no+" and  TO_CHAR(usage_date,'yyyy-MM-dd') BETWEEN '"+start_date+"' and '"+end_date+"'").toString().toInteger()
					if(inv_cnt >= 0)
					{
						double tot_units = db.executeQueryP2("select NVL(sum(usage_units),0) from ariacore.usage where usage_type="+usagetype+" and exclude_ind=0 and acct_no="+acct_no+" and  TO_CHAR(usage_date,'yyyy-MM-dd') BETWEEN '"+start_date+"' and '"+end_date+"'").toString().toDouble()
						already_recorded_units = (tot_units - (inv_cnt * threshold.toDouble())).toDouble()
					}
					if(already_recorded_units < 0)
					{
						already_recorded_units =0
					}
				}
				else
				{
					already_recorded_units=db.executeQueryP2("select NVL(sum(billable_units),0) from ariacore.usage where usage_type="+usagetype+" and exclude_ind=0 and acct_no="+acct_no+" and  TO_CHAR(usage_date,'yyyy-MM-dd') BETWEEN '"+start_date+"' and '"+end_date+"'").toString().toDouble()
				}

			}


		}
		else
		{
			String threshold
			String ser_nos = service_nos.toString().replace("[", "")
			ser_nos = ser_nos.replace("]", "")
			if(!ser_nos.equals(""))
				threshold = db.executeQueryP2("Select unbillable_usg_threshold from ariacore.plan_services where service_no IN ("+ser_nos+") and plan_no="+plan_no+" and client_no="+client_no).toString()
			else
				threshold = "null"

			if(!threshold.equalsIgnoreCase("null"))
			{
				logi.logInfo("usage recorded for Adv billing Threshold enabled service")
				int inv_cnt=db.executeQueryP2("select count(distinct invoice_no) from ariacore.usage where usage_type="+usagetype+" and exclude_ind=0 and acct_no="+acct_no+" and (invoice_no IS NULL or invoice_no=(Select max(invoice_no) FROM ARIACORE.GL Where ACCT_NO ="+acct_no+" and client_no="+client_no+"))").toString().toInteger()
				if(inv_cnt >= 0)
				{
					double tot_units = db.executeQueryP2("select NVL(sum(usage_units),0) from ariacore.usage where usage_type="+usagetype+" and exclude_ind=0 and acct_no="+acct_no+" and (invoice_no IS NULL or invoice_no=(Select max(invoice_no) FROM ARIACORE.GL Where ACCT_NO ="+acct_no+" and client_no="+client_no+"))").toString().toDouble()
					already_recorded_units = (tot_units - (inv_cnt * threshold.toDouble())).toDouble()
					if(already_recorded_units < 0)
					{
						already_recorded_units =0
					}
				}

			}
			else
			{
				already_recorded_units=db.executeQueryP2("SELECT CASE WHEN TOTAL_UNITS = 0 THEN (SELECT NVL(SUM(billable_units),0) AS TOTAL_UNITS FROM ariacore.usage WHERE usage_type ="+usagetype+" AND exclude_ind =0 AND CLIENT_RATED=0 AND acct_no="+acct_no+" AND (USAGE_DATE > (Select Case when bill_DATE is null then (Select bill_date from ariacore.gl WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+" and invoice_no in (select max(invoice_no) from ariacore.gl where acct_no ="+acct_no+" and invoice_no < (select max(invoice_no) from ariacore.gl where acct_no ="+acct_no+" ))) else bill_DATE end from (SELECT MAX(bill_DATE) as bill_DATE FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+" AND bill_DATE < (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+" ) AND INVOICE_NO IN (SELECT INVOICE_NO FROM ARIACORE.GL_DETAIL WHERE client_no ="+client_no+" ))) AND USAGE_DATE < (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+" )) ) ELSE TOTAL_UNITS END AS FINAL_TOTAL_UNITS FROM (SELECT NVL(SUM(billable_units),0) AS TOTAL_UNITS FROM ariacore.usage WHERE usage_type ="+usagetype+" AND exclude_ind =0 AND CLIENT_RATED=0 AND acct_no="+acct_no+" AND (USAGE_DATE > (SELECT MAX(bill_DATE) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+" )) )").toString().toDouble()
			}

		}
		for(int flg=0;flg<service_nos.size();flg++)
		{
			price_rule=md_get_tier_pricing_type(service_nos.get(flg),acct_no,usagetype,plan_no)

			ArrayList<String> rate_amt = []
			hm_unit_rates = GET_PRICE_TIER(acct_no,service_nos.get(flg),plan_no)
			if(price_rule.equals("1"))
			{

				double rem_units=input_units;
				double sum=0.0;
				double unit_rate;

				logi.logInfo "Rate tier hash from DB standard"+hm_unit_rates.toString()
				List<String> keyList = new ArrayList<String>(hm_unit_rates.keySet());
				List<String> valList = new ArrayList<String>(hm_unit_rates.values());
				double lb;
				if(hm_unit_rates.size()==1)
				{
					sum=input_units*(hm_unit_rates.get("UBN").toDouble())
				}

				else
				{
					for(int i=0;i<keyList.size();)
					{
						if(rem_units>0.0)
						{
							int j=i-1;
							double ub;

							double max_units;

							if(j<0)
							{
								logi.logInfo "already_recorded_units now"+already_recorded_units
								lb=already_recorded_units-usage_units.toDouble();
								logi.logInfo "new lb"+lb
								//lb=0
							}
							else
							{
								lb=(keyList.get(j).split("UB")[1]).toDouble()
							}

							if(keyList.get(i).equals("UBN"))
							{
								max_units=rem_units;
							}

							else
							{
								ub=(keyList.get(i).split("UB")[1]).toDouble()
								max_units=ub-lb;
							}



							logi.logInfo("max units"+max_units);
							logi.logInfo("current i"+i);

							if(max_units<0.0)
							{	   logi.logInfo("max:"+max_units)
								logi.logInfo "max units less than 0"
								while(max_units<0.0)
								{
									logi.logInfo "inside while"
									if(i<keyList.size())
									{
										logi.logInfo("i value:"+i)
										logi.logInfo("keyList.get(i) :"+keyList.get(i).toString())
										if(keyList.get(i).equals("UBN"))
										{
											max_units=rem_units;
										}
										else
										{

											if(keyList.get(i+1).equals("UBN"))
											{
												max_units=rem_units;
											}
											else
											{
												ub=(keyList.get(i+1).split("UB")[1]).toDouble()
												max_units=ub-lb;
											}

										}
										i++

									}
									else
									{
										i=i-1
									}
								}
							}

							if(rem_units>=max_units)
							{
								rem_units=rem_units-max_units;
								unit_rate=max_units*(valList.get(i).toDouble());
								sum=sum+unit_rate;
								logi.logInfo("sum"+sum);
								logi.logInfo("rem_units"+rem_units);
							}

							else
							{
								unit_rate=rem_units*(valList.get(i).toDouble());
								sum=sum+unit_rate;
								logi.logInfo("else sum"+sum);
								break;
							}

							i++
						}
						else
						{
							break;
						}
					}
				}
				logi.logInfo("total usgae rate"+sum);
				logi.logInfo("rate per unit"+(sum/input_units));

				rate_amt.add((sum/input_units).toDouble().round(rate_rnd).toString())
				rate_amt.add(sum.toString())
				usage_hm.put(service_nos.get(flg), rate_amt)
				logi.logInfo("standard:"+usage_hm.toString())
				logi.logInfo "lb now"+lb


			}
			else if(price_rule.equals("2"))
			{
				int sum=0;

				logi.logInfo "Rate tier hash from DB volume discount"+hm_unit_rates.toString()
				List<String> keyList = new ArrayList<String>(hm_unit_rates.keySet());
				List<String> valList = new ArrayList<String>(hm_unit_rates.values());
				if(input_units > 0)
				{
					if(hm_unit_rates.size()==1)
					{
						sum=input_units*(hm_unit_rates.get("UBN").toInteger())
					}
					else
					{
						for(int i=0;i<keyList.size();i++)
						{
							if(keyList.get(i)!="UBN")
							{
								int max_units = (keyList.get(i).split("UB")[1]).toInteger()
								logi.logInfo("vd max units"+max_units)
								if(input_units<=max_units)
								{
									sum=input_units*valList.get(i).toInteger()
									break;
								}
							}

							else
							{
								sum=input_units*valList.get(i).toInteger()
								break;
							}

						}
					}
				}

				logi.logInfo("total usgae rate"+sum);
				logi.logInfo("rate per unit"+(sum/input_units));


				rate_amt.add((sum/input_units).toDouble().round(rate_rnd).toString())
				rate_amt.add(sum.toString())
				usage_hm.put(service_nos.get(flg), rate_amt)
				logi.logInfo("Volume Discount:"+usage_hm.toString())

			}

			else if(price_rule.equals("3"))
			{
				int sum;

				logi.logInfo "Rate tier hash from DB flat"+hm_unit_rates.toString()
				List<String> keyList = new ArrayList<String>(hm_unit_rates.keySet());
				List<String> valList = new ArrayList<String>(hm_unit_rates.values());
				if(input_units > 0)
				{
					if(hm_unit_rates.size()==1)
					{
						sum=1*(hm_unit_rates.get("UBN").toInteger())

					}
					else
					{
						for(int i=0;i<keyList.size();i++)
						{
							if(keyList.get(i)!="UBN")
							{
								int max_units = (keyList.get(i).split("UB")[1]).toInteger()

								if(input_units<=max_units)
								{

									sum=(1*valList.get(i).toString().toInteger())
									break
								}
							}

							else
							{
								sum=1*valList.get(i).toInteger()
								break;
							}
						}
					}
				}
				logi.logInfo("total usgae rate"+sum);
				logi.logInfo("rate per unit"+1);


				rate_amt.add("1")
				rate_amt.add(sum.toString())
				usage_hm.put(service_nos.get(flg), rate_amt)
				logi.logInfo("Flat:"+usage_hm.toString())
			}
		}
		return usage_hm
	}
	/**
	 * Getting the expected invoice date for the account
	 * @param tcid
	 * @return String 
	 */
	public String md_GET_EXP_INVOICE_DATE(String tcid)
	{
		logi.logInfo("md_GET_EXP_INVOICE_DATE")
		String acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		String plan_no = getUsedPlanNoP2(acct_no,"")
		String bill_int_qry ="SELECT distinct billing_interval FROM ariacore.client_plan WHERE plan_no="+plan_no+" AND client_no="+client_no
		ConnectDB db = new ConnectDB()
		int interval = db.executeQueryP2(bill_int_qry).toInteger()
		String lag_days_qry
		int lag_days
		Date acct_created_date
		logi.logInfo("billing interval is:"+interval)

		String created_date_qry = "select created from ariacore.acct where acct_no="+acct_no
		ResultSet rs_created_date_qry = db.executePlaQuery(created_date_qry)
		while(rs_created_date_qry.next())
		{
			acct_created_date = rs_created_date_qry.getDate(1)
		}

		logi.logInfo("acct_created_date:"+acct_created_date.toString())

		if(interval == 1)
		{
			logi.logInfo("Monthly plan")
			lag_days_qry = "Select param_val from ariacore.all_client_params where param_name= 'MONTHLY_BILL_LAG_DAYS' and client_no="+client_no
			lag_days = db.executeQueryP2(lag_days_qry).toInteger()
		}
		else if(interval == 3)
		{
			logi.logInfo("Quartely plan")
			lag_days_qry = "Select param_val from ariacore.all_client_params where param_name= 'QUARTERLY_BILL_LAG_DAYS' and client_no="+client_no
			lag_days = db.executeQueryP2(lag_days_qry).toInteger()
		}
		else if(interval == 6)
		{
			logi.logInfo("Semi annulaly plan")
			lag_days_qry = "Select param_val from ariacore.all_client_params where param_name= 'SEMI_ANNUAL_BILL_LAG_DAYS' and client_no="+client_no
			lag_days = db.executeQueryP2(lag_days_qry).toInteger()
		}

		else
		{
			logi.logInfo("custom cycle plan")
		}

		logi.logInfo("bill lag_days:"+lag_days)

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy")
		Calendar now = Calendar.getInstance();
		now.setTime(acct_created_date);
		now.add(Calendar.MONTH,interval);
		logi.logInfo("after adding billing interval:"+sdf.format(now.getTime()).toString())
		now.add(Calendar.DAY_OF_MONTH,lag_days)
		logi.logInfo("after adding lag days:"+sdf.format(now.getTime()).toString())
		String exp_inv_date = sdf.format(now.getTime()).toString()
		return exp_inv_date.toUpperCase()
	}

	public String md_TOTAL_BILLED_UNITS_EXP(String tcid)
	{
		String acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		return md_TOTAL_BILLED_UNITS_EXP(tcid,acct_no)
	}

	public String md_CHILD_TOTAL_BILLED_UNITS_EXP(String tcid)
	{
		String acct_no = getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		return md_TOTAL_BILLED_UNITS_EXP(tcid,acct_no)
	}

	public String md_TOTAL_BILLED_UNITS_RATE_EXP(String tcid)
	{
		String acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		return md_TOTAL_BILLED_UNITS_RATE_EXP(tcid,acct_no)
	}

	public String md_CHILD_TOTAL_BILLED_UNITS_RATE_EXP(String tcid)
	{
		String acct_no = getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		return md_TOTAL_BILLED_UNITS_RATE_EXP(tcid,acct_no)
	}

	public String md_TOTAL_BILLED_UNITS_AMT_EXP(String tcid)
	{
		String acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		return md_TOTAL_BILLED_UNITS_AMT_EXP(tcid,acct_no)
	}

	public String md_CHILD_TOTAL_BILLED_UNITS_AMT_EXP(String tcid)
	{
		String acct_no = getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		return md_TOTAL_BILLED_UNITS_AMT_EXP(tcid,acct_no)
	}
	/**
	 * Getting the total billed units of the account
	 * @param tcid
	 * @param acct_no
	 * @return String
	 */
	public String md_TOTAL_BILLED_UNITS_EXP(String tcid,String acct_no)
	{
		logi.logInfo("md_TOTAL_BILLED_UNITS_EXP")

		ConnectDB db = new ConnectDB()
		String usg_types_qry = "Select distinct usage_type from ariacore.usage where acct_no ="+acct_no+" and ctrl_id = (select max(ctrl_id) from ariacore.usage where acct_no="+acct_no+")"
		ResultSet rs_usg_types_qry = db.executePlaQuery(usg_types_qry)

		List<String> usg_list = []
		double units_sum = 0.0
		double units
		DecimalFormat d = new DecimalFormat("#.##########");
		while(rs_usg_types_qry.next())
		{
			usg_list.add(rs_usg_types_qry.getString(1))
		}

		for(usage in usg_list)
		{
			List ser_list=md_GET_USAGE_SERVICE_NO(acct_no.toString(),usage)
			logi.logInfo("Services no for the usgae type "+ser_list)
			for(service in ser_list)
			{
				String pricing_rule=md_get_tier_pricing_type(service,acct_no,usage)
				if(pricing_rule == '3')
				{
					String rate_null_qry = db.executeQueryP2("Select rate from ariacore.usage where acct_no ="+acct_no+" and usage_type="+usage+" and exclude_ind=0 and ctrl_id = (select max(ctrl_id) from ariacore.usage where acct_no ="+acct_no+")").toString()
					if(rate_null_qry == "null")
					{
						String units_qry ="Select NVL(sum(billable_units),0) from ariacore.usage where acct_no ="+acct_no+" and usage_type="+usage+" and exclude_ind=0 and ctrl_id = (select max(ctrl_id) from ariacore.usage where acct_no ="+acct_no+")"
						units = db.executeQueryP2(units_qry).toDouble()

						if(units != 0)
							units = 1
					}
					else
					{
						String units_qry ="Select NVL(sum(billable_units),0) from ariacore.usage where acct_no ="+acct_no+" and usage_type="+usage+" and exclude_ind=0 and ctrl_id = (select max(ctrl_id) from ariacore.usage where acct_no ="+acct_no+")"
						units = db.executeQueryP2(units_qry).toDouble()
					}

				}
				else
				{
					String units_qry ="Select NVL(sum(billable_units),0) from ariacore.usage where acct_no ="+acct_no+" and usage_type="+usage+" and exclude_ind=0 and ctrl_id = (select max(ctrl_id) from ariacore.usage where acct_no ="+acct_no+")"
					units = db.executeQueryP2(units_qry).toDouble()
				}
				units_sum = units_sum + units
			}

			logi.logInfo("UNITS_AMT: total units consumed for the usage type"+usage+" is:"+d.format(units_sum).toString())
		}

		return d.format(units_sum).toString()

	}
	/**
	 * Getting the total rate of the billed units
	 * @param tcid
	 * @param acct_no
	 * @return String
	 */
	public String md_TOTAL_BILLED_UNITS_RATE_EXP(String tcid,String acct_no)
	{
		logi.logInfo("md_TOTAL_BILLED_UNITS_RATE_EXP")
		ConnectDB db = new ConnectDB()
		String usg_types_qry = "Select distinct usage_type from ariacore.usage where acct_no ="+acct_no+" and exclude_ind=0 and ctrl_id = (select max(ctrl_id) from ariacore.usage where acct_no="+acct_no+")"
		ResultSet rs_usg_types_qry = db.executePlaQuery(usg_types_qry)
		HashMap<String, ArrayList<String>> usg_rateamt_hm = new HashMap<String, ArrayList<String>>()
		List<String> usg_list = []
		double rate_sum = 0.0
		double rate
		DecimalFormat d = new DecimalFormat("#.##########");
		while(rs_usg_types_qry.next())
		{
			usg_list.add(rs_usg_types_qry.getString(1))
		}

		for(usage in usg_list)
		{
			List ser_list=md_GET_USAGE_SERVICE_NO(acct_no.toString(),usage)
			logi.logInfo("Services no for the usgae type "+ser_list)
			for(service in ser_list)
			{
				String pricing_rule=md_get_tier_pricing_type(service,acct_no,usage)
				String rate_null_qry = db.executeQueryP2("Select rate from ariacore.usage where acct_no ="+acct_no+" and usage_type="+usage+" and exclude_ind=0 and ctrl_id = (select max(ctrl_id) from ariacore.usage where acct_no ="+acct_no+")").toString()
				String bill_units = db.executeQueryP2("Select billable_units from ariacore.usage where acct_no ="+acct_no+" and usage_type="+usage+" and exclude_ind=0 and ctrl_id = (select max(ctrl_id) from ariacore.usage where acct_no ="+acct_no+")").toString()

				if(pricing_rule == '3' && rate_null_qry.equalsIgnoreCase("null"))
				{
					logi.logInfo("price rule 3 nd rate null")
					usg_rateamt_hm = md_USAGE_AMT_RATE_EXP(acct_no.toString(),ser_list,bill_units,usage.toString())
					rate = usg_rateamt_hm.get(service)[0].toString().toDouble()
				}

				else if(pricing_rule == '2' && rate_null_qry.equalsIgnoreCase("null"))
				{
					logi.logInfo("price rule 2 nd rate null")
					usg_rateamt_hm = md_USAGE_AMT_RATE_EXP(acct_no.toString(),ser_list,bill_units,usage.toString())
					rate = usg_rateamt_hm.get(service)[0].toString().toDouble()
				}

				else
				{
					String rate_qry ="Select NVL(sum(rate),0) from ariacore.usage where acct_no ="+acct_no+" and usage_type="+usage+" and exclude_ind=0  and ctrl_id = (select max(ctrl_id) from ariacore.usage where acct_no ="+acct_no+")"
					rate = db.executeQueryP2(rate_qry).toDouble()
				}
				rate_sum = rate_sum + rate
			}

			logi.logInfo("BILLED_UNITS_RATE: total units consumed for the usage type"+usage+" is:"+d.format(rate_sum).toString())
		}

		return d.format(rate_sum).toString()

	}
	/**
	* Getting the total amount of the billed units
	* @param tcid
	* @param acct_no
	* @return String
	*/
	public String md_TOTAL_BILLED_UNITS_AMT_EXP(String tcid,String acct_no)
	{
		logi.logInfo("md_TOTAL_BILLED_UNITS_AMT_EXP")
		ConnectDB db = new ConnectDB()
		String usg_types_qry = "Select distinct usage_type from ariacore.usage where acct_no ="+acct_no+" and exclude_ind=0 and ctrl_id = (select max(ctrl_id) from ariacore.usage where acct_no="+acct_no+")"
		ResultSet rs_usg_types_qry = db.executePlaQuery(usg_types_qry)
		HashMap<String, ArrayList<String>> usg_rateamt_hm = new HashMap<String, ArrayList<String>>()
		List<String> usg_list = []
		double amt_sum = 0.0
		double amt
		DecimalFormat d = new DecimalFormat("#.##########");
		while(rs_usg_types_qry.next())
		{
			usg_list.add(rs_usg_types_qry.getString(1))
		}

		for(usage in usg_list)
		{
			List ser_list=md_GET_USAGE_SERVICE_NO(acct_no.toString(),usage)
			logi.logInfo("Services no for the usgae type "+ser_list)
			for(service in ser_list)
			{
				String pricing_rule=md_get_tier_pricing_type(service,acct_no,usage)
				String rate_null_qry = db.executeQueryP2("Select rate from ariacore.usage where acct_no ="+acct_no+" and usage_type="+usage+" and exclude_ind=0 and ctrl_id = (select max(ctrl_id) from ariacore.usage where acct_no ="+acct_no+")").toString()
				String bill_units = db.executeQueryP2("Select billable_units from ariacore.usage where acct_no ="+acct_no+" and usage_type="+usage+" and exclude_ind=0 and ctrl_id = (select max(ctrl_id) from ariacore.usage where acct_no ="+acct_no+")").toString()

				if(pricing_rule == '3' && rate_null_qry.equalsIgnoreCase("null"))
				{
					logi.logInfo("price rule 3 nd rate null")
					usg_rateamt_hm = md_USAGE_AMT_RATE_EXP(acct_no.toString(),ser_list,bill_units,usage.toString())
					amt = usg_rateamt_hm.get(service)[1].toString().toDouble()
				}

				else if(pricing_rule == '2' && rate_null_qry.equalsIgnoreCase("null"))
				{
					logi.logInfo("price rule 2 nd rate null")
					usg_rateamt_hm = md_USAGE_AMT_RATE_EXP(acct_no.toString(),ser_list,bill_units,usage.toString())
					amt = usg_rateamt_hm.get(service)[1].toString().toDouble()
				}

				else
				{
					String amt_qry ="Select NVL(sum(amt),0) from ariacore.usage where acct_no ="+acct_no+" and usage_type="+usage+" and ctrl_id = (select max(ctrl_id) from ariacore.usage where acct_no ="+acct_no+")"
					amt = db.executeQueryP2(amt_qry).toDouble()
				}
				amt_sum = amt_sum + amt
			}

			logi.logInfo("UNITS_AMT: total units consumed for the usage type"+usage+" is:"+d.format(amt_sum).toString())
		}

		return d.format(amt_sum).toString()

	}
	/**
	* Getting the plan end date
	* @param tcid
	* @param acct_no
	* @return Date
	*/
	public Date subractMonthsFromGivenDate(Date dateGiven, int noOfMonths)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateGiven);
		cal.add(Calendar.MONTH,noOfMonths*-1)
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		String dateMonthsAdded = format1.format(cal.getTime());
		Date planEndDate = format1.parse(dateMonthsAdded)
		return planEndDate
	}
	/**
	 * Getting the cancelled supplemental plan credit amount
	 * @param tcid
	 * @return String
	 */
	public String md_CANCEL_SUPP_PLAN_CREDIT_AMOUNT(String tcid)
	{
		String apiname=tcid.split("-")[2]
		String supp_plan_no = getValueFromRequest(apiname,"//supp_plan_no")
		return md_SUPP_PLAN_CREDIT_AMOUNT(tcid,supp_plan_no)
	}
	/**
	 * Getting the replaced supplemental plan credit amount
	 * @param tcid
	 * @return String 
	 */
	public String md_REPLACE_SUPP_PLAN_CREDIT_AMOUNT(String tcid)
	{
		String apiname=tcid.split("-")[2]
		String supp_plan_no = getValueFromRequest(apiname,"//existing_supp_plan_no")
		return md_SUPP_PLAN_CREDIT_AMOUNT(tcid,supp_plan_no)
	}
	/**
	 * Calculating the supplemental plan credit amount
	 * @param tcid
	 * @return String
	 */
	public String md_SUPP_PLAN_CREDIT_AMOUNT(String tcid,String supp_plan_no )
	{
		logi.logInfo("inside md_SUPP_PLAN_CREDIT_AMOUNT")
		String apiname=tcid.split("-")[2]
		String acct_no = getValueFromRequest(apiname,"//acct_no")
		String client_no = getValueFromRequest(apiname,"//client_no")
		Date assigned_date
		Date unassigned_date
		ConnectDB db = new ConnectDB()
		
		String date_qry = "select trunc(create_date),trunc(terminate_date) from ariacore.acct_supp_plan_map_view where acct_no="+acct_no+" and supp_plan_no="+supp_plan_no
		ResultSet rs_date_qry = db.executePlaQuery(date_qry)
		while(rs_date_qry.next())
		{
			assigned_date = rs_date_qry.getDate(1)
			unassigned_date = rs_date_qry.getDate(2)
		}

		logi.logInfo("supp plan assigned date:"+assigned_date.toString())
		logi.logInfo("supp plan unassigned date:"+unassigned_date.toString())

		int used_days= getDaysBetween(assigned_date,unassigned_date)
		logi.logInfo("supp plan used days:"+used_days)

		String bill_int_qry ="SELECT distinct billing_interval FROM ariacore.client_plan WHERE plan_no="+supp_plan_no+" AND client_no="+client_no
		int billing_int = db.executeQueryP2(bill_int_qry).toInteger()
		logi.logInfo("supp plan billing interval"+billing_int)
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		Calendar now = Calendar.getInstance();
		now.setTime(assigned_date);
		now.add(Calendar.MONTH,billing_int);
		String tem = now.getTime().toString()
		String tem1 = sdf.format(now.getTime()).toString()
		logi.logInfo("after adding billing interval:"+tem1) 
		
		Date next_bill_date =(Date) sdf.parse(tem1)
		logi.logInfo("next bill date:"+next_bill_date)
		int billing_days = getDaysBetween(assigned_date,next_bill_date)

		logi.logInfo("supp plan billing cycle days:"+billing_days)

		double proration_factor = ((billing_days-used_days)/billing_days).toDouble().round(10)

		logi.logInfo("supp plan proration factor:"+proration_factor.toString())

		String amt_qry = "Select NVL(SUM(debit),0) from ariacore.gl_detail where invoice_no=(SELECT invoice_no FROM (SELECT invoice_no,row_number() over (order by bill_date DESC) AS s_no FROM ariacore.gl WHERE acct_no="+acct_no+") WHERE s_no=1) and plan_no="+supp_plan_no
		String amt = db.executeQueryP2(amt_qry).toString()
		logi.logInfo("supp plan amt:"+amt)
		
		return (proration_factor*amt.toDouble()).round(2).toString()
	}
	/**
	 * Getting cancelled supplemental plan status code
	 * @param tcid
	 * @return String
	 */
	public String md_CANCEL_SUPP_PLAN_STATUS_CD(String tcid)
	{
		String apiname=tcid.split("-")[2]
		String supp_plan_no = getValueFromRequest(apiname,"//supp_plan_no")
		return SUPP_PLAN_STATUS_CD(tcid,supp_plan_no)
	}
	/**
	 * Getting replaced supplemental plan status code
	 * @param tcid
	 * @return String
	 */
	public String md_EXISTING_SUPP_PLAN_STATUS_CD(String tcid)
	{
		String apiname=tcid.split("-")[2]
		String supp_plan_no = getValueFromRequest("replace_supp_plan","//existing_supp_plan_no")
		return SUPP_PLAN_STATUS_CD(tcid,supp_plan_no)
	}
	/**
	 * Getting assigned supplemental plan status code
	 * @param tcid
	 * @return String
	 */
	public String md_REPLACED_SUPP_PLAN_STATUS_CD(String tcid)
	{
		String apiname=tcid.split("-")[2]
		String supp_plan_no = getValueFromRequest("replace_supp_plan","//new_supp_plan_no")
		return SUPP_PLAN_STATUS_CD(tcid,supp_plan_no)
	}
	/**
	 * Getting the supplemental plan status code
	 * @param tcid
	 * @param supp_plan_no
	 * @return String
	 */
	public String SUPP_PLAN_STATUS_CD(String tcid,String supp_plan_no)
	{
		logi.logInfo("inside SUPP_PLAN_STATUS_CD ")
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String plan_status_cd="select status_cd from ariacore.acct_supp_plan_map_view where acct_no="+acct_no+" and supp_plan_no="+supp_plan_no+" and client_no="+client_no
		String status_cd=null
		ConnectDB database = new ConnectDB()
		
		ResultSet resultSet = database.executeQuery(plan_status_cd);
		while (resultSet.next()){
			status_cd=resultSet.getString(1)
		}
		
		resultSet.close()
		return status_cd
	}
	/**
	 * Getting the replaced supplemental plan proration amount
	 * @param tcid
	 * @return String
	 */
	public String md_REPLACE_SUPP_PLAN_PRORATION_RESULT_AMOUNT(String tcid)
	{
		logi.logInfo("inside md_REPLACE_SUPP_PLAN_PRORATION_RESULT_AMOUNT")
		DecimalFormat d = new DecimalFormat("#.##########");
		String credit_amt = md_REPLACE_SUPP_PLAN_CREDIT_AMOUNT(tcid)
		
		logi.logInfo("credit_amt is:"+credit_amt)
		String proration_amt = md_proration_result_amount(tcid)
		logi.logInfo("proration_amt is:"+proration_amt)
		
		String cal_amt = d.format((credit_amt.toDouble() +  proration_amt.toDouble()).toDouble().round(2)).toString()
		return cal_amt
	}
	/**
	 * Getting the update master plan credit amount
	 * @param tcid
	 * @return String
	 */
	public String md_UPDATE_MASTER_PLAN_CREDIT_AMOUNT(String tcid)
	{
		logi.logInfo("inside md_UPDATE_MASTER_PLAN_CREDIT_AMOUNT")
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=tcid.split("-")[2]
		//String acct_no = getValueFromRequest(apiname,"//acct_no")
		String acct_no = getValueFromResponse("create_acct_complete","//acct_no")
		//String client_no = getValueFromRequest(apiname,"//client_no")
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		String credit_amt
		Date assigned_date
		Date unassigned_date
		ConnectDB db = new ConnectDB()
		
		String plan_no = db.executeQueryP2("SELECT old_plan FROM ariacore.acct_plan_hist WHERE acct_no ="+acct_no+" AND old_plan !=0").toString()
		logi.logInfo("old plan no:"+plan_no)
		String date_qry = "select trunc(from_date),trunc(to_date) FROM ariacore.acct_plan_hist WHERE new_plan="+plan_no+" and acct_no ="+acct_no
		ResultSet rs_date_qry = db.executePlaQuery(date_qry)
		while(rs_date_qry.next())
		{
			assigned_date = rs_date_qry.getDate(1)
			unassigned_date = rs_date_qry.getDate(2)
		}

		logi.logInfo("plan assigned date:"+assigned_date.toString())
		logi.logInfo("plan plan unassigned date:"+unassigned_date.toString())

		int inv_count = db.executeQueryP2("SELECT count(invoice_no) FROM ariacore.gl  WHERE acct_no="+acct_no+" and plan_no="+plan_no).toString().toInteger()
		if(!inv_count.equals("0"))
		{
			if(inv_count > 1)
			{
				logi.logInfo " Inside inv_count "
				String assigned_date_qry = "SELECT trunc(due_date) FROM ariacore.gl WHERE acct_no="+acct_no+" and plan_no="+plan_no+" and invoice_no=(SELECT max(invoice_no) FROM ariacore.gl WHERE acct_no="+acct_no+" and plan_no="+plan_no+")"
				ResultSet rs= db.executePlaQuery(assigned_date_qry)
				while(rs.next())
				{
					assigned_date = rs.getDate(1)
				}
				logi.logInfo("new assigned date"+assigned_date.toString())
			}
			int used_days= getDaysBetween(assigned_date,unassigned_date)+1
			logi.logInfo("plan used days:"+used_days)

			String bill_int_qry ="SELECT distinct billing_interval FROM ariacore.client_plan WHERE plan_no="+plan_no+" AND client_no="+client_no
			int billing_int = db.executeQueryP2(bill_int_qry).toInteger()
			logi.logInfo("plan billing interval"+billing_int)
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
			
			Calendar now = Calendar.getInstance();
			now.setTime(assigned_date);
			now.add(Calendar.MONTH,billing_int);
			String tem1 = sdf.format(now.getTime()).toString()
			logi.logInfo("after adding billing interval:"+tem1) //now.getTime()).toString())
			Date next_bill_date =(Date) sdf.parse(tem1)
			logi.logInfo("next bill date:"+next_bill_date)
			int billing_days = getDaysBetween(assigned_date,next_bill_date)

			logi.logInfo("plan billing cycle days:"+billing_days)
			String amt = db.executeQueryP2("Select NVL(SUM(debit),0) from ariacore.gl_detail where invoice_no= (Select max(invoice_no) from ariacore.gl where acct_no="+acct_no+ "and plan_no="+plan_no+") and proration_factor IS NOT NULL").toString()
			logi.logInfo("total billing cycle plan amt:"+amt)
			String used_days_plan_amt = ((amt.toDouble()/billing_days.toDouble())*used_days.toDouble()).toString()
			logi.logInfo("used_days_plan_amt:"+used_days_plan_amt)
			credit_amt= d.format((amt.toDouble() - used_days_plan_amt.toDouble()).toDouble().round(2)).toString()
		}
		
		else
			credit_amt=0
		logi.logInfo("credit_amt:"+credit_amt)
		
		return credit_amt
	}
	/**
	 * Getting the master plan proration amount
	 * @param tcid
	 * @return String
	 */
	public String md_UPDATED_MASTER_PLAN_PRORATION_AMT_EXP(String tcid)
	{
		logi.logInfo("inside md_UPDATED_MASTER_PLAN_PRORATION_AMT_EXP")
		String apiname=tcid.split("-")[2]
		DecimalFormat d = new DecimalFormat("#.##########");
		String acct_no = getValueFromRequest(apiname,"//acct_no")
		String client_no = getValueFromRequest(apiname,"//client_no")
		String proration_amt
		ConnectDB db = new ConnectDB()
		
		String unbilled_flag =db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name = 'INVOICE_UNBILLED_USAGE_DURING_PRORATION' and client_no="+client_no)
		if(unbilled_flag.equalsIgnoreCase("TRUE"))
		{
			String exp_inv=  md_ExpInvoice_Amt(tcid,acct_no)
			String usg_amt = md_ExpUsage_Amt(tcid)
			logi.logInfo("exp_inv :"+exp_inv)
			logi.logInfo("usg_amt :"+usg_amt)
			proration_amt = d.format((exp_inv.toDouble()+usg_amt.toDouble()).toDouble().round(2)).toString()
		}
		else if(unbilled_flag.equalsIgnoreCase("FALSE"))
		{
			proration_amt = md_ExpInvoice_Amt(tcid,acct_no)
		}
		if(proration_amt.toDouble() < 1)
		{
			proration_amt ="."+proration_amt.split("\\.")[1]
			
		}
		logi.logInfo("proration_amt :"+proration_amt)
		return proration_amt
	}
	/**
	 * Getting the supplemental plan proration result amount
	 * @param tcid
	 * @return String
	 */
	public String md_SUPP_PLAN_PRORATION_RESULT_AMOUNT(String tcid)
	{
		DecimalFormat d = new DecimalFormat("#.##########");
		logi.logInfo("inside md_SUPP_PLAN_PRORATION_RESULT_AMOUNT")
		String credit_amt=md_proration_result_amount(tcid)
		logi.logInfo("supp credit_amt:"+credit_amt)
		
		String usg_amt = md_ExpUsage_Amt(tcid)
		logi.logInfo("supp usage amt:"+usg_amt)

		String proration_amt = d.format((usg_amt.toDouble()+credit_amt.toDouble()).toDouble().round(2)).toString()
		logi.logInfo("supp proration_amt :"+proration_amt)
		return proration_amt
	}
	/**
	 * Verifying that usage is billed based on backdate
	 * @param tcid
	 * @return boolean
	 */
	boolean md_verify_usage_billed_based_on_backdated_rates(String tcid){
		boolean successFlag = false
		ConnectDB db = new ConnectDB()

		String backDatedUsageQry =  ""
		String rateTierQry = ""
		String backDateFlag = db.executeQueryP2(backDatedUsageQry)
		logi.logInfo("Back dated usage flag is :: "+backDateFlag)

		String rateTier = db.executeQueryP2(rateTierQry)
		logi.logInfo("rateTier is :: "+rateTier)

		if(backDateFlag == "FALSE"){
			successFlag = true
		}

		return successFlag
	}
	/**
	 * Getting 'BACKDATE_USAGE_RATING' flag value for the client
	 * @return boolean
	 */
	public boolean get_Back_Dated_Usage_Flag(){
		boolean backDateFlag = false

		ConnectDB db = new ConnectDB()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')

		String backDateQry = "select PARAM_VAL from ariacore.aria_client_params where PARAM_NAME = 'BACKDATE_USAGE_RATING' AND client_no = "+clientNo
		String flag = db.executeQueryP2(backDateQry)
		if(flag.equals("TRUE")){
			backDateFlag = true
		}
		logi.logInfo "Auto rate unrated usage flag for the client : "+clientNo+" is : "+backDateFlag
		return backDateFlag
	}
	/**
	 * Getting the master plan proration amount when updating the master plan through update_acct_complete
	 * @param tcid
	 * @return String
	 */
	public String md_UPDATE_ACCT_COMPLETE_PRORATION_RESULT_AMT_EXP(String tcid)
	{
		logi.logInfo("inside md_UPDATE_ACCT_COMPLETE_PRORATION_RESULT_AMT_EXP")
		String result
		DecimalFormat d = new DecimalFormat("#.##########");
		String pro_amt = md_UPDATED_MASTER_PLAN_PRORATION_AMT_EXP(tcid)
		logi.logInfo("pro_amt:"+pro_amt)
		
		String credit_amt = md_UPDATE_MASTER_PLAN_CREDIT_AMOUNT(tcid)
		logi.logInfo("cre_amt:"+credit_amt)

		if(credit_amt.toDouble() > pro_amt.toDouble())
			result ="0"
		else
			result = d.format((pro_amt.toDouble() - credit_amt.toDouble()).toDouble().round(2)).toString()

		return result
	}
	/**
	 * Getting the expected usage amount based on Auto_Rate_Unrated flag
	 * @param testCaseId
	 * @return String
	 */
	public String md_ExpUsage_Amt_With_Auto_Rate(String testCaseId){
		logi.logInfo("calling md_ExpUsage_Amt_With_Auto_Rate")

		String acct_no = getValueFromResponse('create_acct_complete',"//acct_no")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		double usg = 0.0
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=testCaseId.split("-")[2]
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no).toString();
		logi.logInfo( "invoiceNo in md_ExpUsage_Amt method " +invoiceNo)
		HashMap<String, ArrayList<String>> usagetypeplans= new HashMap<String, ArrayList<String>>()
		HashMap<String, ArrayList<String>> usage_rateamt_hm = new HashMap<String, ArrayList<String>>()
		String unbilled_flag =db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name = 'INVOICE_UNBILLED_USAGE_DURING_PRORATION' and client_no="+clientNo)
		boolean auto_RateFlag = get_Auto_Rate_Unrated_Usage_Flag()

		logi.logInfo("recordedusageamounts - in md exp usage amt method "+Constant.recordedusageamounts.toString())
		if(unbilled_flag == "TRUE" )
		{
			logi.logInfo("Inside auto rate "+auto_RateFlag+" loop")
			int utcount=1;
			Constant.recordedusageamounts.each
			{ k,v ->
				if(k.split("-")[3].equals(acct_no)==true)
				{
					String plan_no = k.split("-")[6].toString()
					String amt = v.toString()
					if (amt.contains("calc") == false)
					{
						logi.logInfo("inside if calc false loop")
						if (usagetypeplans.containsKey(k.split("-")[5]))
						{
							logi.logInfo("inside if calc false loop - if " + usagetypeplans.get(k.split("-")[5]).toString().split(",")[1].toString() +" , amt : "+amt)
							double tmpamtt = usagetypeplans.get(k.split("-")[5]).toString().split(",")[1].toString().toDouble() + amt.toDouble()
							logi.logInfo("Calculated amt: " + tmpamtt.toString())
							usagetypeplans.put(k.split("-")[5],plan_no+","+tmpamtt.toString())
						}
						else
							usagetypeplans.put(k.split("-")[5],plan_no+","+amt)
					}
					else
					{
						logi.logInfo("inside calc loop")
						usagetypeplans.put(k.split("-")[5]+ ","+utcount.toString(),plan_no+","+amt )
						utcount++;
					}
					logi.logInfo("utcount :: "+utcount)
					//usagetypeplans.put(k.split("-")[5]+ ","+utcount.toString(),plan_no+","+amt)

				}
				logi.logInfo "usagetypeplans hash true loop :: "+usagetypeplans.toString()
			}
		}

		else if(unbilled_flag == "FALSE")
		{
			int utcount=1;
			List res_plan_no = getAllUsedPlanNoP2(acct_no,"")
			logi.logInfo("usage should be billed for the plan:"+res_plan_no)

			Constant.recordedusageamounts.each
			{ k,v ->

				for(int planN = 0; planN <res_plan_no.size(); planN++){
					if(k.split("-")[3].equals(acct_no)==true && k.split("-")[6].toString() == res_plan_no[planN])
					{
						String plan_no = k.split("-")[6].toString()
						String amt = v.toString()
						if (amt.contains("calc") == false)
						{
							logi.logInfo("inside if calc false loop")
							if (usagetypeplans.containsKey(k.split("-")[5]))
							{
								logi.logInfo("inside if calc false loop - if " + usagetypeplans.get(k.split("-")[5]).toString().split(",")[1].toString() +" , amt : "+amt)
								double tmpamtt = usagetypeplans.get(k.split("-")[5]).toString().split(",")[1].toString().toDouble() + amt.toDouble()
								logi.logInfo("Calculated amt: " + tmpamtt.toString())
								usagetypeplans.put(k.split("-")[5],plan_no+","+tmpamtt.toString())
							}
							else
								usagetypeplans.put(k.split("-")[5],plan_no+","+amt)
						}
						else
						{
							logi.logInfo("inside calc loop")
							usagetypeplans.put(k.split("-")[5]+ ","+utcount.toString(),plan_no+","+amt )
							utcount++;
						}
						logi.logInfo("utcount :: "+utcount)
					}

				}
			}
		}

		logi.logInfo( "usagetypeplans in md_ExpUsage_Amt method " +usagetypeplans.toString())

		usagetypeplans.each
		{ k,v ->
			logi.logInfo( "Inside usagetype set loop")
			String[] karray = k.split(",")
			logi.logInfo ("Key val :: "+karray[0]);
			String[] varray = v.split(",")
			if(varray[1].toString().contains("calc:") == true)
			{
				double calc_amt = varray[1].split("calc:")[1].toString().toDouble().round(2)
				usg = (usg + calc_amt).toDouble()
			}
			else
			{
				String totalunits=db.executeQueryP2("SELECT NVL(sum(billable_units),0) FROM ariacore.usage u JOIN ariacore.acct act ON act.acct_no=u.acct_no WHERE act.acct_no = "+acct_no+" AND u.usage_type ="+karray[0]+" AND (u.invoice_no ="+invoiceNo+" or u.invoice_no  is null) and u.exclude_ind=0 and u.client_rated=0")
				List service_nos=md_GET_USAGE_SERVICE_NO(acct_no.toString(),karray[0],varray[0])
				for(service in service_nos)
				{
					logi.logInfo("service_no in md_ExpUsage_Amt "+service_nos)
					usage_rateamt_hm=md_USAGE_AMT_RATE_EXP(acct_no.toString(),service_nos,totalunits,karray[0],varray[0])
					logi.logInfo("usage_rateamt_hm in md_ExpUsage_Amt "+usage_rateamt_hm)
					String amt=usage_rateamt_hm.get(service)[1].toString()
					usg=usg+amt.toString().toDouble()

				}
			}
		}

		return d.format(usg.round(2)).toString()
	}

	public def md_GetQualifiersFromUsageHistoryAPIa(String tcid)
	{
		return md_GetQualifiersFromUsageHistoryAPI(tcid,"a")
	}
	public def md_GetQualifiersFromUsageHistoryAPIb(String tcid)
	{
		return md_GetQualifiersFromUsageHistoryAPI(tcid,"b")
	}
	public def md_GetQualifiersFromUsageHistoryAPIc(String tcid)
	{
		return md_GetQualifiersFromUsageHistoryAPI(tcid,"c")
	}
	/**
	 * Getting the usage qualifiers value from Usage HistoryAPI
	 * @param tcid
	 * @param type
	 * @return HashMap
	 */
	HashMap md_GetQualifiersFromUsageHistoryAPI (String tcid,String type)
	{
		logi.logInfo("md_GetQualifiersFromUsageHistoryAPI")
		String q1,q2,q3,q4,q1_ru,q2_ru,q3_ru,q4_ru
		String apiname=tcid.split("-")[2]
		LinkedHashMap qualifier_histy_api=new LinkedHashMap()
		q1=getValueFromRequest(apiname,"//usage_qualifier_1")
		q2=getValueFromRequest(apiname,"//usage_qualifier_2")
		q3=getValueFromRequest(apiname,"//usage_qualifier_3")
		q4=getValueFromRequest(apiname,"//usage_qualifier_4")

		if(!q1.equals("NoVal") && !q2.equals("NoVal") && !q3.equals("NoVal") && !q4.equals("NoVal"))
		{
			qualifier_histy_api.put("USAGE_QUALIFIER_1",q1)
			qualifier_histy_api.put("USAGE_QUALIFIER_2",q2)
			qualifier_histy_api.put("USAGE_QUALIFIER_3",q3)
			qualifier_histy_api.put("USAGE_QUALIFIER_4",q4)
		}
		else
		{
			String apiname1="record_usage"
			q1_ru=getValueFromRequest(apiname1 +"."+type,"//qualifier_1")
			q2_ru=getValueFromRequest(apiname1 +"."+type,"//qualifier_2")
			q3_ru=getValueFromRequest(apiname1 +"."+type,"//qualifier_3")
			q4_ru=getValueFromRequest(apiname1 +"."+type,"//qualifier_4")
			qualifier_histy_api.put("USAGE_QUALIFIER_1",q1_ru)
			qualifier_histy_api.put("USAGE_QUALIFIER_2",q2_ru)
			qualifier_histy_api.put("USAGE_QUALIFIER_3",q3_ru)
			qualifier_histy_api.put("USAGE_QUALIFIER_4",q4_ru)
		}
		logi.logInfo "Qualifier hash " + qualifier_histy_api.toString()
		return qualifier_histy_api.sort()

	}
	/**
	 * Getting the usage qualifiers value from response
	 * @param tcid
	 * @return HashMap
	 */
	HashMap md_GetQualifiersFromResponse(String tcid)
	{
		logi.logInfo("md_GetQualifiersFromResponse")
		logi.logInfo " tcid " + tcid
		String qr1,qr2,qr3,qr4
		String apiname=tcid.split("-")[2]
		logi.logInfo " api name " +apiname
		LinkedHashMap qualifier_res_api=new LinkedHashMap()
		qr1=getValueFromResponse(apiname,"//*/*/*/*:qualifier_1[1]")
		qr2=getValueFromResponse(apiname,"//*/*/*/*:qualifier_2[1]")
		qr3=getValueFromResponse(apiname,"//*/*/*/*:qualifier_3[1]")
		qr4=getValueFromResponse(apiname,"//*/*/*/*:qualifier_4[1]")
		qualifier_res_api.put("USAGE_QUALIFIER_1",qr1)
		qualifier_res_api.put("USAGE_QUALIFIER_2",qr2)
		qualifier_res_api.put("USAGE_QUALIFIER_3",qr3)
		qualifier_res_api.put("USAGE_QUALIFIER_4",qr4)
		logi.logInfo "Qualifier response hash "+qualifier_res_api.toString()
		return qualifier_res_api.sort()
	}

	/**
	 * Getting the credit balance amount from credit table
	 * @param tcid
	 * @return String
	 */
	String md_verifyCrediBalanceFromAccount(String tcid){

		ConnectDB db = new ConnectDB()
		String accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')

		double creditamt=db.executeQueryP2("select SUM(LEFT_TO_APPLY) from ariacore.all_credits  where acct_no="+accountNo+" and client_no ="+clientNo).toString().toDouble()
		logi.logInfo ("The Credit amount from amount is :: "+creditamt)

		return df.format(creditamt.toString().toDouble()).toString()
	}

	/**
	 * Getting the expected credit balance amount
	 * @param tcid
	 * @return String
	 */
	String md_verifyCrediBalanceFromAccountExp(String tcid){

		ConnectDB db = new ConnectDB()
		String actualCreditAmt = getValueFromRequest("apply_service_credit","//credit_amount")
		logi.logInfo("Total amount credited to the account is :: "+actualCreditAmt)

		String accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String serviceCreditNo = getValueFromResponse("apply_service_credit","//*:apply_service_creditResponse[1]/credit_id")
		String invoiceNo = getInvoiceNoVT(clientNo, accountNo).toString();
		logi.logInfo("Invoice number while calculating remaining credit amount is :: "+invoiceNo)

		String creditedAmtQry = "select sum(debit) from ariacore.gl_detail where invoice_no="+invoiceNo+"  and orig_credit_id = "+serviceCreditNo
		double ddiscAmt=db.executeQueryP2(creditedAmtQry).toString().toDouble()
		def balance =  Math.abs(ddiscAmt)
		String remainingCreditAmt = actualCreditAmt.toDouble() - balance.toDouble()
		logi.logInfo("Remaining Credit amount is :: "+remainingCreditAmt)

		return df.format(remainingCreditAmt.toString().toDouble()).toString()
	}
	/**
	 * Getting accumulation reset months value from create_acct_complete api
	 * @param tcid
	 * @return String
	 */
	String md_ACCUMALATION_RESET_MONTHS_EXP(String tcid)
	{
		logi.logInfo("inside md_ACCUMALATION_RESET_MONTHS_EXP")
		String reset_months
		reset_months = getValueFromRequest("create_acct_complete","//usage_accumulation_config_row/usage_accumulation_reset_months").toString()
		
		
		if(reset_months.equalsIgnoreCase("NoVal"))
		reset_months = getValueFromRequest("update_acct_complete","//usage_accumulation_config_row/usage_accumulation_reset_months").toString()
		
		if(reset_months.equalsIgnoreCase("NoVal"))
		reset_months = getValueFromRequest("create_acct_hierarchy","//a1_usage_accumulation_reset_months").toString()
		
		return reset_months
	}
		
	
	/**
	 * Getting the second accumulation reset months value from create_acct_complete api
	 * @param tcid
	 * @return String
	 */
	String md_ACCUMALATION_RESET_MONTHS_EXP_2(String tcid)
	{
		logi.logInfo("inside md_ACCUMALATION_RESET_MONTHS_EXP")
		String reset_months = getValueFromRequest("create_acct_complete","//usage_accumulation_config_row[2]/usage_accumulation_reset_months").toString()
		return reset_months
	}
	/**
	 * Getting the child account accumulation reset months value
	 * @param tcid
	 * @return String
	 */
	String md_CHILD_ACCUMALATION_RESET_MONTHS_EXP(String tcid)
	{
		logi.logInfo("inside md_ACCUMALATION_RESET_MONTHS_EXP")
		String reset_months = getValueFromRequest("create_acct_complete.a","//usage_accumulation_config_row/usage_accumulation_reset_months").toString()
		return reset_months
	}
	/**
	 * Getting the usage details from 'get_unbilled_usage_summary' api
	 * @param tcid
	 * @return HashMap
	 */
	def md_GetUnbilledUsageSummaryApi(String tcid)
	{
		logi.logInfo("Entered into md_GetUnbilledUsageSummaryApi")
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap unbilled_summary_api=new HashMap()
		com.eviware.soapui.support.XmlHolder outputholder

		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains('get_unbilled_usage_summary') && testCaseCmb[j].toString().contains(Constant.TESTCASEID)) {
				outputholder = xmlValues[j]
				unbilled_summary_api.put("CURRENCY_CODE", outputholder.getNodeValue("//*:get_unbilled_usage_summaryResponse[1]/currency_cd[1]"))
				unbilled_summary_api.put("CURRENCY_NAME",outputholder.getNodeValue("//*:get_unbilled_usage_summaryResponse[1]/currency_name[1]"))
				unbilled_summary_api.put("MTD_BALANCE_AMOUNT",outputholder.getNodeValue("//*:get_unbilled_usage_summaryResponse[1]/mtd_balance_amount[1]"))
				unbilled_summary_api.put("PTD_BALANCE_AMOUNT",outputholder.getNodeValue("//*:get_unbilled_usage_summaryResponse[1]/ptd_balance_amount[1]"))
				unbilled_summary_api.put("UNAPP_SVC_CREDIT_BAL_AMOUNT",outputholder.getNodeValue("//*:get_unbilled_usage_summaryResponse[1]/unapp_svc_credit_bal_amount[1]"))
				unbilled_summary_api.put("UNAPP_SVC_CREDIT_DELTA_SIGN",outputholder.getNodeValue("//*:get_unbilled_usage_summaryResponse[1]/unapp_svc_credit_delta_sign[1]"))
				unbilled_summary_api.put("UNAPP_SVC_CREDIT_DELTA_AMOUNT",outputholder.getNodeValue("//*:get_unbilled_usage_summaryResponse[1]/unapp_svc_credit_delta_amount[1]"))

			}

		}

		return unbilled_summary_api;
	}
	/**
	 * Getting the usage details from usage table
	 * @param tcid
	 * @return HashMap
	 */
	def md_GetUnbilledUsageSummaryDb(String tcid)
	{
		logi.logInfo("md_GetUnbilledUsageSummaryDb")
		String curr_cd
		String curr_name
		HashMap unbilled_summary_db=new HashMap()
		String apiname=tcid.split("-")[2]
		String acct_no=getValueFromRequest(apiname,ExPathRpcEnc.IR_GET_UNBILLED_USAGE_ACCTNO)
		def client_no = getValueFromRequest(apiname,"//client_no")
		ConnectDB db = new ConnectDB()
		String cur_cd_qry = "Select currency_cd from ariacore.acct where acct_no="+acct_no
		ResultSet rs_cur_cd_qry = db.executePlaQuery(cur_cd_qry)
		while(rs_cur_cd_qry.next())

		{
			curr_cd=rs_cur_cd_qry.getString(1)
			unbilled_summary_db.put("CURRENCY_CODE", curr_cd)
		}

		String curr_name_qry = 	"Select currency_name from ariacore.currency where currency_cd='"+curr_cd+"'"
		ResultSet rs_curr_name_qry = db.executePlaQuery(curr_name_qry)
		while(rs_curr_name_qry.next())
		{
			curr_name=rs_curr_name_qry.getString(1)
			unbilled_summary_db.put("CURRENCY_NAME", curr_name)
		}

		String bal_qry = "Select NVL(MTD_UNBILLED_USG_BAL_TRUE,0) as mtd_balance_amount,NVL(PTD_UNBILLED_USG_BAL_TRUE,0) as ptd_balance_amount,NVL(CURRENT_UNAPPLIED_CREDIT_BAL,0) unapp_svc_credit_bal_amount from ariacore.acct_usage_summary where acct_no="+acct_no+" and client_no="+client_no
		ResultSet rs_bal_qry = db.executePlaQuery(bal_qry);
		ResultSetMetaData met = rs_bal_qry.getMetaData();
		int columns = met.getColumnCount();
		while (rs_bal_qry.next())
		{
			for(int i=1; i<=columns; i++)
			{
				unbilled_summary_db.put(met.getColumnName(i),rs_bal_qry.getObject(i));
			}
		}

		unbilled_summary_db.put("UNAPP_SVC_CREDIT_DELTA_AMOUNT",unbilled_summary_db.get("MTD_BALANCE_AMOUNT"))

		if(unbilled_summary_db.get("UNAPP_SVC_CREDIT_DELTA_AMOUNT")>unbilled_summary_db.get("UNAPP_SVC_CREDIT_BAL_AMOUNT"))
		{
			unbilled_summary_db.put("UNAPP_SVC_CREDIT_DELTA_SIGN","+")
		}

		else if(unbilled_summary_db.get("UNAPP_SVC_CREDIT_DELTA_AMOUNT")<unbilled_summary_db.get("UNAPP_SVC_CREDIT_BAL_AMOUNT"))
		{
			unbilled_summary_db.put("UNAPP_SVC_CREDIT_DELTA_SIGN","-")
		}

		else
			unbilled_summary_db.put("UNAPP_SVC_CREDIT_DELTA_SIGN","=")

		return unbilled_summary_db

	}
	/**
	 * Getting the failed records count from 'discard_usage' api
	 * @param testcaseid
	 * @return String
	 */
	def md_DiscardUsageFailedRecordsCount(String testcaseid)
	{
		logi.logInfo "Entered into md_DiscardUsageFailedRecordsCount "
		return getNodeCountFromResponse("discard_usage","//*/*:failed_records_row").toString()
	}

	public def md_GetUnBilledUsageSummaryRecordsApi1(String tcid)
	{
		return md_GetUnBilledUsageSummaryRecordsApi(tcid,"1")
	}

	public def md_GetUnBilledUsageSummaryRecordsApi2(String tcid)
	{
		return md_GetUnBilledUsageSummaryRecordsApi(tcid,"2")
	}
	/**
	 * Getting unbilled usage records details from get_unbilled_usage_summary api
	 * @param tcid
	 * @param line_no
	 * @return HashMap
	 */
	HashMap md_GetUnBilledUsageSummaryRecordsApi(String tcid,String line_no)
	{
		logi.logInfo("Entered into md_GetUnBilledUsageSummaryRecordsApi")
		int tmp=1
		LinkedHashMap unbilled_summary_rec_api=new LinkedHashMap()
		String xpath="//*:get_unbilled_usage_summaryResponse"
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()

		for(int j=0; j<testCaseCmb.size(); j++) {

			if(testCaseCmb[j].toString().equals(tcid))
			{
				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
					def responseXml = new XmlSlurper().parseText(holder.getXml())
					def rootNode = responseXml.Body.get_unbilled_usage_summaryResponse.unbilled_usage_recs
					rootNode.children().each()
					{
						def nme=it.name()+"["+tmp+"]"
						logi.logInfo("nme:"+nme.toString())
						if(nme.equals("unbilled_usage_recs_row["+line_no+"]"))
						{
							it.children().each()
							{
								logi.logInfo(it.name())
								logi.logInfo(it.text())
								unbilled_summary_rec_api.put(it.name().toUpperCase(), it.text())
							}
						}

						tmp++

					}
				}
			}

		}

		logi.logInfo "Unbilled Usage summary hash " +unbilled_summary_rec_api.toString()
		return unbilled_summary_rec_api.sort()
	}

	public def md_GetUnBilledUsageSummaryRecordsDb1(String tcid)
	{
		return md_GetUnBilledUsageSummaryRecordsDb(tcid,"1")
	}

	public def md_GetUnBilledUsageSummaryRecordsDb2(String tcid)
	{
		return md_GetUnBilledUsageSummaryRecordsDb(tcid,"2")
	}
	/**
	 * Getting unbilled usage records details from usage table
	 * @param tcid
	 * @param seq_no
	 * @return HashMap
	 */
	HashMap md_GetUnBilledUsageSummaryRecordsDb(String tcid,String seq_no)
	{
		logi.logInfo "Entered into md_GetUnBilledUsageSummaryRecordsDb"
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=tcid.split("-")[2]
		LinkedHashMap<String,String> unbilled_summary_rec_db=new LinkedHashMap()
		logi.logInfo("apiname"+apiname)
		def acct_no = getValueFromRequest(apiname,"//acct_no")
		String unit_type
		def client_no = getValueFromRequest(apiname,"//client_no")
		String usg_qry = "SELECT usage_type AS usage_type_no,TO_CHAR(usage_date,'yyyy-MM-dd') AS usage_date,usage_units AS units,billable_units AS recorded_units,NVL(rate,0) AS pre_rated_rate,amt AS specific_record_charge_amount,rec_no AS usage_rec_no FROM(Select acct_no,usage_type, usage_date,usage_units,billable_units,rate,amt,rec_no,row_number() over(order by usage_date asc) as s_no FROM ariacore.usage WHERE acct_no="+acct_no+") where s_no="+seq_no
		ResultSet resultSet = db.executePlaQuery(usg_qry);
		ResultSetMetaData md = resultSet.getMetaData();
		int columns = md.getColumnCount();

		while (resultSet.next()){
			for(int i=1; i<=columns; i++){
				unbilled_summary_rec_db.put(md.getColumnName(i),resultSet.getObject(i));
			}

		}

		String unit_read_qry = "Select param_val from ariacore.all_client_params where param_name ='USAGE_RECORD_READ_SCALE' and  client_no="+client_no
		ResultSet rs_unit_read = db.executePlaQuery(unit_read_qry)
		while(rs_unit_read.next())
		{

			int un_rnd=rs_unit_read.getInt(1)
			unbilled_summary_rec_db.put("UNITS",d.format(unbilled_summary_rec_db.get("UNITS").toString().toDouble().round(un_rnd)).toString())
			unbilled_summary_rec_db.put("RECORDED_UNITS",d.format(unbilled_summary_rec_db.get("RECORDED_UNITS").toString().toDouble().round(un_rnd)).toString())
			if(unbilled_summary_rec_db.get("SPECIFIC_RECORD_CHARGE_AMOUNT").toString() != "null")
			{
				unbilled_summary_rec_db.put("SPECIFIC_RECORD_CHARGE_AMOUNT",d.format(unbilled_summary_rec_db.get("SPECIFIC_RECORD_CHARGE_AMOUNT").toString().toDouble().round(un_rnd)).toString())
			}
			else
			{
				unbilled_summary_rec_db.remove("SPECIFIC_RECORD_CHARGE_AMOUNT")
			}
		}


		String des_qry = "Select description,usage_unit_type,usage_type_code from ARIACORE.usage_types where usage_type="+unbilled_summary_rec_db.get("USAGE_TYPE_NO")
		ResultSet rs_des_qry = db.executePlaQuery(des_qry)
		while(rs_des_qry.next())
		{
			String unit_des = rs_des_qry.getString(1)
			unit_type = rs_des_qry.getString(2)
			String usage_type_code=rs_des_qry.getString(3)
			unbilled_summary_rec_db.put("USAGE_TYPE_DESCRIPTION", unit_des)
			unbilled_summary_rec_db.put("USAGE_TYPE_CODE", usage_type_code)
		}

		return unbilled_summary_rec_db.sort()
	}

	String md_isEventTriggered438(String tcid){
		return getEventTriggered("438").toString().toUpperCase()
	}
	String md_isEventTriggered439(String tcid){
		return getEventTriggered("439").toString().toUpperCase()
	}
	String md_isEventTriggered440(String tcid){
		return getEventTriggered("440").toString().toUpperCase()
	}
	String md_isEventTriggered501(String tcid){
		return getEventTriggered("501").toString().toUpperCase()
	}
	String md_isEventTriggered502(String tcid){
		return getEventTriggered("502").toString().toUpperCase()
	}
	String md_isEventTriggered503(String tcid){
		return getEventTriggered("503").toString().toUpperCase()
	}
	String md_isEventTriggered504(String tcid){
		return getEventTriggered("504").toString().toUpperCase()
	}
	String md_isEventTriggered505(String tcid){
		return getEventTriggered("505").toString().toUpperCase()
	}
	String md_isEventTriggered506(String tcid){
		return getEventTriggered("506").toString().toUpperCase()
	}
	String md_isEventTriggered507(String tcid){
		return getEventTriggered("507").toString().toUpperCase()
	}
	String md_isEventTriggered508(String tcid){
		return getEventTriggered("508").toString().toUpperCase()
	}
	String md_isEventTriggered509(String tcid){
		return getEventTriggered("509").toString().toUpperCase()
	}
	String md_isEventTriggered510(String tcid){
		return getEventTriggered("510").toString().toUpperCase()
	}

	/**
	 * Returns true/false based on the Event triggered
	 * @param eventId
	 * @return boolean 
	 */
	boolean getEventTriggered(String eventId){

		logi.logInfo("Inside method getEventTriggered")
		ConnectDB db = new ConnectDB()
		String event
		boolean eventFlag = false

		String accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')

		String qry = "SELECT EVENT_ID FROM ARIACORE.ACCT_PROV2_EVENTS WHERE PROV_SEQ IN (SELECT PROV_SEQ FROM ARIACORE.ACCT_PROV2_STEPS  WHERE ACCT_NO="+accountNo+" AND CLIENT_NO="+clientNo+") and EVENT_ID = "+eventId+" GROUP BY EVENT_ID"
		event = db.executeQueryP2(qry)
		logi.logInfo("event "+event)

		if(event.equals(eventId)){
			eventFlag = true
		}

		return eventFlag
	}
	/**
	 * Getting the  MTD Threshold value
	 * @param eventId
	 * @return String
	 */
	public String md_verifyAcctMTDThresholdValueAPI(String tcid){
		String acctMTDVal
		String apiname=tcid.split("-")[2]
		acctMTDVal = getValueFromRequest(apiname,"//amount").toString()
		if(acctMTDVal.equals("NoVal") || acctMTDVal.toInteger()<0)
			acctMTDVal = null
		logi.logInfo("acctMTDVal :: "+acctMTDVal)
		return acctMTDVal
	}
	/**
	 * Getting the  Client MTD Threshold value
	 * @param eventId
	 * @return String
	 */
	public String md_verifyClientMTDThresholdValueAPI(String tcid){
		String clientMTDVal
		String apiname=tcid.split("-")[2]
		clientMTDVal = getValueFromRequest(apiname,"//amount").toString()
		if(clientMTDVal.equals("NoVal") || clientMTDVal.toInteger()<0)
			clientMTDVal = null
		logi.logInfo("clientMTDVal :: "+clientMTDVal)
		return clientMTDVal
	}
	/**
	 * Getting the PTD Threshold value
	 * @param eventId
	 * @return String
	 */
	public String md_verifyAcctPTDThresholdValueAPI(String tcid){
		String acctPTDVal
		String apiname=tcid.split("-")[2]
		acctPTDVal = getValueFromRequest(apiname,"//amount").toString()
		if(acctPTDVal.equals("NoVal") || acctPTDVal.toInteger()<0)
			acctPTDVal = null
		logi.logInfo("acctPTDVal :: "+acctPTDVal)
		return acctPTDVal
	}
	/**
	 * Getting the Client PTD Threshold value
	 * @param eventId
	 * @return String
	 */
	public String md_verifyClientPTDThresholdValueAPI(String tcid){
		String clientPTDVal
		String apiname=tcid.split("-")[2]
		clientPTDVal = getValueFromRequest(apiname,"//amount").toString()
		if(clientPTDVal.equals("NoVal") || clientPTDVal.toInteger()<0)
			clientPTDVal = null
		logi.logInfo("clientPTDVal :: "+clientPTDVal)
		return clientPTDVal
	}
	/**
	 * Getting the MTD Threshold value from database
	 * @param eventId
	 * @return String
	 */
	public String md_verifyAcctMTDThresholdValueDB(String tcid){
		String acctMTDValDB
		acctMTDValDB = verify_MTD_PTD_ThresholdValue("MTD_ACCT_THRESHOLD_NOTICE_AMT")
		logi.logInfo("acctMTDValDB :: "+acctMTDValDB)
		return acctMTDValDB
	}
	/**
	 * Getting the Client MTD Threshold value from database
	 * @param eventId
	 * @return String
	 */
	public String md_verifyClientMTDThresholdValueDB(String tcid){
		String clientMTDValDB
		clientMTDValDB = verify_MTD_PTD_ThresholdValue("MTD_CLI_THRESHOLD_NOTICE_AMT")
		logi.logInfo("clientMTDValDB :: "+clientMTDValDB)
		return clientMTDValDB
	}
	/**
	 * Getting the PTD Threshold value from database
	 * @param eventId
	 * @return String
	 */
	public String md_verifyAcctPTDThresholdValueDB(String tcid){
		String acctPTDValDB
		acctPTDValDB = verify_MTD_PTD_ThresholdValue("PTD_ACCT_THRESHOLD_NOTICE_AMT")
		return acctPTDValDB
	}
	/**
	 * Getting the Client PTD Threshold value from database
	 * @param eventId
	 * @return String
	 */
	public String md_verifyClientPTDThresholdValueDB(String tcid){
		String clientPTDValDB
		clientPTDValDB = verify_MTD_PTD_ThresholdValue("PTD_CLI_THRESHOLD_NOTICE_AMT")
		logi.logInfo("clientPTDValDB :: "+clientPTDValDB)
		return clientPTDValDB
	}
	/**
	 * Getting the MTD unbilled usage balance value
	 * @param eventId
	 * @return String
	 */
	public String md_verifyMTDUnbilledUsageBalanceValue(String tcid){
		String mtdBalance
		mtdBalance = verify_MTD_PTD_ThresholdValue("MTD_UNBILLED_USG_BAL_MEASURED")
		logi.logInfo("MTD balance :: "+mtdBalance)
		return mtdBalance
	}

	/**
	 * Getting the unbilled PTD usage balance value
	 * @param tcid
	 * @return String
	 */
	public String md_verifyPTDUnbilledUsageBalanceValue(String tcid){
		String ptdBalance
		ptdBalance = verify_MTD_PTD_ThresholdValue("PTD_UNBILLED_USG_BAL_MEASURED")
		logi.logInfo("PTD balance :: "+ptdBalance)
		return ptdBalance
	}
	/**
	 * Getting MTD/PTD Threshold Value
	 * @param level
	 * @return String value
	 */
	public String verify_MTD_PTD_ThresholdValue(String level){
		logi.logInfo("Inside method verify_MTD_PTD_ThresholdValue")
		ConnectDB db = new ConnectDB()
		String accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String valueFromDB
		String query = "select %s from ariacore.acct_usage_summary where acct_no = "+accountNo+" and client_no ="+clientNo
		valueFromDB = db.executeQueryP2(String.format(query,level))
		logi.logInfo("verify_MTD_PTD_ThresholdValue :: "+valueFromDB)
		return valueFromDB
	}
	/**
	 * Getting the usage summary details from 'get_usage_summary_by_type' api
	 * @param tcid
	 * @return HashMap
	 */
	def md_GetUsageSummaryByTypeApi(String tcid)
	{
		logi.logInfo("Entered into md_GetUsageSummaryByTypeApi")
		LinkedHashMap usage_summary_by_type_api=new LinkedHashMap()
		String xpath="//*:get_usage_summary_by_typeResponse"
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()

		for(int j=0; j<testCaseCmb.size(); j++) {

			if(testCaseCmb[j].toString().equals(tcid))
			{
				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
					def responseXml = new XmlSlurper().parseText(holder.getXml())
					def rootNode = responseXml.Body.get_usage_summary_by_typeResponse
					rootNode.children().each()
					{
						usage_summary_by_type_api.put(it.name(),rootNode[it.name()])
						usage_summary_by_type_api.remove("usage_summary_records")

						if(usage_summary_by_type_api.containsKey("start_time"))
						{
							String usage_start_time=usage_summary_by_type_api.get("start_time")
							usage_start_time=usage_start_time.replaceAll(":","-")
							usage_summary_by_type_api.put("start_time",usage_start_time)
						}

						if(usage_summary_by_type_api.containsKey("end_time"))
						{
							String usage_end_time=usage_summary_by_type_api.get("end_time")
							usage_end_time=usage_end_time.replaceAll(":","-")
							usage_summary_by_type_api.put("end_time",usage_end_time)
						}

						if(!usage_summary_by_type_api.get("error_code").equals("0"))
						{
							usage_summary_by_type_api.remove("end_date")
							usage_summary_by_type_api.remove("end_time")
							usage_summary_by_type_api.remove("start_date")
							usage_summary_by_type_api.remove("start_time")
						}

					}
				}
			}

		}

		logi.logInfo "Usage summary by type hash " +usage_summary_by_type_api.toString()
		return usage_summary_by_type_api.sort()
	}
	/**
	 * Getting the usage summary details from database
	 * @param tcid
	 * @return HashMap
	 */
	def md_GetUsageSummaryByTypeDb(String tcid)
	{
		logi.logInfo("Entered into md_GetUsageSummaryByTypeDb")
		ConnectDB db = new ConnectDB()
		LinkedHashMap usage_summary_by_type_db=new LinkedHashMap()
		String apiname=tcid.split("-")[2]
		String rec_count,usage_start_date,usage_end_date,start_date_new,end_date_new,start_time_q,start_time_r,end_time_q,end_time_r
		String last_usage_bill_date,last_usg_date
		def client_no=getValueFromRequest(apiname,"//client_no")
		def acct_no=getValueFromRequest(apiname,"//acct_no")
		def user_id=getValueFromRequest(apiname,"//user_id")
		String start_date=getValueFromRequest(apiname,"//date_filter_start_date").toString()
		def start_time=getValueFromRequest(apiname,"//date_filter_start_time")
		def end_date=getValueFromRequest(apiname,"//date_filter_end_date")
		def end_time=getValueFromRequest(apiname,"//date_filter_end_time")
		def usage_type=getValueFromRequest(apiname,"//usage_type_filter")
		def billed_filter=getValueFromRequest(apiname,"//billed_filter")
		def billing_period_flag=getValueFromRequest(apiname,"//billing_period_flag")

		if(acct_no=='NoVal')
		{
			String acct_no_q="select acct_no from ariacore.acct where userid='"+user_id+"\'"
			acct_no=db.executeQueryP2(acct_no_q)
		}

		/* Setting usage start date and usage end date based on the input */

		start_date_new=formattedDate(start_date)
		logi.logInfo " start date after formatting "+start_date_new

		if(start_time=='NoVal')
		{
			start_time_q="01:00:00"
			start_time_r="00-00-00"
			logi.logInfo " start time for query set as " +start_time_q
			logi.logInfo " start time for response set as " +start_time_r
		}
		else
		{
			start_time_r=start_time.toString().replaceAll(":","-")
			start_time_q=start_time
		}

		if(end_date=='NoVal')
		{
			String vt_qry="SELECT  TO_CHAR (ARIACORE.ARIAVIRTUALTIME("+client_no+"), 'YYYY-MM-DD')  FROM DUAL"
			end_date=db.executeQueryP2(vt_qry)
			end_date_new=formattedDate(end_date)
			logi.logInfo " end date after formatting " +end_date_new
		}

		else
		{
			end_date_new=formattedDate(end_date)
		}

		if(end_time=='NoVal')
		{
			end_time_q="23:59:59"
			end_time_r="23-59-59"
			logi.logInfo " end time for query set as " +end_time_q
			logi.logInfo " end time for response set as " +end_time_r
		}
		else
		{
			end_time_r=end_time.toString().replaceAll(":","-")
			end_time_q=end_time
		}

		usage_start_date=start_date_new +" "+start_time_q
		usage_end_date=end_date_new +" "+end_time_q

		logi.logInfo " usage_start_date set as "+usage_start_date
		logi.logInfo " usage_end_date set as "+usage_end_date

		/*Getting the usage records count based on the filters provided in the input*/

		if((billed_filter.equals("0") || billed_filter=='NoVal') && (billing_period_flag=='NoVal' || billing_period_flag.equals("0")) )
		{
			logi.logInfo " Inside billed_filter=0"
			String usg_qry1="select count(*) from ariacore.usage where acct_no="+acct_no+" and client_no="+client_no+" and usage_date between to_date('"+usage_start_date+"','dd-MON-yyyy hh24:mi:ss') and to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss')  %s"
			String usg_type1=""
			if(usage_type!='NoVal')
			{
				usg_type1="and usage_type= "+usage_type
			}
			String formattedQry1 = String.format(usg_qry1,usg_type1)
			rec_count=db.executeQueryP2(formattedQry1)
		}

		if(billed_filter.equals("1") && (billing_period_flag=='NoVal' || billing_period_flag.equals("0")) )
		{
			logi.logInfo " Inside billed_filter=1"
			String usg_qry2="select count(*) from ariacore.usage where acct_no="+acct_no+" and client_no="+client_no+" and invoice_no is not null and usage_date between to_date('"+usage_start_date+"','dd-MON-yyyy hh24:mi:ss') and to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss')  %s"
			String usg_type2=""
			if(usage_type!='NoVal')
			{
				usg_type2="and usage_type= "+usage_type
			}
			String formattedQry2 = String.format(usg_qry2,usg_type2)
			rec_count=db.executeQueryP2(formattedQry2)
		}

		if(billed_filter.equals("2") && (billing_period_flag=='NoVal' || billing_period_flag.equals("0")) )
		{
			logi.logInfo " Inside billed_filter=2"
			String usg_qry3="select count(*) from ariacore.usage where acct_no="+acct_no+" and client_no="+client_no+" and invoice_no is null and usage_date between to_date('"+usage_start_date+"','dd-MON-yyyy hh24:mi:ss') and to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss') %s"
			String usg_type3=""
			if(usage_type!='NoVal')
			{
				usg_type3="and usage_type= "+usage_type
			}
			String formattedQry3 = String.format(usg_qry3,usg_type3)
			logi.logInfo " formatted query3 "+formattedQry3
			rec_count=db.executeQueryP2(formattedQry3)
		}


		last_usage_bill_date=db.executeQueryP2("select to_char(LAST_USAGE_BILL_DATE,'YYYY-MM-DD') from ariacore.acct where acct_no="+acct_no)
		last_usg_date=formattedDate(last_usage_bill_date)
		logi.logInfo " last usage bill date " + last_usage_bill_date
		logi.logInfo " last usage date " + last_usg_date

		if((billed_filter.equals("0") || billed_filter=='NoVal') && billing_period_flag.equals("1") )
		{
			logi.logInfo " Inside billed_filter=0 & billed_flag=1"
			String usg_qry1="select count(*) from ariacore.usage where acct_no="+acct_no+" and client_no="+client_no+" and usage_date between to_date('"+last_usg_date+"','dd-MON-yyyy hh24:mi:ss') and to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss')  %s"
			String usg_type1=""
			if(usage_type!='NoVal')
			{
				usg_type1="and usage_type= "+usage_type
			}
			String formattedQry1 = String.format(usg_qry1,usg_type1)
			rec_count=db.executeQueryP2(formattedQry1)
		}

		if(billed_filter.equals("1") && billing_period_flag.equals("1") )
		{
			logi.logInfo " Inside billed_filter=1 & billed_flag=1"
			String usg_qry2="select count(*) from ariacore.usage where acct_no="+acct_no+" and client_no="+client_no+" and invoice_no is not null and usage_date between to_date('"+last_usg_date+"','dd-MON-yyyy hh24:mi:ss') and to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss')  %s"
			String usg_type2=""
			if(usage_type!='NoVal')
			{
				usg_type2="and usage_type= "+usage_type
			}
			String formattedQry2 = String.format(usg_qry2,usg_type2)
			rec_count=db.executeQueryP2(formattedQry2)
		}

		if(billed_filter.equals("2") && billing_period_flag.equals("1") )
		{
			logi.logInfo " Inside billed_filter=2 & billed_flag=1"
			String usg_qry3="select count(*) from ariacore.usage where acct_no="+acct_no+" and client_no="+client_no+" and invoice_no is null and usage_date between to_date('"+last_usg_date+"','dd-MON-yyyy hh24:mi:ss') and to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss') %s"
			String usg_type3=""
			if(usage_type!='NoVal')
			{
				usg_type3="and usage_type= "+usage_type
			}
			String formattedQry3 = String.format(usg_qry3,usg_type3)
			logi.logInfo " formatted query3 "+formattedQry3
			rec_count=db.executeQueryP2(formattedQry3)
		}

		/*Setting the expected hash if there are any usage records for the filters applied*/

		logi.logInfo"usage record count is "+ rec_count
		if(rec_count.equals("0"))
		{
			usage_summary_by_type_db.put("error_code",1008)
			usage_summary_by_type_db.put("error_msg","no line items provided")
		}
		else
		{
			usage_summary_by_type_db.put("error_code",0)
			usage_summary_by_type_db.put("error_msg","OK")
			if(!billing_period_flag.equals("1"))
			{
				usage_summary_by_type_db.put("start_date",start_date)
				usage_summary_by_type_db.put("start_time",start_time_r)
			}
			else
			{
				usage_summary_by_type_db.put("start_date",last_usage_bill_date)
				usage_summary_by_type_db.put("start_time","23-59-59")
			}
			usage_summary_by_type_db.put("end_date",end_date)

			usage_summary_by_type_db.put("end_time",end_time_r)
		}

		logi.logInfo "Usage summary by type Db hash " +usage_summary_by_type_db.toString()
		return usage_summary_by_type_db.sort()
	}

	/**
	 * formats the given input date from dd-MM-yyyy to dd-MMM-yyyy
	 * @param date
	 * @return String
	 */
	public String formattedDate(String date)
	{
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		Date date77 = simpleFormat.parse(date);
		Calendar now = Calendar.getInstance();

		now.setTime(date77);
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
		String formatted_date = format1.format(now.getTime());

		logi.logInfo('Date return :'+formatted_date)
		return formatted_date
	}

	public def md_GetUsageSummaryByTypeRecordsApi1(String tcid)
	{
		return md_GetUsageSummaryByTypeRecordsApi(tcid,"1")
	}

	public def md_GetUsageSummaryByTypeRecordsApi2(String tcid)
	{
		return md_GetUsageSummaryByTypeRecordsApi(tcid,"2")
	}

	public def md_GetUsageSummaryByTypeRecordsApi3(String tcid)
	{
		return md_GetUsageSummaryByTypeRecordsApi(tcid,"3")
	}
	/**
	 * Getting the usage summary details from 'get_usage_summary_by_type' api
	 * @param tcid
	 * @return HashMap
	 */
	def md_GetUsageSummaryByTypeRecordsApi(String tcid,String line_no)
	{
		logi.logInfo("md_GetUsageSummaryByTypeRecordsApiString")
		int tmp=1
		LinkedHashMap usg_summary_by_type_rec_api=new LinkedHashMap()
		String xpath="//*:get_usage_summary_by_typeResponse"
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()

		for(int j=0; j<testCaseCmb.size(); j++) {

			if(testCaseCmb[j].toString().equals(tcid))
			{

				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
					def responseXml = new XmlSlurper().parseText(holder.getXml())
					def rootNode = responseXml.Body.get_usage_summary_by_typeResponse.usage_summary_records
					rootNode.children().each()
					{
						def nme=it.name()+"["+tmp+"]"
						logi.logInfo("nme:"+nme.toString())

						if(nme.equals("usage_summary_records_row["+line_no+"]"))
						{
							it.children().each()
							{
								logi.logInfo(it.name())
								logi.logInfo(it.text())
								usg_summary_by_type_rec_api.put(it.name().toUpperCase(), it.text())
							}
						}

						tmp++

					}
				}
			}

		}
		logi.logInfo " usage summary by type records  api hash "+ usg_summary_by_type_rec_api.toString()
		return usg_summary_by_type_rec_api.sort()
	}



	public def md_GetUsageSummaryByTypeRecordsDb1(String tcid)
	{
		return md_GetUsageSummaryByTypeRecordsDb(tcid,"1")
	}

	public def md_GetUsageSummaryByTypeRecordsDb2(String tcid)
	{
		return md_GetUsageSummaryByTypeRecordsDb(tcid,"2")
	}

	public def md_GetUsageSummaryByTypeRecordsDb3(String tcid)
	{
		return md_GetUsageSummaryByTypeRecordsDb(tcid,"3")
	}
	/**
	 * Getting the usage summary details from database
	 * @param tcid
	 * @return HashMap
	 */
	def md_GetUsageSummaryByTypeRecordsDb(String tcid,String seq_no)
	{
		logi.logInfo("Entered into md_GetUsageSummaryByTypeRecordsDb")
		String apiname=tcid.split("-")[2]
		ConnectDB db=new ConnectDB()
		LinkedHashMap usg_summary_by_type_rec_db=new LinkedHashMap()
		String rec_count,usage_start_date,usage_end_date,start_date_new,end_date_new,start_time_q,start_time_r,end_time_q,end_time_r
		String last_usage_bill_date,last_usg_date
		def client_no=getValueFromRequest(apiname,"//client_no")
		def acct_no=getValueFromRequest(apiname,"//acct_no")
		def user_id=getValueFromRequest(apiname,"//user_id")
		String start_date=getValueFromRequest(apiname,"//date_filter_start_date").toString()
		def start_time=getValueFromRequest(apiname,"//date_filter_start_time")
		def end_date=getValueFromRequest(apiname,"//date_filter_end_date")
		def end_time=getValueFromRequest(apiname,"//date_filter_end_time")
		def usage_type=getValueFromRequest(apiname,"//usage_type_filter")
		def billed_filter=getValueFromRequest(apiname,"//billed_filter")
		def billing_period_flag=getValueFromRequest(apiname,"//billing_period_flag")

		if(acct_no=='NoVal')
		{
			String acct_no_q="select acct_no from ariacore.acct where userid='"+user_id+"\'"
			acct_no=db.executeQueryP2(acct_no_q)
		}

		/* Setting usage start date and usage end date based on the input */

		start_date_new=formattedDate(start_date)
		logi.logInfo " start date after formatting "+start_date_new

		if(start_time=='NoVal')
		{
			start_time_q="01:00:00"
			logi.logInfo " start time for query set as " +start_time_q

		}
		else
		{
			start_time_q=start_time
		}

		if(end_date=='NoVal')
		{
			String vt_qry="SELECT  TO_CHAR (ARIACORE.ARIAVIRTUALTIME("+client_no+"), 'YYYY-MM-DD')  FROM DUAL"
			end_date=db.executeQueryP2(vt_qry)
			end_date_new=formattedDate(end_date)
			logi.logInfo " end date after formatting " +end_date_new
		}
		else
		{
			end_date_new=formattedDate(end_date)
		}

		if(end_time=='NoVal')
		{
			end_time_q="23:59:59"
			logi.logInfo " end time for query set as " +end_time_q
		}
		else
		{
			end_time_q=end_time
		}

		usage_start_date=start_date_new +" "+start_time_q
		usage_end_date=end_date_new +" "+end_time_q

		logi.logInfo " usage_start_date set as "+usage_start_date
		logi.logInfo " usage_end_date set as "+usage_end_date

		/*Getting the usage type no and invoice no based on the filters provided in the input*/

		String usg_qry1,formattedQry1,usg_detail1_qry,formattedqry11,formattedqry12,usg_type1=""

		if((billed_filter.equals("0") || billed_filter=='NoVal') && (billing_period_flag=='NoVal' || billing_period_flag.equals("0")) )
		{
			logi.logInfo " Inside billed_filter=0"
			usg_qry1="SELECT usage_type as usage_type_no,invoice_no from(SELECT usage_type,invoice_no,row_number() over(order by invoice_no) as seq FROM (SELECT usage_type,invoice_no FROM ariacore.usage WHERE acct_no="+acct_no+" AND usage_date BETWEEN to_date('"+usage_start_date+"','dd-MON-yyyy hh24:mi:ss') AND to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss') AND CLIENT_NO ="+client_no+" %s group by invoice_no,usage_type,usage_date order by usage_date) group by usage_type,invoice_no) where seq="+seq_no

			usg_type1=""
			if(usage_type!='NoVal')
			{
				usg_type1="and usage_type= "+usage_type
			}
			formattedQry1 = String.format(usg_qry1,usg_type1)
			usg_summary_by_type_rec_db= db.executeQueryP2(formattedQry1)
		}

		if(billed_filter.equals("1") && (billing_period_flag=='NoVal' || billing_period_flag.equals("0")) )
		{
			logi.logInfo " Inside billed_filter=1"
			usg_qry1="SELECT usage_type as usage_type_no,invoice_no from(SELECT usage_type,invoice_no,row_number() over(order by invoice_no) as seq FROM (SELECT usage_type,invoice_no FROM ariacore.usage WHERE acct_no="+acct_no+" AND usage_date BETWEEN to_date('"+usage_start_date+"','dd-MON-yyyy hh24:mi:ss') AND to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss') AND CLIENT_NO ="+client_no+" AND invoice_no is not null  %s group by invoice_no,usage_type,usage_date order by usage_date) group by usage_type,invoice_no) where seq="+seq_no

			usg_type1=""
			if(usage_type!='NoVal')
			{
				usg_type1="and usage_type= "+usage_type
			}
			formattedQry1 = String.format(usg_qry1,usg_type1)
			usg_summary_by_type_rec_db= db.executeQueryP2(formattedQry1)
		}

		if(billed_filter.equals("2") && (billing_period_flag=='NoVal' || billing_period_flag.equals("0")) )
		{
			logi.logInfo " Inside billed_filter=2"
			String usg_qry3="SELECT usage_type as usage_type_no,invoice_no from(SELECT usage_type,invoice_no,row_number() over(order by invoice_no) as seq FROM (SELECT usage_type,invoice_no FROM ariacore.usage WHERE acct_no="+acct_no+" AND usage_date BETWEEN to_date('"+usage_start_date+"','dd-MON-yyyy hh24:mi:ss') AND to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss') AND CLIENT_NO ="+client_no+" AND invoice_no is null  %s group by invoice_no,usage_type,usage_date order by usage_date) group by usage_type,invoice_no) where seq="+seq_no
			String usg_type3=""
			if(usage_type!='NoVal')
			{
				usg_type3="and usage_type= "+usage_type
			}
			String formattedQry3 = String.format(usg_qry3,usg_type3)
			ResultSet resultSet = db.executePlaQuery(formattedQry3);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();

			while (resultSet.next()){
				for(int i=1; i<=columns; i++){
					usg_summary_by_type_rec_db.put(md.getColumnName(i),resultSet.getObject(i));
				}
			}
		}


		last_usage_bill_date=db.executeQueryP2("select LAST_USAGE_BILL_DATE from ariacore.acct where acct_no="+acct_no)
		last_usg_date=formattedDate(last_usage_bill_date)


		if((billed_filter.equals("0") || billed_filter=='NoVal') && billing_period_flag.equals("1") )
		{
			logi.logInfo " Inside billed_filter=0 & billed_flag=1"
			usg_qry1="SELECT usage_type as usage_type_no,invoice_no from(SELECT usage_type,invoice_no,row_number() over(order by invoice_no) as seq FROM (SELECT usage_type,invoice_no FROM ariacore.usage WHERE acct_no="+acct_no+" AND usage_date BETWEEN to_date('"+last_usg_date+"','dd-MON-yyyy hh24:mi:ss') AND to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss') AND CLIENT_NO ="+client_no+" %s group by invoice_no,usage_type,usage_date order by usage_date) group by usage_type,invoice_no) where seq="+seq_no
			usg_type1=""
			if(usage_type!='NoVal')
			{
				usg_type1="and usage_type= "+usage_type
			}
			formattedQry1 = String.format(usg_qry1,usg_type1)
			ResultSet resultSet = db.executePlaQuery(formattedQry1);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();

			while (resultSet.next()){
				for(int i=1; i<=columns; i++){
					usg_summary_by_type_rec_db.put(md.getColumnName(i),resultSet.getObject(i));
				}
			}
		}

		if(billed_filter.equals("1") && billing_period_flag.equals("1") )
		{
			logi.logInfo " Inside billed_filter=1 & billed_flag=1"
			String usg_qry2="SELECT usage_type as usage_type_no,invoice_no from(SELECT usage_type,invoice_no,row_number() over(order by invoice_no) as seq FROM (SELECT usage_type,invoice_no FROM ariacore.usage WHERE acct_no="+acct_no+" AND usage_date BETWEEN to_date('"+last_usg_date+"','dd-MON-yyyy hh24:mi:ss') AND to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss') AND CLIENT_NO ="+client_no+" AND invoice_no is not null  %s group by invoice_no,usage_type,usage_date order by usage_date) group by usage_type,invoice_no) where seq="+seq_no
			String usg_type2=""
			if(usage_type!='NoVal')
			{
				usg_type2="and usage_type= "+usage_type
			}
			String formattedQry2 = String.format(usg_qry2,usg_type2)
			ResultSet resultSet = db.executePlaQuery(formattedQry2);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();

			while (resultSet.next()){
				for(int i=1; i<=columns; i++){
					usg_summary_by_type_rec_db.put(md.getColumnName(i),resultSet.getObject(i));
				}
			}
		}

		if(billed_filter.equals("2") && billing_period_flag.equals("1") )
		{
			logi.logInfo " Inside billed_filter=2 & billed_flag=1"
			String usg_qry3="SELECT usage_type as usage_type_no,invoice_no from(SELECT usage_type,invoice_no,row_number() over(order by invoice_no) as seq FROM (SELECT usage_type,invoice_no FROM ariacore.usage WHERE acct_no="+acct_no+" AND usage_date BETWEEN to_date('"+last_usg_date+"','dd-MON-yyyy hh24:mi:ss') AND to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss') AND CLIENT_NO ="+client_no+" AND invoice_no is null  %s group by invoice_no,usage_type,usage_date order by usage_date) group by usage_type,invoice_no) where seq="+seq_no
			String usg_type3=""
			if(usage_type!='NoVal')
			{
				usg_type3="and usage_type= "+usage_type
			}
			String formattedQry3 = String.format(usg_qry3,usg_type3)
			ResultSet resultSet = db.executePlaQuery(formattedQry3);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();

			while (resultSet.next()){
				for(int i=1; i<=columns; i++){
					usg_summary_by_type_rec_db.put(md.getColumnName(i),resultSet.getObject(i));
				}
			}
		}

		/* calculating total units,last_usage date  and total value amount for billing period flag=0 and billing period flag=1 */

		if(billing_period_flag=='NoVal' || billing_period_flag.equals("0"))
		{
			logi.logInfo" Inside billing period flag no val or 0"
			if(usg_summary_by_type_rec_db.get("INVOICE_NO").toString().equals("null"))
			{
				logi.logInfo"Inside Invoice no IS NULL"
				usg_detail1_qry="SELECT sum(usage_units) as total_units ,sum(amt) as total_value_amount,max(TO_CHAR(usage_date,'yyyy-MM-dd hh24:mi:ss')) as last_usage_date FROM ariacore.usage WHERE acct_no="+acct_no+" AND usage_date BETWEEN to_date('"+usage_start_date+"','dd-MON-yyyy hh24:mi:ss') AND to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss') AND CLIENT_NO ="+client_no+" AND invoice_no is null %s group by invoice_no"
				formattedqry11=String.format(usg_detail1_qry,usg_type1)

				ResultSet resultSet = db.executePlaQuery(formattedqry11);
				ResultSetMetaData md = resultSet.getMetaData();
				int columns = md.getColumnCount();

				while (resultSet.next()){
					for(int i=1; i<=columns; i++){
						usg_summary_by_type_rec_db.put(md.getColumnName(i),resultSet.getObject(i));
					}
				}
			}
			else
			{
				logi.logInfo"Inside Invoice no IS NOT NULL"
				usg_detail1_qry="SELECT sum(usage_units) as total_units,sum(amt) as total_value_amount,max(TO_CHAR(usage_date,'yyyy-MM-dd hh24:mi:ss')) as last_usage_date FROM ariacore.usage WHERE acct_no="+acct_no+" AND usage_date BETWEEN to_date('"+usage_start_date+"','dd-MON-yyyy hh24:mi:ss') AND to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss') AND CLIENT_NO ="+client_no+" AND invoice_no is not null %s group by invoice_no"
				formattedqry12=String.format(usg_detail1_qry,usg_type1)
				ResultSet resultSet = db.executePlaQuery(formattedqry12);
				ResultSetMetaData md = resultSet.getMetaData();
				int columns = md.getColumnCount();

				while (resultSet.next()){
					for(int i=1; i<=columns; i++){
						usg_summary_by_type_rec_db.put(md.getColumnName(i),resultSet.getObject(i));
					}
				}
			}
		}
		else
		{
			logi.logInfo" Inside billing period 1"
			if(usg_summary_by_type_rec_db.get("INVOICE_NO").toString().equals("null"))
			{
				usg_detail1_qry="SELECT sum(usage_units) as total_units ,sum(amt) as total_value_amount,max(TO_CHAR(usage_date,'yyyy-MM-dd hh24:mi:ss')) as last_usage_date FROM ariacore.usage WHERE acct_no="+acct_no+" AND usage_date BETWEEN to_date('"+last_usg_date+"','dd-MON-yyyy hh24:mi:ss') AND to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss') AND CLIENT_NO ="+client_no+" AND invoice_no is null %s group by invoice_no"
				formattedqry11=String.format(usg_detail1_qry,usg_type1)

				ResultSet resultSet = db.executePlaQuery(formattedqry11);
				ResultSetMetaData md = resultSet.getMetaData();
				int columns = md.getColumnCount();

				while (resultSet.next()){
					for(int i=1; i<=columns; i++){
						usg_summary_by_type_rec_db.put(md.getColumnName(i),resultSet.getObject(i));
					}
				}
			}
			else
			{
				usg_detail1_qry="SELECT sum(usage_units) as total_units,sum(amt) as total_value_amount,max(TO_CHAR(usage_date,'yyyy-MM-dd hh24:mi:ss')) as last_usage_date FROM ariacore.usage WHERE acct_no="+acct_no+" AND usage_date BETWEEN to_date('"+last_usg_date+"','dd-MON-yyyy hh24:mi:ss') AND to_date('"+usage_end_date+"','dd-MON-yyyy hh24:mi:ss') AND CLIENT_NO ="+client_no+" AND invoice_no is not null %s group by invoice_no"
				formattedqry12=String.format(usg_detail1_qry,usg_type1)
				ResultSet resultSet = db.executePlaQuery(formattedqry12);
				ResultSetMetaData md = resultSet.getMetaData();
				int columns = md.getColumnCount();

				while (resultSet.next()){
					for(int i=1; i<=columns; i++){
						usg_summary_by_type_rec_db.put(md.getColumnName(i),resultSet.getObject(i));
					}
				}
			}
		}

		/* Currency,usage type label and billed indicator validations  */

		def error_code = getValueFromResponse(apiname,"//error_code")
		if(!error_code.equals("1008"))
		{
			String cur_q="select currency_cd from ariacore.acct where acct_no="+acct_no
			String cur_cd=db.executeQueryP2(cur_q)
			usg_summary_by_type_rec_db.put("TOTAL_VALUE_CURRENCY_CODE",cur_cd)

			if(usg_summary_by_type_rec_db.get("INVOICE_NO").toString().equals("null"))
				usg_summary_by_type_rec_db.put("BILLED_IND","2")
			else
				usg_summary_by_type_rec_db.put("BILLED_IND","1")

			usg_summary_by_type_rec_db.remove("INVOICE_NO")

			String usg_type_q="select description from ariacore.usage_types where usage_type="+usg_summary_by_type_rec_db.get("USAGE_TYPE_NO")+" and (custom_to_client_no="+client_no+" or custom_to_client_no is null)"
			String usg_type_lb=db.executeQueryP2(usg_type_q)
			usg_summary_by_type_rec_db.put("USAGE_TYPE_LABEL",usg_type_lb)
			
			String usg_type_cd_q="select usage_type_code from ariacore.usage_types where usage_type="+usg_summary_by_type_rec_db.get("USAGE_TYPE_NO")+" and custom_to_client_no="+client_no
			String usg_type_cd_lb=db.executeQueryP2(usg_type_cd_q)
			usg_summary_by_type_rec_db.put("USAGE_TYPE_CD",usg_type_cd_lb)
		}
		logi.logInfo "Usage summary by type records Db hash " +usg_summary_by_type_rec_db.toString()
		return usg_summary_by_type_rec_db.sort()
	}
	/**
	 * Getting the usage rec_no
	 * @param tcid
	 * @return String
	 */
	String md_RECORD_USAGE_REC_NO(String tcid)
	{
		ConnectDB db=new ConnectDB()
		String apiname=tcid.split("-")[2]
		String senior_acct_no,res_level_cd
		String acct_no = getValueFromRequest(apiname,"//acct_no")
		String acct_no_qry = "Select senior_acct_no,resp_level_cd from ariacore.acct where acct_no="+acct_no
		ResultSet rs_acct_no_qry = db.executePlaQuery(acct_no_qry);
		while(rs_acct_no_qry.next())
		{
			senior_acct_no = rs_acct_no_qry.getString(1)
			res_level_cd = rs_acct_no_qry.getString(2)
		}

		if ((senior_acct_no != "null" && res_level_cd == "3") || (senior_acct_no != "null" && res_level_cd == "4"))
		{
			logi.logInfo("this is child account")
			acct_no = senior_acct_no
		}

		String rec_no=db.executeQueryP2("SELECT rec_no from ariacore.usage where acct_no="+acct_no+" ORDER BY REC_NO DESC").toString()
		return rec_no
	}

	public String md_Is_Usage_Pooled(String tcid)
	{
		def acct_no = getValueFromResponse("create_acct_complete","//acct_no")
		return md_Is_Usage_Pooled(tcid,acct_no)
	}
	/**
	 * Returns True/false value based on 'ENABLE_USAGE_POOLING' parameter value
	 * @param tcid
	 * @return String
	 */
	public String md_Is_Usage_Pooled(String tcid,String acct_no)
	{
		logi.logInfo("Calling md_Is_Usage_Pooled ")
		ConnectDB db=new ConnectDB()
		boolean uflag = false
		int pooled_plans_count=db.executeQueryP2("select count(plan_no)  from ariacore.acct_plan_params where acct_no="+acct_no+" and param_name='ENABLE_USAGE_POOLING' and param_val='TRUE'").toString().toInteger()
		logi.logInfo( "pooled_plans_count "+pooled_plans_count)
		if(pooled_plans_count.toInteger()>0)
		{
			uflag=true
		}
		return uflag.toString().toUpperCase()
	}
	/**
	 * Returns True/false value based on 'CALC_USAGE_USING_RECUR_FACTOR' parameter value
	 * @param tcid
	 * @return String
	 */
	public String md_Is_Usage_Mutiplier_Enabled(String plan_no,String service_no)
	{
		logi.logInfo("Calling md_Is_Usage_Mutiplier_Enabled ")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db=new ConnectDB()
		boolean uflag = false
		String multiplier_index=db.executeQueryP2("select CALC_USAGE_USING_RECUR_FACTOR from ariacore.plan_services where plan_no="+plan_no+" and service_no="+service_no+" and client_no="+clientNo).toString()
		logi.logInfo( "multiplier_index "+multiplier_index)
		if(multiplier_index =="1")
		{
			uflag=true
		}
		return uflag.toString().toUpperCase()
	}
	/**
	 * Getting the expected invoice amount after the coupon is applied
	 * @param tcid
	 * @return String
	 */
	String md_ExpInvoiceWithUsage_Amt_After_Coupon(String tcid)
	{
		
		logi.logInfo "Calling md_ExpInvoiceWithUsage_Amt_After_Coupon..."
		String invAmount = md_ExpInvoiceWithUsage_Amt(tcid)
		logi.logInfo "Invoice with out Coupon: " + invAmount

		//Calculating Coupon Alone
		//Getting the coupon code from the create aacct complete request
		String coupon_code =   getValueFromRequest("create_acct_complete","//coupon_codes")
		String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
		logi.logInfo "Coupon Applied: " + coupon_code

		//Getting the coupon scope
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()

		String expected_coupon_discount

		// Coupon Type Finding
		String Coupon_Type = db.executeQueryP2("select DType from ( SELECT CASE WHEN NVL(SUM(RECURRING_CREDIT_TEMPLATE_NO),0) > 0 Then 'CT' end as DType FROM ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE Client_no = "+ clientNo +" AND Coupon_cd   ='"+coupon_code +"' UNION ALL select CASE  When NVL(SUM(RULE_NO),0) > 0 Then 'DR' End as DType from ariacore.coupon_discount_rule_map where Client_no =" +clientNo +" AND Coupon_cd   ='"+ coupon_code +"' UNION ALL SELECT  case when NVL(SUM(bundle_no),0) > 0 then 'DB' end as Dtype FROM ariacore.coupon_discount_bundle_map WHERE Client_no = "+clientNo+" AND Coupon_cd='"+coupon_code+"') where Dtype is not null")

		// DISCOUNT RULE
		if (Coupon_Type == "DR")
		{

			String scope_no=db.executeQueryP2("SELECT Scope_No from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")
			//All Plan Charges, Specified Plans
			if (scope_no == "11")
			{

			}

			//All Plans, Specified Service Charges
			else if (scope_no == "12")
			{
				//Flat or Percentage
				String flat_or_percent=db.executeQueryP2("SELECT flat_percent_ind from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")
				//Inline or Percent
				String inline_or_offset=db.executeQueryP2("SELECT INLINE_OFFSET_IND from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")
				//Finding Debit after coupon applied
				if (flat_or_percent == "F")
					expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit  THEN 0 ELSE a.act_debit  END AS exp_debit FROM (SELECT (0 + (SELECT amount FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' AND Client_no="+clientNo +" AND RULE_NO = (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = " + clientNo+" AND coupon_cd = '"+ coupon_code+ "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no  in (select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND USAGE_RATE != 0 AND service_no IN(SELECT KEY_1_VAL FROM ariacore.client_dr_components WHERE rule_no IN (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+clientNo+" AND coupon_cd ='"+coupon_code+"') AND key_1_type = 'SERVICE_NO') ) a)")
				else
				{
					if (inline_or_offset == "I")
						expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit  THEN 0 ELSE a.act_debit  END AS exp_debit FROM (SELECT ((usage_units * pre_discount_unit_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO = (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = " + clientNo+" AND coupon_cd = '"+ coupon_code+ "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no  in (select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(SELECT KEY_1_VAL FROM ariacore.client_dr_components WHERE rule_no IN (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+clientNo+" AND coupon_cd ='"+coupon_code+"') AND key_1_type = 'SERVICE_NO') ) a)")
					else
						expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit  THEN 0 ELSE a.act_debit  END AS exp_debit FROM (SELECT ((usage_units * usage_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +"  AND RULE_NO = (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = " + clientNo+" AND coupon_cd = '"+ coupon_code+ "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no  in (select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(SELECT KEY_1_VAL FROM ariacore.client_dr_components WHERE rule_no IN (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+clientNo+" AND coupon_cd ='"+coupon_code+"') AND key_1_type = 'SERVICE_NO') ) a)")
				}
				logi.logInfo "Expected invoice debit: "  + expected_coupon_discount
			}

			//Specified Plan/Service Charges
			else if (scope_no == "13")
			{

				//Flat or Percentage
				String flat_or_percent=db.executeQueryP2("SELECT flat_percent_ind from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")

				//Inline or Percent
				String inline_or_offset=db.executeQueryP2("SELECT INLINE_OFFSET_IND from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")

				if (flat_or_percent == "F")
					expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit  THEN 0 ELSE a.act_debit  END AS exp_debit FROM (SELECT (0 + (SELECT amount FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' AND Client_no="+clientNo +" AND RULE_NO = (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = " + clientNo+" AND coupon_cd = '"+ coupon_code+ "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no  in (select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND USAGE_RATE != 0 AND service_no IN(SELECT KEY_2_VAL FROM ariacore.client_dr_components WHERE rule_no IN (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+clientNo+" AND coupon_cd ='"+coupon_code+"') AND key_2_type = 'SERVICE_NO') AND plan_no IN (SELECT KEY_1_VAL FROM ariacore.client_dr_components WHERE rule_no IN (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   ='"+ coupon_code +"') AND key_1_type = 'PLAN_NO' ) ) a)")
				else
				{
					if (inline_or_offset == "I")
						expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit  THEN 0 ELSE a.act_debit  END AS exp_debit FROM (SELECT ((usage_units * pre_discount_unit_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +"  AND RULE_NO = (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = " + clientNo+" AND coupon_cd = '"+ coupon_code+ "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no  in (select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(SELECT KEY_2_VAL FROM ariacore.client_dr_components WHERE rule_no IN (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+clientNo+" AND coupon_cd ='"+coupon_code+"') AND key_2_type = 'SERVICE_NO') AND plan_no IN (SELECT KEY_1_VAL FROM ariacore.client_dr_components WHERE rule_no IN (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   ='"+ coupon_code +"') AND key_1_type = 'PLAN_NO' ) ) a)")
					else
						expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit  THEN 0 ELSE a.act_debit  END AS exp_debit FROM (SELECT ((usage_units * usage_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO = (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = " + clientNo+" AND coupon_cd = '"+ coupon_code+ "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no  in (select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(SELECT KEY_2_VAL FROM ariacore.client_dr_components WHERE rule_no IN (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+clientNo+" AND coupon_cd ='"+coupon_code+"') AND key_2_type = 'SERVICE_NO') AND plan_no IN (SELECT KEY_1_VAL FROM ariacore.client_dr_components WHERE rule_no IN (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   ='"+ coupon_code +"') AND key_1_type = 'PLAN_NO' ) ) a)")
				}
				logi.logInfo "Expected invoice debit: "  + expected_coupon_discount

			}

			//All Recurring Service Charges
			else if (scope_no == "21")
			{
				//Flar or Percentage
				String flat_or_percent=db.executeQueryP2("SELECT flat_percent_ind from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")
				//Inline or Percent
				String inline_or_offset=db.executeQueryP2("SELECT INLINE_OFFSET_IND from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")

				//Finding Debit after coupon applied
				if (flat_or_percent == "F")
					expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * usage_rate)- (SELECT amount FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND USAGE_RATE != 0 AND service_no IN(select service_no from ARIACORE.services where recurring = 1 ) ) a  )")
				else
				{
					if (inline_or_offset == "O")
						expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * usage_rate) - (usage_units * usage_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where recurring = 1 ) ) a  )")
					else
						expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * pre_discount_unit_rate) - (usage_units * pre_discount_unit_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where recurring = 1 ) ) a  )")
				}
				logi.logInfo "Expected invoice debit: "  + expected_coupon_discount
			}

			//All Usage Service Charges
			else if (scope_no == "22")
			{
				//Flar or Percentage
				String flat_or_percent=db.executeQueryP2("SELECT flat_percent_ind from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")
				//Inline or Percent
				String inline_or_offset=db.executeQueryP2("SELECT INLINE_OFFSET_IND from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")


				//Finding Debit after coupon applied
				if (flat_or_percent == "F")
					expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT (0 + (SELECT amount FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND USAGE_RATE != 0 AND service_no IN(select service_no from ARIACORE.services where usage_based = 1 ) ) a  )")
				else
				{
					if (inline_or_offset == "O")
						expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * usage_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where usage_based = 1 ) ) a  )")
					else
						expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * pre_discount_unit_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where usage_based = 1 ) ) a  )")
				}
				logi.logInfo "Expected invoice debit: "  + expected_coupon_discount
			}

			//All Activation Service Charges
			else if (scope_no == "23")
			{
				//Flar or Percentage
				String flat_or_percent=db.executeQueryP2("SELECT flat_percent_ind from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")
				//Inline or Percent
				String inline_or_offset=db.executeQueryP2("SELECT INLINE_OFFSET_IND from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")

				//Finding Debit after coupon applied
				if (flat_or_percent == "F")
					expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * usage_rate)- (SELECT amount FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND USAGE_RATE != 0 AND service_no IN(select service_no from ARIACORE.services where is_setup = 1 ) ) a  )")
				else
				{
					if (inline_or_offset == "O")
						expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * usage_rate) - (usage_units * usage_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where is_setup = 1 ) ) a  )")
					else
						expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * pre_discount_unit_rate) - (usage_units * pre_discount_unit_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where is_setup = 1 ) ) a  )")
				}
				logi.logInfo "Expected coupon discount: "  + expected_coupon_discount
			}
		}

		// CREDIT TEMPLATE
		if (Coupon_Type == "CT")
		{
			//Get the scope
			String specific_plan = db.executeQueryP2("SELECT eligible_plan_no FROM ariacore.RECURRING_CREDIT_TEMPLATES WHERE RECURRING_CREDIT_TEMPLATE_NO =(SELECT RECURRING_CREDIT_TEMPLATE_NO FROM ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP   WHERE Client_no = "+clientNo+" AND Coupon_cd   ='"+coupon_code+"'  )")
			String specific_service_type = db.executeQueryP2("select SERVICE_TYPE_CD from recur_credit_tmplt_srvc_tp where recurring_credit_template_no in (SELECT RECURRING_CREDIT_TEMPLATE_NO FROM ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP   WHERE Client_no = "+clientNo+" AND Coupon_cd   ='"+coupon_code+"'  )")
			logi.logInfo "Credit Template"
			//Specific Plan and Charge
			if (specific_plan != null)
			{
				logi.logInfo "Specific Plan / Charges"
				//Get service scope
				String specific_service = db.executeQueryP2("SELECT eligible_service_no FROM ariacore.RECURRING_CREDIT_TEMPLATES WHERE RECURRING_CREDIT_TEMPLATE_NO =(SELECT RECURRING_CREDIT_TEMPLATE_NO FROM ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP   WHERE Client_no = "+clientNo+" AND Coupon_cd   ='"+coupon_code+"'  )")

				//Flat or Percentage
				String isFlatType = db.executeQueryP2("SELECT flat_amount FROM ariacore.RECURRING_CREDIT_TEMPLATES WHERE RECURRING_CREDIT_TEMPLATE_NO =(SELECT RECURRING_CREDIT_TEMPLATE_NO FROM ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP   WHERE Client_no = "+clientNo+" AND Coupon_cd   ='"+coupon_code+"'  )")
				//Flat
				if (isFlatType != null)
				{
					logi.logInfo "Flat Type"
					expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE  WHEN 0 >= a.act_debit  THEN 0  ELSE a.act_debit  END AS exp_debit FROM (SELECT (0 +      (SELECT flat_amount FROM ariacore.RECURRING_CREDIT_TEMPLATES where RECURRING_CREDIT_TEMPLATE_NO =(SELECT RECURRING_CREDIT_TEMPLATE_NO FROM ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE Client_no = " + clientNo+" AND coupon_cd   = '"+ coupon_code+ "' ) and client_no="+clientNo+")) AS act_debit FROM ariacore.gl_detail WHERE invoice_no IN (SELECT MAX(invoice_no) FROM ariacore.gl WHERE acct_no="+ accountNo+" ) AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND USAGE_RATE != 0 AND service_no IN ( "+specific_service+" ) AND plan_no in ("+specific_plan+") ) a )")
				}
				//Percent
				else
				{
					logi.logInfo "Percentage Type"
					String per_specific_plan = db.executeQueryP2("SELECT percent_eval_plan_no FROM ariacore.RECURRING_CREDIT_TEMPLATES WHERE RECURRING_CREDIT_TEMPLATE_NO =(SELECT RECURRING_CREDIT_TEMPLATE_NO FROM ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP   WHERE Client_no = "+clientNo+" AND Coupon_cd   ='"+coupon_code+"'  )")
					String per_specific_service = db.executeQueryP2("SELECT percent_eval_service_no FROM ariacore.RECURRING_CREDIT_TEMPLATES WHERE RECURRING_CREDIT_TEMPLATE_NO =(SELECT RECURRING_CREDIT_TEMPLATE_NO FROM ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP   WHERE Client_no = "+clientNo+" AND Coupon_cd   ='"+coupon_code+"'  )")
					expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE  WHEN 0 >= a.act_debit  THEN 0  ELSE a.act_debit  END AS exp_debit FROM (SELECT ((usage_units * usage_rate) * (SELECT percent_amount/100 FROM ariacore.RECURRING_CREDIT_TEMPLATES where RECURRING_CREDIT_TEMPLATE_NO =(SELECT RECURRING_CREDIT_TEMPLATE_NO FROM ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE Client_no = " + clientNo+" AND coupon_cd   = '"+ coupon_code+ "' ) and client_no="+clientNo+" )) AS act_debit FROM ariacore.gl_detail WHERE invoice_no IN (SELECT MAX(invoice_no) FROM ariacore.gl WHERE acct_no="+ accountNo+" ) AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND USAGE_RATE != 0 AND service_no IN ( "+per_specific_service+" ) AND plan_no in ("+per_specific_plan+") ) a )")
				}
				logi.logInfo "Expected coupon discount: "  + expected_coupon_discount
			}
			//Specific Service Type
			else if (specific_service_type != null)
			{
				logi.logInfo "Sepcifice Service Type"
				//For Usage Type Services
				if (specific_service_type == "US")
				{
					logi.logInfo "Usage Service Type"
					logi.logInfo "Flat Type"
					expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE  WHEN 0 >= a.act_debit  THEN 0  ELSE a.act_debit  END AS exp_debit FROM (SELECT (0 +      (SELECT flat_amount FROM ariacore.RECURRING_CREDIT_TEMPLATES where RECURRING_CREDIT_TEMPLATE_NO =(SELECT RECURRING_CREDIT_TEMPLATE_NO FROM ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE Client_no = " + clientNo+" AND coupon_cd   = '"+ coupon_code+ "' ) and client_no="+clientNo+" )) AS act_debit FROM ariacore.gl_detail WHERE invoice_no IN (SELECT MAX(invoice_no) FROM ariacore.gl WHERE acct_no="+ accountNo+" ) AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND USAGE_RATE != 0 AND service_no IN (select max(service_no) from ariacore.gl_detail where invoice_no in (SELECT MAX(invoice_no) FROM ariacore.gl WHERE acct_no="+accountNo+") and service_no in (SELECT service_no FROM ARIACORE.services WHERE usage_based = 1))  ) a )")
				}
				logi.logInfo "Expected coupon discount: "  + expected_coupon_discount
			}
			//All Charges
			else
			{
				logi.logInfo "All Charges - Flat Type"
				expected_coupon_discount=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE  WHEN 0 >= a.act_debit  THEN 0  ELSE a.act_debit  END AS exp_debit FROM (SELECT (0 +      (SELECT flat_amount FROM ariacore.RECURRING_CREDIT_TEMPLATES where RECURRING_CREDIT_TEMPLATE_NO =(SELECT RECURRING_CREDIT_TEMPLATE_NO FROM ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE Client_no = " + clientNo+" AND coupon_cd   = '"+ coupon_code+ "' ) and client_no="+clientNo+")) AS act_debit FROM ariacore.gl_detail WHERE invoice_no IN (SELECT MAX(invoice_no) FROM ariacore.gl WHERE acct_no="+ accountNo+" ) AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND USAGE_RATE != 0 AND service_no IN (select max(service_no) from ariacore.gl_detail where invoice_no in (SELECT MAX(invoice_no) FROM ariacore.gl WHERE acct_no="+accountNo+") and service_no in (SELECT service_no FROM ARIACORE.services))  ) a )")
				logi.logInfo "Expected coupon discount: "  + expected_coupon_discount
			}


		}
		DecimalFormat d = new DecimalFormat("#.##########");
		String cal_amt = d.format((invAmount.toDouble() -  expected_coupon_discount.toDouble()).toDouble().round(2)).toString()
		return cal_amt

	}
	/**
	 * Getting the coupon discount amount
	 * @param tcid
	 * @return String
	 */
	String md_GET_COUPON_DISCOUNTED_AMOUNT(String tcid)
	{
		logi.logInfo "Calling md_GET_COUPON_DISCOUNTED_AMOUNT..."
		ConnectDB db = new ConnectDB()
		//Getting the coupon code from the create aacct complete request
		String coupon_code
		coupon_code =   getValueFromRequest("create_acct_complete","//coupon_codes")
		String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
		if(coupon_code=="NoVal")
		coupon_code =db.executeQueryP2("select coupon_cd from ariacore.acct_coupons where acct_no="+accountNo)		
		logi.logInfo "Coupon Applied: " + coupon_code

		//Getting the coupon scope
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		
		String scope_no=db.executeQueryP2("SELECT Scope_No from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")
		String expected_invoice_debit

		//All Plan Charges, Specified Plans
		if (scope_no == "11")
		{

		}

		//All Plans, Specified Service Charges
		else if (scope_no == "12")
		{
			//Flat or Percentage
			String flat_or_percent=db.executeQueryP2("SELECT flat_percent_ind from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")
			//Inline or Percent
			String inline_or_offset=db.executeQueryP2("SELECT INLINE_OFFSET_IND from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")
			//Finding Debit after coupon applied
			if (flat_or_percent == "F")
				expected_invoice_debit=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit  THEN 0 ELSE a.act_debit  END AS exp_debit FROM (SELECT ((usage_units * usage_rate)- (SELECT amount FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' AND Client_no="+clientNo +" AND RULE_NO = (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = " + clientNo+" AND coupon_cd = '"+ coupon_code+ "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no  in (select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(SELECT KEY_1_VAL FROM ariacore.client_dr_components WHERE rule_no IN (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+clientNo+" AND coupon_cd ='"+coupon_code+"') AND key_1_type = 'SERVICE_NO') UNION ALL SELECT ((usage_units * usage_rate)) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in (select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no NOT IN (SELECT KEY_1_VAL FROM ariacore.client_dr_components WHERE rule_no IN(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = " + clientNo+ " AND coupon_cd   = '"+coupon_code+"') AND key_1_type = 'SERVICE_NO' )) a)")
			else
			{
				if (inline_or_offset == "I")
					expected_invoice_debit=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit  THEN 0 ELSE a.act_debit  END AS exp_debit FROM (SELECT ((usage_units * pre_discount_unit_rate) - (usage_units * pre_discount_unit_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO = (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = " + clientNo+" AND coupon_cd = '"+ coupon_code+ "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no  in (select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(SELECT KEY_1_VAL FROM ariacore.client_dr_components WHERE rule_no IN (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+clientNo+" AND coupon_cd ='"+coupon_code+"') AND key_1_type = 'SERVICE_NO') UNION ALL SELECT ((usage_units * usage_rate)) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in (select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no NOT IN (SELECT KEY_1_VAL FROM ariacore.client_dr_components WHERE rule_no IN(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = " + clientNo+ " AND coupon_cd   = '"+coupon_code+"') AND key_1_type = 'SERVICE_NO' )) a)")
				else
					expected_invoice_debit=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit  THEN 0 ELSE a.act_debit  END AS exp_debit FROM (SELECT ((usage_units * usage_rate) - (usage_units * usage_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO = (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = " + clientNo+" AND coupon_cd = '"+ coupon_code+ "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no  in (select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(SELECT KEY_1_VAL FROM ariacore.client_dr_components WHERE rule_no IN (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+clientNo+" AND coupon_cd ='"+coupon_code+"') AND key_1_type = 'SERVICE_NO') UNION ALL SELECT ((usage_units * usage_rate)) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in (select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no NOT IN (SELECT KEY_1_VAL FROM ariacore.client_dr_components WHERE rule_no IN(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = " + clientNo+ " AND coupon_cd   = '"+coupon_code+"') AND key_1_type = 'SERVICE_NO' )) a)")
			}
			logi.logInfo "Expected invoice debit: "  + expected_invoice_debit
		}

		//Specified Plan/Service Charges
		else if (scope_no == "13")
		{

		}

		//All Recurring Service Charges
		else if (scope_no == "21")
		{
			//Flat or Percentage
			String flat_or_percent=db.executeQueryP2("SELECT flat_percent_ind from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")
			//Inline or Percent
			String inline_or_offset=db.executeQueryP2("SELECT INLINE_OFFSET_IND from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")

			//Finding Debit after coupon applied
			if (flat_or_percent == "F")
				expected_invoice_debit=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * usage_rate)- (SELECT amount FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where recurring = 1 ) ) a  )")
			else
			{
				if (inline_or_offset == "O")
					expected_invoice_debit=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * usage_rate) - (usage_units * usage_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where recurring = 1 ) ) a  )")
				else
					expected_invoice_debit=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * pre_discount_unit_rate) - (usage_units * pre_discount_unit_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where recurring = 1 ) ) a  )")
			}
			logi.logInfo "Expected invoice debit: "  + expected_invoice_debit
		}

		//All Usage Service Charges
		else if (scope_no == "22")
		{
			//Flar or Percentage
			String flat_or_percent=db.executeQueryP2("SELECT flat_percent_ind from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")
			//Inline or Percent
			String inline_or_offset=db.executeQueryP2("SELECT INLINE_OFFSET_IND from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")


			//Finding Debit after coupon applied
			if (flat_or_percent == "F")
				expected_invoice_debit=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * usage_rate)- (SELECT amount FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where usage_based = 1 ) ) a  )")
			else
			{
				if (inline_or_offset == "O")
					expected_invoice_debit=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * usage_rate) - (usage_units * usage_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where usage_based = 1 ) ) a  )")
				else
					expected_invoice_debit=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * pre_discount_unit_rate) - (usage_units * pre_discount_unit_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where usage_based = 1 ) ) a  )")
			}
			logi.logInfo "Expected invoice debit: "  + expected_invoice_debit
		}

		//All Activation Service Charges
		else if (scope_no == "23")
		{
			//Flar or Percentage
			String flat_or_percent=db.executeQueryP2("SELECT flat_percent_ind from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")
			//Inline or Percent
			String inline_or_offset=db.executeQueryP2("SELECT INLINE_OFFSET_IND from ariacore.client_discount_rules where rule_no in (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo + " AND coupon_cd   = '" + coupon_code + "')")

			//Finding Debit after coupon applied
			if (flat_or_percent == "F")
				expected_invoice_debit=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * usage_rate)- (SELECT amount FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where is_setup = 1 ) ) a  )")
			else
			{
				if (inline_or_offset == "O")
					expected_invoice_debit=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * usage_rate) - (usage_units * usage_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where is_setup = 1 ) ) a  )")
				else
					expected_invoice_debit=db.executeQueryP2("SELECT NVL(SUM(exp_debit),0) FROM (SELECT CASE WHEN 0 >= a.act_debit THEN 0 ELSE a.act_debit END AS exp_debit FROM (SELECT ((usage_units * pre_discount_unit_rate) - (usage_units * pre_discount_unit_rate)* (SELECT amount/100 FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND Client_no="+clientNo +" AND RULE_NO =(SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+ clientNo +" AND coupon_cd   = '"+ coupon_code + "'))) AS act_debit FROM ariacore.gl_detail WHERE invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ accountNo+") AND plan_no IS NOT NULL AND orig_coupon_cd IS NULL AND service_no IN(select service_no from ARIACORE.services where is_setup = 1 ) ) a  )")
			}
			logi.logInfo "Expected invoice debit: "  + expected_invoice_debit
		}
		return expected_invoice_debit
	}


	public def md_IsPrevInvVoidedInPlanChange2(String tcid)
	{
		return md_IsPrevInvVoidedInPlanChange(tcid,"2")
	}
	//public def md_IsPrevInvVoidedInPlanChange1(String tcid)
	//{
		//return md_IsPrevInvVoidedInPlanChange(tcid,"1")
	//}
	/**
	 * Returns true/false based on void transaction no
	 * @param tcid
	 * @param seq_no
	 * @return String
	 */
	def md_IsPrevInvVoidedInPlanChange(String tcid,String seq_no)
	{
		logi.logInfo "Entered into md_IsPrevInvVoidedInPlanChange "
		ConnectDB db=new ConnectDB()
		boolean voided = false
		def acctno=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def invoiceno= getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String voided_q="SELECT COUNT(*) FROM ARIACORE.ACCT_TRANSACTION WHERE EVENT_NO IN(SELECT VOIDING_EVENT_NO FROM((SELECT VOIDING_EVENT_NO,ROW_NUMBER() OVER(ORDER BY INVOICE_NO) AS NUM  FROM ARIACORE.GL WHERE ACCT_NO="+acctno+")A)WHERE NUM="+seq_no+")AND TRANSACTION_TYPE IN(-1) AND ACCT_NO="+acctno
		String voidflag=db.executeQueryP2(voided_q)
		if(voidflag.equals("1"))
			voided= true
		return voided.toString().toUpperCase()
	}
	def md_IsPrevInvVoidedInPlanChangechild(String tcid,String seq_no)
	{
		logi.logInfo "Entered into md_IsPrevInvVoidedInPlanChange "
		ConnectDB db=new ConnectDB()
		boolean voided = false
		def acctno=getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def invoiceno= getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String voided_q="SELECT COUNT(*) FROM ARIACORE.ACCT_TRANSACTION WHERE EVENT_NO IN(SELECT VOIDING_EVENT_NO FROM((SELECT VOIDING_EVENT_NO,ROW_NUMBER() OVER(ORDER BY INVOICE_NO) AS NUM  FROM ARIACORE.GL WHERE ACCT_NO="+acctno+")A)WHERE NUM="+seq_no+")AND TRANSACTION_TYPE IN(-1) AND ACCT_NO="+acctno
		String voidflag=db.executeQueryP2(voided_q)
		if(voidflag.equals("1"))
			voided= true
		return voided.toString().toUpperCase()
	}
	
	
	/**
	 * Getting the expected bill date for the account
	 * @param tcid
	 * @return String date
	 */
	def md_ExpBillDateOfAcct(String tcid)
	{
		logi.logInfo "Entered into md_ExpBillDateOfAcct "
		ConnectDB db=new ConnectDB()
		String apiname="create_acct_complete"
		def acctno=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String created_date_q="select created from ariacore.acct WHERE ACCT_NO="+acctno
		def created_date = db.executeQueryP2(created_date_q)
		def retro_date = getValueFromRequest(apiname,"//retroactive_start_date")
		def client_no = getValueFromRequest(apiname,"//client_no")
		String plan_no_q1 = "select plan_no from ariacore.acct where acct_no="+acctno+"  and client_no="+client_no
		String plan_no=db.executeQueryP2(plan_no_q1)
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Calendar now = Calendar.getInstance();

		if(!retro_date.equals("NoVal"))
		{
			created_date=retro_date
		}
		else{
			created_date=created_date
		}

		Date date = dateFormat.parse(created_date);
		logi.logInfo " created date set as " +created_date
		now.setTime(date)

		String bill_interval_q="select billing_interval from ariacore.client_plan where plan_no="+plan_no+" and client_no="+client_no
		String bill_interval=db.executeQueryP2(bill_interval_q)
		logi.logInfo " bill interval is " + bill_interval
		def lag_name,actual_bill_date,new_bill_date

		if(bill_interval.equals("1"))
			lag_name="MONTHLY_BILL_LAG_DAYS"
		else if(bill_interval.equals("3"))
			lag_name="QUARTERLY_BILL_LAG_DAYS"
		else if(bill_interval.equals("6"))
			lag_name="SEMI_ANNUAL_BILL_LAG_DAYS"
		else if(bill_interval.equals("12"))
			lag_name="ANNUAL_BILL_LAG_DAYS"
		else
			lag_name="CUSTOM_BILL_LAG_DAYS"

		String bill_lag_q="Select param_val from ariacore.aria_client_params where client_no ="+client_no+" and param_name='"+lag_name+"'"
		String bill_lag_day=db.executeQueryP2(bill_lag_q)
		logi.logInfo " blill ag" + bill_lag_day
		if(bill_lag_day == null)
			bill_lag_day=0

		String cur_time=dateFormat.format(now.getTime())
		logi.logInfo "Current time before adding bill interval is " + cur_time

		int bill_inter=Integer.parseInt(bill_interval)
		now.add(Calendar.MONTH,bill_inter)
		logi.logInfo "Date after adding bill interval is " + dateFormat.format(now.getTime())

		int bill_lag=Integer.parseInt(bill_lag_day)
		now.add(Calendar.DATE,bill_lag)
		logi.logInfo "Date after adding bill lag day is " + dateFormat.format(now.getTime())

		SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-YYYY");
		String date1 = format1.format(now.getTime());
		
		logi.logInfo " new bill date after adding bill interval and bill lag day is  " + format1.format(now.getTime())
		return date1
	}
	/**
	 * Getting the expected bill date for the account including bill lag days
	 * @param tcid
	 * @return String date
	 */
	def md_GetBillDateOfAccountInBillLagDay(String tcid)
	{
		logi.logInfo " Entered into md_GetBillDateOfAccountInBillLagDay "
		ConnectDB db=new ConnectDB()
		def acctno=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String ex_bill_date_q="select to_char(last_bill_date,'dd-Mon-YYYY') from ariacore.acct  where acct_no="+acctno
		String exp_bill_date=db.executeQueryP2(ex_bill_date_q)
		return exp_bill_date
	}

	/**
	 * Getting the expected bill thru date for the account
	 * @param tcid
	 * @return String date
	 */
	def md_ExpBillThruDateOfAcct(String tcid)
	{
		logi.logInfo "Entered into md_ExpBillThruDateOfAcct "
		ConnectDB db=new ConnectDB()
		String apiname="create_acct_complete"
		def acctno=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String bill_date_q1="select last_bill_date from ariacore.acct WHERE ACCT_NO="+acctno
		def bill_date_q2 = db.executeQueryP2(bill_date_q1)
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		Date date = dateFormat.parse(bill_date_q2);
		Calendar now = Calendar.getInstance();
		now.setTime(date)
		String curr_time=dateFormat.format(now.getTime())
		logi.logInfo "Current bill date from method  before adding bill interval is " + curr_time

		def plan_no = getValueFromRequest(apiname,"//master_plan_no")
		def client_no = getValueFromRequest(apiname,"//client_no")
		def bill_lag_day,lag_name,actual_bill_date
		String bill_int_q1="select billing_interval from ariacore.client_plan where plan_no="+plan_no+" and client_no="+client_no
		String bill_intl=db.executeQueryP2(bill_int_q1)
		logi.logInfo " bill interval is " + bill_intl

		if(bill_intl.equals("1"))
			lag_name="MONTHLY_BILL_LAG_DAYS"
		else if(bill_intl.equals("3"))
			lag_name="QUARTERLY_BILL_LAG_DAYS"
		else if(bill_intl.equals("6"))
			lag_name="SEMI_ANNUAL_BILL_LAG_DAYS"
		else if(bill_intl.equals("12"))
			lag_name="ANNUAL_BILL_LAG_DAYS"
		else
			lag_name="CUSTOM_BILL_LAG_DAYS"

		String bill_lag_q="Select param_val from ariacore.aria_client_params where client_no ="+client_no+" and param_name='"+lag_name+"'"
		bill_lag_day=db.executeQueryP2(bill_lag_q)

		int bill_lag=Integer.parseInt(bill_lag_day)
		bill_lag=Math.abs(bill_lag)
		now.add(Calendar.DATE,bill_lag)
		logi.logInfo " Date after adding bill lag day " + dateFormat.format(now.getTime())

		int bill_interl=Integer.parseInt(bill_intl)
		now.add(Calendar.MONTH,bill_interl)
		logi.logInfo " Added bill interval to current date " + dateFormat.format(now.getTime())

		now.add(Calendar.DATE,-1)
		logi.logInfo " Reduced one day to current date " + dateFormat.format(now.getTime())

		SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-YYYY");
		String date3 = format2.format(now.getTime())
		logi.logInfo " new bill thru date after adding bill lag day and bill interval is  " + now.getTime()
		return date3
	}
	
	/**
	 * Getting the expected bill thru date for the account including bill lag days
	 * @param tcid
	 * @return String date
	 */
	def md_GetBillThruDateOfAccountInBillLagDay(String tcid)
	{
		logi.logInfo " Entered into md_GetBillThruDateOfAccountInBillLagDay "
		ConnectDB db=new ConnectDB()
		def acctno=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String ex_bill_thru_date_q="select to_char(last_bill_thru_date,'dd-Mon-yyyy') from ariacore.acct  where acct_no="+acctno
		String exp_bill_thru_date=db.executeQueryP2(ex_bill_thru_date_q)
		return exp_bill_thru_date
	}
	/**
	 * Getting the proration result amount (including bill lag days) for the supplemental plan
	 * @param tcid
	 * @return String proration amount
	 */
	public String md_SuppPlanProrationResultAmountBillLagDay(String tcid)
	{
		DecimalFormat d = new DecimalFormat("#.##########")
					String apiname=tcid.split("-")[2]
					String usg_amt
					logi.logInfo("inside md_SuppPlanProrationResultAmountBillLagDay")
					ReadData objRD =new ReadData()
					logi.logInfo " tc id " +tcid
					String localTcId = tcid.split("-")[1]
					logi.logInfo " local tc id " +localTcId
					def ad=getValueFromRequest(apiname,"//assignment_directive")
					def acct_no=getValueFromRequest(apiname,"//acct_no")
					
					String ClearHashVal = objRD.getTestDataMap("create_acct_complete",localTcId).get("Clear_UsageHash")
					logi.logInfo "Usage Clear Hash Value " + ClearHashVal
					String credit_amt=md_proration_result_amount(tcid)
					logi.logInfo("supp credit_amt:"+credit_amt)
					usg_amt = md_ExpUsage_Amt(tcid)
					
					if(ad.equals("3") || ad.equals("6") || ad.equals("8")|| ad.equals("11") || usg_amt== null)
					usg_amt="0"
																	
					logi.logInfo("supp usage amt:"+usg_amt)
					String proration_amt = d.format((usg_amt.toDouble()+credit_amt.toDouble()).toDouble().round(2)).toString()
					logi.logInfo("supp proration_amt :"+proration_amt)
					return proration_amt
	}

	String md_After_Discount_Bundle1(String tcid)
	{
		return "696"
	}
	String md_After_Discount_Bundle2(String tcid)
	{
		return "558"
	}
	String md_After_Discount_Bundle3(String tcid)
	{
		return "756"
	}
	String md_After_Discount_Bundle4(String tcid)
	{
		return "558"
	}
	/**
	 * Getting the invoices count of child account
	 * @param tcid
	 * @return String invoice count
	 */
	public String md_GET_CHILD2_INVOICE_COUNT(String tcid)
	{
		ConnectDB db = new ConnectDB()
		logi.logInfo("inside md_GET_CHILD2_INVOICE_COUNT")
		String acct_no = getValueFromResponse("create_acct_complete.b",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String inv_cnt = db.executeQueryP2("SELECT COUNT(INVOICE_NO) FROM ARIACORE.gl WHERE CLIENT_NO="+clientNo+" AND ACCT_NO="+acct_no).toString()
		logi.logInfo("invoice count: "+inv_cnt)
		return inv_cnt
	}

	///////////////// DUMMY VERIFICATIONS /////////////////////////

	def md_ExpUsage_Amt_AutoRate(String tcid){
		return "2750"
	}
	def md_ExpInvoiceWithUsage_Amt_AutoRate(String tcid){
		return "2760"
	}
	def md_ExpUsage_Amt1(String tcid){
		return "420"
	}
	def md_ExpInvoiceWithUsage_Amt1(String tcid){
		return "420"
	}
	def md_ExpUsage_Amt2(String tcid){
		return "1040"
	}
	def md_ExpInvoiceWithUsage_Amt2(String tcid){
		return "1040"
	}
	def md_ExpUsage_Amt3(String tcid){
		return "200"
	}
	def md_ExpInvoiceWithUsage_Amt3(String tcid){
		return "200"
	}
	/**
	 * Getting the expected Bill thru date of an account
	 * @param tcid
	 * @return String date
	 */
	def md_ExpBillThruDateOfAcct1(String tcid)
	{
		logi.logInfo "Entered into md_ExpBillThruDateOfAcct1 "
		ConnectDB db=new ConnectDB()
		String apiname="create_acct_complete"
		def acctno=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String created_date_q="select created from ariacore.acct WHERE ACCT_NO="+acctno
		def created_date = db.executeQueryP2(created_date_q)
		def retro_date = getValueFromRequest(apiname,"//retroactive_start_date")
		if(!retro_date.equals("NoVal"))
		{
			created_date=retro_date
		}
		else{
			created_date=created_date
		}

		logi.logInfo " created date set as " +created_date
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		Date date = dateFormat.parse(created_date);
		Calendar now = Calendar.getInstance();
		now.setTime(date)

		String curr_time=dateFormat.format(now.getTime())
		logi.logInfo "Current created date or retroactive date - before adding bill interval is " + curr_time

		def plan_no = getValueFromRequest(apiname,"//master_plan_no")
		def client_no = getValueFromRequest(apiname,"//client_no")
		def first_bill_thru_date
		String bill_int_q1="select billing_interval from ariacore.client_plan where plan_no="+plan_no+" and client_no="+client_no
		String bill_intl=db.executeQueryP2(bill_int_q1)
		logi.logInfo " bill interval is " + bill_intl

		int bill_interl=Integer.parseInt(bill_intl)
		now.add(Calendar.MONTH,bill_interl)
		logi.logInfo " Added bill interval to created date or retroactive date " + dateFormat.format(now.getTime())

		now.add(Calendar.DATE,-1)
		first_bill_thru_date = dateFormat.format(now.getTime())
		logi.logInfo " Reduced one day to first bill date. Hence the first bill thru date is " + first_bill_thru_date

		now.add(Calendar.MONTH,bill_interl)
		logi.logInfo " Added bill interval again to the first bill thru date and final bill thru date is " + dateFormat.format(now.getTime())

		SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-YYYY");
		String date3 = format2.format(now.getTime())
		logi.logInfo " new bill thru date after date formatting is  " + now.getTime()
		return date3
	}
	/**
	 * Getting the actual credit amount of the updated master plan from credits table
	 * @param tcid
	 * @return String Credit amount
	 */
	public String md_UpdateMasterPlanActualCreditAmountBillLagDay(String tcid)
	{
		logi.logInfo("inside md_UpdateMasterPlanActualCreditAmountBillLagDay")
		ConnectDB db=new ConnectDB()
		String apiname=tcid.split("-")[2]
		String actualCreditAmt="0"
		String acct_no = getValueFromRequest(apiname,"//acct_no")
		String credit_id_q="SELECT MIN(credit_id) FROM ariacore.all_credits WHERE acct_no ="+acct_no
		String credit_id = db.executeQueryP2(credit_id_q)

		if(credit_id == null)
		{
			actualCreditAmt
		}
		else
		{
			String credit_amt_q="SELECT amount FROM ariacore.all_credits WHERE acct_no ="+acct_no+" AND credit_id ="+credit_id
			actualCreditAmt=db.executeQueryP2(credit_amt_q)
		}

		return actualCreditAmt
	}
	/**
	 * Getting master plan number from update master plan api
	 * @param tcid
	 * @return String Master plan no
	 */
	def md_GetMasterPlanfromUMP(String tcid)
	{
		logi.logInfo("inside md_GetMasterPlanfromUMP")
		String apiname="update_master_plan"
		def master_plan=getValueFromRequest(apiname,"//master_plan_no")
		return master_plan
	}
	/**
	 * Getting the update master plan total amount from gl_detail table
	 * @param tcid
	 * @return String total amount
	 */
	def md_UpdateMPTotalAmt(String tcid)
	{
		logi.logInfo("inside md_UpdateMPTotalAmt")
		ConnectDB db=new ConnectDB()
		String apiname="update_master_plan"
		def master_plan=getValueFromRequest(apiname,"//master_plan_no")
		def acct_no = getValueFromRequest(apiname,"//acct_no")
		String total_amt_q="SELECT NVL(SUM(DEBIT),0) FROM ariacore.gl_detail WHERE invoice_no=(SELECT invoice_no FROM ariacore.gl WHERE acct_no="+acct_no+"  AND PLAN_NO ="+master_plan+")"
		String total_amt=db.executeQueryP2(total_amt_q)
		return total_amt
	}
	/**
	 * Getting the update master plan proration amount (including bill lag days)
	 * @param tcid
	 * @return String proration amount
	 */
	public String md_UpdatedMasterPlanProrationAmtExpBillLagDay (String tcid)
	{
		logi.logInfo("inside md_UpdatedMasterPlanProrationAmtExpBillLagDay")
		String apiname="update_master_plan"
		DecimalFormat d = new DecimalFormat("#.##########");
		String acct_no = getValueFromRequest(apiname,"//acct_no")
		String client_no = getValueFromRequest(apiname,"//client_no")
		def ad=getValueFromRequest(apiname,"//assignment_directive")
		String proration_amt,usg_amt="0"
		ConnectDB db = new ConnectDB()
		String unbilled_flag =db.executeQueryP2("Select param_val from ariacore.all_client_params where param_name = 'INVOICE_UNBILLED_USAGE_DURING_PRORATION' and client_no="+client_no)
		if(unbilled_flag.equalsIgnoreCase("TRUE"))
		{
			String exp_inv=  md_ExpInvoice_Amt(tcid,acct_no)
			usg_amt = md_ExpUsage_Amt(tcid)
			logi.logInfo("exp_inv :"+exp_inv)
			logi.logInfo("usg_amt :"+usg_amt)
			proration_amt = d.format((exp_inv.toDouble()+usg_amt.toDouble()).toDouble().round(2)).toString()
		}
		else if(unbilled_flag.equalsIgnoreCase("FALSE"))
		{
			proration_amt = md_ExpInvoice_Amt(tcid,acct_no)
		}
		logi.logInfo("proration_amt :"+proration_amt)

		if(ad.equals("3") || ad.equals("6") ||  ad.equals("7") || ad.equals("8")||  ad.equals("9") ||  ad.equals("10")|| ad.equals("11"))
			proration_amt=0
		return proration_amt
	}
	/**
	 * Getting the bill thru date of an account
	 * @param tcid
	 * @return Date 
	 */
	def md_ExpBillThruDateOfAcct2(String tcid)
	{
		logi.logInfo "Entered into md_ExpBillThruDateOfAcct2 "
		ConnectDB db=new ConnectDB()
		String apiname="create_acct_complete"
		def acctno=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String created_date_q="select created from ariacore.acct WHERE ACCT_NO="+acctno
		def created_date = db.executeQueryP2(created_date_q)
		def retro_date = getValueFromRequest(apiname,"//retroactive_start_date")
		if(!retro_date.equals("NoVal"))
		{
			created_date=retro_date
		}
		else{
			created_date=created_date
		}

		logi.logInfo " created date set as " +created_date
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		Date date = dateFormat.parse(created_date);
		Calendar now = Calendar.getInstance();
		now.setTime(date)

		String curr_time=dateFormat.format(now.getTime())
		logi.logInfo "Current created date or retroactive date - before adding bill interval is " + curr_time

		def plan_no = getValueFromRequest(apiname,"//master_plan_no")
		def client_no = getValueFromRequest(apiname,"//client_no")
		def first_bill_thru_date
		String bill_int_q1="select billing_interval from ariacore.client_plan where plan_no="+plan_no+" and client_no="+client_no
		String bill_intl=db.executeQueryP2(bill_int_q1)
		logi.logInfo " bill interval is " + bill_intl

		int bill_interl=Integer.parseInt(bill_intl)
		now.add(Calendar.MONTH,bill_interl)
		logi.logInfo " Added bill interval to created date or retroactive date " + dateFormat.format(now.getTime())

		now.add(Calendar.DATE,-1)
		first_bill_thru_date = dateFormat.format(now.getTime())
		logi.logInfo " Reduced one day to first bill date. Hence the first bill thru date is " + first_bill_thru_date

		SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-YYYY");
		String date3 = format2.format(now.getTime())
		logi.logInfo " new bill thru date after date formatting is  " + now.getTime()
		return date3
	}
	/**
	 * Getting the update master plan total amount from gl table
	 * @param tcid
	 * @return String total amount
	 */
	def md_UpdateMasterPlanTotalAmtBillLagDay(String tcid)
	{
		logi.logInfo "Entered into md_UpdateMasterPlanTotalAmt "
		ConnectDB db=new ConnectDB()
		String apiname="update_master_plan"
		def master_plan = getValueFromRequest(apiname,"//master_plan_no")
		def acct_no = getValueFromRequest(apiname,"//acct_no")
		String amt_q="Select case when a.debit IS NULL THEN 0 when a.debit IS NOT NULL THEN a.debit END AS debit from (SELECT Sum(debit) as debit from ariacore.gl where acct_no="+acct_no+" and plan_no="+master_plan+")a"
		String amt=db.executeQueryP2(amt_q)
		return amt
	}
	/**
	 * Getting the update master plan proration amount (including bill lag days)
	 * @param tcid
	 * @return String proration amount
	 */
	def md_UpdateMasterPlanProrationAmtBillLagDay(String tcid)
	{
		logi.logInfo "Entered into md_UpdateMasterPlanProrationAmtBillLagDay "
		ConnectDB db=new ConnectDB()
		String apiname="update_master_plan"
		def master_plan = getValueFromRequest(apiname,"//master_plan_no")
		def acct_no = getValueFromRequest(apiname,"//acct_no")
		String pro_amt_q="Select case when a.debit IS NULL THEN 0 when a.debit IS NOT NULL THEN a.debit END AS debit from (SELECT Sum(debit) as debit from ariacore.gl where acct_no="+acct_no+" and plan_no="+master_plan+")a"
		String pro_amt=db.executeQueryP2(pro_amt_q)
		return pro_amt
	}

	// Record standing usage block
	def md_getUsageSummaryBySequenceNoApi1(String tcid){
		return getUsageSummaryBySequenceNoApi("1")
	}
	def md_getUsageSummaryBySequenceNoApi2(String tcid){
		return getUsageSummaryBySequenceNoApi("2")
	}
	def md_getUsageSummaryBySequenceNoApi3(String tcid){
		return getUsageSummaryBySequenceNoApi("3")
	}
	def md_getUsageSummaryBySequenceNoDB1(String tcid){
		return getUsageSummaryBySequenceNoDB("1")
	}
	def md_getUsageSummaryBySequenceNoDB2(String tcid){
		return getUsageSummaryBySequenceNoDB("2")
	}
	def md_getUsageSummaryBySequenceNoDB3(String tcid){
		return getUsageSummaryBySequenceNoDB("3")
	}
	/**
	 * Getting standing usage details from standing_usage table
	 * @param seqNo
	 * @return LinkedHashMap usage details
	 */
	def getUsageSummaryBySequenceNoDB(String seqNo){
		ConnectDB db = new ConnectDB()
		LinkedHashMap hashValue
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		String usage_type = getValueFromRequest("record_standing_usage","//usage_type")
		String acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)

		String query1 = "SELECT a.standing_usage_no, a.usage_type, a.usage_units, to_char(a.next_usage_date,'yyyy-MM-dd') as next_usage_date, a.alt_desc, a.plan_no, to_char(a.first_usage_date,'yyyy-MM-dd') as first_usage_date, a.recurring_ind FROM (SELECT  standing_usage_no, usage_type, usage_units, next_usage_date, alt_desc, plan_no, first_usage_date, recurring_ind, row_number() over(order by create_date) AS seq FROM ariacore.standing_usage WHERE acct_no = "+acct_no+" and is_active_ind=1  and client_no="+client_no+" )a WHERE a.seq="+seqNo
		String query2 = "select USAGE_TYPE_CD from ariacore.client_usage_type where usage_type_no = "+usage_type+" and client_no="+client_no

		hashValue = db.executeQueryP2(query1)
		logi.logInfo("Hash value from database :: "+hashValue.toString())

		//if(!hashValue.isEmpty()){
		if(!hashValue.toString().equals("null")){
			logi.logInfo("Inside hash val not null block")
			String tempVal = db.executeQueryP2(query2)
			logi.logInfo("tempVal tempVal :: "+tempVal)

			//hashValue.put("USAGE_TYPE_CODE", tempVal)
			logi.logInfo("Hash value from database after adding usage code :: "+hashValue.toString())
		}
		logi.logInfo("Hash value from database :: ")
		if(!hashValue.toString().equals("null")){
			return hashValue.sort()
		}
		else{
			LinkedHashMap dummyHashVal = new LinkedHashMap<String,String>()
			logi.logInfo("Hash value from database :: inside dmmy loop")
			dummyHashVal.put("ALT_DESC","No data found")
			dummyHashVal.put("FIRST_USAGE_DATE","No data found")
			dummyHashVal.put("NEXT_USAGE_DATE","No data found")
			dummyHashVal.put("PLAN_NO","No data found")
			dummyHashVal.put("RECURRING_IND","No data found")
			dummyHashVal.put("STANDING_USAGE_NO","No data found")
			dummyHashVal.put("USAGE_TYPE","No data found")
			//dummyHashVal.put("USAGE_TYPE_CODE","No data found")
			dummyHashVal.put("USAGE_UNITS","No data found")
			logi.logInfo("Dummy hash data for verification :: "+dummyHashVal.toString())
			return dummyHashVal.sort()
		}
	}
	/**
	 * Getting usage details from 'get_standing_usage' api
	 * @param lno
	 * @return LinkedHashMap usage details
	 */
	def getUsageSummaryBySequenceNoApi(String lno) {
		def node
		def cnodes
		def cnodeIndex
		LinkedHashMap responseData = new LinkedHashMap()

		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		logi.logInfo(testCaseCmb[0].toString())
		com.eviware.soapui.support.XmlHolder outputholder

		for(int j=0; j<testCaseCmb.size(); j++) {
			
			logi.logInfo "The standing usage line item values for sequence - '"
			
			if(testCaseCmb[j].toString().contains("get_standing_usage") && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				responseData.put("STANDING_USAGE_NO",outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:standing_usage_rec_no[1]")==null?"No data found":outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:standing_usage_rec_no[1]"))
				responseData.put("USAGE_TYPE",outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:usage_type[1]")==null?"No data found":outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:usage_type[1]"))
				responseData.put("USAGE_UNITS",outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:usage_units[1]")==null?"No data found":outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:usage_units[1]"))
				responseData.put("NEXT_USAGE_DATE",outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:next_usage_date[1]")==null?"No data found":outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:next_usage_date[1]"))
				responseData.put("ALT_DESC",outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:alt_desc[1]")==null?"No data found":outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:alt_desc[1]"))
				responseData.put("PLAN_NO",outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:plan_no[1]")==null?"No data found":outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:plan_no[1]"))
				responseData.put("FIRST_USAGE_DATE",outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:first_usage_date[1]")==null?"No data found":outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:first_usage_date[1]"))
				responseData.put("RECURRING_IND",outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:recurring_ind[1]")==null?"No data found":outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:recurring_ind[1]"))
				//responseData.put("USAGE_TYPE_CODE",outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:usage_type_code[1]")==null?"No data found":outputholder.getNodeValue("//*:get_standing_usageResponse[1]/su[1]/*:su_row["+lno+"]/*:usage_type_code[1]"))
			}
		}

		logi.logInfo "The standing usage line item values for sequence - '"+lno+"' are : "+ responseData.toString()
		return responseData.sort()
	}
	/**
	 * Getting the standing usage count from  standing_usage table
	 * @param tcid
	 * @return String usage count
	 */
	String md_getStandingRecordCount(String tcid){
		ConnectDB db = new ConnectDB()
		String acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String countVal
		String qry = "select count(*) from ariacore.standing_usage where acct_no ="+acct_no
		countVal = db.executeQueryP2(qry)
		return countVal
	}
	/**
	 * Getting the usage details from 'Standing usage' api
	 * @param tcid
	 * @param line_no
	 * @return Hashmap
	 */
	HashMap md_GetStandingRecordUsageApi(String tcid,String line_no)
	{
		logi.logInfo("md_GetStandingRecordUsageApi")
		int tmp=1
		LinkedHashMap standingrecord_api=new LinkedHashMap()
		String xpath="//*:get_standing_usageResponse"
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()

		for(int j=0; j<testCaseCmb.size(); j++) {

			if(testCaseCmb[j].toString().equals(tcid))
			{

				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
					def responseXml = new XmlSlurper().parseText(holder.getXml())
					def rootNode = responseXml.Body.get_standing_usageResponse.su
					rootNode.children().each()
					{
						def nme=it.name()+"["+tmp+"]"
						logi.logInfo("nme:"+nme.toString())

						if(nme.equals("su_row["+line_no+"]"))
						{
							it.children().each()
							{
								logi.logInfo(it.name())
								logi.logInfo(it.text())
								standingrecord_api.put(it.name().toUpperCase(), it.text())
							}
						}

						tmp++

					}
				}
			}

		}



		return standingrecord_api.sort()
	}

	public String md_isStandingRecordUsageInActiveState1(String tcid){
		boolean rtype = true
		String val = rtype.toString().toUpperCase()
		logi.logInfo("Return value is 1 : "+val)
		return val
	}
	public String md_isStandingRecordUsageInActiveState2(String tcid){
		boolean rtype = true
		String val = rtype.toString().toUpperCase()
		logi.logInfo("Return value is 2 : "+val)
		return val
	}
	/**
	 * Getting IS_ACTIVE_IND column value for the usgae recorderd 
	 * @param tcid
	 * @param rowNum
	 * @return String UsageState
	 */
	String getDisabledStandingUsageState(String tcid,String rowNum){

		ConnectDB dbase = new ConnectDB()
		String apiname=tcid.split("-")[2]
		String nodeCnt = getNodeCountFromResponse(apiname,"//*:get_standing_usageResponse[1]/su[1]/*:su_row")

		String standingRecNo = getValueFromResponse(apiname,ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)

		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		String acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)

		String query = "select IS_ACTIVE_IND from ariacore.standing_usage where acct_no = "+acct_no+" and client_no = "+client_no+" and standing_usage_no = "+usageRecNo
		String status = dbase.executeQueryP2(query)
		return status
	}
	
	public HashMap md_VERIFY_USAGE_RATE_PRECISION_IN_GL_DETAIL(String tcid)
	{
		String acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		return md_VERIFY_USAGE_RATE_PRECISION_IN_GL_DETAIL(tcid,acct_no)
	}
	/**
	 * Getting usage type and the rate(in comments section) from gl_details table 
	 * @param tcid
	 * @param acct_no
	 * @return HashMap 
	 */
	public HashMap md_VERIFY_USAGE_RATE_PRECISION_IN_GL_DETAIL(String tcid,String acct_no)
	{
		logi.logInfo("Inside md_VERIFY_USAGE_RATE_PRECISION_IN_GL_DETAIL")
		ConnectDB db = new ConnectDB()
		int i=0
		HashMap<String, String> gl_comments = new HashMap<String, String>()
		String comments_qry = "Select usage_type,comments from ariacore.gl_detail where invoice_no=(select max(invoice_no) from ariacore.gl where acct_no="+acct_no+") and usage_type IN (Select usage_type from (Select distinct usage_type,rec_no from ariacore.usage where acct_no ="+acct_no+" and exclude_ind=0 and invoice_no=(Select max(invoice_no) from ariacore.gl where acct_no="+acct_no+") order by rec_no asc))"
		ResultSet rs=db.executePlaQuery(comments_qry)
		while(rs.next())
		{
			i=i+1
			String usage_type = rs.getString(1)
			String comments = "'"+rs.getString(2)+"'"
			logi.logInfo("usage_type :"+usage_type)
			logi.logInfo("comments :"+comments)
			if(comments.contains("EUR"))
			{
				comments = comments.split("EUR")[1]
				comments = comments.split("\\)")[0]
			}

			else
			{
				comments = comments.split("\\\$")[1]
				comments = comments.split("\\)")[0]
			}

			gl_comments.put("USAGE_TYPE-"+i, usage_type)
			gl_comments.put("GL_DETAIL_COMMENTS-"+i, comments)
			logi.logInfo("gl_commenst Hash:"+gl_comments.toString())
		}

		return gl_comments.sort()
	}

	public HashMap md_USAGE_RATE_PRECISION_IN_COMMENTS_EXP(String tcid)
	{
		String acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		return md_USAGE_RATE_PRECISION_IN_COMMENTS_EXP(tcid,acct_no)
	}
	/**
	 * Getting usage type and the expected rate from usage table
	 * @param tcid
	 * @param acct_no
	 * @return HashMap
	 */
	public HashMap md_USAGE_RATE_PRECISION_IN_COMMENTS_EXP(String tcid,String acct_no)
	{
		logi.logInfo("Inside md_USAGE_RATE_PRECISION_IN_COMMENTS_EXP")
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##########");
		double rate
		int i=0
		HashMap<String, ArrayList<String>> usg_rateamt_hm = new HashMap<String, ArrayList<String>>()
		HashMap<String, String> gl_comments_exp = new HashMap<String, String>()
		List<String> usg_list = []
		String usg_types_qry = "Select usage_type from (Select distinct usage_type,rec_no from ariacore.usage where acct_no ="+acct_no+" and exclude_ind=0 and invoice_no=(Select max(invoice_no) from ariacore.gl where acct_no="+acct_no+") order by rec_no asc)"
		ResultSet rs_usg_types_qry = db.executePlaQuery(usg_types_qry)
		while(rs_usg_types_qry.next())
		{
			usg_list.add(rs_usg_types_qry.getString(1))
		}

		for(usage in usg_list)
		{
			i=i+1
			List ser_list=md_GET_USAGE_SERVICE_NO(acct_no.toString(),usage)
			logi.logInfo("Services no for the usgae type "+ser_list)
			for(service in ser_list)
			{
				String pricing_rule=md_get_tier_pricing_type(service,acct_no,usage)
				String rate_null_qry = db.executeQueryP2("Select rate from ariacore.usage where acct_no ="+acct_no+" and usage_type="+usage+" and exclude_ind=0 and invoice_no = (select max(invoice_no) from ariacore.gl where acct_no ="+acct_no+")").toString()
				String bill_units = db.executeQueryP2("Select billable_units from ariacore.usage where acct_no ="+acct_no+" and usage_type="+usage+" and exclude_ind=0 and invoice_no = (select max(invoice_no) from ariacore.gl where acct_no ="+acct_no+")").toString()

				if(pricing_rule == '3' && rate_null_qry.equalsIgnoreCase("null"))
				{
					logi.logInfo("price rule 3 nd rate null")
					usg_rateamt_hm = md_USAGE_AMT_RATE_EXP(acct_no.toString(),ser_list,bill_units,usage.toString())
					rate = usg_rateamt_hm.get(service)[0].toString().toDouble()
				}

				else if(pricing_rule == '2' && rate_null_qry.equalsIgnoreCase("null"))
				{
					logi.logInfo("price rule 2 nd rate null")
					usg_rateamt_hm = md_USAGE_AMT_RATE_EXP(acct_no.toString(),ser_list,bill_units,usage.toString())
					rate = usg_rateamt_hm.get(service)[0].toString().toDouble()
				}

				else
				{
					String rate_qry ="Select NVL(sum(rate),0) from ariacore.usage where acct_no ="+acct_no+" and usage_type="+usage+" and exclude_ind=0  and invoice_no = (select max(invoice_no) from ariacore.gl where acct_no ="+acct_no+")"
					rate = db.executeQueryP2(rate_qry).toDouble()
				}
			}

			gl_comments_exp.put("USAGE_TYPE-"+i, usage)
			gl_comments_exp.put("GL_DETAIL_COMMENTS-"+i, d.format(rate).toString())
			logi.logInfo("gl_commenst exp Hash:"+gl_comments_exp.toString())
		}

		return gl_comments_exp.sort()
	}

	public String md_Credit_Card_Discount(String testcaseid)
	{
		String acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		return md_Credit_Card_Discount(testcaseid,acct_no)
	}
	/**
	 * Getting the discount amount for the credit card payment
	 * @param testcaseid
	 * @param accountNo 
	 * @return String
	 */
	public String md_Credit_Card_Discount(String testcaseid,String accountNo)
	{
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String pay_method=getValueFromRequest("create_acct_complete","//*/pay_method").toString()
		ConnectDB database =new ConnectDB();
		DecimalFormat d = new DecimalFormat("#.##########");
		String query2="select discount_rule_no from ariacore.client_payment_method where client_no=" + clientNo + "And method_id=" + pay_method
		String  ex_query=database.executeQueryP2(query2)
		int drule=Integer.parseInt(ex_query)
		logi.logInfo("drule"+ drule)
		if(drule == 1)
		{
			logi.logInfo("Entering into rule 1")
			String curr_code=getValueFromRequest("create_acct_complete","//*/currency_cd").toString()
			String query1="select amount from ariacore.CLI_PAY_MTH_FLAT_DISC_CURR_MAP WHERE CLIENT_NO=" + clientNo + " AND METHOD_ID =" + pay_method + " AND currency_cd=" + "'"+curr_code+"'"
			String flat_amt=database.executeQueryP2(query1)
			logi.logInfo("Value"+flat_amt)
			database.closeConnection()			
			return (d.format(flat_amt.toDouble()).toString())
		}
		else if(drule == 2)
		{
			logi.logInfo("Enter into the drule 2!!!")
			logi.logInfo("Enter drule 2!!!")			
			String discount_query = "select discount_percentage from ariacore.client_payment_method where client_no ="+ clientNo + "and method_id ="+pay_method
			String recur_amt=database.executeQueryP2(discount_query)
			logi.logInfo("Dicount amount" +recur_amt)
			double exp = md_ExpInvoice_AllCharges_Amt(testcaseid,accountNo).toDouble()			
			logi.logInfo("md_ExpInvoice_AllCharges_Amt in md_Credit_Card_Discount "+exp.toString())
			String activation_amt=database.executeQueryP2("select sum(debit) from ariacore.gl_detail where invoice_no in (SELECT invoice_no FROM ariacore.gl WHERE ACCT_NO="+accountNo+") and service_no in  (select distinct service_no from ariacore.services where  recurring=0 and usage_based=0 ) and service_no !=0")
			int invoiceCount=database.executeQueryP2("Select count(*) from ariacore.gl where acct_no="+accountNo).toInteger()
			logi.logInfo "invoiceCount :: "+invoiceCount
			double recurring_amt
			if(invoiceCount <=1)
			recurring_amt=(exp * (recur_amt.toDouble()/100)).toDouble()
			else
			recurring_amt=((exp - activation_amt.toDouble() )* (recur_amt.toDouble()/100)).toDouble()
			
			logi.logInfo("Amount"+recurring_amt)
			return (d.format(recurring_amt).toString())
			 
		}
		else if(drule == 3)
		{
			logi.logInfo("Enter into the drule 3!!!")
			logi.logInfo("Enter drule 3!!!")			
			String discount_query = "select discount_percentage from ariacore.client_payment_method where client_no ="+ clientNo + "and method_id ="+pay_method
			String recur_amt=database.executeQueryP2(discount_query)
			logi.logInfo("Dicount amount" +recur_amt)
			double exp = md_ExpInvoice_Amt(testcaseid,accountNo).toDouble()
			logi.logInfo("md_ExpInvoice_Amt in md_Credit_Card_Discount "+exp.toString())
			double recurring_amt=exp * (recur_amt.toDouble()/100)
			logi.logInfo("Amount"+recurring_amt)
			return (d.format(recurring_amt).toString())
			 
		}//end of drule
		else if(drule == 4)
		{
			//usage based services
			logi.logInfo("Enter into the drule 4!!!")
			logi.logInfo("Enter drule 4!!!")
			String discount_query = "select discount_percentage from ariacore.client_payment_method where client_no ="+ clientNo + "and method_id ="+pay_method
			String recur_amt=database.executeQueryP2(discount_query)
			logi.logInfo("Dicount amount" +recur_amt)
			double exp =md_ExpUsage_Amt(testcaseid,accountNo).toDouble()
			logi.logInfo("md_ExpUsage_Amt in md_Credit_Card_Discount "+exp.toString())
			double recurring_amt=exp * (recur_amt.toDouble()/100)
			logi.logInfo("Amount"+recurring_amt)
			return (d.format(recurring_amt).toString())
		}//end of drule
		else if(drule == 5)
		{
			logi.logInfo("Entering drule5")
			String order_query= " select amount from orders where order_no=(select max(order_no) from orders where acct_no="+ accountNo +")"
			String order_amt=database.executeQueryP2(order_query)
			logi.logInfo("Order Amount"+ order_amt)
			logi.logInfo("After loop enter the calc part!!!!")
			String discount_query = "select discount_percentage from ariacore.client_payment_method where client_no ="+ clientNo + "and method_id ="+pay_method
			String recur_amt=database.executeQueryP2(discount_query)
			logi.logInfo("Dicount amount" +recur_amt)
			double recurring_amt=order_amt.toDouble() * (recur_amt.toDouble()/100)
			logi.logInfo("Amount"+recurring_amt)
			double final_amt = order_amt.toDouble() - recurring_amt
			return (d.format(final_amt).toString())
			
		}
	}//end of loop
	/**
	 * Getting all the expected charges amount
	 * @param testcaseid
	 * @param accountNo
	 * @return String
	 */
	public String md_ExpInvoice_AllCharges_Amt(String testcaseid,String accountNo){
		DecimalFormat d = new DecimalFormat("#.##########");
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = database.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time : "+ currentVirtualTime
		//int useddays=database.executeQueryP2("SELECT (TRUNC(next_bill_date) -  trunc(plan_date)) AS days FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo).toInteger()
		int useddays=database.executeQueryP2("SELECT TRUNC(LAST_BILL_THRU_DATE+1) - (select CASE WHEN  (trunc(plan_date) >= trunc(last_recur_bill_date) or last_recur_bill_date is null ) THEN trunc(plan_date) ELSE trunc(last_recur_bill_date)  END               FROM ariacore.acct WHERE acct_no = "+accountNo+")from ariacore.acct WHERE acct_no = "+accountNo+"  AND client_no ="+clientNo).toInteger()

		Map<String, HashMap<String,String>> hash = getPlanServiceAndTieredPricingAllCharges()

		Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
		resultHash.putAll(hash)

		logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()

		Iterator iterator = resultHash.entrySet().iterator()
		logi.logInfo "Before Key setting "

		int invoice = 0
		double proration_factor

		while (iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) iterator.next()
			String planNo = pairs.getKey().toString()
			logi.logInfo "Execution for the plan :  "+ planNo
			String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
			String planStatusQuery, planStatusDataQuery

			int planTypeInd = Integer.parseInt(database.executeQueryP2(planTypeQuery))

			logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo


			if(planTypeInd==1) {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT_SUPP_PLAN_MAP AC WHERE ACCT_NO   ="+accountNo+" AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date from ariacore.ACCT_SUPP_PLAN_MAP where acct_no = "+ accountNo +"AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo

			}
			else {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+" AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+ " AND CLIENT_NO= " + clientNo

			}

			int planStatus = Integer.parseInt(database.executeQueryP2(planStatusQuery))
			logi.logInfo "Plan Status : "+planStatus+ " plan : " +planNo
			logi.logInfo "Plan planStatusDataQuery : "+planStatusDataQuery+ " plan : " +planNo


			if(planStatus!=0){
				String statusDateDB = database.executeQueryP2(planStatusDataQuery)
				logi.logInfo "Status date from DB: "+statusDateDB
				Date statusDate = simpleFormat.parse(statusDateDB)

				// Stores the current virtual time in the specified format
				Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
				logi.logInfo "planStatus : "+planStatus+ " plan : " +planNo
				logi.logInfo "statusDate.compareTo(currentVTDate) : "+statusDate.compareTo(currentVTDate)+ " plan : " +planNo


				if((planStatus==1 && statusDate.compareTo(currentVTDate)<=0)|| (planTypeInd==0)){

					Map<String,String> valueMap = pairs.getValue()
					Map<String,String> value = new TreeMap<String,String> ()
					value.putAll(valueMap)

					int billingInterval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planNo+ "and client_no="+clientNo))
					Date nextbilldate
					ResultSet rs=database.executePlaQuery("SELECT TRUNC(LAST_BILL_THRU_DATE+1) FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo)
					rs.next()
					nextbilldate=rs.getDate(1)
					Date virtualstartdate = subractMonthsFromGivenDate(nextbilldate,billingInterval)

					logi.logInfo "virtualstartdate "+virtualstartdate
					logi.logInfo "nextbilldate " +nextbilldate
					int durationOfPlan = getDaysBetween(virtualstartdate,nextbilldate )
					logi.logInfo "Duration of a plan : "+planNo + " :: "+durationOfPlan
					logi.logInfo "DaysInvoice "+useddays
					logi.logInfo "daysInvoice % durationOfPlan Value : "+ (useddays % durationOfPlan )
					int tmp_days = durationOfPlan-useddays
					if(tmp_days<=0)
						tmp_days = durationOfPlan
					proration_factor=useddays/durationOfPlan.toDouble()
					logi.logInfo "proration_factor "+proration_factor
					Iterator service = value.entrySet().iterator()
					logi.logInfo "Invoice calculation for a plan : "+planNo
					String unitsquery="select recurring_factor from ariacore.all_acct_recurring_factors where ACCT_NO ="+accountNo+" and plan_no="+planNo
					int noofUnits =database.executeQueryP2(planStatusQuery).toInteger()

					logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo

					while(service.hasNext()){
						Map.Entry servicePairs = (Map.Entry) service.next()
						String serviceNo = servicePairs.getKey().toString()
						logi.logInfo "Invoice calculation for a service : "+serviceNo
						int seqNo=1;
						String tieredPricing = servicePairs.getValue().toString()
						int tieredPricingInt = Integer.parseInt(tieredPricing)
						logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
						String altFee = "NoVal"
						int serviceInvoice
						String query
						switch(tieredPricingInt){
							case 1:
								logi.logInfo "Case 1 "
								if(!altFee.equals("NoVal"))
								{
									query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
								}
								else
									query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
								serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								invoice  = invoice+serviceInvoice
								logi.logInfo "After adding with invoice " + invoice
								seqNo++;
								break;
							case 2 :
								logi.logInfo "Case 2"
								if(!altFee.equals("NoVal"))
								{
									query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
								}
								else
									query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"

								serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								invoice  = invoice+serviceInvoice
								logi.logInfo "After adding with invoice " + invoice
								break;
							case 3 :
								logi.logInfo "Case 3"
								query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
								serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								invoice  = invoice+serviceInvoice
								logi.logInfo "After adding with invoice " + invoice
								break;
						}
					}
				
				}
			}

		}

		//String proration_factor_db=db.executeQueryP2("select distinct proration_factor from ariacore.gl_detail where invoice_no="+invoiceNo+" and proration_factor is not null")
		double tinvoice=(invoice*proration_factor.toDouble()).round(2)
		return d.format(tinvoice).toString()
	}	
	
	public String md_USAGE_CTRL_DATE_EXP(String tcid)
	{
		ConnectDB db = new ConnectDB();
		String acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		String bulk_date = db.executeQueryP2("SELECT TO_CHAR(trunc(rate_date)) from ariacore.usage_ctrl where ctrl_id= (Select distinct ctrl_id from  ariacore.usage where acct_no="+acct_no+")")
		
	}
	
	/**
	 * Takes the Credit applied for an invoice
	 * @param testCaseId
	 * @return credit amount
	 */
	public String md_invoice_credit_amt(String testCaseId)
	{
			logi.logInfo("calling md_invoice_credit_amt")
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')	
			ConnectDB db = new ConnectDB();
			DecimalFormat d = new DecimalFormat("#.##########");	
			String acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
			String invoiceNo = getInvoiceNoVT(clientNo, acct_no).toString();
			logi.logInfo( "invoiceNo in md_invoice_credit_amt method " +invoiceNo)			
			//double creditamt=db.executeQueryP2("select NVL(sum(credit),0) from ariacore.gl where acct_no="+acct_no+" and client_no="+clientNo+" and invoice_no="+invoiceNo).toString().toDouble()
			double creditamt=db.executeQueryP2("select (sum(debit)*-1) from ariacore.gl_detail where invoice_no="+invoiceNo+" and client_no="+clientNo+" and orig_credit_id is not null").toDouble()			
			logi.logInfo("totalInvoice in md_invoice_credit_amt method "+creditamt.round(2).toString())
			return d.format(creditamt.round(2)).toString()	
		
	}
	
	def md_ExpBillDate(String tcid)
	{
		logi.logInfo "Calling md_ExpBillDate"
		String cac_acc_num =  getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB objDBConnect = new ConnectDB();
		
		String created_date_q="select trunc(created) from ariacore.acct WHERE ACCT_NO="+cac_acc_num
		def created_date = objDBConnect.executeQueryP2(created_date_q)
		def retro_date = getValueFromRequest("create_acct_complete","//retroactive_start_date")
		if(!retro_date.equals("NoVal"))
		{
			created_date=retro_date
		}
		else{
			created_date=created_date
		}

		logi.logInfo " created date set as " +created_date
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		Date date = dateFormat.parse(created_date);
		Calendar now = Calendar.getInstance();
		now.setTime(date)
		
		/*String annidateqry = "Select trunc(next_bill_date) - (SELECT trunc(ARIACORE.ARIAVIRTUALTIME(" +clientNo + ")) from DUAL) as days from ariacore.acct where acct_no = " + cac_acc_num + " and client_no = " + clientNo
		logi.logInfo "Dates ==>"
		logi.logInfo objDBConnect.executeQueryP2("Select trunc(next_bill_date) from ariacore.acct where acct_no="+cac_acc_num)
		logi.logInfo objDBConnect.executeQueryP2("SELECT trunc(ARIACORE.ARIAVIRTUALTIME(" +clientNo + ")) from DUAL")
		ResultSet daystoAdv = objDBConnect.executePlaQuery(annidateqry)
		daystoAdv.next()
		int nbdays = daystoAdv.getInt(1)
		daystoAdv.close()*/	
		
		String mpplannum = getValueFromRequest("create_acct_complete","//master_plan_no")
		String planIntervalQry = "Select Billing_Interval from ariacore.client_plan where plan_no =" + mpplannum + " and client_no =" +clientNo
		ResultSet planInter = objDBConnect.executePlaQuery(planIntervalQry)
		planInter.next()
		String planInterMonths = planInter.getString(1)
		now.add(Calendar.MONTH,planInterMonths.toInteger())
		logi.logInfo " Added billing cycle " + dateFormat.format(now.getTime())
		String lagName
		planInter.close()
		if (planInterMonths == "1")
		   lagName = "MONTHLY_BILL_LAG_DAYS"
		else
		if(planInterMonths == "3")
		   lagName = "QUARTERLY_BILL_LAG_DAYS"
		else
		if(planInterMonths == "6")
		   lagName = "SEMI_ANNUAL_BILL_LAG_DAYS"
		else
		if(planInterMonths == "12")
		   lagName = "ANNUAL_BILL_LAG_DAYS"
		else
		 lagName = "CUSTOM_BILL_LAG_DAYS"
	
		int clientLagDays = 0
		String lagDaysQry = "Select param_val from ariacore.aria_client_params where client_no =" + clientNo+"  and param_name='"+ lagName + "'"
		
		
		ResultSet lagDaysResult = objDBConnect.executePlaQuery(lagDaysQry)
		//calcuating pay method lag days
		 String paymethodLagDays= objDBConnect.executeQueryP2( "select NVL(lag_days,0) from ARIACORE.CLIENT_PAYMENT_METHOD  where client_no=" + clientNo+" and method_id=(select pay_method from ariacore.acct where acct_no="+cac_acc_num +")")
		logi.logInfo "paymethodLagDays :: "+paymethodLagDays.toString()
		if (lagDaysResult.next())
		   clientLagDays = lagDaysResult.getInt(1)
		logi.logInfo "clientLagDays :: "+clientLagDays.toString()
	     int finaldaystoAD
		if(paymethodLagDays.toInteger() != 0) //No lag days set for paymethod
		  finaldaystoAD = paymethodLagDays.toInteger()
		else
		finaldaystoAD = clientLagDays
		 logi.logInfo "finaldaystoAD :: "+finaldaystoAD.toString()
		 
		 now.add(Calendar.DATE,finaldaystoAD)
		 logi.logInfo " Added bill interval to created date or retroactive date " + dateFormat.format(now.getTime())		
 
		 SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy");
		 String date3 = format2.format(now.getTime())
		 logi.logInfo " date3=> "+date3
		 return date3
		
	}
	
	/**
	 * Modified md_ExpInvoice_Amt to use as a verification point
	 * @param testcaseid
	 * @return
	 */
	public String md_ExpInvoice_Amount(String testcaseid){
		String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
		DecimalFormat d = new DecimalFormat("#.##########");
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo = getInvoiceNoVT(clientNo, accountNo).toString();
		ConnectDB db = new ConnectDB();
		def serviceInvoiceActivationAmount = md_serviceInvoiceActivationAmount(testcaseid).toDouble()		
		logi.logInfo "serviceInvoiceActivationAmount "+serviceInvoiceActivationAmount.toString()		
		def recserviceAmt=md_ExpInvoice_Amt(testcaseid, accountNo).toDouble()
		logi.logInfo "recserviceAmt "+recserviceAmt.toString()
		int invoiceCount=db.executeQueryP2("Select count(*) from ariacore.gl where acct_no="+accountNo).toInteger()
		logi.logInfo "invoiceCount :: "+invoiceCount
		def creditamt=db.executeQueryP2("select NVL((sum(debit)*-1),0) from ariacore.gl_detail where invoice_no="+invoiceNo+" and client_no="+clientNo+" and orig_credit_id is not null").toDouble()		
		logi.logInfo "credit amount :: "+creditamt
		def total = 0.0
		if(invoiceCount <=1)			
		total=serviceInvoiceActivationAmount+recserviceAmt		
		else
		total=recserviceAmt
		
	   return  d.format((total-creditamt).round(2)).toString()
	}
	
	
	////////////////////// SURCAHRGE ////////////////////////////////////////////////
	
	public def md_GET_ACCT_PLAN_SURCHARGE_DETAILS_EXP1(String tcid)
	{
		return md_GET_ACCT_PLAN_SURCHARGE_DETAILS_EXP(tcid,"1")
	}
	
	public def md_GET_ACCT_PLAN_SURCHARGE_DETAILS_EXP_MP1_1(String tcid)
	{
		return md_GET_ACCT_PLAN_SURCHARGE_DETAILS_EXP(tcid,"1","1")
	}
	public def md_GET_ACCT_PLAN_SURCHARGE_DETAILS_EXP_SP1_1(String tcid)
	{
		return md_GET_ACCT_PLAN_SURCHARGE_DETAILS_EXP(tcid,"2","1")
	}
	LinkedHashMap md_GET_ACCT_PLAN_SURCHARGE_DETAILS_EXP(String tcid,String plan_lno,String sur_lno)
	{
		logi.logInfo("Inside md_GET_ACCT_PLAN_SURCHARGE_DETAILS_EXP")
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=tcid.split("-")[2]
		logi.logInfo apiname
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no
		ConnectDB db = new ConnectDB()
		acct_no= getValueFromRequest(apiname,"//acct_no")
		logi.logInfo acct_no
		HashMap input_hash = new HashMap()
		LinkedHashMap expected_hash = new LinkedHashMap()
		String xpath="//*:surcharges_all//*"
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		int tmp=1
		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().equals(tcid)) {
				logi.logInfo "here"
				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				logi.logInfo xmlValues[j].getXml().toString()
				//if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
					def responseXml = new XmlSlurper().parseText(holder.getXml())
					def rootNode = responseXml.Body.get_acct_plans_allResponse.all_acct_plans
					List<String> exclude_items=['SURCHARGE_RATE']
					logi.logInfo "exclude_items" +exclude_items.toString()
					rootNode.children().each()
					{
						logi.logInfo "inside rootnode loop"
						def nme=it.name()+"["+tmp+"]"
						logi.logInfo("nme:"+nme.toString())

						if(nme.equals("all_acct_plans_row["+plan_lno+"]"))
						{
							logi.logInfo("plan matched")
							
							it.children().each()
							{
								logi.logInfo("plan children: "+it.name())
								
								if(it.name().equals("surcharges_all"))
								{
									logi.logInfo("Got surchrge all")
									int cnt =1
									it.children().each()
									{
										def sur_row= it.name()+"["+cnt+"]"
										if(sur_row.equals("surcharges_all_row["+sur_lno+"]"))
										{
											it.children().each()
											{
												logi.logInfo(it.name())
												if(exclude_items.contains(it.name().toString().toUpperCase())==false)
												{
												input_hash.put(it.name().toUpperCase(), it.text())
												}
											}
										}
										
										cnt++
									}
									
								}
								
							}
						}

						tmp++

					}
				//}
			}
		}
		
		logi.logInfo "input_hash"
		logi.logInfo input_hash.toString()
		return input_hash

		
		
	}
	LinkedHashMap md_GET_ACCT_PLAN_SURCHARGE_DETAILS_EXP(String tcid,String line_no) {
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=tcid.split("-")[2]
		logi.logInfo apiname
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no		
		ConnectDB db = new ConnectDB()
		acct_no= getValueFromRequest(apiname,"//acct_no")		
		logi.logInfo acct_no
		HashMap input_hash = new HashMap()
		LinkedHashMap expected_hash = new LinkedHashMap()
		String xpath="//*:surcharges_all//*"
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		int tmp=1
		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().equals(tcid)) {
				logi.logInfo "here"
				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				logi.logInfo xmlValues[j].getXml().toString()
				//if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
					def responseXml = new XmlSlurper().parseText(holder.getXml())
					def rootNode = responseXml.Body.get_acct_plans_allResponse.all_acct_plans.all_acct_plans_row.surcharges_all
					List<String> exclude_items=['SURCHARGE_RATE','CURRENCY','EXT_DESCRIPTION','DESCRIPTION']
					logi.logInfo "exclude_items" +exclude_items.toString()
					rootNode.children().each()
					{
						logi.logInfo "inside rootnode loop"
						def nme=it.name()+"["+tmp+"]"
						logi.logInfo("nme:"+nme.toString())

						if(nme.equals("surcharges_all_row["+line_no+"]"))
						{
							it.children().each()
							{
								logi.logInfo(it.name())
								if(exclude_items.contains(it.name().toString().toUpperCase())==false)
								{								
								input_hash.put(it.name().toUpperCase(), it.text())
								}
							}
						}

						tmp++

					}
				//}
			}
		}
		logi.logInfo "input_hash"
		logi.logInfo input_hash.toString()
		return input_hash
		
	}	
	
	public def md_GET_ACCT_PLAN_SURCHARGE_DETAILS_DB1(String tcid)
	{
		return md_GET_ACCT_PLAN_SURCHARGE_DETAILS_DB(tcid,"1")
	}
	public def md_GET_ACCT_PLAN_SURCHARGE_DETAILS_DB2(String tcid)
	{
		return md_GET_ACCT_PLAN_SURCHARGE_DETAILS_DB(tcid,"2")
	}
	
	public def md_GET_ACCT_PLAN_SURCHARGE_DETAILS_DB_MP1_1(String tcid)
	{
		String apiname=tcid.split("-")[2]
		String acct_no = getValueFromRequest(apiname,"//acct_no")
		ConnectDB db = new ConnectDB()
		String mplan = db.executeQueryP2("SELECT  plan_no FROM ariacore.all_acct_active_plans WHERE acct_no="+acct_no+" and supp_plan_ind =0").toString()
		String sur_num = GET_SURCHARGE_NO_FOR_PLAN(mplan,"1")
		logi.logInfo("returned surcharge number is: "+sur_num)
		return md_GET_ACCT_PLAN_SURCHARGE_DETAILS_DB(tcid,sur_num)
	}
	public def md_GET_ACCT_PLAN_SURCHARGE_DETAILS_DB_SP1_1(String tcid)
	{
		String apiname=tcid.split("-")[2]
		String acct_no = getValueFromRequest(apiname,"//acct_no")
		ConnectDB db = new ConnectDB()
		String splan = db.executeQueryP2("SELECT  plan_no FROM ariacore.all_acct_active_plans WHERE acct_no="+acct_no+" and supp_plan_ind =1").toString()
		String sur_num = GET_SURCHARGE_NO_FOR_PLAN(splan,"1")
		logi.logInfo("returned surcharge number is: "+sur_num)
		return md_GET_ACCT_PLAN_SURCHARGE_DETAILS_DB(tcid,sur_num)
	}
	LinkedHashMap md_GET_ACCT_PLAN_SURCHARGE_DETAILS_DB(String tcid,String sno)
	{

		logi.logInfo "Calling md_GET_ACCT_PLAN_SURCHARGE_DETAILS_DB"
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=tcid.split("-")[2]
		def acct_no
		
		ConnectDB db = new ConnectDB()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		acct_no = getValueFromRequest(apiname,"//acct_no")
		
		//String sutcharge_detail_query="select CLIENT_SURCHARGE_ID,surcharge_type_cd as SURCHARGE_TYPE ,surcharge_name as SURCHARGE_NAME,surcharge_no as SURCHARGE_NO ,inline_offset_ind as INVOICE_APP_METHOD from ariacore.acct_surcharge_by_acct_no  where  acct_no="+acct_no
		String sutcharge_detail_query="select csur.CLIENT_SURCHARGE_ID as CLIENT_SURCHARGE_ID ,csur.surcharge_type_cd as SURCHARGE_TYPE ,curmap.currency_cd as CURRENCY, csur.DESCRIPTION as DESCRIPTION, csur.surcharge_name as SURCHARGE_NAME, csur.ext_desc AS EXT_DESCRIPTION, csur.surcharge_no as SURCHARGE_NO ,csur.inline_offset_ind as INVOICE_APP_METHOD from ariacore.client_surcharge csur JOIN ariacore.client_surcharge_currency_map curmap ON csur.surcharge_no=curmap.surcharge_no where csur.surcharge_no="+sno+" and csur.client_no="+clientNo
		ResultSet rs = db.executePlaQuery(sutcharge_detail_query);
		ResultSetMetaData md = rs.getMetaData();
		int columns = md.getColumnCount();
		LinkedHashMap surcharge_details = new LinkedHashMap()
		while (rs.next()){
			for(int i=1; i<=columns; i++){
				logi.logInfo(" adding")
				surcharge_details.put(md.getColumnName(i),rs.getString(i));
			}
			logi.logInfo "Added"
		}

		logi.logInfo "surcharge_details DB : "+surcharge_details.toString()
		return surcharge_details
	}
	
	public String md_IS_SURCHARGE_APPLIED(String tcid)
	{
		   String surchargeFlg=""
		   String apiname=tcid.split("-")[2]
		   String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		   String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		   String invoice_no=getInvoiceNoVT(clientNo, acct_no).toString();
		   ConnectDB db = new ConnectDB();
		   
		   int surChargeCnt = db.executeQueryP2("Select count(surcharge_no) from ARIACORE.GL_DTL_SURCHARGE_DTL where invoice_no="+invoice_no).toInteger()
		   
		   if(surChargeCnt == 0)
		   {
				  surchargeFlg="FALSE"
		   }
		   else if (surChargeCnt > 0)
		   {
				  surchargeFlg="TRUE"
		   }
	}
	
	public String md_APPLIED_SURCHARGE_CURRENCY_CD(String tcid)
	{
		   String apiname=tcid.split("-")[2]
		   String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		   String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		   String invoice_no=getInvoiceNoVT(clientNo, acct_no).toString();
		   ConnectDB db = new ConnectDB();
		   String currCode = db.executeQueryP2("Select distinct currency_cd from CLIENT_SURCHARGE_CURRENCY_MAP where surcharge_no IN (Select surcharge_no from GL_DTL_SURCHARGE_DTL where invoice_no="+invoice_no+")").toString()
		   
		   return currCode
	}
	public List GET_SURCHARGE_NO_LIST(String tcid,String acct_no)
	{
		logi.logInfo("Inside GET_SURCHARGE_NO_LIST")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB();
		List<String> surchargeNos =[];
		String surnos_qry = "Select surcharge_no from ariacore.CLIENT_SURCHARGE_CURRENCY_MAP surmap join ariacore.acct act on surmap.currency_cd = act.currency_cd where act.acct_no="+acct_no+" and surmap.surcharge_no IN (SELECT surcharge_no FROM ariacore.acct_surcharge WHERE acct_no="+acct_no+
		" UNION SELECT surcharge_no FROM ariacore.plan_surcharge WHERE plan_no IN (SELECT plan_no FROM ariacore.all_acct_active_plans WHERE acct_no="+acct_no+" ))"
		
		ResultSet rs= db.executePlaQuery(surnos_qry)
		
		while(rs.next())
		{
			String surNo = rs.getString(1)
			surchargeNos.add(surNo)
		}
		
		return surchargeNos
	}
	
	
	public String md_GET_SURCHAGRE_AMT_EXP(String tcid)
	{
		logi.logInfo("Inside md_GET_SURCHAGRE_AMT_EXP")
		String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		DecimalFormat d = new DecimalFormat("#.##########");
		String surchargeAmt="0.0"
		List surNos = GET_SURCHARGE_NO_LIST(tcid,acct_no)
		logi.logInfo("Surcharges applied:"+surNos.toString())
		ConnectDB db = new ConnectDB();
		for(int i=0;i<surNos.size();i++)
		{
			String scope = Get_surcharge_scope(surNos.get(i),clientNo)
			String discount_type = get_discount_type(surNos.get(i),clientNo)
			String surcg_type = get_surcharge_type(surNos.get(i),clientNo)
			String inl_ind=""
			if(surcg_type.equals("P"))
			{
				logi.logInfo("This is percentage type")
				inl_ind = get_app_method(surNos.get(i),clientNo)
			}
			
			LinkedHashMap<String,String> hm_unit_rates=new LinkedHashMap<String, String>();
			hm_unit_rates = GET_SURCHARGE_PRICE_TIER(surNos.get(i),clientNo)
			
				String inv_amt = get_invoice_amt(surNos.get(i),acct_no,scope,discount_type,inl_ind,clientNo)
				String no_of_sers = get_no_of_services(surNos.get(i),acct_no,scope,clientNo)
				String amt = calculate_surcharge_amt(inv_amt,hm_unit_rates,no_of_sers,surcg_type)
				String zero_inv_flag = db.executeQueryP2("Select apply_to_zero_invoice_ind from ariacore.client_surcharge where surcharge_no="+surNos.get(i)+" and client_no="+clientNo).toString()
				if( (inv_amt.toDouble() == 0) && (zero_inv_flag.equals("0")))
				{
					logi.logInfo("apply_to_zero_invoice_ind flag is FALSE")
					amt="0"
				}
				
				surchargeAmt = (surchargeAmt.toDouble() + amt.toDouble()).toString()
		
		}
		
		return d.format(surchargeAmt.toDouble().round(3)).toString();
	}
	
	public String Get_surcharge_scope(String surNumber,String clientNo)
	{
		logi.logInfo("Inside Get_surcharge_scope")
		ConnectDB db = new ConnectDB();
		String scpNo = db.executeQueryP2("Select distinct scope_no from ariacore.client_surcharge where surcharge_no="+surNumber+" and client_no="+clientNo).toString()
		return scpNo
	}
	
	public String get_discount_type(String surNumber,String clientNo)
	{
		logi.logInfo("Inside get_discount_type")
		ConnectDB db = new ConnectDB();
		String discount_cd = db.executeQueryP2("Select apply_b4_or_after_discount_cd from ARIACORE.CLIENT_SURCHARGE where surcharge_no="+surNumber+" and client_no="+clientNo).toString()
		return discount_cd
	}
	
	public String get_invoice_amt(String surNumber,String acct_no,String scope,String discount_type,String in_off_ind,String clientNo)
	{
		logi.logInfo("Inside get_invoice_amt:"+" acct no:"+acct_no+" scope:"+scope+" discount_type:"+discount_type+" clientNo:"+clientNo)
		DecimalFormat d = new DecimalFormat("#.##########");
		String inv_no=getInvoiceNoVT(clientNo,acct_no);
		logi.logInfo("Latest invoice no is:"+inv_no)
		double tmpinvoice_amt
		double invoice_amt
		ConnectDB db = new ConnectDB();
		String mapped_plan=db.executeQueryP2("select plan_no from ariacore.plan_surcharge  where  surcharge_no="+surNumber)
		logi.logInfo(" mapped_plan " +mapped_plan)
		double coupon_amt=	db.executeQueryP2("select NVL(sum(usage_rate*usage_units),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no  and glt.client_no=als.client_no where glt.invoice_no="+inv_no+" AND glt.ORIG_CLIENT_SKU IS NULL and orig_coupon_cd is  not null").toDouble()
		if(scope.equals("1") ) //Done
		{
			tmpinvoice_amt = db.executeQueryP2("select NVL(sum(usage_rate*base_plan_units*proration_factor),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no  and glt.client_no=als.client_no where glt.invoice_no="+inv_no+" AND glt.ORIG_CLIENT_SKU IS NULL and glt.orig_credit_id is  null and orig_coupon_cd is null ").toDouble()
			
			if(discount_type.equals("A"))
			{
				logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
				invoice_amt = tmpinvoice_amt+coupon_amt
			}			
			
			
		}
		
		else if(scope.equals("2") && discount_type.equals("B"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" AND gl_charge_seq_num=gl_surcharge_seq_num AND surcharge_no="+surNumber+") AND glt.ORIG_CLIENT_SKU IS NULL ").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") AND ORIG_CLIENT_SKU IS NULL").toDouble()
			}
			
		}
		
		else if(scope.equals("2") && discount_type.equals("A"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" AND gl_charge_seq_num=gl_surcharge_seq_num AND surcharge_no="+surNumber+") AND glt.ORIG_CLIENT_SKU IS NULL ").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") AND ORIG_CLIENT_SKU IS NULL").toDouble()
			}
		}
		
		else if(scope.equals("3") && discount_type.equals("B"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" AND gl_charge_seq_num=gl_surcharge_seq_num AND surcharge_no="+surNumber+") AND glt.client_no=als.client_no AND glt.ORIG_CLIENT_SKU IS NULL ").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") AND ORIG_CLIENT_SKU IS NULL").toDouble()
			}
			
		}
		
		else if(scope.equals("3") && discount_type.equals("A"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" AND gl_charge_seq_num=gl_surcharge_seq_num AND surcharge_no="+surNumber+") AND glt.client_no=als.client_no AND glt.ORIG_CLIENT_SKU IS NULL ").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") AND ORIG_CLIENT_SKU IS NULL").toDouble()
			}
		}
		
		else if(scope.equals("4") && discount_type.equals("B"))
		{
			
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" AND gl_charge_seq_num=gl_surcharge_seq_num AND surcharge_no="+surNumber+") AND glt.client_no=als.client_no AND glt.ORIG_CLIENT_SKU IS NULL ").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				String mapped_sevice=db.executeQueryP2("select key_1_val from ariacore.CLIENT_SURCHARGE_DEFINITION  where  surcharge_no="+surNumber+" and key_1_type='SERVICE_NO' and client_no="+clientNo)
				invoice_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") AND ORIG_CLIENT_SKU IS NULL and orig_credit_id is  null and orig_coupon_cd is  null and service_no ="+mapped_sevice).toDouble()
			}
		}
		
		else if(scope.equals("4") && discount_type.equals("A"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" AND gl_charge_seq_num=gl_surcharge_seq_num AND surcharge_no="+surNumber+") AND glt.client_no=als.client_no AND glt.ORIG_CLIENT_SKU IS NULL ").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") AND ORIG_CLIENT_SKU IS NULL").toDouble()
			}
		}
		
		else if(scope.equals("5") && discount_type.equals("A"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" AND gl_charge_seq_num=gl_surcharge_seq_num AND surcharge_no="+surNumber+") AND glt.client_no=als.client_no AND glt.ORIG_CLIENT_SKU IS NULL ").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") AND ORIG_CLIENT_SKU IS NULL").toDouble()
			}
		}
		else if(scope.equals("5") && discount_type.equals("B"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" AND gl_charge_seq_num=gl_surcharge_seq_num AND surcharge_no="+surNumber+") AND glt.client_no=als.client_no AND glt.ORIG_CLIENT_SKU IS NULL ").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				String mapped_sevice=db.executeQueryP2("select key_2_val from ariacore.CLIENT_SURCHARGE_DEFINITION  where  surcharge_no="+surNumber+" and key_2_type='SERVICE_NO' and client_no="+clientNo)
				invoice_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") AND ORIG_CLIENT_SKU IS NULL and orig_credit_id is  null and orig_coupon_cd is  null  and ORIG_CLIENT_SKU IS NULL and  service_no in (select service_no  from ariacore.plan_services where plan_no="+mapped_plan+" and service_no ="+mapped_sevice+")").toDouble()
			}
		}
		
		else if(scope.equals("6")) //Done
		{
			if(discount_type.equals("B"))
			{
				logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
				invoice_amt = db.executeQueryP2("select NVL(sum(usage_units*usage_rate),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no  and glt.client_no=als.client_no where glt.invoice_no="+inv_no+" and  als.is_recurring=1  AND glt.ORIG_CLIENT_SKU IS NULL and glt.orig_credit_id is  null and glt.orig_coupon_cd is  null ").toDouble()
				
			}
			else if(discount_type.equals("A"))
			{
				invoice_amt = db.executeQueryP2("select NVL(sum(usage_units*usage_rate),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no  and glt.client_no=als.client_no where glt.invoice_no="+inv_no+" and  als.is_recurring=1  AND glt.ORIG_CLIENT_SKU IS NULL and glt.orig_credit_id is  null ").toDouble()
			
			}	
			
			
		
		}
		
		else if(scope.equals("7") && discount_type.equals("B"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(SUM(glt.debit),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no where glt.invoice_no="+inv_no+" and glt.seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" and gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber+") and glt.client_no=als.client_no and als.is_usage=1").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber+" and invoice_no="+inv_no+" and client_no="+clientNo).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(SUM(glt.debit),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no where glt.invoice_no="+inv_no+" and glt.seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") and glt.client_no=als.client_no and als.is_usage=1").toDouble()
			}
			
		}
		
		else if(scope.equals("7") && discount_type.equals("A"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(SUM(glt.debit),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no where glt.invoice_no="+inv_no+" and glt.seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" and gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber+") and glt.client_no=als.client_no and als.is_usage=1").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber+" and invoice_no="+inv_no+" and client_no="+clientNo).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(SUM(glt.debit),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no where glt.invoice_no="+inv_no+" and glt.seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") and glt.client_no=als.client_no and als.is_usage=1").toDouble()
			}
		}
		
		else if(scope.equals("8") && discount_type.equals("B"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(SUM(glt.debit),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no where glt.invoice_no="+inv_no+" and glt.seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" and gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber+") and glt.client_no=als.client_no and als.is_setup=1").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber+" and invoice_no="+inv_no+" and client_no="+clientNo).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(SUM(glt.debit),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no where glt.invoice_no="+inv_no+" and glt.seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") and glt.client_no=als.client_no and als.is_setup=1").toDouble()
			}

			
		}
		
		else if(scope.equals("8") && discount_type.equals("A"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(SUM(glt.debit),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no where glt.invoice_no="+inv_no+" and glt.seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" and gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber+") and glt.client_no=als.client_no and als.is_setup=1").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber+" and invoice_no="+inv_no+" and client_no="+clientNo).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(SUM(glt.debit),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no where glt.invoice_no="+inv_no+" and glt.seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") and glt.client_no=als.client_no and als.is_setup=1").toDouble()
			}
			
		}
		
		else if(scope.equals("9") && discount_type.equals("B"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" AND gl_charge_seq_num=gl_surcharge_seq_num AND surcharge_no="+surNumber+") AND glt.client_no=als.client_no AND als.is_setup=1").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber+" and invoice_no="+inv_no+" and client_no="+clientNo).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(SUM(glt.debit),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no where glt.invoice_no="+inv_no+" and glt.seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") and glt.client_no=als.client_no and als.is_setup=1").toDouble()
			}
		}
		
		else if(scope.equals("9") && discount_type.equals("A"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" AND gl_charge_seq_num=gl_surcharge_seq_num AND surcharge_no="+surNumber+") AND glt.client_no=als.client_no AND als.is_setup=1").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber+" and invoice_no="+inv_no+" and client_no="+clientNo).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(SUM(glt.debit),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no where glt.invoice_no="+inv_no+" and glt.seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") and glt.client_no=als.client_no and als.is_setup=1").toDouble()
			}
		}
		
		else if(scope.equals("10") && discount_type.equals("B"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" AND gl_charge_seq_num=gl_surcharge_seq_num AND surcharge_no="+surNumber+") AND glt.client_no=als.client_no AND als.is_setup=1").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber+" and invoice_no="+inv_no+" and client_no="+clientNo).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(SUM(glt.debit),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no where glt.invoice_no="+inv_no+" and glt.seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") and glt.client_no=als.client_no and als.is_setup=1").toDouble()
			}
		}
		
		else if(scope.equals("10") && discount_type.equals("A"))
		{
			logi.logInfo("Scope: "+scope+" and discount_type: "+discount_type)
			if(in_off_ind.equals("I"))
			{
				String ser_amt = db.executeQueryP2("select NVL(Sum(debit),0) from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+inv_no+" and seq_num IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+" AND gl_charge_seq_num=gl_surcharge_seq_num AND surcharge_no="+surNumber+") AND glt.client_no=als.client_no AND als.is_setup=1").toString()
				
				String surchg_amt = db.executeQueryP2("SELECT NVL(sum(actual_surcharge_amt),0) FROM ariacore.GL_DTL_SURCHARGE_DTL where gl_charge_seq_num=gl_surcharge_seq_num and surcharge_no="+surNumber+" and invoice_no="+inv_no+" and client_no="+clientNo).toString()
				invoice_amt = d.format(ser_amt.toDouble() - surchg_amt.toDouble()).toDouble()
			}
			else
			{
				invoice_amt = db.executeQueryP2("select NVL(SUM(glt.debit),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no where glt.invoice_no="+inv_no+" and glt.seq_num NOT IN (Select gl_surcharge_seq_num from ARIACORE.GL_DTL_SURCHARGE_DTL where client_no="+clientNo+" and invoice_no="+inv_no+") and glt.client_no=als.client_no and als.is_setup=1").toDouble()
			}
		}
		logi.logInfo("Invoice amount get_invoice_amt "+invoice_amt.toString())
		return invoice_amt.toString()
	}
	
	public String get_surcharge_type(String surNumber,String clientNo)
	{
		logi.logInfo("Inside get_surcharge_type")
		ConnectDB db = new ConnectDB();
		String scType = db.executeQueryP2("Select surcharge_type_cd from ARIACORE.CLIENT_SURCHARGE where surcharge_no="+surNumber+" and client_no="+clientNo).toString()
		return scType

	}
	
	LinkedHashMap GET_SURCHARGE_PRICE_TIER(String surNumber,String clientNo)
	{
		logi.logInfo "Calling GET_SURCHARGE_PRICE_TIER"
		LinkedHashMap<String,String> rate_tier=new LinkedHashMap<String, String>();
		DecimalFormat d = new DecimalFormat("#.##########");
		ConnectDB db = new ConnectDB();

		String unit_rate_qry = "SELECT NVL(to_unit,0)as to_unit,rate_per_unit FROM ARIACORE.CLIENT_SURCHARGE_PRICE WHERE surcharge_no="+surNumber+" and client_no="+clientNo
		ResultSet rs_unit_rate_qry=db.executePlaQuery(unit_rate_qry)
		while(rs_unit_rate_qry.next())
		{
			String unit = rs_unit_rate_qry.getString(1)
			String rate=rs_unit_rate_qry.getString(2)

			if(unit.equals("0"))
			{
				unit="N"
			}

			rate_tier.put("UB"+unit,rate)
			logi.logInfo( "hm_rates "+rate_tier.toString())
		}
		rs_unit_rate_qry.close();
		return rate_tier
	}
	
	public String get_no_of_services(String surNumber,String acct_no,String scope,String clientNo)
	{
		logi.logInfo("Inside get_no_of_services")
		ConnectDB db = new ConnectDB();
		String inv_no=getInvoiceNoVT(clientNo,acct_no);
		String mapped_plan=db.executeQueryP2("select plan_no from ariacore.plan_surcharge  where  surcharge_no="+surNumber)
		logi.logInfo(" mapped_plan " +mapped_plan)
		String serCnt=""
		if(scope.equals("1"))
		{
			serCnt = db.executeQueryP2("Select count(glt.service_no) from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no="+inv_no+"and ser.service_type NOT IN ('SC') and glt.client_no=ser.client_no and glt.client_no="+clientNo)
		}
		else if(scope.equals("2"))
		{
			serCnt = db.executeQueryP2("Select count(glt.service_no) from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no="+inv_no+"and ser.service_type NOT IN ('SC','OR') and glt.client_no=ser.client_no and glt.client_no="+clientNo)
		}
		else if(scope.equals("3"))
		{
			serCnt = db.executeQueryP2("Select count(glt.service_no) from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no="+inv_no+"and ser.service_type NOT IN ('SC') and glt.client_no=ser.client_no and glt.client_no="+clientNo)
		}
		else if(scope.equals("4"))
		{
			String mapped_sevice=db.executeQueryP2("select key_1_val from ariacore.CLIENT_SURCHARGE_DEFINITION  where  surcharge_no="+surNumber+" and key_1_type='SERVICE_NO' and client_no="+clientNo)
			serCnt = db.executeQueryP2("Select count(glt.service_no) from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no="+inv_no+"and ser.service_type NOT IN ('SC') and glt.client_no=ser.client_no and glt.service_no="+mapped_sevice+" and glt.client_no="+clientNo)
		}
		else if(scope.equals("5"))
		{
			String mapped_sevice=db.executeQueryP2("select key_2_val from ariacore.CLIENT_SURCHARGE_DEFINITION  where  surcharge_no="+surNumber+" and key_2_type='SERVICE_NO' and client_no="+clientNo)
			serCnt = db.executeQueryP2("Select count(glt.service_no) from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no="+inv_no+"and ser.service_type NOT IN ('SC') and glt.client_no=ser.client_no and  glt.client_no="+clientNo+"and   glt.service_no in (select service_no  from ariacore.plan_services where plan_no="+mapped_plan+" and service_no ="+mapped_sevice+")")
		}
		else if(scope.equals("6"))
		{
			serCnt = db.executeQueryP2("Select count(glt.service_no) from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no="+inv_no+"and ser.is_recurring=1 and glt.client_no=ser.client_no and glt.client_no="+clientNo)
		}
		else if(scope.equals("7"))
		{
			serCnt = db.executeQueryP2("Select count(distinct glt.service_no) from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no="+inv_no+"and ser.is_usage=1 and glt.client_no=ser.client_no and glt.client_no="+clientNo)
		}
		else if(scope.equals("8"))
		{
			serCnt = db.executeQueryP2("Select count(glt.service_no) from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no="+inv_no+"and ser.service_type IN ('AC') and glt.client_no=ser.client_no and glt.client_no="+clientNo)
		}
		else if(scope.equals("9"))
		{
			serCnt = db.executeQueryP2("Select count(glt.service_no) from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no="+inv_no+"and ser.service_type NOT IN ('SC') and glt.client_no=ser.client_no and glt.client_no="+clientNo)
		}
		else if(scope.equals("10"))
		{
			serCnt = db.executeQueryP2("Select count(glt.service_no) from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no="+inv_no+"and ser.service_type NOT IN ('SC') and glt.client_no=ser.client_no and glt.client_no="+clientNo)
		}
		
		logi.logInfo("Total services cnt: "+serCnt)
		return serCnt
	}
	
	public String calculate_surcharge_amt(String invoice_amt,LinkedHashMap<String,String> price_tier,String servicesCnt,String type)
	{
		logi.logInfo("Inside calculate_surcharge_amt")
		DecimalFormat d = new DecimalFormat("#.##########");
		String amount="0"
		
		if(type.equals("F"))
		{
			logi.logInfo("type: "+type)
			List<String> keyList = new ArrayList<String>(price_tier.keySet());
			List<String> valList = new ArrayList<String>(price_tier.values());
			if(invoice_amt.toDouble() >= 0)
			{
				if(price_tier.size()==1)
				{
					amount = (servicesCnt.toDouble()*price_tier.get("UBN").toDouble()).toString()
				}
				else
				{
					for(int i=0;i<keyList.size();i++)
					{
						if(keyList.get(i)!="UBN")
						{
							double max_units = (keyList.get(i).split("UB")[1]).toDouble()
							logi.logInfo("now max units"+max_units)
							if(invoice_amt.toDouble() <= max_units)
							{
								amount = (servicesCnt.toDouble()*valList.get(i).toDouble()).toString()
								break;
							}
							
						}
						else
						{
							amount = (servicesCnt.toDouble()*valList.get(i).toDouble()).toString()
							break;
						}
					}
				}
			}
			
			logi.logInfo("Fixed calculated amount:"+d.format(amount.toDouble()).toString())
		}
		else if(type.equals("P"))
		{
			logi.logInfo("type: "+type)
			List<String> keyList = new ArrayList<String>(price_tier.keySet());
			List<String> valList = new ArrayList<String>(price_tier.values());
			if(invoice_amt.toDouble() >= 0)
			{
				if(price_tier.size()==1)
				{
					amount = (invoice_amt.toDouble()*(price_tier.get("UBN").toDouble()/100).toDouble()).toString()
				}
				else
				{
					for(int i=0;i<keyList.size();i++)
					{
						if(keyList.get(i)!="UBN")
						{
							double max_units = (keyList.get(i).split("UB")[1]).toDouble()
							logi.logInfo("now max unit is"+max_units)
							if(invoice_amt.toDouble() <= max_units)
							{
								amount = (invoice_amt.toDouble()*(valList.get(i).toDouble()/100).toDouble()).toString()
								break;
							}
						}
						
						else
						{
							amount = (invoice_amt.toDouble()*(valList.get(i).toDouble()/100).toDouble()).toString()
							break;
						}
					}
				}
			}
			
			logi.logInfo("percentage amount:"+d.format(amount.toDouble()).toString())
		}
		
		return d.format(amount.toDouble()).toString()
	}
	public String md_IS_CREDIT_APPLIED(String tcid)
	{
		   String Flg=""
		   String apiname=tcid.split("-")[2]
		   String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		   String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		   String invoice_no=getInvoiceNoVT(clientNo, acct_no).toString();
		   ConnectDB db = new ConnectDB();
		   int surChargeCnt = db.executeQueryP2("select count(*) from ariacore.gl_detail where invoice_no="+invoice_no+" and orig_credit_id is not null and orig_coupon_cd is null").toInteger()
		   if(surChargeCnt == 0)
		   {
				  Flg="FALSE"
		   }
		   else if (surChargeCnt > 0)
		   {
				  Flg="TRUE"
		   }
	}
	
	public String get_app_method(String surNumber,String clientNo)
	{
		logi.logInfo("Inside get_app_method")
		ConnectDB db = new ConnectDB();
		String app_met = db.executeQueryP2("Select inline_offset_ind from ARIACORE.CLIENT_SURCHARGE where surcharge_no="+surNumber+" and client_no="+clientNo).toString()
		return app_met
	}
	
	public String md_IS_COUPON_APPLIED(String tcid)
	{
		String success=""
		logi.logInfo("Inside md_IS_COUPON_APPLIED")
		ConnectDB db = new ConnectDB();
		String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoice_no=getInvoiceNoVT(clientNo, acct_no).toString();
		String cnt = db.executeQueryP2("Select count(*) from ariacore.gl_detail where invoice_no="+invoice_no+" and client_no="+clientNo+" and orig_coupon_cd IS NOT NULL").toString()
		
		if(cnt.equals("0"))
		{
			success = "FALSE"
		}
		else
		{
			success = "TRUE"
		}
		
		return success
	}
	
	public String md_GET_ROLL_OVER_PLAN_EXP(String tcid)
	{
		logi.logInfo("Inside md_GET_ROLL_OVER_PLAN_EXP")
		String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB();
		String old_plan_no = db.executeQueryP2("Select old_plan from ariacore.acct_plan_hist where acct_no="+acct_no+" and to_date IS NULL and client_no="+clientNo).toString()
		
		String ro_plan_no= db.executeQueryP2("Select ROLLOVER_PLAN_NO from ARIACORE.CLIENT_PLAN where PLAN_NO ="+old_plan_no+" and client_no="+clientNo).toString()
		return ro_plan_no
	}
	
	public List GET_ALL_SURCHARGES_LIST(String tcid,String acct_no)
	{
		logi.logInfo("Inside GET_ALL_SURCHARGES_LIST")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB();
		List<String> surchargeNos =[];
		String surnos_qry = "SELECT surcharge_no FROM ariacore.acct_surcharge WHERE acct_no="+acct_no+" UNION  SELECT surcharge_no FROM ariacore.plan_surcharge WHERE plan_no IN (SELECT plan_no FROM ariacore.all_acct_active_plans WHERE acct_no="+acct_no+")"		
		ResultSet rs= db.executePlaQuery(surnos_qry)		
		while(rs.next())
		{
			String surNo = rs.getString(1)
			surchargeNos.add(surNo)
		}
		
		return surchargeNos
	}
	
	
	LinkedHashMap md_APPLIED_SURCHARGES_DETAILS_DB(String tcid)
	{
		logi.logInfo("Calling md_APPLIED_SURCHARGES_DETAILS_DB")
		String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		List surchargeNos=GET_ALL_SURCHARGES_LIST(tcid,acct_no)
		ConnectDB db = new ConnectDB();
		logi.logInfo(" surchargeNos " +surchargeNos.toString())
		LinkedHashMap<String,HashMap<String,String>> allsurdetails=new HashMap<String,HashMap<String,String>>()
		for (sno in surchargeNos)
		{
			logi.logInfo "inside"
			
			HashMap<String,String> surdetails=new HashMap<String,String>()
			String surname=db.executeQueryP2("select surcharge_name from ariacore.client_surcharge  where  surcharge_no="+sno+" and client_no="+clientNo)
			surdetails.put("SURCHARGE_NAME", surname)
			String isappliedflag=md_IS_SURCHARGE_APPLIED(tcid,sno)
			surdetails.put("IS_SURCHARGE_APPLIED", isappliedflag)
			logi.logInfo(" surdetails " +surdetails.toString())
			allsurdetails.put(sno, surdetails)
		}
		logi.logInfo(" allsurdetails " +allsurdetails.toString())
		return allsurdetails
		
	}
	
	LinkedHashMap md_APPLIED_SURCHARGES_DETAILS_EXP(String tcid)
	{
		logi.logInfo("Calling md_APPLIED_SURCHARGES_DETAILS_EXP")
		String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		List surchargeNos=GET_ALL_SURCHARGES_LIST(tcid,acct_no)
		String invoice_no=getInvoiceNoVT(clientNo, acct_no).toString();
		ConnectDB db = new ConnectDB();
		logi.logInfo(" surchargeNos " +surchargeNos.toString())
		LinkedHashMap<String,HashMap<String,String>> allsurdetails=new HashMap<String,HashMap<String,String>>()
		for (sno in surchargeNos)
		{
			logi.logInfo "inside"
			
			HashMap<String,String> surdetails=new HashMap<String,String>()
			String surname=db.executeQueryP2("select surcharge_name from ariacore.client_surcharge  where  surcharge_no="+sno+" and client_no="+clientNo)
			surdetails.put("SURCHARGE_NAME", surname)
			String isappliedflag=md_IS_SURCHARGE_ELIGIBLE_FOR_BILLING(tcid,invoice_no,sno)
			surdetails.put("IS_SURCHARGE_APPLIED", isappliedflag)
			logi.logInfo(" surdetails " +surdetails.toString())
			allsurdetails.put(sno, surdetails)
		}
		logi.logInfo(" allsurdetails " +allsurdetails.toString())
		return allsurdetails
		
	}
	
	String md_IS_SURCHARGE_ELIGIBLE_FOR_BILLING(String tcid,String invoiceno,String surNumber)
	{
		logi.logInfo("Calling md_IS_SURCHARGE_ELIGIBLE_FOR_BILLING")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String scope=Get_surcharge_scope(surNumber, clientNo)
		String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		logi.logInfo(" scope " +scope.toString())
		String flag=""
		ConnectDB db = new ConnectDB();
		String mapped_plan=db.executeQueryP2("select plan_no from ariacore.plan_surcharge  where  surcharge_no="+surNumber)
		logi.logInfo(" mapped_plan " +mapped_plan)
		String mapped_sevice
		
		Map<String, HashMap<String,String>> hash = getPlanServiceAndTieredPricingAllCharges()
		HashMap<String,String> servicetypes=new HashMap<String,String>()
		List<String> acct_services=[] 
		logi.logInfo hash.toString()
		hash.each { k,v-> 		
			v.each { k1,v1 ->
				acct_services.add(k1.toString())
			}
		} 
		logi.logInfo "acct_services "+acct_services.toString()
		String services=acct_services.toString().replace("[", "(")
		services=services.toString().replace("]", ")")
		logi.logInfo "framed acct_services "+services
		String acct_plan=db.executeQueryP2("select plan_no from ariacore.acct_service_map  where  service_no in"+services+" and acct_no="+acct_no)
		logi.logInfo(" acct_plan " +acct_plan)
		for(String s in acct_services)
		{
			String stype=db.executeQueryP2("select service_type from ariacore.all_service where service_no="+s)
			servicetypes.put(s,stype)
			
		}
		String acct_currency=db.executeQueryP2("SELECT CURRENCY_CD FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+clientNo)
		String sur_currency=db.executeQueryP2("SELECT currency_cd from ariacore.CLIENT_SURCHARGE_CURRENCY_MAP where surcharge_no="+surNumber+" AND CLIENT_NO="+clientNo)
		if(sur_currency==acct_currency)
		{
			if(scope=="1") //All Charges
			{
				logi.logInfo("All charges scope")
				flag= "TRUE"				
			}
			else if(scope=="2") //All Charges
			{
				logi.logInfo("All plan services scope")
				flag= "TRUE"
			}
			else if(scope=="7") //All Charges
			{
				logi.logInfo("All Usage Service Charges")
				
				if (servicetypes.values().contains("US") )
				{
					int recorded_usages=db.executeQueryP2("select count(*) from ariacore.usage where acct_no="+acct_no).toInteger()
					if(recorded_usages>0)
					flag= "TRUE"
					else
					flag= "FALSE"
					
				}
				
			}
			else if(scope.equals("3"))
			{
				logi.logInfo("All Plan Charges, Specified Plans")
				
				if(mapped_plan==acct_plan)
				flag= "TRUE"
				else
				flag= "FALSE"
				
			}
			else if(scope.equals("4"))
			{
				logi.logInfo("All Plans, Specified Service Charges")	
				mapped_sevice=db.executeQueryP2("select key_1_val from ariacore.CLIENT_SURCHARGE_DEFINITION  where  surcharge_no="+surNumber+" and key_1_type='SERVICE_NO' and client_no="+clientNo)
				logi.logInfo(" mapped_sevice " +mapped_sevice)
				if(acct_services.contains(mapped_sevice))
				flag= "TRUE"
				else
				flag= "FALSE"				
				
			}
			else if(scope.equals("5"))
			{
				logi.logInfo("Specified Plan/Service Charges")
				mapped_sevice=db.executeQueryP2("select key_2_val from ariacore.CLIENT_SURCHARGE_DEFINITION  where  surcharge_no="+surNumber+" and key_2_type='SERVICE_NO' and client_no="+clientNo)
				logi.logInfo(" mapped_sevice " +mapped_sevice)
				if(mapped_plan==acct_plan && acct_services.contains(mapped_sevice) )
				flag= "TRUE"
				else
				flag= "FALSE"

				
			}
			else if(scope.equals("6"))
			{
				logi.logInfo("All Recurring Service Charges")
				
				int rec_services_cnt=db.executeQueryP2("select count(*) from ariacore.all_service where service_no in "+services+" and client_no="+clientNo+" and Is_recurring=1").toString().toInteger()
				if(rec_services_cnt >0 )
				flag= "TRUE"
				else
				flag= "FALSE"
				
			}
			else if(scope.equals("8"))
			{
				logi.logInfo("All Activation Service Charges")
				int act_services_cnt=db.executeQueryP2("select count(*) from ariacore.all_service where service_no in "+services+" and client_no="+clientNo+" and service_type='AC'and Is_setup=1").toString().toInteger()
				if(act_services_cnt >0 )
				flag= "TRUE"
				else
				flag= "FALSE"
				
				
			}
			
		}
		else
		flag= "FALSE"
		
		String invoice_amt = db.executeQueryP2("select NVL(sum(usage_units*usage_rate),0) from ariacore.gl_detail glt join ariacore.all_service als ON  glt.service_no=als.service_no  and glt.client_no=als.client_no where glt.invoice_no="+invoiceno+" and glt.orig_credit_id is  null AND als.service_type NOT IN 'SC'")
		String applly_zero_inv = db.executeQueryP2("Select apply_to_zero_invoice_ind from ariacore.client_surcharge where surcharge_no="+surNumber+" and client_no="+clientNo).toString()
		if(invoice_amt.equals("0") && applly_zero_inv.equals("0"))
		
		{
			logi.logInfo("invoice is Zero and apply_to_zero_invoice_ind flag is FALSE")
			flag= "FALSE"
		}
		
		return flag
		
		
	}
	public String md_IS_SURCHARGE_APPLIED(String tcid,String surcharge_no)
	{
		   String surchargeFlg=""
		   String apiname=tcid.split("-")[2]
		   String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		   String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		   String invoice_no=getInvoiceNoVT(clientNo, acct_no).toString();
		   ConnectDB db = new ConnectDB();
		   
		   int surChargeCnt = db.executeQueryP2("Select count(*) from ARIACORE.GL_DTL_SURCHARGE_DTL where invoice_no="+invoice_no +" and surcharge_no="+surcharge_no).toInteger()
		   
		   if(surChargeCnt == 0)
		   {
				  surchargeFlg="FALSE"
		   }
		   else if (surChargeCnt > 0)
		   {
				  surchargeFlg="TRUE"
		   }
	}
	public String md_IS_SURCHARGE_CREDIT_INCLUDED (String tcid)
	{
		   List<String> Flg=[]
		   String apiname=tcid.split("-")[2]
		   String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		   String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		   String invoice_no=getInvoiceNoVT(clientNo, acct_no).toString();
		   ConnectDB db = new ConnectDB();
		   List surchargeNos=GET_ALL_SURCHARGES_LIST(tcid,acct_no)
		   for (sno in surchargeNos)
		   {
			   String sur_ext_dec=db.executeQueryP2("select ext_desc from ariacore.client_surcharge  where  surcharge_no="+sno+" and client_no="+clientNo)
			   String credit_comments=db.executeQueryP2("select distinct orig_credit_comments from ariacore.gl_detail where invoice_no="+invoice_no+"  and orig_credit_id is not null")
			   if(credit_comments.contains(sur_ext_dec))
			   Flg.add("TRUE")
			   else
			   Flg.add("FALSE")
		   }
		   
		   if(Flg.contains("FALSE"))
		   return "FALSE"
		   else
		   return "TRUE"
		   
		 
	}
	
	
	public String md_UPDATED_PLAN_NO(String tcid)
	{
		logi.logInfo("Inside md_UPDATED_PLAN_NO")
		String plan_no = getValueFromRequest("update_master_plan","//master_plan_no")
		
		return plan_no
	}
	
	public String md_UPDATED_CURRENCY_CODE(String tcid)
	{
		logi.logInfo("Inside md_UPDATED_CURRENCY_CODE")
		String force_cur = getValueFromRequest("update_master_plan","//force_currency_change")
		String cur_cd=""
		String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB();
		if(force_cur.equalsIgnoreCase("TRUE"))
		{
			String plan_no = getValueFromRequest("update_master_plan","//master_plan_no")
			cur_cd = db.executeQueryP2("Select currency_cd from ariacore.client_plan where plan_no="+plan_no+" and client_no="+clientNo).toString()
		}
		else
		{
			cur_cd = db.executeQueryP2("Select currency_cd from ariacore.acct where acct_no="+acct_no+" and client_no="+clientNo).toString()
		}
		
		return cur_cd
	}
	
	/**
	 * Getting the expected invoice with surcharge
	 * @param testCaseId
	 * @param acct_no
	 * @return String
	 */
	public String md_ExpInvoiceWithSurcharge_Amt(String testCaseId){
		logi.logInfo("calling md_ExpInvoiceWithSurcharge_Amt")
		String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=testCaseId.split("-")[2]
		double totalInvoice=0.0
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no).toString();
		logi.logInfo( "invoiceNo in md_ExpInvoiceWithSurcharge_Amt method " +invoiceNo)
		double exp = md_ExpInvoice_Amt(testCaseId,acct_no).toDouble()
		logi.logInfo("expInvoice "+exp.toString())

		logi.logInfo "Incoming acct"+acct_no

		List<String> usagekeys=[]
		Constant.recordedusageamounts.each
		{ k,v ->
			logi.logInfo "key now in hash "+k.split("-")[3]
			if(k.split("-")[3].equals(acct_no)==true)

			{
				//usg=usg+v.toString().toDouble()
				usagekeys.add(k)
			}

		}
		logi.logInfo("usagekeys list  "+usagekeys.toString())

		boolean auto_rateFlg = get_Auto_Rate_Unrated_Usage_Flag()
		String usg
		if(!auto_rateFlg){
			usg = md_ExpUsage_Amt(testCaseId,acct_no)
			//md_ExpUsage_Amt_With_Auto_Rate(testCaseId)
		} else
			usg=md_ExpUsage_Amt(testCaseId,acct_no)
		logi.logInfo("usgInvoice "+usg.toString())
		double creditamt=db.executeQueryP2("select NVL(sum(credit),0) from ariacore.gl where acct_no="+acct_no+" and client_no="+clientNo+" and invoice_no="+invoiceNo).toString().toDouble()

		String resp_level_cd = db.executeQueryP2("select resp_level_cd from ariacore.acct where acct_no="+acct_no+" and client_no="+clientNo).toString()
		if(resp_level_cd == '2' || !getValueFromRequest("create_acct_complete","//collections_acct_groups").equals("NoVal") || !getValueFromRequest("create_acct_complete","//coupon_codes").equals("NoVal"))
			creditamt = 0.0
		totalInvoice =  exp+ (usg.toDouble().round(2))-creditamt.round(2)
		logi.logInfo("totalInvoice "+totalInvoice)
		//Constant.recordedusageamounts.clear()

		logi.logInfo "All recorded usage hash before "+testCaseId.split("-")[1] + " => "+Constant.recordedusageamounts.toString()

		/*Clearing usage hash for bill lag day usage amount calculation*/
		ReadData objRD =new ReadData()
		logi.logInfo " tc id " +testCaseId
		String localTcId = testCaseId.split("-")[1]
		logi.logInfo " local tc id " +localTcId
		String ClearHashVal = objRD.getTestDataMap("create_acct_complete",localTcId).get("Clear_UsageHash")
		logi.logInfo "Usage Clear Hash Value " + ClearHashVal
		if(!ClearHashVal.equalsIgnoreCase("No"))
		{
			logi.logInfo "Gonna clear usage hash"
			for (key in usagekeys){
				Constant.recordedusageamounts.remove(key)
			}
		}

		logi.logInfo "All recorded usage hash after "+testCaseId.split("-")[1] + " => "+Constant.recordedusageamounts.toString()
		String surAmount=md_GET_SURCHAGRE_AMT_EXP(testCaseId)
		logi.logInfo("surAmount "+surAmount)
		return d.format(Math.abs(totalInvoice+surAmount.toDouble().round(2))).toString()
	}
	
	public  String md_GET_TOTAL_SERVICE_CREDIT_AMT_EXP(String tcid)
	{
		logi.logInfo "Calling md_GET_TOTAL_SERVICE_CREDIT_AMT_EXP "
	
	DecimalFormat d = new DecimalFormat("#.##########");
	String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	String accountNo=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
	ConnectDB database = new ConnectDB();
	SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
	String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
	String currentVirtualTime = database.executeQueryP2(Query);
	logi.logInfo "Current Virtual Time : "+ currentVirtualTime
	//int useddays=database.executeQueryP2("SELECT (TRUNC(next_bill_date) -  trunc(plan_date)) AS days FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo).toInteger()
	int useddays=database.executeQueryP2("SELECT TRUNC(LAST_BILL_THRU_DATE+1) - (select CASE WHEN  (trunc(plan_date) >= trunc(last_recur_bill_date) or last_recur_bill_date is null ) THEN trunc(plan_date) ELSE trunc(last_recur_bill_date)  END 	FROM ariacore.acct WHERE acct_no = "+accountNo+")from ariacore.acct WHERE acct_no = "+accountNo+"  AND client_no ="+clientNo).toInteger()

	Map<String, HashMap<String,String>> hash = getPlanServiceAndTieredPricing()

	Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
	resultHash.putAll(hash)

	logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()

	Iterator iterator = resultHash.entrySet().iterator()
	logi.logInfo "Before Key setting "

	def invoice = 0
	double proration_factor

	while (iterator.hasNext()) {
		Map.Entry pairs = (Map.Entry) iterator.next()
		String planNo = pairs.getKey().toString()
		logi.logInfo "Execution for the plan :  "+ planNo
		String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
		String planStatusQuery, planStatusDataQuery

		int planTypeInd = Integer.parseInt(database.executeQueryP2(planTypeQuery))

		logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo


		if(planTypeInd==1) {
			planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT_SUPP_PLAN_MAP AC WHERE ACCT_NO   ="+accountNo+" AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo
			planStatusDataQuery = "SELECT status_date from ariacore.ACCT_SUPP_PLAN_MAP where acct_no = "+ accountNo +"AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo

		}
		else {
			planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+" AND CLIENT_NO= " + clientNo
			planStatusDataQuery = "SELECT status_date FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+ " AND CLIENT_NO= " + clientNo

		}

		int planStatus = Integer.parseInt(database.executeQueryP2(planStatusQuery))
		logi.logInfo "Plan Status : "+planStatus+ " plan : " +planNo
		logi.logInfo "Plan planStatusDataQuery : "+planStatusDataQuery+ " plan : " +planNo


		if(planStatus!=0){
			String statusDateDB = database.executeQueryP2(planStatusDataQuery)
			logi.logInfo "Status date from DB: "+statusDateDB
			Date statusDate = simpleFormat.parse(statusDateDB)

			// Stores the current virtual time in the specified format
			Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
			logi.logInfo "planStatus : "+planStatus+ " plan : " +planNo
			logi.logInfo "statusDate.compareTo(currentVTDate) : "+statusDate.compareTo(currentVTDate)+ " plan : " +planNo


			if((planStatus==1 && statusDate.compareTo(currentVTDate)<=0)|| (planTypeInd==0)){

				Map<String,String> valueMap = pairs.getValue()
				Map<String,String> value = new TreeMap<String,String> ()
				value.putAll(valueMap)

				int billingInterval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planNo+ "and client_no="+clientNo))
				Date nextbilldate
				ResultSet rs=database.executePlaQuery("SELECT TRUNC(LAST_BILL_THRU_DATE+1) FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo)
				rs.next()
				nextbilldate=rs.getDate(1)
				Date virtualstartdate = subractMonthsFromGivenDate(nextbilldate,billingInterval)

				logi.logInfo "virtualstartdate "+virtualstartdate
				logi.logInfo "nextbilldate " +nextbilldate
				int durationOfPlan = getDaysBetween(virtualstartdate,nextbilldate )
				logi.logInfo "Duration of a plan : "+planNo + " :: "+durationOfPlan
				logi.logInfo "DaysInvoice "+useddays
				logi.logInfo "daysInvoice % durationOfPlan Value : "+ (useddays % durationOfPlan )
				int tmp_days = durationOfPlan-useddays
				if(tmp_days<=0)
					tmp_days = durationOfPlan
				proration_factor=useddays/durationOfPlan.toDouble()
				logi.logInfo "proration_factor "+proration_factor
				int invoice_count=database.executeQueryP2("select count(*) from  ARIACORE.GL  WHERE ACCT_NO = "+accountNo).toInteger()
				String last_invoice
				if (invoice_count<=1)
				last_invoice=database.executeQueryP2("select  max(INVOICE_NO) from ARIACORE.GL WHERE ACCT_NO = "+accountNo)
				else
				last_invoice=database.executeQueryP2("select max(INVOICE_NO) from ARIACORE.GL  WHERE ACCT_NO = "+accountNo+" and  INVOICE_NO not in (select  max(INVOICE_NO) from ARIACORE.GL WHERE ACCT_NO = "+accountNo+")")
				double last_invoice_amt=database.executeQueryP2("Select NVL(sum(usage_units*usage_rate),0)  from ariacore.gl_detail glt  join ariacore.all_service ser on glt.service_no=ser.service_no and glt.client_no=ser.client_no  where glt.invoice_no="+last_invoice+" and ser.service_type NOT IN ('SC','AC') and glt.client_no="+clientNo).toDouble()
				return d.format((last_invoice_amt*proration_factor).round(2)).toString()
			}
		}
	  }
	}
	public String GET_SURCHARGE_NO_FOR_PLAN(String plan_no,String rownum)
	{
		logi.logInfo("Inside GET_SURCHARGE_NO_FOR_PLAN")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB();
		String sur_no = db.executeQueryP2("SELECT surcharge_no from (Select surcharge_no,row_number() over (order by surcharge_no asc) as rowno FROM ariacore.plan_surcharge where plan_no="+plan_no+" and client_no="+clientNo+") where rowno="+rownum)
		
		return sur_no
	}
	
	public String md_isSurchargeRemovedUAC(String tcid)
	{
		boolean removedFlag = false
		String surchargeNo = getValueFromRequest("update_acct_complete","//*/update_surcharge/update_surcharge_row[1]/surcharge_no")
		logi.logInfo("Surcharge no : "+surchargeNo)
		
		String surchargeDir = getValueFromRequest("update_acct_complete","//*/update_surcharge/update_surcharge_row[1]/surcharge_directive")
		logi.logInfo("Surcharge Dir : "+surchargeDir)
		
		String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		List availableSurcharges = GET_SURCHARGE_NO_LIST(tcid,acct_no)
		logi.logInfo("Available list of surcharges : "+availableSurcharges.toString())
		if(Integer.parseInt(surchargeDir) == 2)
		{
			logi.logInfo("Inside directory if loop")
			if(!availableSurcharges.contains(surchargeNo))
			{
				logi.logInfo("Inside list if loop")
				removedFlag = true
			}
		}
	
		return 	removedFlag.toString().toUpperCase()
	}
	
	public String md_CHILD_POOLING_PLAN_NO(String tcid)
	{
		String acct_no=getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		ConnectDB db = new ConnectDB();
		String plan_no = db.executeQueryP2("SELECT distinct plan_no from ariacore.acct_plan_params where param_name='ENABLE_USAGE_POOLING' and acct_no="+acct_no).toString()
		
		return plan_no
	}
	
	public String md_UPDATED_PLAN_NO_UAC(String tcid)
	{
		logi.logInfo("Inside md_UPDATED_PLAN_NO_UAC")
		String plan_no = getValueFromRequest("update_acct_complete","//master_plan_no")
		logi.logInfo("plan_no"+plan_no)
		return plan_no
	}
	public String md_Is_Surcharge_LineItem_Reversed(String tcid)
	{
		boolean Flag = false		
		String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()		
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no).toString();	
		String sname=db.executeQueryP2("select surcharge_name from ariacore.client_surcharge where surcharge_no in (select surcharge_no  from ariacore.acct_surcharge where acct_no="+acct_no+" )")
		String query=" SELECT count(*)   FROM ariacore.gl_Detail  WHERE invoice_no="+invoiceNo+" and comments  like '%"+sname+"%'   AND seq_num  IN (SELECT gl_seq_num FROM ariacore.refund_detail WHERE invoice_no="+invoiceNo+" )"
		logi.logInfo("query"+query)
		int cnt=db.executeQueryP2(query).toString().toInteger()
		if(cnt >0)
		Flag=true
		return Flag.toString().toUpperCase()
		
	}
	
	String getPendingInvoiceNoVT(String clientNo, String accountNo) throws ParseException, SQLException{
		logi.logInfo "Calling getPendingInvoiceNoVT"
		ConnectDB db = new ConnectDB();
		String invoiceNo

		logi.logInfo("current VT "+db.executeQueryP2("select trunc(ariacore.ariavirtualtime("+clientNo+")) from dual"))
		String currentVirtualTime = getStoredVirtualTime(clientNo)
		logi.logInfo("stored VT "+currentVirtualTime)

		ResultSet rs=db.executePlaQuery("SELECT max(invoice_no) FROM ARIACORE.GL_PENDING Where ACCT_NO ="+accountNo+" and client_no="+clientNo)

		if(rs.next())
			invoiceNo=rs.getString(1)
		else
			invoiceNo = "Invoice not updated in DB "

		logi.logInfo 'Invoice No : : ' + invoiceNo
		return invoiceNo;
	}
	
	
	public String md_IS_SURCHARGE_PRORATED(String tcid)
	{
		boolean Flag = false
		String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no).toString();
		String sname=db.executeQueryP2("select surcharge_name from ariacore.client_surcharge where surcharge_no in (select surcharge_no  from ariacore.acct_surcharge where acct_no="+acct_no+" )")
		String query=" SELECT count(*)   FROM ariacore.gl_Detail  WHERE invoice_no="+invoiceNo+" and orig_credit_id is not null and  orig_credit_comments  like '%"+sname+"%' "
		logi.logInfo("query"+query)
		int cnt=db.executeQueryP2(query).toString().toInteger()
		if(cnt >0)
		Flag=true
		return Flag.toString().toUpperCase()
		
	}
	//verify child acct voided invoice
	def md_IsPrevInvVoidedInPlanChange1(String tcid)
	{
		String seq_no="1"
		logi.logInfo "Entered into md_IsPrevInvVoidedInPlanChange "
		ConnectDB db=new ConnectDB()
		boolean voided = false
		def acct_no=getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def invoiceno= getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String voided_q="SELECT COUNT(*) FROM ARIACORE.ACCT_TRANSACTION WHERE EVENT_NO IN(SELECT VOIDING_EVENT_NO FROM((SELECT VOIDING_EVENT_NO,ROW_NUMBER() OVER(ORDER BY INVOICE_NO) AS NUM  FROM ARIACORE.GL WHERE ACCT_NO="+acct_no+")A)WHERE NUM="+seq_no+")AND TRANSACTION_TYPE IN(-1) AND ACCT_NO="+acct_no
		String voidflag=db.executeQueryP2(voided_q)
		if(voidflag.equals("1"))
			voided= true
		return voided.toString().toUpperCase()
	}
	
	//verify child acct voided invoice in create_acct_hiearachy
	def md_IsPrevInvVoidedInPlanChangehie(String tcid,String seq_no)
	{
	
		logi.logInfo "Entered into md_IsPrevInvVoidedInPlanChange "
		ConnectDB db=new ConnectDB()
		boolean voided = false
		def acct_no=getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO3)
		def invoiceno= getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_INVOICE_NO_3)
		String voided_q="SELECT COUNT(*) FROM ARIACORE.ACCT_TRANSACTION WHERE EVENT_NO IN(SELECT VOIDING_EVENT_NO FROM((SELECT VOIDING_EVENT_NO,ROW_NUMBER() OVER(ORDER BY INVOICE_NO) AS NUM  FROM ARIACORE.GL WHERE ACCT_NO="+acct_no+")A)WHERE NUM="+seq_no+")AND TRANSACTION_TYPE IN(-1) AND ACCT_NO="+acct_no
		String voidflag=db.executeQueryP2(voided_q)
		if(voidflag.equals("1"))
			voided= true
		return voided.toString().toUpperCase()
	}
	
	//
	
	
	
	
	//get  transacetion information fromn db
	
	
	def md_transaction_details_from_DB_for_parent(String testcaseid,String transtype,String API) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()

		ConnectDB db=new ConnectDB()
String acct_no = getValueFromResponse(API, ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
logi.logInfo("xxxxxxxxxxxxxxxx"+acct_no)
String q = "SELECT * FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" and transaction_type="+transtype+""
logi.logInfo("xxxxxxx  "+q)

ResultSet rs=db.executePlaQuery(q)

while(rs.next())
{
hashResult.put("CURRENCY_CODE", rs.getString('CURRENCY_CD'))
hashResult.put("TRANSACTION_TYPE", rs.getString('TRANSACTION_TYPE'))
hashResult.put("EVENT_NO", rs.getString('EVENT_NO'))
hashResult.put("AMOUNT", rs.getString('AMOUNT'))

}
String update=db.executeQueryP2("SELECT TO_CHAR(UPDATE_DATE,'yyyy-MM-dd hh:mm:ss') FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" and transaction_type="+transtype+"")
String fully_applied=db.executeQueryP2("SELECT TO_CHAR(FULLY_APPLIED_DATE,'yyyy-MM-dd hh:mm:ss') FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" and transaction_type="+transtype+"")
hashResult.put("UPDATE_DATE",update)
hashResult.put("FULLY_APPLIED_DATE",fully_applied)

logi.logInfo("Hashmap is : " + hashResult)
return hashResult.sort()


}

	//get_tranasaction informatio API
	def md_transaction_details_from_API_for_parent(String testcaseid,String seq_No,String API) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		String currency_cd= getValueFromResponse(API, "//*[1]/transaction_information[1]/*:transaction_information_row["+seq_No+"]/*:currency_cd[1]")
		
		String transaction_type= getValueFromResponse(API,"//*[1]/transaction_information[1]/*:transaction_information_row["+seq_No+"]/*:transaction_type_no[1]")
		
		String event_no= getValueFromResponse(API,"//*[1]/transaction_information[1]/*:transaction_information_row["+seq_No+"]/*:aria_event_no[1]")
		
		String amount= getValueFromResponse(API,"//*[1]/transaction_information[1]/*:transaction_information_row["+seq_No+"]/*:amount[1]")
		
		String update_date= getValueFromResponse(API,"//*[1]/transaction_information[1]/*:transaction_information_row["+seq_No+"]/*:update_date[1]")
		
		String fullyapplied_date= getValueFromResponse(API,"//*[1]/transaction_information[1]/*:transaction_information_row["+seq_No+"]/*:fully_applied_date[1]")
		
			
		hashResult.put("CURRENCY_CODE",currency_cd)
		hashResult.put("TRANSACTION_TYPE",transaction_type)
		hashResult.put("EVENT_NO",event_no)
		hashResult.put("AMOUNT",amount)
		hashResult.put("UPDATE_DATE",update_date)
		hashResult.put("FULLY_APPLIED_DATE",fullyapplied_date)
		return hashResult.sort()
	}
	
	//method for db and API "-4" transaction type
	def md_transaction_details_from_DB_for_parent_3(String testcaseid) {
		md_transaction_details_from_DB_for_parent(testcaseid, "-4","create_acct_complete")
	}
	def md_transaction_details_from_API_for_parent_3(String testcaseid) {
		md_transaction_details_from_API_for_parent(testcaseid, "1","get_transaction_information")
	}
	//method for db and API "-4" transaction type in create_acct_hierarchy
	def md_transaction_details_from_DB_for_parenthie(String testcaseid) {
		logi.logInfo("came inside")
		md_transaction_details_from_DB_for_parenthie(testcaseid, "-4","create_acct_hierarchy","1")
	}
	def md_transaction_details_from_API_for_parenthie(String testcaseid) {
		md_transaction_details_from_API_for_parent(testcaseid, "1","get_transaction_information")
	}
	//method for db and API "-1" transaction type
	def md_transaction_details_from_DB_for_parent_4(String testcaseid) {
		md_transaction_details_from_DB_for_parent(testcaseid, "-1","create_acct_complete.a")
	}
	def md_transaction_details_from_API_for_parent_4(String testcaseid) {
		md_transaction_details_from_API_for_parent(testcaseid, "1","get_transaction_information.a")
	}
	//method for db and API "-1" transaction type in create_acct_hierarchy
	def md_transaction_details_from_DB_for_parenthie_1(String testcaseid) {
		logi.logInfo("came inside")
		md_transaction_details_from_DB_for_parenthie(testcaseid, "-1","create_acct_hierarchy","3")
	}
	def md_transaction_details_from_API_for_parenthie_1(String testcaseid) {
		md_transaction_details_from_API_for_parent(testcaseid, "1","get_transaction_information")
	}

	//method for db and API "-5" transaction type
	def md_transaction_details_from_DB_for_parent_5(String testcaseid) {
		md_transaction_details_from_DB_for_parent(testcaseid, "-5","create_acct_complete.a")
	}
	def md_transaction_details_from_API_for_parent_5(String testcaseid) {
		md_transaction_details_from_API_for_parent(testcaseid, "1","get_transaction_information.b")
	}
	def md_transaction_details_from_DB_for_parenthie_2(String testcaseid) {
		logi.logInfo("came inside")
		md_transaction_details_from_DB_for_parenthie(testcaseid, "-5","create_acct_hierarchy","3")
	}
	def md_transaction_details_from_API_for_parenthie_2(String testcaseid) {
		md_transaction_details_from_API_for_parent(testcaseid, "1","get_transaction_information")
	}
	
	def md_transaction_details_from_DB_for_parenthie(String testcaseid,String transtype,String API,String accseq) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
int seq=accseq.toInteger()
		ConnectDB db=new ConnectDB()
		String acct_no;
		if(seq==1)
		{
			acct_no = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		else
			acct_no = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO3)
logi.logInfo("xxxxxxxxxxxxxxxx"+acct_no)
String q = "SELECT * FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" and transaction_type="+transtype+""
logi.logInfo("xxxxxxx  "+q)

ResultSet rs=db.executePlaQuery(q)

while(rs.next())
{
hashResult.put("CURRENCY_CODE", rs.getString('CURRENCY_CD'))
hashResult.put("TRANSACTION_TYPE", rs.getString('TRANSACTION_TYPE'))
hashResult.put("EVENT_NO", rs.getString('EVENT_NO'))
hashResult.put("AMOUNT", rs.getString('AMOUNT'))

}
String update=db.executeQueryP2("SELECT TO_CHAR(UPDATE_DATE,'yyyy-MM-dd hh:mm:ss') FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" and transaction_type="+transtype+"")
String fully_applied=db.executeQueryP2("SELECT TO_CHAR(FULLY_APPLIED_DATE,'yyyy-MM-dd hh:mm:ss') FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" and transaction_type="+transtype+"")
hashResult.put("UPDATE_DATE",update)
hashResult.put("FULLY_APPLIED_DATE",fully_applied)

logi.logInfo("Hashmap is : " + hashResult)
return hashResult.sort()


}
	public def md_IsPrevInvVoidedInPlanChange2hie(String tcid)
	{
		return md_IsPrevInvVoidedInPlanChangehie(tcid,"2")
	}
	
	public def md_IsPrevInvVoidedInPlanChange1hie(String tcid)
	{
		return md_IsPrevInvVoidedInPlanChangehie(tcid,"1")
	}
	
	//DEV-3032
	def md_usagedate_verification_DB(String testcaseid)
	{
		logi.logInfo("xxxxxxx")
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		ConnectDB database = new ConnectDB();
		String billlag_day="SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='MONTHLY_BILL_LAG_DAYS' AND CLIENT_NO="+clientNo
		
		
		String lagDays = database.executeQueryP2(billlag_day).toString()
		String usage_from_date=database.executeQueryP2("SELECT to_char(LAST_USAGE_BILL_DATE,'YYYY-MM-DD') from ariacore.acct where acct_no="+accountNo)
		hashResult.put("USAGE_FROM_DATE",usage_from_date)
		
		String usage_end_date=database.executeQueryP2("SELECT to_char(END_DATE,'YYYY-MM-DD') from ariacore.acct_plan_contracts where client_no= "+clientNo+ " and acct_no= "+accountNo).toString()
		if(usage_end_date.equalsIgnoreCase("null"))
		{
			logi.logInfo("multicheck")
			usage_end_date=database.executeQueryP2("SELECT to_char(END_DATE,'YYYY-MM-DD') from ariacore.acct_multi_plan_contracts where client_no= "+clientNo+ " and acct_no= "+accountNo).toString()
		}
		if(usage_end_date.equalsIgnoreCase("null"))
		{
			logi.logInfo("universalcheck")
			usage_end_date=database.executeQueryP2("SELECT to_char(END_DATE,'YYYY-MM-DD') from ariacore.acct_universal_contracts where client_no= "+clientNo+ " and acct_no= "+accountNo).toString()
		}
		
		
		
		hashResult.put("USAGE_TO_DATE",usage_end_date)
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	def md_usagedate_verification_api(String testcaseid)
	{
		logi.logInfo("aaaaaaaa")
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		ConnectDB database = new ConnectDB();
		String q="SELECT to_char(USAGE_FROM_DATE,'YYYY-MM-DD')as USAGE_FROM_DATE,to_char(USAGE_TO_DATE,'YYYY-MM-DD') as USAGE_TO_DATE from ARIACORE.GL where invoice_no in (SELECT max(invoice_no) from ARIACORE.GL where client_no= "+clientNo+ " and acct_no= "+accountNo+")"
		ResultSet rs=database.executePlaQuery(q)
		logi.logInfo("aaaaaaaa11212121212")
		while(rs.next())
		{
			hashResult.put("USAGE_FROM_DATE",rs.getString('USAGE_FROM_DATE').toString())
			hashResult.put("USAGE_TO_DATE",rs.getString('USAGE_TO_DATE').toString())
		}
		return hashResult.sort()
		}
}

