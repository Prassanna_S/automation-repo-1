package library
import java.util.ArrayList;

import com.eviware.soapui.impl.wadl.inference.schema.Context;
import com.eviware.soapui.impl.wsdl.teststeps.TestRequest;
import com.eviware.soapui.model.testsuite.TestRunner.Status;
import com.eviware.soapui.model.testsuite.AssertionError;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStepResult;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequest;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStep;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestRunContext
import org.apache.log4j.*;
import org.jfree.util.Log;
import library.Constants.Constant;

import groovy.time.*
import java.util.Calendar;

/**
 * Log Result Class having the methods to log the results to the file
 */
class LogResultObjQuery {
	// Variable declaration
	WsdlTestRequestStepResult testRequestStepResult
	WsdlTestRequest testRequest
	WsdlTestRequestStep objWsdlTestRequest
	static boolean pathFlag
	String testResultLogPath
	String responseLogPath
	
	static Logger rootLogger 
	FileAppender fileappender
	def timeStart, timeStop 
	long calendarIns
	FileOperation objTestResultFile, objResponseLogFile
	
	/**
	 * Constructor for the class
	 */
	LogResultObjQuery(String logFilePath) {
		String tempPath = ''
		String[] logFileArr = logFilePath.split('\\\\')
		for(int i=0; i<logFileArr.length-1; i++) {
			tempPath = tempPath + logFileArr[i] + "\\"
		}
		
		// Create the log directory under soap ui installed directory
		File responseFile = new File(tempPath)		
		if (responseFile.exists()== false) {
			responseFile.mkdir()
		}
		
		testResultLogPath = tempPath + "TestResultLog_" + Constant.PROJECTNAME + "_" + logFileArr[logFileArr.length-1] + '.txt'	
		responseLogPath = tempPath + "RequestResponseLog_" + Constant.PROJECTNAME + "_" + logFileArr[logFileArr.length-1] + '.txt'	
		
		objTestResultFile = new FileOperation(testResultLogPath)
		objResponseLogFile = new FileOperation(responseLogPath)
	}	
	
	
	/**
	 * Log the start of the script execution
	 * @param testCaseName name of the test case
	 * @author vasanth.manickam	
	 */
	void logScriptStart(String testCaseName) {
		calendarIns = Calendar.getInstance().getTimeInMillis();
		logInfoWithNoFormat 'Started the test case execution for the object query api : ' + testCaseName + "'" + System.getProperty("line.separator")
		logReqResInfo "Request(s) and Response(s) for the test scenario - '" + testCaseName + "'" + System.getProperty("line.separator")
	}
	
	/**
	* Log the start of the script execution
	* @param testCaseName name of the test case
	* @author vasanth.manickam
	*/
   void logTestStepStart(String testStepName) {
	   logInfoWithNoFormat 'Script : (' + Constant.QUERYCOUNT + ') - ' + testStepName + System.getProperty("line.separator")
	   objResponseLogFile.appendFileWithNewLine 'Script : (' + Constant.QUERYCOUNT + ') - ' + testStepName + System.getProperty("line.separator")
	   // Increment the counter
	   Constant.QUERYCOUNT++
   }
	/**
	 * Log the end of the script execution
	 * @param testCaseName name of the test case
	 * @author vasanth.manickam	
	 */
	void logScriptCompletion(String testCaseName) {		
		long calendar2 = Calendar.getInstance().getTimeInMillis();
		int time = calendar2 - calendarIns;		

		logInfo "Completed the test case execution - '" + testCaseName + "' in " + time.toString() + " milliseconds "
		logReqResInfo "Test execution completed - '" + testCaseName + System.getProperty("line.separator") 
		
		if(Constant.FAILFLAG) {
			logInfoWithNoFormat(System.getProperty("line.separator") + '--- >> Test Result : (FAILED)')
		} else {
			logInfoWithNoFormat(System.getProperty("line.separator") + '--- >> Test Result : (PASSED)')
		}
		
		logInfoWithNoFormat System.getProperty("line.separator") + "************************************************************* " + System.getProperty("line.separator")
		objResponseLogFile.appendFileWithNewLine System.getProperty("line.separator") + "*************************************************************" + System.getProperty("line.separator")
		
		if(Constant.FAILFLAG){
			// Reset the flag
			Constant.FAILFLAG = false
			assert !true
		}	
	}
	

//	void logSample(String testCaseName) {
//		long calendar2 = Calendar.getInstance().getTimeInMillis();
//		int time = calendar2 - calendarIns;
//
//		logInfo "Completed the test case execution - '" + testCaseName + "' in " + time.toString() + " milliseconds "
//		logReqResInfo "Test execution completed - '" + testCaseName + System.getProperty("line.separator")
//		
//		if(Constant.FAILFLAG) {
//			logInfoWithNoFormat(System.getProperty("line.separator") + '--- >> Test Result : (FAILED)')
//		} else {
//			logInfoWithNoFormat(System.getProperty("line.separator") + '--- >> Test Result : (PASSED)')
//		}
//		
//		logInfoWithNoFormat System.getProperty("line.separator") + "************************************************************* " + System.getProperty("line.separator")
//		objResponseLogFile.appendFileWithNewLine System.getProperty("line.separator") + "*************************************************************" + System.getProperty("line.separator")
//		
//		if(Constant.FAILFLAG){
//			// Reset the flag
//			Constant.FAILFLAG = false
//			assert !true
//		}
//	}
	
	/**
	 * Log the test environment details
	 */
	void logTestEnvironment() {
		// Get the host name and the address
		InetAddress addr = InetAddress.getLocalHost();
		String hostName = addr.getHostName();
		String hostAddress = addr.getHostAddress();
		
		// Get the environment(QA Future/Stage Future...)
		String env = RegExp.getStringMatch(Constant.WSDLDEFINITION, "\\.[a-z]*\\.[a-z]*", 0)
		String[] envArr = env.toUpperCase().split('\\.')
		
		// Log the details in test result log
		logInfoWithNoFormat System.getProperty("line.separator")
		logInfoWithNoFormat 'Test Environment'
		logInfoWithNoFormat '----------------'
		logInfoWithNoFormat 'Environment	        : ' + envArr[2] + ' ' + envArr[1]
		logInfoWithNoFormat 'WSDL(Core API)	        : ' + Constant.WSDLDEFINITION
		logInfoWithNoFormat 'WSDL(Object Query API)	: ' + Constant.WSDLDEFINITION_OBJQUERY
		logInfoWithNoFormat 'Test Machine		: ' + hostAddress + '(' + hostName + ')'
		
		// Log the details in the req/res file
		logInfoWithNoFormat System.getProperty("line.separator")
		objResponseLogFile.appendFileWithNewLine 'Test Environment'
		objResponseLogFile.appendFileWithNewLine '----------------'
		objResponseLogFile.appendFileWithNewLine 'Environment		: ' + envArr[1] + ' ' + envArr[2]
		objResponseLogFile.appendFileWithNewLine 'WSDL(Core API)		: ' + Constant.WSDLDEFINITION
		objResponseLogFile.appendFileWithNewLine 'WSDL(Object Query API)	: ' + Constant.WSDLDEFINITION_OBJQUERY
		objResponseLogFile.appendFileWithNewLine 'Test Machine		: ' + hostAddress + '(' + hostName + ')'
		objResponseLogFile.appendFileWithNewLine System.getProperty("line.separator")
	}
	/**
	* Log the info into file using logger
	* @param message info(String) that has to be logged
	* @author vasanth.manickam
	*/
	void logInfo(String message) {
		rootLogger = Logger.getRootLogger();
		if(message == '\n'){
			fileappender = new FileAppender(new PatternLayout(), testResultLogPath);
		}else {
			fileappender = new FileAppender(new PatternLayout("%d{dd MMM yyyy HH:mm:ss,SSS} %5p %m%n"), testResultLogPath);
		}
		
		rootLogger.setLevel(Level.INFO)
		rootLogger.addAppender(fileappender);
	   
		rootLogger.info message
	   
		rootLogger.removeAppender fileappender
		fileappender.close()
  	}
   
	/**
	 * Log the message(with out log4j format)
	 * @param message
	 */
	void logInfoWithNoFormat(String message) {
		objTestResultFile.appendFileWithNewLine message
	}
	
	/**
	 * Log a new line(with out log4j format)
	 * @author vasanth.manickam
	 */
	void logInfoNewLineNoFormat() {
		objTestResultFile.appendFileWithNewLine System.getProperty("line.separator")
	}
	/**
	* Log the error into file using logger
	* @param message info(String) that has to be logged
	* @author indira.badrinath
	*/
	void logError(String message) {
		rootLogger = Logger.getRootLogger();
		fileappender = new FileAppender(new PatternLayout("%d{dd MMM yyyy HH:mm:ss,SSS} %5p %m%n"), testResultLogPath);
		rootLogger.setLevel(Level.ERROR)
		rootLogger.addAppender(fileappender);
		rootLogger.error message
		rootLogger.removeAppender fileappender
		fileappender.close()
	  }
	
	boolean scriptAssert(com.eviware.soapui.impl.wsdl.testcase.WsdlTestRunContext context, com.eviware.soapui.support.XmlHolder holder, LinkedHashMap m) {
		boolean assertFlag = true
		def expValue
		def nodeValue
		
		List key = m.keySet().asList()
		List value = m.values().asList()
		for(int i=0; i<key.size(); i++) {
			nodeValue = holder.getNodeValue( key.get(i) )
			if(value.get(i).toString().contains('$')){
				expValue = context.expand(value.get(i))
			} else {
				expValue = value.get(i)
			}
			def logAssertFlag = logAssert(key.get(i) , nodeValue, expValue)
			assertFlag = assertFlag && logAssertFlag
		}
		return assertFlag
	}

	/**
	* Log the result of the test request
	* @param testRequest name of the test request that was executed
	* @param result test request run result object of type WsdlTestRequestStepResult
	* @author vasanth.manickam
	*/
	void logTestRequestResult(testRequest, com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStepResult result) {
		this.testRequestStepResult = result
	 
		if(testRequestStepResult.status.toString() == "FAILED"){
		 
			logInfo "Executed the test request - " + testRequest + " in " + testRequestStepResult.getTimeTaken().toString() + "\t" + ". Test Run Status : " + testRequestStepResult.status.toString() /*+ " Reason : " + testRequestStepResult.getError().toString()*/ + System.getProperty("line.separator");
		 
			if(!(testRequestStepResult.hasResponse())) {
				logInfo "No Reponse been fetched"
			}
			 //TODO Parse the response and get the error code and error message
		 
		} else {
			 logInfo "Executed the test request - " + testRequest + " in " + testRequestStepResult.getTimeTaken().toString() + ". Test Run Status : " + testRequestStepResult.status.toString() + System.getProperty("line.separator");
		}
	}
	
   /**
   * Log the result of the test request
   * @param testRequest name of the test request that was executed
   * @param result test request run result object of type WsdlTestRequestStepResult
   * @author vasanth.manickam
   */
   void logTestRequestResult(testRequest, com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStepResult result, queryString) {	
	   this.testRequestStepResult = result		
	
	   if(testRequestStepResult.status.toString() == "FAILED"){
		
		   logInfo "Executed the test request - " + testRequest + " with the query - '" + queryString + "' in " + testRequestStepResult.getTimeTaken().toString() + "\t" + ". Test Run Status : " + testRequestStepResult.status.toString() + System.getProperty("line.separator");
		
		   if(!(testRequestStepResult.hasResponse())) {
			   logInfo "No Reponse been fetched"
		   }					
	   } else {
			logInfo "Executed the test request - " + testRequest + " with the query - '" + queryString + "' in " + testRequestStepResult.getTimeTaken().toString() + ". Test Run Status : " + testRequestStepResult.status.toString() + System.getProperty("line.separator");
	   }	
   }  
   
   void logRequest (testStepName, request) {
	   logReqResInfo("Soap Request for the API call - '" + testStepName + "'" + System.getProperty("line.separator"))
	   logReqResInfo(request + System.getProperty("line.separator") + System.getProperty("line.separator"))
   }
   
   /**
   * Log the API call Response
   * @param testStepName name of the test step(api)
   * @author indira.badrinath
   */
   void logResponse (testStepName, response) {
	   logReqResInfo("Soap Response for the API call - '" + testStepName + "'" + System.getProperty("line.separator"))
	   logReqResInfo(response + System.getProperty("line.separator"))
   }
   
   /**
    * Log the info in the response log file
    * @param message information to be logged
    * @author vasanth.manickam
    */
   void logReqResInfo (String message) {
	   def responseFile = new File(responseLogPath)
	   
	   if (responseFile.exists()== false) {
		   responseFile.createNewFile()
	   }
	   responseFile.append(message + System.getProperty("line.separator"));
   }
   
   boolean logAssert(nodeName, actual, String expected) {
	   boolean matchFlag = false
	   
	   // Check match for multiple expected data
	   if(expected.contains(";")) {
		   String[] expArr = expected.split(";")
		   int i = 0;
		    while(i<expArr.length) {
			   if(expArr[i].startsWith("~")) {
				   matchFlag = matchFlag || RegExp.exactMatch(actual, expArr[i].substring(1, expArr[i].length()))
			   } else if(expArr[i].equals('null')){
			   	   matchFlag = matchFlag || actual.equals (null)
			   } else {
			   	   matchFlag = matchFlag || actual.equals(expArr[i])
			   }
			   i++
		   }
	   } else {
		   if(expected.startsWith("~")) {
			   matchFlag = RegExp.exactMatch(actual, expected.substring(1, expected.length()))
		   } else if(expected.equals('null')){
		   	   matchFlag = actual.equals (null)
		   } else {
		   	   matchFlag = (actual.equals(expected))
		   }
	   }
	   //TODO Modify the log if we expect the data to be either of option separated with ';'
	   if(matchFlag) {
		   logInfo("PASS - Verified: '" + nodeName + "' -RESULT: '" + actual + "'" )
	   } else {
	   	   if(expected.startsWith("~")) {
				logInfo("(FAIL) - Verified: '" + nodeName + "' -RESULT: '" + expected + "' - ERROR: '" + actual + "'");  
		   } else {
		   		logInfo("(FAIL) - Verified: '" + nodeName + "' -RESULT: '" + expected + "' - ERROR: '" + actual + "'");
				Constant.FAILFLAG = true
		   }	   	   
	   }	   
	   return matchFlag
   }
}  
