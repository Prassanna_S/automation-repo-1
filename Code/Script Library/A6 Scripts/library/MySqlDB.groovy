package library


import library.Constants.Constant;

import com.eviware.soapui.impl.wsdl.submit.transports.http.support.methods.ExtendedGetMethod;

import System;

import java.util.Date.*
import java.text.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.CallableStatement;

class MySqlDB 
{
	public static Connection connection=null
	public static Statement statement=null
		
	String connSatus = 'Initialized'
		LogResult logi = new LogResult(Constant.logFilePath)
		
		public MySqlDB() throws SQLException {							
		logi.logInfo("-------- MySQL DB Connection Testing ------");

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			logi.logInfo("Where is your MYSQL JDBC Driver?");
			e.printStackTrace();
			return;
		}
		logi.logInfo("MySQL JDBC Driver Registered!");
		
		try {
			connection = DriverManager.getConnection("jdbc:mysql://172.16.30.201:3306/arcqu", "amuthu", "amuthu274");
		} catch (SQLException e) {
			logi.logInfo("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
		if (connection != null) {
			logi.logInfo("You made it, take control your database now!");
		} else {
			logi.logInfo("Failed to make connection!");
		}
	}
	
	public ResultSet executeQuery(String query) throws SQLException
	{
		logi.logInfo "::: Executing Query :::" + query
		if (statement == null)
		 statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
		String QueryString = query;
		ResultSet resultSet = statement.executeQuery(QueryString);
		return resultSet;
	}
	
	public ResultSet executePlaQuery(String query) throws SQLException
	{
		String QueryString = query;
		int eventTimeout = 0
		ResultSet resultSet		
		//logi.logInfo statement.toString()
		//statement.close()
		
		boolean connectionStatementClosed
		connectionStatementClosed=false
		//logi.logInfo statement.toString()

		
		//logi.logInfo connectionStatementClosed.toString()
			if (statement == null || connectionStatementClosed)
			statement = connection.createStatement();
		
		 
		logi.logInfo "::: Executing Query :::"
		//logi.logInfo statement.closed.toString()
		logi.logInfo query
		if(QueryString.contains("NoVal")) {
			QueryString = "SELECT ARC_TYPE FROM ARIACORE.ARC_TYPES WHERE ARC_TYPE='NULL'"
			resultSet = statement.executeQuery(QueryString);
		} else if(QueryString.contains("341")) {
			resultSet = statement.executeQuery(QueryString);
			while(!resultSet.isBeforeFirst() && eventTimeout < 60) {
				Thread.sleep(1000)
				resultSet = statement.executeQuery(QueryString);
				eventTimeout++
			}
		} else {
			resultSet = statement.executeQuery(QueryString);
			logi.logInfo "Query Executed!"
		}
		
		return resultSet;
	}
	
	public def executeQueryP2(String query) throws SQLException {
		HashMap tempValues
		ResultSet resultSet
		
		boolean connectionStatementClosed
		connectionStatementClosed=false
		
			if (statement == null || connectionStatementClosed)
			statement = connection.createStatement();
		
		logi.logInfo "::: Executing Query :::"
		logi.logInfo query
		resultSet = statement.executeQuery(query);
		
		ResultSetMetaData metaData = resultSet.getMetaData();
		int columns = metaData.getColumnCount();
		//logi.logInfo("Column count is  : "+columns)
		
		if(resultSet == null){
			String emptyValue = 0;
			return emptyValue
		}
		while (resultSet !=null && resultSet.next()) {
			   if(columns>1){
					// logi.logInfo("Return type should be hash")
					 tempValues = new HashMap<String,String>()
					 for (int col = 1; col <= columns; col++) {
							String columnValue = resultSet.getString(col);
							tempValues.put(metaData.getColumnName(col),columnValue)
					 }
					// logi.logInfo("The result set of hash is : "+tempValues)
					 return tempValues
			   } else{
					 //logi.logInfo("Return type should be String")
					 return resultSet.getString(1)
			   }
		}
 }

	public String[] executeQueryReturnValuesAsArray(String query) throws SQLException
	{
		def valueArray = []
		int i = 0;
		if (statement == null)
		 statement = connection.createStatement();
		 logi.logInfo "::: Executing Query :::"
		 logi.logInfo query
		ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			valueArray[i] = resultSet.getString(1);
			i++;
		}

		resultSet.close()
		return valueArray;
	}
	public void closeConnection() throws SQLException
	{		
		statement.close()
		connection.close();
		logi.logInfo "DB CONNECTION CLOSED"
	}
	
}
