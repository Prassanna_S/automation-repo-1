package library
import java.text.DecimalFormat
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import net.iharder.*
import java.lang.Object;

import com.eviware.soapui.impl.wadl.inference.schema.Context;
import com.eviware.soapui.impl.wsdl.teststeps.TestRequest;
import com.eviware.soapui.model.testsuite.TestRunner.Status;
import com.eviware.soapui.model.testsuite.AssertionError;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStepResult;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequest;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStep;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestRunContext;
import com.eviware.soapui.model.testsuite.TestStepResult;
import com.eviware.soapui.support.XmlHolder

import org.apache.log4j.*;
import org.apache.poi.hssf.usermodel.HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.poifs.filesystem.POIFSFileSystem
import org.jfree.util.Log;
import library.Constants.Constant;
import library.Constants.ExpValueConstants;
import library.Constants.ExPathRpc;
import library.Constants.ExPathRpcEnc;
import library.VerificationMethods;
import library.UsageVerificationMethods;
import library.AdminToolsVerificationMethods;

import groovy.time.*
import hermes.taglib.GetXMLTag;

import java.util.Calendar;
/*KO*/
import java.sql.ResultSet;
import library.Constants.Constant.APIName;
import java.io.BufferedWriter;
import java.io.Console;
import java.io.File;
import java.io.BufferedInputStream;
import java.io.FileWriter;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import library.Constants.Constant.RESULT_TYPE;
import library.GenericLibrary.Common;
import library.UI.ManageAuthKey
import library.UI.StatementTemplates

/**
 * Log Result Class having the methods to log the results to the file
 */
class LogResult {

	// Variable declaration
	WsdlTestRequestStepResult testRequestStepResult
	TestStepResult testHTTPRequestStepResult
	WsdlTestRequest testRequest
	com.eviware.soapui.impl.support.http.HttpRequest HTTPtestRequest
	WsdlTestRequestStep objWsdlTestRequest
	static boolean pathFlag
	String testResultLogPath
	String responseLogPath

	static Logger rootLogger
	FileAppender fileappender
	def timeStart, timeStop
	long calendarIns
	FileOperation objTestResultFile, objResponseLogFile

	
	//KO
	LinkedHashMap<String, String> lhmAllOutputResponse = new LinkedHashMap<String, String>()
	LinkedHashMap<String, String> lhmAllInputResponse = new LinkedHashMap<String, String>()
	LinkedHashMap<String,LinkedHashMap<String, com.eviware.soapui.support.XmlHolder>> lhmAllApiResponsesAll =new LinkedHashMap<String,LinkedHashMap<String, com.eviware.soapui.support.XmlHolder>> ()

	/*Logging Results  New Template*/
	public FileWriter fWriter = null, fWriterOverall = null;
	public FileReader freader = null;
	public BufferedWriter writer = null, writerOverall = null;
	public BufferedWriter failwriter = null
	public BufferedReader reader = null;
	public String sFilePath = null;
	public String ResultFile = null, ResultFileOverall = null;
	
	
	//Testlink - XML results import
	public FileWriter xmlfWriter = null		
	public BufferedWriter xmlwriter = null 
	public String xmlResultFile = null

	public int intStep = 1, intStepOverall = 1;
	public int[] intStepNo = new int[5000];

	static boolean initStatus //added for p2
	public LinkedHashMap<String, Boolean> lhmTCStatusP2 = new LinkedHashMap<String, Boolean>() //added for p2
	public ArrayList<ArrayList<String>> strTotalReport = new ArrayList<ArrayList<String>>();

	public LinkedHashMap<String, String> lhmTotalReport = new LinkedHashMap<String, String>()
	public String keyResult = "", valueResult = "";

	public String failSeprate = ""
	public String reportfileHeader = ""
	public LinkedHashMap<String, String> lhmFailureReport = new LinkedHashMap<String, String>()

	public LinkedHashMap<String, String> lhmTCHeaderDetails = new LinkedHashMap<String, String>()
	public LinkedHashMap<String, Boolean> lhmTCStatus = new LinkedHashMap<String, Boolean>()
	public int PassCount =0, FailCount = 0;

	/******************* Variable Declaration ************************
	 ******************************************************************/

	public LinkedHashMap<String, String> lhmTCDesc = new LinkedHashMap<String, String>()
	public LinkedHashMap<String, String> lhmTCGenericException = new LinkedHashMap<String, String>()

	public FileWriter fWriterCSV = null;
	public BufferedWriter writerCSV = null;
	public String ResultFileCSV = null;


	/**
	 * Constructor for the class
	 */

	LogResult(String logFilePath) {
		try {
			String tempPath = ''
			String[] logFileArr = logFilePath.split('\\\\')
			for(int i=0; i<logFileArr.length; i++) {
				tempPath = tempPath + logFileArr[i] + "\\"
			}

			// Create the log directory under soap ui installed directory
			File responseFile = new File(tempPath)
			if (responseFile.exists()== false) {
				responseFile.mkdir()
			}

			testResultLogPath = tempPath + "TestResultLog_" + Constant.PROJECTNAME + "_" + logFileArr[logFileArr.length-1] + '.txt'
			responseLogPath = tempPath + "RequestResponseLog_" + Constant.PROJECTNAME + "_" + logFileArr[logFileArr.length-1] + '.txt'

			objTestResultFile = new FileOperation(testResultLogPath)
			objResponseLogFile = new FileOperation(responseLogPath)
		}
		catch (Exception e)
		{
			System.out.println(e)
		}
	}

	LogResult() {}

	/**
	 * Log the start of the script execution
	 * @param testCaseName name of the test case
	 * @author vasanth.manickam
	 */

	void logScriptStart(String testCaseName) {
		System.out.println("Test");
		calendarIns = Calendar.getInstance().getTimeInMillis();
		logInfo "::: Test Case Execution Begins: " + testCaseName + " :::"
		objResponseLogFile.appendFileWithNewLine 'Script : (' + Constant.SCRIPTCOUNT + ') - ' + testCaseName + System.getProperty("line.separator")
		// Increment the counter
		//Constant.SCRIPTCOUNT++
		//logInfo 'Started the test case execution'
		logReqResInfo "Request(s) and Response(s) for the test scenario - '" + testCaseName + "'" + System.getProperty("line.separator")
	}


	void logSimpleStart(String logfilepath) {
		calendarIns = Calendar.getInstance().getTimeInMillis();
		logInfoWithNoFormat 'Script the test log' + testResultLogPath + ' : ' + logfilepath
	}

	/**
	 * Log the end of the script execution
	 * @param testCaseName name of the test case
	 * @author vasanth.manickam
	 */
	void logScriptCompletion(String testCaseName) {
		long calendar2 = Calendar.getInstance().getTimeInMillis();
		int time = calendar2 - calendarIns;

		logInfo "Completed the test case execution - '" + testCaseName + "' in " + time.toString() + " milliseconds "
		logReqResInfo "Test execution completed - '" + testCaseName + System.getProperty("line.separator")

		if(Constant.FAILFLAG) {
			logInfoWithNoFormat(System.getProperty("line.separator") + '--- >> Test Result : (FAILED)')
		} else {
			logInfoWithNoFormat(System.getProperty("line.separator") + '--- >> Test Result : (PASSED)')
		}

		logInfoWithNoFormat System.getProperty("line.separator") + "************************************************************* " + System.getProperty("line.separator")
		objResponseLogFile.appendFileWithNewLine System.getProperty("line.separator") + "*************************************************************" + System.getProperty("line.separator")

		// Reset the flag
		Constant.FAILFLAG = false
	}
	/**
	 * Log the test environment details
	 */
	void logTestEnvironment() {
		// Get the host name and the address
		InetAddress addr = InetAddress.getLocalHost();
		String hostName = addr.getHostName();
		String hostAddress = addr.getHostAddress();

		// Get the environment(QA Future/Stage Future...)
		String env = RegExp.getStringMatch(Constant.WSDLDEFINITION, "\\.[a-z]*\\.[a-z]*", 0)
		String[] envArr = env.toUpperCase().split('\\.')

		// Log the details in test result log
		logInfoWithNoFormat 'Test Environment'
		logInfoWithNoFormat '----------------'
		logInfoWithNoFormat 'Environment	: ' + envArr[2] + ' ' + envArr[1]
		logInfoWithNoFormat 'WSDL		: ' + Constant.WSDLDEFINITION
		logInfoWithNoFormat 'Test Machine	: ' + hostAddress + '(' + hostName + ')'
		logInfoWithNoFormat "*************************************************************"
		logInfoWithNoFormat System.getProperty("line.separator")

		// Log the details in the req/res file
		objResponseLogFile.appendFileWithNewLine 'Test Environment'
		objResponseLogFile.appendFileWithNewLine '----------------'
		objResponseLogFile.appendFileWithNewLine 'Environment	: ' + envArr[1] + ' ' + envArr[2]
		objResponseLogFile.appendFileWithNewLine 'WSDL		: ' + Constant.WSDLDEFINITION
		objResponseLogFile.appendFileWithNewLine 'Test Machine	: ' + hostAddress + '(' + hostName + ')'
		objResponseLogFile.appendFileWithNewLine "*************************************************************"
		objResponseLogFile.appendFileWithNewLine System.getProperty("line.separator")
	}
	/**
	 * Log the info into file using logger
	 * @param message info(String) that has to be logged
	 * @author vasanth.manickam
	 */

	void logInfo(String message) {
		rootLogger = Logger.getRootLogger();
		if(message == '\n'){
			fileappender = new FileAppender(new PatternLayout(), testResultLogPath);
		}else {
			fileappender = new FileAppender(new PatternLayout("%d{dd MMM yyyy HH:mm:ss,SSS} %5p %m%n"), testResultLogPath);
		}

		rootLogger.setLevel(Level.INFO)
		rootLogger.addAppender(fileappender);

		rootLogger.info message

		rootLogger.removeAppender fileappender
		fileappender.close()
	}
	/**
	 * Log the message(with out log4j format)
	 * @param message
	 */
	void logInfoWithNoFormat(String message) {
		objTestResultFile.appendFileWithNewLine message
	}

	/**
	 * Log a new line(with out log4j format)
	 * @author vasanth.manickam
	 */
	void logInfoNewLineNoFormat() {
		objTestResultFile.appendFileWithNewLine System.getProperty("line.separator")
	}
	/**
	 * Log the error into file using logger
	 * @param message info(String) that has to be logged
	 * @author indira.badrinath
	 */
	void logError(String message) {
		rootLogger = Logger.getRootLogger();
		fileappender = new FileAppender(new PatternLayout("%d{dd MMM yyyy HH:mm:ss,SSS} %5p %m%n"), testResultLogPath);
		rootLogger.setLevel(Level.ERROR)
		rootLogger.addAppender(fileappender);
		rootLogger.error message
		rootLogger.removeAppender fileappender
		fileappender.close()
	}
	boolean scriptAssert(com.eviware.soapui.impl.wsdl.testcase.WsdlTestRunContext context, /*com.eviware.soapui.support.XmlHolder holder,*/ LinkedHashMap m, String testCaseId) {

		boolean assertFlag = true
		def expValue, expected, actual
		def nodeValue
		def tmp
		/*KO*/
		String strQuery
		RESULT_TYPE resultType
		ConnectDB db = new ConnectDB()
		/*KO*/
		List key = m.keySet().asList()
		List value = m.values().asList()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData rd = new ReadData()
		rd.fetchTestData(testId)
		for(int i=0; i<key.size(); i++) {
			List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
			List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()

			// To get the actual value
			// if the key is a DB query
			if(key.get(i).toString().contains('SELECT')) {
				strQuery = db.buildQuery(key.get(i), rd.TD_clientNo, getNodeVal(testCaseId, key.get(i)))

				ResultSet resultSet = db.executePlaQuery(strQuery)
				if (resultSet.next()) {
					nodeValue = resultSet.getString(1)
				}
				else {
					nodeValue = 'No Actual Value returned from DB'
				}

				actual = strQuery

			} else if(key.get(i).toString().contains('md')) {

				VerificationMethods verifyMethods = new VerificationMethods(db)
				Class[] parametersTypes = new Class[1]
				parametersTypes[0] = String.class;

				Object[] parameters = new Object[1];
				parameters[0] = testCaseId

				Method method = verifyMethods.class.getMethod(key.get(i).toString(), parametersTypes)
				nodeValue = method.invoke(verifyMethods, parameters)

				resultType = RESULT_TYPE.METHOD_COMPARISON
				actual = key.get(i)

			} else if(key.get(i).toString().contains('TD')) {
				nodeValue = key.get(i).toString().substring(key.get(i).toString().indexOf("_")+1)
				actual = key.get(i)
			} else {
				// if the key is xpath of the XML response
				for(int j=0; j<testCaseCmb.size(); j++) {
					if(testCaseCmb[j].equals(testCaseId)) {
						com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
						if(holder.containsKey(key.get(i)) && holder.containsValue(key.get(i))) {
							nodeValue = holder.getNodeValue( key.get(i) )
						} else {
							nodeValue = 'NO NODE'
						}
					}
				}
				actual = key.get(i)
			}

			// TO get the expected values
			if(value.get(i).toString().contains('$')){
				expValue = context.expand(value.get(i))
				resultType = RESULT_TYPE.TEST_DATA_COMPARISON
				expected = value.get(i)
				/*KO*/
			} else if(value.get(i).toString().contains('SELECT')) {

				strQuery = db.buildQuery(value.get(i), rd.TD_clientNo, getNodeVal(testCaseId, value.get(i)))

				ResultSet resultSet = db.executePlaQuery(strQuery)
				if (resultSet.next()) {
					expValue = resultSet.getString(1)
				}
				else {
					expValue = 'No Exp Value returned from DB'
				}
				resultType = RESULT_TYPE.DB_COMPARISON
				expected = strQuery

				/*KO*/
			} else if(value.get(i).toString().contains('md')) {

				VerificationMethods verifyMethods = new VerificationMethods(db)
				Class[] parametersTypes = new Class[1]
				parametersTypes[0] = String.class;

				Object[] parameters = new Object[1];
				parameters[0] = testCaseId

				Method method = verifyMethods.class.getMethod(value.get(i), parametersTypes)
				expValue = method.invoke(verifyMethods, parameters)

				resultType = RESULT_TYPE.METHOD_COMPARISON
				expected = value.get(i)
			} else {
				expValue = value.get(i)
				resultType = RESULT_TYPE.TEST_DATA
				expected = value.get(i)
			}

			def logAssertFlag = logAssert(key.get(i), nodeValue, expValue)
			assertFlag = assertFlag && logAssertFlag

			String[] tcID = testCaseId.split("-");
			if (logAssertFlag){
				logPassorFail(tcID[1], resultType, actual + " " + expected + " : Actual Value - " + nodeValue + " Expected Value - " + expValue , true)
				logExcelPassorFail(tcID[1], resultType , actual + " " + expected + " : Actual Value - " + nodeValue + " Expected Value - " + expValue , true,nodeValue,expValue)

			}
			else{
				logPassorFail(tcID[1], resultType, actual + " " + expected + " : Actual Value - " + nodeValue + " Expected Value - " + expValue , false)
				logExcelPassorFail(tcID[1], resultType , actual + " " + expected + " : Actual Value - " + nodeValue + " Expected Value - " + expValue , false,nodeValue,expValue)
			}
		}
		return assertFlag
	}

	// com.eviware.soapui.impl.wsdl.panels.support.MockTestRunContext context
	boolean scriptAssert(com.eviware.soapui.impl.wsdl.panels.support.MockTestRunContext context, /*com.eviware.soapui.support.XmlHolder holder,*/ LinkedHashMap m, String testCaseId) {

		boolean assertFlag = true
		def expValue, expected, actual
		def nodeValue
		def tmp
		/*KO*/
		String strQuery
		RESULT_TYPE resultType
		ConnectDB db = new ConnectDB()
		/*KO*/
		List key = m.keySet().asList()
		List value = m.values().asList()
		ReadData rd = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		rd.fetchTestData(testId)
		for(int i=0; i<key.size(); i++) {
			//rd.fetchTestData(testId)
			//logInfo "after fetch"
			List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
			List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()

			// To get the actual value
			// if the key is a DB query
			if(key.get(i).toString().contains('SELECT')) {
				logInfo("In SELECT loop : " + key.get(i) + " :: test case : " + testCaseId)
				
				String newQuery = null
				VerificationMethods vm =new VerificationMethods()
				String acct_no= vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
				logInfo "Ac No : " + acct_no
				if (key.get(i).toString().contains('<CREATE_ACCT_COMPLETE_ACCTNO1>')){
					newQuery=key.get(i).toString().replaceAll('<CREATE_ACCT_COMPLETE_ACCTNO1>',acct_no).toString()
				}
				logInfo newQuery
				if(newQuery != null){
					strQuery = db.buildQuery(newQuery, rd.TD_clientNo, getNodeVal(testCaseId, newQuery))
				}
				else
					strQuery = db.buildQuery(key.get(i), rd.TD_clientNo, getNodeVal(testCaseId, key.get(i)))
				
				//strQuery = db.buildQuery(key.get(i), rd.TD_clientNo, getNodeVal(testCaseId, key.get(i)))

				ResultSet resultSet = db.executePlaQuery(strQuery)
				if (resultSet.next()) {
					nodeValue = resultSet.getString(1)
				}
				else {
					nodeValue = 'No Actual Value returned from DB'
				}
				resultSet.close()
				actual = strQuery

			} else if(key.get(i).toString().contains('md')) {

				VerificationMethods verifyMethods = new VerificationMethods(db)
				Class[] parametersTypes = new Class[1]
				parametersTypes[0] = String.class;

				Object[] parameters = new Object[1];
				parameters[0] = testCaseId

				Method method = verifyMethods.class.getMethod(key.get(i).toString(), parametersTypes)
				nodeValue = method.invoke(verifyMethods, parameters)

				resultType = RESULT_TYPE.METHOD_COMPARISON
				actual = key.get(i)

			} else if(key.get(i).toString().contains('TD')) {
				nodeValue = key.get(i).toString().substring(key.get(i).toString().indexOf("_")+1)
				actual = key.get(i)
			} else {
				// if the key is xpath of the XML response
				for(int j=0; j<testCaseCmb.size(); j++) {
					if(testCaseCmb[j].equals(testCaseId)) {
						com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
						if(holder.containsKey(key.get(i)) && holder.containsValue(key.get(i))) {
							nodeValue = holder.getNodeValue( key.get(i) )
						} else {
							nodeValue = 'NO NODE'
						}
					}
				}
				actual = key.get(i)
			}

			// TO get the expected values
			if(value.get(i).toString().contains('$')){
				expValue = context.expand(value.get(i))
				resultType = RESULT_TYPE.TEST_DATA_COMPARISON
				expected = value.get(i)
				/*KO*/
			} else if(value.get(i).toString().contains('SELECT')) {
				logInfo("In SELECT loop : " + value.get(i) + " :: test case : " + testCaseId)
				String newQuery = null
				VerificationMethods vm =new VerificationMethods()
				String acct_no= vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
				logInfo "Ac No : " + acct_no
				if (value.get(i).toString().contains('<CREATE_ACCT_COMPLETE_ACCTNO1>')){
					newQuery=value.get(i).toString().replaceAll('<CREATE_ACCT_COMPLETE_ACCTNO1>',acct_no).toString()
				}
				logInfo newQuery
				if(newQuery != null){
					logInfo "in not null "
					strQuery = db.buildQuery(newQuery, rd.TD_clientNo, getNodeVal(testCaseId, newQuery))
				}
				else
					strQuery = db.buildQuery(value.get(i), rd.TD_clientNo, getNodeVal(testCaseId, value.get(i)))
				
				
				ResultSet resultSet = db.executePlaQuery(strQuery)
				if (resultSet.next()) {
					expValue = resultSet.getString(1)
				}
				else {
					expValue = 'No Exp Value returned from DB'
				}
				resultSet.close()
				resultType = RESULT_TYPE.DB_COMPARISON
				expected = strQuery

				/*KO*/
			} else if(value.get(i).toString().contains('md')) {

				VerificationMethods verifyMethods = new VerificationMethods(db)
				Class[] parametersTypes = new Class[1]
				parametersTypes[0] = String.class;

				Object[] parameters = new Object[1];
				parameters[0] = testCaseId

				Method method = verifyMethods.class.getMethod(value.get(i), parametersTypes)
				expValue = method.invoke(verifyMethods, parameters)

				resultType = RESULT_TYPE.METHOD_COMPARISON
				expected = value.get(i)
			} else {
				expValue = value.get(i)
				resultType = RESULT_TYPE.TEST_DATA
				expected = value.get(i)
			}

			def logAssertFlag = logAssert(key.get(i), nodeValue, expValue)
			assertFlag = assertFlag && logAssertFlag

			String[] tcID = testCaseId.split("-");
			if (logAssertFlag){
				//logPassorFail(tcID[1], resultType, actual + " " + expected + " Actual Value - " + nodeValue + " Expected Value - " + expValue , true)
				logPassorFail(tcID[1], resultType, "Actual Data - " + actual + "Expected Data - " + expected + " Actual Result - " + nodeValue + " Expected Result - " + expValue , true)
				logExcelPassorFail(tcID[1], resultType , actual + " " + expected + " Actual Value - " + nodeValue + " Expected Value - " + expValue , true,nodeValue,expValue)

			}
			else{
				//logPassorFail(tcID[1], resultType, actual + " " + expected + " Actual Value - " + nodeValue + " Expected Value - " + expValue , false)
				logPassorFail(tcID[1], resultType, "Actual Data - " + actual + "Expected Data - " + expected + " Actual Result - " + nodeValue + " Expected Result - " + expValue , false)
				logExcelPassorFail(tcID[1], resultType , actual + " " + expected + " Actual Value - " + nodeValue + " Expected Value - " + expValue , false,nodeValue,expValue)
			}
		}
		return assertFlag
	}

/*ArrayList<String> getNodeVal(String testCaseId, String query) {
	boolean assertFlag = true
	def expValue
	def tmp
	ArrayList<String> nodeVals = new ArrayList<String>();
	KO
	String[] temp
	String strQuery
	ConnectDB db = new ConnectDB()
	KO

	List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
	List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
	for(int i=0; i<testCaseCmb.size(); i++) {
		com.eviware.soapui.support.XmlHolder holder = xmlValues[i]

		if(testCaseCmb[i].equals(testCaseId)) {
			ArrayList<String> xPathTags = new ArrayList<String>();
			xPathTags = queryNodeXPathTags(query)

			for(int j=0 ; j<xPathTags.size ; j++)
			{
				ReadData rd = new ReadData()
				if(holder.containsKey(rd.getXPath(xPathTags.get(j))) && holder.containsValue(rd.getXPath(xPathTags.get(j)))) {
					nodeVals.add(holder.getNodeValue(rd.getXPath(xPathTags.get(j))));
				} else {
					nodeVals.add("NoVal");
				}

			}
		}
	}
	return nodeVals
}
*/
	
	ArrayList<String> getNodeVal(String testCaseId, String query) {
		boolean assertFlag = true
		def expValue
		def tmp
		ArrayList<String> nodeVals = new ArrayList<String>();
		/*KO*/
		String[] temp
		String strQuery
		ConnectDB db = new ConnectDB()
		/*KO*/

		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		List reqtestCaseCmb = Constant.lhmAllInputResponseAsXML_Global.keySet().asList()
		List reqxmlValues = Constant.lhmAllInputResponseAsXML_Global.values().asList()
		com.eviware.soapui.support.XmlHolder reqholder
		for(int i=0; i<reqtestCaseCmb.size(); i++) {
			if(reqtestCaseCmb[i].equals(testCaseId)) {
				reqholder = reqxmlValues[i]
			}
		}
		for(int i=0; i<testCaseCmb.size(); i++) {
			com.eviware.soapui.support.XmlHolder holder = xmlValues[i]

			if(testCaseCmb[i].equals(testCaseId)) {
				ArrayList<String> xPathTags = new ArrayList<String>();
				xPathTags = queryNodeXPathTags(query)

				for(int j=0 ; j<xPathTags.size ; j++)
				{
					ReadData rd = new ReadData()
					String xpath_to_find = rd.getXPath(xPathTags.get(j))
					if (xPathTags.get(j).contains("IR_"))
					{
						logInfo "IR if"
						if(reqholder.containsKey(xpath_to_find) && reqholder.containsValue(xpath_to_find)) {
							nodeVals.add(reqholder.getNodeValue(xpath_to_find));
						} else {
							nodeVals.add("NoVal");
						}
					}
					else
					{
						if(holder.containsKey(rd.getXPath(xPathTags.get(j))) && holder.containsValue(rd.getXPath(xPathTags.get(j)))) {
							nodeVals.add(holder.getNodeValue(rd.getXPath(xPathTags.get(j))));
						} else {
							nodeVals.add("NoVal");
						}
					}

				}
			}
		}
		return nodeVals
	}


	ArrayList<String> getNodeValP2(String testCaseId, String query) {
		boolean assertFlag = true
		def expValue
		def tmp
		ArrayList<String> nodeVals = new ArrayList<String>();
		/*KO*/
		String[] temp
		String strQuery
		ConnectDB db = new ConnectDB()
		/*KO*/

		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()

		List reqtestCaseCmb = Constant.lhmAllInputResponseAsXML_Global.keySet().asList()
		List reqxmlValues = Constant.lhmAllInputResponseAsXML_Global.values().asList()

		com.eviware.soapui.support.XmlHolder reqholder
		for(int i=0; i<reqtestCaseCmb.size(); i++) {
			if(reqtestCaseCmb[i].equals(testCaseId)) {
				reqholder = reqxmlValues[i]
			}
		}

		for(int i=0; i<testCaseCmb.size(); i++) {
			com.eviware.soapui.support.XmlHolder holder = xmlValues[i]

			if(testCaseCmb[i].equals(testCaseId)) {
				ArrayList<String> xPathTags = new ArrayList<String>();
				xPathTags = queryNodeXPathTags(query)
				//logInfo "xPathTags ==>"+xPathTags.toString()
				for(int j=0 ; j<xPathTags.size ; j++)
				{
					ReadData rd = new ReadData(Constant.RESPONSEDATABOOK)
					String xpath_to_find = rd.getXPath(xPathTags.get(j))
					//logInfo "xpath_to_find =>"+xpath_to_find
					if (xPathTags.get(j).contains("IR_"))
					{
						//logInfo "IR if"
						if(reqholder.containsKey(xpath_to_find) && reqholder.containsValue(xpath_to_find)) {
							nodeVals.add(reqholder.getNodeValue(xpath_to_find));
						} else {
							nodeVals.add("NoVal");
						}
					}
					else
					{
						//logInfo "No IR present"

						//logInfo holder.getXml().toString()
						if(holder.containsKey(xpath_to_find) && holder.containsValue(xpath_to_find)) {
							nodeVals.add(holder.getNodeValue(xpath_to_find));
							//logInfo "xpth value "+holder.getNodeValue(xpath_to_find)
							//logInfo "nodeVals "+nodeVals.toString()
						} else {
							nodeVals.add("NoVal");
						}
					}


				}
			}
		}
		return nodeVals
	}


	ArrayList<String> queryNodeXPathTags(String query)
	{
		String[] sp = query.split("<");
		ArrayList<String> arrValues = new ArrayList<>();
		for(int i=1 ; i<sp.length ; i++)
		{
			arrValues.add(sp[i].substring(0, sp[i].indexOf(">")));
		}
		return arrValues;
	}


	String getAccountNo(com.eviware.soapui.impl.wsdl.testcase.WsdlTestRunContext context, com.eviware.soapui.support.XmlHolder holder) {
		boolean assertFlag = true
		String nodeValue

		nodeValue = holder.getNodeValue( ExPathRpc.CREATE_ACCOUNT_ACCTNO1 )

		return nodeValue
	}

	boolean httpScriptAssert(com.eviware.soapui.model.testsuite.TestRunContext context, com.eviware.soapui.support.XmlHolder holder, LinkedHashMap m) {
		boolean assertFlag = true
		def expValue
		def nodeValue

		List key = m.keySet().asList()
		List value = m.values().asList()
		for(int i=0; i<key.size(); i++) {
			nodeValue = holder.getNodeValue( key.get(i) )
			if(value.get(i).toString().contains('$')){

				expValue = context.expand(value.get(i))
			} else {
				expValue = value.get(i)
			}
			def logAssertFlag = logAssert(key.get(i) , nodeValue, expValue)
			assertFlag = assertFlag && logAssertFlag
		}
		return assertFlag
	}
	/**
	 * Log the result of the test request
	 * @param testRequest name of the test request that was executed
	 * @param result test request run result object of type WsdlTestRequestStepResult
	 * @author vasanth.manickam
	 */
	void logTestRequestResult(testRequest, com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStepResult result) {
		this.testRequestStepResult = result

		if(testRequestStepResult.status.toString() == "FAILED"){
			Constant.FAILFLAG = true
			logInfo "Executed the test request - " + testRequest + " in " + testRequestStepResult.getTimeTaken().toString() + "\t" + ". Test Run Status : " + testRequestStepResult.status.toString() /*+ " Reason : " + testRequestStepResult.getError().toString()*/ + System.getProperty("line.separator");

			if(!(testRequestStepResult.hasResponse())) {
				logInfo "No Reponse been fetched"
			}
			//TODO Parse the response and get the error code and error message

		} else {
			logInfo "Executed the test request - " + testRequest + " in " + testRequestStepResult.getTimeTaken().toString() + ". Test Run Status : " + testRequestStepResult.status.toString() + System.getProperty("line.separator");
		}
	}

	/**
	 * Log the start of the script execution
	 * @param testCaseName name of the test case
	 * @author indira.badrinath
	 */
	void logTestStepStart(String testStepName) {
		logInfoWithNoFormat 'Test Step : (' + Constant.QUERYCOUNT + ') - ' + testStepName + System.getProperty("line.separator")
		objResponseLogFile.appendFileWithNewLine 'Test Step : (' + Constant.QUERYCOUNT + ') - ' + testStepName + System.getProperty("line.separator")
		// Increment the counter
		Constant.QUERYCOUNT++
	}

	/**
	 * Log the result of the HTTP test request
	 * @param testRequest name of the test request that was executed
	 * @param result test request run result object of type WsdlTestRequestStepResult
	 * @author indira.badrinath
	 */
	void logHTTPTestRequestResult(HTTPtestRequest, com.eviware.soapui.model.testsuite.TestStepResult result) {
		this.testHTTPRequestStepResult = result

		if(testHTTPRequestStepResult.status.toString() == "FAILED"){
			Constant.FAILFLAG = true
			logInfo "Executed the test request - " + HTTPtestRequest + " in " + testHTTPRequestStepResult.getTimeTaken().toString() + "\t" + ". Test Run Status : " + testHTTPRequestStepResult.status.toString() /*+ " Reason : " + testRequestStepResult.getError().toString()*/ + System.getProperty("line.separator");

			if(!(testHTTPRequestStepResult.size < 0)) {
				logInfo "No Reponse been fetched"
			}
			//TODO Parse the response and get the error code and error message

		} else {
			logInfo "Executed the test request - " + HTTPtestRequest + " in " + testHTTPRequestStepResult.getTimeTaken().toString() + ". Test Run Status : " + testHTTPRequestStepResult.status.toString() + System.getProperty("line.separator");
		}
	}

	void logRequest (testStepName, request) {
		logReqResInfo("Soap Request for the API call - '" + testStepName + "'" + System.getProperty("line.separator"))
		logReqResInfo(request + System.getProperty("line.separator") + System.getProperty("line.separator"))
	}

	//void logHttpRequest(testStepName, int propertyCount, com.eviware.soapui.model.testsuite.TestCase targetStep, com.eviware.soapui.impl.wsdl.testcase.WsdlTestRunContext context)
	void logHttpRequest(testStepName,com.eviware.soapui.impl.wsdl.testcase.WsdlTestRunContext context)
	{

		String propValueList = new StringBuffer();
		logReqResInfo("Request for the API call - '" + testStepName + "'" + System.getProperty("line.separator"))
		// Print the Endpoint for the Request
		if (context.containsKey("requestUri") == true)
		{
			propValueList = context.get("requestUri")

		}
		logReqResInfo("Endpoint for the API Call: " + propValueList + System.getProperty("line.separator") + System.getProperty("line.separator"))
		if (context.containsKey("httpResponseProperties") == true)
		{
			propValueList = context.get("httpResponseProperties")

		}

		logReqResInfo("Request Parameter: " + propValueList + System.getProperty("line.separator") + System.getProperty("line.separator"))
	}
		
	/**
	 * Log the API call Response
	 * @param testStepName name of the test step(api)
	 * @author indira.badrinath
	 */
	void logResponse (testStepName, response) {
		logReqResInfo("Soap Response for the API call - '" + testStepName + "'" + System.getProperty("line.separator"))
		logReqResInfo(response + System.getProperty("line.separator"))
	}
	/**
	 * Log the HHTP API call's Response
	 * @param testStepName name of the test step(api)
	 * @author indira.badrinath
	 */
	void logHTTPResponse (testStepName, response) {
		logReqResInfo("HTTP Response for the API call - '" + testStepName + "'" + System.getProperty("line.separator"))
		logReqResInfo(response + System.getProperty("line.separator"))
	}
	/**
	 * Log the info in the response log file
	 * @param message information to be logged
	 * @author vasanth.manickam
	 */
	void logReqResInfo (String message) {
		def responseFile = new File(responseLogPath)

		if (responseFile.exists()== false) {
			responseFile.createNewFile()
		}
		responseFile.append(message + System.getProperty("line.separator"));
	}

	boolean logAssert(nodeName, actual, String expected) {
		boolean matchFlag = false

		// Check match for multiple expected data
		if(expected.contains(";")) {
			String[] expArr = expected.split(";")
			int i = 0;
			while(i<expArr.length) {
				if(expArr[i].startsWith("~")) {
					matchFlag = matchFlag || RegExp.exactMatch(actual, expArr[i].substring(1, expArr[i].length()))
				} else if(expArr[i].equals('null')){
					matchFlag = matchFlag || actual.equals (null)
				} else {
					matchFlag = matchFlag || actual.equals(expArr[i])
				}
				i++
			}
		} else {
			if(expected.startsWith("~")) {
				matchFlag = RegExp.exactMatch(actual, expected.substring(1, expected.length()))
			} else if(expected.equals('null')){
				matchFlag = actual.equals (null)
			} else {
				matchFlag = (actual.equals(expected))
			}
		}
		//TODO Modify the log if we expect the data to be either of option separated with ';'
		if(matchFlag) {
			logInfo("PASS - Verified: '" + nodeName + "' -RESULT: '" + actual + "'" )
		} else {
			if(expected.startsWith("~")) {
				logInfo("(FAIL) - Verified: '" + nodeName + "' -RESULT: '" + expected + "' - ERROR: '" + actual + "'");
			} else {
				logInfo("(FAIL) - Verified: '" + nodeName + "' -RESULT: '" + expected + "' - ERROR: '" + actual + "'");
				Constant.FAILFLAG = true
			}
		}
		return matchFlag
	}

	boolean logAssertP2(String nodeName, def actual, def expected) {
		logInfo ""
		logInfo("Verifying NODE: " + nodeName.toString())
		logInfo("Actual => " + actual.toString())
		logInfo("Expected => " + expected.toString())
		boolean matchFlag = false


		// Check match for multiple expected data
		
		if(expected.toString().contains(";") && !expected.getClass().toString().contains("Map")) {
			String[] expArr = expected.split(";")
			int i = 0;
			while(i<expArr.length) {
				if(expArr[i].startsWith("~")) {
					//logInfo "condna"
					matchFlag = matchFlag || RegExp.exactMatch(actual, expArr[i].substring(1, expArr[i].length()))
				} else if(expArr[i].equals('null')){
					//	logInfo "condnb"
					matchFlag = matchFlag || actual.equals (null)
				} else {
					//logInfo "condnc"
					matchFlag = matchFlag || actual.equals(expArr[i])
				}
				i++
			}
		} else {
			if(expected.toString().startsWith("~")) {
				matchFlag = RegExp.exactMatch(actual, expected.toString().substring(1, expected.toString().length()))
				//	logInfo "condn1"
			} else if(expected.toString().equals('null')){
				//logInfo "condn2"
				matchFlag = actual.equals (null)
			} else {
				//logInfo "condn3"
				matchFlag = (actual.toString().equals(expected.toString()))
			}
		}

		//TODO Modify the log if we expect the data to be either of option separated with ';'
		if(matchFlag) {
			logInfo("Verification PASSED!" )
		} else {
			if(expected.toString().startsWith("~")) {
				logInfo("(FAIL) - Verified: '" + nodeName + "' -RESULT: '" + expected + "' - ERROR: '" + actual + "'");
			} else {
				logInfo("Verification FAILED!");
				Constant.FAILFLAG = true
			}
		}
		logInfo ""
		return matchFlag
	}

	//KO
	/**
	 * Add the specified APIName as Key and its output XML response as Value to the specified LinkedHashMap and return the LinkedHashMap
	 * @param objLinkedHashMap -  LinkedHashMap to which the key and value to be added
	 * @param apiName - String API Name to be added as Key
	 * @param holder - Output XML response of the API to be added as Value
	 */
	/* LinkedHashMap addResponseToMap1(LinkedHashMap objLinkedHashMap, String apiName, com.eviware.soapui.support.XmlHolder holder) {
	 objLinkedHashMap.put apiName, holder
	 lhmAllOutputResponse.put apiName, holder
	 return objLinkedHashMap
	 } */

	/**
	 * Add the specified APIName as Key and its output XML response as Value to the specified LinkedHashMap and return the LinkedHashMap
	 * @param apiName - String API Name to be added as Key
	 * @param holder - Output XML response of the API to be added as Value
	 */
	/* LinkedHashMap addResponseToMap(String apiName, String holder) {
	 lhmAllOutputResponse.put apiName, holder
	 return lhmAllOutputResponse
	 }
	 */
	/**
	 * Add the specified APIName as Key and its output XML response as Value to the specified LinkedHashMap and return the LinkedHashMap
	 * @param apiName - String API Name to be added as Key
	 * @param holder - Output XML response of the API to be added as Value
	 */
	LinkedHashMap addOutputResponse(String apiName, String holder) {
		lhmAllOutputResponse.put apiName, holder
		return lhmAllOutputResponse
	}

	/**
	 * Add the specified APIName as Key and its output XML response as Value to the specified LinkedHashMap and return the LinkedHashMap
	 * @param apiName - String API Name to be added as Key
	 * @param holder - Input XML request for the API to be added as Value
	 */
	LinkedHashMap addInputResponse(String apiName, String holder) {
		lhmAllInputResponse.put apiName, holder
		return lhmAllInputResponse
	}


	/**
	 * Add the specified APIName as Key and its output XML response as Value to the specified LinkedHashMap and return the LinkedHashMap
	 * @param apiName - String API Name to be added as Key
	 * @param holder - Output XML response of the API to be added as Value
	 */
	LinkedHashMap addOutputResponseAsXML(String apiName, com.eviware.soapui.support.XmlHolder holder) {
		Constant.lhmAllOutputResponseAsXML_Global.put apiName, holder
		return Constant.lhmAllOutputResponseAsXML_Global
	}

	void clearOutputResponseAsXML() {
		Constant.lhmAllOutputResponseAsXML_Global.clear();
		//logInfo "Cleared Responses hashmap"


	}
	void addServiceHashAsXml(String servicename,LinkedHashMap<String,String> service_data)
	{
		Constant.lhmAllServcieDataAsXML_Global.put(servicename, service_data)
		//return Constant.lhmAllServcieDataAsXML_Global

	}

	void clearServiceHashAsXml()
	{
		Constant.lhmAllServcieDataAsXML_Global.clear();
		//return Constant.lhmAllServcieDataAsXML_Global

	}

	/**S
	 * Add the specified APIName as Key and its output XML response as Value to the specified LinkedHashMap and return the LinkedHashMap
	 * @param apiName - String API Name to be added as Key
	 * @param holder - Input XML request of the API to be added as Value
	 */
	LinkedHashMap addInputResponseAsXML(String apiName, com.eviware.soapui.support.XmlHolder holder) {
		Constant.lhmAllInputResponseAsXML_Global.put apiName, holder
		return Constant.lhmAllInputResponseAsXML_Global
	}

	/**
	 * To verify the output XML response with the calculated expected output after VIrtual time conversion
	 * @param holder - Output response in XML format
	 * @param m - XPath & Value fetched from ResponseVerification file
	 * @param testCaseId - Test case Id in same format as we saved in LinkedHashMap like - library.Constants.Constant.TESTSUITENAME + "-" +testcaseid +"-create_acct_complete"
	 */
	boolean scriptAssertAfterVT(/*m.eviware.soapui.impl.wsdl.testcase.WsdlTestRunContext context,*/ com.eviware.soapui.support.XmlHolder holder, LinkedHashMap m ,
			String testCaseId) {
		boolean assertFlag = true
		def expValue
		def nodeValue

		String strQuery
		ConnectDB db = new ConnectDB()

		List key = m.keySet().asList()
		List value = m.values().asList()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData rd = new ReadData()
		rd.fetchTestData(testId)
		for(int i=0; i<key.size(); i++) {
			nodeValue = holder.getNodeValue( key.get(i) )

			if(value.get(i).toString().contains('$')){
				//expValue = context.expand(value.get(i))

				/*KO*/
			} else if(value.get(i).toString().contains('SELECT')) {


				strQuery = db.buildQuery(value.get(i), rd.TD_clientNo, getNodeVal(testCaseId, value.get(i)))

				ResultSet resultSet = db.executePlaQuery(strQuery)
				if (resultSet.next()) {
					expValue = resultSet.getString(1)
				}
			} else {
				expValue = value.get(i)
			}
			def logAssertFlag = logAssert(key.get(i) , nodeValue, expValue)

			String[] tcID = testCaseId.split("-");
			if (logAssertFlag)
				logPassorFail(tcID[1], RESULT_TYPE.DB_COMPARISON, key.get(i) +  ": XML - " + nodeValue + "DB - " + expValue , true)
			else
				logPassorFail(tcID[1], RESULT_TYPE.DB_COMPARISON, key.get(i) +  ": XML - " + nodeValue + "DB - " + expValue , false)

			assertFlag = assertFlag && logAssertFlag
			assertFlag
		}
		return assertFlag
	}


	/**
	 * TODO Put here a description of what this method does.
	 *
	 * @param sFilePath
	 * @param Script_Name
	 * @return
	 */
	public boolean createResFile_Overall(String sPath, String Script_Name)
	{
		boolean status = true;
		sFilePath = sPath;
		try
		{
			//			   Constant.batchLogFilePath = sPath + Script_Name + "_batchLog.txt"
			ResultFileOverall = sPath + Script_Name + ".html";
			fWriterOverall = new FileWriter(ResultFileOverall);
			writerOverall = new BufferedWriter(fWriterOverall);

			writerOverall.write("<Html> " +
					"<head>  <title> Result - " + Script_Name + " </title>  </head> " +
					"<body bgcolor=\"DAF1FE\">");

			writerOverall.newLine();
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return status;
	}

	/**
	 * TODO Put here a description of what this method does.
	 *
	 * @param Script_Name
	 * @return
	 */
	public String createResFile(String sPath,String Script_Name)
	{
		try
		{
			sFilePath = sPath;
			ResultFile = sFilePath + Script_Name + ".html";
			fWriter = new FileWriter(ResultFile);
			writer = new BufferedWriter(fWriter);

			writer.write("<Html> " +
					"<head>  <title> Result - " + Script_Name + " </title>  </head> " +
					"<script> function showDiv(objectID) { " +
					"var theElementStyle = document.getElementById(objectID); " +

					" if(theElementStyle.style.display == \"none\") " +
					" { theElementStyle.style.display = \"block\";  }" +
					" else { theElementStyle.style.display = \"none\"; } " +
					" } " +

					" </script> " +
					"<body bgcolor=\"DAF1FE\">");

			writer.newLine();
			intStep = 1;
			Arrays.fill(intStepNo, 1);
			//Arrays.fill(logFinalStatement, "");

		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return ResultFile;
	}



	public String createResFile(String Script_Name)
	{
		try
		{
			ResultFile = sFilePath + Script_Name + ".html";
			fWriter = new FileWriter(ResultFile);
			writer = new BufferedWriter(fWriter);
			
			//Testlink XML import
			xmlResultFile = sFilePath + "Testlink_Results_Import.xml";
			xmlfWriter = new FileWriter(xmlResultFile);
			xmlwriter = new BufferedWriter(xmlfWriter);
			
			xmlwriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			xmlwriter.newLine();
			xmlwriter.write("<results>");
			
			
			reportfileHeader = "<Html> " +
					"<head>  " +
					"<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>"+
					"<script type=\"text/javascript\">    google.load('visualization', '1.0', {'packages':['corechart']});  google.setOnLoadCallback(drawChart);"+
					"function drawChart() {"+
					"var data = new google.visualization.DataTable(); data.addColumn('string', 'Result'); data.addColumn('number', 'Count');"+
					
					"data.addRows(["+
						"['PASS', PASS_COUNT_REPLACEMENT],"+
						"['FAIL', FAIL_COUNT_REPLACEMENT],"+
					  "]);"+
					 " var options = {'title':'::: Execution Trend :::',"+
						 " 'width':500,"+
						 " 'height':300,"+
						 " 'backgroundColor':'DAF1FE',"+
						 " 'colors': ['#009933', '#F00000'],"+
						 " 'fontSize': 15};"+
						 "var chart = new google.visualization.PieChart(document.getElementById('chart_div'));"+
						 "chart.draw(data, options);	}  </script>"+
					  
					"<title> Result - " + Script_Name + " </title>  </head> " +
					"<script> function showDiv(objectID) { " +
					"var theElementStyle = document.getElementById(objectID); " +

					" if(theElementStyle.style.display == \"none\") " +
					" { theElementStyle.style.display = \"block\";  }" +
					" else { theElementStyle.style.display = \"none\"; } " +
					" } " +

					" </script> " +
					"<body bgcolor=\"DAF1FE\">"

			writer.newLine();
			intStep = 1;
			Arrays.fill(intStepNo, 1);
			//Arrays.fill(logFinalStatement, "");

		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return ResultFile;
	}

	public String createFailResFile(String Script_Name)
	{
		String failFile
		try
		{
			failFile = sFilePath + Script_Name + ".html";
			fWriter = new FileWriter(failFile);
			failwriter = new BufferedWriter(fWriter);

			failwriter.write("<Html> " +
					"<head>  <title> Result - " + Script_Name + " </title>  </head> " +
					"<script> function showDiv(objectID) { " +
					"var theElementStyle = document.getElementById(objectID); " +

					" if(theElementStyle.style.display == \"none\") " +
					" { theElementStyle.style.display = \"block\";  }" +
					" else { theElementStyle.style.display = \"none\"; } " +
					" } " +

					" </script> " +
					"<body bgcolor=\"DAF1FE\">");

			failwriter.newLine();
			intStep = 1;
			Arrays.fill(intStepNo, 1);
			//Arrays.fill(logFinalStatement, "");

		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return failFile;
	}

	public void logHeader(String ScriptName)
	{   
		VerificationMethods vm =new VerificationMethods()
		String strReport = null;
		String strHeaderTableWidth1 = "35%";
		String strHeaderTableWidth = "100%";
		String strHeaderFirstColumnWidth = "45%";
		String strHeaderSecondColumnWidth = "70%";
		try
		{
			reportfileHeader = reportfileHeader + "<table width=\"100%\" border=\"0\" margin=\"10,5,5,50\"  >" +
					"<tr width=\"100%\">" +
					"<th > " + ScriptName + " Execution Report</th>" +
					"</tr>	" +
					/*"<tr width=\"100%\">" +
			 "<th align=\"right\"> <a href=\"#\" Target=\"_bottom\"> Go to Bottom </a></th>" +
			 "</tr>	" +*/
					"</table>" +
					"<br /> <div id=\"chart_div\" style=\"float:right\"></div>";


			//writer.write(strReport);
			//writer.newLine();

			reportfileHeader =reportfileHeader+ "<table width=\"" +strHeaderTableWidth1 +"\" border=\"1\" margin=\"10,5,5,50\"    >" +
					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\" >Test Suite</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + ScriptName + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Environment</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "QU" + "</th>" +
					"</tr>	" +
					
					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Sprint Name</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + vm.md_get_sprint_name() + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Start Time</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hStartTime" + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">End Time</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hEndTime" + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Total Execution Time</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hTotalTime" + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Total Count</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hTestCaseCount" + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Pass Count</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hPassCount" + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Fail Count</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hFailCount" + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Total Time Taken for VT </th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hvtTimeTaken" + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Average Response Time </th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hAPIResponseTime" + "</th>" +
					"</tr>	" +

					"</table>"  +
					"<table width=\"100%\" border=\"0\" margin=\"10,5,5,50\"  >" +
					"<tr width=\"100%\">" +
					"<th width=\"100%\"> <font color=\"red\"><a  href=\"Failure_Report.html\" > Filter Failures <a/></font></th>" +
					"</tr> </table>"+
					"<br /> ";

			//	writer.write(strReport);
			//	writer.newLine();


			reportfileHeader =reportfileHeader+ "<table width=\"100%\" class=\"" + ScriptName + "\"  bgcolor=\"#82B6EA\" border=\"1\" margin=\"10,5,5,50\">" +
					"<tr>" +
					"<th width=\"8%\">Environment</th>" +
					"<th width=\"64%\">Test Case Number & Description</th>" +
					"<th width=\"10%\">Execution Duration (Seconds)</th>" +
					"<th width=\"8%\">Test Status</th>" +
					"<th width=\"10%\">Bug_ID</th>" +
					"</tr>" +
					"</table>";
			//writer.write(strReport);
			//writer.newLine();
		}
		catch (Exception ex) { 		}
	}


	/**
	 * TODO Put here a description of what this method does.
	 *
	 */
	//	   public void logTCDesc(String strTCNumber, String strTCDesc)
	//	   {
	//		   String strReport = "";
	//		   int stepTCID = getTCNumber(strTCNumber)
	//		   try
	//		   {
	//			   if (stepTCID >1){
	//				   strReport ="<br /> </div>";
	//				   //sb.append("<br /> </div>")
	//			   }
	//
	//			   strReport = strReport.toString() + "<table width=\"100%\" class=\"" + strTCNumber + "\"  bgcolor=\"#DEE6EE\" border=\"2\" margin=\"10,5,5,50\">" +
	//					   " <tr>" +
	//					   "<td width=\"8%\" >" + "QU" + "</td>" +
	//					   //"<td width=\"64%\"><a href=\"#\" ID=\"One\" style=\"visibility:visible\">" + strTCNumber + ". " + strTCDesc + "</a></td>" +
	//					   "<td width=\"64%\"><a href=\"#\" onClick=\"showDiv('targetDiv" + strTCNumber +"');return false;\">" + strTCNumber + ". " + strTCDesc + "</a></td>" +
	//					   "<td width=\"10%\" align=\"center\"> " + "tcExecDuration" + stepTCID + " </td>" +
	//					   "<td width=\"8%\" align=\"center\"> " + "tcStatus" + stepTCID + " </td>" +
	//					   "<td width=\"10%\" align=\"center\"> " + "tcException" + stepTCID + " </td>" +
	//					   "</tr>" +
	//					   "</table>" + "<div id=\"targetDiv" + strTCNumber + "\">" ;
	//
	//					   keyResult = strTCNumber.toString() + "_0" //+ (stepTCID-1)
	//						//keyResult = keyResult + stepTCID+ "_" + (stepTCID-1)
	//						//keyResult = keyResult
	//					   valueResult = strReport.toString()
	//					   lhmTotalReport.put keyResult, valueResult
	//					   lhmTCStatus.put strTCNumber, true
	//		   }
	//		   catch(Exception ex) { }
	//	   }

	public void logTCDesc(String strTCNumber, String strTCDesc)
	{
		String strReport = "";
		int stepTCID = getTCNumber(strTCNumber)
		try
		{

			strReport = strReport.toString() + "<table width=\"100%\" class=\"" + strTCNumber + "\"  bgcolor=\"#DEE6EE\" border=\"2\" margin=\"10,5,5,50\">" +
					" <tr>" +
					"<td width=\"8%\" >" + "QU" + "</td>" +
					//"<td width=\"64%\"><a href=\"#\" ID=\"One\" style=\"visibility:visible\">" + strTCNumber + ". " + strTCDesc + "</a></td>" +
					"<td width=\"64%\"><a href=\"#\" onClick=\"showDiv('targetDiv" + strTCNumber +"');return false;\">" + strTCNumber + ". " + strTCDesc + "</a></td>" +
					"<td width=\"10%\" align=\"center\"> " + "tcExecDuration" + stepTCID + " </td>" +
					"<td width=\"8%\" align=\"center\"> " + "tcStatus" + stepTCID + " </td>" +
					"<td width=\"10%\" align=\"center\"> " + "tcException" + stepTCID + " </td>" +
					"</tr>" +
					"</table>" + "<div id=\"targetDiv" + strTCNumber + "\" style=\"display:none\">" ;

			keyResult = strTCNumber.toString() + "_0" //+ (stepTCID-1)
			//keyResult = keyResult + stepTCID+ "_" + (stepTCID-1)
			//keyResult = keyResult
			valueResult = strReport.toString()
			lhmTotalReport.put keyResult, valueResult
			lhmTCStatus.put strTCNumber, true
			lhmTCDesc.put strTCNumber, strTCDesc
			lhmTCGenericException.put strTCNumber, ""
		}
		catch(Exception ex) { }
	}

	public void logFailTCDesc(String strTCNumber, String strTCDesc)
	{
		String strReport = "";
		int stepTCID = getTCNumber(strTCNumber)
		try
		{

			strReport = strReport.toString() + "<table width=\"100%\" class=\"" + strTCNumber + "\"  bgcolor=\"#DEE6EE\" border=\"2\" margin=\"10,5,5,50\">" +
					" <tr>" +
					"<td width=\"8%\" >" + "QU" + "</td>" +
					//"<td width=\"64%\"><a href=\"#\" ID=\"One\" style=\"visibility:visible\">" + strTCNumber + ". " + strTCDesc + "</a></td>" +
					"<td width=\"64%\"><font color=\"blue\">" + strTCNumber + ". " + strTCDesc + "</font></td>" +
					"<td width=\"10%\" align=\"center\"> " + "tcExecDuration" + stepTCID + " </td>" +
					"<td width=\"8%\" align=\"center\"> " + "tcStatus" + stepTCID + " </td>" +
					"<td width=\"10%\" align=\"center\"> " + "tcException" + stepTCID + " </td>" +
					"</tr>" +
					"</table>" + "<div id=\"targetDiv" + strTCNumber + "\">" ;

			keyResult = strTCNumber.toString() + "_0" //+ (stepTCID-1)
			//keyResult = keyResult + stepTCID+ "_" + (stepTCID-1)
			//keyResult = keyResult
			valueResult = strReport.toString()
			lhmFailureReport.put keyResult, valueResult
			lhmTCStatus.put strTCNumber, true
			lhmTCDesc.put strTCNumber, strTCDesc
			lhmTCGenericException.put strTCNumber, ""
		}
		catch(Exception ex) { }
	}

	/**
	 * This is used to log statements with Pass in black and Fail in red color
	 *
	 * @param enumResultType - To define what kind of step it is.
	 * 		Like SCRIPT , DB_VALUE , DB_COMPARISON, APPLICATION_ERROR , DB_ERROR, SCRIPT_ERROR , UNKNOWN_ERROR
	 * @param strReport - Logger statement
	 * @param status - To define whether its pass or fail. True - Pass, False - Fail
	 */
	public void logPassorFail(String strTCNumber, RESULT_TYPE enumResultType, String strReport, boolean status)
	{
		int stepTCID = getTCNumber(strTCNumber)-1
		String failmessage = ""
		try
		{
			if (status)
			{

				if(strReport.contains('Created Account No :') || strReport.contains('Statement Generated'))
				{
					strReport =  "<TABLE ID=\"One\" class= \"" + strTCNumber + "\" bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\">" +
							" <TR> <TD width = \"8%\">" +
							"StepNo:  " + intStepNo[stepTCID] + //intStep +
							"    </TD>       <TD width = \"92%\"  bgcolor=\"DarkSeaGreen\" ><b>" +
							enumResultType.toString() + " :: " + strReport + "</b> </TD>        </TR></TABLE>" ;
					failmessage =strReport
				}
				else if (strReport.contains('##### ADVANCING VT FOR') == true)
				{
					strReport =  "<TABLE ID=\"One\" class= \"" + strTCNumber + "\" bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\">" +
							" <TR> <TD width = \"8%\">" +
							"StepNo:  " + intStepNo[stepTCID] + //intStep +
							"    </TD>       <TD width = \"92%\"  bgcolor=\"LightSalmon\" ><b>" +
							enumResultType.toString() + " :: " + strReport + "</b> </TD>        </TR></TABLE>" ;

				}
				else if((strReport.contains('<-----------------') == true) && (strReport.contains('<----------------- Verifying VTV') == false))
				{
					
					if(strReport.contains(' in UI') == true)
					{
						String test = ""
						StatementTemplates objStmt = new StatementTemplates()
						UsageVerificationMethods uvm = new UsageVerificationMethods()
						String client_no =Constant.mycontext.expand('${Properties#Client_No}')
						String acct_no = uvm.getValueFromResponse("create_acct_complete","//acct_no")
						String invoiceno = uvm.getInvoiceNoVT(client_no, acct_no).toString();
						objStmt.captureShot(strTCNumber+"-"+invoiceno)
						String screenshotPath = Constant.logFilePath+strTCNumber+"-"+invoiceno+".png"
						logInfo "Now the UI screenshot path is: "+screenshotPath
						String encoded = Base64.encodeFromFile(screenshotPath)
						//logInfo "encoded : "+encoded
						String showScreenshot = "<pre><font color=\"blue\" size=\"4\"><b>Show Screenshot</b></font><br><br>"
						String shwResdivIndex = "TargetShowRes" + Constant.showResIndex.toString()
						Constant.showResIndex = Constant.showResIndex + 1
						
						test = "<pre><font color=\"blue\" size=\"4\"><b>Screenshot</b></font><br><br>"
						//showReqResText= showReqResText.replace("|| REPLACE WITH RESP HEADER ||", "<br><br><hr><br>" + "<font color=\"blue\" size=\"4\"><b>RESPONSE</b></font><br><br>")
						test = test + "<img src=\"data:image/png;base64,"+encoded+"\" />" 
						//test = test + "<img src=\""+screenshotPath+"\" />"
						test = test + "</pre></div>"
						
						
						strReport =  "<TABLE ID=\"One\" class= \"" + strTCNumber + "\" bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\">" +
								" <TR> <TD width = \"8%\">" +
								"StepNo:  " + intStepNo[stepTCID] + //intStep +
								"    </TD>       <TD width = \"92%\"  bgcolor=\"PowderBlue\" > <b>" +
								enumResultType.toString() + " :: " + strReport + "</b> <a href=\"#\" onClick=\"showDiv('"+shwResdivIndex +"');return false;\"> Show Screenshot </a>  </TD>        </TR></TABLE> " +
								"<div id=\""+ shwResdivIndex+"\" style=\"display:none\">" + test
					}
					else
					{
						String showReqResText = Constant.TestCaseAPI_Req_Res
						String shwResdivIndex = "TargetShowRes" + Constant.showResIndex.toString()
						Constant.showResIndex = Constant.showResIndex + 1
						
						showReqResText = showReqResText.replace("<", "&lt;")
						showReqResText = showReqResText.replace(">", "&gt;")
						showReqResText = showReqResText.replace("&gt;&lt;", "&gt;<br>&lt;")
						showReqResText = "<pre><font color=\"blue\" size=\"4\"><b>REQUEST</b></font><br><br>" + showReqResText
						showReqResText= showReqResText.replace("|| REPLACE WITH RESP HEADER ||", "<br><br><hr><br>" + "<font color=\"blue\" size=\"4\"><b>RESPONSE</b></font><br><br>")
						showReqResText = showReqResText + "</pre></div>"
						
						strReport =  "<TABLE ID=\"One\" class= \"" + strTCNumber + "\" bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\">" +
								" <TR> <TD width = \"8%\">" +
								"StepNo:  " + intStepNo[stepTCID] + //intStep +
								"    </TD>       <TD width = \"92%\"  bgcolor=\"PowderBlue\" > <b>" +
								enumResultType.toString() + " :: " + strReport + "</b> <a href=\"#\" onClick=\"showDiv('"+shwResdivIndex +"');return false;\"> Show Request / Response </a>  </TD>        </TR></TABLE> " +
								"<div id=\""+ shwResdivIndex+"\" style=\"display:none\">" + showReqResText
					}
					
					failmessage =strReport
				}
				else if (strReport.contains('<----------------- Verifying VTV') == true)
				{
					strReport =  "<TABLE ID=\"One\" class= \"" + strTCNumber + "\" bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\">" +
							" <TR> <TD width = \"8%\">" +
							"StepNo:  " + intStepNo[stepTCID] + //intStep +
							"    </TD>       <TD width = \"92%\"  bgcolor=\"PowderBlue\" ><b>" +
							enumResultType.toString() + " :: " + strReport + "</b> </TD>        </TR></TABLE>" ;
					failmessage =strReport
				}
				else
				{
					//strReport = strReport.replace("Actual Value -", "<br><b>Actual Value -</b>")
					//strReport = strReport.replace("Expected Value -", "<br><b>Expected Value -</b>")
					strReport = strReport.replace("Actual Data -", "<b>Actual Data -</b>")
					strReport = strReport.replace("Expected Data -", "<br><b>Expected Data -</b>")
					strReport = strReport.replace("Actual Result -", "<br><hr size='1'><b>Actual Result -</b>")
					strReport = strReport.replace("Expected Result -", "<br><b>Expected Result -</b>")
					strReport =  "<TABLE ID=\"One\" class= \"" + strTCNumber + "\" bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\">" +
							" <TR> <TD width = \"8%\" >" +
							"StepNo:  " + intStepNo[stepTCID] + //intStep +
							"    </TD>       <TD width = \"92%\">" +
							 strReport + " </TD>        </TR></TABLE>" ;
				}
			} else {
				//strReport = strReport.replace("Actual Value -", "<br><b>Actual Value -</b>")
				//strReport = strReport.replace("Expected Value -", "<br><b>Expected Value -</b>")
			strReport = strReport.replace("Actual Data -", "<b>Actual Data -</b>")
			strReport = strReport.replace("Expected Data -", "<br><b>Expected Data -</b>")
			strReport = strReport.replace("Actual Result -", "<br><hr size='1'><b>Actual Result -</b>")
			strReport = strReport.replace("Expected Result -", "<br><b>Expected Result -</b>")
				strReport = "<TABLE ID=\"One\" class= \"" +strTCNumber +  "\" bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White; color:red\">" +
						" <TR> <TD width = \"8%\" >" +
						"StepNo:  " +  intStepNo[stepTCID] + //intStep +
						"    </TD>       <TD width = \"92%\">" +
						strReport + " </TD>        </TR></TABLE>" ;
				lhmTCStatus.put strTCNumber, false
				failmessage =strReport
				if(strReport.contains('Failed during xpath verification in UI') == true)
				{
					StatementTemplates objStmt = new StatementTemplates()
					UsageVerificationMethods uvm = new UsageVerificationMethods()
					String client_no =Constant.mycontext.expand('${Properties#Client_No}')
					String acct_no = uvm.getValueFromResponse("create_acct_complete","//acct_no")
					String invoiceno = uvm.getInvoiceNoVT(client_no, acct_no).toString();
					objStmt.captureShot(strTCNumber+"-"+invoiceno)
				}
				// Calculate step id and number of failure count here
			}

			keyResult = strTCNumber.toString() + "_"+ intStepNo[stepTCID]
			valueResult = strReport
			lhmTotalReport.put keyResult, valueResult
			if (failmessage != "") lhmFailureReport.put keyResult, failmessage

			//writer.write(strReport);
			//writer.newLine();
			intStep++;
			//intStepNo[stepTCID] = intStepNo[stepTCID] + 1
			incrementStepNo(stepTCID);
		}
		catch (Exception e) {  }
		//return (intStepNo[stepTCID])
	}

	/**
	 * This function updates result file with specified content - Generic consolidated statement in green color and warning messages in blue color
	 *
	 * @param strReport - Display message to be printed in output html report
	 * @param bWarning - To specify whether the message to be printed is
	 * 		"Warning" - true (will be printed in blue color)
	 * 		Consolidated statement - false (will be printed in green color)
	 */
	public void logGenericorWarning(String strTCNumber, RESULT_TYPE enumResultType, String strReport, boolean bWarning, boolean bReplace)
	{
		try
		{
			int stepTCID = getTCNumber(strTCNumber) -1

			if (!bWarning) {
				strReport = "<TABLE width = '100%' cellspacing = 0 cellpadding = 2  style = 'border-left:solid 3px White;color:green'>" +
						"   <TR>       <TD width = '8%' >" +
						"       </TD>       <TD width = '84%'>" +
						strReport + "       </TD>       <TD width = '8%'>" +
						"<SPAN></SPAN>       </TD>   </TR></TABLE>";
			} else {
				strReport = "<TABLE ID=\"One\"   class= \"" + strTCNumber + "\" bgcolor=\"#FBFBEF\" border width ='100%'  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White; color:blue; visibility:visible\"\">" +
						" <TR> <TD width = \"8%\" >" +
						"StepNo:  " +  intStepNo[stepTCID] + //intStep +
						"    </TD>       <TD width = \"92%\">" +
						enumResultType.toString() + " :: " + strReport + " </TD>        </TR></TABLE>";
			}

			//strTotalReport.get(stepTCID-1).add(strReport);
			keyResult = strTCNumber.toString() +"_"+ intStepNo[stepTCID]
			valueResult = strReport
			lhmTotalReport.put keyResult, valueResult

			intStep++;
			//intStepNo[stepTCID-1]++;
			incrementStepNo(stepTCID);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	//	   public void logTCEnd(String strOverallReportPath, List tcIDs) {
	//		   try
	//		   {
	//			   totalPassorFailTC()
	//
	//			   printInFile(tcIDs )
	//			   String strReport = "</div> <br /> <table width=\"100%\" border=\"0\" margin=\"10,5,5,50\"  >" +
	//					   "<tr width=\"100%\">" +
	//					   "<th width=\"90%\"> <a  href=\"file:///" + strOverallReportPath +"#\" > Back to Overall Report <a/></th>" +
	//					   "<th width=\"10%\"> <a  href=\"#\" Target=\"_top\"> Go to Top <a/></th>" +
	//					   "</tr>	</table> <br /> ";
	//
	//			   writer.write(strReport);
	//			   writer.newLine();
	//
	//			   writer.write(" </body> </html>");
	//			   writer.newLine();
	//			   writer.close();
	//		   }
	//		   catch (Exception e)  {  }
	//	   }

	public void logTCEnd(String strOverallReportPath, List tcIDs, String strScriptName) {
		try
		{
			totalPassorFailTC()

			printInFile(tcIDs, strScriptName )
			String strReport = "</div> <br /> <table width=\"100%\" border=\"0\" margin=\"10,5,5,50\"  >" +
					"<tr width=\"100%\">" +
					"<th width=\"90%\"> <a  href=\"file:///" + strOverallReportPath +"#\" > Back to Overall Report <a/></th>" +
					"<th width=\"10%\"> <a  href=\"#\" Target=\"_top\"> Go to Top <a/></th>" +
					"</tr>	</table> <br /> ";

			writer.write(strReport);
			writer.newLine();

			writer.write(" </body> </html>");
			writer.newLine();
			writer.close();
			
			//XML testlink results 
			xmlwriter.newLine();
			xmlwriter.newLine();
			xmlwriter.write("</results>");
			xmlwriter.close()
			
		}
		catch (Exception e)  {  }
	}



	public void logHeader_Overall(String ScriptName)
	{
		String strReport = null;
		String strHeaderTableWidth1 = "35%";
		String strHeaderTableWidth = "100%";
		String strHeaderFirstColumnWidth = "30%";
		String strHeaderSecondColumnWidth = "70%";
		try
		{
			strReport = "<table width=\"100%\" border=\"0\" margin=\"10,5,5,50\"  >" +
					"<tr width=\"100%\">" +
					"<th > " + ScriptName + " </th>" +
					"</tr>	" +
					"</table>" +
					"<br /> ";


			writerOverall.write(strReport);
			writerOverall.newLine();

			strReport = "<table width=\"" +strHeaderTableWidth1 +"\" border=\"1\" margin=\"10,5,5,50\"    >" +
					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\" >Test Type</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + ScriptName + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Environment</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "QU" + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Start Time</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hStartTime" + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">End Time</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hEndTime" + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Total Count</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hTestCaseCount" + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Pass Count</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hPassCount" + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Fail Count</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hFailCount" + "</th>" +
					"</tr>	" +

					"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Skipped Count</th>" +
					"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hSkippedCount" + "</th>" +
					"</tr>	" +

					//						"<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					//						"<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Avg API Response Time</th>" +
					//						"<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " + "hAPIResponseTime" + "</th>" +
					//						"</tr>	" +

					"</table>" +
					"<br /> ";

			writerOverall.write(strReport);
			writerOverall.newLine();


			strReport = "<table width=\"100%\" class=\"" + ScriptName + "\"  bgcolor=\"#82B6EA\" border=\"1\" margin=\"10,5,5,50\">" +
					"<tr>" +
					"<th width=\"8%\">Number</th>" +
					"<th width=\"30%\">Module Name</th>" +
					/*"<th width=\"8%\">Execution Start Time</th>" +
			 "<th width=\"8%\">Execution End Time</th>" +*/
					"<th width=\"8%\">TC Executed</th>" +
					"<th width=\"8%\">TC Passed</th>" +
					"<th width=\"8%\">TC Failed</th>" +
					"<th width=\"8%\">TC Skipped</th>" +
					//						"<th width=\"10%\">Exec Time (in min)</th>" +
					"<th width=\"20%\">Detailed Report</th>" +
					"</tr>" +
					"</table>";
			writerOverall.write(strReport);
			writerOverall.newLine();
		}
		catch (Exception ex) { 		}
	}


	public void logTCEnd_Overall()
	{
		try
		{
			writerOverall.write(" </body> </html>");
			writerOverall.newLine();
			writerOverall.close();
		}
		catch (Exception e) { 		   }
	}

	public void logTCEnd_Overall(String path)
	{
		try
		{
			fWriterOverall = new FileWriter(path);
			writerOverall = new BufferedWriter(fWriterOverall);
			writerOverall.write(" </body> </html>");
			writerOverall.newLine();
			writerOverall.close();
		}
		catch (Exception e) { 		   }
	}
	/**
	 * TODO Put here a description of what this method does.
	 *
	 */
	public void logTCDesc_Overall(String strModuleName, String strTotalExecuted, String strPass, String strFail, String strDetailedRptPath, String strSkipped, String strExecTime)
	{
		//scenarioStart = System.DateTime.Now;
		String strReport = null;
		try
		{
			strReport = "<table width=\"100%\" class=\"" + Constant.TESTSUITENAME + "\"  bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\">" +
					" <tr>" +
					"<td width=\"8%\" >" + intStepOverall + "</td>" +
					"<td width=\"30%\">" + strModuleName + "</td>" +
					/*"<td width=\"8%\" align=\"center\"> " + "Current Date Time" + "</td>" +
			 "<td width=\"8%\" align=\"center\"> " + "SEndTime" + " </td>" +*/
					"<td width=\"8%\" align=\"center\"> " + strTotalExecuted + " </td>" +
					"<td width=\"8%\" align=\"center\"> " + strPass + " </td>" +
					"<td width=\"8%\" align=\"center\"> " + strFail + " </td>" +
					"<td width=\"8%\" align=\"center\"> " + strSkipped + " </td>" +
					//					   "<td width=\"10%\" align=\"center\"> " + strExecTime + " </td>" +
					"<td width=\"20%\" align=\"center\"> <a href=\"file:///" + strDetailedRptPath +"#\" ID=\"One\" style=\"visibility:visible\">"  + "Click here for detailed report" + "</a> </td>" +
					"</tr>" +
					"</table>";

			writerOverall.write(strReport);
			writerOverall.newLine();

			intStepOverall++;
		}
		catch(Exception ex) { }
	}

	public void logTCDesc_Overall(String path,String strModuleName, String strTotalExecuted, String strPass, String strFail, String strDetailedRptPath, String strSkipped, String strExecTime,String countModule)
	{
		//scenarioStart = System.DateTime.Now;
		String strReport = null;

		try
		{

			fWriterOverall = new FileWriter(path,true);
			writerOverall = new BufferedWriter(fWriterOverall);

			strReport = "<table width=\"100%\" class=\"" + Constant.TESTSUITENAME + "\"  bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\">" +
					" <tr>" +
					"<td width=\"8%\" >" + countModule + "</td>" +
					"<td width=\"30%\">" + strModuleName + "</td>" +
					/*"<td width=\"8%\" align=\"center\"> " + "Current Date Time" + "</td>" +
			 "<td width=\"8%\" align=\"center\"> " + "SEndTime" + " </td>" +*/
					"<td width=\"8%\" align=\"center\"> " + strTotalExecuted + " </td>" +
					"<td width=\"8%\" align=\"center\"> " + strPass + " </td>" +
					"<td width=\"8%\" align=\"center\"> " + strFail + " </td>" +
					"<td width=\"8%\" align=\"center\"> " + strSkipped + " </td>" +
					//					   "<td width=\"10%\" align=\"center\"> " + strExecTime + " </td>" +
					"<td width=\"20%\" align=\"center\"> <a href=\"file:///" + strDetailedRptPath +"#\" ID=\"One\" style=\"visibility:visible\">"  + "Click here for detailed report" + "</a> </td>" +
					"</tr>" +
					"</table>";

			fWriterOverall.write(strReport);
			//fWriterOverall.newLine();
			fWriterOverall.close();
			intStepOverall++;
		}
		catch(Exception ex) { }
	}
	public void log_overAll_Close(){
		writerOverall.close();
	}

	//Need to rewamp this
	public String getTCStatusWithFormat(boolean tcStatus)
	{
		String s = "";
		try
		{
			if (tcStatus)
				s = "<font color=\"green\"><b>Passed</b></font>"
			else
				s =  "<font color=\"red\"><b>Failed</b></font>"

		}
		catch (Exception ex) {  }
		return s
	}

	public int getTCNumber(String strTCNumber) {
		String[] temp = strTCNumber.split("_")
		return temp[2].toInteger();
	}
	public boolean printInFile(List tcIDs, String strScriptName){
		boolean status = false;
		int i, j, ctIDFormated = 0;

		createExcelReport(strScriptName)

		List key = lhmTotalReport.keySet().asList()
		List value = lhmTotalReport.values().asList()
		List valueStatus = lhmTCStatus.values().asList()
		
		reportfileHeader=reportfileHeader.replaceAll("PASS_COUNT_REPLACEMENT", (PassCount/2).toString());
		reportfileHeader=reportfileHeader.replaceAll("FAIL_COUNT_REPLACEMENT", (FailCount/2).toString());		
		writer.write(reportfileHeader)
		writer.newLine()
		
		//XML Testlink Results
		List testlinkKeys = Constant.lhmTestlinkIDList.keySet().asList()
		List testlinkValues = Constant.lhmTestlinkIDList.values().asList()
		for(Iterator<String> tccc = testlinkKeys.iterator(); tccc.hasNext(); ) {
			String xmltcID = tccc.next();
			String xmlbuglink;
			xmlwriter.newLine();
			xmlwriter.newLine();
			xmlwriter.write("<testcase external_id=\"" + Constant.lhmTestlinkIDList.get(xmltcID) + "\">")
			xmlwriter.newLine();
			xmlwriter.write("<tester>automationuser</tester>")
			
			String testlinkResult = "p"
			if (lhmTCStatus.get(xmltcID) == false)
			testlinkResult = "f"
			xmlwriter.newLine();
			xmlwriter.write("<result>"+testlinkResult+"</result>")
			xmlwriter.newLine();
			xmlwriter.write("<notes>Tested the test case</notes>")
			
			if (Constant.lhmBugList.get(xmltcID) == "" || Constant.lhmBugList.get(xmltcID) == null || Constant.lhmBugList.get(xmltcID).contains("BUG-") == false)
			{				
				if(Constant.lhmBugList.get(xmltcID) != null)
					xmlbuglink = Constant.lhmBugList.get(xmltcID)
				else
					xmlbuglink = "--";
			}
			else
				xmlbuglink = "https://ariasystems.atlassian.net/browse/"+Constant.lhmBugList.get(xmltcID) 
				xmlwriter.newLine();
				xmlwriter.write("<jira_bug_id>"+xmlbuglink+"</jira_bug_id>")
				xmlwriter.newLine();
				xmlwriter.write("</testcase>")					
		}
		
		for(Iterator<String> k = tcIDs.iterator(); k.hasNext(); ) {
			String tcID = k.next();
			ctIDFormated = getTCNumber(tcID);
			if (j>0) {
				writer.write("<br /> </div>");
			}

			for (i=0; i<key.size(); i++) {
				if (getTCNumber(key[i]) == ctIDFormated) {
					if (key[i].toString().endsWith("_0")){
						value[i] = value[i].toString().replace("tcStatus" + ctIDFormated, getTCStatusWithFormat(valueStatus[j]))
					}
					writer.write(value[i]);
					writer.newLine();
					status = true;
				}
			}
			csvLogTCDetails(tcID, lhmTCDesc[tcID] , lhmTCStatus[tcID], lhmTCGenericException[tcID])
			j++;
		}
		csvClose();
		return status
	}

	public int incrementStepNo(int currentTCNo)
	{
		intStepNo[currentTCNo] = intStepNo[currentTCNo] + 1
		return intStepNo[currentTCNo]
	}

	public void totalPassorFailTC() {
		List valueStatus = lhmTCStatus.values().asList()

		for (int i = 0 ; i < valueStatus.size() ; i++) {
			if (valueStatus[i])
				PassCount++
			else
				FailCount++
		}
	}

	public String updateReportHeader(int totalTCCount) {

		freader = new FileReader(ResultFile);
		reader = new BufferedReader(freader);
		String line = "", s= "";
		int i=1
		while ((line = reader.readLine()) != null || i<4)
		{
			s = s +  line;
		}
		//while

		//s = s.replace("hStartTime", "hStartTime1");
		s = s.replace("hTestCaseCount", totalTCCount.toString());
		s = s.replace("hPassCount", PassCount.toString());
		s = s.replace("hFailCount", FailCount.toString());

		writer.write(s);

		return s;
	}

	public void updateModuleReportHeader(List replacements)
	{
		Common.stringReplacementInFile(ResultFile, replacements)
	}

	public void updateOverallReportHeader(List replacements)
	{
		logInfo("Result File Path " + ResultFileOverall)
		Common.stringReplacementInFile(ResultFileOverall, replacements)
	}

	public void updateOverallReportHeader(String path,List replacements)
	{
		ResultFileOverall = path;
		logInfo("Result File Path " + ResultFileOverall)
		Common.stringReplacementInFile(ResultFileOverall, replacements)
	}

	/******************* New methods to be added ************************
	 ******************************************************************/
	public void createExcelReport(String tsrScriptName){

		ResultFileCSV = sFilePath + tsrScriptName + ".csv";
		fWriterCSV = new FileWriter(ResultFileCSV);
		writerCSV = new BufferedWriter(fWriterCSV);

		writerCSV.writeLine("TestCase ID,Test Description,Actual Result,Expected Result,Status,Response Time, Test Case Result, Bug_ID");// first row third column

	}

	public void csvLogTCDetails(String tcID, String tcDesc, boolean tcStatus ,String tcGenericException) {
		if (tcStatus)
			writerCSV.writeLine(tcID+ "," + tcDesc + ","+ "Pass" + ","+ tcGenericException);
		else
			writerCSV.writeLine(tcID+ "," + tcDesc + ","+ "Fail" + ","+ tcGenericException);
	}


	public void logExcelTCDesc(String strTCNumber, String strTCDesc)
	{
		String strReport = "";
		int stepTCID = getTCNumber(strTCNumber)
		try
		{
			writerCSV.writeLine(strTCNumber+","+ strTCDesc + ","+ "" + ","+ "");

		}
		catch(Exception ex) { }
	}

	public void csvLogPassorFail(String strTCNumber, RESULT_TYPE enumResultType, String strReport, boolean status) {

		int stepTCID = getTCNumber(strTCNumber)-1
		try
		{
			if (status)
			{
				writerCSV.writeLine(""+","+enumResultType.toString() + " :: " + strReport + ","+ "" + ","+ "");
			} else {
				writerCSV.writeLine(""+","+enumResultType.toString() + " :: " + strReport + ","+ "" + ","+ "");

			}

		}
		catch (Exception e) {  }

	}

	public void csvLogResponseTime(String strTCNumber, RESULT_TYPE enumResultType, String strReport, boolean status,String responseTimeInMilliseconds ){
		int stepTCID = getTCNumber(strTCNumber)-1
		try
		{
			writerCSV.writeLine(""+","+"" + ","+ "" + ","+ ""+ ","+""+ ","+responseTimeInMilliseconds);
		}
		catch (Exception e) {

		}



	}
	public void logExcelPassorFail(String strTCNumber,RESULT_TYPE enumResultType, String strReport, boolean status,String actualResult,String expectedResult)
	{
		int stepTCID = getTCNumber(strTCNumber)-1
		try
		{
			if (status)
			{
				writerCSV.writeLine(""+","+"\""+ enumResultType.toString() + " :: " + strReport+"\"" + ","+ actualResult + ","+ expectedResult+ ","+"Pass");
			} else {
				writerCSV.writeLine(""+","+"\""+ enumResultType.toString() + " :: " + strReport+"\"" + ","+ actualResult + ","+ expectedResult+ ","+"Fail");

			}

		}
		catch (Exception e) {  }
	}

	public void logExcelPassorFailP2(String strTCNumber,RESULT_TYPE enumResultType, String strReport, boolean status,def actualResult,def expectedResult)
	{
		int stepTCID = getTCNumber(strTCNumber)-1
		try
		{
			if (status)
			{
				writerCSV.writeLine(""+","+"\""+ enumResultType.toString() + " :: " + strReport+"\"" + ","+ actualResult.toString() + ","+ expectedResult.toString()+ ","+"Pass");
			} else {
				writerCSV.writeLine(""+","+"\""+ enumResultType.toString() + " :: " + strReport+"\"" + ","+ actualResult.toString() + ","+ actualResult.toString()+ ","+"Fail");

			}

		}
		catch (Exception e) {  }
	}


	public void logExcelPassorFailRes(String strTCNumber,RESULT_TYPE enumResultType, String strReport, boolean status,String actualResult,String expectedResult,String resTime,String caseStatus)
	{
		int stepTCID = getTCNumber(strTCNumber)-1
		try
		{
			if(caseStatus.equalsIgnoreCase("true"))
				writerCSV.writeLine(strTCNumber+","+"\"\"" + ","+""+","+""+ ","+""+ ","+resTime+ ","+"Pass");
			else
				writerCSV.writeLine(strTCNumber+","+"\"\"" + ","+""+","+""+ ","+""+ ","+resTime+ ","+"Fail");

		}
		catch (Exception e) {  }
	}

	public void csvClose(){
		writerCSV.flush();
		writerCSV.close();
		fWriterCSV.close();
	}




	public void logPassorFailP2(String strTCNumber, RESULT_TYPE enumResultType, String strReport, boolean status)
	{
		int stepTCID = getTCNumber(strTCNumber)-1
		String failmessage = ""
		try
		{
			String veridscText = ""
			if (Constant.verificationDescription.size() > 0)
				veridscText = Constant.verificationDescription.get(Constant.verificationIndex)
			if (status)
			{
				//strReport = strReport.replace("Actual Value -", "<br><b>Actual Value -</b>")
				//strReport = strReport.replace("Expected Value -", "<br><b>Expected Value -</b>")
				strReport = strReport.replace("Actual Data -", "<br><b>Actual Data -</b>")
				strReport = strReport.replace("Expected Data -", "<b>Expected Data -</b>")
				strReport = strReport.replace("Actual Result -", "<br><b>Actual Result -</b>")
				strReport = strReport.replace("Expected Result -", "<br><hr size='1'><b>Expected Result -</b>")
				strReport =  "<TABLE ID=\"One\" class= \"" + strTCNumber + "\" bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\">" +
						" <TR> <TD width = \"8%\" >" +
						"StepNo:  " + intStepNo[stepTCID] + //intStep +
						"    </TD>       <TD width = \"92%\">" + veridscText+
						strReport + " </TD>        </TR></TABLE>" ;
			} else {
				//strReport = strReport.replace("Actual Value -", "<br><b>Actual Value -</b>")
				//strReport = strReport.replace("Expected Value -", "<br><b>Expected Value -</b>")
				strReport = strReport.replace("Actual Data -", "<br><b>Actual Data -</b>")
				strReport = strReport.replace("Expected Data -", "<b>Expected Data -</b>")
				strReport = strReport.replace("Actual Result -", "<br><b>Actual Result -</b>")
				strReport = strReport.replace("Expected Result -", "<br><hr size='1'><b>Expected Result -</b>")

				strReport = "<TABLE ID=\"One\" class= \"" +strTCNumber +  "\" bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White; color:red\">" +
						" <TR> <TD width = \"8%\" >" +
						"StepNo:  " +  intStepNo[stepTCID] + //intStep +
						"    </TD>       <TD width = \"92%\">" + veridscText+
						strReport + " </TD>        </TR></TABLE>" ;
				failmessage =strReport
			}
			//logInfo 'enumResultType outside loop : '+ enumResultType.toString()
			if((enumResultType.toString().contains("TEST_DATA")|| enumResultType.toString().contains("DB_COMPARISON")||enumResultType.toString().contains("METHOD_COMPARISON"))&& lhmTCStatusP2.containsKey(strTCNumber)){
				//logInfo 'enumResultType inside loop : '+ enumResultType.toString()
				//logInfo 'initstatus before update : '+ lhmTCStatusP2.getAt(strTCNumber)
				initStatus= lhmTCStatusP2.getAt(strTCNumber)
				//logInfo 'initstatus after update : '+ lhmTCStatusP2.getAt(strTCNumber)
				//logInfo 'case status  :: : :   : '+ (initStatus && status)
				lhmTCStatusP2.put strTCNumber, initStatus && status
			}
			else if (enumResultType.toString().contains("TEST_DATA") || enumResultType.toString().contains("DB_COMPARISON") || enumResultType.toString().contains("METHOD_COMPARISON") ){
				initStatus= true
				lhmTCStatusP2.put strTCNumber, initStatus && status
			}
			keyResult = strTCNumber.toString() + "_"+ intStepNo[stepTCID]
			valueResult = strReport
			lhmTotalReport.put keyResult, valueResult
			if (failmessage != "") lhmFailureReport.put keyResult, failmessage

			intStep++;
			incrementStepNo(stepTCID);

		}
		catch (Exception e) {  }
		//return (intStepNo[stepTCID])
	}

	//********************

	public void logTCEndP2(String strOverallReportPath, List tcIDs, String strScriptName, String[] testCase_Exec_Time_Array, List resultList) {
		try
		{
			totalPassorFailTCP2()
			printInFileP2(tcIDs, strScriptName, testCase_Exec_Time_Array,resultList)
			String strReport = "</div> <br /> <table width=\"100%\" border=\"0\" margin=\"10,5,5,50\"  >" +
					"<tr width=\"100%\">" +
					"<th width=\"90%\"> <a  href=\"file:///" + strOverallReportPath +"#\" > Back to Overall Report <a/></th>" +
					"<th width=\"10%\"> <a  href=\"#\" Target=\"_top\"> Go to Top <a/></th>" +
					"</tr> </table> <br /> ";

			writer.write(strReport);
			writer.newLine();

			writer.write(" </body> </html>");
			writer.newLine();
			writer.close();

			// Failure Seperation
			printFailureInFileP2(tcIDs, strScriptName, testCase_Exec_Time_Array )
			strReport = "</div> <br />";
			failwriter.write(strReport);
			failwriter.newLine();

			failwriter.write("</body> </html>");
			failwriter.newLine();
			failwriter.close()
			
			//XML testlink results
			xmlwriter.newLine();
			xmlwriter.newLine();
			xmlwriter.write("</results>");
			xmlwriter.close()
		}
		catch (Exception e)  {  }
	}
	
	

	//********************

	public void totalPassorFailTCP2() {
		List valueStatus = lhmTCStatusP2.values().asList()

		for (int i = 0 ; i < valueStatus.size() ; i++) {
			if (valueStatus[i])
				PassCount++
			else
				FailCount++
		}
	}

	//********************

	public boolean printInFileP2(List tcIDs, String strScriptName, String[] testCase_Exec_Time_Array, List replacementStr){
		boolean status = false;
		int i, j, ctIDFormated = 0;

		createExcelReport(strScriptName)
		
		List key = lhmTotalReport.keySet().asList()
		List value = lhmTotalReport.values().asList()
		List valueStatus = lhmTCStatusP2.values().asList()
		for(i=0 ; i<replacementStr.size() ; i++)
		{
			logInfo("i=" + i.toString())
			String[] replace = replacementStr.get(i).split("&");
			logInfo replace.toString()
			//To replace a line in a file
			reportfileHeader = reportfileHeader.replaceAll(replace[0], replace[1]);

		}
		reportfileHeader=reportfileHeader.replaceAll("PASS_COUNT_REPLACEMENT", (PassCount/2).toString());
		reportfileHeader=reportfileHeader.replaceAll("FAIL_COUNT_REPLACEMENT", (FailCount/2).toString());
		writer.write (reportfileHeader)
		writer.newLine()
		//logInfo "totalreport" +lhmTotalReport.toString()
		//logInfo "REPORT2 = "  +reportfileHeader
		//logInfo "tcIDs" +tcIDs.toString()
		//logInfo Constant.lhmBugList.toString()
		
		//XML Testlink Results
		List testlinkKeys = Constant.lhmTestlinkIDList.keySet().asList()
		List testlinkValues = Constant.lhmTestlinkIDList.values().asList()
		for(Iterator<String> tccc = testlinkKeys.iterator(); tccc.hasNext(); ) {
			String xmltcID = tccc.next();
			String xmlbuglink;
			xmlwriter.newLine();
			xmlwriter.newLine();
			xmlwriter.write("<testcase external_id=\"" + Constant.lhmTestlinkIDList.get(xmltcID) + "\">")
			xmlwriter.newLine();
			xmlwriter.write("<tester>automationuser</tester>")
			
			String testlinkResult = "p"
			if (lhmTCStatusP2.get(xmltcID) == false)
			testlinkResult = "f"
			xmlwriter.newLine();
			xmlwriter.write("<result>"+testlinkResult+"</result>")
			xmlwriter.newLine();
			xmlwriter.write("<notes>Tested the test case</notes>")
			
			if (Constant.lhmBugList.get(xmltcID) == "" || Constant.lhmBugList.get(xmltcID).contains("BUG-") == false)
			{
				if(Constant.lhmBugList.get(xmltcID).length()>0)
					xmlbuglink = Constant.lhmBugList.get(xmltcID)
				else
					xmlbuglink = "--";
			}
			else
				xmlbuglink = "https://ariasystems.atlassian.net/browse/"+Constant.lhmBugList.get(xmltcID)
			
				xmlwriter.newLine();
				xmlwriter.write("<jira_bug_id>"+xmlbuglink+"</jira_bug_id>")
				xmlwriter.newLine();
				xmlwriter.write("</testcase>")
		}

		
		for(Iterator<String> k = tcIDs.iterator(); k.hasNext(); ) {
			String tcID = k.next();
			ctIDFormated = getTCNumber(tcID);
			if (j>0) {
				writer.write("<br /> </div>");
			}

			for (i=0; i<key.size(); i++) {
				
				if (getTCNumber(key[i]) == ctIDFormated) {
					if (key[i].toString().endsWith("_0")){
						//.contains('_0')) {
						value[i] = value[i].toString().replace("tcStatus" + ctIDFormated, getTCStatusWithFormat(valueStatus[j]))
						value[i] = value[i].toString().replace("tcExecDuration" + ctIDFormated, testCase_Exec_Time_Array[j])
						if(Constant.lhmBugList.containsKey(tcID)){
							//if (!Constant.lhmBugList.get(tcID).isEmpty()){
							if (Constant.lhmBugList.get(tcID) == "" || Constant.lhmBugList.get(tcID).contains("BUG-") == false)
							{
								if(Constant.lhmBugList.get(tcID).length()>0)
									value[i] = value[i].toString().replace("tcException" + ctIDFormated, Constant.lhmBugList.get(tcID))
								else
									value[i] = value[i].toString().replace("tcException" + ctIDFormated, "--")
							}
							else
								value[i] = value[i].toString().replace("tcException" + ctIDFormated, "<a href=\"https://ariasystems.atlassian.net/browse/"+Constant.lhmBugList.get(tcID) + "\" target=\"_blank\">" + Constant.lhmBugList.get(tcID) + "</a>")
							//}
						}
					}
					writer.write(value[i]);
					writer.newLine();					
					status = true;
				}
			}
			csvLogTCDetails(tcID, lhmTCDesc[tcID] , lhmTCStatusP2[tcID], lhmTCGenericException[tcID])
			j++;
		}
		//writer.close()
		csvClose();
		return status
	}

	public boolean printFailureInFileP2(List tcIDs, String strScriptName, String[] testCase_Exec_Time_Array){
		boolean status = false;
		int i, j, ctIDFormated = 0;

		List key = lhmFailureReport.keySet().asList()
		List value = lhmFailureReport.values().asList()
		List valueStatus = lhmTCStatusP2.values().asList()

		for(Iterator<String> k = tcIDs.iterator(); k.hasNext(); ) {
			String tcID = k.next();
			ctIDFormated = getTCNumber(tcID);

			if (valueStatus[j] == false)
			{
				if (j>0) {
					failwriter.write("<br /> </div> <hr>");
				}
				for (i=0; i<key.size(); i++) {
					if (getTCNumber(key[i]) == ctIDFormated) {
						if (key[i].toString().contains('_0')) {
							value[i] = value[i].toString().replace("tcStatus" + ctIDFormated, getTCStatusWithFormat(valueStatus[j]))
							value[i] = value[i].toString().replace("tcExecDuration" + ctIDFormated, testCase_Exec_Time_Array[j])
							if(Constant.lhmBugList.containsKey(tcID)){
								//if (!Constant.lhmBugList.get(tcID).isEmpty()){
								logInfo "bug list" + Constant.lhmBugList.get(tcID)

								if (Constant.lhmBugList.get(tcID) == "" || Constant.lhmBugList.get(tcID).contains("BUG-") == false)
								{
									if(Constant.lhmBugList.get(tcID).length()>0)
										value[i] = value[i].toString().replace("tcException" + ctIDFormated, Constant.lhmBugList.get(tcID))
									else
										value[i] = value[i].toString().replace("tcException" + ctIDFormated, "--")


								}


								else
									value[i] = value[i].toString().replace("tcException" + ctIDFormated, "<a href=\"https://ariasystems.atlassian.net/browse/"+Constant.lhmBugList.get(tcID) + "\" target=\"_blank\">" + Constant.lhmBugList.get(tcID) + "</a>")
								//}
							}
						}
						failwriter.write(value[i]);
						failwriter.newLine();
						status = true;
					}
				}
			}

			j++;
		}

		return status
	}

	String replaceContractNoInQuery(String testId,String newQuery,String contract_no) {
		//if(newQuery.contains("<CREATE_ACCT_PLAN_CONTRACT_CONTRACTNO1>")||newQuery.contains("<CREATE_ACCT_MULTIPLAN_CONTRACT_CONTRACTNO>")||newQuery.contains("<CREATE_ACCT_UNIVERSAL_CONTRACTNO1>"))
			if(newQuery.contains("<CREATE_ACCT_PLAN_CONTRACT_CONTRACTNO1>"))
				return newQuery.toString().replaceAll('<CREATE_ACCT_PLAN_CONTRACT_CONTRACTNO1>',contract_no).toString()
			else if(newQuery.contains("<CREATE_ACCT_MULTIPLAN_CONTRACT_CONTRACTNO>"))
				return newQuery.toString().replaceAll('<CREATE_ACCT_MULTIPLAN_CONTRACT_CONTRACTNO>',contract_no).toString()
			else if(newQuery.contains("<CREATE_ACCT_UNIVERSAL_CONTRACTNO1>"))
				return newQuery.toString().replaceAll('<CREATE_ACCT_UNIVERSAL_CONTRACTNO1>',contract_no).toString()
		return newQuery
	}

	boolean scriptAssertP2(def context, /*com.eviware.soapui.support.XmlHolder holder,*/ LinkedHashMap m, String testCaseId) {
		logInfo "::: API Verification Begins :::"
		
		logInfo "Entire hash :: "+m.toString()

		Constant.mycontext=context
		VerificationMethods vm =new VerificationMethods()
		String acct_no= vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logInfo 'acct_no : ' + acct_no
		String child_acct_no= vm.getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String child2_acct_no= vm.getValueFromResponse('create_acct_complete.b',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String cch_acct_no1 = vm.getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO1)
		String cch_acct_no2 = vm.getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO2)
		String cch_acct_no3 = vm.getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO3)
		String cch_acct_no4 = vm.getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO4)
		String cch_acct_no5 = vm.getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO5)
		
		logInfo 'child_acct_no : ' + child_acct_no
		
		String contract_no =''
		String client_no=context.expand('${Properties#Client_No}')
		logInfo "Acct_no from properties => "+acct_no
		boolean assertFlag = true
		def expValue, expected, actual
		def nodeValue
		def tmp
		/*KO*/
		String strQuery
		RESULT_TYPE resultType
		ConnectDB db = new ConnectDB()
		/*KO*/
		List key = m.keySet().asList()
		
		//logInfo "Keys are :: "+key.toString()
		
		String[] tcID = testCaseId.split("-");
		List value = m.values().asList()
		//logInfo "Values are :: "+value.toString()

		ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL");
		String currentVT
		if(rs.next()) {
			currentVT = rs.getString(1);
		}
		logInfo 'Current VT during Verification : ' + currentVT

		for(int i=0; i<key.size(); i++) {
			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))

			List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
			List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()

			// To get the actual value
			// if the key is a DB query
			if(key.get(i).toString().contains('SELECT')) {
				String newQuery=key.get(i)
				if (key.get(i).toString().contains('<CREATE_ACCT_COMPLETE_ACCTNO1>')){
					newQuery=key.get(i).toString().replaceAll('<CREATE_ACCT_COMPLETE_ACCTNO1>',acct_no).toString()
					logInfo 'After replacing acct_no Actual-newQuery  : ' + newQuery
				}
				
				else if (key.get(i).toString().contains('<CREATE_ACCT_COMPLETE_CHILD_ACCTNO1>')){
					newQuery=key.get(i).toString().replaceAll('<CREATE_ACCT_COMPLETE_CHILD_ACCTNO1>',child_acct_no).toString()
					logInfo 'After replacing child_acct_no Actual-newQuery  : ' + newQuery
				}
				
			else if (key.get(i).toString().contains('<CREATE_ACCT_COMPLETE_CHILD2_ACCTNO1>')){
				newQuery=key.get(i).toString().replaceAll('<CREATE_ACCT_COMPLETE_CHILD2_ACCTNO1>',child2_acct_no).toString()
				logInfo 'After replacing child_acct_no Actual-newQuery  : ' + newQuery
			}
				else if (key.get(i).toString().contains('<CAH_ACCT_NO1>')){					
					newQuery=key.get(i).toString().replaceAll('<CAH_ACCT_NO1>',cch_acct_no1).toString()
				}
				else if (key.get(i).toString().contains('<CAH_ACCT_NO2>')){
					newQuery=key.get(i).toString().replaceAll('<CAH_ACCT_NO2>',cch_acct_no2).toString()
				}
				else if (key.get(i).toString().contains('<CAH_ACCT_NO3>')){
					newQuery=key.get(i).toString().replaceAll('<CAH_ACCT_NO3>',cch_acct_no3).toString()
				}
				else if (key.get(i).toString().contains('<CAH_ACCT_NO4>')){
					newQuery=key.get(i).toString().replaceAll('<CAH_ACCT_NO4>',cch_acct_no4).toString()
				}
				else if (key.get(i).toString().contains('<CAH_ACCT_NO5>')){
					newQuery=key.get(i).toString().replaceAll('<CAH_ACCT_NO5>',cch_acct_no5).toString()
				}
				
				if(testId.contains("CONTRACT")&& !(testCaseId.contains("create_acct_complete"))) {
					contract_no = context.expand('${Properties#ContractNo}')
					if (key.get(i).toString().contains('<CREATE_ACCT_PLAN_CONTRACT_CONTRACTNO1>'))
					{
					    contract_no= vm.getValueFromResponse(testId,'create_acct_plan_contract',ExPathRpcEnc.CREATE_ACCT_PLAN_CONTRACT_CONTRACTNO1)
						contract_no=(contract_no=='NoVal')?context.expand('${Properties#ContractNo}'):contract_no;
					}
					newQuery = replaceContractNoInQuery(testId, newQuery, contract_no)
				}

				strQuery = db.buildQuery(newQuery, client_no, getNodeValP2(testCaseId, newQuery))
				ResultSet resultSet = db.executePlaQuery(strQuery)
				if (resultSet.next()) {
					nodeValue = resultSet.getString(1)
				}
				else {
					nodeValue = 'No Actual Value returned from DB'
				}
				actual = strQuery
			}
			else if(key.get(i).toString().contains('md')) {
				//UsageVerificationMethods verifyMethods	= new UsageVerificationMethods(db)
				//KeyAPIsVerificationMethods verifyMethods	= new KeyAPIsVerificationMethods(db)
				//AdminToolsVerificationMethods verifyMethods = new AdminToolsVerificationMethods(db)
				//TaxationVerificationMethods verifyMethods = new TaxationVerificationMethods(db)
				//ServiceCreditsVerificationMethods verifyMethods = new ServiceCreditsVerificationMethods(db)
				DunningVerificationMethods verifyMethods = new DunningVerificationMethods(db)
				Class[] parametersTypes = new Class[1]
				parametersTypes[0] = String.class;

				Object[] parameters = new Object[1];
				parameters[0] = testCaseId
				try
				{
					Method method = verifyMethods.class.getMethod(key.get(i).toString(), parametersTypes)
					nodeValue = method.invoke(verifyMethods, parameters)
				}catch(Exception e)
				{
					logInfo("API Exception")
					logInfo (e.toString())
				}
				resultType = RESULT_TYPE.METHOD_COMPARISON
				actual = key.get(i)

			}
			//////////// UI //////////////////
			else if(key.get(i).toString().contains('uv')) {
				StatementTemplates verifyMethods = new StatementTemplates()
				Class[] parametersTypes = new Class[1]
				parametersTypes[0] = String.class;

				Object[] parameters = new Object[1];
				parameters[0] = testCaseId
				try
				{
					Method method = verifyMethods.class.getMethod(key.get(i).toString(), parametersTypes)
					nodeValue = method.invoke(verifyMethods, parameters)
				}catch(Exception e)
				{
					logInfo("API Exception")
					logInfo (e.toString())
				}
				resultType = RESULT_TYPE.METHOD_COMPARISON
				actual = key.get(i)

			}
			
			else if(key.get(i).toString().contains('manageauth_')) {
				ManageAuthKey verifyMethods = new ManageAuthKey()
				Class[] parametersTypes = new Class[1]
				parametersTypes[0] = String.class;

				Object[] parameters = new Object[1];
				parameters[0] = testCaseId
				try
				{
					Method method = verifyMethods.class.getMethod(key.get(i).toString(), parametersTypes)
					nodeValue = method.invoke(verifyMethods, parameters)
				}catch(Exception e)
				{
					logInfo("API Exception")
					logInfo (e.toString())
				}
				resultType = RESULT_TYPE.METHOD_COMPARISON
				actual = key.get(i)

			}
			/////////////////
			 else if(key.get(i).toString().contains('TD')) {
				nodeValue = key.get(i).toString().substring((key.get(i).toString().indexOf("-")+1),key.get(i).toString().length())
				actual = key.get(i)
			}

			else if(key.get(i).toString().contains('$')){
				nodeValue = context.expand(key.get(i))
				resultType = RESULT_TYPE.TEST_DATA_COMPARISON
				actual = key.get(i)
			}else if(key.get(i).toString().contains('ui#')){
				StatementTemplates st = new StatementTemplates()
				nodeValue = st.getValueFromUI(key.get(i).toString())
				actual = key.get(i).split('#')[1]
			}
			else if(key.get(i).toString().contains('exist#')){
				StatementTemplates st = new StatementTemplates()
				nodeValue = st.isElementFound(key.get(i).toString())
				actual = key.get(i).split('#')[1]
			}
			else if(key.get(i).toString().contains('attr-')){
				StatementTemplates st = new StatementTemplates()
				nodeValue = st.getAttributeValueFromUI(key.get(i).toString())
				actual = key.get(i).split('#')[1]
			}
			else if(key.get(i).toString().contains('text-')){
				StatementTemplates st = new StatementTemplates()
				nodeValue = st.getTextUIWithIndex(key.get(i).toString())
				actual = key.get(i).split('#')[1]
			}
			else {
				// if the key is xpath of the XML response
				for(int j=0; j<testCaseCmb.size(); j++) {
					if(testCaseCmb[j].equals(testCaseId)) {
						com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
						if(holder.containsKey(key.get(i)) && holder.containsValue(key.get(i))) {
							nodeValue = holder.getNodeValue( key.get(i) )
						} else {
							nodeValue = 'NO NODE'
						}
					}
				}
				actual = key.get(i)
			}

			// TO get the expected values
			if(value.get(i).toString().contains('$')){
				expValue = context.expand(value.get(i))
				resultType = RESULT_TYPE.TEST_DATA_COMPARISON
				expected = value.get(i)
				/*KO*/
			} else if(value.get(i).toString().contains('SELECT')) {
				String newQuery=value.get(i)
				if (value.get(i).toString().contains('<CREATE_ACCT_COMPLETE_ACCTNO1>')){
					newQuery=value.get(i).toString().replaceAll('<CREATE_ACCT_COMPLETE_ACCTNO1>',acct_no).toString()
					logInfo 'After replacing acct_no Expected-newQuery  : ' + newQuery
				}
				else if (value.get(i).toString().contains('<CREATE_ACCT_COMPLETE_CHILD_ACCTNO1>')){
					newQuery=value.get(i).toString().replaceAll('<CREATE_ACCT_COMPLETE_CHILD_ACCTNO1>',child_acct_no).toString()
					logInfo 'After replacing acct_no Expected-newQuery  : ' + newQuery
				}
				else if (value.get(i).toString().contains('<CREATE_ACCT_COMPLETE_CHILD2_ACCTNO1>')){
					newQuery=value.get(i).toString().replaceAll('<CREATE_ACCT_COMPLETE_CHILD2_ACCTNO1>',child2_acct_no).toString()
					logInfo 'After replacing acct_no Expected-newQuery  : ' + newQuery
				}
				else if (value.get(i).toString().contains('<CAH_ACCT_NO1>')){					
					newQuery=value.get(i).toString().replaceAll('<CAH_ACCT_NO1>',cch_acct_no1).toString()
				}
				else if (value.get(i).toString().contains('<CAH_ACCT_NO2>')){
					newQuery=value.get(i).toString().replaceAll('<CAH_ACCT_NO2>',cch_acct_no2).toString()
				}
				else if (value.get(i).toString().contains('<CAH_ACCT_NO3>')){
					newQuery=value.get(i).toString().replaceAll('<CAH_ACCT_NO3>',cch_acct_no3).toString()
				}
				else if (value.get(i).toString().contains('<CAH_ACCT_NO4>')){
					newQuery=value.get(i).toString().replaceAll('<CAH_ACCT_NO4>',cch_acct_no4).toString()
				}
				else if (value.get(i).toString().contains('<CAH_ACCT_NO5>')){
					newQuery=value.get(i).toString().replaceAll('<CAH_ACCT_NO5>',cch_acct_no5).toString()
				}
				if (value.get(i).toString().contains('<GCPA_MASTER_PLAN>')){
					logInfo 'Before replacing master plan  : ' + newQuery
					String mp= vm.getValueFromRequest('get_client_plans_all',"//parent_plan_no")
					newQuery=value.get(i).toString().replaceAll('<GCPA_MASTER_PLAN>',mp).toString()
					logInfo 'After replacing master plan  : ' + newQuery
				}
				if(testId.contains("CONTRACT")&& !(testCaseId.contains("create_acct_complete"))) {
					contract_no = context.expand('${Properties#ContractNo}')
					newQuery = replaceContractNoInQuery(testId, newQuery, contract_no)
				}
				boolean stflag=false
				if(newQuery.startsWith("ST#"))
				{
					stflag=true
					newQuery=newQuery.split("ST#")[1]
					
				}
				strQuery = db.buildQuery(newQuery, client_no, getNodeValP2(testCaseId, newQuery))				
				ResultSet resultSet = db.executePlaQuery(strQuery)
				if (resultSet.next()) {
					expValue = resultSet.getString(1)
					if (expValue != null)
					{
						if(!stflag)
						{
							//expValue=d.format(expValue.toDouble()).toString()
							if((expValue.contains("."))) {
								try {
									expValue=Float.parseFloat(expValue)
								}
								catch(Exception e) {
									logInfo("No Float number found")
								}
								logInfo("Cancat value : " + expValue)
							}
						}
						
					}
					
				}
				else {
					expValue = 'No Actual Value returned from DB'
				}
				resultType = RESULT_TYPE.DB_COMPARISON
				expected = strQuery
			}
			else if(value.get(i).toString().contains('md')) {
				//UsageVerificationMethods verifyMethods	= new UsageVerificationMethods(db)
				//KeyAPIsVerificationMethods verifyMethods	= new KeyAPIsVerificationMethods(db)
				//AdminToolsVerificationMethods verifyMethods = new AdminToolsVerificationMethods(db)
				//TaxationVerificationMethods verifyMethods = new TaxationVerificationMethods(db)
				//ServiceCreditsVerificationMethods verifyMethods = new ServiceCreditsVerificationMethods(db)
				DunningVerificationMethods verifyMethods = new DunningVerificationMethods(db)
				
				Class[] parametersTypes = new Class[1]
				parametersTypes[0] = String.class;

				Object[] parameters = new Object[1];
				parameters[0] = testCaseId
				try
				{
					Method method = verifyMethods.class.getMethod(value.get(i).toString(), parametersTypes)
					expValue = method.invoke(verifyMethods, parameters)
				}catch(Exception a)
				{
					logInfo "Exception"
					logInfo(a.getMessage());

				}
				resultType = RESULT_TYPE.METHOD_COMPARISON
				expected = value.get(i)
			}
			///////////////// UI ////////////////////
			else if(value.get(i).toString().contains('uv')) {
				StatementTemplates verifyMethods = new StatementTemplates()
				Class[] parametersTypes = new Class[1]
				parametersTypes[0] = String.class;

				Object[] parameters = new Object[1];
				parameters[0] = testCaseId
				try
				{
					Method method = verifyMethods.class.getMethod(value.get(i).toString(), parametersTypes)
					expValue = method.invoke(verifyMethods, parameters)
				}catch(Exception a)
				{
					logInfo "Exception"
					logInfo(a.getMessage());

				}
				resultType = RESULT_TYPE.METHOD_COMPARISON
				expected = value.get(i)
			}
			
			else if(value.get(i).toString().contains('manageauth_')) {
				ManageAuthKey verifyMethods = new ManageAuthKey()
				Class[] parametersTypes = new Class[1]
				parametersTypes[0] = String.class;

				Object[] parameters = new Object[1];
				parameters[0] = testCaseId
				try
				{
					Method method = verifyMethods.class.getMethod(value.get(i).toString(), parametersTypes)
					expValue = method.invoke(verifyMethods, parameters)
				}catch(Exception a)
				{
					logInfo "Exception"
					logInfo(a.getMessage());

				}
				resultType = RESULT_TYPE.METHOD_COMPARISON
				expected = value.get(i)
			}
			/////////////////////
				else if(value.get(i).toString().contains('TD')) {
				expValue = value.get(i).toString().substring((value.get(i).toString().indexOf("-")+1),value.get(i).toString().length())
				expected = value.get(i)
			} else if(value.get(i).toString().contains('$')){
				expValue = context.expand(value.get(i))
				resultType = RESULT_TYPE.TEST_DATA_COMPARISON
				expected = value.get(i)
			} else {
				expValue = value.get(i)
				resultType = RESULT_TYPE.TEST_DATA
				expected = value.get(i)
			}

			def logAssertFlag = logAssertP2(key.get(i), nodeValue, expValue)
			assertFlag = assertFlag && logAssertFlag
			Constant.verificationIndex = i

					String [] nodeValue_Array = nodeValue.toString().split(",")
					String [] expValue_Array = expValue.toString().split(",")
					int reporthashCount
					reporthashCount=nodeValue_Array.size()
					if (expValue_Array.size() > reporthashCount)
						reporthashCount = expValue_Array.size()

					//if (nodeValue.toString().startsWith("[") && nodeValue.toString().endsWith("]") && expValue.toString().startsWith("[") && expValue.toString().endsWith("]") && reporthashCount > 1)
					//
						logInfo "nodeValue class "+nodeValue.getClass()
						logInfo "expValue class "+expValue.getClass()
						if(nodeValue.getClass().toString().contains("Map") && nodeValue.getClass().toString().equals(expValue.getClass().toString()) )
						{					
							
						logInfo "HASH REPORTING"
						logInfo "Node value is a HASHMAP"					
						reporthashCount=nodeValue_Array.size()
						if (expValue_Array.size() > reporthashCount)
							reporthashCount = expValue_Array.size()
						logInfo "hash count: " + reporthashCount.toString()

						String hashreportnew = "<table style=\"border:1px solid black; border-collapse:collapse; background-color: Gainsboro \" border width =\"100%\"><tr><th style=\"border:1px solid black; \">Verification Parameter</th><th style=\"border:1px solid black;\">Expected</th><th style=\"border:1px solid black;\">Actual</th></tr>"
						def newnodevalue = ""
						def newexpvalue = ""
						String tableverifyItem=""
						def innernodevalue = ""
						def innerexpvalue = ""
						String innertableverifyItem=""
						String hashreptColor = ""
						
						nodeValue.each { k,v ->	
											
							tableverifyItem =k
							newnodevalue=nodeValue.get(k)
							newexpvalue=expValue.get(k)
							
							//logInfo ("newnodevalue class value :"+newnodevalue.getClass().toString())
							//logInfo ("newexpvalue class value :"+newexpvalue.getClass().toString())

							if(newnodevalue.getClass().toString().contains("Map") && newexpvalue.getClass().toString().contains("Map")){
								logInfo "Inside inner hash loop"
								hashreportnew = hashreportnew + "<tr><td style=\"border:1px solid black; background-color:Gainsboro\"> <font color=\"Blue\"><b>" + tableverifyItem + "</b></font></td></tr>"
								newnodevalue.each { k1,v1 ->
									innertableverifyItem =k1
									
									innernodevalue=newnodevalue.get(k1).toString()
									//logInfo "innernodevalue "+innernodevalue
									
									innerexpvalue=newexpvalue.get(k1).toString()
									//logInfo "newexpvalue "+innerexpvalue
									
									hashreptColor = "Tomato"
									if (innernodevalue.equals(innerexpvalue))
									{
										//logInfo "inner loop entered"
										hashreptColor = "Gainsboro"
									}
									
									//hashreportnew = hashreportnew + "<tr><td style=\"border:1px solid black; background-color: " + hashreptColor + "\">" + innertableverifyItem + "</td>"
									hashreportnew = hashreportnew + "<tr><td style=\"border:1px solid black; background-color: " + hashreptColor + "\">" + innertableverifyItem + "</td>"
									hashreportnew = hashreportnew + "<td style=\"border:1px solid black; background-color: " + hashreptColor + "\">" + innerexpvalue + "</td>"
									hashreportnew = hashreportnew + "<td style=\"border:1px solid black; background-color: " + hashreptColor + "\">" + innernodevalue + "</td></tr>"
								}
							}else{	
							logInfo "Inside inner hash else loop"
							newnodevalue=nodeValue.get(k).toString()
							//logInfo "newnodevalue "+newnodevalue
							
							newexpvalue=expValue.get(k).toString()				
							//logInfo "newexpvalue "+newexpvalue
							hashreptColor = "Tomato"
							if (newnodevalue.equals(newexpvalue))
							{
								logInfo "entered"
								hashreptColor = "Gainsboro"
							}
							
							hashreportnew = hashreportnew + "<tr><td style=\"border:1px solid black; background-color: " + hashreptColor + "\">" + tableverifyItem + "</td>"
							hashreportnew = hashreportnew + "<td style=\"border:1px solid black; background-color: " + hashreptColor + "\">" + newexpvalue + "</td>"
							hashreportnew = hashreportnew + "<td style=\"border:1px solid black; background-color: " + hashreptColor + "\">" + newnodevalue + "</td></tr>"
							}		
							}		
						
						hashreportnew = hashreportnew + "</table>"

				if (logAssertFlag){
					//logPassorFailP2(tcID[1], resultType, actual + " : " + expected + "<br>" + hashreportnew , true)
					logPassorFailP2(tcID[1], resultType, expected + " : " + actual + "<br>" + hashreportnew , true)
					logExcelPassorFailP2(tcID[1], resultType , actual + " " + expected + " Actual Value - " + nodeValue + " Expected Value - " + expValue , true,nodeValue,expValue)

				}
				else{
					//logPassorFailP2(tcID[1], resultType, actual + " : " + expected + "<br>" + hashreportnew , false)
					logPassorFailP2(tcID[1], resultType, expected + " : " + actual + "<br>" + hashreportnew , false)
					logExcelPassorFailP2(tcID[1], resultType , actual + " " + expected + " Actual Value - " + nodeValue + " Expected Value - " + expValue , false,nodeValue,expValue)
				}
			}
			else
			{

				if (logAssertFlag){
					//logPassorFailP2(tcID[1], resultType, actual + " " + expected + " Actual Value - " + nodeValue + " Expected Value - " + expValue , true)
					//logPassorFailP2(tcID[1], resultType, "Actual Data - " + actual + "Expected Data - " + expected + " Actual Result - " + nodeValue + " Expected Result - " + expValue , true)
					logPassorFailP2(tcID[1], resultType, "Expected Data - " + expected + "Actual Data - " + actual +  " Expected Result - " + expValue + " Actual Result - " + nodeValue, true)
					logExcelPassorFailP2(tcID[1], resultType , actual + " " + expected + " Actual Value - " + nodeValue + " Expected Value - " + expValue , true,nodeValue,expValue)

				}
				else{
					//logPassorFailP2(tcID[1], resultType, actual + " " + expected + " Actual Value - " + nodeValue + " Expected Value - " + expValue , false)
					//logPassorFailP2(tcID[1], resultType, "Actual Data - " + actual + "Expected Data - " + expected + " Actual Result - " + nodeValue + " Expected Result - " + expValue , false)
					logPassorFailP2(tcID[1], resultType, "Expected Data - " + expected + "Actual Data - " + actual +  " Expected Result - " + expValue + " Actual Result - " + nodeValue, false)
					logExcelPassorFailP2(tcID[1], resultType , actual + " " + expected + "Actual Value - " + nodeValue + " Expected Value - " + expValue , false,nodeValue,expValue)
				}

			}
		}
			return assertFlag
		
	}
	
	
	//For AdminTools
	
	void logHttpRequest(testStepName, com.eviware.soapui.impl.wsdl.panels.support.MockTestRunContext context)
	{

		String propValueList = new StringBuffer();
		logReqResInfo("Request for the API call - '" + testStepName + "'" + System.getProperty("line.separator"))
		// Print the Endpoint for the Request
		if (context.containsKey("requestUri") == true)
		{
			propValueList = context.get("requestUri")

		}
		logReqResInfo("Endpoint for the API Call: " + propValueList + System.getProperty("line.separator") + System.getProperty("line.separator"))
		if (context.containsKey("httpResponseProperties") == true)
		{
			propValueList = context.get("httpResponseProperties")

		}

		logReqResInfo("Request Parameter: " + propValueList + System.getProperty("line.separator") + System.getProperty("line.separator"))
	}
	
	/**
	 * Add the specified APIName as Key and its output XML response as Value to the specified LinkedHashMap and return the LinkedHashMap
	 * @param apiName - String API Name to be added as Key
	 * @param holder - Input XML request of the API to be added as Value
	 */
	LinkedHashMap addHttpInputRequest(String apiName, com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest holder) {
		Constant.lhmAllHttpInputRequest_Global.put apiName, holder
		return Constant.lhmAllHttpInputRequest_Global
	}
	
	
	boolean scriptHttpAssertP2(com.eviware.soapui.impl.wsdl.panels.support.MockTestRunContext context, /*com.eviware.soapui.support.XmlHolder holder,*/ LinkedHashMap m, String testCaseId) {
		logInfo ":::ADMINTOOLS API Verification Begins :::"

		Constant.mycontext=context
		VerificationMethods vm =new VerificationMethods()
		String acct_no= vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String contract_no =''
		String client_no=context.expand('${Properties#Client_No}')
		logInfo "Acct_no from properties => "+acct_no
		boolean assertFlag = true
		def expValue, expected, actual
		def nodeValue
		def tmp
		/*KO*/
		String strQuery
		RESULT_TYPE resultType
		ConnectDB db = new ConnectDB();
		/*KO*/
		List key = m.keySet().asList()
		String[] tcID = testCaseId.split("-");
		List value = m.values().asList()

		ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL");
		String currentVT
		if(rs.next()) {
			currentVT = rs.getString(1);
		}
		logInfo 'Current VT during Verification : ' + currentVT

		for(int i=0; i<key.size(); i++) {
			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))

			List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
			List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()

			List testCaseCmbRequest = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
			List requestValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
			logInfo(" Node Valueeee " + key.get(i).toString())
			// To get the actual value
			// if the key is a DB query
			if(key.get(i).toString().contains('SELECT')) {
				String newQuery=key.get(i)
				if (key.get(i).toString().contains('<CREATE_ACCT_COMPLETE_ACCTNO1>')){
					newQuery=key.get(i).toString().replaceAll('<CREATE_ACCT_COMPLETE_ACCTNO1>',acct_no).toString()
				}

				if(testId.contains("CONTRACT")&& !(testCaseId.contains("create_acct_complete"))) {
					contract_no = context.expand('${Properties#ContractNo}')
					newQuery = replaceContractNoInQuery(testId, newQuery, contract_no)
				}

				strQuery = db.buildQuery(newQuery, client_no, getNodeValP2(testCaseId, newQuery))
				ResultSet resultSet = db.executePlaQuery(strQuery)
				if (resultSet.next()) {
					nodeValue = resultSet.getString(1)
				}
				else {
					nodeValue = 'No Actual Value returned from DB'
				}
				actual = strQuery
			}
			else if(key.get(i).toString().contains('md')) {
				//UsageVerificationMethods verifyMethods   = new UsageVerificationMethods(db)
				AdminToolsVerificationMethods verifyMethods = new AdminToolsVerificationMethods(db)				
				Class[] parametersTypes = new Class[1]
				parametersTypes[0] = String.class;

				Object[] parameters = new Object[1];
				parameters[0] = testCaseId
				try
				{
					Method method = verifyMethods.class.getMethod(key.get(i).toString(), parametersTypes)
					nodeValue = method.invoke(verifyMethods, parameters)
				}catch(Exception e)
				{
					logInfo("API Exception")
					logInfo (e.toString())
				}
				resultType = RESULT_TYPE.METHOD_COMPARISON
				actual = key.get(i)

			} else if(key.get(i).toString().contains('TD')) {
				nodeValue = key.get(i).toString().substring((key.get(i).toString().indexOf("-")+1),key.get(i).toString().length())
				actual = key.get(i)
			}
			else if(key.get(i).toString().contains('$')){
				nodeValue = context.expand(key.get(i))
				resultType = RESULT_TYPE.TEST_DATA_COMPARISON
				actual = key.get(i)
			} else if(key.get(i).toString().contains('http')) {

				for(int k=0; k<testCaseCmbRequest.size(); k++) {
					if(testCaseCmbRequest[k].toString().equals(testCaseId)) {
						com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = requestValues[k]
						nodeValue = request.getPropertyValue(key.get(i).toString().substring((key.get(i).toString().indexOf("_")+1),key.get(i).toString().length()))
					}
				}

				resultType = RESULT_TYPE.TEST_DATA_COMPARISON
				actual = key.get(i)

			} else if(key.get(i).toString().contains('rs_')) {
				logInfo(" Node Valueeee " + key.get(i).toString())
				logInfo(" Node Valueeee " + key.get(i).toString().substring((key.get(i).toString().indexOf("rs_")+3),key.get(i).toString().length()))
				nodeValue = Constant.lhmResultSet.get(key.get(i).toString().substring((key.get(i).toString().indexOf("rs_")+3),key.get(i).toString().length()))
				resultType = RESULT_TYPE.TEST_DATA_COMPARISON
				actual = key.get(i)
			} else {
				// if the key is xpath of the XML response
				for(int j=0; j<testCaseCmb.size(); j++) {
					if(testCaseCmb[j].equals(testCaseId)) {
						com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
						if(holder.containsKey(key.get(i)) && holder.containsValue(key.get(i))) {
							nodeValue = holder.getNodeValue( key.get(i) )
						} else {
							nodeValue = 'NO NODE'
						}
					}
				}
				actual = key.get(i)
			}

			// TO get the expected values
			if(value.get(i).toString().contains('$')){
				expValue = context.expand(value.get(i))
				resultType = RESULT_TYPE.TEST_DATA_COMPARISON
				expected = value.get(i)
				/*KO*/
			} else if(value.get(i).toString().contains('SELECT')) {
				String newQuery=value.get(i)
				if (value.get(i).toString().contains('<CREATE_ACCT_COMPLETE_ACCTNO1>')){
					newQuery=value.get(i).toString().replaceAll('<CREATE_ACCT_COMPLETE_ACCTNO1>',acct_no).toString()
				}

				if(testId.contains("CONTRACT")&& !(testCaseId.contains("create_acct_complete"))) {
					contract_no = context.expand('${Properties#ContractNo}')
					newQuery = replaceContractNoInQuery(testId, newQuery, contract_no)
				}

				strQuery = db.buildQuery(newQuery, client_no, getNodeValP2(testCaseId, newQuery))
				ResultSet resultSet = db.executePlaQuery(strQuery)
				if (resultSet.next()) {
					expValue = resultSet.getString(1)
					if((expValue.contains("."))) {
						try {
							expValue=Float.parseFloat(expValue)
						}
						catch(Exception e) {
							logInfo("No Float number found")
						}
						logInfo("Cancat value : " + expValue)
					}
				}
				else {
					expValue = 'No Actual Value returned from DB'
				}
				resultType = RESULT_TYPE.DB_COMPARISON
				expected = strQuery
			}
			else if(value.get(i).toString().contains('md')) {
				//UsageVerificationMethods verifyMethods   = new UsageVerificationMethods(db)
				AdminToolsVerificationMethods verifyMethods = new AdminToolsVerificationMethods(db)				
				Class[] parametersTypes = new Class[1]
				parametersTypes[0] = String.class;

				Object[] parameters = new Object[1];
				parameters[0] = testCaseId
				try
				{
					Method method = verifyMethods.class.getMethod(value.get(i).toString(), parametersTypes)
					expValue = method.invoke(verifyMethods, parameters)
				}catch(Exception a)
				{
					logInfo "Exception"
					logInfo(a.getMessage());

				}
				resultType = RESULT_TYPE.METHOD_COMPARISON
				expected = value.get(i)
			}      else if(value.get(i).toString().contains('TD')) {
				expValue = value.get(i).toString().substring((value.get(i).toString().indexOf("-")+1),value.get(i).toString().length())
				expected = value.get(i)
			} else if(value.get(i).toString().contains('$')){
				expValue = context.expand(value.get(i))
				resultType = RESULT_TYPE.TEST_DATA_COMPARISON
				expected = value.get(i)
			} else if(value.get(i).toString().contains('rs_')) {
				expValue = Constant.lhmResultSet.get(value.get(i).toString().substring((value.get(i).toString().indexOf("rs_")+3),value.get(i).toString().length()))
				resultType = RESULT_TYPE.TEST_DATA_COMPARISON
				expected = value.get(i)
			} else {
				expValue = value.get(i)
				resultType = RESULT_TYPE.TEST_DATA
				expected = value.get(i)
			}

			def logAssertFlag = logAssertP2(key.get(i), nodeValue, expValue)
			assertFlag = assertFlag && logAssertFlag
			Constant.verificationIndex = i

			String [] nodeValue_Array = nodeValue.toString().split(",")
			String [] expValue_Array = expValue.toString().split(",")
			int reporthashCount
			reporthashCount=nodeValue_Array.size()
			if (expValue_Array.size() > reporthashCount)
				reporthashCount = expValue_Array.size()

			if (nodeValue.toString().startsWith("[") && nodeValue.toString().endsWith("]") && expValue.toString().startsWith("[") && expValue.toString().endsWith("]") && reporthashCount > 1)
			{
				logInfo "HASH REPORTING"
				nodeValue=nodeValue.toString().replace("[","")
				nodeValue=nodeValue.toString().replace("]","")
				expValue=expValue.toString().replace("[","")
				expValue=expValue.toString().replace("]","")

				nodeValue_Array = nodeValue.split(",")
				expValue_Array = expValue.split(",")
				reporthashCount=nodeValue_Array.size()
				if (expValue_Array.size() > reporthashCount)
					reporthashCount = expValue_Array.size()
				logInfo "hash count: " + reporthashCount.toString()

				String hashreportnew = "<table style=\"border:1px solid black; border-collapse:collapse; background-color: Gainsboro \" border width =\"100%\"><tr><th style=\"border:1px solid black; \">Verification Parameter</th><th style=\"border:1px solid black;\">Actual</th><th style=\"border:1px solid black;\">Expected</th></tr>"
				String newnodevalue = ""
				String newexpvalue = ""
				for (int hashchkIndex = 0; hashchkIndex<reporthashCount; hashchkIndex++)
				{
					String tableverifyItem = ""
					if (nodeValue_Array.size() > hashchkIndex && nodeValue_Array[hashchkIndex] != ":")
					{
						logInfo nodeValue_Array[hashchkIndex]
						newnodevalue = nodeValue_Array[hashchkIndex].split(":")[1]
						if(nodeValue_Array[hashchkIndex].contains("https"))
							newnodevalue = nodeValue_Array[hashchkIndex].split(":")[1] + ":" + (nodeValue_Array[hashchkIndex].split(":")[2])
						tableverifyItem = nodeValue_Array[hashchkIndex].split(":")[0]}
					else
						newnodevalue = "No data found"

					if (expValue_Array.size() > hashchkIndex &&  expValue_Array[hashchkIndex] != ":")
					{
						logInfo expValue_Array[hashchkIndex]
						newexpvalue = expValue_Array[hashchkIndex].split(":")[1]
						if(expValue_Array[hashchkIndex].contains("https"))
							newexpvalue = expValue_Array[hashchkIndex].split(":")[1] + ":" + (expValue_Array[hashchkIndex].split(":")[2])
						tableverifyItem = expValue_Array[hashchkIndex].split(":")[0]}
					else
						newexpvalue = "No data found"

					String hashreptColor = "Tomato"
					if (newnodevalue == newexpvalue)
						hashreptColor = "Gainsboro"
					hashreportnew = hashreportnew + "<tr><td style=\"border:1px solid black; background-color: " + hashreptColor + "\">" + tableverifyItem + "</td>"
					hashreportnew = hashreportnew + "<td style=\"border:1px solid black; background-color: " + hashreptColor + "\">" + newnodevalue + "</td>"
					hashreportnew = hashreportnew + "<td style=\"border:1px solid black; background-color: " + hashreptColor + "\">" + newexpvalue + "</td></tr>"
				}
				hashreportnew = hashreportnew + "</table>"

				if (logAssertFlag){
					logPassorFailP2(tcID[1], resultType, actual + " : " + expected + "<br>" + hashreportnew , true)
					//logPassorFailP2(tcID[1], resultType, "Actual Data - " + actual + "Expected Data - " + expected + " Actual Result - " + nodeValue + " Expected Result - " + expValue , true)
					logExcelPassorFailP2(tcID[1], resultType , actual + " " + expected + "  Actual Value - " + nodeValue + " Expected Value - " + expValue , true,nodeValue,expValue)

				}
				else{
					logPassorFailP2(tcID[1], resultType, actual + " : " + expected + "<br>" + hashreportnew , false)
					//logPassorFailP2(tcID[1], resultType, "Actual Data - " + actual + "Expected Data - " + expected + " Actual Result - " + nodeValue + " Expected Result - " + expValue , false)
					logExcelPassorFailP2(tcID[1], resultType , actual + " " + expected + "  Actual Value - " + nodeValue + " Expected Value - " + expValue , false,nodeValue,expValue)
				}
			}
			else
			{

				if (logAssertFlag)
				{
					//logPassorFailP2(tcID[1], resultType, actual + " " + expected + "  Actual Value - " + nodeValue + " Expected Value - " + expValue , true)
			        //logPassorFailP2(tcID[1], resultType, "Actual Data - " + actual + "Expected Data - " + expected + " Actual Result - " + nodeValue + " Expected Result - " + expValue , true)
					//logExcelPassorFailP2(tcID[1], resultType , actual + " " + expected + "  Actual Value - " + nodeValue + " Expected Value - " + expValue , true,nodeValue,expValue)
					
					logPassorFailP2(tcID[1], resultType, "Expected Data - " + expected + "Actual Data - " + actual +  " Expected Result - " + expValue + " Actual Result - " + nodeValue, true)
					logExcelPassorFailP2(tcID[1], resultType , actual + " " + expected + " Actual Value - " + nodeValue + " Expected Value - " + expValue , true,nodeValue,expValue)
				}
				else
				{
					//logPassorFailP2(tcID[1], resultType, actual + " " + expected + "  Actual Value - " + nodeValue + " Expected Value - " + expValue , false)
					//logPassorFailP2(tcID[1], resultType, "Actual Data - " + actual + "Expected Data - " + expected + " Actual Result - " + nodeValue + " Expected Result - " + expValue , false)
					//logExcelPassorFailP2(tcID[1], resultType , actual + " " + expected + "  Actual Value - " + nodeValue + " Expected Value - " + expValue , false,nodeValue,expValue)
					logPassorFailP2(tcID[1], resultType, "Expected Data - " + expected + "Actual Data - " + actual +  " Expected Result - " + expValue + " Actual Result - " + nodeValue, false)
					logExcelPassorFailP2(tcID[1], resultType , actual + " " + expected + " Actual Value - " + nodeValue + " Expected Value - " + expValue , false,nodeValue,expValue)
				}
			}

		}
		return assertFlag

	}
	
	void logHttpTestRequestResult(testRequest, com.eviware.soapui.impl.wsdl.panels.support.MockTestRunContext result) {
		this.testRequestStepResult = result

		if(testRequestStepResult.status.toString() == "FAILED"){
			Constant.FAILFLAG = true
			logInfo "Executed the test request - " + testRequest + " in " + testRequestStepResult.getTimeTaken().toString() + "\t" + ". Test Run Status : " + testRequestStepResult.status.toString() /*+ " Reason : " + testRequestStepResult.getError().toString()*/ + System.getProperty("line.separator");

			if(!(testRequestStepResult.hasResponse())) {
				logInfo "No Reponse been fetched"
			}
			//TODO Parse the response and get the error code and error message

		} else {
			logInfo "Executed the test request - " + testRequest + " in " + testRequestStepResult.getTimeTaken().toString() + ". Test Run Status : " + testRequestStepResult.status.toString() + System.getProperty("line.separator");
		}
	}

	}//class end

