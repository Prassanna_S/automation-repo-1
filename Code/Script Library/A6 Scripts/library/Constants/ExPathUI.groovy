package library.Constants

class ExPathUI {
	
	public static final ui_clientSelectXpath = "ui#//select[@id='clientSelector']"
	public static final ui_clientNameXpath = "ui#//header[@id='sitehead']/ul/li[1]/a"
	
	//106
	public static final ui_AccountNumber_106="ui#//div[@id='content']/table[1]/tbody/tr/td[3]/table/tbody/tr[1]/td"
	public static final ui_StatementDate_106="ui#//div[@id='content']/table[1]/tbody/tr/td[3]/table/tbody/tr[2]/td"
	public static final ui_StatementNumber_106="ui#//div[@id='content']/table[1]/tbody/tr/td[3]/table/tbody/tr[3]/td"
	public static final ui_InvoiceNumber_106="ui#//div[@id='content']/table[1]/tbody/tr/td[3]/table/tbody/tr[4]/td"
	public static final ui_BillingPeriod_106="ui#//div[@id='content']/table[1]/tbody/tr/td[3]/table/tbody/tr[5]/td"
	public static final ui_BalanceDue_106="ui#//div[@id='content']/table[1]/tbody/tr/td[3]/table/tbody/tr[6]/td"
	public static final ui_DueDate_106="ui#//div[@id='content']/table[1]/tbody/tr/td[3]/table/tbody/tr[7]/td"
	public static final ui_PreviousBalance="ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Previous Balance:')]/following-sibling::td[string-length()>1]"
	public static final ui_TotalCharges="ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Total Charges This Statement:')]/following-sibling::td[string-length()>1]"
	public static final ui_TotalCredits="ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Total Credits/Payments Received:')]/following-sibling::td[string-length()>1]"
	public static final ui_TotalBalanceDue="ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Total Balance Due:')]/following-sibling::td[string-length()>1]"
	public static final ui_Remit_ClientName_106 = "ui#//div[@id='content']/table[1]//tr[1]/td[ strong[contains(text(),'Remit')]]/text()[2]"
	static final ui_BalanceDue="ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Total Balance Due:')]/following-sibling::td[string-length()>1]"
	public static final ui_Client_Logo_106 = "attr-alt#//*[@id='logo']"
	public static final ui_ClientCsrEmail_106 = "ui#//*[@id='content']/p[2]/a"
	public static final ui_ClientUssUrl_106 = "ui#//*[@id='content']/p[4]/a"
	public static final ui_VoidedInvoiceItem_106="exist#//table//td[@class='lineitem' and contains(text(),'Voided Invoice')]"
	
	// Template 208
	public static final ui_ClientLogo_208 = "exist#//img[contains(@src,'NEWAsurion')]"
	public static final ui_PaymentsReceived_208="ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Payments Received:')]/following-sibling::td[string-length()>1]"
	public static final ui_BalanceTransferTotal_208="ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Balance Transfer Total:')]/following-sibling::td[string-length()>1]"
	public static final ui_Taxes_208="ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Taxes:')]/following-sibling::td[string-length()>1]"
	
	//Template 214
	public static final ui_AccountNumber_214="ui#//div[@id='content']/table//tr//td/strong[contains(text(),'Account Number: ')]/parent::td/following-sibling::td"
	public static final ui_InvoiceDate_214="ui#//div[@id='content']/table//tr//td/strong[contains(text(),'Invoice Date: ')]/parent::td/following-sibling::td"
	public static final ui_InvoiceNumber_214="ui#//div[@id='content']/table//tr//td/strong[contains(text(),'Invoice Number: ')]/parent::td/following-sibling::td"
	public static final ui_BalanceForward_214="ui#//div[@id='content']/table//tr//td/strong[contains(text(),'Balance Forward: ')]/parent::td/following-sibling::td"
	public static final ui_newBalance_214="ui#//div[@id='content']/table//strong[contains(text(),'New Balance:')]/../following-sibling::td"
	public static final ui_ElectronicStatementItem_214="exist#//div[@id='content']/table//tr/td[contains(text(),'Electronic Payment')]"
	public static final ui_VoidedInvoiceItem_214="exist#//table/tbody/tr//td[contains(text(),'Voided')]"
	
			
	// Template 222
	public static final ui_PONumber_222 = "ui#//div[@id='content']/table[1]/tbody/tr/td[2]/table/tbody/tr[1]/td[2]"
	public static final ui_invoiceNumber_222 = "ui#//div[@id='content']/table[1]/tbody/tr/td[2]/table/tbody/tr[2]/td[2]"
	public static final ui_invoiceDate_222 = "ui#//div[@id='content']/table[1]/tbody/tr/td[2]/table/tbody/tr[3]/td[2]"
	public static final ui_paymentTerms_222 = "ui#//div[@id='content']/table[1]/tbody/tr/td[2]/table/tbody/tr[4]/td[2]"
	public static final ui_dueDate_222 = "ui#//div[@id='content']/table[1]/tbody/tr/td[2]/table/tbody/tr[5]/td[2]"
	public static final ui_billingPeriod_222 = "ui#//div[@id='content']/table[1]/tbody/tr/td[2]/table/tbody/tr[6]/td[2]"
	public static final ui_billingAccountId_222 = "ui#//div[@id='content']/table[1]/tbody/tr/td[2]/table/tbody/tr[7]/td[2]"
	public static final ui_TotolAmountOwed_222 = "ui#//div[@id='content']/table[2]/tbody/tr[td[contains(text(),'Total Amount Owed')]]/td[4]"
	public static final ui_totalRecurringDiscountCredits_222="ui#//div[@id='content']/table[4]/tbody/tr[2]/td[2]"
	public static final ui_glCredit_222="ui#//div[@id='content']/table[4]/tbody/tr[1]/td[2]"
	public static final ui_gltotalsercdt_222="ui#//div[@id='content']/table[4]/tbody/tr[5]/td[2]"
	public static final ui_totalUsageDiscountCredits_222="ui#//div[@id='content']/table[4]/tbody/tr[6]/td[2]"
	public static final ui_totalRecurringAfterCredits_222="ui#//div[@id='content']/table[4]/tbody/tr[9]/td[2]"
	public static final ui_totalUsageAfterCredits_222="ui#//div[@id='content']/table[4]/tbody/tr[10]/td[2]"
	public static final ui_glOnlyDebit_222="ui#//div[@id='content']/table[4]/tbody/tr[11]/td[2]"
	public static final ui_glDebit_222="ui#//div[@id='content']/table[4]/tbody/tr[12]/td[2]"
	public static final ui_glOnlyBaseDebit_222="ui#//div[@id='content']/table[4]/tbody/tr[13]/td[2]"
	public static final ui_glOnlyTaxDebit_222="ui#//div[@id='content']/table[4]/tbody/tr[14]/td[2]"
	public static final ui_glTotalRecurringCharges_222="ui#//div[@id='content']/table[4]/tbody/tr[15]/td[2]"
	public static final ui_glTotalUsageCharges_222="ui#//div[@id='content']/table[4]/tbody/tr[16]/td[2]"
	public static final ui_glBaseDebit_222="ui#//div[@id='content']/table[4]/tbody/tr[17]/td[2]"
	public static final ui_glTaxDebit_222="ui#//div[@id='content']/table[4]/tbody/tr[18]/td[2]"
	public static final ui_glTotalCredits_222="ui#//div[@id='content']/table[4]/tbody/tr[4]/td[2]"
	public static final ui_totalCashAndSerCredits_222="ui#//div[@id='content']/table[4]/tbody/tr[8]/td[2]"
	
	// Template 201,1111111
	public static final ui_StatementNumber_201="text-1#//table[@id='customer-detail']//td[3]"
	public static final ui_InvoiceNumber_201="text-2#//table[@id='customer-detail']//td[3]"
	public static final ui_InvoiceDate_201="text-3#//table[@id='customer-detail']//td[3]"
	public static final ui_InvoiceServicePeriod_201="text-4#//table[@id='customer-detail']//td[3]"
	public static final ui_MaskedFormPymt_201="text-5#//table[@id='customer-detail']//td[3]"
	public static final  ui_PreviousBalance_201="ui#//*[@id='content']/table//td[text()='Previous Balance']/following-sibling::td[string-length()>1]"
	public static final ui_TotalCharges_201="ui#//div[@id='content']/table//td[text()='Total']/following-sibling::td[string-length()>1]"
	public static final ui_balance_201="ui#//*[@id='content']/table//td[text()='Balance']/following-sibling::td[string-length()>1]"
	public static final ui_AcctTotalOwed_202="ui#//*[@id='content']/table//td[contains(text(),'Amount')]/following-sibling::td[string-length()>1]"
	public static final ui_orderDate_201="ui#//div[@id='content']/table[4]//tr[3]/td[1]"
	public static final ui_orderClientReceiptId_201 ="ui#//div[@id='content']/table[4]//tr[3]/td[2]"
	public static final ui_clientOrderId_201 ="ui#//div[@id='content']/table[4]//tr[3]/td[3]"
	public static final ui_orderNumber_201="ui#//div[@id='content']/table[7]/tbody/tr/td[text()='Order No']/following-sibling::td[1]"
	public static final ui_orderTotal_201="ui#//div[@id='content']/table[7]/tbody/tr/td[text()='Order Total']/following-sibling::td[1]"
	public static final ui_orderComments_201="ui#//div[@id='content']/table[7]/tbody/tr/td[text()='Order Comments']/following-sibling::td[1]"
	// Template 211
	
	public static final ui_AccountNumber_211 = "ui#//table[1]//table[@class='oneborder']//tr[2]"
	public static final ui_InvoiceNumber_211 = "ui#//table[1]//table[@class='oneborder']//tr[4]"
	public static final ui_Date_211 = "ui#//table[1]//table[@class='oneborder']//tr[6]"
	
	// Template 222222
	public static final ui_CreditAmountForUsage_222222 = "ui#//div[@id='content']/table[4]/tbody/tr[6]/td[2]"
	// Template 219
	public static final ui_insertCurrentStmtTotalCredits_219 = "ui#//bill_data/payment_received"
	public static final ui_insertGLBalFwd_219 = "ui#//bill_data/open_balance"
	public static final ui_insertGLDebit_219 = "ui#//bill_data/total_renewal_due"
	public static final ui_insertAcctTotalOwed_219 = "ui#//bill_data/billing_totals/total_amount_due"
	public static final ui_insertGLDueDate_219 = "ui#//bill_data/billing_totals/payment_due_date"
	public static final ui_insertFormattedStmtID_219 = "ui#//bill_data/primary_member_details/statement_no"
	public static final ui_insertGLUsageFromDate_219 = "ui#//bill_data/billing_info/usage_from_date"
	public static final ui_insertGLUsageToDate_219 = "ui#//bill_data/billing_info/usage_to_date"
	public static final ui_insertGLUsageDateRange_219 = "ui#//bill_data/billing_info/usage_gl_date_range"

	public static final ui_insertTotalAccountOwed_222555 = "ui#//div[@id='content']/table[4]/tbody/tr/td[4]"	

	//Template 232
	public static final ui_FormattedStmtID_232 = "ui#//td[2]/table/tbody/tr[1]/td[2]"
	public static final ui_AcctUserID_232 = "ui#//td[2]/table/tbody/tr[2]/td[2]"
	public static final ui_GLBillDate_232 = "ui#//td[2]/table/tbody/tr[3]/td[2]"
	public static final ui_GLTaxDebit_232 = "ui#//table/tbody/tr[2]/td[3]"
	public static final ui_GLTotalDue_232 ="ui#//table/tbody/tr[3]/td[3]"
	public static final ui_GLOnlyBaseDebit_232 = "ui#//td/table/tbody/tr[1]/td[3]"
	
	// 10101010
	public static final ui_insertGLBaseDebit = "ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Total (excluding taxes):')]/following-sibling::td[string-length()>1]"
	public static final ui_insertGLTaxDebit = "ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Total taxes:')]/following-sibling::td[string-length()>1]"

	public static final ui_AcctTotalNewCharges = "ui#//div[@id='content']/table[3]/tbody/tr/td[2]/table/tbody/tr[1]/td[2]"
	public static final ui_AcctTotalNewEndBalance = "ui#//div[@id='content']/table[3]/tbody/tr/td[2]/table/tbody/tr[3]/td[2]"
	public static final ui_CurrentTotalWriteoff = "ui#//div[@id='content']/table[3]/tbody/tr/td[2]/table/tbody/tr[4]/td[2]"
	public static final ui_LastInvoiceAmount = "ui#//div[@id='content']/table[3]/tbody/tr/td[2]/table/tbody/tr[5]/td[2]"

	//209
	public static final ui_StatementTotal_209="ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Statement Total:')]/following-sibling::td[string-length()>1]"
	public static final  ui_Taxes_209="ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Taxes:')]/following-sibling::td[string-length()>1]"
	public static final ui_lineItems_209="exist#//tr[@class='lineitem']"
	public static final ui_insertPlanName_209="ui#//th[@class='branded']"
	//205
	public static final ui_customerName_205="ui#//div[@id='content']/table//table//td/strong[contains(text(),'Customer Number:')]/../../td[2] "
	public static final ui_statementID_205="ui#//div[@id='content']/table//table//td/strong[contains(text(),'Invoice Number:')]/../../td[2] "
	public static final ui_invoiceDate_205="ui#//div[@id='content']/table//table//td/strong[contains(text(),'Invoice Date:')]/../../td[2] "
	public static final ui_totalGST_205="ui#//div[@id='content']/table//table//td/strong[contains(text(),'Total (excl GST):')]/../../td[2]"
	public static final ui_totalGstAmtPayable_205="ui#//div[@id='content']/table//table//td/strong[contains(text(),'GST Amount Payable:')]/../../td[2]"
	public static final ui_totalAmtDueIncGst_205="ui#//div[@id='content']/table//table//td/strong[contains(text(),'Total Amount Due (incl GST):')]/../../td[2]"
	public static final ui_PaymentDueDate_205="ui#//div[@id='content']/table//table//td/strong[contains(text(),'Payment Due Date:')]/../../td[2]"
	public static final ui_invoiceValue_205="ui#//td/strong[contains(text(),'Invoice Value:')]//../../td[2]"
	public static final ui_currencyValue_205="ui#//td/strong[contains(text(),'Currency:')]//../../td[2]"

	//template 216
	public static final ui_GlDebit_216="ui#//div[@id='content']/table[2]/tbody/tr[10]/td"

	//credit template

	//XML
	public static final ui_previewStatementText="exist#//*[@id='page-instructions']/p[contains(text(),'You are viewing a preview of the account')]"

	public static final  ui_allCreditsElements="ui#//xhtml:html/xhtml:body/xhtml:pre"

	//template 106111
	public static final ui_ContractNo = "ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Contract No:')]/following-sibling::td[string-length()>1]"
	public static final ui_ContractEndDate = "ui#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Contract Ends On:')]/following-sibling::td[string-length()>1]"
	public static final ui_ContractNo_Expired  = "exist#//div[@id='content']/table/tbody/tr/td[@class='totals' and contains(text(),'Contract No:')]/following-sibling::td[string-length()>1]"

	//template 217

	public static final ui_GLBaseDebit="ui#//div[@id='content']/table[2]/tbody/tr[4]/td"
	public static final ui_GLTaxDebit="ui#//div[@id='content']/table[2]/tbody/tr[11]/td"
	public static final ui_AcctTotalNewPayments="ui#//div[@id='content']/table[2]/following::*//tr[2]/td[2]"

	//template 887766 dev3750
	public static final ui_TSummaryInvText = "ui#//div[@id='content']/table[2]/tbody/tr[2]/td[4]"
	public static final ui_TSummaryInvText_NO  = "exist#//div[@id='content']/table[2]/tbody/tr[2]/td[4]"
	
		
	// Template 222222
	//public static final ui_GLUsageDiscountCredits_222222 = "ui#//div[@id='content']/table[4]/tbody/tr[6]/td[2]"
	public static final ui_AcctTotalUnappCredits_222222 = "ui#//div[@id='content']/table[4]/tbody/tr[7]/td[2]"
	public static final ui_TtlCashAndServiceCrdts_222222 = "ui#//div[@id='content']/table[4]/tbody/tr[8]/td[2]"
	public static final ui_GLTotalCredits222222="ui#//div[@id='content']/table[4]/tbody/tr[4]/td[2]"


	
	//template 218
	public static final ui_Client_Name_Logo_218 = "attr-alt#//*[@id='overview-top']/div[1]/div/p[1]/img"
	public static final ui_AcctNo_218 = "ui#//div[@id='overview-top']/div[2]/ul/li[1]"
	public static final ui_GLInvNo_218 = "ui#//div[@id='overview-top']/div[2]/ul/li[2]"
	public static final ui_GLBillDate = "ui#//div[@id='overview-top']/div[2]/ul/li[3]"
	public static final ui_GLAdvanceDateRange_218 = "ui#//div[@id='overview-top']/div[2]/ul/li[4]"
	public static final ui_GLBalFwd_218 = "ui#//div[@id='overview-middle']/div/table/tbody/tr[1]/td[3]"
	public static final ui_ClientName_218  = "ui#//div[@id='overview-top']/div[2]/p/span[1]"
	public static final ui_AcctTotalOwed_218 ="ui#//ul[@id='charge-total']/li"
	public static final ui_insertGLDueDate_218  = "ui#//ul[@id='charge-subtotal']/li"
	public static final ui_GLDueDate_218 = "ui#//tr[@class = 'detail-total-due']/td[2]/span"
	public static final ui_insertAcctTotalOwed_218 = "ui#//tr[@class = 'detail-total-due']/td[3]"
	
	

}
