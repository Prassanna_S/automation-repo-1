package library

import java.awt.geom.Arc2D.Double;
import java.lang.reflect.Method
import java.sql.ResultSet;
import java.sql.ResultSetMetaData
import java.sql.SQLException
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import groovy.xml.StreamingMarkupBuilder

import org.apache.ivy.core.module.descriptor.ExtendsDescriptor;

import library.Constants.Constant;
import library.Constants.ExpValueConstants;
import library.Constants.ExPathRpcEnc;
import library.Constants.Constant.RESULT_TYPE;
import library.VerificationMethods
import library.ReadData

public class TaxationVerificationMethods extends KeyAPIsVerificationMethods
{
	LogResult logi = new LogResult(Constant.logFilePath)
	static ConnectDB db = null;
	public DecimalFormat df=new DecimalFormat("#.##")

	public TaxationVerificationMethods(ConnectDB db) {
		super(db)
	}
	
	
	/**
	 * Verifies Create Order with amount,discount amount and coupon with taxation from DB
	 * @param testCaseId,Invoice Sequence Number
	 * @return Calculated invoice charges HashMap
	 */
	//This method is in under development, yet to add more conditions and remove log commands
	def md_verifyCreateOrderTotalChargesFromDBcheck(String tcid)
	{
		com.eviware.soapui.support.XmlHolder outputholder
		logi.logInfo("md_verifyCreateOrderTotalChargesFromDBcheck")
		String apiname=tcid.split("-")[2]
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=Constant.mycontext.expand('${Properties#AccountNo}')
		def inv_no=getValueFromResponse('create_order',ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)
		String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String units = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNITS)
		String amounts = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_AMOUNTS)
		String units_discount_amounts = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNIT_DISCOUNT_AMOUNTS)
		String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
		DecimalFormat df = new DecimalFormat("#.##");
		String coupon_cd
		if (apiname == "create_order_with_plans")
			coupon_cd = getValueFromRequest(apiname,"//*[1]/*[local-name()='coupon_code']")
		else
			coupon_cd = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_COUPON_CD)
		//Ram
		def creditNo = getValueFromResponse('apply_service_credit',ExPathRpcEnc.APPLY_SERVICE_CREDIT_CREDITID1)
		logi.logInfo("Ram Service Credit " + creditNo.toString())
		//Ram
		logi.logInfo("Coupon Code applied is " +coupon_cd)
		ConnectDB db = new ConnectDB();
		logi.logInfo("units " + units.toString())
		def order_name=getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_ORDER_NAME)
		String rate,rate1
		def seqno,charges,credits,tax_rate,tax_charges,creditAmt,coupon_amt,coupon_amt_per,coupon_dr_inline_amt,coupon_dr_offset_amt,coupon_dr_inline_amt_per,coupon_dr_offset_amt_per
		def sum=0
		def couponAmt = getCouponAmtFromCreateOrder(tcid)
		logi.logInfo("couponAmttttttt " + couponAmt)
		String rate_qry= "Select INVIP.price from ariacore.inventory_item_prices INVIP  JOIN ariacore.INVENTORY_ITEMS INVI ON invip.item_no=invi.item_no and invip.client_no=invi.client_no where INVI.client_sku='"+order_name+"' and INVI.client_no="+client_no+" and invip.currency_cd=(SELECT CURRENCY_CD FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+ " AND CLIENT_NO="+client_no+")"
		//Ram
		String coupon_ind_query="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String coupon_ind = db.executeQueryP2(coupon_ind_query)
		logi.logInfo("coupon_ind " + coupon_ind)
		String coupon_type_query="SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String coupon_type = db.executeQueryP2(coupon_type_query)
		logi.logInfo("coupon_typeeee " + coupon_type)
		
		String coupon_DB_query="SELECT amount FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Coupon_cd = '"+coupon_cd+"' AND CLIENT_NO="+client_no+")) and FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='I'"
		def coupon_DB= db.executeQueryP2(coupon_DB_query)
		
		String coupon_DB_OFF_query="SELECT amount FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Coupon_cd = '"+coupon_cd+"' AND CLIENT_NO="+client_no+")) and FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='O'"
		def coupon_DB_OFF= db.executeQueryP2(coupon_DB_OFF_query)

		String creditreduces = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = "+client_no
		String creditreducesvalue = db.executeQueryP2(creditreduces)
		logi.logInfo("Client Credit reduces = "+creditreducesvalue)

		String external_tax_client = "SELECT METHOD_CD FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO=" + client_no
		external_tax_client = db.executeQueryP2(external_tax_client)
		logi.logInfo("External Client ID = " +external_tax_client)
		HashMap seqs=new HashMap()
		HashMap row = new HashMap();
	
		def debtqury="SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + inv_no + " AND Client_no = "+client_no+" AND SERVICE_NO NOT IN (400,401,402,403,405)"
		String total_charges=db.executeQueryP2(debtqury)
		double total_chargesd=total_charges.toDouble()
		logi.logInfo("Total Charge Before IN DB " + total_chargesd)
		def debtoutqury = "SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO = " + inv_no + " AND Client_no = "+client_no+" AND SERVICE_NO IN (400,401,402,403,405)"
		String total_tax=db.executeQueryP2(debtoutqury)
		double total_taxd=total_tax.toDouble()
		if(total_tax == null)
		{
		total_taxd = 0
		}
		logi.logInfo("Total Tax IN DB " + total_tax)
		def debtouttaxqury = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO = " + inv_no + " AND Client_no = "+client_no+""
		String char_after_tax = db.executeQueryP2(debtouttaxqury)
		double char_after_taxd=char_after_tax.toDouble()
		logi.logInfo("Total Charge After IN DB " + (char_after_taxd).toString())
			
		row.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(total_chargesd)).toString())
		row.put("TOTAL_TAX_CHARGES",(df.format(total_taxd)).toString())
		row.put("TOTAL_CHARGES_AFTER_TAX",(df.format(char_after_taxd)).toString())
		//row.put("TOTAL_CREDITS",df.format(outputholder.getNodeValue("//*[1]/*[local-name()='total_credit']")).toString())
		
		logi.logInfo("md_verifyCreateOrderTotalChargesFromDBcheck_hashresult=> "+row)
		return row.sort()
		//cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (tot_bef_tax).toString())
		
	}
	
	 /**
	 * Verifies Create order total chargers from API
	 * @param testCaseId
	 * @return Calculated Create Order Total Chargers Amount
	 */
	def md_verifyCreateOrderTotalChargesFromAPIcheck(String tcid)
	{
	logi.logInfo("md_verifyCreateOrderTotalChargesFromAPIcheck")
	List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
	List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
	HashMap cotc_api=new HashMap()
	com.eviware.soapui.support.XmlHolder outputholder
	DecimalFormat df = new DecimalFormat("#.##");
	String client_no=Constant.mycontext.expand('${Properties#Client_No}')
	def acct_no=Constant.mycontext.expand('${Properties#AccountNo}')
	def inv_no=getValueFromResponse('create_order',ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)
	String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
	for(int j=0; j<testCaseCmb.size(); j++) {
		if(testCaseCmb[j].toString().contains('create_order') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID)
		{
			double calc, calcDisc, calc1, calc2, calc3,state_tax,fed_tax,con_tax, plan_rates
			def creditAmt, coupon_type_value, couponDisBunYes_res, couponDisBunNo_res,taxAmt
			def units = getValueFromRequest('create_order',ExPathRpcEnc.IR_CREATE_ORDER_UNITS)
			units = units.toInteger()
			def amt = getValueFromRequest('create_order',ExPathRpcEnc.IR_CREATE_ORDER_AMOUNTS)
			amt = amt.toInteger()
			def discUnits = getValueFromRequest('create_order',ExPathRpcEnc.IR_CREATE_ORDER_UNIT_DISCOUNT_AMOUNTS)
			discUnits =discUnits.toInteger()
			def billImm = getValueFromRequest('create_order',ExPathRpcEnc.IR_CREATE_ORDER_BILL_IMMEDIATELY)
			billImm = billImm.toInteger()
			def couponCd = getValueFromRequest('create_order',ExPathRpcEnc.IR_CREATE_ORDER_COUPON_CD)
			def clientSku = getValueFromRequest('create_order',ExPathRpcEnc.IR_CREATE_ORDER_ORDER_NAME)
			def couponAmtFin = getCouponAmtFromCreateOrder(tcid)
			double couponAmt = couponAmtFin.toDouble()
			double tot_bef_tax, tot_aft_tax, tot_credit
			String couponCTFlat_res,couponCTPercentage_res,couponDRFlatOffset_res,couponDRFlatInline_res,couponDRPerOffset_res,couponDRPerInline_res, coupon_DB, coupon_DB_OFF,couponDisBunFla_res,couponDisBunPer_res
			String couponCTPercentage_fin_res,couponDRPercentage_off,couponDRPercentage_inline,finderOverlap_res
			logi.logInfo("Coupon amount is= " + couponAmtFin )
			
			ConnectDB db = new ConnectDB();
			
			String coupon_ind_query="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
			String coupon_ind = db.executeQueryP2(coupon_ind_query)
			
			String coupon_type_query="SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
			String coupon_type = db.executeQueryP2(coupon_type_query)
			
			String creditreduces = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = "+client_no
			String creditreducesvalue = db.executeQueryP2(creditreduces)
	
			String external_tax_client = "SELECT METHOD_CD FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO=" + client_no
			external_tax_client = db.executeQueryP2(external_tax_client)
			
			
			// For all tax calc
			List seqNum=[]
			def amt1 =0, taxes=0
			String invoice_no = "select invoice_no from ariacore.gl where client_no ="+client_no+ "and  acct_no =" +acct_no
			String Seq_Query =  "SELECT seq_num FROM ariacore.gl_detail WHERE invoice_no IN ("+inv_no+")AND service_no IN(400,401,402,403,405)"
			ResultSet rs=db.executePlaQuery(Seq_Query)
			while(rs.next())
			{
							   String Seq_no=rs.getString(1)
							   seqNum.add(Seq_no)
			}
			
			for(int k=0;k<seqNum.size();k++)
			{
																											
				String amount1= "select sum(debit) from ariacore.gl_tax_detail where invoice_no in("+inv_no+ ") and seq_num="+seqNum.get(k).toString()
				taxAmt=(db.executeQueryP2(amount1)).toDouble()
				logi.logInfo("Tax amount " + taxAmt)
				amt1 = amt1 + taxAmt
				logi.logInfo("total Tax amount " + (amt1).toString())
			}
			  taxes = df.format(amt1).toString()
			
											
			String couponCTFlat= "select flat_amount from ariacore.RECURRING_CREDIT_TEMPLATES where percent_amount is null and RECURRING_CREDIT_TEMPLATE_NO = (select RECURRING_CREDIT_TEMPLATE_NO from ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP where Client_no = "+client_no+" and Coupon_cd ='"+couponCd+"')"
			String couponCTPercentage = "select percent_amount from ariacore.RECURRING_CREDIT_TEMPLATES where flat_amount is null and RECURRING_CREDIT_TEMPLATE_NO = (select RECURRING_CREDIT_TEMPLATE_NO from ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP where Client_no = "+client_no+" and Coupon_cd ='"+couponCd+"')"
			String couponDRFlatOffset= "SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='O' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
			String couponDRFlatInline="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='I' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
			String couponDRPerOffset= "SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' and INLINE_OFFSET_IND='O' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
			String couponDRPerInline="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' and INLINE_OFFSET_IND='I' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
			String findOverlap="Select allow_overlap_ind from ariacore.client_discount_rule_bundles where bundle_no=(Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
			
			
			couponCTFlat_res=db.executeQueryP2(couponCTFlat)
			couponCTPercentage_res=db.executeQueryP2(couponCTPercentage)
			couponDRFlatOffset_res=db.executeQueryP2(couponDRFlatOffset)
			couponDRFlatInline_res=db.executeQueryP2(couponDRFlatInline)
			couponDRPerOffset_res=db.executeQueryP2(couponDRPerOffset)
			couponDRPerInline_res=db.executeQueryP2(couponDRPerInline)
			finderOverlap_res= db.executeQueryP2(findOverlap)
			
			logi.logInfo("Flat Amount = " +couponCTFlat_res)
			logi.logInfo("Percent Amount = " +couponCTPercentage_res)
			logi.logInfo("Flat Off = " +couponDRFlatOffset_res)
			logi.logInfo("Flat In  = " +couponDRFlatInline_res)
			logi.logInfo("Per Off = " +couponDRPerOffset_res)
			logi.logInfo("Per In = " +couponDRPerInline_res)
			logi.logInfo("Coupon Flat Yes = " +couponDisBunFla_res)
			logi.logInfo("Coupon Flat No = " +couponDisBunPer_res)
			logi.logInfo("Overlap Yes or No = " +finderOverlap_res)
			
					
			def line_units=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICEUNITS2)
			def line_rate_units=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICERATEPERUNIT2)
			
			if(couponCTFlat_res != null)
				{
					// Flat
					calc = units * amt
					calcDisc= units * discUnits
					tot_bef_tax = calc.toDouble() - calcDisc.toDouble() - couponAmt.toDouble()
					def tax =  taxes.toDouble()
					tot_aft_tax = tot_bef_tax + taxes.toDouble()
					tot_credit = calcDisc.toDouble() + couponAmt.toDouble()
					cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
					cotc_api.put("TOTAL_TAX_CHARGES",(df.format(tax)).toString())
					cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
				}
			else if(couponCTPercentage_res != null )
				{
					// Percentage
					calc = units * amt
					calcDisc= units * discUnits
					tot_bef_tax = calc.toDouble()  - calcDisc.toDouble() - couponAmt.toDouble()
					def tax =  taxes.toDouble()
					tot_aft_tax = tot_bef_tax.toDouble() + taxes.toDouble()
					tot_credit = calcDisc.toDouble() + couponAmt.toDouble()
					cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
					cotc_api.put("TOTAL_TAX_CHARGES",(df.format(tax)).toString())
					cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
				}
			else if(couponDRFlatOffset_res != null)
				{
					 //OFF line Flat	
					calc = units * amt
					calcDisc= units * discUnits
					tot_bef_tax = calc.toDouble() - couponAmt.toDouble() - calcDisc.toDouble()
					tot_aft_tax = tot_bef_tax.toDouble() + taxes.toDouble()
					def tax =  taxes.toDouble()
					tot_credit = calcDisc.toDouble()
					cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
					cotc_api.put("TOTAL_TAX_CHARGES",(df.format(tax)).toString())
					cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
				}
			else if(couponDRFlatInline_res != null)
				{
					// IN line Flat			
					calcDisc= units * discUnits
					calc = (units * amt) - couponAmt.toDouble() - calcDisc.toDouble()
					tot_bef_tax = calc.toDouble()
					def tax =  taxes.toDouble()
					tot_aft_tax = tot_bef_tax.toDouble() + taxes.toDouble()
					tot_credit = calcDisc.toDouble()
					cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
					cotc_api.put("TOTAL_TAX_CHARGES",(df.format(tax)).toString())
					cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
				}
			else if(couponDRPerOffset_res != null)
				{
					// Off line Percentage
					calc = units * amt
					calcDisc= units * discUnits
					tot_bef_tax = calc.toDouble() - couponAmt.toDouble() - calcDisc.toDouble()
					def tax =  taxes.toDouble()
					tot_aft_tax = tot_bef_tax.toDouble() + taxes.toDouble()
					logi.logInfo("Service Credit " + calcDisc)
					tot_credit = couponAmt.toDouble() + calcDisc.toDouble()
					cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
					cotc_api.put("TOTAL_TAX_CHARGES",(df.format(tax)).toString())
					cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
				}
			else if(couponDRPerInline_res != null)
				{
					double inlinevalue
					// In line Percentage
					calc = units * amt 
					calcDisc= units * discUnits 
					inlinevalue = calc - couponAmt
					tot_bef_tax = calc.toDouble() - calcDisc.toDouble() - inlinevalue.toDouble()
					def tax =  taxes.toDouble()
					tot_aft_tax =  tot_bef_tax.toDouble() + taxes.toDouble()
					tot_credit = calcDisc.toDouble()
					cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
					cotc_api.put("TOTAL_TAX_CHARGES",(df.format(tax)).toString())
					cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
				}
				
				if (finderOverlap_res == 'Y')
				{
					//Discount bundle Offset
				    calc = (units * amt)
					calcDisc= units * discUnits
					tot_bef_tax = calc.toDouble() - couponAmt.toDouble() - calcDisc.toDouble()
					logi.logInfo("Before" + tot_bef_tax)
					def tax =  taxes.toDouble()
					tot_aft_tax =  tot_bef_tax.toDouble() + taxes.toDouble()
					tot_credit = calcDisc.toDouble()
					cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
					cotc_api.put("TOTAL_TAX_CHARGES",(df.format(tax)).toString())
					cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
				}
				
				if (finderOverlap_res == 'N')
				{
					//Discount bundle Offset
					String taxQuery = "select sum(debit) from ariacore.gl_tax_detail where invoice_no in("+inv_no+ ") and CLIENT_NO="+client_no
					String taxFin = db.executeQueryP2(taxQuery)
					double wholeTax=taxFin.toDouble()
					String coupon_DisBun_Flat="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select RULE_NO FROM ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+client_no+" and coupon_cd = '"+couponCd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='F'"
					String coupon_DisBun_Per="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+client_no+" and coupon_cd = '"+couponCd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='P'"
					couponDisBunFla_res=db.executeQueryP2(coupon_DisBun_Flat)
					couponDisBunPer_res=db.executeQueryP2(coupon_DisBun_Per)
					calc = (units * amt)
					calcDisc= units * discUnits
					tot_bef_tax = calc.toDouble() - couponAmt.toDouble() - calcDisc.toDouble()
					tot_aft_tax =  tot_bef_tax.toDouble() + wholeTax.toDouble()
					tot_credit = calcDisc.toDouble()
					cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
					cotc_api.put("TOTAL_TAX_CHARGES",(df.format(wholeTax)).toString())
					cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
				}
			outputholder = xmlValues[j]
			
		}
		
		if (cotc_api.get("TOTAL_TAX_CHARGES").toString() == "null")
			{
			//cotc_api.put("TOTAL_TAX_CHARGES","0")
			logi.logInfo("Finish")
			}
		else if (cotc_api.get("TOTAL_CREDITS").toString() == "null")
		{
			logi.logInfo("Finish")
			//cotc_api.put("TOTAL_CREDITS","0")
		}
	}
	logi.logInfo("md_verifyCreateOrderTotalChargesFromAPIcheck_hashresult=> "+cotc_api)
	return cotc_api.sort();
}
	
	HashMap md_verifyCreateOrderInvoiceDetailsFromDBTax1(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDBTax(tcid,"1")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDBTax2(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDBTax(tcid,"2")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDBTax3(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDBTax(tcid,"3")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDBTax4(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDBTax(tcid,"4")
	}
	
	HashMap md_verifyCreateOrderInvoiceDetailsFromDBTax5(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDBTax(tcid,"5")
	}
	
	/**
	 * Verifies Create Order Invoice Details from DB with unit, unitamount, discount amount and internal tax 
	 * @param testCaseId,Invoice Sequence Number
	 * @return Calculated invoice charges HashMap
	 */
	
	def md_verifyCreateOrderInvoiceDetailsFromDBTax(String tcid, String seq_no)
	{
		logi.logInfo("md_verifyCreateOrderInvoiceDetailsFromDB")
		String apiname=tcid.split("-")[2]
		logi.logInfo("API Name:"+apiname)
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=Constant.mycontext.expand('${Properties#AccountNo}')
		def inv_no=getValueFromResponse(apiname,ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)
		String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		def creditNo = getValueFromResponse('apply_service_credit',ExPathRpcEnc.APPLY_SERVICE_CREDIT_CREDITID1)
		String units = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNITS)
		String amounts = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_AMOUNTS)
		String units_discount_amounts = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNIT_DISCOUNT_AMOUNTS)
		logi.logInfo("line units " + units.toString())
		logi.logInfo("line amounts " + amounts.toString())
		logi.logInfo("line units_discount_amounts " + units_discount_amounts.toString())

		def order_name=getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_ORDER_NAME)
		String coupon_cd
		if (apiname == "create_order_with_plans")
		{
			coupon_cd = getValueFromRequest(apiname,"//*[1]/*[local-name()='coupon_code']")
			if(coupon_cd=="NoVal")
				coupon_cd = getValueFromRequest(apiname,"//*[1]/*[local-name()='coupon_codes']")
		}
		else
			coupon_cd = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_COUPON_CD)
		String rate
		def line_no, service_name, charges, creditAmt,couponActServiceAmt
		HashMap hm_coid= new HashMap();
		ConnectDB db = new ConnectDB();
		String unapplied_amt="SELECT NVL(debit,0) from ariacore.gl_detail where invoice_no="+inv_no+" and service_no=0"
		unapplied_amt = db.executeQueryP2(unapplied_amt)
		logi.logInfo("unapplied_amt :"+unapplied_amt)
		String coupon_ind_query="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String coupon_ind = db.executeQueryP2(coupon_ind_query)
		String coupon_type_query="SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String coupon_type = db.executeQueryP2(coupon_type_query)

		String rate_qry= "Select INVIP.price from ariacore.inventory_item_prices INVIP JOIN ariacore.INVENTORY_ITEMS INVI ON invip.item_no=invi.item_no and invip.client_no=invi.client_no where INVI.client_sku='"+order_name+"' and INVI.client_no="+client_no+" and invip.currency_cd=(SELECT CURRENCY_CD FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+ " AND CLIENT_NO="+client_no+")"
		rate = db.executeQueryP2(rate_qry);
		String coupon_DR_per_query="SELECT amount FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String coupon_DR_per= db.executeQueryP2(coupon_DR_per_query)
		logi.logInfo("coupon_DR_per" + coupon_DR_per)

		String coupon_DB_query="SELECT amount FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Coupon_cd = '"+coupon_cd+"' AND CLIENT_NO="+client_no+")) and FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='I'"
		String coupon_DB= db.executeQueryP2(coupon_DB_query)

		String creditreduces = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = "+client_no
		String creditreducesvalue = db.executeQueryP2(creditreduces)
		logi.logInfo("Client Credit reduces = "+creditreducesvalue)

		def couponAmt = getCouponAmtFromCreateOrder(tcid)		

		String creditAmtQuery = "select NVL(amount,0) from ariacore.all_credits where acct_no="+acct_no+" and credit_type='S' and credit_id="+creditNo
		if(creditNo !="NoVal")
		{			
			creditAmt=db.executeQueryP2(creditAmtQuery)
			logi.logInfo("Credit Amount "+creditAmt)
		}

		if ((!units.equals("NoVal")) && amounts.equals("NoVal") && units_discount_amounts.equals("NoVal") && (seq_no.equals("1") || seq_no.equals("5") || seq_no.equals("9")))
		{
			logi.logInfo("In Loop IF")
			if(coupon_ind.equals("I"))
			{
				if(coupon_type.equals("P"))
				{
					def cou_rate= df.format((rate.toDouble() * (coupon_DR_per.toDouble()/100))).toString()
					logi.logInfo("cou_rate "+cou_rate.toDouble())
					rate = (rate.toDouble() -(cou_rate.toDouble()))
					logi.logInfo("cou_rate1111 "+rate)
					charges =(units.toDouble() * rate.toDouble())
					logi.logInfo("cou_rate1111_charges "+charges)
					charges= df.format(charges.toDouble()).toString()
				}
				else
					charges= ((units.toInteger() * rate.toDouble())-(couponAmt.toDouble()))
			}
			else
				charges= (units.toInteger()) * rate.toDouble()
			if(coupon_DB!=null)
				charges= ((units.toInteger() * rate.toDouble())-(coupon_DB.toDouble()))

			hm_coid.put("LINE_UNITS",units)
			hm_coid.put("RATE_PER_UNITS",df.format(rate.toDouble()).toString())
			hm_coid.put("LINE_AMOUNT",df.format(charges.toDouble()).toString())
		}
		else if((!units.equals("NoVal")) && amounts.equals("NoVal") && units_discount_amounts.equals("NoVal") && (!coupon_cd.equals("NoVal")) && seq_no!= "1")
		{
			logi.logInfo("In loop Else")
			if(coupon_type.equals("P"))
			{
				hm_coid.put("LINE_UNITS",units)
				hm_coid.put("RATE_PER_UNITS",df.format(-(rate.toDouble() * (coupon_DR_per.toDouble()/100))).toString())
				hm_coid.put("LINE_AMOUNT",df.format(-(couponAmt.toDouble())).toString())
			}
			else
			{
				hm_coid.put("LINE_UNITS","1")
				hm_coid.put("RATE_PER_UNITS",df.format(-(couponAmt.toDouble())).toString())
				hm_coid.put("LINE_AMOUNT",df.format(-(couponAmt.toDouble())).toString())
			}

		}
		else if ((!units.equals("NoVal")) && (!amounts.equals("NoVal")) && (units_discount_amounts.equals("NoVal")) && (seq_no.equals("1") || seq_no.equals("5") || seq_no.equals("9")))
		{
			logi.logInfo("In loop else 2")
			rate = amounts
			charges= (units.toInteger()) * rate.toDouble()

			hm_coid.put("LINE_UNITS",units)
			hm_coid.put("RATE_PER_UNITS",rate)
			hm_coid.put("LINE_AMOUNT",df.format(charges.toDouble()).toString())
		}
		else if ((!units.equals("NoVal")) && (amounts.equals("NoVal")) && (!units_discount_amounts.equals("NoVal")) && (seq_no.equals("1") || seq_no.equals("5") || seq_no.equals("9")))
		{
			logi.logInfo("In loop else 3")
			charges= (units.toInteger()) * rate.toDouble()
			hm_coid.put("LINE_UNITS",units)
			hm_coid.put("RATE_PER_UNITS",rate)
			hm_coid.put("LINE_AMOUNT",df.format(charges.toDouble()).toString())
		}
		else if ((!units.equals("NoVal")) && (!amounts.equals("NoVal")) && (!units_discount_amounts.equals("NoVal")) && (seq_no.equals("1") || seq_no.equals("5") || seq_no.equals("9")))
		{
			logi.logInfo("In loop else 44444")
			rate = amounts
			if(coupon_ind.equals("I"))
			{
				if(coupon_type.equals("P"))
				{
					def cou_rate= df.format((rate.toDouble() * (coupon_DR_per.toDouble()/100))).toString()
					logi.logInfo("cou_rate "+cou_rate.toDouble())
					rate = (rate.toDouble() -(cou_rate.toDouble()))
					logi.logInfo("cou_rate1111 "+rate)
					charges =(units.toDouble() * rate.toDouble())
					logi.logInfo("cou_rate1111_charges "+charges)
					charges= df.format(charges.toDouble()).toString()
				}
				else
					charges= ((units.toInteger() * rate.toDouble())-(couponAmt.toDouble()))
			}
			else
				charges= (units.toInteger()) * rate.toDouble()
							
			hm_coid.put("LINE_UNITS",units)
			hm_coid.put("RATE_PER_UNITS",df.format(rate.toDouble()).toString())
			hm_coid.put("LINE_AMOUNT",df.format(charges.toDouble()).toString())
		}

		hm_coid.put("LINE_NO", seq_no)
		String qry = "Select service_no,comments as description from ariacore.gl_detail where invoice_no="+inv_no+" and seq_num="+seq_no
		ResultSet rs_qry = db.executePlaQuery(qry);
		ResultSetMetaData md = rs_qry.getMetaData();
		int columns = md.getColumnCount();
		while (rs_qry.next()){
			for(int i=1; i<=columns; i++){
				hm_coid.put(md.getColumnName(i),rs_qry.getObject(i));
			}
		}
		rs_qry.close();

		if ((!units.equals("NoVal")) &&  (!units_discount_amounts.equals("NoVal")) && ((!seq_no.equals("1")) && (!seq_no.equals("5")) && (!seq_no.equals("9"))))
		{
			charges= -(units.toInteger() * units_discount_amounts.toInteger())
			hm_coid.put("LINE_AMOUNT",df.format(charges.toDouble()).toString())
			hm_coid.put("LINE_UNITS","0")
			hm_coid.put("RATE_PER_UNITS","0")
		}


		if((hm_coid.get("SERVICE_NO").toString() != "0"))
		{
			String qry1 = "select service_name,taxable_ind as service_is_tax_ind from ariacore.all_service where service_no="+hm_coid.get("SERVICE_NO")+" and client_no="+client_no
			ResultSet rs = db.executePlaQuery(qry1);
			ResultSetMetaData rsmd = rs.getMetaData();
			int columns1 = rsmd.getColumnCount();
			while (rs.next())
			{
				for(int j=1; j<=columns1; j++)
				{
					hm_coid.put(rsmd.getColumnName(j),rs.getObject(j));
				}
			}
			rs.close();
			String external_tax_client = db.executeQueryP2("SELECT * FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO=" + client_no)
			if((creditreducesvalue!=null) || (external_tax_client != null))
			{
				hm_coid.put("SERVICE_IS_TAX_IND","0")
			}

			if((couponAmt!= null) && (units_discount_amounts.equals("NoVal")) && (apiname == "create_order_with_plans") && (!coupon_cd.equals("NoVal")) && (!seq_no.equals("1")) && (!seq_no.equals("5"))&& (!seq_no.equals("9")))
			{
				hm_coid.put("LINE_AMOUNT",df.format(-(couponAmt.toDouble())).toString())
				hm_coid.put("SERVICE_IS_TAX_IND","0")
				hm_coid.put("LINE_UNITS","1")
				hm_coid.put("RATE_PER_UNITS",df.format(-(couponAmt.toDouble())).toString())
			}
		}

		if((unapplied_amt!= null) && (units_discount_amounts.equals("NoVal")) && (coupon_cd.equals("NoVal")) && ((!seq_no.equals("1")) && (!seq_no.equals("5")) && (!seq_no.equals("9"))))
		{

			logi.logInfo("Unapplied amount : "+unapplied_amt)
			hm_coid.put("LINE_AMOUNT",df.format((unapplied_amt.toDouble())).toString())
			hm_coid.put("SERVICE_NAME","Account Credit")
			hm_coid.put("SERVICE_IS_TAX_IND","0")
			hm_coid.put("LINE_UNITS","0")
			hm_coid.put("RATE_PER_UNITS","0")
		}
		logi.logInfo("Credit amount :: "+creditAmt)
		if((hm_coid.get("SERVICE_NO").toString() == "0") && (creditAmt!= null) && (coupon_cd.equals("NoVal")) && (!units_discount_amounts.equals("NoVal")) && ((!seq_no.equals("1")) && (!seq_no.equals("5")) && (!seq_no.equals("9"))))
		{
			logi.logInfo("Credit amount :: "+creditAmt)
			creditAmt = -(creditAmt.toInteger())
			hm_coid.put("LINE_AMOUNT",creditAmt)
			hm_coid.put("SERVICE_NAME","Account Credit")
			hm_coid.put("SERVICE_IS_TAX_IND","0")
			hm_coid.put("LINE_UNITS","0")
			hm_coid.put("RATE_PER_UNITS","0")
		}
		if((hm_coid.get("SERVICE_NO").toString() == "0") && (couponAmt!= null) && (!coupon_cd.equals("NoVal"))  && (!units_discount_amounts.equals("NoVal")) && ((!seq_no.equals("1")) && (!seq_no.equals("5")) && (!seq_no.equals("9"))))
		{
			hm_coid.put("LINE_AMOUNT",df.format(-(couponAmt.toDouble())).toString())
			hm_coid.put("SERVICE_NAME","Account Credit")
			hm_coid.put("SERVICE_IS_TAX_IND","0")
			hm_coid.put("LINE_UNITS","0")
			hm_coid.put("RATE_PER_UNITS","0")

		}
		if((hm_coid.get("SERVICE_NO").toString() == "0") && (couponAmt!= null) && (!coupon_cd.equals("NoVal")) && ((!seq_no.equals("1")) && (!seq_no.equals("5")) && (!seq_no.equals("9"))))
		{
			hm_coid.put("LINE_AMOUNT",df.format(-(couponAmt.toDouble())).toString())
			hm_coid.put("SERVICE_NAME","Account Credit")
			hm_coid.put("SERVICE_IS_TAX_IND","0")
			hm_coid.put("LINE_UNITS","0")
			hm_coid.put("RATE_PER_UNITS","0")

		}
		logi.logInfo ("expected method from DB = " +hm_coid.toString())
		return hm_coid.sort()
	}
	
	/**
	 * Verifies Create Order Invoice Charges from DB
	 * @param testCaseId
	 * @return Calculated invoice charges HashMap
	 */
	//This method is in under development, yet to add more conditions and remove log commands
	def md_verifyCreateOrderTotalChargesFromDBTax(String tcid)
	{
		   logi.logInfo("md_verifyCreateOrderTotalChargesFromDB")
		   String apiname=tcid.split("-")[2]
		   String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   def acct_no=Constant.mycontext.expand('${Properties#AccountNo}')
		   def inv_no=getValueFromResponse(apiname,ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)
		   String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		   String units = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNITS)
		   String amounts = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_AMOUNTS)
		   String units_discount_amounts = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNIT_DISCOUNT_AMOUNTS)
		   String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
		   String coupon_cd
		   if (apiname == "create_order_with_plans")
				  coupon_cd = getValueFromRequest(apiname,"//*[1]/*[local-name()='coupon_code']")
		   else
				  coupon_cd = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_COUPON_CD)
		   
		   def creditNo = getValueFromResponse('apply_service_credit',ExPathRpcEnc.APPLY_SERVICE_CREDIT_CREDITID1)
		   logi.logInfo("Ram Service Credit " + creditNo.toString())
		   logi.logInfo("Coupon Code applied is " +coupon_cd)
		   ConnectDB db = new ConnectDB();
		   logi.logInfo("units" + units.toString())
		   def order_name=getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_ORDER_NAME)
		   String rate,rate1
		   def seqno,charges,credits,tax_rate,tax_charges,creditAmt,coupon_amt,coupon_amt_per,coupon_dr_inline_amt,coupon_dr_offset_amt,coupon_dr_inline_amt_per,coupon_dr_offset_amt_per
		   def sum=0
		   def couponAmt = getCouponAmtFromCreateOrder(tcid)
		   logi.logInfo("couponAmttttttt " + couponAmt)
		   String rate_qry= "Select INVIP.price from ariacore.inventory_item_prices INVIP  JOIN ariacore.INVENTORY_ITEMS INVI ON invip.item_no=invi.item_no and invip.client_no=invi.client_no where INVI.client_sku='"+order_name+"' and INVI.client_no="+client_no+" and invip.currency_cd=(SELECT CURRENCY_CD FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+ " AND CLIENT_NO="+client_no+")"
		   String coupon_ind_query="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		   String coupon_ind = db.executeQueryP2(coupon_ind_query)
		   logi.logInfo("coupon_ind " + coupon_ind)
		   String coupon_type_query="SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		   String coupon_type = db.executeQueryP2(coupon_type_query)
		   logi.logInfo("coupon_typeeee " + coupon_type)
		   
		   String coupon_DB_query="SELECT amount FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Coupon_cd = '"+coupon_cd+"' AND CLIENT_NO="+client_no+")) and FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='I'"
		   def coupon_DB= db.executeQueryP2(coupon_DB_query)
		   
		   String coupon_DB_OFF_query="SELECT amount FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Coupon_cd = '"+coupon_cd+"' AND CLIENT_NO="+client_no+")) and FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='O'"
		   def coupon_DB_OFF= db.executeQueryP2(coupon_DB_OFF_query)

		   String creditreduces = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = "+client_no
		   String creditreducesvalue = db.executeQueryP2(creditreduces)
		   logi.logInfo("Client Credit reduces = "+creditreducesvalue)

		   String external_tax_client = "SELECT METHOD_CD FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO=" + client_no
		   external_tax_client = db.executeQueryP2(external_tax_client)
		   logi.logInfo("External Client ID = " +external_tax_client)
		   HashMap seqs=new HashMap()
		   HashMap row = new HashMap();

		   //Returns the Credit Amount when the Apply Service Credit is used in the API
		   if(creditNo =="NoVal")
		   {
				  creditAmt= 0
				  logi.logInfo("creditAmt "+creditAmt)
		   }
		   else
		   {
				  logi.logInfo("SERVICE_CREDITS")
				  String query1 = "select NVL(amount,0) from ariacore.all_credits where acct_no="+acct_no+" and credit_type='S' and credit_id="+creditNo
				  creditAmt=db.executeQueryP2(query1)
				  logi.logInfo(" creditAmt "+creditAmt)
				  //row.put("RAM SERVICE_CREDITS",credits)
		   }
		   
		   // Prints the Total Credit Amount and returns Charges when UNITS is passed and AMOUNT , UNIT DISCOUNT AMOUNT are NOT passed in create Order API
		   if ((!units.equals("NoVal")) && amounts.equals("NoVal") && units_discount_amounts.equals("NoVal"))
		   {
				  logi.logInfo("rate_qry :"+rate_qry)
				  rate = db.executeQueryP2(rate_qry)
				  charges= (units.toDouble() * rate.toDouble())
				  logi.logInfo("First If value  :"+charges)
				  row.put("TOTAL_CREDITS","0")
				  if(coupon_cd.equals("NoVal"))
				  {
						String unapplied_amt="SELECT -NVL(debit,0) from ariacore.gl_detail where invoice_no="+inv_no+" and service_no NOT IN (400,401,402,403,404,405) and seq_num=2"
						unapplied_amt = db.executeQueryP2(unapplied_amt)
						logi.logInfo("unapplied_amt :"+unapplied_amt)
						if(unapplied_amt!= null)
						{
							   charges = charges - unapplied_amt.toDouble()
							   row.put("TOTAL_CREDITS",df.format(unapplied_amt.toDouble()).toString())
						}
						credits = 0
				  }
				  else
				  {
						logi.logInfo("Entered into Coupon_cd method")				
						if(couponAmt!=null)
						{
							   if(coupon_type.equals("P") && coupon_ind.equals("I"))
									  charges = (couponAmt.toDouble())
							   else
									  charges= ((units.toInteger() * rate.toDouble()) - (couponAmt.toDouble()))
							   if(coupon_ind.equals("I"))
									  credits = 0
							   else
									  credits= (couponAmt.toDouble())
							   row.put("TOTAL_CREDITS",df.format(credits.toDouble()).toString())
							   credits = 0
						}
						
						if(coupon_DB!=null)
						{
							   charges= (((units.toInteger() * rate.toDouble()) - (coupon_DB.toDouble()))-(coupon_DB_OFF.toDouble()))
							   credits= (coupon_DB.toDouble())
							   row.put("TOTAL_CREDITS",df.format(credits.toDouble()).toString())
						}
						
				  }
		   }

		   //Prints the Total Credit Amount and returns Charges when UNITS AMD AMOUNT are passed and UNIT DISCOUNT AMOUNT is NOT passed in create Order API

		   else if ((!units.equals("NoVal")) && (!amounts.equals("NoVal")) && units_discount_amounts.equals("NoVal"))
		   {
				  rate = amounts
				  charges= (units.toDouble()) * rate.toDouble()
				  row.put("TOTAL_CREDITS","0")
		   }

		   // Prints the Total Credit Amount and returns Charges when UNITS , AMOUNT and UNIT DISCOUNT AMOUNT are passed in create Order API

		   else if ((!units.equals("NoVal")) && (!amounts.equals("NoVal"))  && (!units_discount_amounts.equals("NoVal")))
		   {
				  
				  logi.logInfo("TESt:")
				  rate = amounts
				  rate1 = units_discount_amounts
				  credits=(units.toInteger() * rate1.toInteger())
				  logi.logInfo("Credits"+credits)
				  charges= ((units.toInteger() * rate.toDouble()) - (units.toInteger() * rate1.toDouble()))
				  credits= (credits + (creditAmt.toDouble()))
				  logi.logInfo("Charges = "+charges)
				  logi.logInfo("Credits = "+credits)
				  row.put("TOTAL_CREDITS",df.format(credits.toDouble()).toString())
				  if(couponAmt!=null)
				  {
						logi.logInfo("coupon = "+couponAmt)
						logi.logInfo("Creditsinsidecoupon = "+credits)
						if(coupon_type.equals("P") && coupon_ind.equals("I"))
							   charges = ((couponAmt.toDouble())-(credits))
						else
							   charges= (charges- (couponAmt.toDouble()))
							   
						if(coupon_ind.equals("I"))
						{
								logi.logInfo("Creditsinlinecoupon = "+credits)
							   row.put("TOTAL_CREDITS",df.format(credits.toDouble()).toString())
						}
						else
						{
							credits= (credits + (couponAmt.toDouble()))
							row.put("TOTAL_CREDITS",df.format(credits.toDouble()).toString())
						}
				  }  
      }

		   //Prints the Total Credit Amount and returns Charges when UNITS and UNIT DISCOUNT AMOUNT are passed in create Order API

		   else if ((!units.equals("NoVal"))  && (!units_discount_amounts.equals("NoVal")))
		   {
				  logi.logInfo("rate_qry :"+rate_qry)
				  rate = db.executeQueryP2(rate_qry)
				  rate1 = units_discount_amounts
				  credits=(units.toInteger() * rate1.toInteger())
				  logi.logInfo("Credits"+credits)

				  if(couponAmt!=null)
				  {
						charges= ((units.toInteger() * rate.toDouble()) - ((units.toInteger() * rate1.toDouble())+couponAmt.toDouble()))
						credits= (couponAmt.toDouble() +(credits + creditAmt.toDouble()))
				  }
				  else
				  {
						charges= ((units.toInteger() * rate.toDouble()) - (units.toInteger() * rate1.toDouble()))
						credits= (credits + (creditAmt.toDouble()))
				  }
				  logi.logInfo("Credits = "+credits)
				  row.put("TOTAL_CREDITS",df.format(credits.toDouble()).toString())
		   }


		   //Returns Charges based on the Credit Amount		   
		   if(creditNo =="NoVal")
		   {
				  creditAmt= 0
				  charges= (charges.toDouble() - creditAmt.toDouble())
				  logi.logInfo("Charges = "+charges)
		   }
		   else
		   {
				  if ((!units.equals("NoVal")) && amounts.equals("NoVal") && units_discount_amounts.equals("NoVal"))
				  {
						logi.logInfo("Charges new = "+charges)
				  }
				  else
				  {
						charges= (charges.toDouble() - creditAmt.toDouble())
				  }
				  logi.logInfo("Charges new 2= "+charges)

		   }
		   

		   logi.logInfo("rate" + rate.toString())
		   logi.logInfo("charges"+charges)

		   row.put("TOTAL_CHARGES_BEFORE_TAX",df.format(charges.toDouble()).toString())


		   String seq_qry= "Select seq_num from ARIACORE.gl_tax_detail where invoice_no="+inv_no+ " order by seq_num asc"
		   ResultSet rs_seq_qry = db.executePlaQuery(seq_qry);
		   int k=1;
		   while (rs_seq_qry.next())
		   {
				  seqno = rs_seq_qry.getObject(1)
				  seqs.put("tax"+k, seqno)
				  k++
		   }
		   logi.logInfo("seqs array"+seqs)

		   if (creditreducesvalue!="0" || creditreducesvalue==null)
		   {

				  logi.logInfo("External Client ID = "+ external_tax_client)
				  logi.logInfo("Charges and Credit" + charges + " :: " + credits)
				  def txcharges = (charges.toDouble() + credits.toDouble()).toDouble()
				  logi.logInfo("calculated tax charges : " + txcharges)
				  
				  if (external_tax_client== "10" ) // Crexendo Tax Method
				  {
						String qury = "SELECT SUM(DEBIT) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (400, 401, 402, 403, 404, 405))"
						logi.logInfo("tax query : " + qury)
						String taxPercent = db.executeQueryP2(qury)
						logi.logInfo("Tax Percent is : " + taxPercent)
						tax_charges = ((taxPercent.toDouble())).toString()
						logi.logInfo("tax charges"+ tax_charges.toString())
				  }
				  else
				  {						
						logi.logInfo("tax_exemption_level == "+ tax_exemption_level.toString())
						String qury
						if (tax_exemption_level.toString()== "1")
							   qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (400, 402, 403, 404, 405))"
						else if (tax_exemption_level.toString()== "2")
							   qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (401, 402, 403, 404, 405))"
						else if (tax_exemption_level.toString()== "3")
							   qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (402, 403, 404, 405))"
						else
							   qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (400, 401, 402, 403, 404, 405))"
						
						logi.logInfo("tax query : " + qury)
						String taxPercent = db.executeQueryP2(qury)
						logi.logInfo("Tax Percent is : " + taxPercent)
						
						tax_charges = ((txcharges.toDouble())*(taxPercent.toDouble())).toString()
						logi.logInfo("tax charges"+ tax_charges.toString())
						
				  }
				  sum= sum.toDouble() + tax_charges.toDouble()
		   }
		   else
		   {
				  logi.logInfo("credit reduces = "+creditreducesvalue)
				  logi.logInfo("Charges and Credit" + charges + " :: " + credits)
				  def txcharges = (charges.toDouble() + credits.toDouble()).toDouble()
				  logi.logInfo("calculated tax charges : " + txcharges)
				  String qury
				  if (tax_exemption_level.toString()== "1")
				  {
						qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (400, 402, 403, 404, 405))"
						logi.logInfo("tax query : " + qury)
						String taxPercent = db.executeQueryP2(qury)
						logi.logInfo("Tax Percent is : " + taxPercent)
						tax_charges = ((txcharges.toDouble())*(taxPercent.toDouble())).toString()
						logi.logInfo("tax charges"+ tax_charges.toString())
						sum= sum.toDouble() + tax_charges.toDouble()
				  }
				  else if (tax_exemption_level.toString()== "2")
				  {
						qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (401, 402, 403, 404, 405))"
						logi.logInfo("tax query : " + qury)
						String taxPercent = db.executeQueryP2(qury)
						logi.logInfo("Tax Percent is : " + taxPercent)
						tax_charges = ((txcharges.toDouble())*(taxPercent.toDouble())).toString()
						logi.logInfo("tax charges"+ tax_charges.toString())
						sum= sum.toDouble() + tax_charges.toDouble()
				  }
				  else if (tax_exemption_level.toString()== "3")
				  {
						qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (402, 403, 404, 405))"
						logi.logInfo("tax query : " + qury)
						String taxPercent = db.executeQueryP2(qury)
						logi.logInfo("Tax Percent is : " + taxPercent)
						tax_charges = ((txcharges.toDouble())*(taxPercent.toDouble())).toString()
						logi.logInfo("tax charges"+ tax_charges.toString())
						sum= sum.toDouble() + tax_charges.toDouble()
				  }
				  else
				  {
						for(int i=1;i<=seqs.size();i++)
						{
							   def s_no=seqs.get("tax"+i)
							   String tax_qry = "Select tax_rate from ARIACORE.gl_tax_detail where invoice_no="+inv_no+" and seq_num="+s_no
							   ResultSet rs_tax = db.executePlaQuery(tax_qry);
							   while(rs_tax.next())
							   {
									  tax_rate = rs_tax.getObject(1)
									  def tempcharges = txcharges.toDouble()
									  logi.logInfo("charges= "+ tempcharges)
									  tax_charges= (tempcharges * (tax_rate*100).toInteger())/100
									  logi.logInfo("tax charges"+ tax_charges)
							   }
							   sum= sum + tax_charges
						}
				  }
		   }
		   logi.logInfo("total tax"+ sum)
		   row.put("TOTAL_TAX_CHARGES",df.format(sum.toDouble()).toString())
		   def total = charges + sum
		   logi.logInfo("all total "+ total)
		   row.put("TOTAL_CHARGES_AFTER_TAX",df.format(total.toDouble()).toString())
		   logi.logInfo("md_verifyCreateOrderTotalChargesFromDB_hashresult=> "+row)
		   return row.sort();
	}

	def md_verify_credit_amount(String tcid)
	{
		DecimalFormat df = new DecimalFormat("#.##")
		String units = getValueFromRequest('create_order_with_plans',ExPathRpcEnc.IR_CREATE_ORDER_WITH_PLANS_UNITS)
		String disc_amt = getValueFromRequest('create_order_with_plans',ExPathRpcEnc.IR_CREATE_ORDER_WITH_PLANS_DISC_AMT)
		String credits = units.toDouble() * disc_amt.toDouble()
		double credit = credits.toDouble()
		return df.format(-(credit))
	}
	
	/**
	 * Verifies Total TAX Amount from API
	 * @param testCaseId
	 * @return Calculated Total TAX Amount
	 */
	 String md_tax_cal_amount(String testCaseId)
	 {
		 
		 logi.logInfo "md_tax_cal_amount"
		 String apiname=testCaseId.split("-")[2]
			 
		 String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		 String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		 ConnectDB db = new ConnectDB();
		 List seqNum=[]
		 List serviceNum=[]
		 double amt1 =0
		 double taxperunit
		 
		 def MP_units = getValueFromRequest(apiname,"//master_plan_units")
		 logi.logInfo "MP_units : " +MP_units
		 
		 String invoice_no = "select invoice_no from ariacore.gl where client_no ="+clientNo+ "and  acct_no =" +accountNo
		 String Seq_Query =  "SELECT seq_num FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no IN(400,401,402,403,405)"
		 ResultSet rs=db.executePlaQuery(Seq_Query)
		 logi.logInfo "Executed result set"
		 while(rs.next())
		 {
				logi.logInfo" Inside result set of SEQ number"
				String Seq_no=rs.getString(1)
				seqNum.add(Seq_no)
		 }
		 
		 logi.logInfo "seqlist"+seqNum.toString()
				 
		 for(int k=0;k<seqNum.size();k++)
		 {
								 
			 String amount= "select sum(debit) from ariacore.gl_tax_detail where invoice_no in("+invoice_no+ ") and seq_num="+seqNum.get(k).toString()
			 double amt=(db.executeQueryP2(amount)).toDouble()
			 taxperunit = amt.toDouble() / MP_units.toDouble()
			 logi.logInfo "tax per units"+taxperunit
			 String totaltaxrate = "select sum(a.tax_rate) from (SELECT distinct tax_rate,seq_num FROM ariacore.gl_tax_detail WHERE invoice_no ("+invoice_no+ ") )a where a.seq_num  in("+seqNum.get(k).toString()
			 logi.logInfo "totaltaxrate"+totaltaxrate
			 logi.logInfo "inside for amount "+amt
			 amt1= amt1+amt
			 logi.logInfo "inside for total amount "+amt
					 
		 }
		 
		 logi.logInfo "after forloop amount"+amt1
		 DecimalFormat df = new DecimalFormat("#.##");
		 return df.format(amt1).toString()
			 
	 }
	 /**
	  * Verifies Individual TAX Amount from API
	  * @param testCaseId
	  * @return Calculated TAX Amount
	  */
	 def md_tax_cal_amount_API(String testCaseId)
	 {
		 logi.logInfo "md_tax_cal_amount from API"
		 String apiname=testCaseId.split("-")[2]
		 HashMap cac_api=new HashMap()
	
		 String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		 String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		 ConnectDB db = new ConnectDB();
		 DecimalFormat df = new DecimalFormat("#.##");
		 double noth
		 logi.logInfo "DB connected"
		 List serviceNum=[]
		 List seqNum=[]
		 String invoice_no = "select invoice_no from ariacore.gl where client_no ="+clientNo+ "and  acct_no =" +accountNo
		 String Service_Query =  "SELECT service_no FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no IN(400,401,402,403,405)"
		 ResultSet rs1=db.executePlaQuery(Service_Query)
		 logi.logInfo "Executed result set"
		 while(rs1.next())
		 {
				logi.logInfo" Inside result set of Service number"
				String Service_no=rs1.getString(1)
				serviceNum.add(Service_no)
		 }
		 logi.logInfo "ServiceList"+serviceNum.toString()
		 
		 String Seq_Query =  "SELECT seq_num FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no IN(400,401,402,403,405)"
		 ResultSet rs=db.executePlaQuery(Seq_Query)
		 logi.logInfo "Executed result set"
		 while(rs.next())
		 {
				logi.logInfo" Inside result set of SEQ number"
				String Seq_no=rs.getString(1)
				seqNum.add(Seq_no)
		 }
		 logi.logInfo "seqlist"+seqNum.toString()
		 
		 for(int k=0;k<serviceNum.size();k++)
		 {
			 logi.logInfo "Enter service" + serviceNum.get(k)
			 logi.logInfo("before API=> "+seqNum.get(k).toString())
			 String amt_api = getValueFromResponse('create_acct_complete',"//*["+seqNum.get(k)+"]/*[local-name()='invoice_line_amount']")
			 logi.logInfo("after API => "+seqNum.get(k).toString())
			 logi.logInfo("after API => "+amt_api)
			 double convert = amt_api.toDouble()
					 
			 if(serviceNum.get(k).toInteger() == 400)
			 {
				 if (amt_api != 0)
				 {
				 cac_api.put("Federal_TAX", convert.toString())
				 }
				 else
				 {
					 cac_api.put("Federal_TAX",  noth.toString())
				 }
			 }
			 else if(serviceNum.get(k).toInteger() == 401)
			 {
				 if(amt_api != 0)
				 {
				 cac_api.put("State_TAX", convert.toString())
				 }
				 else
				 {
					 cac_api.put("State_TAX", noth.toString())
				 }
			 }
			 else if(serviceNum.get(k).toInteger() == 402)
			 {
				 if(amt_api != 0)
				 {
				 cac_api.put("COUNTRY_TAX", convert.toString())
				 }
				 else
				 {
					 cac_api.put("COUNTRY_TAX", noth.toString())
				 }
			 }
			 else if(serviceNum.get(k).toInteger() == 403)
			 {
				 if(amt_api != 0)
				 {
				 cac_api.put("CITY_TAX", convert.toString())
				 }
				 else
				 {
					 cac_api.put("CITY_TAX", noth.toString())
				 }
			 }
			 else if(serviceNum.get(k).toInteger() == 405)
			 {
				 if(amt_api != 0)
				 {
				 cac_api.put("District_TAX", convert.toString())
				 }
				 else
				 {
					 cac_api.put("District_TAX", noth.toString())
				 }
			 }
		 }
		 logi.logInfo("md_tax_cal_amount from API=> "+cac_api)
		 return cac_api.sort()
	 }
	 
	 /**
	  * Verifies Individual TAX Amount from DB
	  * @param testCaseId
	  * @return Calculated TAX Amount
	  */
	 def md_tax_cal_amount_DB(String testCaseId)
	 {
		 
		 logi.logInfo "md_tax_cal_amount from DB"
		 String apiname=testCaseId.split("-")[2]
		 HashMap row = new HashMap();
		 
		 String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		 String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		 ConnectDB db = new ConnectDB();
		 double noth= 0.0
		 List serviceNum=[]
		 String invoice_no = "select invoice_no from ariacore.gl where client_no ="+clientNo+ "and  acct_no =" +accountNo
		 String Service_Query =  "SELECT service_no FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no IN(400,401,402,403,405)"
		 ResultSet rs1=db.executePlaQuery(Service_Query)
		 logi.logInfo "Executed result set"
				 
		 while(rs1.next())
		 {
				logi.logInfo" Inside result set of Service number"
				String Service_no=rs1.getString(1)
				serviceNum.add(Service_no)
		 }
		 logi.logInfo "ServiceList"+serviceNum.toString()
		 for(int k=0;k<serviceNum.size();k++)
		 {
		 
			 if(serviceNum.get(k).toInteger() == 400)
				 {
					 String federal_Query =  "SELECT NVL(sum(debit),0) FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no = 400"
					 double fedval = (db.executeQueryP2(federal_Query)).toDouble()
					 row.put("Federal_TAX",fedval.toString())
				 }
				 else if(serviceNum.get(k).toInteger() == 401)
				 {
					 String state_Query =  "SELECT NVL(sum(debit),0) FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no = 401"
					 double staval = (db.executeQueryP2(state_Query)).toDouble()
					 row.put("State_TAX",staval.toString())
				 }
				 else if(serviceNum.get(k).toInteger() == 402)
				 {
					 String country_Query =  "SELECT NVL(sum(debit),0) FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no = 402"
					 double conval = (db.executeQueryP2(country_Query)).toDouble()
					 row.put("COUNTRY_TAX",conval.toString())
				 }
				 else if(serviceNum.get(k).toInteger() == 403)
				 {
					 String city_Query =  "SELECT NVL(sum(debit),0) FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no = 403"
					 double citval = (db.executeQueryP2(city_Query)).toDouble()
					 row.put("CITY_TAX",citval.toString())
				 }
				 else if(serviceNum.get(k).toInteger() == 405)
				 {
					 String district_Query =  "SELECT NVL(sum(debit),0) FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no = 405"
					 double disval = (db.executeQueryP2(district_Query)).toDouble()
					 row.put("District_TAX",disval.toString())
				 }
			 
		 }
		 logi.logInfo("md_tax_cal_amount from DB=> "+row)
		 return row.sort()
				 
	 }	 
		 
	 
	 String md_tax_action_cd(String testCaseId)
	 {
		 logi.logInfo "Inside md_tax_action_cd"
		 String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		 String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		 String invoiceQuery="SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ " AND CLIENT_NO= "+clientNo + "order by invoice_no desc"
		 ConnectDB db = new ConnectDB()
		 String invoice_no=db.executeQueryP2(invoiceQuery)
		 String TAX_CD = "select TAX_ACTION_CD from ariacore.tax_server_log where log_id ='" + invoice_no + "' order by tax_action_cd desc"
		 
		 String TAX_ACTION=db.executeQueryP2(TAX_CD)
		 return TAX_ACTION
	 }
	 	 	 
	 /**
	  * Verifies Create account complete tax inclusive invoice line items from DB
	  * @param testCaseId,lineitem flag
	  * @return Calculated invoice lineitem amount
	  */
	 String md_create_acct_invoice_lineitem_inclusive_tax(String testCaseId,String flag)
	 {
		 logi.logInfo "md_invoice_lineitem_inclusive_tax"
		 DecimalFormat d = new DecimalFormat("#.##");
		 ConnectDB db = new ConnectDB()
		 logi.logInfo "Flag" +flag
		 String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		 logi.logInfo "account_no" +accountNo
		 String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		 def service_no = getValueFromResponse('create_acct_complete',"//*[" + flag + "]/*[local-name()='invoice_service_no']")
		  logi.logInfo "service_no :" +service_no
		  def plan_no = getValueFromResponse('create_acct_complete',"//*[" + flag + "]/*[local-name()='invoice_plan_no']")
		  logi.logInfo "plan_no :" +plan_no
		  def units = getValueFromResponse('create_acct_complete',"//*[" + flag + "]/*[local-name()='invoice_units']")
		  logi.logInfo "units :" + units
		  String query ="SELECT RATE_PER_UNIT * "+units+ " FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SERVICE_NO= "+service_no+ " AND SCHEDULE_NO=(SELECT RATE_SCHEDULE_NO FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO =" +accountNo + " AND PLAN_NO= "+plan_no+" AND SERVICE_NO= " +service_no+" AND RATE_SEQ_NO=1 AND CLIENT_NO= "+clientNo+") AND RATE_SEQ_NO = 1 AND CLIENT_NO="+clientNo
		  String amount=db.executeQueryP2(query)
		  logi.logInfo "amount :" + amount
		  String query1 = " select max(tax_rate) from ariacore.tax_rate_view where client_no =" + clientNo + "and service_no =" + service_no
		  String tax= db.executeQueryP2(query1)
		  logi.logInfo "tax :" + tax
		  double total_amount = amount.toDouble() / (1+tax.toDouble())
		  logi.logInfo "total_amount :" + total_amount
		  return d.format(total_amount).toString()
		 
	}
	 /**
	  * Verifies Create account complete tax inclusive invoice lineitems2 
	  * @param testCaseId,lineitem flag
	  * @return Calculated invoice lineitem amount
	  */
	 String md_creacte_acct_inclusive_lineitem2(String testCaseId)
	 {
		 String flag
		 String amount
		 flag = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICELINENO2)
		 logi.logInfo "flag" + flag
		 amount = md_create_acct_invoice_lineitem_inclusive_tax(testCaseId,flag)
		return amount
	}
	 /**
	  * Verifies Create account complete tax inclusive invoice lineitems3
	  * @param testCaseId,lineitem flag
	  * @return Calculated invoice lineitem amount
	  */
	 String md_creacte_acct_inclusive_lineitem3(String testCaseId)
	 {
		 String flag
		 String amount
		 flag = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICELINENO3)
		 logi.logInfo "flag" + flag
		 amount = md_create_acct_invoice_lineitem_inclusive_tax(testCaseId,flag)
		return amount
	}
	 /**
	  * Verifies Create account complete tax inclusive invoice lineitems4
	  * @param testCaseId,lineitem flag
	  * @return Calculated invoice lineitem amount
	  */
	 String md_creacte_acct_inclusive_lineitem4(String testCaseId)
	 {
		 String flag
		 String amount
		 flag = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICELINENO4)
		 logi.logInfo "flag" + flag
		 amount = md_create_acct_invoice_lineitem_inclusive_tax(testCaseId,flag)
		return amount
	}
	 /**
	  * Verifies Create account complete tax inclusive invoice lineitems6
	  * @param testCaseId,lineitem flag
	  * @return Calculated invoice lineitem amount
	  */
	 String md_creacte_acct_inclusive_lineitem6(String testCaseId)
	 {
		 String flag
		 String amount
		 flag = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICELINENO6)
		 logi.logInfo "flag" + flag
		 amount = md_create_acct_invoice_lineitem_inclusive_tax(testCaseId,flag)
		return amount
	}
	 /**
	  * Verifies Create account complete tax inclusive invoice lineitems7
	  * @param testCaseId,lineitem flag
	  * @return Calculated invoice lineitem amount
	  */
	 String md_creacte_acct_inclusive_lineitem7(String testCaseId)
	 {
		 String flag
		 String amount
		 flag = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICELINENO7)
		 logi.logInfo "flag" + flag
		 amount = md_create_acct_invoice_lineitem_inclusive_tax(testCaseId,flag)
		return amount
	}
	 /**
	  * Verifies Create account complete tax inclusive invoice lineitems7
	  * @param testCaseId,lineitem flag
	  * @return Calculated invoice lineitem amount
	  */
	 String md_creacte_acct_inclusive_lineitem8(String testCaseId)
	 {
		 String flag
		 String amount
		 flag = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICELINENO8)
		 logi.logInfo "flag" + flag
		 amount = md_create_acct_invoice_lineitem_inclusive_tax(testCaseId,flag)
		return amount
	}
	 /**
	  * Verifies pre calc invoice federal tax amount from DB
	  * @param testCaseId
	  * @return Calculated federal tax amount
	  */
	 String  md_pre_calc_invoice_calc_Federal(String testcaseID)
	 {
		 logi.logInfo "md_invoice_lineitem_inclusive_tax"
		 DecimalFormat d = new DecimalFormat("#.##");
		 String service_no
		 String plan_no
		 def amount
		 def units
		 def tax_exem
		 double total_tax
		 String apiname=testcaseID.split("-")[2]
		 ConnectDB db = new ConnectDB()
		 String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		 logi.logInfo "account_no" +accountNo
		 String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		 logi.logInfo "clientNo" +clientNo
		 service_no = getValueFromRequest(apiname,"//plan_item_service_no")
		 logi.logInfo "service_no : " +service_no
		 plan_no = getValueFromRequest(apiname,"//plan_item_plan_no")
		 logi.logInfo "plan_no : " +plan_no
		 units = getValueFromRequest(apiname,"//plan_item_units")
		 logi.logInfo "units : " +units
		 amount = getValueFromRequest(apiname,"//plan_item_unit_amount")
		 logi.logInfo "amount : " +amount
		 tax_exem = getValueFromRequest(apiname,"//tax_exempt_cd")
		 logi.logInfo "tax_exem : " +tax_exem
		 if (tax_exem == 'NoVal')
		 {
			 tax_exem = "0"
		 }
		 if(units == 'NoVal')
		 {
			 units = "1"
		 }
		 if(amount == 'NoVal')
		 {
			 String query ="SELECT RATE_PER_UNIT FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SERVICE_NO= "+service_no+ " AND SCHEDULE_NO=(SELECT RATE_SCHEDULE_NO FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO =" +accountNo + " AND PLAN_NO= "+plan_no+" AND SERVICE_NO= " +service_no+" AND RATE_SEQ_NO=1 AND CLIENT_NO= "+clientNo+") AND RATE_SEQ_NO = 1 AND CLIENT_NO="+clientNo
			 amount =db.executeQueryP2(query).toDouble()
			 logi.logInfo "amount :" + amount
		 }
		 if(tax_exem == "0" || tax_exem == "1")
		 {
			 logi.logInfo "Entering into the tax calculation"
			 String tax_qry = "SELECT RATE FROM ariacore.TAX_CALC_VALUE WHERE TAX_SERVICE_NO = 400 AND CLIENT_NO =" + clientNo
			 def tax_amt = db.executeQueryP2(tax_qry)
			 logi.logInfo "tax_amt : " +tax_amt
			 double total_amt = amount.toDouble() * units.toDouble()
			 logi.logInfo "total_amt : " +total_amt
			 total_tax =  total_amt * tax_amt.toDouble()
			 logi.logInfo "Total Tax Amount : " +total_tax
			 
		 }
		 else if(tax_exem == "2" || tax_exem == "3")
		 {
			 logi.logInfo "Entering into the tax calculation else part"
			 total_tax = 0
		 }
		 return d.format(total_tax).toString()
		 
		 
	 }
	 /**
	  * Verifies pre calc invoice state tax amount from DB
	  * @param testCaseId
	  * @return Calculated federal tax amount
	  */
	 String  md_pre_calc_invoice_calc_State(String testcaseID)
	 {
		 logi.logInfo "md_invoice_lineitem_inclusive_tax"
		 DecimalFormat d = new DecimalFormat("#.##");
		 String service_no
		 String plan_no
		 def amount
		 def units
		 def tax_exem
		 double total_tax
		 String apiname=testcaseID.split("-")[2]
		 ConnectDB db = new ConnectDB()
		 String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		 logi.logInfo "account_no" +accountNo
		 String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		 logi.logInfo "clientNo" +clientNo
		 service_no = getValueFromRequest(apiname,"//plan_item_service_no")
		 logi.logInfo "service_no : " +service_no
		 plan_no = getValueFromRequest(apiname,"//plan_item_plan_no")
		 logi.logInfo "plan_no : " +plan_no
		 units = getValueFromRequest(apiname,"//plan_item_units")
		 logi.logInfo "units : " +units
		 amount = getValueFromRequest(apiname,"//plan_item_unit_amount")
		 logi.logInfo "amount : " +amount
		 tax_exem = getValueFromRequest(apiname,"//tax_exempt_cd")
		 logi.logInfo "tax_exem : " +tax_exem
		 if (tax_exem == 'NoVal')
		 {
			 tax_exem = "0"
		 }
		 if(units == 'NoVal')
		 {
			 units = "1"
		 }
		 if(amount == 'NoVal')
		 {
			 String query ="SELECT RATE_PER_UNIT FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SERVICE_NO= "+service_no+ " AND SCHEDULE_NO=(SELECT RATE_SCHEDULE_NO FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO =" +accountNo + " AND PLAN_NO= "+plan_no+" AND SERVICE_NO= " +service_no+" AND RATE_SEQ_NO=1 AND CLIENT_NO= "+clientNo+") AND RATE_SEQ_NO = 1 AND CLIENT_NO="+clientNo
			 amount =db.executeQueryP2(query).toDouble()
			 logi.logInfo "amount :" + amount
		 }
		 if(tax_exem == "0" || tax_exem == "2")
		 {
			 logi.logInfo "Entering into the tax calculation"
			 String tax_qry = "SELECT RATE FROM ariacore.TAX_CALC_VALUE WHERE TAX_SERVICE_NO = 401 AND CLIENT_NO =" + clientNo
			 def tax_amt = db.executeQueryP2(tax_qry)
			 logi.logInfo "tax_amt : " +tax_amt
			 double total_amt = amount.toDouble() * units.toDouble()
			 logi.logInfo "total_amt : " +total_amt
			 total_tax =  total_amt * tax_amt.toDouble()
			 logi.logInfo "Total Tax Amount : " +total_tax
			 
		 }
		 else if (tax_exem == "1" || tax_exem == "3")
		 {
			 logi.logInfo "Entering into the tax calculation else part"
			 total_tax = 0
		 }
		 return d.format(total_tax).toString()
		 
		 
	 }
	 
	 /**
	  * Verifies pre calc invoice federal tax amount for order from DB
	  * @param testCaseId
	  * @return Calculated federal tax amount
	  */
	 String  md_pre_calc_orderamt_calc_Federal(String testcaseID)
	 {
		 logi.logInfo "md_invoice_lineitem_inclusive_tax"
		 DecimalFormat d = new DecimalFormat("#.##");
		 def sku
		 def sku_unit
		 def sku_amount
		 def tax_exem
		 double total_tax
		 def amount
		 String apiname=testcaseID.split("-")[2]
		 ConnectDB db = new ConnectDB()
		 String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		 logi.logInfo "account_no" +accountNo
		 String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		 logi.logInfo "clientNo" +clientNo
		 sku = getValueFromRequest(apiname,"//sku")
		 logi.logInfo "sku : " +sku
		 sku_amount = getValueFromRequest(apiname,"//sku_unit_amount")
		 logi.logInfo "sku_amount : " +sku_amount
		 sku_unit = getValueFromRequest(apiname,"//sku_units")
		 logi.logInfo "units : " +sku_unit
		 tax_exem = getValueFromRequest(apiname,"//tax_exempt_cd")
		 logi.logInfo "tax_exem : " +tax_exem
		 if (tax_exem == 'NoVal')
		 {
			 tax_exem = "0"
		 }
		 if(sku_unit == 'NoVal')
		 {
			 sku_unit = "1"
		 }
		 if(sku_amount == 'NoVal')
		 {
			 String query = "Select INVIP.price from ariacore.inventory_item_prices INVIP JOIN ariacore.INVENTORY_ITEMS INVI ON invip.item_no=invi.item_no and invip.client_no=invi.client_no where INVI.client_sku='"+sku+"' and INVI.client_no="+clientNo+" and invip.currency_cd=(SELECT CURRENCY_CD FROM ARIACORE.ACCT WHERE ACCT_NO="+accountNo+ " AND CLIENT_NO="+clientNo+")"
			 sku_amount =db.executeQueryP2(query).toDouble()
			 logi.logInfo "amount :" + sku_amount
		 }
		 if(tax_exem == "0" || tax_exem == "1")
		 {
			 logi.logInfo "Entering into the tax calculation"
			 String tax_qry = "SELECT RATE FROM ariacore.TAX_CALC_VALUE WHERE TAX_SERVICE_NO = 400 AND CLIENT_NO =" + clientNo
			 def tax_amt = db.executeQueryP2(tax_qry)
			 logi.logInfo "tax_amt : " +tax_amt
			 double total_amt = sku_amount.toDouble() * sku_unit.toDouble()
			 logi.logInfo "total_amt : " +total_amt
			 total_tax =  total_amt * tax_amt.toDouble()
			 logi.logInfo "Total Tax Amount : " +total_tax
			 
		 }
		 else if(tax_exem == "2" || tax_exem == "3")
		 {
			 logi.logInfo "Entering into the tax calculation else part"
			 total_tax = 0
		 }
		 return d.format(total_tax).toString()
		 
		 
	 }
	 
	 /**
	  * Verifies pre calc invoice state tax amount for order from DB
	  * @param testCaseId
	  * @return Calculated state tax amount
	  */
	 
	 String  md_pre_calc_orderamt_calc_State(String testcaseID)
	 {
		 logi.logInfo "md_invoice_lineitem_inclusive_tax"
		 DecimalFormat d = new DecimalFormat("#.##");
		 def sku
		 def sku_unit
		 def sku_amount
		 def tax_exem
		 double total_tax
		 def amount
		 String apiname=testcaseID.split("-")[2]
		 ConnectDB db = new ConnectDB()
		 String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		 logi.logInfo "account_no" +accountNo
		 String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		 logi.logInfo "clientNo" +clientNo
		 sku = getValueFromRequest(apiname,"//sku")
		 logi.logInfo "sku : " +sku
		 sku_amount = getValueFromRequest(apiname,"//sku_unit_amount")
		 logi.logInfo "sku_amount : " +sku_amount
		 sku_unit = getValueFromRequest(apiname,"//sku_units")
		 logi.logInfo "units : " +sku_unit
		 tax_exem = getValueFromRequest(apiname,"//tax_exempt_cd")
		 logi.logInfo "tax_exem : " +tax_exem
		 if (tax_exem == 'NoVal')
		 {
			 tax_exem = "0"
		 }
		 
		 if(sku_unit == 'NoVal')
		 {
			 sku_unit = "1"
		 }
		 if(sku_amount == 'NoVal')
		 {
			 String query = "Select INVIP.price from ariacore.inventory_item_prices INVIP JOIN ariacore.INVENTORY_ITEMS INVI ON invip.item_no=invi.item_no and invip.client_no=invi.client_no where INVI.client_sku='"+sku+"' and INVI.client_no="+clientNo+" and invip.currency_cd=(SELECT CURRENCY_CD FROM ARIACORE.ACCT WHERE ACCT_NO="+accountNo+ " AND CLIENT_NO="+clientNo+")"
			 sku_amount =db.executeQueryP2(query).toDouble()
			 logi.logInfo "amount :" + sku_amount
		 }
		 if(tax_exem == "0" || tax_exem == "2")
		 {
			 logi.logInfo "Entering into the tax calculation"
			 String tax_qry = "SELECT RATE FROM ariacore.TAX_CALC_VALUE WHERE TAX_SERVICE_NO = 401 AND CLIENT_NO =" + clientNo
			 def tax_amt = db.executeQueryP2(tax_qry)
			 logi.logInfo "tax_amt : " +tax_amt
			 double total_amt = sku_amount.toDouble() * sku_unit.toDouble()
			 logi.logInfo "total_amt : " +total_amt
			 total_tax =  total_amt * tax_amt.toDouble()
			 logi.logInfo "Total Tax Amount : " +total_tax
			 
		 }
		 else if(tax_exem == "1" || tax_exem == "3")
		 {
			 logi.logInfo "Entering into the tax calculation else part"
			 total_tax = 0
		 }
		 return d.format(total_tax).toString()
		 
		 
	 }
	 
	 /**
	  * Verifies pre calc invoice tax amount from hash map
	  * @param testCaseId
	  * @return Calculated state tax amount
	  */
	 
	 def md_get_invoice_federalTax_map(String testCaseId) {
		 String amount
		 amount = md_pre_calc_invoice_calc_Federal(testCaseId)
		 logi.logInfo "Amount : " +amount
		 return get_pre_cal_invoice_line("400",amount)
	 }
	 def md_get_invoice_stateTax_map(String testCaseId) {
		 String amount
		 amount = md_pre_calc_invoice_calc_State(testCaseId)
		 logi.logInfo "Amount : " +amount
		 return get_pre_cal_invoice_line("401",amount)
	 }
	 def md_get_invoice_federalTax_map_order(String testCaseId) {
		 String amount
		 amount = md_pre_calc_orderamt_calc_Federal(testCaseId)
		 logi.logInfo "Amount : " +amount
		 return get_pre_cal_invoice_line("400",amount)
	 }
	 def md_get_invoice_stateTax_map_order(String testCaseId) {
		 String amount
		 amount = md_pre_calc_orderamt_calc_State(testCaseId)
		 logi.logInfo "Amount : " +amount
		 return get_pre_cal_invoice_line("401",amount)
	 }
	 def md_get_invoice_federalTax_map_response(String testCaseId)
	  {
		  logi.logInfo"Entering into the respose hash map"
		 String amount
		 String apiname=testCaseId.split("-")[2]
		 amount = getValueFromResponse(apiname,ExPathRpcEnc.pre_calc_invoice_federal_tax_amount)
		 logi.logInfo "Amount : " +amount
		 return get_pre_cal_invoice_line("400",amount)
	 }
	  def md_get_invoice_stateTax_map_response(String testCaseId)
	  {
		  logi.logInfo"Entering into the respose hash map"
		 String amount
		 String apiname=testCaseId.split("-")[2]
		 amount = getValueFromResponse(apiname,ExPathRpcEnc.pre_calc_invoice_state_tax_amount)
		 logi.logInfo "Amount : " +amount
		 return get_pre_cal_invoice_line("401",amount)
	 }
	  def md_get_invoice_federalTax_map_order_response(String testCaseId)
	  {
		  logi.logInfo"Entering into the respose hash map"
		 String amount
		 String apiname=testCaseId.split("-")[2]
		 amount = getValueFromResponse(apiname,ExPathRpcEnc.pre_calc_order_federal_tax_amount)
		 logi.logInfo "Amount : " +amount
		 return get_pre_cal_invoice_line("400",amount)
	 }
	  def md_get_invoice_stateTax_map_order_response(String testCaseId)
	  {
		  logi.logInfo"Entering into the respose hash map"
		 String amount
		 String apiname=testCaseId.split("-")[2]
		 amount = getValueFromResponse(apiname,ExPathRpcEnc.pre_calc_order_state_tax_amount)
		 logi.logInfo "Amount : " +amount
		 return get_pre_cal_invoice_line("401",amount)
	 }
	 public HashMap<String,String> get_pre_cal_invoice_line(String service_no,String amount)
	 {
		 HashMap row = new HashMap();
		 String desc
		 logi.logInfo "service_no : " +service_no
		 logi.logInfo "Amount : " +amount
		 if (service_no == '400')
		 {
			desc = 'Federal Tax'
		 }
		 else if (service_no == '401')
		 {
			 desc = 'State Tax'
		 }
		 row.put("Service No",service_no.toString())
		 row.put("Description",desc.toString())
		 row.put("Amount",amount.toString())
		 return row.sort()
		 
	 }
	 
	 /**
	   * Calculates the amount of the given inline flat coupon
	   * @param testCaseId
	   * @return invoice amount
	   */
	  String md_inlineFlat_amount(String testCaseId)
	  {
		  logi.logInfo "Inside md_inlineFlat_amount"
		  String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		  String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		  logi.logInfo "Query execution begins......"
		  ConnectDB db = new ConnectDB()
		  String coupon_query="SELECT COUPON_CD FROM ariacore.ACCT_COUPONS WHERE ACCT_NO= "+accountNo+" AND CLIENT_NO= "+clientNo
		  String coupon_cd=db.executeQueryP2(coupon_query)
		  logi.logInfo "Coupon_cd : "+coupon_cd
		  String amt_query="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+ " and Coupon_cd = '"+coupon_cd+"')"
		  String amt=db.executeQueryP2(amt_query)
		  logi.logInfo "Coupon amount : "+amt
		  return amt
	  }
	  
	  /**
	   * Calculates invoice amount for client CR1
	   * @param testCaseId
	   * @return invoice amount
	   */
	  String md_checkExemptTax(String testCaseId)
	  {
		  logi.logInfo "Inside md_checkExemptTax"
		  String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		  String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		  String flag='FALSE'
		  String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
		  int tax_ex=tax_exemption_level.toInteger()
		  logi.logInfo "Tax_exemption level :"+tax_ex
		  String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		  ConnectDB db = new ConnectDB()
		  String invoice_no=db.executeQueryP2(invoiceQuery)
		  logi.logInfo "Invoice_no :"+invoice_no
		  logi.logInfo "Into the switch........"
		  switch(tax_exemption_level.toInteger())
		  {
			  case 0 :  String gl_query="SELECT SUM(DEBIT) FROM ariacore.gl_detail where invoice_no= "+invoice_no+ " and service_no in (400,401) "
						 String gl=db.executeQueryP2(gl_query)
						if(gl!=null)
						{
							flag='TRUE'
						}
						return flag
						break;
						
			 case 1 : String gl_query="SELECT SUM(DEBIT) FROM ariacore.gl_detail where invoice_no= "+invoice_no+ " and service_no=401"
			 String gl=db.executeQueryP2(gl_query)
			 if(gl=='0')
			 {
				 flag='TRUE'
			 }
			 return flag
			 break;
			 
			 case 2 : String gl_query="SELECT SUM(DEBIT) FROM ariacore.gl_detail where invoice_no= "+invoice_no+ " and service_no=400"
			 String gl=db.executeQueryP2(gl_query)
			 if(gl=='0')
			 {
				 flag='TRUE'
			 }
			 return flag
			 break;
			 
			 case 3 : String gl_query="SELECT SUM(DEBIT) FROM ariacore.gl_detail where invoice_no= "+invoice_no+ " and service_no in (400,401)"
			 String gl=db.executeQueryP2(gl_query)
			 if(gl=='0')
			 {
				 flag='TRUE'
			 }
			 return flag
			 break;
		  }
		  
	  }
	  
	  /**
	   * Calculates invoice amount for client CR0
	   * @param testCaseId
	   * @return invoice amount
	   */
	  String md_invoice_CR0(String testCaseId)
	  {
		  String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		  String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		  String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
		  List seqNum=[]
		  double taxAmount,invoiceAmount,coupon_credit
		  double actualAmount=(md_serviceInvoiceAmount(testCaseId)).toDouble()
		  logi.logInfo "Total Amount "+actualAmount
		  String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		  ConnectDB db = new ConnectDB()
		  String invoice_no=db.executeQueryP2(invoiceQuery)
		  logi.logInfo "Invoice_no " +invoice_no
		 
		  switch(tax_exemption_level.toInteger())
		  {
			  case 0 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO IN (400,401) ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 1 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO=400 ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 2 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO=401 ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 3 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO NOT IN (400,401) ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
		  }
		  for(int k=0;k<seqNum.size();k++)
		  {
			  String tax_query="SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND SEQ_NUM= "+seqNum.get(k).toString()
			  taxAmount=taxAmount+(db.executeQueryP2(tax_query)).toDouble()
			  logi.logInfo "Tax amount :" +taxAmount
		  }
		  
		  String couponqry = "SELECT COUPON_CD FROM ACCT_COUPONS WHERE ACCT_NO="+accountNo
		  String coupon_cd=db.executeQueryP2(couponqry)
		  logi.logInfo "Coupon : "+ coupon_cd
		  String ioflag_qry ="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
		  String ioflag_indicator=db.executeQueryP2(ioflag_qry)
		  logi.logInfo "ioflag_indicator : "+ ioflag_indicator
		  String flat_per_qry = "SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
		  String flatper_indicator=db.executeQueryP2(flat_per_qry)
		  logi.logInfo "flatper_indicator : "+ flatper_indicator
		  String findOverlap="Select allow_overlap_ind from ariacore.client_discount_rule_bundles where bundle_no=(Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
		  String findOverlapInd=db.executeQueryP2(findOverlap)
		  
		  
		  if(flatper_indicator != null  && ioflag_indicator != null )
		  {
			  logi.logInfo "Inside Discount ruleeeeeeeeeeeeee"
			  String amount_qry = "SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
			  coupon_credit = (db.executeQueryP2(amount_qry)).toDouble()
			  logi.logInfo "cou_amount : "+ coupon_credit
		  
			  if(flatper_indicator == 'P' && actualAmount != 0)
			  {
			  coupon_credit = actualAmount * (coupon_credit / 100)
			  }
		  }
		  
		  else if(findOverlapInd!=null)
		  {
			  if(findOverlapInd=='Y')
			  {
				  logi.logInfo "Inside Discount Bundleeeeeeeeeeeeeee YYYYYYYYYY"
				  String coupon_DisBun_Flat="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select RULE_NO FROM ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='F'"
				  String coupon_DisBun_Per="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='P'"
				  double DB_flat=(db.executeQueryP2(coupon_DisBun_Flat)).toDouble()
				  double DB_Per=(db.executeQueryP2(coupon_DisBun_Per)).toDouble()
				  coupon_credit=DB_flat+(actualAmount * (DB_Per / 100))
				  logi.logInfo "Credits fro discount bundle : "+coupon_credit
				  
			  }
			  
			  else
			  {
				  logi.logInfo "Inside Discount Bundleeeeeeeeeeeeeee NNNNNNN"
				  String coupon_DisBun_Flat="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select RULE_NO FROM ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='F'"
				  String coupon_DisBun_Per="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='P'"
				  double DB_flat=(db.executeQueryP2(coupon_DisBun_Flat)).toDouble()
				  double DB_Per=(db.executeQueryP2(coupon_DisBun_Per)).toDouble()
				  coupon_credit=(actualAmount * (DB_Per / 100))
				  logi.logInfo "Credits fro discount bundle : "+coupon_credit
			  }
		  }
		  
		  else
		  {
			  coupon_credit=0
		  }
		  logi.logInfo"The coupon credit applied is :"+coupon_credit
		  
		  invoiceAmount=(actualAmount+taxAmount)-coupon_credit
		  DecimalFormat df = new DecimalFormat("#.##");
		  return df.format(invoiceAmount).toString()
	  }
	  
	  /**
	   * Calculates invoice amount for client CR1
	   * @param testCaseId
	   * @return invoice amount 
	   */
	  String md_invoice_CR1(String testCaseId)
	  {
		  String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		  String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		  String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
		  logi.logInfo"Tax_exempt_level "+tax_exemption_level
		  if(tax_exemption_level=='NoVal')
		  {
			  tax_exemption_level=0
		  }
		  List seqNum=[]
		  double taxAmount,invoiceAmount,coupon_credit
		  double actualAmount=(md_serviceInvoiceAmount(testCaseId)).toDouble()
		  logi.logInfo "Total Amount "+actualAmount
		  String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		  ConnectDB db = new ConnectDB()
		  String invoice_no=db.executeQueryP2(invoiceQuery)
		  logi.logInfo "Invoice_no " +invoice_no
		 
		  switch(tax_exemption_level.toInteger())
		  {
			  case 0 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO IN (400,401) ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 1 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO=400 ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 2 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO=401 ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 3 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO NOT IN (400,401) ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  default : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO  IN (400,401) ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
		  }
		  for(int k=0;k<seqNum.size();k++)
		  {
			  String tax_query="SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND SEQ_NUM= "+seqNum.get(k).toString()
			  taxAmount=taxAmount+(db.executeQueryP2(tax_query)).toDouble()
			  logi.logInfo "Tax amount :" +taxAmount
		  }
		  
		  String couponqry = "SELECT COUPON_CD FROM ACCT_COUPONS WHERE ACCT_NO="+accountNo
		  String coupon_cd=db.executeQueryP2(couponqry)
		  logi.logInfo "Coupon : "+ coupon_cd
		  String ioflag_qry ="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
		  String ioflag_indicator=db.executeQueryP2(ioflag_qry)
		  logi.logInfo "ioflag_indicator : "+ ioflag_indicator
		  String flat_per_qry = "SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
		  String flatper_indicator=db.executeQueryP2(flat_per_qry)
		  logi.logInfo "flatper_indicator : "+ flatper_indicator
		  String findOverlap="Select allow_overlap_ind from ariacore.client_discount_rule_bundles where bundle_no=(Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
		  String findOverlapInd=db.executeQueryP2(findOverlap)
		  
		  
		  if(flatper_indicator != null  && ioflag_indicator != null )
		  {
			  logi.logInfo "Inside Discount ruleeeeeeeeeeeeee"
			  String amount_qry = "SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
			  coupon_credit = (db.executeQueryP2(amount_qry)).toDouble()
			  logi.logInfo "cou_amount : "+ coupon_credit
		  
			  if(flatper_indicator == 'P' && actualAmount != 0)
			  {
			  coupon_credit = actualAmount * (coupon_credit / 100)
			  }
		  }
		  
		  else if(findOverlapInd!=null)
		  {
			  if(findOverlapInd=='Y')
			  {
				  logi.logInfo "Inside Discount Bundleeeeeeeeeeeeeee YYYYYYYYYY"
				  String coupon_DisBun_Flat="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select RULE_NO FROM ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='F'"
				  String coupon_DisBun_Per="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='P'"
				  double DB_flat=(db.executeQueryP2(coupon_DisBun_Flat)).toDouble()
				  double DB_Per=(db.executeQueryP2(coupon_DisBun_Per)).toDouble()
				  coupon_credit=DB_flat+(actualAmount * (DB_Per / 100))
				  logi.logInfo "Credits fro discount bundle : "+coupon_credit
				  
			  }
			  
			  else
			  {
				  logi.logInfo "Inside Discount Bundleeeeeeeeeeeeeee NNNNNNN"
				  String coupon_DisBun_Flat="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select RULE_NO FROM ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='F'"
				  String coupon_DisBun_Per="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='P'"
				  double DB_flat=(db.executeQueryP2(coupon_DisBun_Flat)).toDouble()
				  double DB_Per=(db.executeQueryP2(coupon_DisBun_Per)).toDouble()
				  coupon_credit=(actualAmount * (DB_Per / 100))
				  logi.logInfo "Credits fro discount bundle : "+coupon_credit
			  }
		  }
		  
		  else
		  {
			  coupon_credit=0
		  }
		 logi.logInfo"The coupon credit applied is :"+coupon_credit
		  
		  invoiceAmount=(actualAmount-coupon_credit)+taxAmount
		  DecimalFormat df = new DecimalFormat("#.##");
		  return df.format(invoiceAmount).toString()
	  }

	  /**
	   * Calculates State tax for client CR0
	   * @param testCaseId
	   * @return State tax amount
	   */
	 String md_stateTax_orders_CR0(String testcaseId)
	  {
		  String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		  String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		  String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
		  List seqNum=[]
		  List plan=[],service=[]
		  double taxAmount,invoiceAmount,fedTax
		  double actualAmount=0.0
		  double ans,stateTax
		  //double totalAmount=(md_serviceInvoiceAmount(testcaseId)).toDouble()
		  //logi.logInfo "Total Amount "+totalAmount
		  String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		  ConnectDB db = new ConnectDB()
		  String invoice_no=db.executeQueryP2(invoiceQuery)
		  logi.logInfo "Invoice_no " +invoice_no
		  int taxExempt=tax_exemption_level.toInteger()
			
		  if(taxExempt==0||taxExempt==2)
			{
				String plan_query="SELECT DISTINCT(PLAN_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND PLAN_NO IS  NOT NULL"
				ResultSet rs=db.executePlaQuery(plan_query)
				logi.logInfo "Executed result set...."
				while(rs.next())
				{
					logi.logInfo" Inside result set of plan....."
					String plan_no=rs.getString(1)
					plan.add(plan_no)
				}
				logi.logInfo "Plan list : "+plan.toString()
		
				for(int i=0;i<plan.size();i++)
				{
					service.clear()
					String service_query="SELECT service_no from ariacore.gl_detail where invoice_no= "+invoice_no+ "and service_no not in (0,400,401,402,403,405) and plan_no="+plan.get(i).toString()+ " AND DEBIT > 0"
					rs=db.executePlaQuery(service_query)
					while(rs.next())
					{
						logi.logInfo "Inside result set..."
						String service_no=rs.getString(1)
						service.add(service_no)
					}
					logi.logInfo "Service list : " +service.toString()
		
					for(int j=0;j<service.size();j++)
					{
						String query1="SELECT NVL((PRORATION_FACTOR*BASE_PLAN_UNITS),0) FROM ARIACORE.GL_DETAIL WHERE SERVICE_NO=" +service.get(j).toString()+"AND PLAN_NO= "+plan.get(i).toString()+ "AND INVOICE_NO= "+invoice_no
						ans=(db.executeQueryP2(query1)).toDouble()
						logi.logInfo "Ans : "+ans
						String rate_query="SELECT NVL((rate_per_unit),0) from ARIACORE.NEW_RATE_SCHED_RATES where service_no= " +service.get(j).toString()+ " and SCHEDULE_NO=(SELECT max(RATE_SCHEDULE_NO) FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = " +accountNo+ " AND PLAN_NO= "+plan.get(i).toString()+ " AND SERVICE_NO= " +service.get(i).toString()+ " AND RATE_SEQ_NO=1) AND RATE_SEQ_NO = 1 and client_no= "+clientNo
						double rate=(db.executeQueryP2(rate_query)).toDouble()
						logi.logInfo "Rate : "+rate
						actualAmount=actualAmount+(ans*rate)
						logi.logInfo "Actual amount" +actualAmount
						
					}
		
				}
				String credit_query="SELECT amount FROM ARIACORE.orders WHERE acct_no= "+accountNo
				String order_credit=db.executeQueryP2(credit_query)
				if(order_credit==null)
				{
					order_credit=0
					logi.logInfo "order_credit :" +order_credit
				}
			logi.logInfo "order_credit :" +order_credit
			double totalAmount=actualAmount+order_credit.toDouble()
			logi.logInfo "Total amount :" +totalAmount
			String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO = 401 order by seq_num desc"
			String seq_num=db.executeQueryP2(seq_query)
			String tax_query="SELECT NVL(SUM(DISTINCT(TAX_RATE)),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SEQ_NUM= "+seq_num
			String tax_rate=db.executeQueryP2(tax_query)
			logi.logInfo "Tax rate :" +tax_rate
			stateTax=(totalAmount*(tax_rate.toDouble()))
			logi.logInfo "stateTax :" +stateTax
			DecimalFormat df = new DecimalFormat("#.##");
			return df.format(stateTax).toString()
	
		 }
		 else
		 stateTax=0
		 
	  }
	  
	  /**
	   * Calculates Federal tax for client CR0
	   * @param testCaseId
	   * @return Federal tax amount
	   */
	  String md_fedTax_orders_CR0(String testcaseId)
	  {
		  String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		  String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		  String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
		  List seqNum=[]
		  List plan=[],service=[]
		  double taxAmount,invoiceAmount,fedTax
		  double actualAmount=0.0
		  double ans,stateTax
		  //double totalAmount=(md_serviceInvoiceAmount(testcaseId)).toDouble()
		  //logi.logInfo "Total Amount "+totalAmount
		  String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		  ConnectDB db = new ConnectDB()
		  String invoice_no=db.executeQueryP2(invoiceQuery)
		  logi.logInfo "Invoice_no " +invoice_no
		  int taxExempt=tax_exemption_level.toInteger()
		  
		 if(taxExempt==0||taxExempt==1)
		 {
			 String plan_query="SELECT DISTINCT(PLAN_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND PLAN_NO IS  NOT NULL"
			 ResultSet rs=db.executePlaQuery(plan_query)
			 logi.logInfo "Executed result set...."
			 while(rs.next())
			 {
				 logi.logInfo" Inside result set of plan....."
				 String plan_no=rs.getString(1)
				 plan.add(plan_no)
			 }
			 logi.logInfo "Plan list : "+plan.toString()
	 
			 for(int i=0;i<plan.size();i++)
			 {
				 service.clear()
				 String service_query="SELECT service_no from ariacore.gl_detail where invoice_no= "+invoice_no+ "and service_no not in (0,400,401,402,403,405) and plan_no="+plan.get(i).toString()+ " AND DEBIT > 0"
				 rs=db.executePlaQuery(service_query)
				 while(rs.next())
				 {
					 logi.logInfo "Inside result set..."
					 String service_no=rs.getString(1)
					 service.add(service_no)
				 }
				 logi.logInfo "Service list : " +service.toString()
	 
				for(int j=0;j<service.size();j++)
					{
						
						String query1="SELECT NVL((PRORATION_FACTOR*BASE_PLAN_UNITS),0) FROM ARIACORE.GL_DETAIL WHERE SERVICE_NO=" +service.get(j).toString()+"AND PLAN_NO= "+plan.get(i).toString()+ "AND INVOICE_NO= "+invoice_no
						ans=(db.executeQueryP2(query1)).toDouble()
						logi.logInfo "Ans : "+ans
						String rate_query="SELECT NVL((rate_per_unit),0) from ARIACORE.NEW_RATE_SCHED_RATES where service_no= " +service.get(j).toString()+ " and SCHEDULE_NO=(SELECT max(RATE_SCHEDULE_NO) FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = " +accountNo+ " AND PLAN_NO= "+plan.get(i).toString()+ " AND SERVICE_NO= " +service.get(i).toString()+ " AND RATE_SEQ_NO=1) AND RATE_SEQ_NO = 1 and client_no= "+clientNo
						double rate=(db.executeQueryP2(rate_query)).toDouble()
						logi.logInfo "Rate : "+rate
						actualAmount=actualAmount+(ans*rate)
						logi.logInfo "Actual amount" +actualAmount
						
					}
	 
			 }
			 String credit_query="SELECT amount FROM ARIACORE.orders WHERE acct_no= "+accountNo
			 String order_credit=db.executeQueryP2(credit_query)
			 if(order_credit==null)
			 {
				 order_credit=0
				 logi.logInfo "order_credit :" +order_credit
			 }
			 logi.logInfo "order_credit :" +order_credit
			 double totalAmount=actualAmount+order_credit.toDouble()
			 logi.logInfo "Total amount :" +totalAmount
			String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO = 400 order by seq_num desc"
			String seq_num=db.executeQueryP2(seq_query)
			String tax_query="SELECT NVL(SUM(DISTINCT(TAX_RATE)),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SEQ_NUM= "+seq_num
			String tax_rate=db.executeQueryP2(tax_query)
			logi.logInfo "Tax rate :" +tax_rate
			fedTax=(totalAmount*(tax_rate.toDouble()))
			logi.logInfo "fedTax :" +fedTax
			DecimalFormat df = new DecimalFormat("#.##");
			return df.format(fedTax).toString()
	
		 }
		 else
		 fedTax=0
		 
	  }
	  
	  /**
	   * Calculates State tax for client CR1
	   * @param testCaseId
	   * @return State tax amount
	   */
	  String md_stateTax_orders_CR1(String testcaseId)
	  {
		  String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		  String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		  String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
		  List seqNum=[]
		  List plan=[],service=[]
		  double taxAmount,invoiceAmount,fedTax,coupon_credit
		  double actualAmount=0.0
		  double ans,stateTax
		  //double totalAmount=(md_serviceInvoiceAmount(testcaseId)).toDouble()
		  //logi.logInfo "Total Amount "+totalAmount
		  String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		  ConnectDB db = new ConnectDB()
		  String invoice_no=db.executeQueryP2(invoiceQuery)
		  logi.logInfo "Invoice_no " +invoice_no
		  int taxExempt=tax_exemption_level.toInteger()
			
		  if(taxExempt==0||taxExempt==2)
			{
				String plan_query="SELECT DISTINCT(PLAN_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND PLAN_NO IS  NOT NULL"
				ResultSet rs=db.executePlaQuery(plan_query)
				logi.logInfo "Executed result set...."
				while(rs.next())
				{
					logi.logInfo" Inside result set of plan....."
					String plan_no=rs.getString(1)
					plan.add(plan_no)
				}
				logi.logInfo "Plan list : "+plan.toString()
		
				for(int i=0;i<plan.size();i++)
				{
					service.clear()
					String service_query="SELECT service_no from ariacore.gl_detail where invoice_no= "+invoice_no+ "and service_no not in (0,400,401,402,403,405) and plan_no="+plan.get(i).toString()+ "AND DEBIT > 0"
					rs=db.executePlaQuery(service_query)
					while(rs.next())
					{
						logi.logInfo "Inside result set..."
						String service_no=rs.getString(1)
						service.add(service_no)
					}
					logi.logInfo "Service list : " +service.toString()
		
					for(int j=0;j<service.size();j++)
					{
						String query1="SELECT NVL((PRORATION_FACTOR*BASE_PLAN_UNITS),0) FROM ARIACORE.GL_DETAIL WHERE SERVICE_NO=" +service.get(j).toString()+"AND PLAN_NO= "+plan.get(i).toString()+ "AND INVOICE_NO= "+invoice_no
						ans=(db.executeQueryP2(query1)).toDouble()
						logi.logInfo "Ans : "+ans
						String rate_query="SELECT NVL((rate_per_unit),0) from ARIACORE.NEW_RATE_SCHED_RATES where service_no= " +service.get(j).toString()+ " and SCHEDULE_NO=(SELECT max(RATE_SCHEDULE_NO) FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = " +accountNo+ " AND PLAN_NO= "+plan.get(i).toString()+ " AND SERVICE_NO= " +service.get(i).toString()+ " AND RATE_SEQ_NO=1) AND RATE_SEQ_NO = 1 and client_no= "+clientNo
						double rate=(db.executeQueryP2(rate_query)).toDouble()
						logi.logInfo "Rate : "+rate
						actualAmount=actualAmount+(ans*rate)
						logi.logInfo "Actual amount" +actualAmount
						
					}
		
				}
				String credit_query="SELECT amount FROM ARIACORE.orders WHERE acct_no= "+accountNo
				String order_credit=db.executeQueryP2(credit_query)
				if(order_credit==null)
				{
					order_credit=0
					logi.logInfo "order_credit :" +order_credit
				}
				
				String couponqry = "SELECT COUPON_CD FROM ACCT_COUPONS WHERE ACCT_NO="+accountNo
				String coupon_cd=db.executeQueryP2(couponqry)
				logi.logInfo "Coupon : "+ coupon_cd
				String ioflag_qry ="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
				String ioflag_indicator=db.executeQueryP2(ioflag_qry)
				logi.logInfo "ioflag_indicator : "+ ioflag_indicator
				String flat_per_qry = "SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
				String flatper_indicator=db.executeQueryP2(flat_per_qry)
				logi.logInfo "flatper_indicator : "+ flatper_indicator
				String findOverlap="Select allow_overlap_ind from ariacore.client_discount_rule_bundles where bundle_no=(Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
				String findOverlapInd=db.executeQueryP2(findOverlap)
				
				
				if(flatper_indicator != null  && ioflag_indicator != null )
				{
					logi.logInfo "Inside Discount ruleeeeeeeeeeeeee"
					String amount_qry = "SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
					coupon_credit = (db.executeQueryP2(amount_qry)).toDouble()
					logi.logInfo "cou_amount : "+ coupon_credit
				
					if(flatper_indicator == 'P' && actualAmount != 0)
					{
					coupon_credit = actualAmount * (coupon_credit / 100)
					}
				}
				
				else if(findOverlapInd!=null)
				{
					if(findOverlapInd=='Y')
					{
						logi.logInfo "Inside Discount Bundleeeeeeeeeeeeeee YYYYYYYYYY"
						String coupon_DisBun_Flat="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select RULE_NO FROM ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='F'"
						String coupon_DisBun_Per="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='P'"
						double DB_flat=(db.executeQueryP2(coupon_DisBun_Flat)).toDouble()
						double DB_Per=(db.executeQueryP2(coupon_DisBun_Per)).toDouble()
						coupon_credit=DB_flat+(actualAmount * (DB_Per / 100))
						logi.logInfo "Credits fro discount bundle : "+coupon_credit
						
					}
					
					else
					{
						logi.logInfo "Inside Discount Bundleeeeeeeeeeeeeee NNNNNNN"
						String coupon_DisBun_Flat="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select RULE_NO FROM ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='F'"
						String coupon_DisBun_Per="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='P'"
						double DB_flat=(db.executeQueryP2(coupon_DisBun_Flat)).toDouble()
						double DB_Per=(db.executeQueryP2(coupon_DisBun_Per)).toDouble()
						coupon_credit=(actualAmount * (DB_Per / 100))
						logi.logInfo "Credits fro discount bundle : "+coupon_credit
					}
				}
				
				else
				{
					coupon_credit=0
				}
				logi.logInfo"The coupon credit applied is :"+coupon_credit
			
			logi.logInfo "order_credit :" +order_credit
			double totalAmount=(actualAmount+order_credit.toDouble())-coupon_credit
			logi.logInfo "Total amount :" +totalAmount
			String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO = 401 order by seq_num desc"
			String seq_num=db.executeQueryP2(seq_query)
			String tax_query="SELECT NVL(SUM(DISTINCT(TAX_RATE)),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SEQ_NUM= "+seq_num
			String tax_rate=db.executeQueryP2(tax_query)
			logi.logInfo "Tax rate :" +tax_rate
			stateTax=(totalAmount*(tax_rate.toDouble()))
			logi.logInfo "stateTax :" +stateTax
			DecimalFormat df = new DecimalFormat("#.##");
			return df.format(stateTax).toString()
	
		 }
		 else
		 stateTax=0
		 
	  }
	  
	  /**
	   * Calculates federal tax for client CR1
	   * @param testCaseId
	   * @return Federal tax amount
	   */
	  String md_fedTax_orders_CR1(String testcaseId)
	  {
		  String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		  String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		  String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
		  List seqNum=[]
		  List plan=[],service=[]
		  double taxAmount,invoiceAmount,fedTax
		  double actualAmount=0.0
		  double ans,stateTax
		  double coupon_credit
		  //double totalAmount=(md_serviceInvoiceAmount(testcaseId)).toDouble()
		  //logi.logInfo "Total Amount "+totalAmount
		  String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		  ConnectDB db = new ConnectDB()
		  String invoice_no=db.executeQueryP2(invoiceQuery)
		  logi.logInfo "Invoice_no " +invoice_no
		  int taxExempt=tax_exemption_level.toInteger()
		  
		 if(taxExempt==0||taxExempt==1)
		 {
			 String plan_query="SELECT DISTINCT(PLAN_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND PLAN_NO IS  NOT NULL"
			 ResultSet rs=db.executePlaQuery(plan_query)
			 logi.logInfo "Executed result set...."
			 while(rs.next())
			 {
				 logi.logInfo" Inside result set of plan....."
				 String plan_no=rs.getString(1)
				 plan.add(plan_no)
			 }
			 logi.logInfo "Plan list : "+plan.toString()
	 
			 for(int i=0;i<plan.size();i++)
			 {
				 service.clear()
				 String service_query="SELECT service_no from ariacore.gl_detail where invoice_no= "+invoice_no+ "and service_no not in (0,400,401,402,403,405) and plan_no="+plan.get(i).toString()+ "AND DEBIT > 0"
				 rs=db.executePlaQuery(service_query)
				 while(rs.next())
				 {
					 logi.logInfo "Inside result set..."
					 String service_no=rs.getString(1)
					 service.add(service_no)
				 }
				 logi.logInfo "Service list : " +service.toString()
	 
				for(int j=0;j<service.size();j++)
					{
						
						String query1="SELECT NVL((PRORATION_FACTOR*BASE_PLAN_UNITS),0) FROM ARIACORE.GL_DETAIL WHERE SERVICE_NO=" +service.get(j).toString()+"AND PLAN_NO= "+plan.get(i).toString()+ "AND INVOICE_NO= "+invoice_no
						ans=(db.executeQueryP2(query1)).toDouble()
						logi.logInfo "Ans : "+ans
						String rate_query="SELECT NVL((rate_per_unit),0) from ARIACORE.NEW_RATE_SCHED_RATES where service_no= " +service.get(j).toString()+ " and SCHEDULE_NO=(SELECT max(RATE_SCHEDULE_NO) FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = " +accountNo+ " AND PLAN_NO= "+plan.get(i).toString()+ " AND SERVICE_NO= " +service.get(i).toString()+ " AND RATE_SEQ_NO=1) AND RATE_SEQ_NO = 1 and client_no= "+clientNo
						double rate=(db.executeQueryP2(rate_query)).toDouble()
						logi.logInfo "Rate : "+rate
						actualAmount=actualAmount+(ans*rate)
						logi.logInfo "Actual amount" +actualAmount
						
					}
	 
			 }
			 String credit_query="SELECT amount FROM ARIACORE.orders WHERE acct_no= "+accountNo
			 String order_credit=db.executeQueryP2(credit_query)
			 if(order_credit==null)
			 {
				 order_credit=0
				 logi.logInfo "order_credit :" +order_credit
			 }
			 logi.logInfo "order_credit :" +order_credit
			 
			 
			 String couponqry = "SELECT COUPON_CD FROM ACCT_COUPONS WHERE ACCT_NO="+accountNo
			 String coupon_cd=db.executeQueryP2(couponqry)
			 logi.logInfo "Coupon : "+ coupon_cd
			 String ioflag_qry ="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
			 String ioflag_indicator=db.executeQueryP2(ioflag_qry)
			 logi.logInfo "ioflag_indicator : "+ ioflag_indicator
			 String flat_per_qry = "SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
			 String flatper_indicator=db.executeQueryP2(flat_per_qry)
			 logi.logInfo "flatper_indicator : "+ flatper_indicator
			 String findOverlap="Select allow_overlap_ind from ariacore.client_discount_rule_bundles where bundle_no=(Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
			 String findOverlapInd=db.executeQueryP2(findOverlap)
			 
			 
			 if(flatper_indicator != null  && ioflag_indicator != null )
			 {
				 logi.logInfo "Inside Discount ruleeeeeeeeeeeeee"
				 String amount_qry = "SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
				 coupon_credit = (db.executeQueryP2(amount_qry)).toDouble()
				 logi.logInfo "cou_amount : "+ coupon_credit
			 
				 if(flatper_indicator == 'P' && actualAmount != 0)
				 {
				 coupon_credit = actualAmount * (coupon_credit / 100)
				 }
			 }
			 
			 else if(findOverlapInd!=null)
			 {
				 if(findOverlapInd=='Y')
				 {
					 logi.logInfo "Inside Discount Bundleeeeeeeeeeeeeee YYYYYYYYYY"
					 String coupon_DisBun_Flat="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select RULE_NO FROM ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='F'"
					 String coupon_DisBun_Per="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='P'"
					 double DB_flat=(db.executeQueryP2(coupon_DisBun_Flat)).toDouble()
					 double DB_Per=(db.executeQueryP2(coupon_DisBun_Per)).toDouble()
					 coupon_credit=DB_flat+(actualAmount * (DB_Per / 100))
					 logi.logInfo "Credits fro discount bundle : "+coupon_credit
					 
				 }
				 
				 else
				 {
					 logi.logInfo "Inside Discount Bundleeeeeeeeeeeeeee NNNNNNN"
					 String coupon_DisBun_Flat="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select RULE_NO FROM ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='F'"
					 String coupon_DisBun_Per="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+clientNo+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='P'"
					 double DB_flat=(db.executeQueryP2(coupon_DisBun_Flat)).toDouble()
					 double DB_Per=(db.executeQueryP2(coupon_DisBun_Per)).toDouble()
					 coupon_credit=(actualAmount * (DB_Per / 100))
					 logi.logInfo "Credits fro discount bundle : "+coupon_credit
				 }
			 }
			 
			 else
			 {
				 coupon_credit=0
			 }
			 logi.logInfo"The coupon credit applied is :"+coupon_credit
			 
			 double totalAmount=actualAmount+order_credit.toDouble()-coupon_credit
			 logi.logInfo "Total amount :" +totalAmount
			String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO = 400 order by seq_num desc"
			String seq_num=db.executeQueryP2(seq_query)
			String tax_query="SELECT sum(debit) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SEQ_NUM= "+seq_num
			String tax_rate=db.executeQueryP2(tax_query)
			logi.logInfo "Tax rate :" +tax_rate
			fedTax=(totalAmount*(tax_rate.toDouble()))
			logi.logInfo "fedTax :" +fedTax
			DecimalFormat df = new DecimalFormat("#.##");
			return df.format(fedTax).toString()
	
		 }
		 else
		 fedTax=0
		 
	  }
	  
	  
	  /**
	   * Verifies the invoice amount for client CR0
	   * @param testCaseId
	   * @return Invoice amount generated
	   */
	  String md_finalInvoice_CR0(String testCaseId)
	  {
		  String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		  String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		  String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
		  List seqNum=[]
		  double taxAmount,invoiceAmount,coupon_credit
		  double actualAmount=(md_serviceInvoiceAmount(testCaseId)).toDouble()
		  logi.logInfo "Total Amount "+actualAmount
		  String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		  ConnectDB db = new ConnectDB()
		  String invoice_no=db.executeQueryP2(invoiceQuery)
		  logi.logInfo "Invoice_no " +invoice_no
		 
		  switch(tax_exemption_level.toInteger())
		  {
			  case 0 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO IN (400,401) ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 1 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO=400 ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 2 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO=401 ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 3 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO NOT IN (400,401) ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
		  }
		  for(int k=0;k<seqNum.size();k++)
		  {
			  String tax_query="SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND SEQ_NUM= "+seqNum.get(k).toString()
			  taxAmount=taxAmount+(db.executeQueryP2(tax_query)).toDouble()
			  logi.logInfo "Tax amount :" +taxAmount
		  }
		  
		  String coupon_query="SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no
		  double coupon_amt=(db.executeQueryP2(coupon_query)).toDouble()
		  
		  double invoice_amt=(actualAmount+taxAmount)+coupon_amt
		  DecimalFormat df = new DecimalFormat("#.##");
		  return df.format(coupon_amt).toString()
	  }
	  
	  /**
	   * Verifies the invoice amount for client CR1
	   * @param testCaseId
	   * @return Invoice amount generated
	   */
	  String md_finalInvoice_CR1(String testCaseId)
	  {
		  def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		  String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		  String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
		  if(tax_exemption_level=='NoVal')
		  {
			  tax_exemption_level=0
		  }
		  List seqNum=[]
		  double taxAmount,invoiceAmount,coupon_credit
		  double actualAmount=(md_serviceInvoiceAmount(testCaseId)).toDouble()
		  logi.logInfo "Total Amount "+actualAmount
		  String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		  ConnectDB db = new ConnectDB()
		  String invoice_no=db.executeQueryP2(invoiceQuery)
		  logi.logInfo "Invoice_no " +invoice_no
		 
		  switch(tax_exemption_level.toInteger())
		  {
			  case 0 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO IN (400,401) ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 1 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO=400 ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 2 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO=401 ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 3 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO NOT IN (400,401) ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
		  }
		  for(int k=0;k<seqNum.size();k++)
		  {
			  String tax_query="SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND SEQ_NUM= "+seqNum.get(k).toString()
			  taxAmount=taxAmount+(db.executeQueryP2(tax_query)).toDouble()
			  logi.logInfo "Tax amount :" +taxAmount
		  }
		  
		  String coupon_query="SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no
		  double coupon_amt=(db.executeQueryP2(coupon_query)).toDouble()
		  
		  double invoice_amt=(actualAmount+coupon_amt)+taxAmount
		  DecimalFormat df = new DecimalFormat("#.##");
		  return df.format(coupon_amt).toString()
	  }
	  
	  /**
	   * Verifies Create Order with amount,discount amount and coupon with taxation from DB for CR1 and CR0 client
	   * @param testCaseId,Invoice Sequence Number
	   * @return Calculated invoice charges HashMap
	   */
	  def md_verifyCreateOrderTotalChargesFromAPI_CR1(String tcid)
	  {
	  logi.logInfo("md_verifyCreateOrderTotalChargesFromAPIcheck")
	  List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
	  List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
	  String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
	  HashMap cotc_api=new HashMap()
	  com.eviware.soapui.support.XmlHolder outputholder
	  DecimalFormat df = new DecimalFormat("#.##");
	  String client_no=Constant.mycontext.expand('${Properties#Client_No}')
	  def acct_no=Constant.mycontext.expand('${Properties#AccountNo}')
	  def inv_no=getValueFromResponse('create_order',ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)
	  String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
	  for(int j=0; j<testCaseCmb.size(); j++) {
			 if(testCaseCmb[j].toString().contains('create_order') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID)
			 {
					double calc, calcDisc, calc1, calc2, calc3,state_tax,fed_tax,con_tax, plan_rates
					def creditAmt, coupon_type_value, couponDisBunYes_res, couponDisBunNo_res,taxAmt
					def units = getValueFromRequest('create_order',ExPathRpcEnc.IR_CREATE_ORDER_UNITS)
					units = units.toInteger()
					def amt = getValueFromRequest('create_order',ExPathRpcEnc.IR_CREATE_ORDER_AMOUNTS)
					amt = amt.toInteger()
					def discUnits = getValueFromRequest('create_order',ExPathRpcEnc.IR_CREATE_ORDER_UNIT_DISCOUNT_AMOUNTS)
					discUnits =discUnits.toInteger()
					def billImm = getValueFromRequest('create_order',ExPathRpcEnc.IR_CREATE_ORDER_BILL_IMMEDIATELY)
					billImm = billImm.toInteger()
					def couponCd = getValueFromRequest('create_order',ExPathRpcEnc.IR_CREATE_ORDER_COUPON_CD)
					def clientSku = getValueFromRequest('create_order',ExPathRpcEnc.IR_CREATE_ORDER_ORDER_NAME)
					def couponAmtFin = getCouponAmtFromCreateOrder(tcid)
					double couponAmt = couponAmtFin.toDouble()
					double tot_bef_tax, tot_aft_tax, tot_credit
					String couponCTFlat_res,couponCTPercentage_res,couponDRFlatOffset_res,couponDRFlatInline_res,couponDRPerOffset_res,couponDRPerInline_res, coupon_DB, coupon_DB_OFF,couponDisBunFla_res,couponDisBunPer_res
					String couponCTPercentage_fin_res,couponDRPercentage_off,couponDRPercentage_inline,finderOverlap_res
					logi.logInfo("Coupon amount is= " + couponAmtFin )
					
					ConnectDB db = new ConnectDB();
					
					String coupon_ind_query="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
					String coupon_ind = db.executeQueryP2(coupon_ind_query)
					
					String coupon_type_query="SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
					String coupon_type = db.executeQueryP2(coupon_type_query)
					
					String creditreduces = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = "+client_no
					String creditreducesvalue = db.executeQueryP2(creditreduces)
	  
					String external_tax_client = "SELECT METHOD_CD FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO=" + client_no
					external_tax_client = db.executeQueryP2(external_tax_client)
					
					
					// For all tax calc
					List seqNum=[]
					def amt1 =0, taxes=0
					 switch(tax_exemption_level.toInteger())
		  {
			  case 0 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+inv_no+"AND SERVICE_NO IN (400,401) ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 1 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+inv_no+"AND SERVICE_NO=400 ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 2 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+inv_no+"AND SERVICE_NO=401 ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
			  
			  case 3 : String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+inv_no+"AND SERVICE_NO NOT IN (400,401) ORDER BY SEQ_NUM DESC"
			  ResultSet rs=db.executePlaQuery(seq_query)
			  while(rs.next())
			  {
				  logi.logInfo" Inside result set of SEQ_NUM....."
				  String seq_no=rs.getString(1)
				  seqNum.add(seq_no)
			  }
			  break;
		  }
					
					for(int k=0;k<seqNum.size();k++)
					{
																																														
						  String amount1= "select NVL(sum(debit),0) from ariacore.gl_tax_detail where invoice_no in("+inv_no+ ") and seq_num="+seqNum.get(k).toString()
						  taxAmt=(db.executeQueryP2(amount1)).toDouble()
						  logi.logInfo("Tax amount " + taxAmt)
						  amt1 = amt1 + taxAmt
						  logi.logInfo("total Tax amount " + (amt1).toString())
					}
					  
					
					
					taxes = df.format(amt1).toString()
					
																		  
					String couponCTFlat= "select flat_amount from ariacore.RECURRING_CREDIT_TEMPLATES where percent_amount is null and RECURRING_CREDIT_TEMPLATE_NO = (select RECURRING_CREDIT_TEMPLATE_NO from ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP where Client_no = "+client_no+" and Coupon_cd ='"+couponCd+"')"
					String couponCTPercentage = "select percent_amount from ariacore.RECURRING_CREDIT_TEMPLATES where flat_amount is null and RECURRING_CREDIT_TEMPLATE_NO = (select RECURRING_CREDIT_TEMPLATE_NO from ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP where Client_no = "+client_no+" and Coupon_cd ='"+couponCd+"')"
					String couponDRFlatOffset= "SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='O' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
					String couponDRFlatInline="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='I' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
					String couponDRPerOffset= "SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' and INLINE_OFFSET_IND='O' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
					String couponDRPerInline="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' and INLINE_OFFSET_IND='I' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
					String findOverlap="Select allow_overlap_ind from ariacore.client_discount_rule_bundles where bundle_no=(Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Client_no = "+client_no+" and Coupon_cd = '"+couponCd+"')"
					
					
					couponCTFlat_res=db.executeQueryP2(couponCTFlat)
					couponCTPercentage_res=db.executeQueryP2(couponCTPercentage)
					couponDRFlatOffset_res=db.executeQueryP2(couponDRFlatOffset)
					couponDRFlatInline_res=db.executeQueryP2(couponDRFlatInline)
					couponDRPerOffset_res=db.executeQueryP2(couponDRPerOffset)
					couponDRPerInline_res=db.executeQueryP2(couponDRPerInline)
					finderOverlap_res= db.executeQueryP2(findOverlap)
					
					logi.logInfo("Flat Amount = " +couponCTFlat_res)
					logi.logInfo("Percent Amount = " +couponCTPercentage_res)
					logi.logInfo("Flat Off = " +couponDRFlatOffset_res)
					logi.logInfo("Flat In  = " +couponDRFlatInline_res)
					logi.logInfo("Per Off = " +couponDRPerOffset_res)
					logi.logInfo("Per In = " +couponDRPerInline_res)
					logi.logInfo("Coupon Flat Yes = " +couponDisBunFla_res)
					logi.logInfo("Coupon Flat No = " +couponDisBunPer_res)
					logi.logInfo("Overlap Yes or No = " +finderOverlap_res)
					
								 
					def line_units=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICEUNITS2)
					def line_rate_units=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICERATEPERUNIT2)
					
					if(couponCTFlat_res != null)
						  {
								 // Flat
								 calc = units * amt
								 calcDisc= units * discUnits
								 tot_bef_tax = calc.toDouble() - calcDisc.toDouble() - couponAmt.toDouble()
								 def tax =  taxes.toDouble()
								 tot_aft_tax = tot_bef_tax + taxes.toDouble()
								 tot_credit = calcDisc.toDouble() + couponAmt.toDouble()
								 cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
								 cotc_api.put("TOTAL_TAX_CHARGES",((df.format(tax))).toString())
								 cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
						  }
					else if(couponCTPercentage_res != null )
						  {
								 // Percentage
								 calc = units * amt
								 calcDisc= units * discUnits
								 tot_bef_tax = calc.toDouble()  - calcDisc.toDouble() - couponAmt.toDouble()
								 def tax =  taxes.toDouble()
								 tot_aft_tax = tot_bef_tax.toDouble() + taxes.toDouble()
								 tot_credit = calcDisc.toDouble() + couponAmt.toDouble()
								 cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
								 cotc_api.put("TOTAL_TAX_CHARGES",((df.format(tax))).toString())
								 cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
						  }
					else if(couponDRFlatOffset_res != null)
						  {
								 //OFF line Flat
								 logi.logInfo("case no 3 flat off")
								 calc = units * amt
								 calcDisc= units * discUnits
								 tot_bef_tax = calc.toDouble() - couponAmt.toDouble() - calcDisc.toDouble()
								 tot_aft_tax = tot_bef_tax.toDouble() + taxes.toDouble()
								 def tax =  taxes.toDouble()
								 tot_credit = calcDisc.toDouble()
								 cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
								 cotc_api.put("TOTAL_TAX_CHARGES",((df.format(tax))).toString())
								 cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
						  }
					else if(couponDRFlatInline_res != null)
						  {
								 // IN line Flat
								 calcDisc= units * discUnits
								 calc = (units * amt) - couponAmt.toDouble() - calcDisc.toDouble()
								 tot_bef_tax = calc.toDouble()
								 def tax =  taxes.toDouble()
								 tot_aft_tax = tot_bef_tax.toDouble() + taxes.toDouble()
								 tot_credit = calcDisc.toDouble()
								 cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
								 cotc_api.put("TOTAL_TAX_CHARGES",((df.format(tax))).toString())
								 cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
						  }
					else if(couponDRPerOffset_res != null)
						  {
								 // Off line Percentage
								 calc = units * amt
								 calcDisc= units * discUnits
								 tot_bef_tax = calc.toDouble() - couponAmt.toDouble() - calcDisc.toDouble()
								 def tax =  taxes.toDouble()
								 tot_aft_tax = tot_bef_tax.toDouble() + taxes.toDouble()
								 logi.logInfo("Service Credit " + calcDisc)
								 tot_credit = couponAmt.toDouble() + calcDisc.toDouble()
								 cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
								 cotc_api.put("TOTAL_TAX_CHARGES",((df.format(tax))).toString())
								 cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
						  }
					else if(couponDRPerInline_res != null)
						  {
								 double inlinevalue
								 // In line Percentage
								 calc = units * amt
								 calcDisc= units * discUnits
								 inlinevalue = calc - couponAmt
								 tot_bef_tax = calc.toDouble() - calcDisc.toDouble() - inlinevalue.toDouble()
								 def tax =  taxes.toDouble()
								 tot_aft_tax =  tot_bef_tax.toDouble() + taxes.toDouble()
								 tot_credit = calcDisc.toDouble()
								 cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
								 cotc_api.put("TOTAL_TAX_CHARGES",((df.format(tax))).toString())
								 cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
						  }
						  
						  if (finderOverlap_res == 'Y')
						  {
								 //Discount bundle Offset
							  calc = (units * amt)
								 calcDisc= units * discUnits
								 tot_bef_tax = calc.toDouble() - couponAmt.toDouble() - calcDisc.toDouble()
								 logi.logInfo("Before" + tot_bef_tax)
								 def tax =  taxes.toDouble()
								 tot_aft_tax =  tot_bef_tax.toDouble() + taxes.toDouble()
								 tot_credit = calcDisc.toDouble()
								 cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
								 cotc_api.put("TOTAL_TAX_CHARGES",((df.format(tax))).toString())
								 cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
						  }
						  
						  if (finderOverlap_res == 'N')
						  {
								 //Discount bundle Offset
								 //String taxQuery = "select sum(debit) from ariacore.gl_tax_detail where invoice_no in("+inv_no+ ") and CLIENT_NO="+client_no
								 //String taxFin = db.executeQueryP2(taxQuery)
								 //double wholeTax=taxFin.toDouble()
								 String coupon_DisBun_Flat="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select RULE_NO FROM ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+client_no+" and coupon_cd = '"+couponCd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='F'"
								 String coupon_DisBun_Per="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+client_no+" and coupon_cd = '"+couponCd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='P'"
								 couponDisBunFla_res=db.executeQueryP2(coupon_DisBun_Flat)
								 couponDisBunPer_res=db.executeQueryP2(coupon_DisBun_Per)
								 def tax =  taxes.toDouble()
								 calc = (units * amt)
								 calcDisc= units * discUnits
								 tot_bef_tax = calc.toDouble() - couponAmt.toDouble() - calcDisc.toDouble()
								 tot_aft_tax =  tot_bef_tax.toDouble() + taxes.toDouble()
								 tot_credit = calcDisc.toDouble()
								 cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", (df.format(tot_bef_tax)).toString())
								 cotc_api.put("TOTAL_TAX_CHARGES",((df.format(tax))).toString())
								 cotc_api.put("TOTAL_CHARGES_AFTER_TAX",(df.format(tot_aft_tax)).toString())
						  }
					outputholder = xmlValues[j]
					
			 }
			 
			 if (cotc_api.get("TOTAL_TAX_CHARGES").toString() == "null")
					{
					//cotc_api.put("TOTAL_TAX_CHARGES","0")
					logi.logInfo("Finish")
					}
			 else if (cotc_api.get("TOTAL_CREDITS").toString() == "null")
			 {
					logi.logInfo("Finish")
					//cotc_api.put("TOTAL_CREDITS","0")
			 }
	  }
	  logi.logInfo("md_verifyCreateOrderTotalChargesFromAPIcheck_hashresult=> "+cotc_api)
	  return cotc_api.sort();
}
	  
	  String md_tax_action_code(String testCaseId)
	  {
			 logi.logInfo "Inside md_tax_action_cd"
			 String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			 String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			 String invoiceQuery="SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ " AND CLIENT_NO= "+clientNo
			 ConnectDB db = new ConnectDB()
			 String invoice_no=db.executeQueryP2(invoiceQuery)
			 String TAX_CD = "select max(TAX_ACTION_CD) from ariacore.tax_server_log where log_id ='" + invoice_no + "'"
			 
			 String TAX_ACTION=db.executeQueryP2(TAX_CD)
	  }
	  
	  String md_tax_refund_action_code(String testCaseId)
	  {
			 logi.logInfo "md_tax_refund_action_code"
			 String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			 String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			 String refundQuery="SELECT REFUND_NO FROM ARIACORE.MANAGED_REFUNDS WHERE ACCT_NO = " +accountNo+ " AND CLIENT_NO= "+clientNo
			 ConnectDB db = new ConnectDB()
			 String refund_no =db.executeQueryP2(refundQuery)
			 String TAX_CD = "SELECT TAX_ACTION_CD FROM ARIACORE.TAX_ACTION_QUEUE_2 WHERE CLIENT_NO = " +clientNo+ " and INVOICE_OR_REFUND_NO = "+refund_no
			 String TAX_ACTION=db.executeQueryP2(TAX_CD)
			 if(TAX_ACTION == null)
			 {
					return "No Value"
			 }
			 else
			 {
					return TAX_ACTION
			 }
	  }
	  def md_ExpInvoiceWithordercredit(String testCaseId)
	  {
		  logi.logInfo("calling md_ExpInvoiceWithCashCredit")
		  String apiname=testCaseId.split("-")[2]
		  String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		  def accountNo = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		  def orderNo = getValueFromResponse('create_order',ExPathRpcEnc.CREATE_ORDER_ORDER_NO)
		 // def transactionNo = getValueFromResponse('apply_cash_credit',ExPathRpcEnc.APPLY_CASH_CREDIT_TRANSACTIONID1)
		  def orderAmt, cashCreditAmt ,expPlansInv, expInvoiceAmt,taxamt
		  ConnectDB database = new ConnectDB();
		  expPlansInv =md_serviceInvoiceAmount(testCaseId)
		  logi.logInfo("expInvoice "+expPlansInv)
		  String query = "select NVL(amount,0) from ariacore.orders where acct_no="+accountNo+" and order_no="+orderNo+" and client_no="+clientNo+ "and status_cd =4"
		  orderAmt=database.executeQueryP2(query)
		  if(orderAmt == null)
			  orderAmt = 0
		  logi.logInfo("orderAmt "+orderAmt)
		  String query1 = "SELECT NVL(SUM(AMOUNT),0) FROM ARIACORE.CREDITS WHERE ACCT_NO = "+accountNo+ " AND LEFT_TO_APPLY = 0"
		  cashCreditAmt=database.executeQueryP2(query1)
		  logi.logInfo("creditAmt "+cashCreditAmt)
		  String query2 = "SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO =(SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO =" + accountNo + ")"
		  taxamt=database.executeQueryP2(query2)
		  expInvoiceAmt = ((expPlansInv.toDouble() + orderAmt.toDouble() + taxamt.toDouble())- cashCreditAmt.toDouble()).toString()
		  logi.logInfo("expInvoiceAmt "+expInvoiceAmt)
		  return df.format(expInvoiceAmt.toDouble()).toString()
	  }


def md_planupgrade(String testCaseId)
{
		  
	logi.logInfo("calling md_ExpInvoiceWithCashCredit")
	String apiname=testCaseId.split("-")[2]
	HashMap credit_api=new HashMap()
	String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	def accountNo = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	ConnectDB database = new ConnectDB();
	String after_tax = getValueFromResponse(apiname,"//*/total_charges_after_tax[1]")
	String credit_tax = getValueFromResponse(apiname,"//*/*:total_credit[1]")
	double tax_convert = after_tax.toDouble()
	double credit_convert1 = credit_tax.toDouble()
	logi.logInfo("tax "+tax_convert)
	logi.logInfo("credit "+credit_convert1)
	double total = tax_convert - credit_convert1
	logi.logInfo("credit "+total)
	def planunits = getValueFromRequest('create_acct_complete',ExPathRpcEnc.IR_MASTER_PLAN_UNITS)
	logi.logInfo("old value "+planunits)
	def plannewunits = getValueFromRequest(apiname,ExPathRpcEnc.IR_MASTER_PLAN_UNITS)
	logi.logInfo("new value "+plannewunits)
	DecimalFormat df = new DecimalFormat("#.##");
	if(planunits < plannewunits)
	{
		return df.format(total.toDouble()).toString() 
	}
	else
	{
    return ((total)*(-1)).toString()
	}
}

def md_proration_amt(String testCaseId)
{
	
	logi.logInfo("calling md_proration_amt")
	String apiname=testCaseId.split("-")[2]
	HashMap proration_api=new HashMap()
	
	DecimalFormat df = new DecimalFormat("#.##");
	String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	def accountNo = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	ConnectDB database = new ConnectDB();
	def prorationAmt,totalpro1,totalpro
	String invoice_amt = "select debit from ariacore.all_invoices  where  ACCT_NO=" +accountNo +" and client_no =" +clientNo+" order by due_date desc"
	prorationAmt=database.executeQueryP2(invoice_amt)
	logi.logInfo("proration "+prorationAmt)
	totalpro1 =  md_plancredit(testCaseId)
	logi.logInfo("totalprorationbefore "+totalpro1)
	totalpro = prorationAmt.toDouble() - totalpro1.toDouble()
	logi.logInfo("totalprorationafter "+totalpro)
	return df.format(totalpro.toDouble()).toString()
}

def md_plancredit(String testCaseId)
{
		  
	logi.logInfo("calling md_plancredit")
	String apiname=testCaseId.split("-")[2]
	HashMap credit_api=new HashMap()
	
	DecimalFormat df = new DecimalFormat("#.##");
	String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	def accountNo = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	def cashCreditAmt,lefttoapply
	ConnectDB database = new ConnectDB();
	logi.logInfo "DB connected"
	String amount = "SELECT sum(amount) FROM ARIACORE.CREDITS WHERE ACCT_NO=" +accountNo +" and client_no =" +clientNo+ " ORDER BY LEFT_TO_APPLY DESC"
	cashCreditAmt=database.executeQueryP2(amount)
	logi.logInfo("creditAmt "+cashCreditAmt)
	String left_to_apply =  "SELECT sum(left_to_apply) FROM ARIACORE.CREDITS WHERE ACCT_NO=" +accountNo +" and client_no =" +clientNo+ "  ORDER BY LEFT_TO_APPLY DESC"
	lefttoapply=database.executeQueryP2(left_to_apply)
	logi.logInfo("lefttoapply "+lefttoapply)
	double tax_convert = cashCreditAmt.toDouble()
	logi.logInfo("tax ccreditAmt "+tax_convert)
	double credit_convert1 = lefttoapply.toDouble()
	logi.logInfo("credit Amt "+credit_convert1)
	double total_credit = tax_convert - credit_convert1
	return df.format(total_credit).toString()
	
	//String credit_count = "SELECT count(credit_id) FROM ARIACORE.CREDITS WHERE ACCT_NO=" +accountNo +" and client_no =" +clientNo+ "  ORDER BY LEFT_TO_APPLY DESC"
//	credit_api.put("total_charges_after_tax", tax_convert.toString())
	//credit_api.put("total_credit_before_tax", tax_convert.toString())
	//credit_api.put("total charges_after_tax", tax_convert.toString())
	//double total = tax_convert - credit_convert1
	// return ((total)*(-1)).toString()
}


def md_plancreditcal(String testCaseId)
{
		  
	logi.logInfo("calling md_plancredit")
	String apiname=testCaseId.split("-")[2]
	HashMap credit_api=new HashMap()
	
	DecimalFormat df = new DecimalFormat("#.##");
	String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	def accountNo = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	def cashCreditAmt,lefttoapply
	ConnectDB db = new ConnectDB();
	logi.logInfo "DB connected"
	List serviceNum1=[]
	List seq_num =[]
	List rec_num=[]
	double serviceamount,total_amount,activationamount
	def MP_units = getValueFromRequest('create_acct_complete',"//master_plan_units")
	logi.logInfo "MP_units : " +MP_units
	def plano = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICEPLANNO1)
	logi.logInfo "plan no : " +plano
	String invoice_no = "select min(invoice_no) from ariacore.gl where client_no ="+clientNo+ " and  acct_no =" +accountNo
	def invoice_query = db.executeQueryP2(invoice_no)
	
	String tax_seq_num = "select seq_num from ariacore.gl_detail where invoice_no in ("+invoice_no+") and service_no IN(400,401,402,403,405)"
	ResultSet rs1=db.executePlaQuery(tax_seq_num)
	logi.logInfo "Executed result set"
	while(rs1.next())
	{
		logi.logInfo" chk in"
		   logi.logInfo" Inside result set of seq_count"
		   String seq_no=rs1.getString(1)
		   seq_num.add(seq_no)
		   logi.logInfo "SeqList"+seq_num.toString()
		   logi.logInfo" chk out"
	}
	logi.logInfo "recuList"+seq_num.toString()
	
	String recu_seq_num = "select seq_num from ariacore.gl_detail where invoice_no in ("+invoice_no+") and rate_seq_no = 1 "
	ResultSet rs2=db.executePlaQuery(recu_seq_num)
	logi.logInfo "Executed result set"
	while(rs2.next())
	{
		logi.logInfo" chk in"
		   logi.logInfo" Inside result set of recu_count"
		   String rec_no=rs2.getString(1)
		   rec_num.add(rec_no)
		   logi.logInfo "recuList"+rec_num.toString()
		   logi.logInfo" chk out"
	}
	logi.logInfo "recuList"+rec_num.toString()
	
	def  total_num = []
	def  totalrecu1 = []
	double totalrecu =0
	double outtertotal =0
	def totalrecvalue

	for(int k=0;k<rec_num.size();k++)
	{
		logi.logInfo "recsize"+rec_num.size()
		logi.logInfo"check in"
		logi.logInfo "Enter service" + rec_num.get(k)
		logi.logInfo "Enter loop count" + k
		for(int j=0; j<seq_num.size();j++)	
		{
			logi.logInfo "recsize"+seq_num.size()
			logi.logInfo "Enter loop count" + j
			logi.logInfo"j value "+seq_num.toString()
			logi.logInfo"k value "+rec_num.toString()
			logi.logInfo"k value "+rec_num.get(k).toString()
			logi.logInfo"j value "+seq_num.get(j).toString()
     	
			String recu_seq ="select NVL(SUM(DEBIT),0) from ARIACORE.gl_tax_detail where invoice_no in ("+invoice_no+") and taxed_seq_num = "+rec_num.get(k).toString()+" and seq_num  = "+seq_num.get(j).toString()
			def recu_rate = db.executeQueryP2(recu_seq)
			logi.logInfo"check in "+recu_rate
			logi.logInfo"check in "+totalrecu
			totalrecu = totalrecu.toDouble() + recu_rate.toDouble()
			totalrecu1[k] = totalrecu.toDouble() 		
			logi.logInfo"rec_value "+df.format(totalrecu1[k]).toDouble()
			logi.logInfo"check out "+df.format(totalrecu).toDouble()
			logi.logInfo "exit loop count k " + k
			logi.logInfo "exit loop count j " + j
		
		}
		
		outtertotal =  outtertotal.toDouble() + df.format(totalrecu1[k]).toDouble()
		total_num[k] =  df.format(outtertotal).toDouble()
		logi.logInfo"total_rec_value "+total_num[k]
		totalrecvalue = total_num[k]
		totalrecu =0		
	}
	
		logi.logInfo "Executed enter set"
    String service_count = "SELECT distinct(rate_seq_no) FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO =" +accountNo +" AND PLAN_NO = "+plano
	ResultSet rs3=db.executePlaQuery(service_count)
	logi.logInfo "Executed result set"
	while(rs3.next())
	{
		   logi.logInfo" Inside result set of service_count"
		   String Service_no=rs3.getString(1)
		   serviceNum1.add(Service_no)
	}
	logi.logInfo "ServiceList"+serviceNum1.toString()
	def servicecount = serviceNum1.size()
	logi.logInfo "servicesize"+servicecount
	int activation_unit = 1
	 String service = getValueFromResponse('create_acct_complete',"//*[1]/*[local-name()='invoice_service_no']")
	 logi.logInfo"after API => "+service
	 String sch_no1 =" SELECT NVL(RATE_SCHEDULE_NO,0)  FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = " +accountNo +" AND PLAN_NO= "+plano + " and service_no ="+service+" and RATE_SEQ_NO = 1 and client_no =" +clientNo
	 def sch_number1 = db.executeQueryP2(sch_no1)
	 String rateamount1 = "SELECT NVL(RATE_PER_UNIT,0) FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO  =" +sch_number1+" and service_no ="+service
	 def rate_number1 = db.executeQueryP2(rateamount1)
	 activationamount = (activation_unit * rate_number1.toDouble())
	 logi.logInfo"activation => "+df.format(activationamount).toDouble()
	 
	for(int k=1;k<=servicecount-1;k++)
	{
		
		int d = k-1
		double total_temp = totalrecu1[d].toDouble()
		logi.logInfo"check in"+df.format(total_temp).toDouble()
		double tmp = df.format(total_temp).toDouble()
		logi.logInfo"check in"+tmp
	 	logi.logInfo "enter loop count k " + k
		logi.logInfo "Enter service" + serviceNum1.get(k)
		k =k+1
		String service_api = getValueFromResponse('create_acct_complete',"//*["+k+"]/*[local-name()='invoice_service_no']")
		logi.logInfo"after API => "+service_api
		String sch_no =" SELECT NVL(RATE_SCHEDULE_NO,0)  FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = " +accountNo +" AND PLAN_NO= "+plano + " and service_no ="+service_api+" and RATE_SEQ_NO = 1 and client_no =" +clientNo
		def sch_number = db.executeQueryP2(sch_no)
		String rateamount = "SELECT NVL(RATE_PER_UNIT,0) FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO  =" +sch_number+" and service_no ="+service_api
		def rate_number = db.executeQueryP2(rateamount)
		serviceamount = ((MP_units.toDouble() * rate_number.toDouble()) - (tmp))
		k=k-1
	
		logi.logInfo"after API => "+df.format(serviceamount).toDouble()
		total_amount = total_amount.toDouble() + df.format(serviceamount).toDouble()
		logi.logInfo"after API => "+total_amount.toDouble()
		logi.logInfo"check out"
		
	
	}
	total_amount = ((total_amount + activationamount) + (totalrecvalue.toDouble()))
	logi.logInfo"outsidefor API => "+total_amount
	return df.format(total_amount.toDouble()).toString()

}


String md_proration_result_amount_cal(String testCaseId){
	logi.logInfo('Inside md_proration_result_amount ' )
	ConnectDB db=new ConnectDB()
	String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	String result
	List credit_num =[]
	double total_app_amt =0
	double totalamt =0
	int credit_cnt = db.executeQueryP2("Select count(distinct credit_id) from ariacore.all_credits WHERE acct_no ="+accountNo).toInteger()
	if(credit_cnt >= 1)
	{
	
	String credit_id = "Select distinct (credit_id) from ariacore.all_credits WHERE acct_no ="+accountNo
	ResultSet rs1=db.executePlaQuery(credit_id)
	logi.logInfo "Executed result set"
	while(rs1.next())
	{
		   logi.logInfo" Inside result set of credit_count"
		   String credit_no=rs1.getString(1)
		   credit_num.add(credit_no)
	}
	logi.logInfo "credit List"+credit_num.toString()
	for (int k =0;k<credit_cnt;k++)
	{
		
		String amt_cal = "SELECT sum(amount) FROM ariacore.all_credits where acct_no = "+accountNo+" AND credit_id  = "+credit_num.get(k).toString()
		def amt_number = db.executeQueryP2(amt_cal)

		totalamt = totalamt.toDouble() + amt_number.toDouble()
		logi.logInfo "total amt" +totalamt
		
	}
	
	for (int k =0;k<credit_cnt;k++)
	{
		
		String amt_cal = "SELECT sum(total_applied) FROM ariacore.all_credits where acct_no = "+accountNo+" AND credit_id  = "+credit_num.get(k).toString()
		def app_amt_number = db.executeQueryP2(amt_cal)

		total_app_amt = total_app_amt.toDouble() + app_amt_number.toDouble()
		logi.logInfo "total amt" +total_app_amt
		
	}
	
	double lefttoapply = totalamt - total_app_amt
	logi.logInfo "left inside amt" +lefttoapply.toString()
	result = df.format(lefttoapply).toString()
	logi.logInfo "left amt" +result
	
	}
	
	else
		result = db.executeQueryP2("Select NVL(sum(amount),0) FROM ariacore.all_credits where acct_no ="+accountNo+" and credit_id= (SELECT case when MIN(credit_id) is null then  0 else MIN(credit_id) end  FROM ariacore.all_credits WHERE acct_no ="+accountNo+")").toString()
	
	if(result.equals("null"))
	result=0
	return ""+result
}



def md_tax_cal_amount_update_API(String testCaseId)
{
	logi.logInfo "md_tax_cal_amount_update_API from API"
	String apiname=testCaseId.split("-")[2]
	HashMap cac_api=new HashMap()

	String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
	String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
	ConnectDB db = new ConnectDB();
	DecimalFormat df = new DecimalFormat("#.##");
	double noth
	logi.logInfo "DB connected"
	List serviceNum=[]
	List seqNum=[]
	String invoice_no = "select invoice_no from ariacore.gl where client_no ="+clientNo+ "and  acct_no =" +accountNo
	String Service_Query =  "SELECT service_no FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no IN(400,401,402,403,405)"
	ResultSet rs1=db.executePlaQuery(Service_Query)
	logi.logInfo "Executed result set"
	while(rs1.next())
	{
		   logi.logInfo" Inside result set of Service number"
		   String Service_no=rs1.getString(1)
		   serviceNum.add(Service_no)
	}
	logi.logInfo "ServiceList"+serviceNum.toString()
	
	String Seq_Query =  "SELECT seq_num FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no IN(400,401,402,403,405)"
	ResultSet rs=db.executePlaQuery(Seq_Query)
	logi.logInfo "Executed result set"
	while(rs.next())
	{
		   logi.logInfo" Inside result set of SEQ number"
		   String Seq_no=rs.getString(1)
		   seqNum.add(Seq_no)
	}
	logi.logInfo "seqlist"+seqNum.toString()
	
	for(int k=0;k<serviceNum.size();k++)
	{
		logi.logInfo "Enter service" + serviceNum.get(k)
		logi.logInfo("before API=> "+seqNum.get(k).toString())
		String amt_api = getValueFromResponse(apiname,"//*["+seqNum.get(k)+"]/*[local-name()='line_amount']")
		logi.logInfo("before API2 => "+seqNum.get(k).toString())
		logi.logInfo("after API => "+amt_api)
		double convert = amt_api.toDouble()
				
		if(serviceNum.get(k).toInteger() == 400)
		{
			if (amt_api != 0)
			{
			cac_api.put("Federal_TAX", convert.toString())
			}
			else
			{
				cac_api.put("Federal_TAX",  noth.toString())
			}
		}
		else if(serviceNum.get(k).toInteger() == 401)
		{
			if(amt_api != 0)
			{
			cac_api.put("State_TAX", convert.toString())
			}
			else
			{
				cac_api.put("State_TAX", noth.toString())
			}
		}
		else if(serviceNum.get(k).toInteger() == 402)
		{
			if(amt_api != 0)
			{
			cac_api.put("COUNTRY_TAX", convert.toString())
			}
			else
			{
				cac_api.put("COUNTRY_TAX", noth.toString())
			}
		}
		else if(serviceNum.get(k).toInteger() == 403)
		{
			if(amt_api != 0)
			{
			cac_api.put("CITY_TAX", convert.toString())
			}
			else
			{
				cac_api.put("CITY_TAX", noth.toString())
			}
		}
		else if(serviceNum.get(k).toInteger() == 405)
		{
			if(amt_api != 0)
			{
			cac_api.put("District_TAX", convert.toString())
			}
			else
			{
				cac_api.put("District_TAX", noth.toString())
			}
		}
	}
	logi.logInfo("md_tax_cal_amount from API=> "+cac_api)
	return cac_api.sort()
}

String md_get_Tax_Error_details(String testCaseId)
{
	logi.logInfo "Inside md_get_Tax_Error_details"
	String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
	String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
	String invoice_count = 5
	String error_cd = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_TAXERRORCODE)
	logi.logInfo "Verify the error code"+error_cd
	ConnectDB db = new ConnectDB();
	Map<String, String> suppFieldValues = new TreeMap<String, String>()
	String STOP_ON_BATCH_INV_TAX_ERR = db.executeQueryP2("select * from ariacore.aria_client_params where param_name = 'STOP_ON_BATCH_INV_TAX_ERR' and client_no=" +clientNo)
	logi.logInfo "Inside STOP_ON_BATCH_INV_TAX_ERR" + STOP_ON_BATCH_INV_TAX_ERR
	if(STOP_ON_BATCH_INV_TAX_ERR == null)
	{
		logi.logInfo "Inside Sedsfdsgfd"
		STOP_ON_BATCH_INV_TAX_ERR = 'TRUE'
			 
	}
	logi.logInfo "Inside STOP_ON_BATCH_INV_TAX_ERR Default" + STOP_ON_BATCH_INV_TAX_ERR
	String STOP_ON_IMMEDIATE_INV_TAX_ERR = db.executeQueryP2("select * from ariacore.aria_client_params where param_name = 'STOP_ON_IMMEDIATE_INV_TAX_ERR' and client_no=" +clientNo)
	logi.logInfo "Inside STOP_ON_IMMEDIATE_INV_TAX_ERR" + STOP_ON_IMMEDIATE_INV_TAX_ERR
	if ( (STOP_ON_BATCH_INV_TAX_ERR == 'FALSE') && (STOP_ON_IMMEDIATE_INV_TAX_ERR = 'FALSE'))
	{
		logi.logInfo "Entering into the both false method"
		if (error_cd == 'NO NODE')
		{
			invoice_count = db.executeQueryP2("select count(invoice_no) from ariacore.gl where account_no="+ accountNo+ "and client_no=" +clientNo)
		}
	}
	else if ( (STOP_ON_BATCH_INV_TAX_ERR == 'TURE') && (STOP_ON_IMMEDIATE_INV_TAX_ERR = 'TRUE'))
	{
		logi.logInfo "Entering into the both TRUE method"
		if (error_cd != 'NO NODE')
		{
			invoice_count = 0
		}
		
	}
	
	return invoice_count

}

String md_get_Tax_Error_details1(String testCaseId)
{
	logi.logInfo "Inside md_get_Tax_Error_details"
	String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
	String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
	String invoice_count = 5
	String error_cd = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_TAXERRORCODE)
	logi.logInfo "Verify the error code"+error_cd
	ConnectDB db =new ConnectDB()
	Map<String, String> suppFieldValues = new TreeMap<String, String>()
	
	String STOP_ON_IMMEDIATE_INV_TAX_ERR = db.executeQueryP2("select Param_val from ariacore.aria_client_params where param_name = 'STOP_ON_IMMEDIATE_INV_TAX_ERR' and client_no=" +clientNo)
	logi.logInfo "Inside STOP_ON_IMMEDIATE_INV_TAX_ERR" + STOP_ON_IMMEDIATE_INV_TAX_ERR
	if (STOP_ON_IMMEDIATE_INV_TAX_ERR == 'TRUE')
	{
		logi.logInfo "Entering into the both true method"
		if (error_cd != 'NO NODE')
		{
			invoice_count = 0
		}
	}
	
	return invoice_count

}

def md_tax_cal_amount_Update_DB(String testCaseId)
{
	
	logi.logInfo "md_tax_cal_amount from DB"
	String apiname=testCaseId.split("-")[2]
	HashMap row = new HashMap();
	
	String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
	String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
	ConnectDB db = new ConnectDB();
	double noth= 0.0
	List serviceNum=[]
	String invoice_no = "select invoice_no from ariacore.gl where client_no ="+clientNo+ "and  acct_no =" +accountNo
	double invoice = (db.executeQueryP2(invoice_no)).toDouble()
	logi.logInfo "invoice"+invoice
	String Service_Query =  "SELECT service_no FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no IN(400,401,402,403,405)"
	ResultSet rs1=db.executePlaQuery(Service_Query)
	logi.logInfo "Executed result set"
			
	while(rs1.next())
	{
		   logi.logInfo" Inside result set of Service number"
		   String Service_no=rs1.getString(1)
		   serviceNum.add(Service_no)
	}
	logi.logInfo "ServiceList"+serviceNum.toString()
	for(int k=0;k<serviceNum.size();k++)
	{
	
		if(serviceNum.get(k).toInteger() == 400)
			{
				String federal_Query =  "SELECT NVL(debit,0) FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no = 400 AND usage_rate is not null order by invoice_no desc "
				double fedval = (db.executeQueryP2(federal_Query)).toDouble()
				if(fedval == null)
				{
					fedval=0
				}
				row.put("Federal_TAX",fedval.toString())
			}
			else if(serviceNum.get(k).toInteger() == 401)
			{
				String state_Query =  "SELECT NVL(debit,0) FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no = 401 AND usage_rate is not null order by invoice_no desc "
				double staval = (db.executeQueryP2(state_Query)).toDouble()
				if(staval == null)
				{
					staval=0
				}
				row.put("State_TAX",staval.toString())
			}
			else if(serviceNum.get(k).toInteger() == 402)
			{
				String country_Query =  "SELECT NVL(debit,0) FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no = 402 AND usage_rate is not null order by invoice_no desc "
				double conval = (db.executeQueryP2(country_Query)).toDouble()
				if(conval == null)
				{
					conval=0
				}
				row.put("COUNTRY_TAX",conval.toString())
			}
			else if(serviceNum.get(k).toInteger() == 403)
			{
				String city_Query =  "SELECT NVL(debit,0) FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no = 403 AND usage_rate is not null order by invoice_no desc "
				double citval = (db.executeQueryP2(city_Query)).toDouble()
				if(citval == null)
				{
					citval=0
				}
				row.put("CITY_TAX",citval.toString())
			}
			else if(serviceNum.get(k).toInteger() == 405)
			{
				String district_Query =  "SELECT NVL(debit,0) FROM ariacore.gl_detail WHERE invoice_no IN ("+invoice_no+")AND service_no = 405 AND usage_rate is not null order by invoice_no desc "
				double disval = (db.executeQueryP2(district_Query)).toDouble()
				if(disval == null)
				{
					disval=0
				}
				row.put("District_TAX",disval.toString())
			}
		
	}
	logi.logInfo("md_tax_cal_amount from DB=> "+row)
	return row.sort()
			
}


def md_get_aria_xml_statement_response_XML(String testCaseId)
{
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String out_statement_temp = getValueFromResponse("get_aria_xml_statement", ExPathRpcEnc.GET_ARIA_XML_STATEMENT_OUT_STATEMENT)
		String plan_no = getValueFromRequest("create_acct_complete", "//master_plan_no")
		logi.logInfo("Plan No : " + plan_no)
		def plan_name
		logi.logInfo("out statement111 : " + out_statement_temp)
		
		//String prefix = '"""'
		//String out_statement = (prefix.concat(out_statement_temp)).concat('"""')
		
		//out_statement = "'''"+out_statement+"'''"
		out_statement_temp=out_statement_temp.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>","")
		out_statement_temp = out_statement_temp.replaceAll("Usgae", "Usage")
		logi.logInfo("out statement after adding quotes: " + out_statement_temp)
		HashMap <String, String> resultHash = new HashMap<String, String>()
		def doc = new XmlSlurper().parseText(out_statement_temp)
			logi.logInfo("After parse12 " + doc)
		ConnectDB db = new ConnectDB()
		String query = db.executeQueryP2("select count(service_no) from ariacore.plan_services where plan_no =" + plan_no)
		logi.logInfo("count No : " + query)
		int count1 = query.toInteger()
		for (int i = 0 ; i < count1; i++)
		{
			 plan_name = doc.invoices.invoice.invoice_line_item_charges.invoice_line_item[i].plan_name.text().trim()
			 resultHash.put("Service " + i + " Plan Name", plan_name )
		}
		
		return resultHash.sort()
		
	}
def md_get_aria_xml_statement_response_Planno_XML(String testCaseId)
{
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String out_statement_temp = getValueFromResponse("get_aria_xml_statement", ExPathRpcEnc.GET_ARIA_XML_STATEMENT_OUT_STATEMENT)
		String plan_no = getValueFromRequest("create_acct_complete", "//master_plan_no")
		logi.logInfo("Plan No : " + plan_no)
		def plan_name
		logi.logInfo("out statement111 : " + out_statement_temp)
		
		//String prefix = '"""'
		//String out_statement = (prefix.concat(out_statement_temp)).concat('"""')
		
		//out_statement = "'''"+out_statement+"'''"
		out_statement_temp=out_statement_temp.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>","")
		out_statement_temp = out_statement_temp.replaceAll("Usgae", "Usage")
		logi.logInfo("out statement after adding quotes: " + out_statement_temp)
		HashMap <String, String> resultHash = new HashMap<String, String>()
		def doc = new XmlSlurper().parseText(out_statement_temp)
			logi.logInfo("After parse12 " + doc)
		ConnectDB db = new ConnectDB()
		String query = db.executeQueryP2("select count(service_no) from ariacore.plan_services where plan_no =" + plan_no)
		logi.logInfo("count No : " + query)
		int count1 = query.toInteger()
		for (int i = 0 ; i < count1; i++)
		{
			 plan_name = doc.invoices.invoice.invoice_line_item_charges.invoice_line_item[i].plan_no.text().trim()
			 resultHash.put("Service " + i + " Plan No", plan_name )
		}
		
		return resultHash.sort()
		
	}
def md_get_aria_xml_statement_response_ServiceNo_XML(String testCaseId)
{
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String out_statement_temp = getValueFromResponse("get_aria_xml_statement", ExPathRpcEnc.GET_ARIA_XML_STATEMENT_OUT_STATEMENT)
		String plan_no = getValueFromRequest("create_acct_complete", "//master_plan_no")
		logi.logInfo("Plan No : " + plan_no)
		def plan_name
		logi.logInfo("out statement111 : " + out_statement_temp)
		
		//String prefix = '"""'
		//String out_statement = (prefix.concat(out_statement_temp)).concat('"""')
		
		//out_statement = "'''"+out_statement+"'''"
		out_statement_temp=out_statement_temp.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>","")
		out_statement_temp = out_statement_temp.replaceAll("Usgae", "Usage")
		logi.logInfo("out statement after adding quotes: " + out_statement_temp)
		HashMap <String, String> resultHash = new HashMap<String, String>()
		def doc = new XmlSlurper().parseText(out_statement_temp)
			logi.logInfo("After parse12 " + doc)
		ConnectDB db = new ConnectDB()
		String query = db.executeQueryP2("select count(service_no) from ariacore.plan_services where plan_no =" + plan_no)
		logi.logInfo("count No : " + query)
		int count1 = query.toInteger()
		for (int i = 0 ; i < count1; i++)
		{
			 plan_name = doc.invoices.invoice.invoice_line_item_charges.invoice_line_item[i].service_no.text().trim()
			 resultHash.put("Service " + i + " Service No", plan_name )
		}
		
		return resultHash.sort()
		
	}
def md_get_aria_xml_statement_response_ProrationFactor_XML(String testCaseId)
{
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		DecimalFormat d = new DecimalFormat("#.##");
		String out_statement_temp = getValueFromResponse("get_aria_xml_statement", ExPathRpcEnc.GET_ARIA_XML_STATEMENT_OUT_STATEMENT)
		String plan_no = getValueFromRequest("create_acct_complete", "//master_plan_no")
		logi.logInfo("Plan No : " + plan_no)
		def plan_name
		logi.logInfo("out statement111 : " + out_statement_temp)
		
		//String prefix = '"""'
		//String out_statement = (prefix.concat(out_statement_temp)).concat('"""')
		
		//out_statement = "'''"+out_statement+"'''"
		out_statement_temp=out_statement_temp.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>","")
		out_statement_temp = out_statement_temp.replaceAll("Usgae", "Usage")
		logi.logInfo("out statement after adding quotes: " + out_statement_temp)
		HashMap <String, String> resultHash = new HashMap<String, String>()
		def doc = new XmlSlurper().parseText(out_statement_temp)
			logi.logInfo("After parse12 " + doc)
		ConnectDB db = new ConnectDB()
		String query = db.executeQueryP2("select count(service_no) from ariacore.plan_services where plan_no =" + plan_no)
		logi.logInfo("count No : " + query)
		int count1 = query.toInteger()
		for (int i = 0 ; i < count1; i++)
		{
			 plan_name = doc.invoices.invoice.invoice_line_item_charges.invoice_line_item[i].proration_factor.text().trim()
			 resultHash.put("Service " + i + " Service No", plan_name )
		}
		
		return resultHash.sort()
		
	}
def md_get_aria_xml_statement_response_XML_DB(String testCaseId)
{
	String plan_no = getValueFromRequest("create_acct_complete", "//master_plan_no")
	logi.logInfo("Plan No : " + plan_no)
	def plan_name
	HashMap <String, String> resultHash = new HashMap<String, String>()
	ConnectDB db = new ConnectDB()
	String query = db.executeQueryP2("select count(service_no) from ariacore.plan_services where plan_no =" + plan_no)
	logi.logInfo("count No : " + query)
	int count1 = query.toInteger()
	for (int i = 0 ; i < count1; i++)
		{
			 plan_name = db.executeQueryP2("select plan_name from ariacore.client_plan where plan_no =" + plan_no)
			 resultHash.put("Service " + i + " Plan Name", plan_name )
		}
	return resultHash.sort()
}
def md_get_aria_xml_statement_response_XML_Plan_no_DB(String testCaseId)
{
	String plan_no = getValueFromRequest("create_acct_complete", "//master_plan_no")
	logi.logInfo("Plan No : " + plan_no)
	def plan_name
	HashMap <String, String> resultHash = new HashMap<String, String>()
	ConnectDB db = new ConnectDB()
	String query = db.executeQueryP2("select count(service_no) from ariacore.plan_services where plan_no =" + plan_no)
	logi.logInfo("count No : " + query)
	int count1 = query.toInteger()
	for (int i = 0 ; i < count1; i++)
		{
			 plan_name = db.executeQueryP2("select plan_no from ariacore.client_plan where plan_no =" + plan_no)
			 resultHash.put("Service " + i + " Plan No", plan_name )
		}
	return resultHash.sort()
}
def md_get_aria_xml_statement_response_XML_ProrationFactor_DB(String testCaseId)
{
	String plan_no = getValueFromRequest("create_acct_complete", "//master_plan_no")
	def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	logi.logInfo("Plan No : " + plan_no)
	def plan_name
	HashMap <String, String> resultHash = new HashMap<String, String>()
	ConnectDB db = new ConnectDB()
	String query = db.executeQueryP2("select max(invoice_no) from ariacore.gl where acct_no =" + acct_no)
	logi.logInfo("count No : " + query)
	
	int i =0
	
			 String query1 = "select proration_factor from ariacore.gl_detail where invoice_no=" + query
			 ResultSet resultSet = db.executeQuery(query1);
			 while (resultSet.next())
			 {
				/*for (int i = 0 ; i< count1 ; i++)
				{*/
					logi.logInfo("Service No : " + resultSet.getString(1))
					resultHash.put("Service " + i + " Service No", resultSet.getString(1));
					i++
				//}
			 }
			 
		
	return resultHash.sort()
}
def md_get_aria_xml_statement_response_XML_Service_no_DB(String testCaseId)
{
	String plan_no = getValueFromRequest("create_acct_complete", "//master_plan_no")
	logi.logInfo("Plan No : " + plan_no)
	def plan_name
	HashMap <String, String> resultHash = new HashMap<String, String>()
	ConnectDB db = new ConnectDB()
	String query = db.executeQueryP2("select count(service_no) from ariacore.plan_services where plan_no =" + plan_no)
	logi.logInfo("count No : " + query)
	int count1 = query.toInteger()
	int i =0
	
			 String query1 = "select service_no from ariacore.plan_services where plan_no=" + plan_no
			 ResultSet resultSet = db.executeQuery(query1);
			 while (resultSet.next())
			 {
				/*for (int i = 0 ; i< count1 ; i++)
				{*/
					logi.logInfo("Service No : " + resultSet.getString(1))
					resultHash.put("Service " + i + " Service No", resultSet.getString(1));
					i++
				//}
			 }
			 
		
	return resultHash.sort()
}

/**
 * Method to verify the below information are stored in gl_tax_detail table
 * TaxName 
 * JurisType
 * JurisName from external tax engine responses 
 * @param testCaseId
 * @return
 */
def md_verifyJurisNameInTaxtable(String testCaseId){
	logi.logInfo("Verify the TaxName, JurisType, JurisName data from external tax engine responses")
	HashMap <String, String> resultHash = new HashMap<String, String>()
	def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	def acct_no=Constant.mycontext.expand('${Properties#AccountNo}')
	ConnectDB db = new ConnectDB()
	ResultSet rs=db.executePlaQuery("select TAX_AUTHORITY,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAXED_SEQ_NUM from ariacore.gl_tax_detail where invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+acct_no+")" )
	logi.logInfo("count No : " + query)
	int count=1
	while(rs.next())
	{
		logi.logInfo" Inside result set of Verification....."
		if(rs.getString(1)!=null&&rs.getString(2)!=null&&rs.getString(3)!=null){
			logi.logInfo "Details are corerctly stored for the line item "+count
			resultHash.put(rs.getString(3)+" TaxName for Invoice Line Item" + rs.getString(4), rs.getString(1));
			resultHash.put(rs.getString(3)+" JurisType for Invoice Line Item" + rs.getString(4), rs.getString(2));
			resultHash.put(rs.getString(3)+" JurisName for Invoice Line Item" + rs.getString(4), rs.getString(3));
		}
		else{
			resultHash.put("DB has no data JurisName,JurisType",null)
		}
		count++
	}
	return resultHash
}
def md_verifyJurisNameInTaxtableEXP(String testCaseId){
	logi.logInfo("Verify the TaxName, JurisType, JurisName data from external tax engine responses")
	HashMap <String, String> resultHash = new HashMap<String, String>()
	def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	def acct_no=Constant.mycontext.expand('${Properties#AccountNo}')
	ConnectDB db = new ConnectDB()
	ResultSet rs=db.executePlaQuery("select TAX_AUTHORITY,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAXED_SEQ_NUM from ariacore.gl_tax_detail where invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+acct_no+")" )
	logi.logInfo("count No : " + query)
	int count=1
	while(rs.next())
	{
		logi.logInfo" Inside result set of Verification....."
		if(rs.getString(1)!=null&&rs.getString(2)!=null&&rs.getString(3)!=null){
			logi.logInfo "Details are corerctly stored for the line item "+count
			resultHash.put(rs.getString(3)+" TaxName for Invoice Line Item" + rs.getString(4), rs.getString(1));
			resultHash.put(rs.getString(3)+" JurisType for Invoice Line Item" + rs.getString(4), rs.getString(2));
			resultHash.put(rs.getString(3)+" JurisName for Invoice Line Item" + rs.getString(4), rs.getString(3));
		}
		else{
			resultHash.put("DB has no data JurisName,JurisType","No Data")
		}
		count++
	}
	return resultHash
}



}