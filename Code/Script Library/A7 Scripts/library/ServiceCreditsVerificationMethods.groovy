package library

import java.awt.geom.Arc2D.Double;
import java.lang.reflect.Method
import java.sql.ResultSet;
import java.sql.ResultSetMetaData
import java.sql.SQLException
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import groovy.xml.StreamingMarkupBuilder

import org.apache.ivy.core.module.descriptor.ExtendsDescriptor;

import library.Constants.Constant;
import library.Constants.ExpValueConstants;
import library.Constants.ExPathRpcEnc;
import library.Constants.Constant.RESULT_TYPE;
import library.VerificationMethods
import library.ReadData

public class ServiceCreditsVerificationMethods extends TaxationVerificationMethods
{
	LogResult logi = new LogResult(Constant.logFilePath)
	static ConnectDB db = null;
	public DecimalFormat df=new DecimalFormat("#.##")
	

	public ServiceCreditsVerificationMethods(ConnectDB db) {
		super(db)
	}
	
	public String md_Expinvoice_final_amount(String testcaseid)
	{
	
		DecimalFormat d = new DecimalFormat("#.##########");
		def credit_amt
		def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		if(acct_no == 'NoVal')
		{
			 acct_no = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		def total_amount =md_ExpInvoiceWithUsage_Amt_WithServiceCredit(testcaseid).toDouble()
		logi.logInfo("Total usage invoice Amount" + total_amount )
		logi.logInfo("Before query execution" )
		String query2 = "Select currency_cd from ariacore.acct where acct_no = " + acct_no
		String acct_curcd = db.executeQueryP2(query2)
		logi.logInfo("acct_curcd" + acct_curcd )
		String query3 = "select currency_cd from ariacore.credits where acct_no = " + acct_no
		String credit_curcd = db.executeQueryP2(query3)
		if (acct_curcd == credit_curcd)
		{
			String query = "select NVL((amount),0) from ariacore.credits where credit_id = (select max(credit_id) from ariacore.credits where acct_no = " + acct_no + ")"
			logi.logInfo(query)
			String credit = db.executeQueryP2(query)
			 credit_amt = credit.toDouble()
			logi.logInfo("Total Credit amount"+ credit)
		}
		else
		{
			  credit_amt = 0
		}	 
		def total_tax = md_total_tax_amount(total_amount)
		logi.logInfo("Total tax amount"+ total_tax)
		
		def invoice_amt = (total_amount + total_tax.toDouble() - credit_amt)
		logi.logInfo("Total tax amount"+ invoice_amt)
		return d.format(invoice_amt).toString()
	}
	public String md_Expinvoice_final_amountCR1(String testcaseid)
	{
	
		DecimalFormat d = new DecimalFormat("#.##########");
		def credit_amt
		def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		if(acct_no =='NoVal')
		{
			 acct_no = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		def total_amount =md_ExpInvoiceWithUsage_Amt_WithServiceCredit(testcaseid).toDouble()
		logi.logInfo("Total usage invoice Amount" + total_amount )
		logi.logInfo("Before query execution" )
		String query2 = "Select currency_cd from ariacore.acct where acct_no = " + acct_no
		String acct_curcd = db.executeQueryP2(query2)
		logi.logInfo("acct_curcd" + acct_curcd )
		String query3 = "select currency_cd from ariacore.credits where acct_no = " + acct_no
		String credit_curcd = db.executeQueryP2(query3)
		if (acct_curcd == credit_curcd)
		{
			String query = "select NVL((amount),0) from ariacore.credits where credit_id = (select max(credit_id) from ariacore.credits where acct_no = " + acct_no + ")"
			logi.logInfo(query)
			String credit = db.executeQueryP2(query)
			 credit_amt = credit.toDouble()
			logi.logInfo("Total Credit amount"+ credit)
		}
		else
		{
			  credit_amt = 0
		}
		def total_tax_amount = total_amount - credit_amt
		logi.logInfo("total_tax_amount" + total_tax_amount )
		def total_tax = md_total_tax_amount(total_tax_amount)
		logi.logInfo("Total tax amount"+ total_tax)
		
		def invoice_amt = (total_amount + total_tax.toDouble() - credit_amt)
		logi.logInfo("Total tax amount"+ invoice_amt)
		return d.format(invoice_amt).toString()
	}
	
	public String md_total_tax_amount(double amount)
	{
		logi.logInfo("Entering into the tax amount")
		DecimalFormat d = new DecimalFormat("#.##########");
		
		def State_tax = (amount * 0.2).toDouble()
		def federal_tax = (amount * 0.1).toDouble()
		def total_tax = (State_tax + federal_tax).toDouble()
		return d.format(total_tax).toString()
	}
	
	
	String md_ExpInvoiceWithUsage_Amt_WithServiceCredit(String testcaseid)
	{
		def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		if(acct_no == 'NoVal')
		{
			 acct_no = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		return md_ExpInvoiceWithUsage_Amt_WithServiceCredit(testcaseid,acct_no)
	}
	
	public String md_ExpInvoice_Usage_amt(String testcaseid)
	{
		ConnectDB db = new ConnectDB()
		def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		if(acct_no == 'NoVal')
		{
			acct_no = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		String query = "Select sum(amt) from ariacore.usage where invoice_no = (select max(invoice_no) from ariacore.gl where acct_no=" + acct_no + " )"
		String Exp_usage = db.executeQueryP2(query)
		logi.logInfo("Exp_usage" + Exp_usage)
		return (Exp_usage)
	}
	
	public String md_ExpInvoiceWithUsage_Amt_WithServiceCredit(String testCaseId,String acct_no)
	{
		logi.logInfo("calling md_ExpInvoiceWithUsage_Amt")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##########");
		String apiname=testCaseId.split("-")[2]
		double totalInvoice=0.0
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no).toString();
		logi.logInfo( "invoiceNo in md_ExpInvoiceWithUsage_Amt method " +invoiceNo)
		
		double exp = md_ExpInvoice_Amt(testCaseId,acct_no).toDouble()
		logi.logInfo("expInvoice "+exp.toString())
	
		logi.logInfo "Incoming acct"+acct_no
	
		List<String> usagekeys=[]
		Constant.recordedusageamounts.each
		{ k,v ->
			logi.logInfo "key now in hash "+k.split("-")[3]
			if(k.split("-")[3].equals(acct_no)==true)
	
			{
				//usg=usg+v.toString().toDouble()
				usagekeys.add(k)
			}
	
		}
		logi.logInfo("usagekeys list  "+usagekeys.toString())
	
		boolean auto_rateFlg = get_Auto_Rate_Unrated_Usage_Flag()
		String usg
		if(!auto_rateFlg){
			usg = md_ExpInvoice_Usage_amt(testCaseId)
			//md_ExpUsage_Amt_With_Auto_Rate(testCaseId)
		} else
			usg=md_ExpInvoice_Usage_amt(testCaseId)
		logi.logInfo("usgInvoice "+usg.toString())
		double creditamt=db.executeQueryP2("select NVL(sum(credit),0) from ariacore.gl where acct_no="+acct_no+" and client_no="+clientNo+" and invoice_no="+invoiceNo).toString().toDouble()
	
		String resp_level_cd = db.executeQueryP2("select resp_level_cd from ariacore.acct where acct_no="+acct_no+" and client_no="+clientNo).toString()
		if(resp_level_cd == '2' || !getValueFromRequest("create_acct_complete","//collections_acct_groups").equals("NoVal") || !getValueFromRequest("create_acct_complete","//coupon_codes").equals("NoVal") ||getValueFromResponse("create_acct_complete","//acct_no").equals("NoVal"))
			creditamt = 0.0
		totalInvoice =  exp+ (usg.toDouble().round(2))-creditamt.round(2)
		logi.logInfo("totalInvoice "+totalInvoice)
		return (totalInvoice).toString()
	}
	
	String md_couponApplied_check(String testCaseId)
	{
		String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = getValueFromRequest("create_acct_complete","//client_no")
		ConnectDB db = new ConnectDB()
		String flag='FALSE'
		String invoice_no = "SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE CLIENT_NO ="+clientNo+ "AND ACCT_NO =" +accountNo
		String invoiceNo=db.executeQueryP2(invoice_no)
		String couponqry = "SELECT COUPON_CD FROM ariacore.ACCT_COUPONS WHERE ACCT_NO="+accountNo
		String coupon_cd=db.executeQueryP2(couponqry)
		String gl_query = "SELECT debit from ariacore.gl_detail where invoice_no= "+invoiceNo+ "and orig_coupon_cd= '"+coupon_cd+"'"
		String indicator=db.executeQueryP2(gl_query)
		if(indicator!=null)
		{
		flag='TRUE'
		}
		return flag
	}
	
	String md_coupon_amount_serviceCredit(String testCaseId)
	{
		logi.logInfo "Inside md_coupon_amount"

		String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String query1, query2
		ConnectDB dbConnection = new ConnectDB()
		query1="SELECT invoice_no from (SELECT invoice_no ,row_number() over (order by invoice_no desc ) as sno FROM ARIACORE.GL where acct_no=" +accountNo+ ")"
		String invoice_No = dbConnection.executeQueryP2(query1)
		logi.logInfo "Invoice no is" +invoice_No
		query2=" SELECT (SUM(DISTILLED_AMOUNT)) AS COUPON_AMT FROM (SELECT ELIGIBLE_SERVICE_NO, DISTILLED_AMOUNT FROM ariacore.all_recur_credit WHERE acct_no = (SELECT acct_no FROM ariacore.gl WHERE invoice_no="+invoice_No+") UNION SELECT CDR.KEY_2_VAL AS ELIGIBLE_SERVICE_NO, CASE WHEN CDRS.INLINE_OFFSET_IND='I' THEN 0 ELSE AIDD.ACTUAL_DISCOUNT_AMT END AS DISTILLED_AMOUNT FROM ARIACORE.CLIENT_DR_COMPONENTS CDR JOIN ariacore.all_invoice_discount_details AIDD ON AIDD.rule_no=CDR.rule_no JOIN ariacore.CLIENT_DISCOUNT_RULES CDRS ON CDRS.rule_no =AIDD.rule_no WHERE AIDD.invoice_no ="+invoice_No+")a"
		String coupon_amount = dbConnection.executeQueryP2(query2)
		if(coupon_amount==null)
		{
			return 0
		}
		else
		{
		logi.logInfo "Coupon amount is" +coupon_amount
		dbConnection.closeConnection()
		return coupon_amount
		}
	}
	
	
	/**
	 * Calculates the detail generated from get_recurring_credit_info in DB
	 * @param coupon details
	 * @return invoice items HashMap
	 */
	
	def md_get_recurring_credit_info_DB(String testCaseId)
	{
		logi.logInfo "md_get_recurring_credit_info_DB"
		String apiname=testCaseId.split("-")[2]
		HashMap row =new HashMap()
	
		String accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo  = getValueFromRequest("create_acct_complete","//client_no")
		ConnectDB db = new ConnectDB();
		DecimalFormat df = new DecimalFormat("#.##");
		List servicetype=[]
			
		String coupon_count = "SELECT count(recurring_credit_no) from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo
		def coupon_size = db.executeQueryP2(coupon_count)
		logi.logInfo"total coupon count "+coupon_size
		
		String Service_type =  "SELECT Service_type_cd from ariacore.RECUR_CREDIT_ELGBL_SRVC_TP where client_no = "+ clientNo + " and recurring_credit_no = (select recurring_credit_no from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo+")"
		ResultSet rs1=db.executePlaQuery(Service_type)
		logi.logInfo "Executed result set"
			
		while(rs1.next())
		{
		   logi.logInfo" Inside result set of Service_type"
		   String Service_cd = rs1.getString(1)
		   servicetype.add(Service_cd)
		}
		logi.logInfo "ServiceList"+servicetype.toString()
		
			
		
	for(int k=1;k<=coupon_size;k++)
	{
		String First_Credit_Date =  "SELECT to_char(First_credit_date,'yyyy-mm-dd') from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo
		String FC_date = db.executeQueryP2(First_Credit_Date)
		//String FC_date_format = df.format(FC_date.toString())
		row.put("First_Credit_Date",FC_date.toString())
		
		String Last_Credit_Date =  "SELECT to_char(last_credit_date,'yyyy-mm-dd') from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo
		String LC_date = db.executeQueryP2(Last_Credit_Date)
		row.put("Last_Credit_Date",LC_date.toString())
		
		String Next_Credit_Date =  "SELECT to_char(next_credit_date,'yyyy-mm-dd')  from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo
		String NC_date = db.executeQueryP2(Next_Credit_Date)
		row.put("Next_Credit_Date",NC_date.toString())
		
		String Currency_cd =  "SELECT currency_cd  from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo
		String Currency = db.executeQueryP2(Currency_cd)
		row.put("Currency_CD",Currency.toString())
		
		String num_credits_completed =  "SELECT num_credits_completed from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo
		String NCC = db.executeQueryP2(num_credits_completed)
		row.put("Num_Credits_Completed",NCC.toString())
		
		
		String num_credits_remaining =  "SELECT num_credits_remaining from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo
		String NCR = db.executeQueryP2(num_credits_remaining)
		row.put("Num_Credits_Remaining",NCR.toString())
		
		
		String credit_status_cd =  "SELECT Status_cd from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo
		String CSC = db.executeQueryP2(credit_status_cd)
		row.put("Credit_status_cd",CSC.toString())
					
		String credit_reason_cd =  "SELECT reason_cd from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo
		String CRC = db.executeQueryP2(credit_reason_cd)
		row.put("Credit_reason_cd",CRC.toString())
		
		String create_date =  "SELECT to_char(create_date,'yyyy-mm-dd') from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo
		String CDate = db.executeQueryP2(create_date)
		row.put("Create_date",CDate.toString())
				
		String update_date =  "SELECT to_char(update_date,'yyyy-mm-dd') from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo
		String UPDate = db.executeQueryP2(update_date)
		row.put("Update_date",UPDate.toString())
		
		
		String Credit_month_interval =  "SELECT credit_interval_months from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo
		String CMI = db.executeQueryP2(Credit_month_interval)
		row.put("Credit_interval_months",CMI.toString())
		
		List ser_seq_type=[]
		String service_type = "SELECT service_type_cd from ariacore.RECUR_CREDIT_ELGBL_SRVC_TP where client_no = "+ clientNo+" and recurring_credit_no =(select recurring_credit_no from ariacore.recurring_credits where acct_no = "+accountNo+" and client_no = "+ clientNo+")"
		ResultSet rs=db.executePlaQuery(service_type)
		while(rs.next())
		{
			   String service_type_values = rs.getString(1)
			   ser_seq_type.add(service_type_values)
			   logi.logInfo("Value of st is " + ser_seq_type)
		}
		StringBuilder commaSepValueBuilder = new StringBuilder()
		for ( int i = 0; i< ser_seq_type.size(); i++)
		{
			   commaSepValueBuilder.append(ser_seq_type.get(i))
			   if ( i != ser_seq_type.size()-1)
			   {
			   commaSepValueBuilder.append(" & ");
			   }
		 }
		logi.logInfo("And seperator " + commaSepValueBuilder.toString())
		row.put("Service_type_cd ",commaSepValueBuilder.toString())
			
	
		}
		
	logi.logInfo("md_get_recurring_credit_info_DB => "+row)
	return row.sort()
		
	}
	
	
	/**
	 * Calculates the detail generated from get_recurring_credit_info in API
	 * @param coupon details
	 * @return invoice items HashMap
	 */
	 
	
	def md_get_recurring_credit_info_API(String testCaseId)
	{
		logi.logInfo "md_get_recurring_credit_info_API"
		String apiname=testCaseId.split("-")[2]
		HashMap row =new HashMap()
	    
		String accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo  = getValueFromRequest("create_acct_complete","//client_no")
	
		ConnectDB db = new ConnectDB();
		DecimalFormat df = new DecimalFormat("#.##");

		String FC_date  = getValueFromResponse('get_recurring_credit_info',ExPathRpcEnc.GET_RECURRING_CREDIT_INFO_FIRST_CREDIT_DATE)
		logi.logInfo("first credit date  => "+FC_date.toString())
		row.put("First_Credit_Date",FC_date.toString())
		
		String LC_date  = getValueFromResponse('get_recurring_credit_info',ExPathRpcEnc.GET_RECURRING_CREDIT_INFO_LAST_CREDIT_DATE)
		logi.logInfo("Last credit date  => "+LC_date.toString())
		row.put("Last_Credit_Date",LC_date.toString())
		
		String NC_date  = getValueFromResponse('get_recurring_credit_info',ExPathRpcEnc.GET_RECURRING_CREDIT_INFO_NEXT_CREDIT_DATE)
		logi.logInfo("next credit date  => "+NC_date.toString())
		row.put("Next_Credit_Date",NC_date.toString())
		
		String CCD  = getValueFromResponse('get_recurring_credit_info',ExPathRpcEnc.GET_RECURRING_CREDIT_INFO_CURRENCY_CD )
		logi.logInfo("Currency_CD  => "+CCD.toString())
		row.put("Currency_CD",CCD.toString())
		
		String CD  = getValueFromResponse('get_recurring_credit_info',ExPathRpcEnc.GET_RECURRING_CREDIT_INFO_CREDITS_COMPLETED )
		logi.logInfo("Num_Credits_Completed  => "+CD.toString())
		row.put("Num_Credits_Completed",CD.toString())
		
		String CR  = getValueFromResponse('get_recurring_credit_info',ExPathRpcEnc.GET_RECURRING_CREDIT_INFO_CREDITS_REMAINING )
		logi.logInfo("Num_Credits_Remaining  => "+CR.toString())
		row.put("Num_Credits_Remaining",CR.toString())
		
		String CSC = getValueFromResponse('get_recurring_credit_info',ExPathRpcEnc.GET_RECURRING_CREDIT_INFO_CREDITSTATUSCD1 )
		logi.logInfo("credit_status_cd  => "+CSC.toString())
		row.put("Credit_status_cd",CSC.toString())
		
		String CRC = getValueFromResponse('get_recurring_credit_info',ExPathRpcEnc.GET_RECURRING_CREDIT_INFO_CREDITREASONCD1 )
		logi.logInfo("credit_reason_cd  => "+CRC.toString())
		row.put("Credit_reason_cd",CRC.toString())
		
		String CDATE = getValueFromResponse('get_recurring_credit_info',ExPathRpcEnc.GET_RECURRING_CREDIT_INFO_CREATE_DATE )
		logi.logInfo("create_date  => "+CDATE.toString())
		row.put("Create_date",CDATE.toString())
		
		String UPDATE = getValueFromResponse('get_recurring_credit_info',ExPathRpcEnc.GET_RECURRING_CREDIT_INFO_UPDATE_DATE )
		logi.logInfo("update_date  => "+UPDATE.toString())
		row.put("Update_date",UPDATE.toString())
		
		
		String CMI = getValueFromResponse('get_recurring_credit_info',ExPathRpcEnc.GET_RECURRING_CREDIT_INFO_CREDITINTERVALMONTHS1)
		logi.logInfo("update_user  => "+CMI.toString())
		row.put("Credit_interval_months",CMI.toString())
		
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap hm_coupons_details_api=new HashMap()
		com.eviware.soapui.support.XmlHolder outputholder
		for(int j=0; j<testCaseCmb.size(); j++)
		{
		 if(testCaseCmb[j].toString().contains('get_recurring_credit_info') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID)
		 {
		outputholder = xmlValues[j]
		List ser_seq_type=[]

		String typecount=getNodeCountFromResponse("get_recurring_credit_info","//*/*/*/*:eligible_service_types")
		logi.logInfo("API Service Types Count "+typecount)
		StringBuilder commaSepValueBuilder = new StringBuilder()
		for(int h = 1 ; h < typecount.toInteger() ; h++)
		{
		   logi.logInfo("Value of h is " +h)
		   ser_seq_type.add(outputholder.getNodeValue("//*/*/*/*:eligible_service_types/*["+h+"]/eligible_service_types"))
		   logi.logInfo("Service in sequence is " +ser_seq_type[h])
		}
	for ( int i = 0; i< ser_seq_type.size(); i++)
		{
		   commaSepValueBuilder.append(ser_seq_type.get(i))
		   if ( i != ser_seq_type.size()-1)
		   {
		 commaSepValueBuilder.append(" & ");
		}
		  }
		logi.logInfo("And seperator " + commaSepValueBuilder.toString())
		row.put("Service_type_cd ",commaSepValueBuilder.toString())
	
		 }
		}
				
		logi.logInfo("md_get_recurring_credit_info_DB => "+row)
		return row.sort()
		
	}
	
		
	
	public String md_ExpInvoice_assign_supp_plan_without_credits(String testCaseId)
	{
		logi.logInfo("calling md_ExpInvoiceWithoutUsage_Amt")
		String acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo  = getValueFromRequest("create_acct_complete","//client_no")
	
		double usg = 0.0
		double cal_usg
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##");
		String apiname=testCaseId.split("-")[2]
		double totalInvoice=0.0
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no)
		logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		def exp = md_assign_supp_Proration_amount(testCaseId,acct_no)
		logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		def taxamt =db.executeQueryP2("select NVL(sum(debit),0) from gl_detail where invoice_no =(select Max(invoice_no) from gl where acct_no ="+acct_no +") and service_no in(400,401,402,405,406) and debit > 0").toString().toDouble()
		logi.logInfo( "Tax Maount " +taxamt)
		totalInvoice =  ((exp.toDouble() + taxamt.toDouble()))
		logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		return d.format(totalInvoice.toDouble()).toString()
		logi.logInfo("totalInvoice in md_ExpInvoiceWithoutUsage_Amt method "+totalInvoice)
		return d.format(totalInvoice).toString()

	}

/**
	 * Calculates the detail generated from Get coupon credit details in API
	 * @param coupon details
	 * @return invoice items HashMap
	 */
	 
	 def md_verifyGetCreditTemplateCouponFromAPI(String tcid)
	 {
	 logi.logInfo("inside md_verifyGetCreditTemplateCouponFromAPI")
	 String acct_no=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	 String client_no=getValueFromRequest("create_acct_complete","//client_no[1]")
	 List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap hm_coupons_details_api=new HashMap()
		com.eviware.soapui.support.XmlHolder outputholder
		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains('get_coupon_details') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				def linecount=getNodeCountFromResponse("get_coupon_details","//*[1]/*[local-name()='coupons']/*[local-name()='item']/*[local-name()='credit_templates']").toString()
				logi.logInfo("Line count of coupons :"+linecount)
				
				for(int k = 1 ; k <= linecount.toInteger() ; k++)
				{		List ser_seq_type=[]
				hm_coupons_details_api.put("Flat amount ["+k+"]", outputholder.getNodeValue("//*[1]/*[local-name()='coupons']//*["+k+"]/*[local-name()='credit_templates']/*/*[local-name()='flat_amount']"))
				hm_coupons_details_api.put("Credits required ["+k+"]",outputholder.getNodeValue("//*[1]/*[local-name()='coupons']//*["+k+"]/*[local-name()='credit_templates']/*/*[local-name()='num_credits_required']"))
				hm_coupons_details_api.put("Credit Interval months ["+k+"]",outputholder.getNodeValue("//*[1]/*[local-name()='coupons']//*["+k+"]/*[local-name()='credit_templates']/*/*[local-name()='credit_interval_months']"))
				String typecount=getNodeCountFromResponse("get_coupon_details","//*[1]/*[local-name()='coupons']//*["+k+"]/*[local-name()='credit_templates']/*/*[local-name()='eligible_service_types']/*/*[local-name()='eligible_service_types']").toString()
				logi.logInfo("API Service Types Count "+typecount)
				StringBuilder commaSepValueBuilder = new StringBuilder()
				for(int h = 1 ; h <= typecount.toInteger() ; h++)
				{
					logi.logInfo("Value of h is " +h)
					ser_seq_type.add(outputholder.getNodeValue("//*[1]/*[local-name()='coupons']//*["+k+"]/*[local-name()='credit_templates']/*/*[local-name()='eligible_service_types']/*["+h+"]/*[local-name()='eligible_service_types']"))
				}
				for ( int i = 0; i< ser_seq_type.size(); i++){
					commaSepValueBuilder.append(ser_seq_type.get(i))
					if ( i != ser_seq_type.size()-1){
					commaSepValueBuilder.append(" & ");
					}
					}
				logi.logInfo("And seperator " + commaSepValueBuilder.toString())
				hm_coupons_details_api.put("Eligible service types ["+k+"]",commaSepValueBuilder.toString())
				hm_coupons_details_api.put("Currency code ["+k+"]", outputholder.getNodeValue("//*[1]/*[local-name()='coupons']//*["+k+"]/*[local-name()='credit_templates']/*/*[local-name()='currency_cd']"))
				}
			}
			//hm_coupons_details_api.sort()
		}
		return hm_coupons_details_api.sort()
	 }
	 
	
	/**
	 * Calculates the detail generated from Get coupon credit details in DB
	 * @param coupon details
	 * @return invoice items HashMap
	 */
	 
	 def md_verifyGetCreditTemplateCouponFromDB(String tcid)
	 {
	 logi.logInfo("md_verifyGetCreditTemplateCouponFromDB")
	 String apiname=tcid.split("-")[2]
	 logi.logInfo("API Name:"+apiname)
	 String acct_no=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	 String client_no=getValueFromRequest("create_acct_complete","//client_no[1]")
	 ConnectDB db = new ConnectDB();
	 HashMap hm_coupons_details_db=new HashMap()
	 com.eviware.soapui.support.XmlHolder outputholder
	 
	 def linecount="SELECT count(orig_coupon_cd) FROM ARIACORE.CREDITS WHERE ACCT_NO="+acct_no+" AND REASON_CD=9999 and client_no="+client_no
	 linecount = db.executeQueryP2(linecount)
	 logi.logInfo("Line count :"+linecount)
	 for(int k = 1 ; k <= linecount.toInteger() ; k++)
	 {
		 String coupon="select orig_coupon_cd from (SELECT rownum as t,orig_coupon_cd FROM ARIACORE.CREDITS WHERE ACCT_NO="+acct_no+" AND REASON_CD=9999 and client_no = "+client_no+") where t ="+k
		 coupon = db.executeQueryP2(coupon)
		 logi.logInfo("Coupon code is " +coupon)
			 
	String flat_amt="select FLAT_AMOUNT from  ariacore.RECURRING_CREDIT_TEMPLATES WHERE CLIENT_NO = "+client_no+" and RECURRING_CREDIT_TEMPLATE_NO = (select RECURRING_CREDIT_TEMPLATE_NO from ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP where Coupon_cd = '"+coupon+"' AND CLIENT_NO = "+client_no+")"
	flat_amt = db.executeQueryP2(flat_amt)
	logi.logInfo("Flat amount :"+flat_amt)
	
	String interval_months="select CREDIT_INTERVAL_MONTHS from  ariacore.RECURRING_CREDIT_TEMPLATES WHERE CLIENT_NO = "+client_no+" and RECURRING_CREDIT_TEMPLATE_NO = (select RECURRING_CREDIT_TEMPLATE_NO from ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP where Coupon_cd = '"+coupon+"' AND CLIENT_NO = "+client_no+")"
	interval_months = db.executeQueryP2(interval_months)
	logi.logInfo("Interval Months :"+interval_months)
	
	//service_type = db.executeQueryP2(service_type)
	//logi.logInfo("Service Type :"+service_type)
	String currency_cd = "SELECT currency_cd FROM ARIACORE.CREDITS  WHERE ACCT_NO="+acct_no+" and orig_coupon_cd ='"+coupon+"'"
	currency_cd = db.executeQueryP2(currency_cd)
	logi.logInfo("Currency code  :"+currency_cd)
	String serviceTypescnt = "SELECT count(*) FROM ARIACORE.RECUR_CREDIT_ELGBL_SRVC_TP WHERE CLIENT_NO = "+client_no+" AND  RECURRING_CREDIT_NO=(SELECT ORIG_RECURRING_CREDIT_NO FROM ARIACORE.CREDITS  WHERE ACCT_NO = "+acct_no+" and orig_coupon_cd = '"+coupon+"')"
	serviceTypescnt = db.executeQueryP2(serviceTypescnt)
	logi.logInfo("Service Types Count "+serviceTypescnt)
	String numOfCredits="select num_credits_required from  ariacore.RECURRING_CREDIT_TEMPLATES WHERE CLIENT_NO = "+client_no+" and RECURRING_CREDIT_TEMPLATE_NO = (select RECURRING_CREDIT_TEMPLATE_NO from ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP where Coupon_cd = '"+coupon+"' AND CLIENT_NO = "+client_no+")"
	numOfCredits = db.executeQueryP2(numOfCredits)
	logi.logInfo("Flat amount :"+numOfCredits)
	hm_coupons_details_db.put("Flat amount ["+k+"]",flat_amt)
	hm_coupons_details_db.put("Credits required ["+k+"]",numOfCredits)
	hm_coupons_details_db.put("Credit Interval months ["+k+"]",interval_months)
	List ser_seq_type=[]
	String service_type = "SELECT service_type_cd FROM ARIACORE.RECUR_CREDIT_ELGBL_SRVC_TP WHERE CLIENT_NO="+client_no+" AND  RECURRING_CREDIT_NO in (SELECT ORIG_RECURRING_CREDIT_NO FROM ARIACORE.CREDITS  WHERE CLIENT_NO="+client_no+" and ACCT_NO= "+acct_no+" and orig_coupon_cd = '"+coupon+"')"
	ResultSet rs=db.executePlaQuery(service_type)
	while(rs.next())
	{
		String service_type_values = rs.getString(1)
		ser_seq_type.add(service_type_values)
		logi.logInfo("Value of st is " + ser_seq_type)
	}
	StringBuilder commaSepValueBuilder = new StringBuilder()
	for ( int i = 0; i< ser_seq_type.size(); i++){
		commaSepValueBuilder.append(ser_seq_type.get(i))
		if ( i != ser_seq_type.size()-1){
		commaSepValueBuilder.append(" & ");
		}
		}
	logi.logInfo("And seperator " + commaSepValueBuilder.toString())
	hm_coupons_details_db.put("Eligible service types ["+k+"]",commaSepValueBuilder.toString())
	hm_coupons_details_db.put("Currency code ["+k+"]",currency_cd)
	//hm_coupons_details_db.sort()
	 }
		return hm_coupons_details_db.sort()
	 }
	 
	 public String md_Expinvoice_final_amountCR1B(String testcaseid)
	 {
	 
		 DecimalFormat d = new DecimalFormat("#.##########");
		 def credit_amt
		 def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		 if(acct_no =='NoVal')
		 {
			  acct_no = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		 }
		 String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		 ConnectDB db = new ConnectDB()
		 def total_amount =md_ExpInvoiceWithUsage_Amt_WithServiceCredit(testcaseid).toDouble()
		 logi.logInfo("Total usage invoice Amount" + total_amount )
		 logi.logInfo("Before query execution" )
		 String query2 = "Select currency_cd from ariacore.acct where acct_no = " + acct_no
		 String acct_curcd = db.executeQueryP2(query2)
		 logi.logInfo("acct_curcd" + acct_curcd )
		 String query3 = "select currency_cd from ariacore.credits where acct_no = " + acct_no
		 String credit_curcd = db.executeQueryP2(query3)
		 if (acct_curcd == credit_curcd)
		 {
			 String query = "select NVL(sum(amount),0) from ariacore.credits where reason_cd = 9999 and acct_no = " + acct_no
			 logi.logInfo(query)
			 String credit = db.executeQueryP2(query)
			  credit_amt = credit.toDouble()
			 logi.logInfo("Total Credit amount"+ credit)
		 }
		 else
		 {
			   credit_amt = 0
		 }
		 def total_tax_amount = total_amount
		 logi.logInfo("total_tax_amount" + total_tax_amount )
		 def total_tax = md_total_tax_amount(total_tax_amount)
		 logi.logInfo("Total tax amount"+ total_tax)
		 
		 def invoice_amt = (total_amount + total_tax.toDouble() - credit_amt)
		 logi.logInfo("Total tax amount"+ invoice_amt)
		 return d.format(invoice_amt).toString()
	 }
	 
	
	 
	 public def md_update_master_plan_credit_amt(String testcaseid)
	 {
		 logi.logInfo("calling md_update_master_plan_credit")
		 DecimalFormat d = new DecimalFormat("#.##");
		 String plan_no = getValueFromRequest('create_acct_complete',"//master_plan_no")
		def Credit_amount = md_ExpCredit_Amt1(testcaseid,plan_no)
		 return d.format(Credit_amount).toDouble()
	 }
	 
	 
	 public def md_modify_supp_plan_credit_amt(String testcaseid)
	 {
		 logi.logInfo("calling md_update_master_plan_credit")
		 DecimalFormat d = new DecimalFormat("#.##");
		 String plan_no = getValueFromRequest('create_acct_complete',"//supp_plans")
		 def Credit_amount = md_ExpCredit_Amt1(testcaseid,plan_no)
		 return d.format(Credit_amount).toDouble()
	 }
	 
		 public def md_modify_supp_plan_credit_amt1(String testcaseid)
	 {
		 logi.logInfo("calling md_update_master_plan_credit")
		 DecimalFormat d = new DecimalFormat("#.##");
		 String plan_no = getValueFromRequest('assign_supp_plan',"//supp_plan_no")
		 def Credit_amount = md_ExpCredit_Amt1(testcaseid,plan_no)
		 return d.format(Credit_amount).toDouble()
	 }
	 
	 public def md_replace_supp_plan_credit_amt(String testcaseid)
	 {
		 logi.logInfo("calling md_update_master_plan_credit")
		 DecimalFormat d = new DecimalFormat("#.##");
		 String plan_no = getValueFromRequest('modify_supp_plan',"//supp_plan_no")
		 def Credit_amount = md_ExpCredit_Amt(testcaseid,plan_no)
		 return d.format(Credit_amount).toDouble()
	 }
	 public def md_cancel_supp_plan_credit_amt(String testcaseid)
	 {
		 logi.logInfo("calling md_update_master_plan_credit")
		 DecimalFormat d = new DecimalFormat("#.##");
		 String plan_no = getValueFromRequest('replace_supp_plan',"//new_supp_plan_no")
		 def Credit_amount = md_ExpCredit_Amt(testcaseid,plan_no)
		 return d.format(Credit_amount).toDouble()
	 }
	 
	 public def md_ExpCredit_Amt1(String testcaseid,String plan_no)
	 {
			 logi.logInfo("calling md_ExpCredit_Amt")
			 DecimalFormat d = new DecimalFormat("#.##");
			 String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			 String accountNo = getValueFromResponse('create_acct_complete',"//acct_no")
			 String tax_exmp = getValueFromRequest('create_acct_complete',"//tax_exemption_level")
			 ConnectDB database = new ConnectDB();
			 def totalamt = 0
			 SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
			 String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
			 String currentVirtualTime = database.executeQueryP2(Query);
			 logi.logInfo "Current Virtual Time : "+ currentVirtualTime
			 //int useddays=database.executeQueryP2("SELECT (TRUNC(next_bill_date) -  trunc(plan_date)) AS days FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo).toInteger()
			 int useddays=database.executeQueryP2("SELECT TRUNC(LAST_BILL_THRU_DATE+1) - (select CASE WHEN  (trunc(plan_date) >= trunc(last_recur_bill_date) or last_recur_bill_date is null ) THEN trunc(plan_date) ELSE trunc(last_recur_bill_date)  END 	FROM ariacore.acct WHERE acct_no = "+accountNo+")from ariacore.acct WHERE acct_no = "+accountNo+"  AND client_no ="+clientNo).toInteger()
	 
			 Map<String, HashMap<String,String>> hash = getPlanServiceAndTieredPricingPlan(plan_no)
	 
			 Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
			 resultHash.putAll(hash)
	 
			 logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	 
			 Iterator iterator = resultHash.entrySet().iterator()
			 logi.logInfo "Before Key setting "
	 
			 def invoice = 0
			 double proration_factor
	 
			 while (iterator.hasNext()) {
				 Map.Entry pairs = (Map.Entry) iterator.next()
				 String planNo = pairs.getKey().toString()
				 logi.logInfo "Execution for the plan :  "+ planNo
				 String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
				 String planStatusQuery, planStatusDataQuery
	 
				 int planTypeInd = Integer.parseInt(database.executeQueryP2(planTypeQuery))
	 
				 logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo
	 
	 
				 if(planTypeInd==1) {
					 planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT_SUPP_PLAN_MAP AC WHERE ACCT_NO   ="+accountNo+" AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo
					 planStatusDataQuery = "SELECT status_date from ariacore.ACCT_SUPP_PLAN_MAP where acct_no = "+ accountNo +"AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo
	 
				 }
				 else {
					 planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+" AND CLIENT_NO= " + clientNo
					 planStatusDataQuery = "SELECT status_date FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+ " AND CLIENT_NO= " + clientNo
	 
				 }
	 
				 int planStatus = Integer.parseInt(database.executeQueryP2(planStatusQuery))
				 logi.logInfo "Plan Status : "+planStatus+ " plan : " +planNo
				 logi.logInfo "Plan planStatusDataQuery : "+planStatusDataQuery+ " plan : " +planNo
	 
	 
				 if(planStatus!=0){
					 String statusDateDB = database.executeQueryP2(planStatusDataQuery)
					 logi.logInfo "Status date from DB: "+statusDateDB
					 Date statusDate = simpleFormat.parse(statusDateDB)
	 
					 // Stores the current virtual time in the specified format
					 Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
					 logi.logInfo "planStatus : "+planStatus+ " plan : " +planNo
					 logi.logInfo "statusDate.compareTo(currentVTDate) : "+statusDate.compareTo(currentVTDate)+ " plan : " +planNo
	 
	 
					 //if((planStatus==1 && statusDate.compareTo(currentVTDate)<=0)|| (planTypeInd==0))
					 //{
	 
						 Map<String,String> valueMap = pairs.getValue()
						 Map<String,String> value = new TreeMap<String,String> ()
						 value.putAll(valueMap)
	 
						 int billingInterval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planNo+ "and client_no="+clientNo))
						 Date nextbilldate
						 ResultSet rs=database.executePlaQuery("SELECT TRUNC(LAST_BILL_THRU_DATE+1) FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo)
						 rs.next()
						 nextbilldate=rs.getDate(1)
						 Date virtualstartdate = subractMonthsFromGivenDate(nextbilldate,billingInterval)
	 
						 logi.logInfo "virtualstartdate "+virtualstartdate
						 logi.logInfo "nextbilldate " +nextbilldate
						 int durationOfPlan = getDaysBetween(virtualstartdate,nextbilldate )
						 logi.logInfo "Duration of a plan : "+planNo + " :: "+durationOfPlan
						 logi.logInfo "DaysInvoice "+useddays
						 logi.logInfo "daysInvoice % durationOfPlan Value : "+ (useddays % durationOfPlan )
						 int tmp_days = durationOfPlan-useddays
						 String Proration_qry="SELECT PRORATION_FACTOR FROM GL_DETAIL WHERE SEQ_NUM=2 AND INVOICE_NO=(SELECT MAX(INVOICE_NO) FROM GL WHERE ACCT_NO =" +accountNo+")"
						 String proration=database.executeQueryP2(Proration_qry)
						 proration_factor=proration.toDouble()
						 //d.format(proration_factor).toString()
						 logi.logInfo "proration_factor "+proration_factor
						 
						 Iterator service = value.entrySet().iterator()
						 logi.logInfo "Invoice calculation for a plan : "+planNo
						 String unitsquery="select recurring_factor from ariacore.all_acct_recurring_factors where ACCT_NO ="+accountNo+" and plan_no="+planNo
						 int noofUnits =database.executeQueryP2(planStatusQuery).toInteger()
	 
						 logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo
						 
						 String client_qry = "SELECT METHOD_CD FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO="+ clientNo
						 String Client_cd = database.executeQueryP2(client_qry)
						 logi.logInfo "client_cd " + Client_cd
						 while(service.hasNext()){
							 Map.Entry servicePairs = (Map.Entry) service.next()
							 String serviceNo = servicePairs.getKey().toString()
							 logi.logInfo "Invoice calculation for a service : "+serviceNo
							 int seqNo=1;
							 String tieredPricing = servicePairs.getValue().toString()
							 int tieredPricingInt = Integer.parseInt(tieredPricing)
							 logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
							 String altFee = "NoVal"
							 def serviceInvoice
							 def usedfactor
							 def usedseramt
							 def creditseramt
							 def statetax
							 def federal
						 
							 String query
							 switch(tieredPricingInt){
								 case 1:
									 logi.logInfo "Case 1 "
									 if(!altFee.equals("NoVal"))
									 {
										 query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
									 }
									 else
									 query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
									 serviceInvoice = database.executeQueryP2(query).toDouble()
									 logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
									 usedfactor = 1 - proration_factor
									 logi.logInfo "usedfactor "+usedfactor
									 usedseramt = (serviceInvoice * usedfactor)
									 logi.logInfo "usedseramt "+usedseramt
									 creditseramt = serviceInvoice - usedseramt
									 logi.logInfo "creditseramt "+creditseramt
									 if(Client_cd == '6' || Client_cd == '13')
									 {
										 statetax = creditseramt * 0.06
										 logi.logInfo "statetax "+statetax
										 federal = creditseramt * 0
										 logi.logInfo "federal "+federal
									 }
									 else if(Client_cd == '10')
									 {
										 statetax = creditseramt * 0.11
										 logi.logInfo "statetax "+statetax
										 federal = creditseramt * 0.16047
										 logi.logInfo "federal "+federal
									 }
									 else
									 {
										 statetax = creditseramt * 0.1
										 logi.logInfo "statetax "+statetax
										 federal = creditseramt * 0.2
										 logi.logInfo "federal "+federal
									 }
									 if (tax_exmp == '1')
									 {
										 totalamt = (totalamt + creditseramt + federal).round(2)
									 }
									 else if (tax_exmp == '2')
									 {
										 totalamt = (totalamt + creditseramt + statetax).round(2)
									 }
									 else
									 {
										 totalamt = (totalamt + creditseramt + statetax + federal).round(2)
									 }
									 logi.logInfo "totalamt "+totalamt
									 seqNo++;
									 break;
								 case 2 :
									 logi.logInfo "Case 2"
									 if(!altFee.equals("NoVal"))
									 {
										 query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
									 }
									 else
										 query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
	 
										 serviceInvoice = database.executeQueryP2(query).toDouble()
									 logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
									 usedfactor = 1 - proration_factor
									 logi.logInfo "usedfactor "+usedfactor
									 usedseramt = (serviceInvoice * usedfactor)
									 logi.logInfo "usedseramt "+usedseramt
									 creditseramt = serviceInvoice - usedseramt
									 logi.logInfo "creditseramt "+creditseramt
									 if(Client_cd == '6' ||  Client_cd == '13')
									 {
										 statetax = creditseramt * 0.06
										 logi.logInfo "statetax "+statetax
										 federal = creditseramt * 0
										 logi.logInfo "federal "+federal
									 }
									 else if(Client_cd == '10')
									 {
										 statetax = creditseramt * 0.11
										 logi.logInfo "statetax "+statetax
										 federal = creditseramt * 0.16047
										 logi.logInfo "federal "+federal
									 }
									 else
									 {
										 statetax = creditseramt * 0.1
										 logi.logInfo "statetax "+statetax
										 federal = creditseramt * 0.2
										 logi.logInfo "federal "+federal
									 }
									 if (tax_exmp == '1')
									 {
										 totalamt = (totalamt + creditseramt + federal).round(2)
									 }
									 else if (tax_exmp == '2')
									 {
										 totalamt = (totalamt + creditseramt + statetax).round(2)									}
									 else
									 {
										 totalamt = (totalamt + creditseramt + statetax + federal).round(2)
									 }
									 logi.logInfo "totalamt "+totalamt
								 
									 break;
								 case 3 :
									 logi.logInfo "Case 3"
				 
									 
														 query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
									 serviceInvoice = database.executeQueryP2(query).toDouble()
									 logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
									 usedfactor = 1 - proration_factor
									 logi.logInfo "usedfactor "+usedfactor
									 usedseramt = (serviceInvoice * usedfactor)
									 logi.logInfo "usedseramt "+usedseramt
									 creditseramt = serviceInvoice - usedseramt
									 logi.logInfo "creditseramt "+creditseramt
									 if(Client_cd == '6' || Client_cd == '13')
									 {
										 statetax = creditseramt * 0.06
										 logi.logInfo "statetax "+statetax
										 federal = creditseramt * 0
										 logi.logInfo "federal "+federal
									 }
									 else if(Client_cd == '10')
									 {
										 statetax = creditseramt * 0.11
										 logi.logInfo "statetax "+statetax
										 federal = creditseramt * 0.16047
										 logi.logInfo "federal "+federal
									 }
									 else
									 {
										 statetax = creditseramt * 0.1
										 logi.logInfo "statetax "+statetax
										 federal = creditseramt * 0.2
										 logi.logInfo "federal "+federal
									 }
									 if (tax_exmp == '1')
									 {
										 totalamt = (totalamt + creditseramt + federal)
									 }
									 else if (tax_exmp == '2')
									 {
										 totalamt = (totalamt + creditseramt + statetax)
									 }
									 else
									 {
										 totalamt = (totalamt + creditseramt + statetax + federal).round(2)
									 }
									 logi.logInfo "totalamt "+totalamt
									 break;
							 }
						 }
	 
					 //}
				 }
	 
			 }
	 
			 
	 
			 return totalamt.toDouble()
		 }
	 
	 public def md_ExpCredit_Amt(String testcaseid,String PlanNo)
	 {
			 DecimalFormat d = new DecimalFormat("#.##");
			 String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			 def accountNo = getValueFromResponse('create_acct_complete',"//acct_no")
			 String tax_exmp = getValueFromRequest('create_acct_complete',"//tax_exemption_level")
			 def contractNo=Constant.mycontext.expand('${Properties#ContractNo}')
			 ConnectDB database = new ConnectDB();
			 def usedfactor
			 def usedseramt
			 def creditseramt
			 def statetax
			 def federal
			 def totalamt = 0
			 SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
			 String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
			 String currentVirtualTime = database.executeQueryP2(Query);
			 logi.logInfo "Current Virtual Time : "+ currentVirtualTime
			 //int useddays=database.executeQueryP2("SELECT (TRUNC(next_bill_date) -  trunc(plan_date)) AS days FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo).toInteger()
			 int useddays=database.executeQueryP2("SELECT TRUNC(LAST_BILL_THRU_DATE+1) - (select CASE WHEN  (trunc(plan_date) >= trunc(last_recur_bill_date) or last_recur_bill_date is null ) THEN trunc(plan_date) ELSE trunc(last_recur_bill_date)  END 	FROM ariacore.acct WHERE acct_no = "+accountNo+")from ariacore.acct WHERE acct_no = "+accountNo+"  AND client_no ="+clientNo).toInteger()
	 
			 Map<String, HashMap<String,String>> hash = getPlanServiceAndTieredPricingPlan(PlanNo)
			 Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
			 resultHash.putAll(hash)
			 logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	 
			 Iterator iterator = resultHash.entrySet().iterator()
			 double invoice = 0
			 double proration_factor,usage_proration_unit
	 
			 while (iterator.hasNext()) {
				 Map.Entry pairs = (Map.Entry) iterator.next()
				 String planNo = pairs.getKey().toString()
				 logi.logInfo "Current PlanNo is going to perform:::"+ planNo
				 String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
				 String planStatusQuery, planStatusDataQuery,contractTypeNoQuery,contractStatusQuery,contractStatus,contractTypeNo
								 
				 
				 int planTypeInd = database.executeQueryP2(planTypeQuery).toInteger()
	 
				 logi.logInfo "Plan planTypeInd::: "+planTypeInd+ " plan : " +planNo
	 
				 if(planTypeInd==1) {
					 planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT_SUPP_PLAN_MAP AC WHERE ACCT_NO   ="+accountNo+" AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo
					 planStatusDataQuery = "SELECT status_date from ariacore.ACCT_SUPP_PLAN_MAP where acct_no = "+ accountNo +"AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo
	 
				 }
				 else {
					 planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+" AND CLIENT_NO= " + clientNo
					 planStatusDataQuery = "SELECT status_date FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+ " AND CLIENT_NO= " + clientNo
				}
	 
				 int planStatus = database.executeQueryP2(planStatusQuery).toInteger()
				 logi.logInfo "Plan Status : "+planStatus+ " plan : " +planNo
				 logi.logInfo "Plan planStatusDataQuery : "+planStatusDataQuery+ " plan : " +planNo
	 
	 
				 if(planStatus!=0){
					 String statusDateDB = database.executeQueryP2(planStatusDataQuery)
					 logi.logInfo "Status date from DB: "+statusDateDB
					 Date statusDate = simpleFormat.parse(statusDateDB)
	 
					 // Stores the current virtual time in the specified format
					 Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
					 logi.logInfo "planStatus : "+planStatus+ " plan : " +planNo
					 logi.logInfo "statusDate.compareTo(currentVTDate) : "+statusDate.compareTo(currentVTDate)+ " plan : " +planNo
	 
	 
					 //if((planStatus==1 && statusDate.compareTo(currentVTDate)<=0)|| (planTypeInd==0)){
	 
						 Map<String,String> valueMap = pairs.getValue()
						 Map<String,String> value = new TreeMap<String,String> ()
						 value.putAll(valueMap)
						 
						 //String proration_days = database.executeQueryP2("SELECT  ((Select TRUNC(END_DATE+1) from ARIACORE.ACCT_PLAN_CONTRACTS WHERE CONTRACT_NO ="+ contractNo+" ) - TRUNC(ARIACORE.ARIAVIRTUALTIME("+clientNo+")))  as days from DUAL")
						 
						 //logi.logInfo "proration_days "+proration_days
						 //logi.logInfo "Contract Used Days"+useddays
						 String Proration_qry="SELECT PRORATION_FACTOR FROM GL_DETAIL WHERE SEQ_NUM=2 AND INVOICE_NO=(SELECT MAX(INVOICE_NO) FROM GL WHERE ACCT_NO =" +accountNo+")"
						 String proration=database.executeQueryP2(Proration_qry)
						 proration_factor=proration.toDouble()
						 
						 //proration_factor= (proration_days.toDouble()/useddays.toDouble()).round(10)
						 logi.logInfo "contract proration_factor"+proration_factor
						 Iterator service = value.entrySet().iterator()
						 logi.logInfo "Invoice calculation for a plan : "+planNo
						 String client_qry = "SELECT METHOD_CD FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO="+ clientNo
						 String Client_cd = database.executeQueryP2(client_qry)
						 logi.logInfo "client_cd " + Client_cd
						 
	 
						 while(service.hasNext()){
							 Map.Entry servicePairs = (Map.Entry) service.next()
							 String serviceNo = servicePairs.getKey().toString()
							 logi.logInfo "Invoice calculation for a service : "+serviceNo
							 String unitsquery="select recurring_factor from ariacore.all_acct_recurring_factors where ACCT_NO ="+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no="+planNo
							 logi.logInfo "noofunitsQuery::"+unitsquery
							 int noofUnits =database.executeQueryP2(unitsquery).toInteger()
							 logi.logInfo "no_of_unitsssss: "+noofUnits+ " plan : " +planNo
							 
							 int seqNo=1;
							 String tieredPricing = servicePairs.getValue().toString()
							 int tieredPricingInt =tieredPricing.toInteger()
							 logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
							 double serviceInvoice
							 String query
							 switch(tieredPricingInt){
								 case 1:
									 logi.logInfo "Case 1 "
									 logi.logInfo "Case 1 cs>>"+contractStatus
									 logi.logInfo "Case 1 ct>>"+contractTypeNo
									 usage_proration_unit = (noofUnits * proration_factor)
									 logi.logInfo "usage_proration_unit::"+usage_proration_unit
									 query = "SELECT NRSR.RATE_PER_UNIT * "+noofUnits+" FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
									 serviceInvoice = (database.executeQueryP2(query)).toDouble()
									 usedfactor = 1 - proration_factor
										 logi.logInfo "usedfactor "+usedfactor
										 usedseramt = (serviceInvoice * usedfactor)
										 logi.logInfo "usedseramt "+usedseramt
										 creditseramt = serviceInvoice - usedseramt
										 logi.logInfo "creditseramt "+creditseramt
										 if(Client_cd == '6' || Client_cd == '13')
										 {
											 statetax = creditseramt * 0.06
											 logi.logInfo "statetax "+statetax
											 federal = creditseramt * 0
											 logi.logInfo "federal "+federal
										 }
										 else if(Client_cd == '10')
										 {
											 statetax = creditseramt * 0.11
											 logi.logInfo "statetax "+statetax
											 federal = creditseramt * 0.16047
											 logi.logInfo "federal "+federal
										 }
										 else
										 {
											 statetax = creditseramt * 0.1
											 logi.logInfo "statetax "+statetax
											 federal = creditseramt * 0.2
											 logi.logInfo "federal "+federal
										 }
										 if (tax_exmp == '1')
										 {
											 totalamt = (totalamt + creditseramt + federal).round(2)
										 }
										 else if (tax_exmp == '2')
										 {
											 totalamt = (totalamt + creditseramt + statetax).round(2)
										 }
										 else
										 {
											 totalamt = (totalamt + creditseramt + statetax + federal).round(2)
										 }
										 logi.logInfo "totalamt "+totalamt
									 seqNo++;
									 break;
								 case 2 :
									 logi.logInfo "Case 2"
									 usage_proration_unit = (noofUnits * proration_factor)
									 logi.logInfo "usage_proration_unit::"+usage_proration_unit
									 query ="select "+noofUnits+" * rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
									 serviceInvoice = (database.executeQueryP2(query)).toDouble()
									 usedfactor = 1 - proration_factor
										 logi.logInfo "usedfactor "+usedfactor
										 usedseramt = (serviceInvoice * usedfactor)
										 logi.logInfo "usedseramt "+usedseramt
										 creditseramt = serviceInvoice - usedseramt
										 logi.logInfo "creditseramt "+creditseramt
										 if(Client_cd == '6' || Client_cd == '13')
										 {
											 statetax = creditseramt * 0.06
											 logi.logInfo "statetax "+statetax
											 federal = creditseramt * 0
											 logi.logInfo "federal "+federal
										 }
										 else if(Client_cd == '10')
										 {
											 statetax = creditseramt * 0.11
											 logi.logInfo "statetax "+statetax
											 federal = creditseramt * 0.16047
											 logi.logInfo "federal "+federal
										 }
										 else
										 {
											 statetax = creditseramt * 0.1
											 logi.logInfo "statetax "+statetax
											 federal = creditseramt * 0.2
											 logi.logInfo "federal "+federal
										 }
										 if (tax_exmp == '1')
										 {
											 totalamt = (totalamt + creditseramt + federal).round(2)
										 }
										 else if (tax_exmp == '2')
										 {
											 totalamt = (totalamt + creditseramt + statetax).round(2)
										 }
										 else
										 {
											 totalamt = (totalamt + creditseramt + statetax + federal).round(2)
										 }
										 logi.logInfo "totalamt "+totalamt
									 break;
								 case 3 :
									 logi.logInfo "Case 3"
									 usage_proration_unit = (1*proration_factor)
									 logi.logInfo "usage_proration_unit::"+usage_proration_unit
									 query = "select "+noofUnits+" * rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
									 serviceInvoice = (database.executeQueryP2(query)).toDouble()
									 usedfactor = 1 - proration_factor
										 logi.logInfo "usedfactor "+usedfactor
										 usedseramt = (serviceInvoice * usedfactor)
										 logi.logInfo "usedseramt "+usedseramt
										 creditseramt = serviceInvoice - usedseramt
										 logi.logInfo "creditseramt "+creditseramt
										 if(Client_cd == '6' || Client_cd == '13')
										 {
											 statetax = creditseramt * 0.06
											 logi.logInfo "statetax "+statetax
											 federal = creditseramt * 0
											 logi.logInfo "federal "+federal
										 }
										 else if(Client_cd == '10')
										 {
											 statetax = creditseramt * 0.11
											 logi.logInfo "statetax "+statetax
											 federal = creditseramt * 0.16047
											 logi.logInfo "federal "+federal
										 }
										 else
										 {
											 statetax = creditseramt * 0.1
											 logi.logInfo "statetax "+statetax
											 federal = creditseramt * 0.2
											 logi.logInfo "federal "+federal
										 }
										 if (tax_exmp == '1')
										 {
											 totalamt = (totalamt + creditseramt + federal).round(2)
										 }
										 else if (tax_exmp == '2')
										 {
											 totalamt = (totalamt + creditseramt + statetax).round(2)
										 }
										 else
										 {
											 totalamt = (totalamt + creditseramt + statetax + federal).round(2)
										 }
										 logi.logInfo "totalamt "+totalamt
									 break;
									 
							 }//swith case end;
						 }//service while end;
					 //}//plan status with time compare if end;
				 }//plan status if end;
			 }//plan wise iterator while end;
	 
			 return totalamt.toDouble()
		 }
	 
	 public String md_ExpInvoice_assign_supp_plan_service_credit(String testCaseId)
	 {
 
		 logi.logInfo("calling md_ExpInvoiceWithoutUsage_Amt")
		 String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		 String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		 double usg = 0.0
		 double cal_usg
		 ConnectDB db = new ConnectDB()
		 DecimalFormat d = new DecimalFormat("#.##");
		 String apiname=testCaseId.split("-")[2]
		 double totalInvoice=0.0
		 String invoiceNo = getInvoiceNoVT(clientNo, acct_no)
		 logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		 def exp = md_assign_supp_Proration_amount(testCaseId,acct_no)
		 logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		 def creditamt=db.executeQueryP2("SELECT NVL(SUM(AMOUNT),0) FROM ARIACORE.CREDIT_APPLICATION WHERE INVOICE_NO = (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+ acct_no +")" ).toString().toDouble()
		 logi.logInfo( "Credit amount " +creditamt)
		 def taxamt =db.executeQueryP2("select NVL(sum(debit),0) from gl_detail where invoice_no =(select Max(invoice_no) from gl where acct_no ="+acct_no +") and service_no in(400,401,402,405,406) and debit > 0").toString().toDouble()
		 logi.logInfo( "Tax Maount " +taxamt)
		 totalInvoice =  ((exp.toDouble() + taxamt.toDouble())-(creditamt.toDouble()))
		 logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		 return d.format(totalInvoice.toDouble()).toString()
		 logi.logInfo("totalInvoice in md_ExpInvoiceWithoutUsage_Amt method "+totalInvoice)
		 return d.format(totalInvoice).toString()
 
	 }
	 
	 
	 public String md_ExpInvoice_update_master_plan_inclusive(String testCaseId)
	 {
		 logi.logInfo("calling md_ExpInvoiceWithoutUsage_Amt")
		 String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		 String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		 double usg = 0.0
		 def tax
		 ConnectDB db = new ConnectDB()
		 DecimalFormat d = new DecimalFormat("#.##");
		 String apiname=testCaseId.split("-")[2]
		 def totalInvoice
		 String invoiceNo = getInvoiceNoVT(clientNo, acct_no)
		 logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		 def exp = md_update_master_Proration_amount(testCaseId,acct_no)
		 logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		 def creditamt=db.executeQueryP2("SELECT NVL(SUM(AMOUNT-LEFT_TO_APPLY),0) FROM ARIACORE.CREDITS WHERE ACCT_NO ="+acct_no + " and (alt_service_no_2_apply not in (400,401,402,405,406) or reason_cd =1)").toString().toDouble()
		 logi.logInfo( "Credit amount " +creditamt)
		 def taxamt =db.executeQueryP2("select NVL(sum(debit),0) from gl_detail where invoice_no =(select Max(invoice_no) from gl where acct_no ="+acct_no +") and service_no in(400,401,402,405,406)").toString().toDouble()
		 logi.logInfo( "Tax Maount " +taxamt)
		 totalInvoice =  ((exp.toDouble())-(creditamt.toDouble()))
		 logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		 return d.format(totalInvoice.toDouble()).toString()
	 }
	 
	 public String md_ExpInvoice_update_master_plan_SCredit(String testCaseId)
	 {
		 logi.logInfo("calling md_ExpInvoiceWithoutUsage_Amt")
		 String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		 String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		 def tax
		 ConnectDB db = new ConnectDB()
		 DecimalFormat d = new DecimalFormat("#.##");
		 String apiname=testCaseId.split("-")[2]
		 def totalInvoice
		 String invoiceNo = getInvoiceNoVT(clientNo, acct_no)
		 logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		 def exp = md_update_master_Proration_amount(testCaseId,acct_no)
		 logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		 def creditamt=db.executeQueryP2("SELECT SUM(AMOUNT) FROM ARIACORE.CREDITS WHERE LEFT_TO_APPLY = 0 AND ACCT_NO ="+acct_no ).toString().toDouble()
		 logi.logInfo( "Credit amount " +creditamt)
		 def taxamt = ((exp * 0.2) + (exp * 0.1))
		 logi.logInfo( "Tax Maount " +taxamt)
		 totalInvoice =  ((exp.toDouble() + taxamt.toDouble())-(creditamt.toDouble()))
		 logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		 return d.format(totalInvoice.toDouble()).toString()
	 }
	 
	 /**
	  * Calculates Invoice with Activation CR-1
	  * @param testCaseId
	  * @return Invoice amount
	  */
	 String md_ExpectedInvoiceWithoutActivationAndDiscount_CR1(String testCaseId){
		 logi.logInfo "Inside md_ExpectedInvoiceWithActivationWithoutDiscount_CR1"
		 // Calculating service, federal and state tax amount
		 double serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId).toDouble()
		 //double serviceInvoiceActivationAmount = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
		 double federalTax = md_FEDERAL_TAX_CREDITAMT_BY_ID_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		 double stateTax =  md_STATE_TAX_CREDITAMT_BY_ID_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		 // Calculating deduction amount
		 int creditAmount = Integer.parseInt(md_CREDIT_AMT_BY_CREDITID(testCaseId))
		 // Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
		 logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax + " CreditAmount : "+ creditAmount
		 double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax  - creditAmount.toDouble()
		 logi.logInfo 'Total Invoice : ' + totalInvoice
		 return df.format(totalInvoice).toString();
	 }
	 
	 String md_ExpInvoice_update_master_plan_withoutActivation(String testCaseId) 
	 {
		 String invoice = md_ExpInvoice_update_master_plan(testCaseId)
		 String activation_amt = md_serviceInvoiceActivationAmount(testCaseId)
		 double tot_inv = invoice.toDouble() - activation_amt.toDouble()
		 return (new DecimalFormat("#.##").format(tot_inv)).toString()
	 }
	 
	 
	String md_invoiceCR1_ServiceCredits(String testCaseId)
		{
			String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
			List seqNum=[],service=[],plan=[]
			String credit
			double taxAmount,invoiceAmount,coupon_credit,ans,actualAmount=0
			String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
			ConnectDB db = new ConnectDB()
			String invoice_no=db.executeQueryP2(invoiceQuery)
			String plan_query="SELECT DISTINCT(PLAN_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND PLAN_NO IS  NOT NULL"
			ResultSet rs=db.executePlaQuery(plan_query)
			logi.logInfo "Executed result set...."
			while(rs.next())
			{
				logi.logInfo" Inside result set of plan....."
				String plan_no=rs.getString(1)
				plan.add(plan_no)
			}
			logi.logInfo "Plan list : "+plan.toString()

			for(int i=0;i<plan.size();i++)
			{
				service.clear()
				String service_query="SELECT service_no from ariacore.gl_detail where invoice_no= "+invoice_no+ "and service_no not in (0,400,401,402,403,405) and plan_no="+plan.get(i).toString()+ " AND DEBIT >0"
				rs=db.executePlaQuery(service_query)
				while(rs.next())
				{
					logi.logInfo "Inside result set..."
					String service_no=rs.getString(1)
					service.add(service_no)
				}
				logi.logInfo "Service list : " +service.toString()
				logi.logInfo "Service listi :"+i+"service" +service.toString()

				for(int j=0;j<service.size();j++)
				{
						String act_query="SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+" AND SERVICE_NO= "+service.get(j).toString()+" AND PLAN_NO="+plan.get(i).toString()
						String service_fee=db.executeQueryP2(act_query)
						actualAmount+=service_fee.toDouble()
					
					}
				}
			
			
			logi.logInfo "Invoice_no " +invoice_no
		   
			String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO  IN (400,401) ORDER BY SEQ_NUM DESC"
				 rs=db.executePlaQuery(seq_query)
				while(rs.next())
				{
					logi.logInfo" Inside result set of SEQ_NUM....."
					String seq_no=rs.getString(1)
					seqNum.add(seq_no)
				}
			
			for(int k=0;k<seqNum.size();k++)
			{
				String tax_query="SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND SEQ_NUM= "+seqNum.get(k).toString()
				taxAmount=taxAmount+(db.executeQueryP2(tax_query)).toDouble()
				logi.logInfo "Tax amount :" +taxAmount
			}
			
			String coupon_amt_query="SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO ="+invoice_no+" AND SERVICE_NO=0"
			String coupon_present=db.executeQueryP2(coupon_amt_query)
			if(coupon_present!=null)
			{
				String couponqry = "SELECT COUPON_CD FROM ariacore.ACCT_COUPONS WHERE ACCT_NO="+accountNo
				String coupon_cd=db.executeQueryP2(couponqry)
				logi.logInfo "Coupon : "+ coupon_cd
				String credit_query="SELECT distinct(amount) from ariacore.credits where acct_no="+accountNo+" AND orig_coupon_cd= '"+coupon_cd+"'"
				credit=db.executeQueryP2(credit_query)
			}
			else credit=0
			logi.logInfo "Actual amount "+actualAmount
			logi.logInfo "Credit amount "+credit
			
			invoiceAmount=(actualAmount-credit.toDouble())+taxAmount
			DecimalFormat df = new DecimalFormat("#.##");
			return df.format(invoiceAmount).toString()
		}
		
		String md_invoiceCR0_ServiceCredits(String testCaseId)
		{
			String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
			List seqNum=[],service=[],plan=[]
			String credit
			double taxAmount,invoiceAmount,coupon_credit,ans,actualAmount=0
			String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
			ConnectDB db = new ConnectDB()
			String invoice_no=db.executeQueryP2(invoiceQuery)
			String plan_query="SELECT DISTINCT(PLAN_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND PLAN_NO IS  NOT NULL"
			ResultSet rs=db.executePlaQuery(plan_query)
			logi.logInfo "Executed result set...."
			while(rs.next())
			{
				logi.logInfo" Inside result set of plan....."
				String plan_no=rs.getString(1)
				plan.add(plan_no)
			}
			logi.logInfo "Plan list : "+plan.toString()

			for(int i=0;i<plan.size();i++)
			{
				service.clear()
				String service_query="SELECT service_no from ariacore.gl_detail where invoice_no= "+invoice_no+ "and service_no not in (0,400,401,402,403,405) and plan_no="+plan.get(i).toString()+ " AND DEBIT >0"
				rs=db.executePlaQuery(service_query)
				while(rs.next())
				{
					logi.logInfo "Inside result set..."
					String service_no=rs.getString(1)
					service.add(service_no)
				}
				logi.logInfo "Service list : " +service.toString()
				logi.logInfo "Service listi :"+i+"service" +service.toString()

				for(int j=0;j<service.size();j++)
				{
						String act_query="SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+" AND SERVICE_NO= "+service.get(j).toString()+" AND PLAN_NO= "+plan.get(i).toString()
						String service_fee=db.executeQueryP2(act_query)
						actualAmount+=service_fee.toDouble()
					
					}
				}

			
			logi.logInfo "Invoice_no " +invoice_no
		   
			
			String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO  IN (400,401) ORDER BY SEQ_NUM DESC"
				 rs=db.executePlaQuery(seq_query)
				while(rs.next())
				{
					logi.logInfo" Inside result set of SEQ_NUM....."
					String seq_no=rs.getString(1)
					seqNum.add(seq_no)
				}
			for(int k=0;k<seqNum.size();k++)
			{
				String tax_query="SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND SEQ_NUM= "+seqNum.get(k).toString()
				taxAmount=taxAmount+(db.executeQueryP2(tax_query)).toDouble()
				logi.logInfo "Tax amount :" +taxAmount
			}
			
			String coupon_amt_query="SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO ="+invoice_no+" AND SERVICE_NO=0"
			String coupon_present=db.executeQueryP2(coupon_amt_query)
			logi.logInfo "Coupon_present?? "+coupon_present
			if(coupon_present!=null)
			{
				logi.logInfo "Inside coupon_present iffffffffffff"
				String couponqry = "SELECT COUPON_CD FROM ariacore.ACCT_COUPONS WHERE ACCT_NO="+accountNo
				String coupon_cd=db.executeQueryP2(couponqry)
				logi.logInfo "Coupon : "+ coupon_cd
				//String credit_query="SELECT distinct(amount) from ariacore.credits where acct_no="+accountNo+" AND orig_coupon_cd= '"+coupon_cd+"'"
				String credit_query="SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND SERVICE_NO=0"
				credit=db.executeQueryP2(credit_query)
				if(credit==null)
				{
					credit=coupon_present
				}
			}
			else credit=0
			logi.logInfo "Credit amount"+credit
			
		   logi.logInfo"The coupon credit applied is :"+credit
		   coupon_credit=credit.toDouble()
		   String credit_query="SELECT amount FROM ARIACORE.orders WHERE acct_no= "+accountNo
		   String order_credit=db.executeQueryP2(credit_query)
		   if(order_credit==null)
		   {
			   order_credit=0
			   
		   }
		   logi.logInfo "order_credit :" +order_credit
		   actualAmount+=order_credit.toDouble()
			invoiceAmount=(actualAmount+taxAmount)+coupon_credit
			DecimalFormat df = new DecimalFormat("#.##");
			return df.format(invoiceAmount).toString()
		}
		
		
		public HashMap getPlanServiceAndTieredPricingPlan(String planNo){
			def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String client_no=Constant.mycontext.expand('${Properties#Client_No}')
			ConnectDB database = new ConnectDB();
			String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no = " + planNo + " and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" or custom_to_client_no is null) and recurring = 1) order by plan_no, service_no"
			int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no = " +planNo + " and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" or custom_to_client_no is null) and recurring = 1) order by plan_no, service_no"))
			ResultSet resultSet = database.executeQuery(query)

			HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
			HashMap<String,String> serviceHash

			String[] plan = new String[count]
			String[] service = new String[count]
			String[] tieredPricing = new String[count]

			int i= 0
			while(resultSet.next()){
				   plan[i]=resultSet.getString(1)
				   service[i]=resultSet.getString(2)
				   tieredPricing[i]=resultSet.getString(3)
				   i++
			}
			String plan_no
			int serviceCount=0
			for(int planCount=0;planCount<plan.length;){
				   plan_no = plan[planCount]
				   serviceHash = new HashMap<String,String> ()
				   for(;serviceCount<service.length;serviceCount++){
						 if(plan_no.equalsIgnoreCase(plan[serviceCount])){
								serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
								planCount ++
						 }
						 else
								break
				   }
				   resultHash.put(plan_no, serviceHash)
				   logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
			}
			logi.logInfo "Total Services" + resultHash.toString()
			return resultHash
	 }
		
		
		String md_ExpInvoice_assign_supp_plan_withActivation_serviceCredits(String testCaseId) {
			String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
			ConnectDB db = new ConnectDB()
			String invoice_no=db.executeQueryP2(invoiceQuery)
			String invoice = md_ExpInvoice_assign_supp_plan(testCaseId)
			logi.logInfo "The invoice amount is "+invoice
			String query1="SELECT distinct service_no from ariacore.gl_detail where invoice_no= "+invoice_no+ " and service_no not in (400,401)"
			ResultSet resultSet=db.executePlaQuery(query1)
			List ser_nos=[]
			double activation_amt=0.0
			logi.logInfo "Executed service_no query..."
			while(resultSet.next())
			{
				logi.logInfo "Inside resultSet"
				String ser_no= resultSet.getString(1)
				ser_nos.add(ser_no)
			}
			logi.logInfo("ser_nos list is:"+ser_nos.toString())
			int length=ser_nos.size()
			logi.logInfo "Length of ser_nos : " +length
			for(int i=0;i<ser_nos.size();i++)
			{
				String service_query="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO="+ser_nos.get(i).toString()
				String service_type=db.executeQueryP2(service_query)
				if(service_type=='MN'){
					logi.logInfo "The service with type MN is "+ser_nos.get(i).toString()
					String debit_query="SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+" AND SERVICE_NO="+ser_nos.get(i).toString()
					String debit=db.executeQueryP2(debit_query)
					activation_amt=debit.toDouble()
					
				}
			}
			logi.logInfo "The MINIMUM FEE is "+activation_amt.toString()
			double tot_inv = invoice.toDouble() + activation_amt
			return (new DecimalFormat("#.##").format(tot_inv)).toString()
		}
		
		def md_PlanDetails_DB(String testCaseId)
		{
			logi.logInfo "Inside Plan Details...."
			String client_no = Constant.mycontext.expand('${Properties#Client_No}')
			def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			if(accountNo =='NoVal')
			{
				   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
			}
			logi.logInfo "Got the account no "+accountNo
			List plan=[]
			int i,j,plancount
			ConnectDB db = new ConnectDB()
			String supp_plans="SELECT SUPP_PLAN_NO FROM ariacore.acct_supp_plan_map WHERE ACCT_NO="+accountNo+ " ORDER BY SUPP_PLAN_NO ASC"
			ResultSet rs=db.executePlaQuery(supp_plans)
			logi.logInfo "Executed result set...."
			while(rs.next())
			{
				logi.logInfo" Inside result set of plan....."
				String plan_no=rs.getString(1)
				plan.add(plan_no)
			}
			logi.logInfo "The Plan list is"+plan.toString()
			for (i=0;i<plan.size();i++)
			{
				plancount=i+1
				md_create_plan_details_from_DB_ServiceCredits(plan.get(i).toString(),plancount)
			}
			
			
		}
		
		def md_create_plan_details_from_DB_ServiceCredits(String testCaseId) {
			logi.logInfo "Inside Plan Details method........."
			LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
			String client_no = Constant.mycontext.expand('${Properties#Client_No}')
			def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			if(accountNo =='NoVal')
			{
				   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
			}
			
			logi.logInfo "Got the client no"+client_no+" And account No "+accountNo
			ConnectDB db = new ConnectDB()
			List plan=[]
			int i,j
			String supp_plans="SELECT SUPP_PLAN_NO FROM ariacore.acct_supp_plan_map WHERE ACCT_NO="+accountNo+ " ORDER BY SUPP_PLAN_NO DESC"
			ResultSet rs=db.executePlaQuery(supp_plans)
			logi.logInfo "Executed result set...."
			while(rs.next())
			{
				logi.logInfo" Inside result set of plan....."
				String plan_no=rs.getString(1)
				plan.add(plan_no)
			}		
			logi.logInfo "Plan list : "+plan.toString()
			for (i=0;i<plan.size();i++)
			{
			String plan_name = db.executeQueryP2("SELECT plan_name FROM ARIACORE.CLIENT_PLAN where plan_no="+plan.get(i).toString()+" and client_no="+client_no)
			String planDetailsQuery = "SELECT * FROM ARIACORE.CLIENT_PLAN where PLAN_NO= " + plan.get(i).toString()+ "and client_no="+client_no
			//String dunningPlanQuery = "SELECT DUNNING_PLAN_NO from dunning_plan_assignment where PLAN_NO=" + plan.get(i).toString() + " AND CLIENT_NO=" + client_no
			//hashResult.put("Dunning Plan No", db.executeQueryP2(dunningPlanQuery))
			rs = db.executePlaQuery(planDetailsQuery)
			while(rs.next()) {
				logi.logInfo "Printing deatils of plan_no "+plan.get(i).toString()
				hashResult.put("Plan No "  ,plan.get(i).toString())
				hashResult.put("Plan Name " ,plan_name)
				hashResult.put("Desc ", rs.getString("eu_html_long"))
				hashResult.put("Billing Interval ", rs.getString("billing_interval"))
				hashResult.put("Billing indicator ", rs.getString("billing_ind"))
				hashResult.put("Currency Cd ", rs.getString("currency_cd"))
				hashResult.put("Supp Plan Ind ", rs.getString("supp_plan_ind"))
				hashResult.put("Rollover Acct Status Cd ", rs.getString("rollover_acct_status_cd"))
				hashResult.put("Rollover Acct Status Days ", rs.getString("rollover_acct_status_days"))
			}
			logi.logInfo("Hashmap is for the plan "+plan.get(i).toString()+" is " + hashResult)
			return hashResult.sort()
		}
			}
		
		def md_PlanDetails_API(String testCaseId){
			logi.logInfo "Inside Plan Details APIIIIIIIIII"
			ConnectDB db = new ConnectDB()
			def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			if(accountNo =='NoVal')
			{
				   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
			}
			String supp_plan="SELECT COUNT(DISTINCT(SUPP_PLAN_NO)) FROM ariacore.acct_supp_plan_map WHERE ACCT_NO= "+accountNo
			String count_supp=(db.executeQueryP2(supp_plan))
			logi.logInfo "No. of Supp_plan: "+count_supp
			for(int i=1;i<=count_supp.toInteger();i++)
			{
				md_create_plan_details_from_API_ServiceCredits(i)
			}
		}
			
		
		def md_create_plan_details_from_API_ServiceCredits(String testCaseId) {
			logi.logInfo "Inside Plan details API check...."
			List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
			List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
			HashMap hm_invoice=new HashMap()
			com.eviware.soapui.support.XmlHolder outputholder
			def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			if(accountNo =='NoVal')
			{
				   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
			}
			logi.logInfo "Got the account No"+accountNo
			ConnectDB db = new ConnectDB()
			String supp_plan="SELECT COUNT(DISTINCT(SUPP_PLAN_NO)) FROM ariacore.acct_supp_plan_map WHERE ACCT_NO= "+accountNo
			String count_supp=(db.executeQueryP2(supp_plan))
			logi.logInfo "No. of Supp_plan: "+count_supp
			for(int j=0; j<testCaseCmb.size(); j++) {
				if(testCaseCmb[j].toString().contains('get_client_plans_all') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
					outputholder = xmlValues[j]
					for(int i=1;i<=count_supp.toInteger();i++)
					{
					logi.logInfo "Xpaths fetched for Plan "+i
					hm_invoice.put("Plan No ", outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_no']"))
					String plan_no=outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_no']")
					logi.logInfo "The Plan no got form Xpath is "+plan_no
					hm_invoice.put("Plan Name ",outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_name']"))
					hm_invoice.put("Desc ",outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_desc']"))
					hm_invoice.put("Billing Interval ",outputholder.getNodeValue("//*["+i+"]/*[local-name()='billing_interval']"))
					hm_invoice.put("Billing indicator ",outputholder.getNodeValue("//*["+i+"]/*[local-name()='billing_ind']"))
					hm_invoice.put("Currency Cd ",outputholder.getNodeValue("//*["+i+"]/*[local-name()='currency_cd']"))
					hm_invoice.put("Supp Plan Ind ",outputholder.getNodeValue("//*["+i+"]/*[local-name()='supp_plan_ind']"))
					hm_invoice.put("Rollover Acct Status Cd ",outputholder.getNodeValue("//*["+i+"]/*[local-name()='rollover_acct_status']"))
					hm_invoice.put("Rollover Acct Status Days ",outputholder.getNodeValue("//*["+i+"]/*[local-name()='rollover_acct_status_days']"))
					}
				
		

			return hm_invoice.sort()
				}
	}
}
		
		
		def md_VerifyServiceDetails_DB_ServiceCredits(String testcaseid) {
			logi.logInfo "Service Detailsssssssssssssssssssssssssss"
			LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
			String client_no = Constant.mycontext.expand('${Properties#Client_No}')
			def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			ConnectDB db = new ConnectDB()
			logi.logInfo "Got the account No" +accountNo
			List plan=[],service=[]
			int i,j
			def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String supp_plans="SELECT SUPP_PLAN_NO FROM ariacore.acct_supp_plan_map WHERE ACCT_NO="+accountNo
			ResultSet rs=db.executePlaQuery(supp_plans)
			logi.logInfo "Executed result set...."
			while(rs.next())
			{
				logi.logInfo" Inside result set of plan....."
				String plan_no=rs.getString(1)
				plan.add(plan_no)
			}		
			logi.logInfo "Plan list : "+plan.toString()
			for (i=0;i<plan.size();i++)
			{
				
			String plan_services="SELECT SERVICE_NO FROM ARIACORE.PLAN_SERVICES WHERE PLAN_NO ="+plan.get(i).toString()+ "AND CLIENT_NO="+client_no+ " AND SERVICE_NO NOT IN 108"
			rs=db.executePlaQuery(plan_services)
			while(rs.next())
			{
				logi.logInfo" Inside result set of service....."
				String service_no=rs.getString(1)
				service.add(service_no)
			}
			for(j=0;j<service.size();j++)
			{
			String service_query="SELECT * FROM ARIACORE.PLAN_SERVICES JOIN ARIACORE.SERVICES ON ARIACORE.PLAN_SERVICES.SERVICE_NO = ARIACORE.SERVICES.SERVICE_NO AND ARIACORE.PLAN_SERVICES.client_no = ARIACORE.SERVICES.custom_to_client_no JOIN ARIACORE.CHART_OF_ACCTS ON ARIACORE.SERVICES.COA_ID = ARIACORE.CHART_OF_ACCTS.COA_ID AND ARIACORE.SERVICES.custom_to_client_no = ARIACORE.CHART_OF_ACCTS.custom_to_client_no WHERE ARIACORE.PLAN_SERVICES.PLAN_NO = "+plan.get(i).toString()+" AND ARIACORE.PLAN_SERVICES.CLIENT_NO = "+client_no+" AND ARIACORE.SERVICES.SERVICE_NO="+service.get(j).toString()+ " AND ARIACORE.CHART_OF_ACCTS.COA_ID IN (SELECT COA_ID FROM ARIACORE.SERVICES WHERE SERVICE_NO IN "+service.get(j).toString()+")"
			//hashResult.put("Dunning Plan No", db.executeQueryP2(dunningPlanQuery))
			rs = db.executePlaQuery(service_query)
			while(rs.next()) {
				hashResult.put("Service No", service.get(j).toString())
				hashResult.put("Is Recurring Indicator", rs.getString("recurring"))
				hashResult.put("Is Usage Based Indicator", rs.getString("usage_based"))
				hashResult.put("Taxable Indicator", rs.getString("is_tax"))
				hashResult.put("Tiered Pricing Rule", rs.getString("tiered_pricing_rule_no"))
				hashResult.put("Is Min Fee Indicator", rs.getString("is_min_fee"))
			}
			
			logi.logInfo("Hashmap is : " + hashResult)
			return hashResult.sort()
			}
		}
		}
		
		def md_VerifyServiceDetails_API_ServiceCredits(String testcaseID) {
			logi.logInfo "Serviceeeeeeeeeeeeeeeeeeeeeeeee API"
			List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
			List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
			HashMap hm_invoice=new HashMap()
			com.eviware.soapui.support.XmlHolder outputholder
			def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			logi.logInfo "Got the account "+accountNo
			List plan=[],service=[]
			ConnectDB db = new ConnectDB()
			String supp_plan="SELECT DISTINCT(SUPP_PLAN_NO) FROM ariacore.acct_supp_plan_map WHERE ACCT_NO= "+accountNo+"order by supp_plan_no asc"
			ResultSet rs=db.executePlaQuery(supp_plan)
			logi.logInfo "Executed supp plan query...."
			while(rs.next())
			{
				logi.logInfo" Inside result set of plan....."
				String plan_no=rs.getString(1)
				plan.add(plan_no)
			}	
			for(int j=0; j<testCaseCmb.size(); j++) {
				if(testCaseCmb[j].toString().contains('get_client_plans_all') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
					outputholder = xmlValues[j]
					for(int i=1;i<=plan.size();i++)
					{
						int service_count=(db.executeQueryP2("SELECT count(distinct(service_no))-2 FROM ARIACORE.PLAN_SERVICES where plan_no="+plan.get(i).toString())).toInteger()
					for(int k=2;k<=service_count;k++)
					{
					hm_invoice.put("Service No", outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_services']//*["+k+"]/*[local-name()='service_no']"))
					hm_invoice.put("Is Recurring Indicator",outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_services']//*["+k+"]/*[local-name()='is_recurring_ind']"))
					hm_invoice.put("Is Usage Based Indicator",outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_services']//*["+k+"]/*[local-name()='is_usage_based_ind']"))
					hm_invoice.put("Taxable Indicator",outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_services']//*["+k+"]/*[local-name()='taxable_ind']"))
					hm_invoice.put("Tiered Pricing Rule",outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_services']//*["+k+"]/*[local-name()='tiered_pricing_rule']"))
					hm_invoice.put("Is Min Fee Indicator",outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_services']//*["+k+"]/*[local-name()='is_min_fee_ind']"))
					
			return hm_invoice.sort()
					}
		}
		}
		}
		}
		
		
		def md_VerifyServiceRates_DB_ServiceCredits(String testcaseid) {
			logi.logInfo "DBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB Rate details"
			LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
			String client_no = Constant.mycontext.expand('${Properties#Client_No}')
			List plan=[],service=[],rateSeq=[]
			int i,j
			def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			logi.logInfo "Got account No " +accountNo
			ConnectDB db = new ConnectDB()
			String supp_plans="SELECT SUPP_PLAN_NO FROM ariacore.acct_supp_plan_map WHERE ACCT_NO="+accountNo
			ResultSet rs=db.executePlaQuery(supp_plans)
			logi.logInfo "Executed result set...."
			while(rs.next())
			{
				logi.logInfo" Inside result set of plan....."
				String plan_no=rs.getString(1)
				plan.add(plan_no)
			}
			logi.logInfo "Plan list : "+plan.toString()
			for (i=0;i<plan.size();i++)
			{
			service.clear()	
			String plan_services="SELECT SERVICE_NO FROM ARIACORE.PLAN_SERVICES WHERE PLAN_NO ="+plan.get(i).toString()+ "AND CLIENT_NO="+client_no+ "AND SERVICE_NO NOT IN 108 ORDER BY SERVICE_NO ASC"
			rs=db.executePlaQuery(plan_services)
			while(rs.next())
			{
				logi.logInfo" Inside result set of service....."
				String service_no=rs.getString(1)
				service.add(service_no)
			}
			for(j=0;j<service.size();j++)
			{
			rateSeq.clear()
			String rateSeqQuery="SELECT distinct(rate_seq_no) FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE service_no="+service.get(i).toString()+" and client_no="+client_no+" ORDER BY rate_seq_no ASC"
			rs=db.executePlaQuery(rateSeqQuery)
			while(rs.next())
			{
				logi.logInfo" Inside result set of Rate seq....."
				String rate_seq=rs.getString(1)
				rateSeq.add(rate_seq)
			}
			for(int k=0;k<rateSeq.size();k++)
			{
			String rate_unit="SELECT * FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SERVICE_NO="+service.get(i).toString()+" AND RATE_SEQ_NO= "+rateSeq.get(i).toString()+ " AND CLIENT_NO= "+client_no+"AND SCHEDULE_NO=(SELECT DISTINCT(RATE_SCHEDULE_NO) FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO  ="+accountNo+ " AND PLAN_NO="+plan.get(i).toString()+ " AND SERVICE_NO ="+service.get(j).toString()+" AND RATE_SEQ_NO="+rateSeq.get(i).toString()+" AND CLIENT_NO  ="+client_no+")"
			rs = db.executePlaQuery(rate_unit)
			while(rs.next()) {
				hashResult.put("Service No", service.get(j).toString())
				hashResult.put("Rate_Seq_No", rateSeq.get(k).toString())
				if(service.get(j).toString()==108)
				{
					hashResult.put("From Unit", "null")
					hashResult.put("To Unit", "null")
					hashResult.put("Rate Per Unit", "null")
				}
				else
				{
				hashResult.put("From Unit", rs.getString("from_unit"))
				if(j==2)
				{
					hashResult.put("To Unit", rs.getString("null"))
				}
				else
				{
				hashResult.put("To Unit", rs.getString("to_unit"))
				}
				hashResult.put("Rate Per Unit", rs.getString("rate_per_unit"))
				}
			
			logi.logInfo("Hashmap is : " + hashResult)
			return hashResult.sort()
			}
		}
		}
		}
	}
			

		
		def md_ServiceRates_API_ServiceCredits(String testcaseID) {
			logi.logInfo("Ratessssssssssssss API")
			String client_no = Constant.mycontext.expand('${Properties#Client_No}')
			List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
			List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
			HashMap hm_invoice=new HashMap()
			com.eviware.soapui.support.XmlHolder outputholder
			def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			List plan=[],service=[]
			ConnectDB db = new ConnectDB()
			String supp_plan="SELECT DISTINCT(SUPP_PLAN_NO) FROM ariacore.acct_supp_plan_map WHERE ACCT_NO= "+accountNo
			ResultSet rs=db.executePlaQuery(supp_plan)
			logi.logInfo "Executed result set...."
			while(rs.next())
			{
				logi.logInfo" Inside result set of plan....."
				String plan_no=rs.getString(1)
				plan.add(plan_no)
			}
			logi.logInfo "Plan list : "+plan.toString()
			for(int j=0; j<testCaseCmb.size(); j++) {
				if(testCaseCmb[j].toString().contains('get_client_plans_all') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
					outputholder = xmlValues[j]
					for(int i=1;i<=plan.size();i++)
					{
					int service_count=(db.executeQueryP2("SELECT count(distinct(service_no))-2 FROM ARIACORE.PLAN_SERVICES where plan_no="+plan.get(i).toString()))
					
					service.clear()
					String plan_services="SELECT distinct(SERVICE_NO) FROM ARIACORE.PLAN_SERVICES WHERE PLAN_NO ="+plan.get(i).toString()+ "AND CLIENT_NO="+client_no+ " AND SERVICE_NO NOT IN 108"
					rs=db.executePlaQuery(plan_services)
					while(rs.next())
					{
						
						logi.logInfo" Inside result set of service....."
						String service_no=rs.getString(1)
						service.add(service_no)
					}
					logi.logInfo "Service list : "+service.toString()
					for(int k=2;k<=service_count;k++)
					{
						String rateSeq_query="SELECT COUNT(DISTINCT(RATE_SEQ_NO)) FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SERVICE_NO="+service.get(k-1).toString()+" AND CLIENT_NO="+client_no
						String rateSeq_count=db.executeQueryP2(rateSeq_query)
						logi.logInfo "rateSeq_count" +rateSeq_count
						for(int m=1;m<=rateSeq_count;m++)
						{
							
							
								hm_invoice.put("Service No", outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_services']//*["+k+"]/*[local-name()='service_no']"))
								 hm_invoice.put("Rate_Seq_No",outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_services']//*["+k+"]/*[local-name()='plan_service_rates']/*["+m+"]/*[local-name()='rate_seq_no']"))
								 hm_invoice.put("From Unit",outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_services']//*["+k+"]/*[local-name()='plan_service_rates']/*["+m+"]/*[local-name()='from_unit']"))
								 hm_invoice.put("To Unit",outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_services']//*["+k+"]/*[local-name()='plan_service_rates']/*["+m+"]/*[local-name()='to_unit']"))
								 hm_invoice.put("Rate Per Unit",outputholder.getNodeValue("//*["+i+"]/*[local-name()='plan_services']//*["+k+"]/*[local-name()='plan_service_rates']/*["+m+"]/*[local-name()='rate_per_unit']"))
						
							
					if(hm_invoice.get("From Unit").toString()=="null")
					hm_invoice.put("From Unit","null")
					if(hm_invoice.get("To Unit").toString()=="null")
					hm_invoice.put("To Unit","null")
					if(hm_invoice.get("Rate Per Unit").toString()=="null")
					hm_invoice.put("From Unit","null")
					
					
					
			return hm_invoice.sort()
					}
		}
		}
		}
		}
		}
		
		
		String md_stateTax_ServiceCredits(String testCaseId)
		{
			logi.logInfo "Inside md_stateTax_exclusive"
			def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
			ConnectDB db = new ConnectDB()
			String invoice_no=db.executeQueryP2(invoiceQuery)
			String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO = 401 "
			String seq_num=db.executeQueryP2(seq_query)
			String debit_query="SELECT SUM(DEBIT) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SEQ_NUM= " +seq_num
			String debit = db.executeQueryP2(debit_query)
			debit== null ? "No Data":debit
		}

		/**
		 * Calculates the Federal tax for the invoice amount
		 * @param testCaseId
		 * @return Federal tax
		 */
		String md_federalTax_ServiceCredits(String testCaseId)
		{
			logi.logInfo "Inside md_stateTax_exclusive"
			def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
			ConnectDB db = new ConnectDB()
			String invoice_no=db.executeQueryP2(invoiceQuery)
			String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO = 400"
			String seq_num=db.executeQueryP2(seq_query)
			String debit_query="SELECT SUM(DEBIT) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SEQ_NUM= " +seq_num
			String debit = db.executeQueryP2(debit_query)
			debit== null ? "No Data":debit
		}
		
		public String md_Expinvoice_final_amountCR1A(String testcaseid)
		{
		
			DecimalFormat d = new DecimalFormat("#.##########");
			def credit_amt
			def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			if(acct_no == 'NoVal')
			{
				 acct_no = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
			}
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			ConnectDB db = new ConnectDB()
			def total_amount =md_ExpInvoiceWithUsage_Amt_WithServiceCredit(testcaseid).toDouble()
			logi.logInfo("Total usage invoice Amount" + total_amount )
			logi.logInfo("Before query execution" )
			String query2 = "Select currency_cd from ariacore.acct where acct_no = " + acct_no
			String acct_curcd = db.executeQueryP2(query2)
			logi.logInfo("acct_curcd" + acct_curcd )
			String query3 = "select currency_cd from ariacore.credits where acct_no = " + acct_no
			String credit_curcd = db.executeQueryP2(query3)
			if (acct_curcd == credit_curcd)
			{
				String query = "select NVL(sum(amount),0) from ariacore.credits where reason_cd = 9999 and acct_no = " + acct_no
				logi.logInfo(query)
				String credit = db.executeQueryP2(query)
				 credit_amt = credit.toDouble()
				logi.logInfo("Total Credit amount"+ credit)
			}
			else
			{
				  credit_amt = 0
			}
			def total_tax_amount = total_amount-credit_amt
			logi.logInfo("total_tax_amount" + total_tax_amount )
			def total_tax = md_total_tax_amount(total_tax_amount)
			logi.logInfo("Total tax amount"+ total_tax)
			
			def invoice_amt = (total_amount + total_tax.toDouble() - credit_amt)
			logi.logInfo("Total tax amount"+ invoice_amt)
			return d.format(invoice_amt).toString()
		}
		
		
			 public def md_replace_supp_plan_credit_amt1(String testcaseid)
		{
			logi.logInfo("calling md_update_master_plan_credit")
			DecimalFormat d = new DecimalFormat("#.##");
			String plan_no = getValueFromRequest('assign_supp_plan',"//supp_plan_no")
			def Credit_amount = md_ExpCredit_Amt(testcaseid,plan_no)
			return d.format(Credit_amount).toDouble()
		}
		
		public String md_ActualUsage_Amt_CAH(String testid) {
			
					String accountNo=getValueFromResponse('create_acct_hierarchy',"//a1_acct_no")
					
					return md_ActualUsage_Amt(accountNo,testid);
				}
		public String md_ActualInvoice_Amt_CAH(String testid) {
			
					String accountNo=getValueFromResponse('create_acct_hierarchy',"//a1_acct_no")
					
					return md_ActualInvoice_Amt(accountNo,testid);
				}
		public String md_Expinvoice_final_amount_CAH(String testcaseid)
		{
		
			DecimalFormat d = new DecimalFormat("#.##########");
			def credit_amt
			def acct_no = getValueFromResponse('create_acct_hierarchy',"//a1_acct_no")
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			ConnectDB db = new ConnectDB()
			def total_amount =md_ExpInvoiceWithUsage_Amt_WithServiceCredit_CAH(testcaseid).toDouble()
			logi.logInfo("Total usage invoice Amount" + total_amount )
			logi.logInfo("Before query execution" )
			String query2 = "Select currency_cd from ariacore.acct where acct_no = " + acct_no
			String acct_curcd = db.executeQueryP2(query2)
			logi.logInfo("acct_curcd" + acct_curcd )
			String query3 = "select currency_cd from ariacore.credits where acct_no = " + acct_no
			String credit_curcd = db.executeQueryP2(query3)
			if (acct_curcd == credit_curcd)
			{
				String query = "select NVL((amount),0) from ariacore.credits where credit_id = (select max(credit_id) from ariacore.credits where acct_no = " + acct_no + ")"
				logi.logInfo(query)
				String credit = db.executeQueryP2(query)
				 credit_amt = credit.toDouble()
				logi.logInfo("Total Credit amount"+ credit)
			}
			else
			{
				  credit_amt = 0
			}
			def total_tax = md_total_tax_amount(total_amount)
			logi.logInfo("Total tax amount"+ total_tax)
			
			def invoice_amt = (total_amount + total_tax.toDouble() - credit_amt)
			logi.logInfo("Total tax amount"+ invoice_amt)
			return d.format(invoice_amt).toString()
		}
		String md_ExpInvoiceWithUsage_Amt_WithServiceCredit_CAH(String testcaseid)
		{
			def acct_no = getValueFromResponse('create_acct_hierarchy',"//a1_acct_no")
			return md_ExpInvoiceWithUsage_Amt_WithServiceCredit(testcaseid,acct_no)
		}
		public String md_ExpInvoice_Usage_amt_CAH(String testcaseid)
		{
			ConnectDB db = new ConnectDB()
			def acct_no = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
			String query = "Select sum(amt) from ariacore.usage where invoice_no = (select max(invoice_no) from ariacore.gl where acct_no=" + acct_no + " )"
			String Exp_usage = db.executeQueryP2(query)
			logi.logInfo("Exp_usage" + Exp_usage)
			return (Exp_usage)
		}
		
		public String md_ActualInvoice_Amt_CAH(String testCaseId,String lineItem)
		{
			logi.logInfo("md_ActualInvoice_Amt_CAH")
			String acct_no
			switch(lineItem)
			{
				case "1":
				acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO1)
				logi.logInfo("Account Number" + acct_no)
				break;
				case "2":
				acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO2)
				break;
				case "3":
				acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO3)
				break;
				case "4":
				acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO4)
				break;
				case "5":
				acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO5)
				break;
			}
					return md_ActualInvoice_Amt(acct_no,testCaseId);
		}
		
		public String md_actual_invoice1(String testCaseId)
		{
			return md_ActualInvoice_Amt_CAH(testCaseId,"1");
		}
		public String md_actual_invoice2(String testCaseId)
		{
			return md_ActualInvoice_Amt_CAH(testCaseId,"2");
		}
		public String md_actual_invoice3(String testCaseId)
		{
			return md_ActualInvoice_Amt_CAH(testCaseId,"3");
		}
		public String md_actual_invoice4(String testCaseId)
		{
			return md_ActualInvoice_Amt_CAH(testCaseId,"4");
		}
		public String md_actual_invoice5(String testCaseId)
		{
			return md_ActualInvoice_Amt_CAH(testCaseId,"5");
		}
		
		
		public String md_Expinvoice_amt1(String testCAseId)
		{
			DecimalFormat d = new DecimalFormat("#.##");
			String Invoice_amt = md_checkSecondInvoiceAmount_CAH_1(testCAseId)
			logi.logInfo("EXP_Invoice" + Invoice_amt)
			String Usage_amt = md_ExpInvoice_Usage_amt(testCAseId)
			double total_amt = ((Invoice_amt.toDouble()) + (Usage_amt.toDouble()))
			return d.format(total_amt).toString()
			
		}
		public String md_Expinvoice_amt2(String testCAseId)
		{
			DecimalFormat d = new DecimalFormat("#.##");
			String Invoice_amt = md_checkSecondInvoiceAmount_CAH_2(testCAseId)
			logi.logInfo("EXP_Invoice" + Invoice_amt)
			String Usage_amt = md_ExpInvoice_Usage_amt(testCAseId)
			double total_amt = ((Invoice_amt.toDouble()) + (Usage_amt.toDouble()))
			return d.format(total_amt).toString()
			
		}
		public String md_Expinvoice_amt3(String testCAseId)
		{
			DecimalFormat d = new DecimalFormat("#.##");
			String Invoice_amt = md_checkSecondInvoiceAmount_CAH_3(testCAseId)
			logi.logInfo("EXP_Invoice" + Invoice_amt)
			String Usage_amt = md_ExpInvoice_Usage_amt(testCAseId)
			double total_amt = ((Invoice_amt.toDouble()) + (Usage_amt.toDouble()))
			return d.format(total_amt).toString()
			
		}
		public String md_Expinvoice_amt_with_Usage(String testCAseId)
		{
			DecimalFormat d = new DecimalFormat("#.##");
			String Invoice_amt = md_ExpInvoice_Amount(testCAseId)
			logi.logInfo("EXP_Invoice" + Invoice_amt)
			String Usage_amt = md_ExpInvoice_Usage_amt(testCAseId)
			double total_amt = ((Invoice_amt.toDouble()) + (Usage_amt.toDouble()))
			return d.format(total_amt).toString()
			
		}
		int md_serviceInvoiceActivationAmount1(String testCaseId) {
			logi.logInfo "Inside md_serviceInvoiceActivationAmount1"
			//String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
			logi.logInfo " TestId : "+ testId
			int invoice = 0
			ConnectDB db = new ConnectDB()
	
			String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
			logi.logInfo " Query : "+ Query
			String currentVirtualTime = db.executeQueryP2(Query);
			logi.logInfo "Current Virtual Time before calculating State Taxes: "+ currentVirtualTime
	
			// Getting maximum invoice number from db
			invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
			logi.logInfo "Invoice No : "+invoiceNo
	
			// Getting plans involved in invoice
			HashMap<String, HashMap<String, String>> plans = getPlanServiceAndTieredPricingForActivationByInvoice(invoiceNo)
			Map<String, Map<String,Integer>> resultHash = new TreeMap<String, TreeMap<String,Integer>> ()
			resultHash.putAll(plans)
			logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	
			// Getting Multiply Activation Fee Flag from db for a cleint
			String multiplyActivationFeeFlag =db.executeQueryP2("SELECT param_val FROM ariacore.ARIA_CLIENT_PARAMS WHERE client_no ="+ clientNo+" AND param_name  = 'MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT'")
			logi.logInfo "Multiply Activation Fee Flag : "+multiplyActivationFeeFlag +" for a Client : " +clientNo
			
			Iterator iterator = resultHash.entrySet().iterator()
			logi.logInfo "Before Key setting "
			// Iterating through Plans
			while (iterator.hasNext()) {
				Map.Entry pairs = (Map.Entry) iterator.next()
				String planNo = pairs.getKey().toString()
				logi.logInfo "Execution for the plan :  "+ planNo
				String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
				int planTypeInd = Integer.parseInt(db.executeQueryP2(planTypeQuery))
				logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo
	
				Map<String,String> valueMap = pairs.getValue()
				Map<String,String> value = new TreeMap<String,String> ()
				value.putAll(valueMap)
	
				Iterator service = value.entrySet().iterator()
				logi.logInfo "Invoice calculation for a plan : "+planNo
				int noofUnits =0
				if(planTypeInd==0)
				{
					logi.logInfo "Master Plan : "+ planNo
					String newQuery = "SELECT PLAN_UNITS FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO = " + accountNo
					noofUnits = Integer.parseInt(db.executeQueryP2(newQuery))
				}
				else {
					logi.logInfo("single supp plan")
					String suppPlan = getValueFromRequest("create_acct_complete","//supp_plans").toString()
					if ((!suppPlan.toString().equalsIgnoreCase("null"))&& suppPlan.equalsIgnoreCase(planNo)) {
						noofUnits = 0
	
						logi.logInfo "single_supp_plan ::: " + suppPlan
						logi.logInfo "single_plan_noOfUnits ::: " + noofUnits
					}
					else {
						logi.logInfo "assign or replace supp plan"
						String supp_plan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
						if(supp_plan.equalsIgnoreCase(planNo))
							noofUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
						else{
							supp_plan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
							if(supp_plan.equalsIgnoreCase(planNo))
								noofUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
							else
								noofUnits = 0
						}
					}
				}
	
				logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo
				while(service.hasNext()){
					Map.Entry servicePairs = (Map.Entry) service.next()
					String serviceNo = servicePairs.getKey().toString()
					logi.logInfo "Invoice calculation for a service : "+serviceNo
	
					int seqNo=1;
					//String tieredPricing = servicePairs.getValue().toString()
					int tieredPricingInt = servicePairs.getValue() //Integer.parseInt(tieredPricing)
					logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
					int serviceInvoice
					String query
					switch(tieredPricingInt){
						case 1:
							logi.logInfo "Case 1"
							logi.logInfo "multiplyActivationFeeFlag = " + multiplyActivationFeeFlag
							if (multiplyActivationFeeFlag.toString()!="null" )
							{
								if(multiplyActivationFeeFlag.equalsIgnoreCase("TRUE"))
									query = "SELECT NRSR.RATE_PER_UNIT*"+noofUnits+" as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
								else
									query = "SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
							}
							else
								query = "SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
	
							serviceInvoice = Integer.parseInt(db.executeQueryP2(query))
							logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
							invoice  = invoice+serviceInvoice
							logi.logInfo "After adding with invoice " + invoice
							seqNo++;
							break;
					}
				}
			}
			return invoice
		}
		public String md_ExpInvoice_second(String testCaseId)
		{
	
			logi.logInfo("calling md_ExpInvoiceWithoutUsage_Amt")
			def acct_no = getValueFromResponse('create_acct_complete',"//acct_no")
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			double usg = 0.0
			double cal_usg
			ConnectDB db = new ConnectDB()
			DecimalFormat d = new DecimalFormat("#.##########");
			String apiname=testCaseId.split("-")[2]
			double totalInvoice=0.0
			String invoiceNo = getInvoiceNoVT(clientNo, acct_no).toString();
			logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
			double act = md_serviceInvoiceActivationAmount1(testCaseId).toDouble()
			logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+act.toString())
			double exp = md_ExpInvoice_Amt(testCaseId,acct_no).toDouble()
			logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
			double creditamt=db.executeQueryP2("select NVL(sum(amount),0) from ariacore.credits where acct_no="+acct_no+" and client_no="+clientNo).toString().toDouble()
			totalInvoice =  act+exp-creditamt.round(2)
			logi.logInfo("totalInvoice in md_ExpInvoiceWithoutUsage_Amt method "+totalInvoice)
			return d.format(totalInvoice).toString()
	
		}
		
		
		String md_ExpInvoice_With_Tax(String testCaseId)
		{
			logi.logInfo("calling md_ExpInvoice_With_Tax")
			DecimalFormat d = new DecimalFormat("#.##");
			String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			def taxAmount,invoiceAmount,totalAmt,tax = 0.0
			List seqNum=[]
			String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
			ConnectDB db = new ConnectDB()
			String invoice_no=db.executeQueryP2(invoiceQuery)
			invoiceAmount = md_ExpInvoice_Amount(testCaseId)
			logi.logInfo( "Total invoice amount with credit reduced" +invoiceAmount)
			String plan_query="SELECT DISTINCT(PLAN_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND PLAN_NO IS  NOT NULL"
			ResultSet rs=db.executePlaQuery(plan_query)
			String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO  IN (400,401) ORDER BY SEQ_NUM DESC"
			rs=db.executePlaQuery(seq_query)
			while(rs.next())
			{
			   logi.logInfo" Inside result set of SEQ_NUM....."
			   String seq_no=rs.getString(1)
			   seqNum.add(seq_no)
			}
			for(int k=0;k<seqNum.size();k++)
			{
				String tax_query="SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND SEQ_NUM= "+seqNum.get(k).toString()
				taxAmount=db.executeQueryP2(tax_query).toString()
				tax = tax + taxAmount.toDouble()
				logi.logInfo "Tax amount :" +taxAmount
			}
			
			
			totalAmt = invoiceAmount.toDouble() + tax.toDouble()
			
			return d.format(totalAmt).toString()
			
		}
		public def md_serviceInvoiceAmount1(String testCaseId)
		{
				String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
				DecimalFormat df=new DecimalFormat("#.##")
				String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
				String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
				logi.logInfo "testId : "+ testId
				def invoice = 0
				def total_amt
				String invoiceNo;
				ConnectDB db = new ConnectDB();
				String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
				String currentVirtualTime = db.executeQueryP2(Query);
				logi.logInfo "Current Virtual Time before calculating State Taxes: "+ currentVirtualTime
				// Getting maximum invoice number from db
				invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
				logi.logInfo "Invoice No : "+invoiceNo
				// Getting the plan(s) involved for billing
				HashMap<String, HashMap<String, String>> plans = getPlanServiceAndTieredPricingByInvoice(invoiceNo)
				Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
				resultHash.putAll(plans)
				logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
		
				Iterator iterator = resultHash.entrySet().iterator()
				logi.logInfo "Before Key setting "
				
				String external_tax_client = "SELECT METHOD_CD FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO=" + clientNo
				external_tax_client = db.executeQueryP2(external_tax_client)
				logi.logInfo("External Client ID = " +external_tax_client)
				
				// Iterating through Plans
				while (iterator.hasNext())
				{
					Map.Entry pairs = (Map.Entry) iterator.next()
					String planNo = pairs.getKey().toString()
					logi.logInfo "Execution for the plan :  "+ planNo
					String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
					int planTypeInd = Integer.parseInt(db.executeQueryP2(planTypeQuery))
					logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo
		
					int noOfUnits =0
					// Checking for the type of plan and getting no of units
					if(planTypeInd==0)
					{
						logi.logInfo "Master Plan : "+ planNo
						String newQuery = "SELECT PLAN_UNITS FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO = " + accountNo
						noOfUnits = Integer.parseInt(db.executeQueryP2(newQuery))
					}
					else
					{
						logi.logInfo "Supp Plan : "+ planNo
						String[] noOfUnitArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
						String[] suppPlans = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")
		
						if (!suppPlans.toString().equalsIgnoreCase("null"))
						{
							logi.logInfo("Array of supp plan")
							logi.logInfo "supp_plans :::  " + suppPlans.toString()
							logi.logInfo "noOfUnits :::  " + noOfUnitArray.toString()
							if(Arrays.asList(suppPlans).contains(planNo))
							{
								for( int j=0;j<suppPlans.length;j++)
								{
									if(suppPlans[j].toString().equalsIgnoreCase(planNo))
									{
										noOfUnits = Integer.parseInt(noOfUnitArray[j])
										logi.logInfo "no_of_units : "+noOfUnits+ " plan : " +planNo
										break
									}
								}
							}
						}
						else
						{
							logi.logInfo("single supp plan")
							String suppPlan = getValueFromRequest("create_acct_complete","//supp_plans").toString()
							if ((!suppPlan.toString().equalsIgnoreCase("null"))&& suppPlan.equalsIgnoreCase(planNo))
							{
								noOfUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))
								
								String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
								int noOfUnits2 = Integer.parseInt(db.executeQueryP2(supp_plan_unit_query))
								
								if(noOfUnits2 > noOfUnits)
								{
									noOfUnits = noOfUnits2
								}
								
								logi.logInfo "single_supp_plan :::  " + suppPlan
								logi.logInfo "single_plan_noOfUnits :::  " + noOfUnits
							}
							else
							{
								logi.logInfo "assign or replace supp plan"
								String supp_plan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
								if(supp_plan.equalsIgnoreCase(planNo))
									noOfUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
								else
								{
									supp_plan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
									if(supp_plan.equalsIgnoreCase(planNo))
										noOfUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
									else
									{
										String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
										noOfUnits = Integer.parseInt(db.executeQueryP2(supp_plan_unit_query))
									}
								}
							}
						}
					}
		
					logi.logInfo "No of Units : "+noOfUnits+ " of a plan : " +planNo
		
					Map<String,String> valueMap = pairs.getValue()
					Map<String,String> value = new TreeMap<String,String> ()
					value.putAll(valueMap)
					Iterator service = value.entrySet().iterator()
					logi.logInfo "Invoice calculation for a plan : "+planNo
					// Iterating through Plans
					while(service.hasNext())
					{
						Map.Entry servicePairs = (Map.Entry) service.next()
						String serviceNo = servicePairs.getKey().toString()
						logi.logInfo "Invoice calculation for a service : "+serviceNo
						String isServiceTaxableQuery = "SELECT TAXABLE_IND from ariacore.all_service where service_no = " +serviceNo
						String isServiceTaxable = db.executeQueryP2(isServiceTaxableQuery)
						logi.logInfo "Service "+ serviceNo+" taxable/not : "+isServiceTaxable
						String zeroDollarPlanCheckQuery = "select distinct trunc(sum(rate_per_unit)) as RatePerUnit from ariacore.new_rate_sched_rates where schedule_no in (select schedule_no from ariacore.plan_rate_sched_map where plan_no = "+planNo+" and client_no="+clientNo+") and service_no = "+serviceNo+" and client_no="+clientNo
						int isZeroDollar = Integer.parseInt(db.executeQueryP2(zeroDollarPlanCheckQuery))
						logi.logInfo "Service "+ serviceNo+" Zero Dollar/not : "+isZeroDollar
		
						//Checking the service is taxable and non-zero dollar plan or not
						//if(isServiceTaxable.equalsIgnoreCase("1") && (isZeroDollar!=0)){
							logi.logInfo "Service is taxable : "+serviceNo
							int seqNo=1;
							String tieredPricing = servicePairs.getValue().toString()
							int tieredPricingInt = Integer.parseInt(tieredPricing)
							logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
							def serviceInvoice
							String query
							int upgraded_units
							if(testCaseId.contains("update_master_plan"))
							{
								String units_query = "select recurring_factor,rate_per_unit from ariacore.acct_rates_history_details where acct_no = " + accountNo + " and client_no = " + clientNo + " and plan_no = " + planNo + " and service_no=" + serviceNo + " and (" + noOfUnits + " between from_unit and to_unit)"
								ResultSet rs = db.executePlaQuery(units_query)
								int max_units=noOfUnits, min_units=0
								while(rs.next())
								{
									int db_units = Integer.parseInt(rs.getString(1))
									if(db_units >= max_units)
									{
										max_units = db_units
									}
									else
									{
										min_units = db_units
									}
									if(Integer.parseInt(rs.getString(2)) == 0)
										max_units=min_units=0
								}
								upgraded_units = max_units-min_units
								logi.logInfo "Units Difference : " + upgraded_units
							}
							switch(tieredPricingInt)
							{
								case 1:
									logi.logInfo "Case 1"
									if((testCaseId.contains("update_master_plan")) && (!testCaseId.contains("^update_master_plan")))
										query = "SELECT NRSR.RATE_PER_UNIT * " + upgraded_units + " as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
									//else if ((testCaseId.contains("update_acct_complete")) && (!testCaseId.contains("^update_acct_complete")))
									//	query = "SELECT NRSR.RATE_PER_UNIT * " + (prorationFactor*noOfUnits) + " as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
									else
										query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
									seqNo++;
									break;
								case 2 :
									logi.logInfo "Case 2"
									if((testCaseId.contains("update_master_plan")) && (!testCaseId.contains("^update_master_plan")))
										query ="select rate_per_unit*" + upgraded_units +" from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
									//else if ((testCaseId.contains("update_acct_complete")) && (!testCaseId.contains("^update_acct_complete")))
									//	query = "select rate_per_unit*" + (prorationFactor*noOfUnits) + " from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
									else
										//query ="select NVL(SUM(rate_per_unit*recurring_factor),0) from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
										query = "SELECT case when sum(a.act_debit) is null then 0 else sum(a.act_debit) end as res from (SELECT (rate_per_unit*recurring_factor) AS act_debit FROM ariacore.acct_rates_history_details WHERE acct_no = "+accountNo+" AND client_no = "+clientNo+" AND service_no = "+serviceNo+" AND plan_no = "+planNo+" AND ("+noOfUnits+" BETWEEN from_unit AND to_unit) AND rownum = 1 ORDER BY rate_hist_seq_no DESC)a"
									break;
								case 3 :
									logi.logInfo "Case 3"
									if((testCaseId.contains("update_master_plan")) && (!testCaseId.contains("^update_master_plan")))
										query = "select 1*" + upgraded_units +" from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
									//else if ((testCaseId.contains("update_acct_complete")) && (!testCaseId.contains("^update_acct_complete")))
									//	query = "select 1*" + (prorationFactor*noOfUnits) + " from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
									else
										query = "SELECT case when sum(a.act_debit) is null then 0 else sum(a.act_debit) end as res from (SELECT rate_per_unit AS act_debit FROM ariacore.acct_rates_history_details WHERE acct_no = "+accountNo+" AND client_no = "+clientNo+" AND service_no = "+serviceNo+" AND plan_no = "+planNo+" AND ("+noOfUnits+" BETWEEN from_unit AND to_unit) AND rownum = 1 ORDER BY rate_hist_seq_no DESC)a"
									break;
							}
							logi.logInfo "Before adding with invoice " + invoice
							serviceInvoice = (db.executeQueryP2(query)).toString().toDouble();
							logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
							
											
							invoice  = invoice.toDouble()+serviceInvoice.toDouble()
							logi.logInfo "After adding with invoice " + invoice
						//}
		
						//else
							//log50i.logInfo "Service is not taxable : "+ serviceNo
					}
					
					
					 logi.logInfo " aTotal invoice " + invoice
					
					
				}
				logi.logInfo " Coming Here"
				logi.logInfo " Fromated" + df.format(invoice).toString()
				return df.format(invoice).toString()			
				}
		
		public String md_ExpectedInvoiceWithMultipleServiceCredit_TAX(String testCaseId){
			logi.logInfo "Inside md_ExpectedInvoiceWithServiceCredit_CR0"
			String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			ConnectDB db = new ConnectDB()
			double federalTax = 0
			// Calculating service, federal and state tax amount
			def serviceInvoiceAmount = md_serviceInvoiceAmount1(testCaseId)
			logi.logInfo " Service Amount" + serviceInvoiceAmount
			//double creditamt=db.executeQueryP2("select NVL(sum(amount),0) from ariacore.credits where acct_no="+accountNo+" and client_no="+clientNo).toString().toDouble()
			
			//int serviceInvoiceActivationAmount = md_serviceInvoiceActivationAmount(testCaseId)
			String client_qry = "SELECT METHOD_CD FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO="+ clientNo
			String Client_cd= db.executeQueryP2(client_qry)
			logi.logInfo "client_cd " + Client_cd
			if(Client_cd !="6")
			{
				 federalTax = md_federalTax_ex(testCaseId).toDouble()
			}
			double stateTax =  md_stateTax_ex(testCaseId).toDouble()
			// Calculating deduction amount
			String creditAmount = md_ACCOUNT_MULTIPLE_CREDIT_APPLIED(testCaseId)
			String activation_flg = db.executeQueryP2("select count(service_no) from ariacore.gl_detail where invoice_no=(SELECT max(invoice_no) from ariacore.gl where acct_no = " + accountNo + ") and comments like '%ACTIVATION%'")
			logi.logInfo("Activation flag set : " + activation_flg)
			double activation_amt = 0
			if(activation_flg == "1") {
				activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
				logi.logInfo "Activation amount : " + (activation_amt)/2
				activation_amt = activation_amt/2
			}
			else if(activation_flg != "0" && activation_flg != "1") {
				activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
				logi.logInfo "Activation amount : " + activation_amt
			}
			// Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
			logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax + " CreditAmount : "+ creditAmount + "Activation Amount : " + activation_amt
			double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax  - creditAmount.toDouble() + activation_amt
			logi.logInfo 'Total Invoice : ' + totalInvoice
			
			return totalInvoice.toString()
		}
		
		public HashMap<String,String> md_get_ref_charge_line_no_response(String testcaseID)
		{
				int seqnumcount=0;
				String ref_charge_line=null;
				HashMap row = new HashMap();
				def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
				String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
				ConnectDB db = new ConnectDB()
				String query = "select count(seq_num) from ariacore.gl_detail where invoice_no = (select max(invoice_no) from ariacore.gl where acct_no = " + acct_no +") "
				seqnumcount = db.executeQueryP2(query).toInteger()
				logi.logInfo "seqnumcount : " +seqnumcount
				for(int i = 1 ;i<= seqnumcount; i++)
				{
					logi.logInfo "seqnumcount : " +seqnumcount
					ref_charge_line = getValueFromResponse('create_order_with_plans',"//cart_invoice_line_items/item["+i+"]/ref_charge_line_no")
					logi.logInfo "ref_charge_line : " +ref_charge_line
					if(ref_charge_line == "NoVal")
					{
						ref_charge_line ="null"
					}
					logi.logInfo "ref_charge_line 1: " +ref_charge_line
					row.put("Sequence No" + i ,ref_charge_line.toString())
				}
				return row
		}
		public HashMap<String,String> md_get_ref_charge_line_no_DB(String testcaseID)
		{
				int seqnumcount=0;
				String ref_charge_line=null,invoice_no;
				HashMap row = new HashMap();
				def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
				String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
				ConnectDB db = new ConnectDB()
				String query = "select count(seq_num) from ariacore.gl_detail where invoice_no = (select max(invoice_no) from ariacore.gl where acct_no = " + acct_no +") "
				seqnumcount = db.executeQueryP2(query).toInteger()
				String query1 = "SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE CLIENT_NO ="+clientNo+ "AND ACCT_NO =" +acct_no
				invoice_no = db.executeQueryP2(query1)
				for(int i = 1 ;i<= seqnumcount; i++)
				{
					String query2 = "select NVL(credited_seq_num,0) from ariacore.gl_credit_detail where invoice_no = "+ invoice_no +" and gl_dtl_credit_seq_num =" + i
					ref_charge_line = db.executeQueryP2(query2)
					logi.logInfo "ref_charge_line : " +ref_charge_line
					row.put("Sequence No" + i ,ref_charge_line.toString())
				}
				return row
		}
		public HashMap<String,String> md_get_ref_charge_line_no_rule_DB(String testcaseID)
		{
				int seqnumcount=0;
				String ref_charge_line=null,invoice_no;
				HashMap row = new HashMap();
				def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
				String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
				ConnectDB db = new ConnectDB()
				String query = "select count(seq_num) from ariacore.gl_detail where invoice_no = (select max(invoice_no) from ariacore.gl where acct_no = " + acct_no +") "
				seqnumcount = db.executeQueryP2(query).toInteger()
				String query1 = "SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE CLIENT_NO ="+clientNo+ "AND ACCT_NO =" +acct_no
				invoice_no = db.executeQueryP2(query1)
				for(int i = 1 ;i<= seqnumcount; i++)
				{
					String query2 = "select NVL(GL_CHARGE_SEQ_NUM,0) from ariacore.ALL_INVOICE_DISCOUNT_DETAILS where invoice_no = "+ invoice_no +" and gl_discount_seq_num =" + i
					ref_charge_line = db.executeQueryP2(query2)
					logi.logInfo "ref_charge_line : " +ref_charge_line
					row.put("Sequence No" + i ,ref_charge_line.toString())
				}
				return row
		}
		
		
		public def totalServiceCreditAppliedToInvoice(String testcaseID){
			
			HashMap row = new HashMap();
			def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			ConnectDB db = new ConnectDB()
			String query = "select count(seq_num) from ariacore.gl_detail where invoice_no = (select max(invoice_no) from ariacore.gl where acct_no = " + acct_no +") "
			seqnumcount = db.executeQueryP2(query).toInteger()
		}
		
		
		public def md_serviceCreditLineItemDB(String testcaseID){
			logi.logInfo " Inside service credit DB....."
		
		HashMap row = new HashMap();
		def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		String credit_q = "SELECT * FROM ARIACORE.CREDITS WHERE ACCT_NO = "+acct_no+" AND ELIGIBLE_PLAN_NO IS NOT NULL AND ELIGIBLE_SERVICE_NO IS NOT NULL"
			
		ResultSet rs=db.executePlaQuery(credit_q)
		logi.logInfo "Executed result set...."
		List amt=[],service=[],plan=[],creditId=[]
		
		while(rs.next()){
			plan.add(rs.getString("ELIGIBLE_PLAN_NO"))
			service.add(rs.getString("ELIGIBLE_SERVICE_NO"))
			creditId.add(rs.getString("CREDIT_ID"))
			amt.add(rs.getString("AMOUNT"))
		}
		int appliedAmt =0
		for(int i=0;i<plan.size();i++)
		{
			logi.logInfo " Inside result Credit amount calculation....."
			String plan_no=plan.get(i)
			String service_no=service.get(i)
			String creditID = creditId.get(i)
			String amount = amt.get(i)
			def invoiceAmt = db.executeQueryP2("SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO = (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+") AND PLAN_NO="+plan_no+" AND SERVICE_NO="+service_no)
			invoiceAmt = (Integer.parseInt(invoiceAmt) - appliedAmt).toString()
			logi.logInfo "Invoice"
			//String creditAmt = db.executeQueryP2(SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO = (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+") AND ORIG_CREDIT_ID="+rs.getString("CREDIT_ID"))
			def totalAmtToBeApply = db.executeQueryP2("SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO = (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+") AND PLAN_NO="+plan_no+" AND SERVICE_NO="+service_no)
			logi.logInfo " Total"+amount
			logi.logInfo " totalAmtToBeApply "+totalAmtToBeApply
			logi.logInfo " invoiceAmt "+invoiceAmt
			logi.logInfo "divide "+(Integer.parseInt(amount)/Integer.parseInt(totalAmtToBeApply))
			appliedAmt =(Integer.parseInt(amount)*Integer.parseInt(invoiceAmt))/(Integer.parseInt(totalAmtToBeApply)-appliedAmt)
			logi.logInfo " appliedAmt " +appliedAmt
			if(appliedAmt >= Integer.parseInt(invoiceAmt))
				row.put("Credit_ID: "+creditID+" Applied Amount on Service: "+service_no ,invoiceAmt)
			else
				row.put("Credit_ID: "+creditID+" Applied Amount on Service: "+service_no ,appliedAmt)
		}
		return row.sort()
		}

public def md_serviceCreditLineItemAPI(String testcaseID){
	logi.logInfo " Inside service credit APi....."
		HashMap row = new HashMap();
		def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		ConnectDB db1 = new ConnectDB()
		String credit_q = "SELECT * FROM ARIACORE.CREDITS WHERE ACCT_NO = "+acct_no+" AND ELIGIBLE_PLAN_NO IS NOT NULL AND ELIGIBLE_SERVICE_NO IS NOT NULL"
		
		ResultSet rs=db1.executePlaQuery(credit_q)
		logi.logInfo "Executed result set...."
		List amt=[],service=[],plan=[],creditId=[]
		
		while(rs.next()){
			plan.add(rs.getString("ELIGIBLE_PLAN_NO"))
			service.add(rs.getString("ELIGIBLE_SERVICE_NO"))
			creditId.add(rs.getString("CREDIT_ID"))
			amt.add(rs.getString("AMOUNT"))
		}
		for(int i=0;i<plan.size();i++)
		{
			logi.logInfo " Inside result Credit amount calculation....."
			String plan_no=plan.get(i)
			String service_no=service.get(i)
			String creditID = creditId.get(i)
			String amount = amt.get(i)
			logi.logInfo " Inside service credit DB.....Invoice "+creditID
			def creditAmt = db.executeQueryP2("SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO = (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+") AND ORIG_CREDIT_ID="+creditID)
			logi.logInfo " Inside service credit DB.....Credit"
			row.put("Credit_ID: "+creditID+" Applied Amount on Service: "+service_no ,((Integer.parseInt(creditAmt))*-1).toString())
		}
		return row.sort()
}
	
public def md_acctCreditLineItemEXP(String testcaseID){
	logi.logInfo " Inside service credit DB....."
boolean status = true
HashMap row = new HashMap();
def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
ConnectDB db = new ConnectDB()
ConnectDB dbGL = new ConnectDB()
double totalAppliedAmt = 0.0
String credit_q = "SELECT * FROM ARIACORE.CREDITS WHERE ACCT_NO = "+acct_no+" AND ELIGIBLE_PLAN_NO IS NOT NULL AND ELIGIBLE_SERVICE_NO IS NOT NULL"
ResultSet rs=db.executePlaQuery(credit_q)
String plan_no = null,service_no=null,creditID=null,amount="0"
double totAmt = 0.0
while(rs.next()){
logi.logInfo " Inside result Credit amount calculation....."
plan_no=rs.getString("ELIGIBLE_PLAN_NO")
service_no=rs.getString("ELIGIBLE_SERVICE_NO")
creditID = rs.getString("CREDIT_ID")
amount = (Integer.parseInt(amount) + Integer.parseInt(rs.getString("AMOUNT"))).toString()
logi.logInfo "Executed result set....Amount..........."+amount
}
String totAcctCreToBeApply = db.executeQueryP2("SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO = (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+") AND ORIG_CREDIT_ID IS NULL")
credit_q = "SELECT * FROM ARIACORE.CREDITS WHERE ACCT_NO = "+acct_no+" AND ELIGIBLE_PLAN_NO IS NULL AND ELIGIBLE_SERVICE_NO IS NULL"
rs=db.executePlaQuery(credit_q)
int count = 1
while(rs.next())
{
	int ccAmt = rs.getString("Amount").toInteger()
	totAmt = rs.getString("Amount").toDouble()
	ResultSet rsGL = dbGL.executePlaQuery("SELECT * FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO = (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+") AND ORIG_CREDIT_ID IS NULL")
	logi.logInfo "Invoice"
	
	while(rsGL.next()){
		
		int appliedAmt = 0
		String debit = null
		String ccServiceNO = rsGL.getString("SERVICE_NO")
		String ccPlanNo = rsGL.getString("PLAN_NO")
		debit = rsGL.getString("DEBIT")
		if((rsGL.getString("PLAN_NO")==plan_no)&&(rsGL.getString("SERVICE_NO")==service_no)){
			
		
		
		//def totalAmtToBeApply = dbGL.executeQueryP2("SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO = (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+") AND PLAN_NO="+plan_no+" AND SERVICE_NO="+service_no)
		logi.logInfo " Total"+amount
		logi.logInfo " totalAmtToBeApply "+debit
		logi.logInfo " invoiceAmt "+debit
		//appliedAmt =(Integer.parseInt(amount)*Integer.parseInt(invoiceAmt))/Integer.parseInt(totalAmtToBeApply)
		appliedAmt =(Integer.parseInt(debit)/Integer.parseInt(debit))*Integer.parseInt(amount)
		if(appliedAmt >= Integer.parseInt(debit)){
			appliedAmt = Integer.parseInt(debit)
			logi.logInfo "appliedAmt ..............=" +appliedAmt
		}
		totAcctCreToBeApply = (Integer.parseInt(totAcctCreToBeApply) - appliedAmt).toString()
		if(appliedAmt >= Integer.parseInt(debit)){
			logi.logInfo "Continue................ ="
			continue;
		}
		}
		if(Integer.parseInt(debit)!=0){
			logi.logInfo "Debit not eqaul to zero "+debit
		logi.logInfo " CCAmount"+ccAmt
		logi.logInfo " totAcctCreToBeApply  "+totAcctCreToBeApply
		logi.logInfo " debit "+debit
		logi.logInfo "(Integer.parseInt(totAcctCreToBeApply) - appliedAmt) ="+(Integer.parseInt(totAcctCreToBeApply) - appliedAmt)
		logi.logInfo "(ccAmt/(Integer.parseInt(totAcctCreToBeApply) - appliedAmt)) = "+(ccAmt/(Integer.parseInt(totAcctCreToBeApply) - appliedAmt))
		logi.logInfo "Test2 = "+(((Integer.parseInt(debit)-appliedAmt)/(Integer.parseInt(totAcctCreToBeApply)))*ccAmt).toString()
		
		
		int appliedLineAmt = (((Integer.parseInt(debit)-appliedAmt)/(Integer.parseInt(totAcctCreToBeApply)))*ccAmt)
		totalAppliedAmt = totalAppliedAmt + (df.format(((Integer.parseInt(debit)-appliedAmt)/(Integer.parseInt(totAcctCreToBeApply)))*ccAmt)).toDouble()
		logi.logInfo "appliedLineAmt "+appliedLineAmt
		if(appliedLineAmt >= Integer.parseInt(debit)){
			status = false
			logi.logInfo "Test3 "+Integer.parseInt(debit)
			logi.logInfo "Test3 "+df.format(Integer.parseInt(debit)).toString()
			row.put("Applied Acct credit Amount on Line Item: "+count,df.format(Integer.parseInt(debit)-appliedAmt).toString())
		}
		else
			row.put("Applied Acct credit Amount on Line Item: "+count,df.format(((Integer.parseInt(debit)-appliedAmt)/(Integer.parseInt(totAcctCreToBeApply)))*ccAmt).toString())
		count++
		}
	}
}
logi.logInfo "totalAppliedAmt "+totalAppliedAmt
if((totalAppliedAmt.toString()!=totAmt.toString())&& status){
	logi.logInfo "Applied Amt and credit amount not matched"
	double amtDiff = totalAppliedAmt - totAmt.toDouble()
	amtDiff = df.format(amtDiff).toDouble()
	logi.logInfo "Diff amount : "+amtDiff
	Iterator<Map.Entry<Integer,Integer>> iter = row.entrySet().iterator();
	Map.Entry<Integer,Integer> entry = null;
	while(iter.hasNext()) {
		entry = iter.next();
	}
	
	logi.logInfo "Last Value of map  : "+entry.getValue().toString()
	def record = (entry.getValue().toDouble()) as double
	logi.logInfo "record "+ record
	record = record - amtDiff
	logi.logInfo " FRecord " +record
	logi.logInfo "count "+count
	logi.logInfo "entry.getKey() "+entry.getKey()
	row.put(entry.getKey(),df.format(record))
	
}
return row.sort()
}

public def md_acctCreditLineItemACT(String testcaseID){
logi.logInfo " Inside md_acctCreditLineItemACT....."
HashMap row = new HashMap();
def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
ConnectDB db = new ConnectDB()
ConnectDB db1 = new ConnectDB()
String credit_q = "SELECT * FROM ARIACORE.CREDITS WHERE ACCT_NO = "+acct_no+" AND ELIGIBLE_PLAN_NO IS NULL AND ELIGIBLE_SERVICE_NO IS NULL"

ResultSet rs=db1.executePlaQuery(credit_q)
logi.logInfo "Executed result set...."
int count = 1
while(rs.next())
{
	logi.logInfo " Inside result Credit amount calculation....."
	
	String creditID = rs.getString("CREDIT_ID")
	logi.logInfo " Inside service credit DB.....Invoice "+creditID
	String q = "SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO = (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+") AND ORIG_CREDIT_ID="+creditID
	logi.logInfo "Query Used "+q
	ResultSet rsDetail=db.executePlaQuery(q)

	while(rsDetail.next()){
		
	logi.logInfo "Test 1 "+rsDetail.getString("DEBIT")
	row.put("Applied Acct credit Amount on Line Item: "+count ,df.format(((rsDetail.getDouble("DEBIT"))*-1)).toString())
	count++
	}
}
return row.sort()
}


public def md_acctBillingDatesExp(String testcaseID){
	logi.logInfo "Inside the acct bill dates Exp"
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
	HashMap billDates = new HashMap();
	def acct_No = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	ConnectDB db = new ConnectDB()
	String plan_no_q1 = "select plan_no from ariacore.acct where acct_no="+acct_No+"  and client_no="+clientNo
	String plan_no=db.executeQueryP2(plan_no_q1)
	String bill_interval_q="select billing_interval from ariacore.client_plan where plan_no="+plan_no+" and client_no="+clientNo
	int bill_interval=db.executeQueryP2(bill_interval_q).toInteger()
	int ref_bill_interval = bill_interval
	logi.logInfo " bill interval is " + bill_interval
	def currentVT = db.executeQueryP2("SELECT TO_CHAR(ARIACORE.ARIAVIRTUALTIME(" + clientNo + "),'DD-MON-YYYY') from DUAL");
	logi.logInfo "Current VT time"+currentVT
	def createdDate = db.executeQueryP2("SELECT TO_CHAR(CREATED,'DD-MON-YYYY') from ariacore.acct where acct_no="+acct_No)
	logi.logInfo "Next Billa date "+createdDate
	logi.logInfo "Next Billa dat"
	def diffDays = db.executeQueryP2("SELECT to_date('"+currentVT+"','DD-MM-YYYY')-to_date('"+createdDate+"','DD-MM-YYYY') AS DiffDate from dual")
	logi.logInfo "DifDays "+diffDays
	if(diffDays!="0"){
		logi.logInfo "Diff mon "
		logi.logInfo "Diff mon "+Integer.parseInt(diffDays)/(bill_interval*30)
		bill_interval = ((Integer.parseInt(diffDays)/(bill_interval*30))+1)
		logi.logInfo " bill interval is " + bill_interval
	}
	String dateCal = "SELECT TO_CHAR(CREATED,'DD')as bill_day,TO_CHAR(ADD_MONTHS(CREATED-1,"+bill_interval+"),'DD-MON-YYYY')as last_bill_thru_date,TO_CHAR(ADD_MONTHS(CREATED,"+bill_interval+"),'DD-MON-YYYY') as next_bill_date, TO_CHAR(ADD_MONTHS(ADD_MONTHS(CREATED,"+bill_interval+"),-1),'DD-MON-YYYY') as last_bill_date,TO_CHAR(ADD_MONTHS(ADD_MONTHS(CREATED,"+bill_interval+"),-1),'DD-MON-YYYY') as last_recur_bill_date from ariacore.acct where acct_no="+acct_No
	logi.logInfo "Query Used "+dateCal
	ResultSet rsDetail=db.executePlaQuery(dateCal)

	while(rsDetail.next()){
		billDates.put("Bill Day",rsDetail.getString(1))
		billDates.put("Last Bill Thru Date",rsDetail.getString(2))
		billDates.put("Next Bill Date",rsDetail.getString(3))
		billDates.put("Last Bill Date",rsDetail.getString(4))
		billDates.put("Last Recurring Bill date",rsDetail.getString(5))
	}
	if((ref_bill_interval+1)==bill_interval)
		billDates.put("Last Bill Date",currentVT)
	return billDates.sort()
}

public def md_acctBillingDatesAct(String testcaseID){
	logi.logInfo "Inside the acct bill dates Act"
	HashMap billDates = new HashMap();
	def acct_No = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	ConnectDB db = new ConnectDB()
	String dateCal = "SELECT bill_day,TO_CHAR(last_bill_thru_date,'DD-MON-YYYY'),TO_CHAR(next_bill_date,'DD-MON-YYYY'),TO_CHAR(last_bill_date,'DD-MON-YYYY'),TO_CHAR(last_recur_bill_date,'DD-MON-YYYY') from ariacore.acct where acct_no="+acct_No
	logi.logInfo "Query Used "+dateCal
	ResultSet rsDetail=db.executePlaQuery(dateCal)

	while(rsDetail.next()){
		if((rsDetail.getInt(1)/10)==0)
			billDates.put("Bill Day","0"+rsDetail.getString(1))
		else
			billDates.put("Bill Day",rsDetail.getString(1))
		billDates.put("Last Bill Thru Date",rsDetail.getString(2))
		billDates.put("Next Bill Date",rsDetail.getString(3))
		billDates.put("Last Bill Date",rsDetail.getString(4))
		billDates.put("Last Recurring Bill date",rsDetail.getString(5))
	}
	return billDates.sort()
}


public def md_acctBillingInterValExp(String testcaseID){
	logi.logInfo "Inside the acct bill dates Exp"
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
	HashMap billDates = new HashMap();
	def acct_No = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	ConnectDB db = new ConnectDB()
	String plan_no_q1 = "select plan_no from ariacore.acct where acct_no="+acct_No+"  and client_no="+clientNo
	String plan_no=db.executeQueryP2(plan_no_q1)
	String bill_interval_q="select billing_interval from ariacore.client_plan where plan_no="+plan_no+" and client_no="+clientNo
	int bill_interval=db.executeQueryP2(bill_interval_q).toInteger()
	int ref_bill_interval = bill_interval
	logi.logInfo " bill interval is " + bill_interval
	def currentVT = db.executeQueryP2("SELECT TO_CHAR(ARIACORE.ARIAVIRTUALTIME(" + clientNo + "),'DD-MON-YYYY') from DUAL");
	logi.logInfo "Current VT time"+currentVT
	def createdDate = db.executeQueryP2("SELECT TO_CHAR(CREATED,'DD-MON-YYYY') from ariacore.acct where acct_no="+acct_No)
	logi.logInfo "Next Billa date "+createdDate
	logi.logInfo "Next Billa dat"
	def diffDays = db.executeQueryP2("SELECT to_date('"+currentVT+"','DD-MM-YYYY')-to_date('"+createdDate+"','DD-MM-YYYY') AS DiffDate from dual")
	logi.logInfo "DifDays "+diffDays
	if(diffDays!="0"){
		logi.logInfo "Diff mon "
		logi.logInfo "Diff mon "+Integer.parseInt(diffDays)/(bill_interval*30)
		bill_interval = ((Integer.parseInt(diffDays)/(bill_interval*30))+1)
		logi.logInfo " bill interval is " + bill_interval
	}
	String invoiceCount="select count(*) from ariacore.gl where acct_no="+acct_No
	int countInvoice=db.executeQueryP2(invoiceCount).toInteger()
	if(countInvoice > 1){
		//bill_interval++
	}
	
	String dateCal = "select to_char(ADD_MONTHS(CREATED-1,"+bill_interval+"),'DD-MON-YYYY') as ad_to_date,to_char(ADD_MONTHS(CREATED,"+(bill_interval - 1)+"),'DD-MON-YYYY') as Ad_from_date,to_char(ADD_MONTHS(ADD_MONTHS(CREATED-1,-1),"+bill_interval+"),'DD-MON-YYYY') as us_to_date,to_char(ADD_MONTHS(ADD_MONTHS(CREATED,-2),"+(bill_interval)+"),'DD-MON-YYYY') as us_from_date from ariacore.acct where acct_no="+acct_No
	logi.logInfo "Query Used "+dateCal
	ResultSet rsDetail=db.executePlaQuery(dateCal)

	while(rsDetail.next()){
		
		billDates.put("Advanced from date",rsDetail.getString(2))
		billDates.put("Advanced to date",rsDetail.getString(1))
		billDates.put("Usage from date",rsDetail.getString(4))
		billDates.put("Usage to date",rsDetail.getString(3))
		
	}
	
	
	if(countInvoice == 1){
		billDates.put("Usage from date","null")
		billDates.put("Usage to date","null")
	}
	return billDates.sort()
}

public def md_acctBillingIntervalAct(String testcaseID){
	logi.logInfo "Inside the acct bill dates Act"
	HashMap billDates = new HashMap();
	def acct_No = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	ConnectDB db = new ConnectDB()
	String dateCal = "select TO_CHAR(bill_from_date,'DD-MON-YYYY'),TO_CHAR(bill_thru_date,'DD-MON-YYYY'),TO_CHAR(usage_from_date,'DD-MON-YYYY'),TO_CHAR(usage_to_date,'DD-MON-YYYY') from ariacore.gl where invoice_no =(select max(invoice_no) from ariacore.gl where acct_no="+acct_No+")"
	logi.logInfo "Query Used "+dateCal
	ResultSet rsDetail=db.executePlaQuery(dateCal)

	while(rsDetail.next()){
		
		billDates.put("Advanced from date",rsDetail.getString(1))
		billDates.put("Advanced to date",rsDetail.getString(2))
		billDates.put("Usage from date",rsDetail.getString(3))
		billDates.put("Usage to date",rsDetail.getString(4))
	}
	return billDates.sort()
}

}//ServiceCreditsVerificationMethods class end