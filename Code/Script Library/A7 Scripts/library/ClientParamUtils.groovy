package library

import java.sql.ResultSet
import java.sql.SQLException
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.*;
import java.time.format.*;
import java.time.temporal.*;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.util.concurrent.TimeUnit;


import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.transform.OutputKeys;
import org.apache.poi.hssf.record.aggregates.WorksheetProtectionBlock;
import org.apache.poi.hssf.usermodel.*;

import library.Constants.*;
import library.GenericLibrary.Common;

class ClientParamUtils {

	VerificationMethods vm =new VerificationMethods()
	
	static ConnectDB db = null;
	String tmpTaxUrl = null;
	LogResult logi = new LogResult(Constant.logFilePath)

	public boolean advanceVirtualTimeBy1Day(String clientNo) {

		boolean isSuccessful = false;
		db = new ConnectDB();

		/*batchLogFile.logInfo("-----------------------------------------------------")
		 batchLogFile.logInfo("Virtual Time Advancement for Client No : " + clientNo)
		 batchLogFile.logInfo("-----------------------------------------------------")*/

		String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);
		// Stores the current virtual time in the specified format
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);

		// Add 15 days
		Calendar cal3 = Calendar.getInstance();
		cal3.setTime(date);
		cal3.add(Calendar.DAY_OF_MONTH, 1);

		long diff = getDiffInHours(cal3, cal1);

		long beforeAniversaryDay = diff - 24
		isSuccessful = advanceOffsetHours(clientNo, diff);
	}

	//	public boolean advanceVirtualTimeToAnniversary(String clientNo) {
	//
	//		boolean isSuccessful = false;
	//		db = new ConnectDB();
	//
	//		String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);
	//		// Stores the current virtual time in the specified format
	//		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
	//		Calendar cal1 = Calendar.getInstance();
	//		cal1.setTime(date);
	//
	//		/*batchLogFile.logInfo("-----------------------------------------------------")
	//		batchLogFile.logInfo("Virtual Time Advancement for Client No : " + clientNo)
	//		batchLogFile.logInfo("-----------------------------------------------------")*/
	//
	//		// Add 15 days
	//		Calendar cal2 = Calendar.getInstance();
	//		cal2.setTime(date);
	//		cal2.add(Calendar.DAY_OF_MONTH, 15);
	//
	//		// Add 1 month
	//		Calendar cal3 = Calendar.getInstance();
	//		cal3.setTime(date);
	//		cal3.add(Calendar.MONTH, 1);
	//		cal3.add(Calendar.DAY_OF_MONTH, 1);
	//
	//		long diff = getDiffInHours(cal3, cal2);
	//		isSuccessful = advanceOffsetHours(clientNo, diff);
	//	}

	public boolean advanceVirtualTimeToAnniversary(String clientNo) {

		boolean isSuccessful = false;
		ConnectDB db = new ConnectDB();

		String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);
		// Stores the current virtual time in the specified format
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		cal1.add(Calendar.DAY_OF_MONTH, -15)

		//batchLogFile.logInfo("-----------------------------------------------------")
		//batchLogFile.logInfo("Virtual Time Advancement for Client No : " + clientNo)
		//batchLogFile.logInfo("-----------------------------------------------------")

		// Add 15 days
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(cal1.getTime());
		cal2.add(Calendar.DAY_OF_MONTH, 15);

		// Add 1 month
		Calendar cal3 = Calendar.getInstance();
		cal3.setTime(cal1.getTime());
		cal3.add(Calendar.MONTH, 1);

		long diff = getDiffInHours(cal3, cal2);
		isSuccessful = advanceOffsetHours(clientNo, diff);
	}

	public boolean advanceVirtualTimeToBeforeOneDayOfAnniversary(String clientNo, int daysAlreadyAdvanced) {

		boolean isSuccessful = false;
		db = new ConnectDB();

		String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);

		// Stores the current virtual time in the specified format Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);

		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		cal1.add(Calendar.DAY_OF_MONTH, -daysAlreadyAdvanced)

		//batchLogFile.logInfo("-----------------------------------------------------")
		//batchLogFile.logInfo("Virtual Time Advancement for Client No : " + clientNo)
		//batchLogFile.logInfo("-----------------------------------------------------")

		// Add 15 days
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(cal1.getTime());
		cal2.add(Calendar.DAY_OF_MONTH, (daysAlreadyAdvanced+2));

		// Add 1 month
		Calendar cal3 = Calendar.getInstance();
		cal3.setTime(cal1.getTime());
		cal3.add(Calendar.MONTH, 1);

		long diff = getDiffInHours(cal3, cal2);
		isSuccessful = advanceOffsetHours(clientNo, diff);
	}


	public boolean advanceVirtualTimeForDays(String clientNo, int days) {

		boolean isSuccessful = false;
		db = new ConnectDB();
		/*batchLogFile.logInfo("-----------------------------------------------------")
		 batchLogFile.logInfo("Virtual Time Advancement for Client No : " + clientNo)
		 batchLogFile.logInfo("-----------------------------------------------------")*/

		String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);
		// Stores the current virtual time in the specified format
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);

		// Add 15 days
		Calendar cal3 = Calendar.getInstance();
		cal3.setTime(date);
		cal3.add(Calendar.DAY_OF_MONTH, days);

		long diff = getDiffInHours(cal3, cal1);
		logi.logInfo("Advancing Hours ::: " + diff.toString())
		isSuccessful = advanceOffsetHours(clientNo, diff);
	}

	public boolean advanceVirtualTime(String clientNo, String duration) throws SQLException, ParseException {

		boolean isSuccessful = false;
		db = new ConnectDB();

		String[] spDuration = duration.split(":");
		String lowestValue = spDuration[0];
		String highestValue = spDuration[1];

		/*	batchLogFile.logInfo("-----------------------------------------------------")
		 batchLogFile.logInfo("Virtual Time Advancement for Client No : " + clientNo)
		 batchLogFile.logInfo("-----------------------------------------------------")*/

		if(Float.parseFloat(lowestValue).toString().equals("0.15") &&  Float.parseFloat(highestValue).toString().equals("0.0")) {

			String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);
			// Stores the current virtual time in the specified format
			Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(date);

			// Add one day
			Calendar cal2 = Calendar.getInstance()
			cal2.setTime(date)
			cal2.add(Calendar.DAY_OF_MONTH, 1)

			// Add two days
			Calendar cal3 = Calendar.getInstance()
			cal3.setTime(date)
			cal3.add(Calendar.DAY_OF_MONTH, 2)

			// Add three days
			Calendar cal4 = Calendar.getInstance()
			cal4.setTime(date)
			cal4.add(Calendar.DAY_OF_MONTH, 3)

			// Add 15 days
			Calendar cal5 = Calendar.getInstance()
			cal5.setTime(date)
			cal5.add(Calendar.DAY_OF_MONTH, 16)

			// Add 1 month
			Calendar cal6 = Calendar.getInstance()
			cal6.setTime(date)
			cal6.add(Calendar.MONTH, 1)

			// Advance 1 day
			long diff = getDiffInHours(cal2, cal1);
			isSuccessful = advanceOffsetHours(clientNo, diff);
			Thread.sleep(2000)

			// Advance to 2nd day
			Thread.sleep(2000)
			diff = getDiffInHours(cal3, cal2);
			isSuccessful = advanceOffsetHours(clientNo, diff);

			// Advance to 3rd day
			Thread.sleep(2000)
			diff = getDiffInHours(cal4, cal3);
			isSuccessful = advanceOffsetHours(clientNo, diff);

			// Advance to 15th day
			Thread.sleep(2000)
			diff = getDiffInHours(cal5, cal4);
			isSuccessful = advanceOffsetHours(clientNo, diff);

			// Advance to 1 month
			Thread.sleep(2000)
			diff = getDiffInHours(cal6, cal5);
			isSuccessful = advanceOffsetHours(clientNo, diff);
		}
		else if (Float.parseFloat(lowestValue).toString().equals("0.15") || !Common.isInteger(lowestValue) || !Common.isInteger(highestValue)) {

			for(int i=0 ; i< Float.parseFloat(highestValue) ; i++) {
				if(i == 0) {

					String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);

					// Stores the current virtual time in the specified format
					Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
					Calendar cal1 = Calendar.getInstance();
					cal1.setTime(date);

					// Add one day
					Calendar cal2 = Calendar.getInstance()
					cal2.setTime(date)
					cal2.add(Calendar.DAY_OF_MONTH, 1)

					// Add two days
					Calendar cal3 = Calendar.getInstance()
					cal3.setTime(date)
					cal3.add(Calendar.DAY_OF_MONTH, 2)

					// Add three days
					Calendar cal4 = Calendar.getInstance()
					cal4.setTime(date)
					cal4.add(Calendar.DAY_OF_MONTH, 3)

					// Add 15 days
					Calendar cal5 = Calendar.getInstance()
					cal5.setTime(date)
					cal5.add(Calendar.DAY_OF_MONTH, 16)

					// Add 1 month
					Calendar cal6 = Calendar.getInstance()
					cal6.setTime(date)
					cal6.add(Calendar.MONTH, 1)

					// Advance 1 day
					long diff = getDiffInHours(cal2, cal1);
					isSuccessful = advanceOffsetHours(clientNo, diff);
					Thread.sleep(2000)

					// Advance to 2nd day
					Thread.sleep(2000)
					diff = getDiffInHours(cal3, cal2);
					isSuccessful = advanceOffsetHours(clientNo, diff);

					// Advance to 3rd day
					Thread.sleep(2000)
					diff = getDiffInHours(cal4, cal3);
					isSuccessful = advanceOffsetHours(clientNo, diff);

					// Advance to 15th day
					Thread.sleep(2000)
					diff = getDiffInHours(cal5, cal4);
					isSuccessful = advanceOffsetHours(clientNo, diff);

					// Advance to 1 day before anniversary
					Thread.sleep(2000)
					diff = getDiffInHours(cal6, cal5);
					isSuccessful = advanceOffsetHours(clientNo, diff - 24);

					// Advance to anniversary
					Thread.sleep(2000)
					isSuccessful = advanceOffsetHours(clientNo, 24);

					// Advance to one day after anniversary
					Thread.sleep(2000)
					isSuccessful = advanceOffsetHours(clientNo, 24);

				} else {

					String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);
					// Stores the current virtual time in the specified format
					Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
					Calendar cal1 = Calendar.getInstance();
					cal1.setTime(date);

					// Add one month
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(date);
					cal2.add(Calendar.MONTH, 1);

					long diff = getDiffInHours(cal2, cal1);
					if(i < 3) {
						if(i==0) {
							isSuccessful = advanceOffsetHours(clientNo, diff-24);
						} else {
							isSuccessful = advanceOffsetHours(clientNo, diff-48);
						}
						Thread.sleep(1000)
						isSuccessful = advanceOffsetHours(clientNo, 24);
						Thread.sleep(1000)
						isSuccessful = advanceOffsetHours(clientNo, 24);
						Thread.sleep(1000)
					} else if(i==4) {
						isSuccessful = advanceOffsetHours(clientNo, diff-24);
					} else {
						isSuccessful = advanceOffsetHours(clientNo, diff);
					}
				}
			}
		} else if (Common.isInteger(lowestValue) && Common.isInteger(highestValue)) {
			for(int i=0 ; i< Integer.parseInt(highestValue) ; i++ ) {
				String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);
				// Stores the current virtual time in the specified format
				Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(date);

				// Add on month
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(date);
				cal2.add(Calendar.MONTH, 1);

				long diff = getDiffInHours(cal2, cal1);

				if(i < 3) {
					if(i==0) {
						isSuccessful = advanceOffsetHours(clientNo, diff-24);
					} else {
						isSuccessful = advanceOffsetHours(clientNo, diff-48);
					}
					Thread.sleep(1000)
					isSuccessful = advanceOffsetHours(clientNo, 24);
					Thread.sleep(1000)
					isSuccessful = advanceOffsetHours(clientNo, 24);
					Thread.sleep(1000)
				} else if(i==4) {
					isSuccessful = advanceOffsetHours(clientNo, diff-24);
				} else {
					isSuccessful = advanceOffsetHours(clientNo, diff);
				}
			}
		}
		//db.closeConnection();
		return isSuccessful;
	}

	public boolean advanceVirtualTimeASP(String clientNo, String duration) throws SQLException, ParseException {

		boolean isSuccessful = false;
		db = new ConnectDB();

		String[] spDuration = duration.split(":");
		String lowestValue = spDuration[0];
		String highestValue = spDuration[1];

		/*batchLogFile.logInfo("-----------------------------------------------------")
		 batchLogFile.logInfo("Virtual Time Advancement for Client No : " + clientNo)
		 batchLogFile.logInfo("-----------------------------------------------------")*/

		if(Float.parseFloat(lowestValue).toString().equals("0.15") &&  Float.parseFloat(highestValue).toString().equals("0.0")) {

			String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);
			// Stores the current virtual time in the specified format
			Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(date);

			// Add one day
			Calendar cal2 = Calendar.getInstance()
			cal2.setTime(date)
			cal2.add(Calendar.DAY_OF_MONTH, 1)

			// Add two days
			Calendar cal3 = Calendar.getInstance()
			cal3.setTime(date)
			cal3.add(Calendar.DAY_OF_MONTH, 2)

			// Add three days
			Calendar cal4 = Calendar.getInstance()
			cal4.setTime(date)
			cal4.add(Calendar.DAY_OF_MONTH, 3)

			// Add 15 days
			Calendar cal5 = Calendar.getInstance()
			cal5.setTime(date)
			cal5.add(Calendar.DAY_OF_MONTH, 16)

			// Add 1 month
			Calendar cal6 = Calendar.getInstance()
			cal6.setTime(date)
			cal6.add(Calendar.MONTH, 1)

			// Advance 1 day
			long diff = getDiffInHours(cal2, cal1);
			isSuccessful = advanceOffsetHours(clientNo, diff);
			Thread.sleep(2000)

			// Advance to 2nd day
			Thread.sleep(2000)
			diff = getDiffInHours(cal3, cal2);
			isSuccessful = advanceOffsetHours(clientNo, diff);

			// Advance to 3rd day
			Thread.sleep(2000)
			diff = getDiffInHours(cal4, cal3);
			isSuccessful = advanceOffsetHours(clientNo, diff);

			// Advance to 15th day
			Thread.sleep(2000)
			diff = getDiffInHours(cal5, cal4);
			isSuccessful = advanceOffsetHours(clientNo, diff);

			// Advance to 1 month
			Thread.sleep(2000)
			diff = getDiffInHours(cal6, cal5);
			isSuccessful = advanceOffsetHours(clientNo, diff);
		}
		else if (Float.parseFloat(lowestValue).toString().equals("0.15") || !Common.isInteger(lowestValue) || !Common.isInteger(highestValue)) {

			for(int i=0 ; i< Float.parseFloat(highestValue) ; i++) {
				if(i == 0) {

					String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);

					// Stores the current virtual time in the specified format
					Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
					Calendar cal1 = Calendar.getInstance();
					cal1.setTime(date);

					// Add one day
					Calendar cal2 = Calendar.getInstance()
					cal2.setTime(date)
					cal2.add(Calendar.DAY_OF_MONTH, 1)

					// Add two days
					Calendar cal3 = Calendar.getInstance()
					cal3.setTime(date)
					cal3.add(Calendar.DAY_OF_MONTH, 2)

					// Add three days
					Calendar cal4 = Calendar.getInstance()
					cal4.setTime(date)
					cal4.add(Calendar.DAY_OF_MONTH, 3)

					// Add 15 days
					Calendar cal5 = Calendar.getInstance()
					cal5.setTime(date)
					cal5.add(Calendar.DAY_OF_MONTH, 16)

					// Add 1 month
					Calendar cal6 = Calendar.getInstance()
					cal6.setTime(date)
					cal6.add(Calendar.MONTH, 1)

					// Advance 1 day
					long diff = getDiffInHours(cal2, cal1);
					isSuccessful = advanceOffsetHours(clientNo, diff);
					Thread.sleep(2000)

					// Advance to 2nd day
					Thread.sleep(2000)
					diff = getDiffInHours(cal3, cal2);
					isSuccessful = advanceOffsetHours(clientNo, diff);

					// Advance to 3rd day
					Thread.sleep(2000)
					diff = getDiffInHours(cal4, cal3);
					isSuccessful = advanceOffsetHours(clientNo, diff);

					// Advance to 15th day
					Thread.sleep(2000)
					diff = getDiffInHours(cal5, cal4);
					isSuccessful = advanceOffsetHours(clientNo, diff);

					// Advance to 1 day before anniversary
					Thread.sleep(2000)
					diff = getDiffInHours(cal6, cal5);
					isSuccessful = advanceOffsetHours(clientNo, diff - 24);

					// Advance to anniversary
					Thread.sleep(2000)
					isSuccessful = advanceOffsetHours(clientNo, 24);

					// Advance to one day after anniversary
					Thread.sleep(2000)
					isSuccessful = advanceOffsetHours(clientNo, 24);

				} else {

					String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);
					// Stores the current virtual time in the specified format
					Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
					Calendar cal1 = Calendar.getInstance();
					cal1.setTime(date);

					// Add one month
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(date);
					cal2.add(Calendar.MONTH, 1);

					long diff = getDiffInHours(cal2, cal1);
					if(i < 3) {
						if(i==0) {
							isSuccessful = advanceOffsetHours(clientNo, diff-24);
						} else {
							isSuccessful = advanceOffsetHours(clientNo, diff-48);
						}
						Thread.sleep(1000)
						isSuccessful = advanceOffsetHours(clientNo, 24);
						Thread.sleep(1000)
						isSuccessful = advanceOffsetHours(clientNo, 24);
						Thread.sleep(1000)
					} else if(i==4) {
						isSuccessful = advanceOffsetHours(clientNo, diff-24);
					} else {
						isSuccessful = advanceOffsetHours(clientNo, diff);
					}
				}
			}
		} else if (Common.isInteger(lowestValue) && Common.isInteger(highestValue)) {
			for(int i=0 ; i< Integer.parseInt(highestValue) ; i++ ) {
				String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);
				// Stores the current virtual time in the specified format
				Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(date);

				// Add on month
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(date);
				cal2.add(Calendar.MONTH, 1);

				long diff = getDiffInHours(cal2, cal1);
				isSuccessful = advanceOffsetHours(clientNo, diff);
			}
		}
		//db.closeConnection();
		return isSuccessful;
	}

	public long getDiffInHours(Calendar cal1, Calendar cal2) {

		long diff = cal1.getTimeInMillis() - cal2.getTimeInMillis();
		// Calculate difference in hours
		long diffHours = diff / (60 * 60 * 1000);

		return diffHours;
	}

	public long getDiffInHours(Date startTime, Date endTime) {

		//		Date date1 = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).parse(startTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(startTime);

		//		Date date2 = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).parse(endTime);
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(endTime);

		long diff = cal1.getTimeInMillis() - cal2.getTimeInMillis();
		// Calculate difference in hours
		long diffHours = diff / (60 * 60 * 1000);

		return diffHours;
	}

	public String getCurrentVirtualTimeInHours(String clientNo) throws SQLException {
		ConnectDB db = new ConnectDB();
		// Get the current virtual time for the client
		ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL");

		String currentVirtualTime = null;
		if(rs.next()) {
			currentVirtualTime = rs.getString(1);
		}
		rs.close()
		return currentVirtualTime;
	}

	public String getCurrentVirtualTimeInAPIFormat(String clientNo) throws SQLException {

		ConnectDB db = new ConnectDB()
		// Get the current virtual time for the client
		ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL");

		String currentVirtualTime = null;
		if(rs.next()) {
			currentVirtualTime = rs.getString(1);
		}

		// Stores the current virtual time in the specified format
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String date1 = format1.format(date);
		//db.closeConnection()
		return date1;
	}

	public String getPastDate(String clientNo) throws SQLException {

		ConnectDB db = new ConnectDB()
		// Get the current virtual time for the client
		ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL");

		String currentVirtualTime = null;
		if(rs.next()) {
			currentVirtualTime = rs.getString(1);
		}

		// Stores the current virtual time in the specified format
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);

		cal1.add(Calendar.MONTH, -2);
		cal1.add(Calendar.DAY_OF_MONTH, -15);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String date1 = format1.format(cal1.getTime());
		return date1;
	}

	public boolean advanceOffsetHours(String clientNo, long additionalHours) throws SQLException {

		boolean isSuccessful = false;
		ConnectDB db = new ConnectDB();
		// Get the offset hours
		ResultSet rs = db.executePlaQuery("SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='VIRTUAL_TIME_OFFSET_HOURS' AND CLIENT_NO=" + clientNo);

		long currentOffsetHours = 0 ;
		if(rs.next()) {
			currentOffsetHours = Long.parseLong(rs.getString(1));
		}
		rs.close()
		long advanceOffsetHours = currentOffsetHours + additionalHours;

		// Update the offset hours to advance the virtual time
		rs = db.executePlaQuery("UPDATE ARIACORE.ARIA_CLIENT_PARAMS SET PARAM_VAL = "+ advanceOffsetHours + " WHERE PARAM_NAME='VIRTUAL_TIME_OFFSET_HOURS' AND CLIENT_NO=" + clientNo);		
		rs.close()
		
		db.connection.commit();
		// Get the current db value
		rs = db.executePlaQuery("SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='VIRTUAL_TIME_OFFSET_HOURS' AND CLIENT_NO=" + clientNo);
		if(rs.next()) {
			if(advanceOffsetHours == Long.parseLong(rs.getString(1))) {
				isSuccessful = true;
				//Thread.sleep(10000);
				Thread.sleep(10);
				logi.logInfo "Running the Batch Update..."
				String status = batch(clientNo);
				logi.logInfo "Batch Update Status: " + status
			}
		}
		rs.close()
		return isSuccessful;
	}

	public boolean advanceOffsetHoursToCurrentTime(String clientNo) throws SQLException {
		ConnectDB db = new ConnectDB()
		long advanceOffsetHours=0
				boolean isSuccessful = false;		
				// Update the offset hours to advance the virtual time
				ResultSet rs = db.executePlaQuery("UPDATE ARIACORE.ARIA_CLIENT_PARAMS SET PARAM_VAL =0  WHERE PARAM_NAME='VIRTUAL_TIME_OFFSET_HOURS' AND CLIENT_NO=" + clientNo);
				rs.close()
				
				db.connection.commit();
				// Get the current db value
				rs = db.executePlaQuery("SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='VIRTUAL_TIME_OFFSET_HOURS' AND CLIENT_NO=" + clientNo);
				if(rs.next()) {
					if(advanceOffsetHours == Long.parseLong(rs.getString(1))) {
						isSuccessful = true;
						//Thread.sleep(10000);
						Thread.sleep(10);
						logi.logInfo "Running the Batch Update..."
						String status = batch(clientNo);
						logi.logInfo "Batch Update Status: " + status
					}
				}
				rs.close()
				return isSuccessful;
			}
	
	String batch(String strClientNo)
	{
		ArrayList<String> sps = new ArrayList<String>();
		sps.add(Constant.BATCH_SP1);
		sps.add(Constant.BATCH_SP2);
		sps.add(Constant.BATCH_SP3);
		sps.add(Constant.BATCH_SP4);
		//		sps.add(Constant.BATCH_SP5);
		sps.add(Constant.BATCH_SP6);
		sps.add(Constant.BATCH_SP7);
		sps.add(Constant.BATCH_SP8);
		sps.add(Constant.BATCH_SP9);

		String status = "success"
		db.closeConnection()
		db = new ConnectDB();		
		
		
		for(int i=0; i<sps.size() && status=="success"; i++) {
			logi.logInfo "Started batch job execution for " + sps[i] + " at " + Common.getCurrentTime()
			 logi.logInfo "Start Time " + Common.getCurrentTime()
			
			status = db.executeSP(sps[i], strClientNo)
			logi.logInfo "End Time " + Common.getCurrentTime()
		}
		return status
	}

	public boolean advanceVirtualTimeByOneMonth(String clientNo) throws SQLException, ParseException {

		boolean isSuccessful = false;
		isSuccessful = advanceVirtualTime(clientNo, true);

		return isSuccessful;
	}

	public boolean advanceVirtualTimeBy15Days(String clientNo) throws SQLException, ParseException {

		boolean isSuccessful = false;
		isSuccessful = advanceVirtualTime(clientNo, false);

		return isSuccessful;
	}

	public boolean advanceVirtualTime(String clientNo, boolean isMonth) throws SQLException, ParseException {

		boolean isSuccessful = false;
		// Get the current virtual time for the client
		ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL");

		String currentVirtualTime = null;
		if(rs.next()) {
			currentVirtualTime = rs.getString(1);
		}

		// Stores the current virtual time in the specified format
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);

		// Advance the time by one month and store
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date);
		if(isMonth) {
			cal2.add(Calendar.MONTH, 1);
		} else {
			cal2.add(Calendar.DATE, 15);
		}

		// Get the difference in hours
		long milis1 = cal1.getTimeInMillis();
		long milis2 = cal2.getTimeInMillis();

		// Calculate difference in milliseconds
		long diff = milis2 - milis1;

		// Calculate difference in hours
		long diffHours = diff / (60 * 60 * 1000);

		// Get the offset hours
		rs = db.executePlaQuery("SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='VIRTUAL_TIME_OFFSET_HOURS' AND CLIENT_NO=" + clientNo);

		long advOffsetHours = 0 ;
		if(rs.next()) {
			advOffsetHours = Long.parseLong(rs.getString(1));
		}

		long setOffsetHours = diffHours + advOffsetHours;

		// Update the offset hours to advance the virtual time
		rs = db.executePlaQuery("UPDATE ARIACORE.ARIA_CLIENT_PARAMS SET PARAM_VAL = "+ setOffsetHours + " WHERE PARAM_NAME='VIRTUAL_TIME_OFFSET_HOURS' AND CLIENT_NO=" + clientNo);

		// Get the current db value
		rs = db.executePlaQuery("SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='VIRTUAL_TIME_OFFSET_HOURS' AND CLIENT_NO=" + clientNo);
		if(rs.next()) {
			if(setOffsetHours == rs.getShort(1)) {
				isSuccessful = true;
			}
		}
		return isSuccessful;
	}

	public String getPastAndFutureDate(String clientNo, String numberOfDays)
	{
		db = new ConnectDB();
		Date date = new Date();
		Calendar now = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL";
		ResultSet resultSet = db.executeQuery(Query);
		while(resultSet.next()){
			// Get only the current Date

			date = resultSet.getDate(1);
		}


		now.setTime(date);
		now.add(Calendar.DATE, Integer.parseInt(numberOfDays));

		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String date1 = format1.format(now.getTime());
		//db.closeConnection()

		return date1;
	}

	public String getPastAndFutureDayOfMonth(String clientNo, String numberOfDays)
	{
		db = new ConnectDB();
		Date date = new Date();
		Calendar now = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL";
		ResultSet resultSet = db.executeQuery(Query);
		while(resultSet.next()){
			// Get only the current Date

			date = resultSet.getDate(1);
		}


		now.setTime(date);
		now.add(Calendar.DATE, Integer.parseInt(numberOfDays));

		SimpleDateFormat format1 = new SimpleDateFormat("dd");
		String date1 = format1.format(now.getTime());
		//db.closeConnection()

		return date1;

	}
	
	public String getPastAndFutureDayP2(String clientNo, String numberOfDays, String dateMonthFormat)
	{
		db = new ConnectDB();
		Date date = new Date();
		Calendar now = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL";
		ResultSet resultSet = db.executeQuery(Query);
		while(resultSet.next()){
			// Get only the current Date

			date = resultSet.getDate(1);
		}
		now.setTime(date);
		if(numberOfDays.contains("-")){
			if(numberOfDays.contains(".")) {
				String[] days = numberOfDays.split("\\.");
				now.add(Calendar.DATE, -(Integer.parseInt(days[1])));
			}
			else
				now.add(Calendar.DATE,Integer.parseInt(numberOfDays));
		}
		else{
			if(numberOfDays.contains(".")) {
				String[] days = numberOfDays.split("\\.");
				now.add(Calendar.DATE, Integer.parseInt(days[1]));
			}
			else
				now.add(Calendar.DATE, Integer.parseInt(numberOfDays));
		}

		SimpleDateFormat format1 = new SimpleDateFormat(dateMonthFormat);
		String date1 = format1.format(now.getTime());
		//db.closeConnection()
		
		return date1;
	}

	public String getCurrentVirtualTime(String clientNo) throws SQLException {

		db = new ConnectDB()
		// Get the current virtual time for the client
		ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL");

		String currentVirtualTime = null;
		if(rs.next()) {
			currentVirtualTime = rs.getString(1);
		}
		//db.closeConnection();
		return currentVirtualTime;
	}

	public String getDifferencefromVTtoAT() throws SQLException {
		db = new ConnectDB()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		// Get the current virtual time for the client
		ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL");

		Date virtualDate ;
		if(rs.next()) {
			virtualDate = rs.getDate(1);
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(virtualDate);
		int month =  cal.get(Calendar.MONTH)
		cal.set(Calendar.MONTH,month+1)
		cal.set(Calendar.DATE,1)

		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		String date = format1.format(cal.getTime());

		Date advanceDate = format1.parse(date)
		cal.setTime(virtualDate);
		date = format1.format(cal.getTime());

		virtualDate = format1.parse(date)
		return (int) ((advanceDate.getTime() - virtualDate.getTime()) / (1000 * 60 * 60 * 24)+2);
	}

	boolean storeVirtualTime(List clientNo) {	

		for(int i=0 ; i<clientNo.size ; i++) {
			Constant.lhmCurrentVirtualTime.put clientNo.get(i), getCurrentVirtualTime(clientNo.get(i))
		}
	}

	public String storeAndUpdateTaxToInvalidURL(String clientNo) {

		db = new ConnectDB()

		ResultSet rs = db.executePlaQuery("SELECT CONFIG_VALUE FROM ARIACORE.CLIENT_TAX_CONFIG_VALUES WHERE CLIENT_NO=" + clientNo + " AND CONFIG_NAME LIKE '%URL'")

		if(rs.next()) {
			tmpTaxUrl = rs.getString(1)
		}

		String query = "UPDATE ARIACORE.CLIENT_TAX_CONFIG_VALUES SET CONFIG_VALUE='" + tmpTaxUrl + "invalid' WHERE CLIENT_NO=" + clientNo + " AND CONFIG_NAME LIKE '%URL'"
		// Update the tax with invalid url
		rs = db.executePlaQuery(query)

		if(rs.next()) {
		}

		return query
	}

	public String revertTaxUrl(String clientNo) {

		db = new ConnectDB()

		String query = "UPDATE ARIACORE.CLIENT_TAX_CONFIG_VALUES SET CONFIG_VALUE='" + tmpTaxUrl + "' WHERE CLIENT_NO=" + clientNo + " AND CONFIG_NAME LIKE '%URL'"
		// Update the tax with invalid url
		ResultSet rs = db.executePlaQuery(query)

		if(rs.next()) {
		}

		return query

	}
	public String getPastAndFutureMonthandDayP2(String clientNo, String numberOfDays, String dateMonthFormat)
	{
		db = new ConnectDB();
		Date date = new Date();
		Calendar now = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL";
		ResultSet resultSet = db.executeQuery(Query);
		while(resultSet.next()){
			// Get only the current Date

			date = resultSet.getDate(1);
		}
		now.setTime(date);
		if(numberOfDays.contains("-")){
			if(numberOfDays.contains(".")) {
				String[] days = numberOfDays.split("\\.");
				now.add(Calendar.MONTH, Integer.parseInt(days[0]));
				now.add(Calendar.DATE, -(Integer.parseInt(days[1])));
			}
			else
				now.add(Calendar.DATE,Integer.parseInt(numberOfDays));
		}
		else{
			if(numberOfDays.contains(".")) {
				String[] days = numberOfDays.split("\\.");
				now.add(Calendar.MONTH, Integer.parseInt(days[0]));
				now.add(Calendar.DATE, Integer.parseInt(days[1]));
			}
			else
				now.add(Calendar.DATE, Integer.parseInt(numberOfDays));
		}

		SimpleDateFormat format1 = new SimpleDateFormat(dateMonthFormat);
		String date1 = format1.format(now.getTime());
		//db.closeConnection()
		return date1;
	}


	public String getPastOrFutureMonthandDayFromDateGievnP2(String acctCreatedDate, String numberOfDays, String dateMonthFormat)
	{
		logi.logInfo('inside getPastOrFutureMonthandDayFromDateGievnP2')
		db = new ConnectDB();
		Date date = new Date(acctCreatedDate);
		Calendar now = Calendar.getInstance();
	//	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
		now.setTime(date);

		logi.logInfo('Date In  :'+date)
		if(numberOfDays.contains("-")){
			if(numberOfDays.contains(".")) {
				String[] days = numberOfDays.split("\\.");
				now.add(Calendar.MONTH, Integer.parseInt(days[0]));
				now.add(Calendar.DATE, -(Integer.parseInt(days[1])));
			}
			else
				now.add(Calendar.DATE,Integer.parseInt(numberOfDays));
		}
		else{
		if(numberOfDays.contains(".")) {
			String[] days = numberOfDays.split("\\.");
			now.add(Calendar.MONTH, Integer.parseInt(days[0]));
			now.add(Calendar.DATE, Integer.parseInt(days[1]));
		}
		else
			now.add(Calendar.DATE, Integer.parseInt(numberOfDays));
		}

		SimpleDateFormat format1 = new SimpleDateFormat(dateMonthFormat);
		String date1 = format1.format(now.getTime());
		logi.logInfo('Date return :'+date1)
		//db.closeConnection()

		return date1;
	}

	public String bulk_record_batch(String clientNo) throws SQLException
	{
		String status = db.executeSP("{ call ariacore.usageTools.bulk_usage_rating(?) }", clientNo)
		return status

	}
	
	public String statement_createall_batch(String clientNo) throws SQLException
	{
		String status = db.executeSP("{ call ariacore.statementmgr.create_all(?) }", clientNo)
		return status

	}
	
	public String taxes_process_taxes(String clientNo) throws SQLException
	{
	String status=db.executeSP("{ call ariacore.taxes.process_taxes(?) }", clientNo)
	logi.logInfo('batch run after issue refund'+status)
	return status
	}
	
	public String events_batch(String clientNo) throws SQLException
	{
		String status = db.executeSP("{ call ARIACORE.PROVMGR.force_paid_invs_to_prov(?) }", clientNo)
		logi.logInfo('batch run after creating order'+status)
		return status

	}

	
	public String getCurrentTimeForBulkRecord()
	{
		Date now = new Date();
		TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles"); // For example...
		DateFormat format = new SimpleDateFormat("dd-MMM-yy"); // Put your pattern here
		format.setTimeZone(zone);
		String text = format.format(now);
		
		return text.toUpperCase()
	}
	
	public String getPastOrFutureVTYear(String clientNo, String numberOfYears, String yearFormat)
	{
		logi.logInfo('inside getPastOrFutureVTYear')
		ConnectDB db = new ConnectDB();
		Date date = new Date();
		Calendar now = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL";
		ResultSet resultSet = db.executeQuery(Query);
		while(resultSet.next()){			
			date = resultSet.getDate(1);
		}
		now.setTime(date);		
		now.add(Calendar.YEAR, Integer.parseInt(numberOfYears));
		SimpleDateFormat format1 = new SimpleDateFormat(yearFormat);
		String year = format1.format(now.getTime());
		logi.logInfo('Date return :'+year)
		return year;
	}
	
	/**
	 * To Get the Coupon Date
	 * @param Number of Days, Format
	 * @return Date
    */
    public String getDateforCoupon(String numberOfDays, String dateMonthFormat)
    {
	 Date date = new Date();
	 Calendar now = Calendar.getInstance();
	 TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles"); // For example...
	 DateFormat format = new SimpleDateFormat("dd-MMM-yy"); // Put your pattern here
	 format.setTimeZone(zone);
	 
	 //date  = format.format(now);
									 
	 now.setTime(date);
	 logi.logInfo('Date In  :'+date)
	 
	 if(numberOfDays.contains("-"))
	 {
					 if(numberOfDays.contains("."))
					 {
									 String[] days = numberOfDays.split("\\.");
									 now.add(Calendar.MONTH, Integer.parseInt(days[0]));
									 now.add(Calendar.DATE, -(Integer.parseInt(days[1])));
					 }
					 else
									 now.add(Calendar.DATE,Integer.parseInt(numberOfDays));
	 }
	 else
	 {
					 if(numberOfDays.contains("."))
					 {
									 String[] days = numberOfDays.split("\\.");
									 now.add(Calendar.MONTH, Integer.parseInt(days[0]));
									 now.add(Calendar.DATE, Integer.parseInt(days[1]));
					 }
					 else
									 now.add(Calendar.DATE, Integer.parseInt(numberOfDays));
	 }

	 SimpleDateFormat format1 = new SimpleDateFormat(dateMonthFormat);
	 String date1 = format1.format(now.getTime());
	 //db.closeConnection()

	 logi.logInfo('Date In2 :'+date1)
	 return date1;
    }

	public String getAlreadyexistUserID(String clientNo)
	{
		db = new ConnectDB();
		String userid=null
		String Query = "select USERID from ariacore.acct where client_no in(" + clientNo + ") order by acct_no asc";
		ResultSet resultSet = db.executeQuery(Query);
		if(resultSet.next()){
			// Get first user id from the result set

			userid = resultSet.getString(1);
		}
		return userid;
	}

	public boolean advanceVirtualTimeToBillLagDays(String clientNo, String accountNo) {

		boolean isSuccessful = false;
		db = new ConnectDB();
		String nextBillDate = null;
		String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);
		// Stores the current virtual time in the specified format
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		String Query = "select NEXT_BILL_DATE from ariacore.acct where acct_no=" + accountNo;
		ResultSet resultSet = db.executeQuery(Query);
		if(resultSet.next()){
			// Get first user id from the result set

			nextBillDate = resultSet.getString(1);
		}
		Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(nextBillDate);
		// Add 15 days
		Calendar cal3 = Calendar.getInstance();
		cal3.setTime(date1);
		cal3.add(Calendar.DAY_OF_MONTH, -15);

		long diff = getDiffInHours(cal3, cal1);
		isSuccessful = advanceOffsetHours(clientNo, diff);
	}
public boolean advanceVirtualTimeToAnniversary2(String clientNo) {
		
		boolean isSuccessful = false;
		db = new ConnectDB();
		
		String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);
		// Stores the current virtual time in the specified format
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date);
		cal2.setTime(cal1.getTime());
		cal2.add(Calendar.DAY_OF_MONTH, 15)
		
		
		
		
		long diff = getDiffInHours(cal2, cal1);
		isSuccessful = advanceOffsetHours(clientNo, diff);
   }
	
	
	public boolean advanceVirtualTimeToAnniversaryMinus15days(String clientNo) {
		
		boolean isSuccessful = false;
		db = new ConnectDB();
		
		String currentVirtualTime = getCurrentVirtualTimeInHours(clientNo);
		// Stores the current virtual time in the specified format
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		int daysInMonth = cal1.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		cal1.add(Calendar.MONTH, 1)
		
		//batchLogFile.logInfo("-----------------------------------------------------")
		//batchLogFile.logInfo("Virtual Time Advancement for Client No : " + clientNo)
		//batchLogFile.logInfo("-----------------------------------------------------")
		
		// Add 15 days
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(cal1.getTime());
			if(daysInMonth>30)
			cal2.add(Calendar.DAY_OF_MONTH, -16);
			else if(daysInMonth==28)
			cal2.add(Calendar.DAY_OF_MONTH, -13);
			else if(daysInMonth==29)
			cal2.add(Calendar.DAY_OF_MONTH, -14);
			else
			cal2.add(Calendar.DAY_OF_MONTH, -15);
			
		
	/*	// Add 1 month
		Calendar cal3 = Calendar.getInstance();
		cal3.setTime(cal1.getTime());
		cal3.add(Calendar.MONTH, 1);*/
		
		long diff = getDiffInHours(cal1, cal2);
		isSuccessful = advanceOffsetHours(clientNo, diff);
   }
	
	public String run_validatecontracts_batch_dev3878(String clientNo) throws SQLException
	{
		String status = db.executeSP("{ call ariacore.contractmgr.validate_contracts(?) }", clientNo)
		return status
	}
	
	public AddNode(String filepath,String pnode, String i, String nn, String nv)
	{
		//The filepath is the path of the xml file
		//pnode is the parent node name
		//i is the index that indicates the position of the parent node when there is more than one of the same name
		//nn is the name of the new node
		//nv is the value given to the new node
		
		
		logi.logInfo "Xml doing working"
			 try
			 {		 int index = i.toInteger()
					 //since we are handling arrayList it always starts form 0 and in the excel sheet we always start to mention form 1
					 index-=1
				DocumentBuilderFactory docFactory =	DocumentBuilderFactory.newInstance()
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder()
				Document doc = docBuilder.parse(filepath)
				//The node to handle the parent node where the index is the occurence of the node
				Node node = doc.getElementsByTagName(pnode).item(index);
				Element ariaacctid = doc.createElement(nn);
				ariaacctid.appendChild(doc.createTextNode(nv));
				node.appendChild(ariaacctid);
				TransformerFactory transformerFactory =	TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(filepath));
				//transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount","2");
				transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,"yes");
				transformer.setOutputProperty(OutputKeys.METHOD, "xml");
				transformer.transform(source, result);
				logi.logInfo "Xml doing working completed"
				System.out.println("Done");
			
			}catch(Exception e){
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			println sw.toString();
		}
	}
	//The above bracket ends the AddNode function
	
	//The module to change the version of the XML file
	public VerChange(String filepath, String ver)
	{
		
		
		try{
			logi.logInfo "Version change started "+ver
			
				DocumentBuilderFactory docFactory =	DocumentBuilderFactory.newInstance()
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder()
				Document doc = docBuilder.parse(filepath)
				Node node = doc.getElementsByTagName("soapenv:Envelope").item(0);
				logi.logInfo "node created"
				
						NamedNodeMap attribute = node.getAttributes();
						Node nodeAttr;
						String key;
						
						for (int i = 0; i < attribute.getLength(); i++) 
						{

			            Attr attr = (Attr) attribute.item(i);
						String attrName = attr.getNodeName();
			            String attrValue = attr.getNodeValue();
						System.out.println("Found attribute: " + attrName + " with value: " + attrValue);

						
						nodeAttr = attribute.getNamedItem(attrName);
						key = nodeAttr.getNodeValue();
						if(key.contains("vers"))
						{		int index=key.indexOf("vers:");
								index+=5;
								String key2=key.substring(0, index);
								String key3=key.substring(key.indexOf(":aria_"),(key.length()));
								nodeAttr.setTextContent(key2+ver+key3);
						}
						attrName = attr.getNodeName();
			            attrValue = attr.getNodeValue();
						System.out.println("Found attribute: " + attrName + " with value: " + attrValue);

			             }
						
						node = doc.getElementsByTagName("soapenv:Body").item(0);
						NodeList nList=node.getChildNodes();
						node = nList.item(1);
						node = doc.getElementsByTagName(node.getNodeName()).item(0);
						attribute = node.getAttributes();
						
						for (int i = 0; i < attribute.getLength(); i++) 
						{

			            Attr attr = (Attr) attribute.item(i);
						String attrName = attr.getNodeName();
			            String attrValue = attr.getNodeValue();
						System.out.println("Found attribute: " + attrName + " with value: " + attrValue);

						
						nodeAttr = attribute.getNamedItem(attrName);
						key = nodeAttr.getNodeValue();
						if(key.contains("vers"))
						{	int index=key.indexOf("vers:");
							System.out.println(key);
							index+=5;
							String key2=key.substring(0, index);
							String key3=key.substring(key.indexOf(":aria_"),(key.length()));
							nodeAttr.setTextContent(key2+ver+key3);
						}
						attrName = attr.getNodeName();
			            attrValue = attr.getNodeValue();
						System.out.println("Found attribute: " + attrName + " with value: " + attrValue);

						
			             }
							
							
				TransformerFactory transformerFactory =	TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(filepath));
				//transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount","2");
				transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
				transformer.setOutputProperty(OutputKeys.INDENT, "no");
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,"yes");
				transformer.setOutputProperty(OutputKeys.METHOD, "xml");
				transformer.transform(source, result);
				logi.logInfo "version change working completed..."
				System.out.println("Done");
		}catch(Exception e){
				
			logi.logInfo "version change working throwed..."
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			println sw.toString();
		}
	}
	
	public void changeval (String input,String filepath) throws Exception
	{
		
		try{
		String true_input=input;
		String  [] inp=input.split("#")
		int count=0;
		for (int i = 0; i < input.length(); i++)
		{
			  if (input.charAt(i) == '#')
			  {
					 count++;
			  }
		}
		//store the target node as input value
		input=	inp[count]
		
		
		logi.logInfo "into the changeval"+input
		String name1=input.split(",")[0];
		int index=Integer.parseInt(name1.split("\\|")[1]);
		index-=1;
		name1=name1.split("\\|")[0];
		String value=input.split(",")[1];
		
		DocumentBuilderFactory docFactory =	DocumentBuilderFactory.newInstance()
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder()
		Document doc = docBuilder.parse(filepath)
		logi.logInfo "Setting boundary"
		//Boundary
		NodeList nodes = doc.getElementsByTagName(doc.getDocumentElement().getNodeName());
		Element element = (Element) nodes.item(0);
		if(count>0)
		{
			for(int t=0;t<count;t++)
			{
				logi.logInfo "Setting boundary "+ true_input.split('#')[t]
				nodes = element.getElementsByTagName(true_input.split('#')[t].split('\\|')[0]);
				element = (Element) nodes.item(((Integer.parseInt(true_input.split('#')[t].split('\\|')[1]))-1));
			}
		}
		//Target
		NodeList names = element.getElementsByTagName(name1);
		Node name = names.item(index);
		name.setTextContent(value);
		
		
		//StringWriter writer = new StringWriter();
		//StreamResult result = new StreamResult(writer);
		StreamResult result = new StreamResult(new File(filepath));
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		DOMSource domSource = new DOMSource(doc);
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.INDENT, "no");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,"yes");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.transform(domSource, result);
		//xmlRecords=writer.toString();
		logi.logInfo "xpath change completed"+input
	    }catch(Exception e){
			
		logi.logInfo "xpath change working throwed..."+input
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		println sw.toString();
	}
		
	}
	String datediff(String d1, String d2)
	{
	logi.logInfo "into the datediff method"
	SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
	 		long diff = 0;
	 		try {
	 		    Date date1 = myFormat.parse(d1);
	 		    Date date2 = myFormat.parse(d2);
	 		    diff = date2.getTime() - date1.getTime();
	 		    System.out.println ("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
	 		} catch (Exception e) {
	 		    e.printStackTrace();
	 		}
	         return String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
    logi.logInfo "done with the datediff method"
	}
	
	public String getPastAndFutureMonthP2(String clientNo, String numberOfDays, String dateMonthFormat)
	{
		db = new ConnectDB();
		Date date = new Date();
		Calendar now = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL";
		ResultSet resultSet = db.executeQuery(Query);
		while(resultSet.next()){
			// Get only the current Date

			date = resultSet.getDate(1);
		}
		now.setTime(date);
		if(numberOfDays.contains("-")){
			if(numberOfDays.contains(".")) {
				String[] days = numberOfDays.split("\\.");
				now.add(Calendar.MONTH, -(Integer.parseInt(days[0])));
				
			}
			else
				now.add(Calendar.MONTH,Integer.parseInt(numberOfDays));
		}
		else{
			if(numberOfDays.contains(".")) {
				String[] days = numberOfDays.split("\\.");
				now.add(Calendar.MONTH, Integer.parseInt(days[0]));
			
			}
			else
				now.add(Calendar.MONTH, Integer.parseInt(numberOfDays));
		}

		SimpleDateFormat format1 = new SimpleDateFormat(dateMonthFormat);
		String date1 = format1.format(now.getTime());
		//db.closeConnection()
		
		
		logi.logInfo('----------> askjdaslkdghsakdgsaojsa'+date1)
				
		return date1;
	}
	public boolean advanceOffsetHoursWithoutBatch(String clientNo, long additionalHours) throws SQLException {
		
				boolean isSuccessful = false;
				ConnectDB db = new ConnectDB();
				// Get the offset hours
				ResultSet rs = db.executePlaQuery("SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='VIRTUAL_TIME_OFFSET_HOURS' AND CLIENT_NO=" + clientNo);
		
				long currentOffsetHours = 0 ;
				if(rs.next()) {
					currentOffsetHours = Long.parseLong(rs.getString(1));
				}
				rs.close()
				long advanceOffsetHours = currentOffsetHours + additionalHours;
		
				// Update the offset hours to advance the virtual time
				rs = db.executePlaQuery("UPDATE ARIACORE.ARIA_CLIENT_PARAMS SET PARAM_VAL = "+ advanceOffsetHours + " WHERE PARAM_NAME='VIRTUAL_TIME_OFFSET_HOURS' AND CLIENT_NO=" + clientNo);
				rs.close()
				
				db.connection.commit();
				// Get the current db value
				rs = db.executePlaQuery("SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='VIRTUAL_TIME_OFFSET_HOURS' AND CLIENT_NO=" + clientNo);
				if(rs.next()) {
					if(advanceOffsetHours == Long.parseLong(rs.getString(1))) {
						isSuccessful = true;
											
						
					}
				}
				rs.close()
				return isSuccessful;
			}
	
}
