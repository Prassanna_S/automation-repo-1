package library

import java.awt.geom.Arc2D.Double;
import java.lang.reflect.Method
import java.sql.ResultSet;
import java.sql.ResultSetMetaData
import java.sql.SQLException
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import groovy.xml.StreamingMarkupBuilder

import java.lang.Object

import org.apache.ivy.core.module.descriptor.ExtendsDescriptor;

import library.Constants.Constant;
import library.Constants.ExpValueConstants;
import library.Constants.ExPathRpc;
import library.Constants.ExPathRpcEnc;
import library.Constants.ExPathAdminToolsApi;
import library.Constants.Constant.RESULT_TYPE;

public class VerificationMethods {

	static ConnectDB db = null;
	static invNo = "1"
	static count = 1;
	LogResult logi = new LogResult(Constant.logFilePath)
	String Query = null
	public static def charges
	public static def order_amt
	public static String inc_charges
	public static HashMap seqs
	public static String invoiceNo

	public DecimalFormat df=new DecimalFormat()

	public VerificationMethods(ConnectDB db) {
		this.db = db
	}
	String md_Do_Proration_Cal_InvSeq_1_Bill_lag(String testCaseId) {

		return md_Do_Proration_Cal_Bill_lag(testCaseId, ExpValueConstants.DB_BILL_LAG_INTERVAL_INVSEQ_1, ExpValueConstants.DB_BILL_LAG_RATE_PER_UNIT_INVSEQ_1,"1")
	}
	String md_Do_Proration_Cal_InvSeq_2_Bill_lag(String testCaseId) {
		return md_Do_Proration_Cal_Bill_lag(testCaseId, ExpValueConstants.DB_BILL_LAG_INTERVAL_INVSEQ_2, ExpValueConstants.DB_BILL_LAG_RATE_PER_UNIT_INVSEQ_2,"2")
	}
	String md_Do_Proration_Cal_InvSeq_1(String testCaseId) {

		return md_Do_Proration_Cal(testCaseId, ExpValueConstants.DB_BILLING_INTERVAL_INVSEQ_2, ExpValueConstants.DB_RATE_PER_UNIT_INVSEQ_2)
	}
	String md_Do_Proration_Cal_InvSeq_2(String testCaseId) {

		return md_Do_Proration_Cal(testCaseId, ExpValueConstants.DB_BILLING_INTERVAL_INVSEQ_3, ExpValueConstants.DB_RATE_PER_UNIT_INVSEQ_3)
	}
	String md_Do_Proration_Cal_InvSeq_3(String testCaseId) {

		return md_Do_Proration_Cal(testCaseId, ExpValueConstants.DB_BILLING_INTERVAL_INVSEQ_4, ExpValueConstants.DB_RATE_PER_UNIT_INVSEQ_4)
	}
	String md_INV_LINE_AMOUNT_1_ACT(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT(testCaseId, invNo, "1").toString();
	}
	String md_INV_LINE_AMOUNT_2_ACT(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT(testCaseId, invNo, "2").toString();
	}
	String md_INV_LINE_AMOUNT_3_ACT(String testCaseId) {
		return md_INV_LINE_AMOUNT_ACT(testCaseId, invNo, "3").toString();
	}
	String md_INV_LINE_AMOUNT_4_ACT(String testCaseId) {
		return md_INV_LINE_AMOUNT_ACT(testCaseId, invNo, "4").toString();
	}
	String md_INV_LINE_AMOUNT_5_ACT(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT(testCaseId, invNo, "5").toString();
	}

	String md_INV_LINE_AMOUNT_9_ACT(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT(testCaseId, invNo, "9").toString();
	}

	String md_INV_LINE_AMOUNT_10_ACT(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT(testCaseId, invNo, "10").toString();
	}

	String md_INV_LINE_AMOUNT_11_ACT(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT(testCaseId, invNo, "11").toString();
	}

	String md_INV_LINE_AMOUNT_12_ACT(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT(testCaseId, invNo, "12").toString();
	}

	String md_INV_LINE_AMOUNT_13_ACT(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT(testCaseId, invNo, "13").toString();
	}

	String md_INV_LINE_AMOUNT_14_ACT(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT(testCaseId, invNo, "14").toString();
	}

	String md_INV_LINE_AMOUNT_15_ACT(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT(testCaseId, invNo, "15").toString();
	}

	String md_INV_LINE_AMOUNT_16_ACT(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT(testCaseId, invNo, "16").toString();
	}


	String md_INV_LINE_AMOUNT_ACT(String testCaseId, String invNo, String seqNum) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT USAGE_RATE * USAGE_UNITS T FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND SEQ_NUM=" + seqNum
		//logi.logInfo(query)
		//logi.logInfo(testCaseId)
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}
		return amt;
	}

	String md_INV_LINE_AMOUNT_1_ACT_ASP_1(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT_ASP(testCaseId, "1").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_ASP_2(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT_ASP(testCaseId, "2").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_ASP_3(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT_ASP(testCaseId, "3").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_ASP_4(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT_ASP(testCaseId, "4").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_ASP_5(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT_ASP(testCaseId, "5").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_ASP_6(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT_ASP(testCaseId, "6").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_ASP_7(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT_ASP(testCaseId, "7").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_ASP_8(String testCaseId) {

		return md_INV_LINE_AMOUNT_ACT_ASP(testCaseId, "8").toString();
	}

	// assign supp plan
	String md_INV_LINE_AMOUNT_ACT_ASP(String testCaseId, String seqNum) {

		String amt
		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")

		readData.fetchTestData(testId);
		ConnectDB db = new ConnectDB()
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def suppPlanNo = readData.TD_assign_supp_plan
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT USAGE_RATE * USAGE_UNITS T FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND PLAN_NO =" + suppPlanNo + " AND SEQ_NUM=" + seqNum
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}
		return amt;
	}

	String md_INV_SERVICE_CREDIT_ACT(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def suppPlanNo = readData.TD_assign_supp_plan
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND PLAN_NO is null"
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}
		return amt;
	}

	String md_INV_SERVICE_CREDIT_EXP(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def suppPlanNo = readData.TD_assign_supp_plan
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND PLAN_NO is null"
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}
		return amt;
	}

	String md_INV_LINE_AMOUNT_ACT_ASPM_SUPP_PLAN_1 (String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		String suppPlan = readData.TD_assign_supp_plan_1
		return md_INV_LINE_AMOUNT_ACT_ASPM(testCaseId, suppPlan)
	}

	String md_INV_LINE_AMOUNT_EXP_ASPM_SUPP_PLAN_1 (String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		String suppPlan = readData.TD_assign_supp_plan_1
		return md_INV_LINE_AMOUNT_ACT_ASPM(testCaseId, suppPlan)
	}

	String md_INV_LINE_AMOUNT_ACT_ASPM_SUPP_PLAN_2 (String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		String suppPlan = readData.TD_assign_supp_plan_2
		return md_INV_LINE_AMOUNT_ACT_ASPM(testCaseId, suppPlan)
	}

	String md_INV_LINE_AMOUNT_EXP_ASPM_SUPP_PLAN_2 (String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		String suppPlan = readData.TD_assign_supp_plan_2
		return md_INV_LINE_AMOUNT_ACT_ASPM(testCaseId, suppPlan)
	}

	String md_INV_LINE_AMOUNT_ACT_ASPM_SUPP_PLAN_3 (String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		String suppPlan = readData.TD_assign_supp_plan_3
		return md_INV_LINE_AMOUNT_ACT_ASPM(testCaseId, suppPlan)
	}

	String md_INV_LINE_AMOUNT_EXP_ASPM_SUPP_PLAN_3 (String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		String suppPlan = readData.TD_assign_supp_plan_3
		return md_INV_LINE_AMOUNT_ACT_ASPM(testCaseId, suppPlan)
	}

	String md_INV_LINE_AMOUNT_ACT_ASPM_SUPP_PLAN_4 (String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		String suppPlan = readData.TD_assign_supp_plan_4
		return md_INV_LINE_AMOUNT_ACT_ASPM(testCaseId, suppPlan)
	}

	String md_INV_LINE_AMOUNT_EXP_ASPM_SUPP_PLAN_4 (String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		String suppPlan = readData.TD_assign_supp_plan_4
		return md_INV_LINE_AMOUNT_ACT_ASPM(testCaseId, suppPlan)
	}
	def getNodeCountFromResponse(String apiname,String xpath)
	{
		logi.logInfo("Calling getNodeCountFromResponse for the node " +xpath+" and for api "+apiname )
		def nodecount = "0"
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()

		for(int j=0; j<testCaseCmb.size(); j++) {
			//logi.logInfo("Key in reponsemap " +testCaseCmb[j].toString())
			if(testCaseCmb[j].toString().contains(apiname) && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				if(holder.containsKey(xpath) && holder.containsValue(xpath)) {

					//nodecount = holder['"'+'count('+xpath+')'+'"']
					//nodecount=holder["count(//*/*:refund_details_row)"]
					nodecount=holder["count("+xpath+")"]

				}
			}
		}
		logi.logInfo "The node count: "+ nodecount.toString()
		return nodecount

	}

	def getNodeCountFromRequest(String apiname,String xpath)
	{
		logi.logInfo("Calling getNodeCountFromRequest for the node " +xpath+" and for api "+apiname )
		def nodecount = "0"
		List testCaseCmb = Constant.lhmAllInputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllInputResponseAsXML_Global.values().asList()
		String tcid= Constant.TESTCASEID
		String suitename=Constant.TESTSUITENAME

		for(int j=0; j<testCaseCmb.size(); j++) {
			//logi.logInfo("Key in reponsemap " +testCaseCmb[j].toString())
			if(testCaseCmb[j].toString().equals(suitename+"-"+tcid+"-"+apiname))
			{
				logi.logInfo("testCaseCmb"+testCaseCmb[j].toString())
				logi.logInfo("inside  if")
				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				logi.logInfo("holder: "+xmlValues[j].getXml())

				if(holder.containsKey(xpath))
				{
					logi.logInfo("inside nested if")
					//nodecount = holder['"'+'count('+xpath+')'+'"']
					//nodecount=holder["count(//*/*:refund_details_row)"]
					nodecount=holder["count("+xpath+")"]

				}
			}
		}
		logi.logInfo "The node count: "+ nodecount.toString()
		return nodecount

	}
	def getValueFromResponse(String apiname,String xpath) {

		def nodeValue = "NoVal"
		String tcid= Constant.TESTCASEID
		String suitename=Constant.TESTSUITENAME
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		//logi.logInfo(testCaseCmb[0].toString())
		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().equals(suitename+"-"+tcid+"-"+apiname)) {
				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
					nodeValue = holder.getNodeValue(xpath)
				}
			}
		}
		return nodeValue
	}
	def getValueFromResponse(String tcid,String apiname,String xpath) {
		
				def nodeValue = "NoVal"				
				String suitename=Constant.TESTSUITENAME
				List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
				List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
				//logi.logInfo(testCaseCmb[0].toString())
				for(int j=0; j<testCaseCmb.size(); j++) {
					if(testCaseCmb[j].toString().equals(suitename+"-"+tcid+"-"+apiname)) {
						com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
						if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
							nodeValue = holder.getNodeValue(xpath)
						}
					}
				}
				return nodeValue
			}

	def getValueFromRequest(String apiname,String xpath) {
		logi.logInfo "Calling getValueFromRequest"
		def nodeValue = "NoVal"
		String tcid= Constant.TESTCASEID
		String suitename=Constant.TESTSUITENAME
		List testCaseCmb = Constant.lhmAllInputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllInputResponseAsXML_Global.values().asList()
		
		for(int j=0; j<testCaseCmb.size(); j++) {
			
			if(testCaseCmb[j].toString().equals(suitename+"-"+tcid+"-"+apiname)) {
				
				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
					nodeValue = holder.getNodeValue(xpath)
				}
				else
				{
					logi.logInfo "The node "+xpath+"does not exist in  Request for "+apiname
				}
			}
		}
		logi.logInfo "The node from Request for "+apiname+ " : "+ nodeValue

		return nodeValue
	}

	// Assign_supp_plan_multi
	String md_INV_LINE_AMOUNT_ACT_ASPM(String testCaseId, String suppPlan) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan_multi", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT SUM(USAGE_RATE * USAGE_UNITS) T FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND PLAN_NO =" + suppPlan
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}
		return amt;
	}

	String md_INV_LINE_AMOUNT_EXP_ASPM(String testCaseId, String suppPlan) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan_multi", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT SUM(DEBIT) T FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND PLAN_NO =" + suppPlan
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}
		return amt;
	}

	String md_InternalTax_ASP(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def suppPlanNo = readData.TD_assign_supp_plan
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + "AND SERVICE_NO not in (401,402,405) AND CLIENT_NO=" + clientNo
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}

		query = "SELECT TAX_RATE FROM ARIACORE.TAX_RATE_VIEW where tax_id = (SELECT tax_id from ARIACORE.TAX_NAMES where client_no = " + clientNo + ")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			taxRate = rs.getString(1);
		}

		double taxAmt = amt.toDouble() * taxRate.toDouble();

		return df.format(taxAmt).toString()
	}

	String md_VERIFY_USAGE_PLAN_MASTER_PLAN_1(String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def planNo = readData.TD_masterPlanNo
		return md_VERIFY_USAGE_ACT(testCaseId, planNo)
	}

	String md_VERIFY_USAGE_PLAN_SUPP_PLAN_1(String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def planNo = readData.TD_suppPlan1
		return md_VERIFY_USAGE_ACT(testCaseId, planNo)
	}

	String md_VERIFY_USAGE_PLAN_MASTER_PLAN_1_EXP(String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def planNo = readData.TD_masterPlanNo
		return md_VERIFY_USAGE_EXP(testCaseId, planNo)
	}

	String md_VERIFY_USAGE_PLAN_SUPP_PLAN_1_EXP(String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def planNo = readData.TD_suppPlan1
		return md_VERIFY_USAGE_EXP(testCaseId, planNo)
	}

	String md_VERIFY_USAGE_ACT(String testCaseId, String planNo) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("record_usage", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def usageType = readData.TD_Usage_Type
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT SUM(USAGE_UNITS * USAGE_RATE) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND USAGE_TYPE = " + usageType + " AND PLAN_NO=" + planNo + " AND CLIENT_NO=" + clientNo
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}
		return amt.toString()
	}

	String md_VERIFY_USAGE_EXP(String testCaseId, String planNo) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("record_usage", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def usageType = readData.TD_Usage_Type
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND USAGE_TYPE = " + usageType + " AND PLAN_NO=" + planNo + " AND CLIENT_NO=" + clientNo
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}
		return amt
	}


	String md_InternalTax_Create_Order(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String createOrderTestId = testCaseId

		String invoiceNo = getXpathNodeValue(createOrderTestId, readData.getXPath("CREATE_ORDER_INVOICENO1"))
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo

		String query = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + "AND SERVICE_NO not in (401,402,405) AND CLIENT_NO=" + clientNo

		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()){
			amt = resultSet.getInt(1);
		}
		else
		{
			amt = "No Val ret"
		}

		query = "SELECT TAX_RATE FROM ARIACORE.TAX_RATE_VIEW where tax_id = (SELECT tax_id from ARIACORE.TAX_NAMES where client_no = " + clientNo + ")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate;
		if(rs.next()) {
			taxRate = rs.getString(1);
		}

		double taxAmt = amt.toDouble() * taxRate.toDouble();

		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(taxAmt).toString()
	}


	String md_ExternalTax_Avalara_Create_Order(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String createOrderTestId = testCaseId

		String invoiceNo = getXpathNodeValue(createOrderTestId, readData.getXPath("CREATE_ORDER_INVOICENO1"))
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo

		String query = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + "AND SERVICE_NO not in (401,402,405) AND CLIENT_NO=" + clientNo

		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()){
			amt = resultSet.getInt(1);
		}
		else
		{
			amt = "No Val ret"
		}

		double taxRate = 0.075;

		double taxAmt = amt.toDouble() * taxRate;

		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(taxAmt).toString()
	}



	String md_ExternalTax_Avalara_ASPM_ACT(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan_multi", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND SERVICE_NO in (401,402) AND CLIENT_NO=" + clientNo
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {
		
			ResultSet resultSet = db.executePlaQuery(query);
			
			if (resultSet.next()){
				amt = resultSet.getString(1);
				
			}
			else
			{
				amt = "No Val ret"
			}
		}
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(amt.toDouble()).toString()
	}

	String md_InternalTax_ASP_ACT(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def suppPlanNo = readData.TD_assign_supp_plan
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + "AND SERVICE_NO in (401,402,405) AND CLIENT_NO=" + clientNo
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getString(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}
		DecimalFormat df = new DecimalFormat("#.#");
		return df.format(amt.toDouble()).toString()
	}

	HashMap md_CHECK_REFUNDABLE_LINE_ITEMS_DB1(String tsetid)
	{
		return  md_CHECK_REFUNDABLE_LINE_ITEMS_DB("1")
	}

	HashMap md_CHECK_REFUNDABLE_LINE_ITEMS_DB2(String tsetid)
	{
		return  md_CHECK_REFUNDABLE_LINE_ITEMS_DB("2")
	}

	HashMap md_CHECK_REFUNDABLE_LINE_ITEMS_DB3(String tsetid)
	{
		return  md_CHECK_REFUNDABLE_LINE_ITEMS_DB("3")
	}
	HashMap md_CHECK_REFUNDABLE_LINE_ITEMS_API1(String tsetid)
	{
		return  md_CHECK_REFUNDABLE_LINE_ITEMS_API("1")
	}

	HashMap md_CHECK_REFUNDABLE_LINE_ITEMS_API2(String tsetid)
	{
		return  md_CHECK_REFUNDABLE_LINE_ITEMS_API("2")
	}

	HashMap md_CHECK_REFUNDABLE_LINE_ITEMS_API3(String tsetid)
	{
		return  md_CHECK_REFUNDABLE_LINE_ITEMS_API("3")
	}

	HashMap md_CHECK_REFUND_DETAILS_LINE_ITEMS_DB1(String tsetid)
	{
		return  md_CHECK_REFUND_DETAILS_LINE_ITEMS_DB("1")
	}

	HashMap md_CHECK_REFUND_DETAILS_LINE_ITEMS_DB2(String tsetid)
	{
		return  md_CHECK_REFUND_DETAILS_LINE_ITEMS_DB("2")
	}

	HashMap md_CHECK_REFUND_DETAILS_LINE_ITEMS_DB3(String tsetid)
	{
		return  md_CHECK_REFUND_DETAILS_LINE_ITEMS_DB("3")
	}


	HashMap md_CHECK_REFUND_DETAILS_LINE_ITEMS_API1(String tsetid)
	{
		return  md_CHECK_REFUND_DETAILS_LINE_ITEMS_API("1")
	}

	HashMap md_CHECK_REFUND_DETAILS_LINE_ITEMS_API2(String tsetid)
	{
		return  md_CHECK_REFUND_DETAILS_LINE_ITEMS_API("2")
	}

	HashMap md_CHECK_REFUND_DETAILS_LINE_ITEMS_API3(String tsetid)
	{
		return  md_CHECK_REFUND_DETAILS_LINE_ITEMS_API("3")
	}
	def md_CHECK_REFUND_DETAILS_LINE_ITEMS_API(String lineno)
	{
		logi.logInfo ("Calling Verification method  md_CHECK_REFUNDABLE_LINE_ITEMS_API for line item: "+lineno)
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		LinkedHashMap apirow = new LinkedHashMap();

		com.eviware.soapui.support.XmlHolder outputholder

		//logi.logInfo(xmlValues.size().toString())

		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains('get_refund_details') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				apirow.put('REFUND_TRANSACTION_ID', outputholder.getNodeValue("//*/*/*:refund_details_row["+lineno+"]/*:refund_transaction_id[1]"))
				apirow.put("REFUND_AMOUNT",outputholder.getNodeValue("//*/*/*:refund_details_row["+lineno+"]/*:refund_amount[1]"))
				apirow.put("REFUND_REASON_CODE",outputholder.getNodeValue("//*/*/*:refund_details_row["+lineno+"]/*:reason_code[1]"))
				apirow.put('REFUND_PAYMENT_TRANSACTION_ID', outputholder.getNodeValue("//*/*/*:refund_details_row["+lineno+"]/*:ref_payment_transaction_id[1]"))
				apirow.put("REFUND_PAYMENT_AMOUNT",outputholder.getNodeValue("//*/*/*:refund_details_row["+lineno+"]/*:ref_payment_amount[1]"))
				apirow.put("PAY_METHOD_ID",outputholder.getNodeValue("//*/*/*:refund_details_row["+lineno+"]/*:pay_method_id[1]"))
				apirow.put("PAY_METHOD_NAME",outputholder.getNodeValue("//*/*/*:refund_details_row["+lineno+"]/*:pay_method_name[1]"))
				apirow.put("IS_VOIDED_IND",outputholder.getNodeValue("//*/*/*:refund_details_row["+lineno+"]/*:is_voided_ind[1]"))

			}
		}
		logi.logInfo("Refund details from the Response:")
		logi.logInfo(apirow.toString())

		return apirow;
	}
	def md_CHECK_REFUND_DETAILS_LINE_ITEMS_DB(String seqno)
	{
		logi.logInfo ("Calling Verification method  md_CHECK_REFUND_DETAILS_LINE_ITEMS_DB for seq number " +seqno)
		ConnectDB db = new ConnectDB()
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String query_all_refund_details="SELECT REFUND_TRANSACTION_ID, REFUND_AMOUNT,REFUND_REASON_CODE,REFUND_PAYMENT_TRANSACTION_ID,REFUND_PAYMENT_AMOUNT,pay_method_id,pay_method_name,is_voided_ind from (SELECT event_no as REFUND_TRANSACTION_ID,amount as REFUND_AMOUNT,reason_cd as REFUND_REASON_CODE,refunded_payment_event_no as REFUND_PAYMENT_TRANSACTION_ID,"+
				" refunded_payment_amount as REFUND_PAYMENT_AMOUNT,pay_method_id,pay_method_name,is_voided_ind,row_number() over (order by event_no) as seqnum from "+
				" ariacore.all_refund_disbursement where acct_no="+acct_no+" and client_no="+client_no+") where seqnum ="+seqno
		//logi.logInfo(query_all_refund_details)
		ResultSet resultSet = db.executePlaQuery(query_all_refund_details);
		ResultSetMetaData md = resultSet.getMetaData();
		int columns = md.getColumnCount();
		LinkedHashMap row = new LinkedHashMap();

		while (resultSet.next()){
			for(int i=1; i<=columns; i++){
				row.put(md.getColumnName(i),resultSet.getObject(i));
			}

		}
		logi.logInfo("Refund details from the DB:")
		logi.logInfo(row.toString())
		return row ;
	}
	String md_GET_REFUND_DETAILS_LINE_ITEMS_COUNT(String testid)
	{
		logi.logInfo("Calling md_GET_REFUND_DETAILS_LINE_ITEMS_COUNT")
		return getNodeCountFromResponse("get_refund_details","//*/*:refund_details_row").toString()

	}

	def  md_CHECK_REFUNDABLE_LINE_ITEMS_DB(String seqno)
	{

		logi.logInfo ("Calling Verification method  md_CHECK_REFUNDABLE_LINE_ITEMS_DB for the line item: " + seqno)
		ConnectDB db = new ConnectDB()
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def pay_method
		def pay_source
		def cc_id

		//String query="select payment_trans_event_no as payment_transaction_id ,to_char(payment_date,'MM/DD/YYYY') as payment_date ,amount as payment_amount ,refunded_amount as payment_refunded_amount ,refundable_amount as payment_refundable_amount from  ariacore.all_refundable_payments where acct_no="+acct_no
		String query="select payment_transaction_id,to_number(TO_CHAR(payment_date,'mm'))  || '/'  || to_number(TO_CHAR(payment_date,'dd'))  || '/'  ||to_number(TO_CHAR(payment_date,'yyyy')) as payment_date  ,payment_amount,payment_refunded_amount ,payment_refundable_amount from (select payment_trans_event_no as payment_transaction_id ,payment_date as payment_date ,amount as payment_amount ,refunded_amount as payment_refunded_amount ,refundable_amount as payment_refundable_amount , row_number() over (order by payment_id desc) as seqnum "+
				"from ariacore.all_refundable_payments where acct_no="+acct_no+" ) where seqnum ="+seqno
		//logi.logInfo (query)
		ResultSet resultSet = db.executePlaQuery(query);
		ResultSetMetaData md = resultSet.getMetaData();
		int columns = md.getColumnCount();
		HashMap row = new HashMap();

		while (resultSet.next()){
			for(int i=1; i<=columns; i++){
				row.put(md.getColumnName(i),resultSet.getObject(i));
			}

		}
		logi.logInfo("Refundable payments from DB:")
		logi.logInfo(row.toString())

		String paysrc_qry="SELECT pay_method,pay_source,cc_id FROM (SELECT pay_method,pay_source,cc_id,row_number() over (order by payment_id DESC) AS seqno FROM ariacore.all_refundable_payments WHERE acct_no="+acct_no+") WHERE seqno="+seqno
		ResultSet rs_paysrc = db.executePlaQuery(paysrc_qry);
		while( rs_paysrc.next())
		{
			pay_method = rs_paysrc.getObject(1)
			pay_source=rs_paysrc.getObject(2)
			cc_id=rs_paysrc.getObject(3)
			logi.logInfo("pay method"+pay_method)
			logi.logInfo("pay source"+pay_source)
			logi.logInfo("cc id"+cc_id)
		}

		if(pay_method.toString()=='1' && cc_id.toString()=='1')
		{
			row.put("PAYMENT_DESCRIPTION","Visa ending in "+pay_source);
		}
		else if(pay_method.toString()=='1' && cc_id.toString()=='2')
		{
			row.put("PAYMENT_DESCRIPTION","MasterCard ending in "+pay_source);
		}
		else if(pay_method.toString()=='2')
		{
			row.put("PAYMENT_DESCRIPTION","Electronic Check (ACH)");
		}
		else if(pay_method.toString()=='13')
		{
			row.put("PAYMENT_DESCRIPTION","Tokenized Credit Card");
		}
		else
		{
			row.put("PAYMENT_DESCRIPTION","External Payment "+pay_source);
		}

		return row ;

	}
	def md_CHECK_REFUNDABLE_LINE_ITEMS_API(String lineno)
	{

		logi.logInfo ("Calling Verification method  md_CHECK_REFUNDABLE_LINE_ITEMS_API for the line item " + lineno)
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap apirow = new HashMap();
       

		com.eviware.soapui.support.XmlHolder outputholder

		logi.logInfo(xmlValues.size().toString())

		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains('get_refundable_payments') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				apirow.put('PAYMENT_TRANSACTION_ID', outputholder.getNodeValue("//*/*/*:refundable_payments_row["+lineno+"]/*:payment_transaction_id[1]"))
				apirow.put("PAYMENT_DATE",outputholder.getNodeValue("//*/*/*:refundable_payments_row["+lineno+"]/*:payment_date[1]"))
				apirow.put("PAYMENT_AMOUNT",outputholder.getNodeValue("//*/*/*:refundable_payments_row["+lineno+"]/*:payment_amount[1]"))
				apirow.put("PAYMENT_REFUNDABLE_AMOUNT",outputholder.getNodeValue("//*/*/*:refundable_payments_row["+lineno+"]/*:payment_refundable_amount[1]"))
				apirow.put("PAYMENT_REFUNDED_AMOUNT",outputholder.getNodeValue("//*/*/*:refundable_payments_row["+lineno+"]/*:payment_refunded_amount[1]"))
				apirow.put("PAYMENT_DESCRIPTION",outputholder.getNodeValue("//*/*/*:refundable_payments_row["+lineno+"]/*:payment_description[1]"))
			}

		}

		logi.logInfo("Refundable payments from the api:")
		logi.logInfo(apirow.toString())


		return apirow;

	}
	String md_InternalTax_ASPM_ACT(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan_multi", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def suppPlanNo = readData.TD_assign_supp_plan
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + "AND SERVICE_NO = 401 AND CLIENT_NO=" + clientNo
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getString(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(amt.toDouble()).toString()
	}

	String md_INV_LINE_AMOUNT_1_ACT_EXP_1(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP_ASP(testCaseId, "1").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_EXP_2(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP_ASP(testCaseId, "2").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_EXP_3(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP_ASP(testCaseId, "3").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_EXP_4(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP_ASP(testCaseId, "4").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_EXP_5(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP_ASP(testCaseId, "5").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_EXP_6(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP_ASP(testCaseId, "6").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_EXP_7(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP_ASP(testCaseId, "7").toString();
	}

	String md_INV_LINE_AMOUNT_1_ACT_EXP_8(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP_ASP(testCaseId, "8").toString();
	}

	// assign supp plan
	String md_INVOICE_LINE_AMOUNT_EXP_ASP(String testCaseId, String seqNum) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def suppPlanNo = readData.TD_assign_supp_plan
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND PLAN_NO = " + suppPlanNo + " AND SEQ_NUM=" + seqNum
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}
		return amt;
	}

	String md_INVOICE_LINE_AMOUNT_EXP(String testCaseId, String invNo, String seqNum) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();

		if(accountNo != "NoVal" || invoiceNo != "NoVal") {

			String query = "SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND SEQ_NUM=" + seqNum

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		} else {
			amt = "No Val Ret"
		}
		return amt;
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_1_EXP(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP(testCaseId, invNo, "1").toString();
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_2_EXP(String testCaseId) {


		return md_INVOICE_LINE_AMOUNT_EXP(testCaseId, invNo, "2").toString();
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_3_EXP(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP(testCaseId, invNo, "3").toString();
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_4_EXP(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP(testCaseId, invNo, "4").toString();
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_5_EXP(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP(testCaseId, invNo, "5").toString();
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_9_EXP(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP(testCaseId, invNo, "9").toString();
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_10_EXP(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP(testCaseId, invNo, "10").toString();
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_11_EXP(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP(testCaseId, invNo, "11").toString();
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_12_EXP(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP(testCaseId, invNo, "12").toString();
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_13_EXP(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP(testCaseId, invNo, "13").toString();
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_14_EXP(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP(testCaseId, invNo, "14").toString();
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_15_EXP(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP(testCaseId, invNo, "15").toString();
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_16_EXP(String testCaseId) {

		return md_INVOICE_LINE_AMOUNT_EXP(testCaseId, invNo, "16").toString();
	}

	String md_SecondMonth_InvoiceExits_Supp1(String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def plan_no = readData.TD_suppPlan1

		return md_IsInvoice_Exists(testCaseId, "1", plan_no)
	}

	String md_SecondMonth_InvoiceExits_Supp2(String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def plan_no = readData.TD_suppPlan2

		return md_IsInvoice_Exists(testCaseId, "1", plan_no)
	}

	String md_SecondMonth_InvoiceExits_Supp3(String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def plan_no = readData.TD_suppPlan3

		return md_IsInvoice_Exists(testCaseId, "1", plan_no)
	}

	String md_IsInvoice_Exists(String testCaseId, String month, String suppPlan) {

		String isSuccessful = 'false'
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = month
		def plan_no = suppPlan
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime)

		String query = "SELECT INVOICE_NO FROM ARIACORE.GL_DETAIL WHERE PLAN_NO=" + plan_no + " AND INVOICE_NO=" + invoiceNo

		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()){
			isSuccessful = 'true'
		}

		return isSuccessful
	}

	String md_INVOICE_LINE_AMOUNT_SEQ_1_ACT(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		if(accountNo == "NoVal") {
			String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString()
			if(invoiceNo == "No Val ret")
			{
				invoiceNo = '0'
			}

			String query = "SELECT RATE_PER_UNIT * <CREATE_ACCT_COMPLETE_INVOICEUNITS1> FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SERVICE_NO=<CREATE_ACCT_COMPLETE_INVOICESERVICENO1> AND SCHEDULE_NO=(SELECT RATE_SCHEDULE_NO FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = <CREATE_ACCT_COMPLETE_ACCTNO1> AND PLAN_NO=<CREATE_ACCT_COMPLETE_INVOICEPLANNO1> AND SERVICE_NO=<CREATE_ACCT_COMPLETE_INVOICESERVICENO1> AND RATE_SEQ_NO=1) AND RATE_SEQ_NO = 1"

			String billIntvQ = db.buildQuery(query, clientNo, getNodeVal(testCaseId, query))
			ResultSet resultSet = db.executePlaQuery(billIntvQ);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		} else {
			amt = "No Val ret"
		}


		return amt.toString();
	}

	String md_AfterVirtualTime(String testCaseId) {
		String ret
		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		if(accountNo == "NoVal") {
			ret = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		} else {
			ret = "NoVal"
		}

		//		return getAllInvoiceNo(accountNo, clientNo).toString();
		return
	}

	String getInvoiceNo(String clientNo, String accountNo, String advMonths) throws ParseException, SQLException{

		String invoiceNo;
		ConnectDB db = new ConnectDB()
		String currentVirtualTime = getStoredVirtualTime(clientNo)

		// Stores the current virtual time in the specified format
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);

		// Add one month
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date);

		if(advMonths.contains(".")) {
			String advVT = advMonths.substring(advMonths.indexOf(".") + 1)
			cal2.add(Calendar.DAY_OF_MONTH, advVT.toInteger());
			cal2.add(Calendar.DAY_OF_MONTH, -1);
		} else {
			cal2.add(Calendar.MONTH, advMonths.toInteger());
			cal2.add(Calendar.DAY_OF_MONTH, -1);
		}

		Calendar cal3 = Calendar.getInstance();
		cal3.setTime(cal2.getTime());
		cal3.add(Calendar.DAY_OF_MONTH, 10);

		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

		String Query = "SELECT INVOICE_NO FROM ARIACORE.GL Where ACCT_NO =" + accountNo + " and BILL_DATE >= TO_DATE('" + format1.format(cal2.getTime()) + "','dd/MM/yyyy') AND BILL_DATE <= TO_DATE('"+ format1.format(cal3.getTime()) + "','dd/MM/yyyy')";

		ResultSet resultSet = db.executePlaQuery(Query);
		if (resultSet.next()){
			invoiceNo = resultSet.getInt(1).toString();
		}
		else
		{
			invoiceNo = "NoVal"
		}
		return invoiceNo;
	}

	public int getDifferencefromVTtoAT(String clientNo) throws SQLException {
		db = new ConnectDB()
		//String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		// Get the current virtual time for the client
		String virtualTime = getStoredVirtualTime(clientNo)
		logi.logInfo 'Stored Virtual Time : '+ virtualTime
		Date virtualDate =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(virtualTime)
		Calendar cal = Calendar.getInstance();
		cal.setTime(virtualDate);
		int month =  cal.get(Calendar.MONTH)
		logi.logInfo 'Month in Time : '+ month
		logi.logInfo 'Month before set : '+cal.get(Calendar.MONTH)
		cal.set(Calendar.MONTH,month+1)
		logi.logInfo 'Month after set : '+cal.get(Calendar.MONTH)

		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		String date = format1.format(cal.getTime());
		logi.logInfo 'Advance Date : '+ date
		Date advanceDate = format1.parse(date)

		cal.setTime(virtualDate);
		date = format1.format(cal.getTime());
		logi.logInfo 'Virtual Date : '+ date
		virtualDate = format1.parse(date)

		return (int) ((advanceDate.getTime() - virtualDate.getTime()) / (1000 * 60 * 60 * 24)); }

	String getInvoiceNoP2(String clientNo, String accountNo, String advMonths) throws ParseException, SQLException{

		String invoiceNo="";
		String currentVirtualTime = getStoredVirtualTime(clientNo)
		String doProrationInvoice = getValueFromRequest("create_acct_complete","//do_prorated_invoicing")
		String altBillDay = getValueFromRequest("create_acct_complete","//alt_bill_day")
		// Stores the current virtual time in the specified format
		Date date = getBillStartDate(clientNo, accountNo)
		logi.logInfo("getBillStartDate "+date )

		// Add one month
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date);
		String acctActualStartDate
		ResultSet resultSet
		logi.logInfo("Proration and Alt bill day : " + doProrationInvoice + "  " + altBillDay)
		if(doProrationInvoice != "NoVal"  && altBillDay != "NoVal")
		{
			if((doProrationInvoice.equalsIgnoreCase("true")) && (!altBillDay.equalsIgnoreCase("NoVal"))){
				SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
				resultSet = db.executeQuery("SELECT bill_date from ariacore.gl where acct_no = "+ accountNo + " order by invoice_no")
				int i=1
				while(resultSet.next()){
					if(i == 2) {
						acctActualStartDate = resultSet.getString(1)
						logi.logInfo("Start dates in Db for loop 2 : " + acctActualStartDate)
					}
					i++
				}
				logi.logInfo("Start date frm Db : " + acctActualStartDate)
				Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(acctActualStartDate);
				cal2.setTime(date1);
				cal2.add(Calendar.MONTH, -1);
			}
		}


		if(advMonths.contains(".")) {
			String advVT = advMonths.substring(advMonths.indexOf(".") + 1)
			cal2.add(Calendar.DAY_OF_MONTH, advVT.toInteger());
			cal2.add(Calendar.DAY_OF_MONTH, -1);
		} else {
			cal2.add(Calendar.MONTH, advMonths.toInteger());
			cal2.add(Calendar.DAY_OF_MONTH, -1);
		}

		Calendar cal3 = Calendar.getInstance();
		cal3.setTime(cal2.getTime());
		cal3.add(Calendar.DAY_OF_MONTH, 20);

		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

		ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL");
		String currentVT
		if(rs.next()) {
			currentVT = rs.getString(1);
		}
		logi.logInfo 'Current VT before taking the Invoice No : : ' + currentVT

		String Query = "SELECT INVOICE_NO FROM ARIACORE.GL Where ACCT_NO =" + accountNo + " and BILL_DATE >= TO_DATE('" + format1.format(cal2.getTime()) + "','dd/MM/yyyy') AND BILL_DATE <= TO_DATE('"+ format1.format(cal3.getTime()) + "','dd/MM/yyyy')";
		resultSet = db.executeQuery(Query);

		int count =0
		boolean flag = true
		while(flag&&count<50){
			if (resultSet.next()){
				invoiceNo = resultSet.getString(1)
				flag= false
			}
			else {
				Thread.sleep(2000)
				resultSet = db.executeQuery(Query);
			}
			count++
		}

		if(invoiceNo.equalsIgnoreCase(""))
			invoiceNo = "Invoice not updated in DB even after waiting for 50 sec"
		logi.logInfo 'Invoice No : : ' + invoiceNo
		return invoiceNo;
	}

	String getInvNo(String clientNo, String accountNo, String seq) throws ParseException, SQLException{

		String invoiceNo;

		//		String currentVirtualTime = getStoredVirtualTime(clientNo)
		//
		//		// Stores the current virtual time in the specified format
		//		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		//		Calendar cal1 = Calendar.getInstance();
		//		cal1.setTime(date);
		//
		//		// Add one month
		//		Calendar cal2 = Calendar.getInstance();
		//		cal2.setTime(date);
		//
		//		if(advMonths.contains(".")) {
		//			String advVT = advMonths.substring(advMonths.indexOf(".") + 1)
		//			cal2.add(Calendar.DAY_OF_MONTH, advVT.toInteger());
		//			cal2.add(Calendar.DAY_OF_MONTH, -10);
		//		} else {
		//			cal2.add(Calendar.MONTH, advMonths.toInteger());
		//			cal2.add(Calendar.DAY_OF_MONTH, -10);
		//		}
		//
		//		Calendar cal3 = Calendar.getInstance();
		//		cal3.setTime(cal2.getTime());
		//		cal3.add(Calendar.DAY_OF_MONTH, 10);

		//		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yy", Locale.ENGLISH);

		String Query = "SELECT INVOICE_NO FROM ARIACORE.GL Where ACCT_NO =" + accountNo + " order by INVOICE_NO desc"

		//		ResultSet resultSet = db.executePlaQuery(Query);
		//		if (resultSet.next()){
		//			invoiceNo = resultSet.getInt(1).toString();
		//		}
		//		else
		//		{
		//			invoiceNo = "NoVal"
		//		}
		return Query;
	}

	String getStoredVirtualTime(String clientNo) {
		List clientNoLst = Constant.lhmCurrentVirtualTime.keySet().asList()
		List virtualTimeLst = Constant.lhmCurrentVirtualTime.values().asList()
		String storedVirtualTime

		for(int j=0; j<clientNoLst.size(); j++) {
			if(clientNoLst[j].equals(clientNo)) {
				storedVirtualTime = virtualTimeLst[j]
			}
		}
		logi.logInfo "storedVirtualTime "+storedVirtualTime
		return storedVirtualTime
	}

	//	String getInvoiceNo(String accountNo, String invRow) throws ParseException, SQLException{
	//
	//		String invoiceNo;
	////		ArrayList<String> invoiceNo = new ArrayList<String>();
	//
	//		String Query = "SELECT INVOICE_NO FROM (SELECT ROWNUM R, INVOICE_NO FROM ARIACORE.GL where ACCT_NO=" + accountNo + ") WHERE R=" + invRow;
	//
	//		ResultSet resultSet = db.executePlaQuery(Query);
	//		if (resultSet.next()){
	//			invoiceNo = resultSet.getInt(1);
	//		}
	//		else
	//		{
	//			invoiceNo = "No Val ret"
	//		}
	//
	//		return invoiceNo.toString();
	//	}

	String md_Do_Proration_Cal(String testCaseId, String billingIntvQuery, String serviceRateQuery) {



		String ret
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);

		def clientNo = readData.TD_clientNo
		def retroActive = readData.TD_retroActiveStartDate
		def masterPlanUnits = getXpathNodeValue(testCaseId, "//*/*/item[1]/invoice_units[1]")//readData.TD_masterPlanUnits
		//intPro++;
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def billingInterval, serRate

		if(!accountNo.equals("NoVal")) {
			if(masterPlanUnits == null || masterPlanUnits == "") {
				masterPlanUnits = 1
			}

			String billIntvQ = db.buildQuery(billingIntvQuery, clientNo, getNodeVal(testCaseId, billingIntvQuery))
			String serRateQ = db.buildQuery(serviceRateQuery, clientNo, getNodeVal(testCaseId, serviceRateQuery))

			ResultSet resultSet = db.executePlaQuery(billIntvQ)
			if (resultSet.next()) {
				billingInterval = resultSet.getInt(1)
			}
			resultSet = db.executePlaQuery(serRateQ)
			if (resultSet.next()) {
				serRate = resultSet.getInt(1)
			}

			if(serRateQ.contains("NoVal") && billIntvQ.contains("NoVal")) {
				ret = "NoVal"
			} else {
				/*logi.logInfo("Service Rate : " + serRate)
				 logi.logInfo("total Day : " + totaldays(accountNo,billingInterval))
				 logi.logInfo("days to bill : " + daystoBill(accountNo, clientNo, retroActive))
				 logi.logInfo("Master Plan : " + masterPlanUnits)
				 logi.logInfo("billing Interval : " + billingInterval)*/
				double servicerate = serRate;
				double proratedAmount = (((servicerate/(totaldays(accountNo,billingInterval)))*daystoBill(accountNo, clientNo, retroActive)) * masterPlanUnits.toInteger()); //client_no, billing interval, acct_no respectively
				DecimalFormat df = new DecimalFormat("#.##");
				ret = df.format(proratedAmount).toString()
				//				ret = proratedAmount
			}

		} else {
			ret = "NoVal"
		}
		//		ret = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_INVOICELINEAMOUNT2"))
		return ret;
	}

	String md_Verify_UserId_Exists(String testCaseId) {
		def isSuccessful = 'true'
		String result
		ConnectDB db = new ConnectDB()
		String userId = Constant.userId
		ResultSet resultSet = db.executePlaQuery("SELECT userid FROM ARIACORE.ACCT where USERID='" + userId + "'")
		if (!resultSet.next()) {
			isSuccessful = 'false'
		}
		return isSuccessful
	}

	def daystoBill(String acctNo, String clientNo, String retroActive) throws SQLException, ParseException {

		boolean isSuccessful = false;

		//ko
		ConnectDB db = new ConnectDB()
		ResultSet rs = db.executePlaQuery("SELECT Next_bill_date from ARIACORE.ACCT where acct_no = (" + acctNo + ")");

		String nextBillDay = null;
		String lastBillDay = null;
		if(rs.next()) {
			nextBillDay = rs.getString(1);
		}

		ClientParamUtils cp = new ClientParamUtils();
		lastBillDay = cp.getPastAndFutureDate(clientNo, retroActive)

		// Get the Next Bill day
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(nextBillDay);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);


		//  Get the Last bill day
		Date date1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(lastBillDay);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date1);


		// Get the difference in millisecs
		long milis1 = cal1.getTimeInMillis();
		long milis2 = cal2.getTimeInMillis();

		// Calculate difference in milliseconds
		long diff = milis1 - milis2;

		// Calculate difference in days
		long daysToBill = (diff / (60 * 60 * 1000 * 24));
		logi.logInfo("Tewst Bill days : ")
		logi.logInfo("daysToBill : " + daysToBill)
		return daysToBill;
	}


	def daystoBill_Bill_lag(String acctNo, String clientNo) throws SQLException, ParseException {

		boolean isSuccessful = false;

		//ko
		ConnectDB db = new ConnectDB()
		ResultSet rs = db.executePlaQuery("SELECT Next_bill_date from ARIACORE.ACCT where acct_no = (" + acctNo + ")");

		String nextBillDay = null;
		String lastBillDay = null;
		if(rs.next()) {
			nextBillDay = rs.getString(1);
		}
		rs = db.executePlaQuery("SELECT Last_bill_date from ARIACORE.ACCT where acct_no = (" + acctNo + ")");
		logi.logInfo("nextBillDay : " + nextBillDay)
		ClientParamUtils cp = new ClientParamUtils();
		if(rs.next()) {
			lastBillDay = rs.getString(1);
		}
		// = cp.getPastAndFutureDate(clientNo, retroActive)
		logi.logInfo("lastBillDay : " + lastBillDay)
		// Get the Next Bill day
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(nextBillDay);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		//GEt Bill lag day
		rs = db.executePlaQuery("select PARAM_VAL from ariacore.aria_client_params where client_no = " + clientNo + " and param_name='BILL_LAG_DAYS'");
		String billLagDay = null;

		if(rs.next()) {
			billLagDay = rs.getString(1);
		}
		logi.logInfo("billLagDay : " + billLagDay)

		cal1.add(Calendar.DATE, Integer.parseInt(billLagDay));

		logi.logInfo("billLagDay added date  : " + cal1.DATE)
		logi.logInfo("billLagDay added date  : " + cal1.getTime())
		//  Get the Last bill day
		Date date1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(lastBillDay);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date1);


		// Get the difference in millisecs
		long milis1 = cal1.getTimeInMillis();
		long milis2 = cal2.getTimeInMillis();

		// Calculate difference in milliseconds
		long diff = milis1 - milis2;
		logi.logInfo("diff : " + diff)
		// Calculate difference in days
		long daysToBill = (diff / (60 * 60 * 1000 * 24));

		return daysToBill;
	}

	String md_Do_Proration_Cal_Bill_lag(String testCaseId, String billingIntvQuery, String serviceRateQuery, String seq) {



		String ret
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);

		def clientNo = readData.TD_clientNo
		//def retroActive = readData.TD_retroActiveStartDate
		def masterPlanUnits = getXpathNodeValue(testCaseId, "//*/*/item["+seq+"]/line_base_units[1]")//readData.TD_masterPlanUnits
		//intPro++;
		String createAcctCompleteTestId = testCaseId.replaceAll("replace_supp_plan", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def billingInterval, serRate
		serviceRateQuery = "SELECT RATE_PER_UNIT FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO=(SELECT RATE_SCHEDULE_NO FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = "+accountNo+" AND PLAN_NO=<ASSIGN_SUPP_PLAN_PLANNO1> AND SERVICE_NO=<ASSIGN_SUPP_PLAN_SERVICENO1> AND RATE_SEQ_NO=1 AND CLIENT_NO=TD_ClientNo) AND RATE_SEQ_NO = 1 AND SERVICE_NO=<ASSIGN_SUPP_PLAN_SERVICENO1>"
		if(!accountNo.equals("NoVal")) {
			if(masterPlanUnits == null || masterPlanUnits == "") {
				masterPlanUnits = 1
			}

			String billIntvQ = db.buildQuery(billingIntvQuery, clientNo, getNodeVal(testCaseId, billingIntvQuery))
			String serRateQ = db.buildQuery(serviceRateQuery, clientNo, getNodeVal(testCaseId, serviceRateQuery))

			ResultSet resultSet = db.executePlaQuery(billIntvQ)
			if (resultSet.next()) {
				billingInterval = resultSet.getInt(1)
			}
			resultSet = db.executePlaQuery(serRateQ)
			if (resultSet.next()) {
				serRate = resultSet.getInt(1)
			}

			if(serRateQ.contains("NoVal") && billIntvQ.contains("NoVal")) {
				ret = "NoVal"
			} else {
				logi.logInfo("Service Rate : " + serRate)
				logi.logInfo("total Day : " + totaldays(accountNo,billingInterval))
				logi.logInfo("days to bill : " + daystoBill_Bill_lag(accountNo, clientNo))
				logi.logInfo("Master Plan : " + masterPlanUnits)
				logi.logInfo("billing Interval : " + billingInterval)
				double servicerate = serRate;

				double proratedAmount = (((servicerate/(totaldays(accountNo,billingInterval)))*daystoBill_Bill_lag(accountNo, clientNo)) * masterPlanUnits.toInteger()); //client_no, billing interval, acct_no respectively
				DecimalFormat df = new DecimalFormat("#.##");
				ret = df.format(proratedAmount).toString()
				//				ret = proratedAmount
			}

		} else {
			ret = "NoVal"
		}
		//		ret = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_INVOICELINEAMOUNT2"))
		return ret;
	}

	/*	def totaldays(String acctNo, int billingInterval) throws SQLException, ParseException
	 {
	 String Query
	 logi.logInfo("billingInterval : " + billingInterval)
	 if(billingInterval == 1) {
	 Query = "SELECT LAST_ARREARS_BILL_THRU_DATE from ARIACORE.ACCT where acct_no =" + acctNo
	 logi.logInfo("Last_bill date : " + Query)
	 } else {
	 Query = "SELECT Next_bill_date from ARIACORE.ACCT where acct_no =" + acctNo
	 logi.logInfo("Next_bill_date : " + Query)
	 }
	 ResultSet rs = db.executePlaQuery(Query);
	 String currentVirtualTime = null;
	 if(rs.next()) {
	 currentVirtualTime = rs.getString(1);
	 }
	 logi.logInfo("currentVirtualTime : " + currentVirtualTime)
	 Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
	 Calendar cal1 = Calendar.getInstance();
	 cal1.setTime(date);
	 long totaldays=0;
	 for (int i=billingInterval; i>0; i--)
	 {
	 totaldays = totaldays + cal1.getActualMaximum(Calendar.DAY_OF_MONTH);
	 cal1.add(Calendar.MONTH, -1);
	 }
	 return totaldays;
	 }*/

	def totaldays(String acctNo, int billingInterval) throws SQLException, ParseException
	{
		String Query
		ConnectDB db = new ConnectDB()
		logi.logInfo("Total days updated")
		logi.logInfo("billingInterval : " + billingInterval)
		if(billingInterval == 1) {
			Query = "SELECT LAST_ARREARS_BILL_THRU_DATE from ARIACORE.ACCT where acct_no =" + acctNo
			logi.logInfo("Last_bill date : " + Query)
		} else {
			Query = "SELECT Next_bill_date from ARIACORE.ACCT where acct_no =" + acctNo
			logi.logInfo("Next_bill_date : " + Query)
		}

		ResultSet rs = db.executePlaQuery(Query);

		String nextBillDate = null;
		if(rs.next()) {
			nextBillDate = rs.getString(1);
		}

		logi.logInfo("nextBillDate : " + nextBillDate)
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(nextBillDate);
		Calendar nextcal = Calendar.getInstance();
		Calendar previouscal = Calendar.getInstance();
		nextcal.setTime(date);
		previouscal.setTime(date);
		String negBillInt = "-"+billingInterval
		previouscal.add(Calendar.MONTH, negBillInt.toInteger());
		long totaldays=0;
		logi.logInfo("nextcal : " + nextcal)
		logi.logInfo("previouscal : " + previouscal)
		// Get the difference in millisecs
		long milis1 = nextcal.getTimeInMillis();
		long milis2 = previouscal.getTimeInMillis();

		// Calculate difference in milliseconds
		long diff = milis1 - milis2;
		logi.logInfo("diff : " + diff)
		// Calculate difference in days
		totaldays = (diff / (60 * 60 * 1000 * 24)); // This one has been used for calculating the current day also, because system is designed like that
		logi.logInfo("totaldays : " + totaldays)
		return totaldays;
	}



	def totaldaysASPM(String acctNo, int billingInterval, String suppPlanNo) throws SQLException, ParseException
	{
		String Query
		// logi.logInfo("billingInterval : " + billingInterval)
		if(billingInterval == 1) {
			Query = "SELECT LAST_ARREARS_BILL_THRU_DATE from ARIACORE.ACCT where acct_no =" + acctNo
			//   logi.logInfo("currentVirtualTime : " + Query)
		} else {
			Query = "SELECT Last_bill_date from ARIACORE.ACCT_SUPP_PLAN_MAP where acct_no = " + acctNo + " and supp_plan_no = " +  suppPlanNo
			//	 logi.logInfo("currentVirtualTime : " + Query)
		}
		ConnectDB db = new ConnectDB()
		ResultSet rs = db.executePlaQuery(Query);

		String currentVirtualTime = null;
		if(rs.next()) {
			currentVirtualTime = rs.getString(1);
		}

		// logi.logInfo("currentVirtualTime : " + currentVirtualTime)
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		long totaldays=0;
		for (int i=0; i<billingInterval; i++)
		{
			totaldays = totaldays + cal1.getActualMaximum(Calendar.DAY_OF_MONTH);

			cal1.add(Calendar.MONTH, -1);
		}
		return totaldays;
	}

	def totaldaysASP(String acctNo, int billingInterval) throws SQLException, ParseException
	{
		// Get the current virtual time for the client
		ConnectDB db = new ConnectDB()
		ResultSet rs = db.executePlaQuery("SELECT NEXT_bill_date from ARIACORE.ACCT where acct_no =" + acctNo);

		String currentVirtualTime
		if(rs.next()) {
			currentVirtualTime = rs.getString(1);
		}
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		long totaldays=0;

		if(billingInterval == 1) {
			cal1.add(Calendar.MONTH, -1);
			totaldays = cal1.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		return totaldays;
	}

	String md_invoiceTotalAmount(String testCaseId) {

		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String query = ExpValueConstants.DB_INVOICE_TOTAL_AMOUNT
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		readData.fetchTestData(testId)
		def clientNo = readData.TD_clientNo

		String result ;
		String queryy = db.buildQuery(query, clientNo, getNodeVal(testCaseId, query));

		ResultSet resultSet = db.executePlaQuery(queryy)
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		return result
	}

	def md_VerifyHashTrueDBPwd(String testCaseId) {


		def isSuccessful = 'true'
		String result
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		readData.fetchTestData(testId);

		String query = db.buildQuery(ExpValueConstants.DB_PASSWORD, readData.TD_clientNo, getNodeVal(testCaseId, ExpValueConstants.DB_PASSWORD))

		ResultSet resultSet = db.executePlaQuery(query)
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		if(readData.TD_password.equals(result)) {
			isSuccessful = 'false'
		} else {
			isSuccessful = 'true'
		}

		return isSuccessful
	}

	String md_VerifyQualifierValue_1(String testCaseId) {

		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData readData = new ReadData()
		readData.fetchTestData(testId);
		//		return readData.TD_qualifierName1
		return md_VerifyQualifierValue(testCaseId, readData.TD_qualifierValue1).toString()
	}

	String md_VerifyQualifierValue_2(String testCaseId) {

		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData readData = new ReadData()
		readData.fetchTestData(testId);
		//		return readData.TD_qualifierName1
		return md_VerifyQualifierValue(testCaseId, readData.TD_qualifierValue2).toString()
	}

	String md_VerifyQualifierName_1(String testCaseId) {

		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData readData = new ReadData()
		readData.fetchTestData(testId);
		//		return readData.TD_qualifierName1
		return md_VerifyQualifierName(testCaseId, readData.TD_qualifierName1).toString() 
	}

	String md_VerifyQualifierName_2(String testCaseId) {

		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData readData = new ReadData()
		readData.fetchTestData(testId);
		//		return readData.TD_qualifierName1
		return md_VerifyQualifierName(testCaseId, readData.TD_qualifierName2).toString()
	}

	String md_VerifyQualifierValue(String testCaseId, String qualifierName) {

		String result
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		readData.fetchTestData(testId);
		String accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String clientNo = readData.TD_clientNo
		String Qulifierquery = "SELECT QUALIFIER_VALUE FROM ARIACORE.ACCT_QUALIFIERS WHERE ACCT_NO="+ accountNo + " AND CLIENT_NO=" + clientNo + " AND rec_type = 'ACCT_TRANSACTION' and QUALIFIER_NAME = '" + qualifierName + "'"
		db = new ConnectDB()
		ResultSet resultSet = db.executePlaQuery(Qulifierquery)

		if(resultSet.next()) {
			result = resultSet.getString(1)
		}
		return qualifierName
	}

	String md_VerifyQualifierName(String testCaseId, String qualifierName) {

		String result
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		readData.fetchTestData(testId);
		String accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String clientNo = readData.TD_clientNo
		String Qulifierquery = "SELECT QUALIFIER_NAME FROM ARIACORE.ACCT_QUALIFIERS WHERE ACCT_NO="+ accountNo + " AND CLIENT_NO=" + clientNo + " AND QUALIFIER_NAME = '" + qualifierName + "'"

		ResultSet resultSet = db.executePlaQuery(Qulifierquery)

		if(resultSet.next()) {
			result = resultSet.getString(1)
		}

		return qualifierName
	}
	ArrayList<String> getNodeVal(String testCaseId, String query) {
		boolean assertFlag = true
		def expValue
		def tmp
		ArrayList<String> nodeVals = new ArrayList<String>();
		/*KO*/
		String[] temp
		String strQuery
		/*KO*/

		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		for(int i=0; i<testCaseCmb.size(); i++) {
			com.eviware.soapui.support.XmlHolder holder = xmlValues[i]

			if(testCaseCmb[i].equals(testCaseId)) {
				ArrayList<String> xPathTags = new ArrayList<String>();
				xPathTags = queryNodeXPathTags(query)

				for(int j=0 ; j<xPathTags.size ; j++)
				{
					ReadData rd = new ReadData()
					if(holder.containsKey(rd.getXPath(xPathTags.get(j))) && holder.containsValue(rd.getXPath(xPathTags.get(j)))) {
						nodeVals.add(holder.getNodeValue(rd.getXPath(xPathTags.get(j))));
					} else {
						nodeVals.add("NoVal");
					}

				}
			}
		}
		return nodeVals
	}

	ArrayList<String> queryNodeXPathTags(String query)
	{
		String[] sp = query.split("<");
		ArrayList<String> arrValues = new ArrayList<>();
		for(int i=1 ; i<sp.length ; i++)
		{
			arrValues.add(sp[i].substring(0, sp[i].indexOf(">")));
		}
		return arrValues;
	}

	def getXpathNodeValue(String testCaseId, String xPath) {

		def nodeValue = "NoVal"
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()

		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].equals(testCaseId)) {
				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				if(holder.containsKey(xPath) && holder.containsValue(xPath)) {
					nodeValue = holder.getNodeValue(xPath)
				}
			}
		}
		return nodeValue
	}

	String md_Internal_Tax(String testCaseId) {

		//		return ExpValueConstants.DB_RATE_PER_UNIT_INVSEQ_2''
		return internalTax(testCaseId, ExpValueConstants.DB_INVOICE_TOTAL_AMOUNT_BEFORE_TAX)
	}

	String md_Internal_Tax_ASPM(String testCaseId) {

		//		return ExpValueConstants.DB_RATE_PER_UNIT_INVSEQ_2''
		return internalTax(testCaseId, ExpValueConstants.DB_INVOICE_ASPM_TOTAL_AMOUNT_BEFORE_INTERNAL_TAX)
	}

	String md_External_Tax_Avalara(String testCaseId) {

		//		return ExpValueConstants.DB_RATE_PER_UNIT_INVSEQ_2''
		return externalTaxAvalara(testCaseId, ExpValueConstants.DB_INVOICE_TOTAL_AMOUNT_BEFORE_AVALARA_TAX)
	}

	String md_ASPM_External_Tax_Avalara(String testCaseId) {

		//		return ExpValueConstants.DB_RATE_PER_UNIT_INVSEQ_2''
		return externalTaxAvalara(testCaseId, ExpValueConstants.DB_INVOICE_ASPM_TOTAL_AMOUNT_BEFORE_AVALARA_TAX)
	}

	String md_External_Tax_Billsoft(String testCaseId) {

		//		return ExpValueConstants.DB_RATE_PER_UNIT_INVSEQ_2''
		return externalTaxBillsoft(testCaseId, ExpValueConstants.DB_INVOICE_TOTAL_AMOUNT_BEFORE_BILLSOFT_TAX)
	}

	String md_Internal_Tax_3(String testCaseId) {

		return internalTax(testCaseId, ExpValueConstants.DB_RATE_PER_UNIT_INVSEQ_3)
	}

	//	String internalTax(String testCaseId, String serviceRateQuery) throws SQLException, ParseException
	//	{
	//			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
	//			ReadData readData = new ReadData()
	//			readData.fetchTestData(testId);
	//			def clientNo = readData.TD_clientNo
	//
	//		   String query = "SELECT TAX_RATE FROM ARIACORE.TAX_RATE_VIEW where tax_id = (SELECT tax_id from ARIACORE.TAX_NAMES where client_no = " + clientNo + ")";
	//		   ResultSet rs = db.executePlaQuery(query);
	//		   def taxRate, servicerate;
	//		   if(rs.next()) {
	//				  taxRate = rs.getString(1);
	//		   }
	//
	//		   String serRateQ = db.buildQuery(serviceRateQuery, clientNo, getNodeVal(testCaseId, serviceRateQuery))
	//
	//		   ResultSet resultSet = db.executePlaQuery(serRateQ)
	//		   if (resultSet.next()) {
	//			   servicerate = resultSet.getInt(1)
	//		   }
	//
	//		   double amt1 = servicerate.toDouble() * taxRate.toDouble();
	//		   DecimalFormat df = new DecimalFormat("#");
	//
	//		   return df.format(amt1).toString();
	//	}
	//
	//	String externalTaxAvalara(String testCaseId, String serviceRateQuery) throws SQLException, ParseException
	//	{
	//			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
	//			ReadData readData = new ReadData()
	//			readData.fetchTestData(testId);
	//			def clientNo = readData.TD_clientNo
	//
	//		   //String query = "SELECT TAX_RATE FROM ARIACORE.TAX_RATE_VIEW where tax_id = (SELECT tax_id from ARIACORE.TAX_NAMES where client_no = " + clientNo + ")";
	//		   //ResultSet rs = db.executePlaQuery(query);
	//		   def taxRate = 0.075;
	//		   def servicerate;
	//		   /*if(rs.next()) {
	//				  taxRate = rs.getString(1);
	//		   }*/
	//
	//		   String serRateQ = db.buildQuery(serviceRateQuery, clientNo, getNodeVal(testCaseId, serviceRateQuery))
	//
	//		   ResultSet resultSet = db.executePlaQuery(serRateQ)
	//		   if (resultSet.next()) {
	//			   servicerate = resultSet.getInt(1)
	//		   }
	//
	//		   double amt1 = servicerate.toDouble() * taxRate.toDouble();
	//		   DecimalFormat df = new DecimalFormat("#");
	//
	//		   return df.format(amt1).toString();
	//	}

	String externalTaxBillsoft(String testCaseId, String serviceRateQuery) throws SQLException, ParseException
	{
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo

		//String query = "SELECT TAX_RATE FROM ARIACORE.TAX_RATE_VIEW where tax_id = (SELECT tax_id from ARIACORE.TAX_NAMES where client_no = " + clientNo + ")";
		//ResultSet rs = db.executePlaQuery(query);
		def taxRate = 0.075;
		def servicerate;
		/*if(rs.next()) {
		 taxRate = rs.getString(1);
		 }*/

		String serRateQ = db.buildQuery(serviceRateQuery, clientNo, getNodeVal(testCaseId, serviceRateQuery))

		ResultSet resultSet = db.executePlaQuery(serRateQ)
		if (resultSet.next()) {
			servicerate = resultSet.getInt(1)
		}

		double amt1 = servicerate.toDouble() * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#");

		return df.format(amt1).toString();
	}

	String md_Credit_Reduces_2(String testCaseId) {

		return creditreduces(testCaseId, ExpValueConstants.DB_BILLING_INTERVAL_INVSEQ_2, ExpValueConstants.DB_RATE_PER_UNIT_INVSEQ_2)
	}

	String md_Credit_Reduces_3(String testCaseId) {

		return creditreduces(testCaseId, ExpValueConstants.DB_BILLING_INTERVAL_INVSEQ_3, ExpValueConstants.DB_RATE_PER_UNIT_INVSEQ_3)
	}

	public double creditreduces(String testCaseId, String billingIntvQuery, String serviceRateQuery) throws SQLException, ParseException
	{
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		double servicerate, billingInterval, creditReduces

		String billIntvQ = db.buildQuery(billingIntvQuery, clientNo, getNodeVal(testCaseId, billingIntvQuery))
		String serRateQ = db.buildQuery(serviceRateQuery, clientNo, getNodeVal(testCaseId, serviceRateQuery))
		String creditReducesQuery = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = " + clientNo

		ResultSet resultSet = db.executePlaQuery(billIntvQ)
		if (resultSet.next()) {
			billingInterval = resultSet.getInt(1)
		}
		resultSet = db.executePlaQuery(serRateQ)
		if (resultSet.next()) {
			servicerate = resultSet.getInt(1)
		}
		resultSet = db.executePlaQuery(creditReducesQuery)
		if (resultSet.next()) {
			creditReduces = resultSet.getInt(1)
		}

		//	   double amt = 0.0;
		//	   int value = 1;
		//
		//		int units = 0;
		//		int coupon = 0;
		//	   if(value==creditReduces) //"SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = 3284726"
		//	   {
		//			  amt = ((servicerate*units) - coupon + internalTax(clientNo)- ((servicerate/totaldays(clientNo,billingInterval)) *daystoBill(accountNo)));
		//	   }
		//	   else if (value == 0)
		//			  amt = ((servicerate*units)+ internalTax(clientNo)- coupon - ((servicerate/totaldays(clientNo,billingInterval)) * daystoBill(accountNo)));
		//	   return amt.toString();
	}

	public double extCreditreduces(String testCaseId, String billingIntvQuery, String serviceRateQuery) throws SQLException, ParseException
	{
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		double servicerate, billingInterval, creditReduces

		String billIntvQ = db.buildQuery(billingIntvQuery, clientNo, getNodeVal(testCaseId, billingIntvQuery))
		String serRateQ = db.buildQuery(serviceRateQuery, clientNo, getNodeVal(testCaseId, serviceRateQuery))
		//String creditReducesQuery = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = " + clientNo

		ResultSet resultSet = db.executePlaQuery(billIntvQ)
		if (resultSet.next()) {
			billingInterval = resultSet.getInt(1)
		}
		resultSet = db.executePlaQuery(serRateQ)
		if (resultSet.next()) {
			servicerate = resultSet.getInt(1)
		}
		/*resultSet = db.executePlaQuery(creditReducesQuery)
		 if (resultSet.next()) {
		 creditReduces = resultSet.getInt(1)
		 }*/
	}

	String md_CreditReduces_Tax_Calc(String testCaseId) {

		return creditReducesCal(testCaseId)
	}

	String md_ext_CreditReduces_Tax_Calc(String testCaseId) {

		return extCreditReducesCal(testCaseId)
	}

	String creditReducesCal(String testCaseId) throws SQLException, ParseException
	{
		def amount
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def invoiceAmtBeforeTax = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_INVOICEAMOUNTBEFORETAX"))
		def invoiceCreditAmount = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_INVOICECREDITAMOUNT"))

		if(accountNo != "NoVal" || invoiceAmtBeforeTax != "NoVal" || invoiceCreditAmount != "NoVal") {
			String creditReducesQuery = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = " + clientNo
			String taxQuery = "SELECT TAX_RATE FROM ARIACORE.TAX_RATE_VIEW where tax_id = (SELECT tax_id from ARIACORE.TAX_NAMES where client_no = " + clientNo + ")";
			def tax
			int creditReduces = 0

			ResultSet resultSet = db.executePlaQuery(creditReducesQuery)
			if (resultSet.next()) {
				creditReduces = resultSet.getInt("CREDIT_REDUCES")
			}

			resultSet = db.executePlaQuery(taxQuery)
			if (resultSet.next()) {
				tax = resultSet.getString(1)
			}

			if(creditReduces.equals(1)) {
				amount = (invoiceAmtBeforeTax.toString().toDouble() - invoiceCreditAmount.toString().toDouble()) * tax.toString().toDouble()
			}
			else
			{
				amount = (invoiceAmtBeforeTax.toString().toDouble() * tax.toString().toDouble())
			}
		} else {
			amount = "NoVal"
		}

		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(amount).toString()
	}

	String extCreditReducesCal(String testCaseId) throws SQLException, ParseException
	{
		def amount
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def invoiceAmtBeforeTax = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_INVOICEAMOUNTBEFORETAX"))
		def invoiceCreditAmount = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_INVOICECREDITAMOUNT"))

		if(accountNo != "NoVal" || invoiceAmtBeforeTax != "NoVal" || invoiceCreditAmount != "NoVal") {
			//String creditReducesQuery = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = " + clientNo
			String taxQuery = "SELECT TAX_RATE FROM ARIACORE.TAX_RATE_VIEW where tax_id = (SELECT tax_id from ARIACORE.TAX_NAMES where client_no = " + clientNo + ")";
			def tax
			int creditReduces = 0

			/*ResultSet resultSet = db.executePlaQuery(creditReducesQuery)
			 if (resultSet.next()) {
			 creditReduces = resultSet.getInt("CREDIT_REDUCES")
			 }
			 */
			ResultSet resultSet = db.executePlaQuery(taxQuery)
			if (resultSet.next()) {
				tax = resultSet.getString(1)
			}

			amount = (invoiceAmtBeforeTax.toString().toDouble() - invoiceCreditAmount.toString().toDouble()) * tax.toString().toDouble()

		} else {
			amount = "NoVal"
		}

		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(amount).toString()
	}


	public String checkContractsStatus(int contractTypeNo,String accountNumber,String clientNo){
		try{

			String planNo = null;
			ConnectDB db = new ConnectDB()
			ResultSet rs;
			String contractStatus = null;

			switch(contractTypeNo){
				case 1:

					int planType = 0;

					rs = db.executePlaQuery("SELECT PLAN_NO FROM ARIACORE.ACCT_PLAN_CONTRACTS   where acct_no = ("+accountNumber+") and client_no = ("+clientNo+")");
					if(rs.next()) {
						planNo = rs.getString(1);
					}

					rs = db.executePlaQuery("SELECT SUPP_PLAN_IND from ARIACORE.CLIENT_PLAN where client_no = ("+clientNo+") and plan_no=("+planNo+")");
					if(rs.next()) {
						planType = rs.getInt(1);
					}

					if(planType==0){
						rs = db.executePlaQuery("SELECT STATUS_CD FROM ARIACORE.ACCT_PLAN_CONTRACTS where acct_no = ("+accountNumber+")");
						if(rs.next()) {
							contractStatus = rs.getString(1);
							return contractStatus;
						}
					}else{
						rs = db.executePlaQuery("SELECT STATUS_CD from ARIACORE.CLIENT_PLAN where client_no = ("+clientNo+") and plan_no=("+planNo+")");
						if(rs.next()) {
							contractStatus = rs.getString(1);
							return contractStatus;
						}
					}
					break;
				case 2:

				case 3:
					rs = db.executePlaQuery("SELECT STATUS_CD FROM ARIACORE.ACCT_PLAN_CONTRACTS where acct_no = ("+accountNumber+")");
					if(rs.next()) {
						contractStatus = rs.getString(1);
						return contractStatus;
					}
					break;
				case 4:
					int planRates = 0;
					int acctRates = 1;
					rs = db.executePlaQuery("SELECT PLAN_NO FROM ARIACORE.ACCT_PLAN_CONTRACTS   where acct_no = ("+accountNumber+") and client_no = ("+clientNo+")");
					if(rs.next()) {
						planNo = rs.getString(1);
					}
					rs = db.executePlaQuery("select RATE_SCHEDULE_NO from ARIACORE.ACCT_RATES where acct_no = ("+accountNumber+") and plan_no = ("+planNo+")");
					if(rs.next()) {
						acctRates = rs.getInt(1);
					}

					rs = db.executePlaQuery("select DEFAULT_RATE_SCHED_NO from ARIACORE.CLIENT_PLAN where plan_no = ("+planNo+")");
					if(rs.next()) {
						planRates = rs.getInt(1);
					}

					if(planRates==acctRates){
						rs = db.executePlaQuery("SELECT STATUS_CD FROM ARIACORE.ACCT_PLAN_CONTRACTS where acct_no = ("+accountNumber+")");
						if(rs.next()) {
							contractStatus = rs.getString(1);
							return contractStatus;
						}
					}else{
						return "Account Rate schedule not matched with plan rate schedule";
					}
					break;

				case 5:
					rs = db.executePlaQuery("SELECT STATUS_CD FROM ARIACORE.ACCT_PLAN_CONTRACTS where acct_no = ("+accountNumber+")");
					if(rs.next()) {
						contractStatus = rs.getString(1);
						return contractStatus;
					}
					break;
				default:break;
			}
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}

		return "";

	}

	String md_Do_Proration_Cal_Supp(String testCaseId) {
		int tempCount = 1;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);

		def clientNo = readData.TD_clientNo
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		double servicerate = 5.0;

		ResultSet rs = db.executePlaQuery("SELECT SUPP_PLAN_NO from ARIACORE.ACCT_SUPP_PLAN_MAP where acct_no = (" + accountNo + ")");
		while(rs.next()){
			if(count == tempCount){
				double proratedAmount = ((servicerate/totaldays(clientNo,billingIntervalSuppPlan(rs.getInt(1))))*daystoBillSuppPlan(accountNo,rs.getInt(1))); //client_no, billing interval, acct_no respectively
				count++;
				return proratedAmount.toString();
			}
		}
	}

	String md_Do_Proration_Cal_Supp_ASP_Seq_1(String testCaseId) {

		return md_Do_Proration_Cal_Supp_ASP(testCaseId, "1")
	}

	String md_Do_Proration_Cal_Supp_ASP_Seq_2(String testCaseId) {

		return md_Do_Proration_Cal_Supp_ASP(testCaseId, "2")
	}

	String md_Do_Proration_Cal_Supp_ASP_Seq_3(String testCaseId) {

		return md_Do_Proration_Cal_Supp_ASP(testCaseId, "3")
	}

	String md_Do_Proration_Cal_Supp_ASP(String testCaseId, String seq) {
		int tempCount = 1;
		String ret
		ReadData readData = new ReadData()
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		ConnectDB db = new ConnectDB();

		String billDate
		def clientNo = readData.TD_clientNo
		def suppPlanNo = readData.TD_assign_supp_plan
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def ratePerUnit = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_RATEPERUNIT" + seq))
		double noOfUnits = (getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_LINEBASEUNITS" + seq)).toString()).toDouble()

		if(Integer.parseInt(billingIntervalSuppPlan(suppPlanNo)) == 1) {
			billDate = "SELECT Last_bill_date from ARIACORE.ACCT where acct_no =" + accountNo
		} else {
			billDate = "SELECT Next_bill_date from ARIACORE.ACCT where acct_no =" + accountNo
		}

		// Get the current virtual time for the client
		ResultSet rs = db.executePlaQuery(billDate);

		String currentVirtualTime
		if(rs.next()) {
			currentVirtualTime = rs.getString(1);
		}
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		def totaldays=0;

		if(Integer.parseInt(billingIntervalSuppPlan(suppPlanNo)) == 1) {
			totaldays = cal1.getActualMaximum(Calendar.DAY_OF_MONTH);
		}



		double serviceRate = ratePerUnit.toString().toDouble()
		double daystoBill = daystoBillSuppPlan(accountNo,suppPlanNo).toString().toDouble()
		double proratedAmount = ((serviceRate/totaldays.toDouble()) * daystoBill) * noOfUnits

		DecimalFormat df = new DecimalFormat("#.##");
		ret = df.format(proratedAmount).toString()
		return ret
	}

	String md_Do_Proration_Cal_Supp_ASPM_Seq_1(String testCaseId) {

		return md_Do_Proration_Cal_Supp_ASPM(testCaseId, "1")
	}

	String md_Do_Proration_Cal_Supp_ASPM_Seq_2(String testCaseId) {

		return md_Do_Proration_Cal_Supp_ASPM(testCaseId, "2")
	}

	String md_Do_Proration_Cal_Supp_ASPM_Seq_3(String testCaseId) {

		return md_Do_Proration_Cal_Supp_ASPM(testCaseId, "3")
	}

	String md_Do_Proration_Cal_Supp_ASPM_Seq_4(String testCaseId) {

		return md_Do_Proration_Cal_Supp_ASPM(testCaseId, "4")
	}

	String md_Do_Proration_Cal_Supp_ASPM_Seq_5(String testCaseId) {

		return md_Do_Proration_Cal_Supp_ASPM(testCaseId, "5")
	}

	String md_Do_Proration_Cal_Supp_ASPM_Seq_6(String testCaseId) {

		return md_Do_Proration_Cal_Supp_ASPM(testCaseId, "6")
	}

	String md_Do_Proration_Cal_Supp_ASPM_Seq_7(String testCaseId) {

		return md_Do_Proration_Cal_Supp_ASPM(testCaseId, "7")
	}

	String md_Do_Proration_Cal_Supp_ASPM_Seq_8(String testCaseId) {

		return md_Do_Proration_Cal_Supp_ASPM(testCaseId, "8")
	}

	String md_Do_Proration_Cal_Supp_ASPM_Seq_9(String testCaseId) {

		return md_Do_Proration_Cal_Supp_ASPM(testCaseId, "9")
	}

	String md_Do_Proration_Cal_Supp_Mod(String testCaseId, String seq) {
		int tempCount = 1;
		String ret
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);

		String billDate
		def clientNo = readData.TD_clientNo
		def suppPlanNo = readData.TD_modifySuppPlan
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def ratePerUnit = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_RATEPERUNIT" + seq))
		double noOfUnits = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_LINEBASEUNITS" + seq)).toString().toDouble()

		if(Integer.parseInt(billingIntervalSuppPlan(suppPlanNo)) == 1) {
			billDate = "SELECT Last_bill_date from ARIACORE.ACCT where acct_no =" + accountNo
		} else {
			billDate = "SELECT Next_bill_date from ARIACORE.ACCT where acct_no =" + accountNo
		}

		// Get the current virtual time for the client
		ResultSet rs = db.executePlaQuery(billDate);

		String currentVirtualTime
		if(rs.next()) {
			currentVirtualTime = rs.getString(1);
		}
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		def totaldays=0;

		if(Integer.parseInt(billingIntervalSuppPlan(suppPlanNo)) == 1) {
			totaldays = cal1.getActualMaximum(Calendar.DAY_OF_MONTH);
		}



		double serviceRate = ratePerUnit.toString().toDouble()
		double daystoBill = daystoBillSuppPlan(accountNo,suppPlanNo).toString().toDouble()
		double proratedAmount = ((serviceRate/totaldays.toDouble()) * daystoBill) * noOfUnits

		DecimalFormat df = new DecimalFormat("#.##");
		ret = df.format(proratedAmount).toString()
		return ret
	}

	def daystoBillSuppPlan(String acctNo,String suppPlanNo) throws SQLException, ParseException {

		boolean isSuccessful = false;
		ConnectDB db = new ConnectDB();
		ResultSet rs = db.executePlaQuery("SELECT SUPP_PLAN_NO,Next_bill_date, Last_bill_date from ARIACORE.ACCT_SUPP_PLAN_MAP where acct_no = (" + acctNo + ") and supp_plan_no = ("+suppPlanNo+")");

		String nextBillDay = null;
		String lastBillDay = null;
		if(rs.next()) {
			nextBillDay = rs.getString(2);
			lastBillDay = rs.getString(3);
		}

		// Get the Next Bill day
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(nextBillDay);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);


		//  Get the Last bill day
		Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(lastBillDay);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date1);


		// Get the difference in millisecs
		long milis1 = cal1.getTimeInMillis();
		long milis2 = cal2.getTimeInMillis();

		// Calculate difference in milliseconds
		long diff = milis1 - milis2;

		// Calculate difference in days
		long daysToBill = (diff / (60 * 60 * 1000 * 24))+1; // This one has been used for calculating the current day also, because system is designed like that

		return daysToBill;
	}

	String billingIntervalSuppPlan(String planNo) {
		int result ;
		ConnectDB db = new ConnectDB();
		ResultSet resultSet = db.executePlaQuery("select billing_interval from ARIACORE.CLIENT_PLAN where plan_no = ("+planNo+")");
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_account_planNo(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);

		def accountNo = readData.TD_acctNo
		ResultSet resultSet = db.executePlaQuery("SELECT PLAN_NO FROM ARIACORE.ACCT_ where acct_no = ("+accountNo+")");
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}





	String md_Get_TotalRowItems(String testCaseId) {
		String result;
		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		int i= 1;
		def clientNo = readData.TD_clientNo
		String noRowWith = getXpathNodeValue(testCaseId, readData.getXPath("GET_CLIENT_PLANS_ALL_NO_ROW"))
		result = noRowWith.substring(noRowWith.indexOf("[") + 1, noRowWith.lastIndexOf("]"))


		return result
	}

	String md_ACCT_SUPP_PLAN_STATUS(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB();
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT STATUS_CD FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE SUPP_PLAN_NO=" + planNo + "AND ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_ACCT_ASPM_SUPP_PLAN_STATUS_1(String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def suppPlan = readData.TD_assign_supp_plan_1
		return md_ACCT_ASPM_SUPP_PLAN_STATUS(testCaseId, suppPlan)
	}

	String md_ACCT_ASPM_SUPP_PLAN_STATUS_2(String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def suppPlan = readData.TD_assign_supp_plan_2
		return md_ACCT_ASPM_SUPP_PLAN_STATUS(testCaseId, suppPlan)
	}

	String md_ACCT_ASPM_SUPP_PLAN_STATUS_3(String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def suppPlan = readData.TD_assign_supp_plan_3
		return md_ACCT_ASPM_SUPP_PLAN_STATUS(testCaseId, suppPlan)
	}

	String md_ACCT_ASPM_SUPP_PLAN_STATUS_4(String testCaseId) {

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);
		def suppPlan = readData.TD_assign_supp_plan_4
		return md_ACCT_ASPM_SUPP_PLAN_STATUS(testCaseId, suppPlan)
	}

	String md_ACCT_ASPM_SUPP_PLAN_STATUS(String testCaseId, String planNo) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan_multi", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT STATUS_CD FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE SUPP_PLAN_NO=" + planNo + " AND ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}
		return result
	}

	String md_VERIFY_SUPP_PLAN_EVENT_110(String testCaseId) {
		return md_VERIFY_SUPP_PLAN_EVENT(testCaseId, "110")
	}

	String md_VERIFY_SUPP_PLAN_EVENT(String testCaseId, String event) {

		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB();
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		String Query = "SELECT EVENT_ID FROM ARIACORE.ACCT_PROV2_EVENTS WHERE PROV_SEQ IN (SELECT PROV_SEQ FROM ARIACORE.ACCT_PROV2_STEPS  WHERE ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo + ") and EVENT_ID = "+ event + " GROUP BY EVENT_ID"
		ResultSet resultSet = db.executePlaQuery(Query);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result

	}

	String md_VERIFY_ASPM_SUPP_PLAN_EVENT_110(String testCaseId) {
		return md_VERIFY_ASPM_SUPP_PLAN_EVENT(testCaseId, "110")
	}

	String md_VERIFY_ASPM_SUPP_PLAN_EVENT(String testCaseId, String event) {

		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB();
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan_multi", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def clientNo = readData.TD_clientNo
		String Query = "SELECT EVENT_ID FROM ARIACORE.ACCT_PROV2_EVENTS WHERE PROV_SEQ IN (SELECT PROV_SEQ FROM ARIACORE.ACCT_PROV2_STEPS  WHERE ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo + ") and EVENT_ID = "+ event + " GROUP BY EVENT_ID"
		ResultSet resultSet = db.executePlaQuery(Query);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_VERIFY_ASPM_ACCT_BAL_110(String testCaseId) {
		return md_VERIFY_ASPM_ACCT_BAL(testCaseId, "110")
	}

	String md_VERIFY_ASPM_ACCT_BAL(String testCaseId, String event) {

		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB();
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan_multi", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def clientNo = readData.TD_clientNo
		String Query = "SELECT BALANCE FROM ARIACORE.ACCT_BALANCE WHERE ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo
		ResultSet resultSet = db.executePlaQuery(Query);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}
		return result
	}


	String md_SUPPPLAN_2(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		//String assignSuppPlantestId = testCaseId
		//String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def acctNo = readData.TD_acctNo
		//def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SUPP_PLAN_NO FROM ( SELECT ROW_NUMBER() OVER (ORDER BY SUPP_PLAN_NO ASC) AS rownumber,SUPP_PLAN_NO FROM ARIACORE.PLAN_SUPP_PLAN_MAP where master_plan_no in (SELECT PLAN_NO FROM ARIACORE.ACCT where acct_no = "+acctNo+" and client_no = "+clientNo+")and client_no = "+clientNo+") where rownumber=2")
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}
	String md_SUPPPLAN_3(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		//String assignSuppPlantestId = testCaseId
		//String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def acctNo = readData.TD_acctNo
		//def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SCHEDULE_NO FROM (SELECT ROW_NUMBER() OVER (ORDER BY SCHEDULE_NO ASC) AS rownumber,SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP where plan_no IN (SELECT SUPP_PLAN_NO FROM ( SELECT ROW_NUMBER() OVER (ORDER BY SUPP_PLAN_NO ASC) AS rownumber,SUPP_PLAN_NO FROM ARIACORE.PLAN_SUPP_PLAN_MAP where master_plan_no in (SELECT PLAN_NO FROM ARIACORE.ACCT where acct_no = "+acctNo+" and client_no = "+clientNo+")and client_no = "+clientNo+") where rownumber=1)and client_no = "+clientNo+" AND SCHEDULE_NO!=-1 order by SCHEDULE_NO) where rownumber=1")
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}
	String md_SUPPPLAN_4(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		//String assignSuppPlantestId = testCaseId
		//String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def acctNo = readData.TD_acctNo
		//def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SCHEDULE_NO FROM (SELECT ROW_NUMBER() OVER (ORDER BY SCHEDULE_NO ASC) AS rownumber,SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP where plan_no IN (SELECT SUPP_PLAN_NO FROM ( SELECT ROW_NUMBER() OVER (ORDER BY SUPP_PLAN_NO ASC) AS rownumber,SUPP_PLAN_NO FROM ARIACORE.PLAN_SUPP_PLAN_MAP where master_plan_no in (SELECT PLAN_NO FROM ARIACORE.ACCT where acct_no = "+acctNo+" and client_no = "+clientNo+")and client_no = "+clientNo+") where rownumber=1)and client_no = "+clientNo+" AND SCHEDULE_NO!=-1 order by SCHEDULE_NO) where rownumber=2")
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}
	String md_SUPPPLAN_1(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		//String assignSuppPlantestId = testCaseId
		//String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def acctNo = readData.TD_acctNo
		//def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SUPP_PLAN_NO FROM ( SELECT ROW_NUMBER() OVER (ORDER BY SUPP_PLAN_NO ASC) AS rownumber,SUPP_PLAN_NO FROM ARIACORE.PLAN_SUPP_PLAN_MAP where master_plan_no in (SELECT PLAN_NO FROM ARIACORE.ACCT where acct_no = "+acctNo+" and client_no = "+clientNo+")and client_no = "+clientNo+") where rownumber=1")
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}
	String md_SUPPPLAN_5(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		//String assignSuppPlantestId = testCaseId
		//String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def acctNo = readData.TD_acctNo
		//def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SCHEDULE_NO FROM (SELECT ROW_NUMBER() OVER (ORDER BY SCHEDULE_NO ASC) AS rownumber,SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP where plan_no IN (SELECT SUPP_PLAN_NO FROM ( SELECT ROW_NUMBER() OVER (ORDER BY SUPP_PLAN_NO ASC) AS rownumber,SUPP_PLAN_NO FROM ARIACORE.PLAN_SUPP_PLAN_MAP where master_plan_no in (SELECT PLAN_NO FROM ARIACORE.ACCT where acct_no = "+acctNo+" and client_no = "+clientNo+")and client_no = "+clientNo+") where rownumber=1)and client_no = "+clientNo+" AND SCHEDULE_NO!=-1 order by SCHEDULE_NO) where rownumber=3")
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}
	String md_SUPPPLAN_6(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		//String assignSuppPlantestId = testCaseId
		//String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def acctNo = readData.TD_acctNo
		//def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SCHEDULE_NO FROM (SELECT ROW_NUMBER() OVER (ORDER BY SCHEDULE_NO ASC) AS rownumber,SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP where plan_no IN (SELECT SUPP_PLAN_NO FROM ( SELECT ROW_NUMBER() OVER (ORDER BY SUPP_PLAN_NO ASC) AS rownumber,SUPP_PLAN_NO FROM ARIACORE.PLAN_SUPP_PLAN_MAP where master_plan_no in (SELECT PLAN_NO FROM ARIACORE.ACCT where acct_no = "+acctNo+" and client_no = "+clientNo+")and client_no = "+clientNo+") where rownumber=2)and client_no = "+clientNo+" AND SCHEDULE_NO!=-1 order by SCHEDULE_NO) where rownumber=1")
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}
	String md_SUPPPLAN_7(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		//String assignSuppPlantestId = testCaseId
		//String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def acctNo = readData.TD_acctNo
		//def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SCHEDULE_NO FROM (SELECT ROW_NUMBER() OVER (ORDER BY SCHEDULE_NO ASC) AS rownumber,SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP where plan_no IN (SELECT SUPP_PLAN_NO FROM ( SELECT ROW_NUMBER() OVER (ORDER BY SUPP_PLAN_NO ASC) AS rownumber,SUPP_PLAN_NO FROM ARIACORE.PLAN_SUPP_PLAN_MAP where master_plan_no in (SELECT PLAN_NO FROM ARIACORE.ACCT where acct_no = "+acctNo+" and client_no = "+clientNo+")and client_no = "+clientNo+") where rownumber=2)and client_no = "+clientNo+" AND SCHEDULE_NO!=-1 order by SCHEDULE_NO) where rownumber=2")
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}
	String md_SUPPPLAN_8(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		//String assignSuppPlantestId = testCaseId
		//String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def acctNo = readData.TD_acctNo
		//def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SCHEDULE_NO FROM (SELECT ROW_NUMBER() OVER (ORDER BY SCHEDULE_NO ASC) AS rownumber,SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP where plan_no IN (SELECT SUPP_PLAN_NO FROM ( SELECT ROW_NUMBER() OVER (ORDER BY SUPP_PLAN_NO ASC) AS rownumber,SUPP_PLAN_NO FROM ARIACORE.PLAN_SUPP_PLAN_MAP where master_plan_no in (SELECT PLAN_NO FROM ARIACORE.ACCT where acct_no = "+acctNo+" and client_no = "+clientNo+")and client_no = "+clientNo+") where rownumber=2)and client_no = "+clientNo+" AND SCHEDULE_NO!=-1 order by SCHEDULE_NO) where rownumber=3")
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_proration_credit(String testCaseId) {
		String result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("cancel_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def clientNo = readData.TD_clientNo
		def planNo = readData.TD_cancelSuppPlan
		db = new ConnectDB()
		ResultSet resultSet = db.executePlaQuery("select SUM(amount) from ariacore.credits where acct_no = (" + accountNo + ") AND CLIENT_NO = (" +clientNo + ")");
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		return "-"+result
	}
	String md_account_plan_status(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("cancel_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def planNo = readData.TD_cancelSuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		db = new ConnectDB()
		ResultSet resultSet = db.executePlaQuery("SELECT STATUS_CD FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE SUPP_PLAN_NO=" + planNo + "AND ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}
	String md_prorate_credit(String testCaseId) {
		String result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("cancel_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def clientNo = readData.TD_clientNo
		def planNo = readData.TD_cancelSuppPlan
		ResultSet resultSet = db.executePlaQuery("select SUM(amount) from ariacore.credits where acct_no = (" + accountNo + ") AND CLIENT_NO = (" +clientNo + ")");
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		return "-"+result
	}







	String md_VERIFY_SUPP_PLAN_EVENT_112(String testCaseId) {
		return md_VERIFY_SUPP_PLAN_EVENT1(testCaseId, "112")
	}



	String md_VERIFY_SUPP_PLAN_EVENT1(String testCaseId, String event) {

		int result ;

		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("cancel_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def planNo = readData.TD_cancelSuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		String Query = "SELECT EVENT_ID FROM ARIACORE.ACCT_PROV2_EVENTS WHERE PROV_SEQ IN (SELECT PROV_SEQ FROM ARIACORE.ACCT_PROV2_STEPS  WHERE ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo + ") and EVENT_ID = "+ event + " GROUP BY EVENT_ID"
		db = new ConnectDB()
		ResultSet resultSet = db.executePlaQuery(Query);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}
		return result
	}


	String md_void_invoice_status_asp(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String modifySuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);
		ConnectDB db = new ConnectDB();

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		//def planNo = readData.TD_cancelSuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT TRANSACTION_TYPE FROM ARIACORE.ACCT_TRANSACTION where ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo +" and transaction_type = -3  or transaction_type = -1");
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		if(result==-3||result==-1)
			return "TRUE"
		else
			return "FALSE"

	}

	//	String md_Get_Act_Fee(String testCaseId)
	//	{
	//		int sum = 0;
	//		try{
	//			ReadData readData = new ReadData()
	//			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
	//			String modifySuppPlantestId = testCaseId
	//			readData.fetchTestData(testId);
	//
	//			String client_No = readData.TD_clientNo
	//			String plan_NO = readData.TD_assign_supp_plan
	//			String units = readData.TD_num_plan_units_sp
	//			Query= "select * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in (select default_rate_sched_no from ARIACORE.CLIENT_PLAN where plan_no=  "+plan_NO+" AND client_no="+client_No+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no= "+plan_NO+") and client_no="+client_No+" and service_type = 'AC') and client_no="+client_No+"";
	//			ResultSet resultSet = db.executeQuery(Query);
	//
	//			while(resultSet.next()){
	//				sum = sum + resultSet.getInt("RATE_PER_UNIT");
	//			}
	//		}
	//		catch(Exception e){
	//
	//		}
	//		return Integer.toString(sum);
	//	}

	//	String md_Get_Rec_Fee_Monthly(String testCaseId)
	//	{
	//		int sum = 0;
	//		String Query1 = null;
	//		try{
	//			ReadData readData = new ReadData()
	//			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
	//			String modifySuppPlantestId = testCaseId
	//			String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
	//			readData.fetchTestData(testId);
	//			def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
	//
	//			String client_No = readData.TD_clientNo
	//			String plan_NO = readData.TD_assign_supp_plan
	//			String units = readData.TD_num_plan_units_sp
	//			String Query= "select * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in (select default_rate_sched_no from ARIACORE.CLIENT_PLAN where plan_no= "+plan_NO+" AND client_no="+client_No+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no="+plan_NO+" and TIERED_PRICING_RULE_NO = 3) and client_no="+client_No+" and service_type = 'RC') and to_unit >="+units+" and from_unit<"+units+" and client_no="+client_No+"";
	//			ResultSet resultSet = db.executeQuery(Query);
	//
	//			while(resultSet.next()){
	//				sum = sum + resultSet.getInt("RATE_PER_UNIT")
	//			}
	//
	//			Query1= "SELECT * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in (select default_rate_sched_no from ARIACORE.CLIENT_PLAN where plan_no= "+plan_NO+" AND client_no="+client_No+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no="+plan_NO+" and TIERED_PRICING_RULE_NO != 3) and client_no="+client_No+" and service_type = 'RC') and to_unit >="+units+" and from_unit<"+units+" and client_no="+client_No+"";
	//			ResultSet resultSet1 = db.executeQuery(Query1);
	//
	//			while(resultSet1.next()){
	//				sum = sum + (resultSet1.getInt("RATE_PER_UNIT") * Integer.parseInt(units))
	//			}
	//		}
	//		catch(Exception e){
	//
	//		}
	//		return Integer.toString(sum);
	//	}
	//
	//	String md_Get_Rec_Fee_Anually(String testCaseId)
	//	{
	//		int sum = 0;
	//		try{
	//			ReadData readData = new ReadData()
	//			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
	//			String modifySuppPlantestId = testCaseId
	//			String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
	//			readData.fetchTestData(testId);
	//			def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
	//
	//			String client_No = readData.TD_clientNo
	//			String plan_NO = readData.TD_assign_supp_plan
	//			String units = readData.TD_num_plan_units_sp
	//			String Query= "select * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in (select default_rate_sched_no from ARIACORE.CLIENT_PLAN where plan_no= "+plan_NO+" AND client_no="+client_No+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no="+plan_NO+" and TIERED_PRICING_RULE_NO = 3) and client_no="+client_No+" and service_type = 'RC') and to_unit >="+units+" and from_unit<"+units+" and client_no="+client_No+""
	//			ResultSet resultSet = db.executeQuery(Query);
	//
	//			while(resultSet.next()){
	//				sum = sum + resultSet.getInt("RATE_PER_UNIT")
	//			}
	//
	//			String Query1= "select * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in (select default_rate_sched_no from ARIACORE.CLIENT_PLAN where plan_no= "+plan_NO+" AND client_no="+client_No+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no="+plan_NO+" and TIERED_PRICING_RULE_NO != 3) and client_no="+client_No+" and service_type = 'RC') and to_unit >="+units+" and from_unit<"+units+" and client_no="+client_No+""
	//			ResultSet resultSet1 = db.executeQuery(Query1);
	//
	//			while(resultSet1.next()){
	//				sum = sum + (resultSet1.getInt("RATE_PER_UNIT")*Integer.parseInt(units))
	//			}
	//			sum = sum * 12
	//		}
	//		catch(Exception e){
	//
	//		}
	//		return Integer.toString(sum);
	//	}

	String md_Get_Rec_Fee_Monthly_After_Invoice_Gen(String testCaseId)
	{
		int sum = 0;
		try{
			ReadData readData = new ReadData()
			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
			String modifySuppPlantestId = testCaseId
			String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
			readData.fetchTestData(testId);
			def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
			ConnectDB db = new ConnectDB();
			String client_No = readData.TD_clientNo
			String plan_NO = readData.TD_assign_supp_plan
			String units = readData.TD_num_plan_units_sp
			String Query= "SELECT USAGE_UNITS,USAGE_RATE FROM ARIACORE.GL_DETAIL  WHERE INVOICE_NO IN (SELECT INVOICE_NO FROM (SELECT ROW_NUMBER() OVER (order by invoice_no DESC) AS rownumber,INVOICE_NO FROM ARIACORE.GL Where ACCT_NO = "+accountNo+" and client_no = "+client_No+") WHERE ROWNUMBER = 1) AND PLAN_NO= "+plan_NO+" AND CLIENT_NO="+client_No+"";

			ResultSet resultSet = db.executeQuery(Query);


			while(resultSet.next()){
				sum = sum +( resultSet.getInt("USAGE_UNITS") * resultSet.getInt("USAGE_RATE"));
			}
		}
		catch(Exception e){

		}
		return Integer.toString(sum);
	}

	String md_Get_Rec_Fee_Anually_After_Invoice_Gen(String testCaseId)
	{
		int sum = 0;
		try{
			ReadData readData = new ReadData()
			ConnectDB db = new ConnectDB()
			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
			String modifySuppPlantestId = testCaseId
			String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
			readData.fetchTestData(testId);
			def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

			String client_No = readData.TD_clientNo
			String plan_NO = readData.TD_assign_supp_plan
			String units = readData.TD_num_plan_units_sp
			String Query= "SELECT USAGE_UNITS,USAGE_RATE FROM ARIACORE.GL_DETAIL  WHERE INVOICE_NO IN (SELECT INVOICE_NO FROM (SELECT ROW_NUMBER() OVER (order by invoice_no DESC) AS rownumber,INVOICE_NO FROM ARIACORE.GL Where ACCT_NO = "+accountNo+" and client_no = "+client_No+") WHERE ROWNUMBER = 1) AND PLAN_NO= "+plan_NO+" AND CLIENT_NO="+client_No+"";
			ResultSet resultSet = db.executeQuery(Query);


			while(resultSet.next()){
				sum = sum +( resultSet.getInt("USAGE_UNITS")*resultSet.getInt("USAGE_RATE"));
			}

			sum = sum * 12;
		}
		catch(Exception e){

		}
		return Integer.toString(sum);

	}

	String md_Get_Act_Fee_MOD(String testCaseId)
	{
		int sum = 0;
		try{
			ReadData readData = new ReadData()
			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
			String modifySuppPlantestId = testCaseId
			readData.fetchTestData(testId);
			ConnectDB db = new ConnectDB();

			String client_No = readData.TD_clientNo
			String plan_NO = readData.TD_modifySuppPlan
			String units = readData.TD_noOfUnits
			Query= "select * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in (select default_rate_sched_no from ARIACORE.CLIENT_PLAN where plan_no=  "+plan_NO+" AND client_no="+client_No+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no= "+plan_NO+") and client_no="+client_No+" and service_type = 'AC') and client_no="+client_No+"";
			ResultSet resultSet = db.executeQuery(Query);

			while(resultSet.next()){
				sum = sum + resultSet.getInt("RATE_PER_UNIT");
			}
		}
		catch(Exception e){

		}
		return Integer.toString(sum);
	}

	String md_Get_Rec_Fee_Monthly_MOD(String testCaseId)
	{
		int sum = 0;
		String Query1 = null;
		try{
			ReadData readData = new ReadData()
			ConnectDB db = new ConnectDB()
			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
			String modifySuppPlantestId = testCaseId
			String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")
			readData.fetchTestData(testId);
			def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

			String client_No = readData.TD_clientNo
			String plan_NO = readData.TD_modifySuppPlan
			String units = readData.TD_noOfUnits
			String Query= "select * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in (select default_rate_sched_no from ARIACORE.CLIENT_PLAN where plan_no= "+plan_NO+" AND client_no="+client_No+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no="+plan_NO+" and TIERED_PRICING_RULE_NO = 3) and client_no="+client_No+" and service_type = 'RC') and to_unit >="+units+" and from_unit<"+units+" and client_no="+client_No+"";
			ResultSet resultSet = db.executeQuery(Query);

			while(resultSet.next()){
				sum = sum + resultSet.getInt("RATE_PER_UNIT")
			}

			Query1= "SELECT * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in (select default_rate_sched_no from ARIACORE.CLIENT_PLAN where plan_no= "+plan_NO+" AND client_no="+client_No+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no="+plan_NO+" and TIERED_PRICING_RULE_NO != 3) and client_no="+client_No+" and service_type = 'RC') and to_unit >="+units+" and from_unit<"+units+" and client_no="+client_No+"";
			ResultSet resultSet1 = db.executeQuery(Query1);

			while(resultSet1.next()){
				sum = sum + (resultSet1.getInt("RATE_PER_UNIT") * Integer.parseInt(units))
			}
		}
		catch(Exception e){

		}
		return Integer.toString(sum);
	}

	String md_Get_Rec_Fee_Anually_MOD(String testCaseId)
	{
		int sum = 0;
		try{
			ReadData readData = new ReadData()
			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
			String modifySuppPlantestId = testCaseId
			String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")
			readData.fetchTestData(testId);
			def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

			ConnectDB db = new ConnectDB();
			String client_No = readData.TD_clientNo
			String plan_NO = readData.TD_modifySuppPlan
			String units = readData.TD_noOfUnits
			String Query= "select * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in (select default_rate_sched_no from ARIACORE.CLIENT_PLAN where plan_no= "+plan_NO+" AND client_no="+client_No+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no="+plan_NO+" and TIERED_PRICING_RULE_NO = 3) and client_no="+client_No+" and service_type = 'RC') and to_unit >="+units+" and from_unit<"+units+" and client_no="+client_No+""
			ResultSet resultSet = db.executeQuery(Query);

			while(resultSet.next()){
				sum = sum + resultSet.getInt("RATE_PER_UNIT")
			}

			String Query1= "select * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in (select default_rate_sched_no from ARIACORE.CLIENT_PLAN where plan_no= "+plan_NO+" AND client_no="+client_No+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no="+plan_NO+" and TIERED_PRICING_RULE_NO != 3) and client_no="+client_No+" and service_type = 'RC') and to_unit >="+units+" and from_unit<"+units+" and client_no="+client_No+""
			ResultSet resultSet1 = db.executeQuery(Query1);

			while(resultSet1.next()){
				sum = sum + (resultSet1.getInt("RATE_PER_UNIT")*Integer.parseInt(units))
			}
			sum = sum * 12
		}
		catch(Exception e){

		}
		return Integer.toString(sum);
	}

	String md_Get_Rec_Fee_Monthly_After_Invoice_Gen_MOD(String testCaseId)
	{
		int sum = 0;
		try{
			ReadData readData = new ReadData()
			ConnectDB db = new ConnectDB()
			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
			String modifySuppPlantestId = testCaseId
			String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")
			readData.fetchTestData(testId);
			def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

			String client_No = readData.TD_clientNo
			String plan_NO = readData.TD_modifySuppPlan
			String units = readData.TD_noOfUnits
			String Query= "SELECT USAGE_UNITS,USAGE_RATE FROM ARIACORE.GL_DETAIL  WHERE INVOICE_NO IN (SELECT INVOICE_NO FROM (SELECT ROW_NUMBER() OVER (order by invoice_no DESC) AS rownumber,INVOICE_NO FROM ARIACORE.GL Where ACCT_NO = "+accountNo+" and client_no = "+client_No+") WHERE ROWNUMBER = 1) AND PLAN_NO= "+plan_NO+" AND CLIENT_NO="+client_No+"";

			ResultSet resultSet = db.executeQuery(Query);


			while(resultSet.next()){
				sum = sum +( resultSet.getInt("USAGE_UNITS") * resultSet.getInt("USAGE_RATE"));
			}
		}
		catch(Exception e){

		}
		return Integer.toString(sum);
	}

	String md_Get_Rec_Fee_Anually_After_Invoice_Gen_MOD(String testCaseId)
	{
		int sum = 0;
		try{
			ReadData readData = new ReadData()
			ConnectDB db = new ConnectDB()
			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
			String modifySuppPlantestId = testCaseId
			String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")
			readData.fetchTestData(testId);
			def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

			String client_No = readData.TD_clientNo
			String plan_NO = readData.TD_modifySuppPlan
			String units = readData.TD_noOfUnits
			String Query= "SELECT USAGE_UNITS,USAGE_RATE FROM ARIACORE.GL_DETAIL  WHERE INVOICE_NO IN (SELECT INVOICE_NO FROM (SELECT ROW_NUMBER() OVER (order by invoice_no DESC) AS rownumber,INVOICE_NO FROM ARIACORE.GL Where ACCT_NO = "+accountNo+" and client_no = "+client_No+") WHERE ROWNUMBER = 1) AND PLAN_NO= "+plan_NO+" AND CLIENT_NO="+client_No+"";
			ResultSet resultSet = db.executeQuery(Query);


			while(resultSet.next()){
				sum = sum +( resultSet.getInt("USAGE_UNITS")*resultSet.getInt("USAGE_RATE"));
			}

			sum = sum * 12;
		}
		catch(Exception e){

		}
		return Integer.toString(sum);

	}

	String md_ACCT_SUPP_PLAN_RECURRING_FACTOR(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);
		ConnectDB db = new ConnectDB();

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE SUPP_PLAN_NO=" + planNo + "AND ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_SUPP_PLAN_PRORATION_RESULT_TAX1(String testCaseId){
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def invoiceNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("ASSIGN_SUPP_PLAN_INVOICE_NO1"))
		def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT sum(debit) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = "+ invoiceNo+" AND CLIENT_NO=" + clientNo);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_VERIFY_SUPP_PLAN_EVENT2(String testCaseId, String event) {

		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def planNo = readData.TD_modifySuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		String Query = "SELECT EVENT_ID FROM ARIACORE.ACCT_PROV2_EVENTS WHERE PROV_SEQ IN (SELECT PROV_SEQ FROM ARIACORE.ACCT_PROV2_STEPS  WHERE ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo + ") and EVENT_ID = "+ event + " GROUP BY EVENT_ID"
		ResultSet resultSet = db.executePlaQuery(Query);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result

	}

	String md_ACCT_SUPP_PLAN_STATUS_MOD(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def planNo = readData.TD_modifySuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT STATUS_CD FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE SUPP_PLAN_NO=" + planNo + "AND ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_ACCT_SUPP_PLAN_MOD_UNITS(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def planNo = readData.TD_modifySuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE SUPP_PLAN_NO=" + planNo + "AND ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_ACCT_SUPP_PLAN_MOD_Q_UNITS(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def planNo = readData.TD_modifySuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT QUEUED_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE SUPP_PLAN_NO=" + planNo + "AND ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_MODD_SUPP_PLAN_CREDITS(String testCaseId) {
		String result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def invoiceNo = getXpathNodeValue(testCaseId, readData.getXPath("MODIFY_SUPP_PLAN_INVOICENO1"))
		def planNo = readData.TD_modifySuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL_ Where INVOICE_NO =" + invoiceNo + "and (orig_coupon_cd is not null or orig_credit_id is not null)");
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		String temp = result.replaceAll("-", "")

		return temp
	}


	String md_VERIFY_SUPP_PLAN_EVENT_114(String testCaseId) {
		return md_VERIFY_SUPP_PLAN_EVENT2(testCaseId, "114")
	}

	String md_VERIFY_SUPP_PLAN_EVENT_115(String testCaseId) {
		return md_VERIFY_SUPP_PLAN_EVENT2(testCaseId, "115")
	}

	String md_prorate_credit_mod(String testCaseId) {
		String result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def clientNo = readData.TD_clientNo
		//def planNo = readData.TD_cancelSuppPlan
		ResultSet resultSet = db.executePlaQuery("select SUM(amount) from ariacore.credits where acct_no = (" + accountNo + ") AND CLIENT_NO = (" +clientNo + ")");
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		return result
	}

	String md_INVOICE_LINE_AMT_SEQ_1(String testCaseId) {
		String result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def units = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("MODIFY_SUPP_PLAN_INVOICEUNITS1"))
		def serviceNo = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("MODIFY_SUPP_PLAN_INVOICESERVICENO1"))
		def clientNo = readData.TD_clientNo
		def planNo = readData.TD_modifySuppPlan
		ResultSet resultSet = db.executePlaQuery("SELECT RATE_PER_UNIT * (" + units + ") FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SERVICE_NO = (" + serviceNo + ")  AND SCHEDULE_NO = (SELECT RATE_SCHEDULE_NO FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = (" + accountNo + ") AND PLAN_NO= (" + planNo + ") AND SERVICE_NO= (" + serviceNo + ") AND RATE_SEQ_NO=1 AND CLIENT_NO=(" +clientNo + ")) AND RATE_SEQ_NO = 1 AND CLIENT_NO=(" +clientNo + ")");
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		return result
	}

	String md_INVOICE_TOTAL_AMOUNT_BEFORE_TAX_ASP1_CREDITS(String testCaseId){
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def invoiceNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("ASSIGN_SUPP_PLAN_INVOICE_NO1"))
		def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT sum(debit) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = "+ invoiceNo+" AND SERVICE_NO NOT IN(401,402,405) AND CLIENT_NO=" + clientNo+" and orig_coupon_cd is NULL");
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}


	String md_STATE_ASP1(String testCaseId){
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def invoiceNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("ASSIGN_SUPP_PLAN_INVOICE_NO1"))
		def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT sum(debit) from ARIACORE.GL_DETAIL WHERE INVOICE_NO =  "+ invoiceNo+"  AND SERVICE_NO IN(401,402,405) AND CLIENT_NO=" + clientNo);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_TOTALAMOUNT_AFTER_TAX_AND_CREDIT_2(String testCaseId) {

		return md_Coupon_Discount(testCaseId, "2")
	}

	String md_proration_result(String testCaseId) {
		String result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("cancel_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def clientNo = readData.TD_clientNo
		def planNo = readData.TD_cancelSuppPlan
		//db = new ConnectDB()
		ResultSet resultSet = db.executePlaQuery("select SUM(amount) from ariacore.credits where acct_no = (" + accountNo + ") AND CLIENT_NO = (" +clientNo + ")");
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		return "-"+result
	}



	String md_prorate_credit_amt(String testCaseId) {
		String result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("cancel_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def clientNo = readData.TD_clientNo
		def planNo = readData.TD_cancelSuppPlan
		ResultSet resultSet = db.executePlaQuery("select SUM(amount) from ariacore.credits where acct_no = (" + accountNo + ") AND ALT_SERVICE_NO_2_APPLY not in (401,402,405) AND CLIENT_NO = (" +clientNo + ")");
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		return result
	}

	String md_prorate_tax_amt(String testCaseId) {
		String result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("cancel_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def clientNo = readData.TD_clientNo
		def planNo = readData.TD_cancelSuppPlan
		ResultSet resultSet = db.executePlaQuery("select SUM(amount) from ariacore.credits where acct_no = (" + accountNo + ") AND ALT_SERVICE_NO_2_APPLY in (401,402,405) AND CLIENT_NO = (" +clientNo + ")");
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		return result
	}

	String md_ACCT_SUPP_PLAN_MOD_SCHEDULE(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def planNo = readData.TD_modifySuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SCHEDULE_NO FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE SUPP_PLAN_NO=" + planNo + "AND ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_Coupon_Discount_Spec_Plan_Spec_Charge_Service_AD1_MOD(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_modifySuppPlan
		def no_units = readData.TD_noOfUnits
		def coupon_code = readData.TD_couponCodeModify

		query = "SELECT * from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			invoiceNo = resultSet.getLong(1)
		}

		String query = "select debit,usage_units,usage_rate from ariacore.gl_detail where  invoice_no="+invoiceNo+" and service_no in(select key_2_val from ARIACORE.CLIENT_DR_COMPONENTS where rule_no in (select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+") and client_no="+clientNo+") and seq_num = "+no_units+""
		//return query
		//logi.logInfo(query)
		resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			usageunits = resultSet.getLong(2)
			usagerates = resultSet.getLong(3)
			debit = resultSet.getLong(1)
		}
		pre_discount_debit = usageunits*usagerates

		query = "select Amount from ariacore.CLIENT_DISCOUNT_RULES where rule_no in(select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			discount_Amount = rs.getLong(1);
		}



		if(debit == (pre_discount_debit-discount_Amount))
			return "TRUE"
		else
			return (debit-(pre_discount_debit-discount_Amount)).toString()


	}

	String md_Coupon_Discount_All_Charges_Spec_Plan_MOD(String testCaseId) {
		String pre_discount_debit;
		String service_no;

		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);

		def invoiceNo = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("MODIFY_SUPP_PLAN_INVOICENO"))
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_modifySuppPlan
		def coupon_code = readData.TD_couponCodeModify

		/*String query = "SELECT ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES where rule_no = (SELECT RULE_NO FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP_ where coupon_cd = '"+coupon_code+"')"
		 ResultSet resultSet = db.executePlaQuery(query);
		 if (resultSet.next()) {
		 service_no = resultSet.getString(1)
		 }*/

		String query = "SELECT sum(debit) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = "+invoiceNo+" AND SERVICE_NO NOT IN(401,402,405) AND SERVICE_NO in (SELECT ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES where rule_no = (SELECT RULE_NO FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP_ where coupon_cd = '"+coupon_code+"')) AND CLIENT_NO="+clientNo+" and orig_coupon_cd is not NULL"

		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			pre_discount_debit = resultSet.getString(1)
		}

		pre_discount_debit.replaceAll('-', '')
		pre_discount_debit.replaceAll("-", "")

		return pre_discount_debit



	}

	String md_Coupon_Discount_All_Charges_Spec_Plan_MOD1(String testCaseId) {
		String pre_discount_debit;
		String service_no;

		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);

		def invoiceNo = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("MODIFY_SUPP_PLAN_INVOICENO"))
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_modifySuppPlan
		def coupon_code = readData.TD_couponCodeModify

		/*String query = "SELECT ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES where rule_no = (SELECT RULE_NO FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP_ where coupon_cd = '"+coupon_code+"')"
		 ResultSet resultSet = db.executePlaQuery(query);
		 if (resultSet.next()) {
		 service_no = resultSet.getString(1)
		 }*/

		String query = "SELECT sum(debit) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = "+invoiceNo+" AND SERVICE_NO NOT IN(401,402,405) AND SERVICE_NO in (SELECT ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES where rule_no = (SELECT RULE_NO FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP_ where coupon_cd = '"+coupon_code+"')) and comments in (SELECT CLIENT_RULE_ID FROM ARIACORE.CLIENT_DISCOUNT_RULES where rule_no = (SELECT RULE_NO FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP_ where coupon_cd = '"+coupon_code+"')) AND CLIENT_NO="+clientNo+""

		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			pre_discount_debit = resultSet.getString(1)
		}

		pre_discount_debit.replaceAll('-', '')
		pre_discount_debit.replaceAll("-", "")

		return pre_discount_debit



	}

	String md_Coupon_Discount_Spec_Service_AD1_MOD(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("modify_supp_plan", "create_acct_complete")
		ConnectDB db = new ConnectDB();

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_modifySuppPlan
		def no_units = readData.TD_noOfUnits
		def coupon_code = readData.TD_couponCodeModify

		query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			invoiceNo = resultSet.getLong(1)
		}

		String query = "select debit,usage_units,usage_rate from ariacore.gl_detail where  invoice_no="+invoiceNo+" and service_no in(select key_1_val from ARIACORE.CLIENT_DR_COMPONENTS where rule_no in (select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+") and client_no="+clientNo+") and seq_num = "+no_units+""
		//return query
		//logi.logInfo(query)
		resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			usageunits = resultSet.getLong(2)
			usagerates = resultSet.getLong(3)
			debit = resultSet.getLong(1)
		}
		pre_discount_debit = usageunits*usagerates

		query = "select Amount from ariacore.CLIENT_DISCOUNT_RULES where rule_no in(select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			discount_Amount = rs.getLong(1);
		}



		if(debit == (pre_discount_debit-discount_Amount))
			return "TRUE"
		else
			return (debit-(pre_discount_debit-discount_Amount)).toString()


	}

	String md_Coupon_Discount_Spec_Service_MOD(String testCaseId) {
		int pre_discount_debit;
		int discount_Amount;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);

		def invoiceNo = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_INVOICE_NO1"))
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_modifySuppPlan
		def coupon_code = readData.TD_couponCodeModify

		String query = "select pre_discount_debit from ariacore.gl_detail where  invoice_no="+invoiceNo+" and service_no in(select key_1_val from ARIACORE.CLIENT_DR_COMPONENTS where rule_no in (select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+") and client_no="+clientNo+")"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			pre_discount_debit = resultSet.getLong(1)
		}


		query = "select Amount from ariacore.CLIENT_DISCOUNT_RULES where rule_no in(select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			discount_Amount = rs.getLong(1);
		}

		query = "select debit from ariacore.gl_detail where  invoice_no="+invoiceNo+" and service_no in(select key_1_val from ARIACORE.CLIENT_DR_COMPONENTS where rule_no in (select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+") and client_no="+clientNo+")"
		//logi.logInfo(query)
		resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			debit = resultSet.getLong(1)
		}

		if(debit == (pre_discount_debit-discount_Amount))
			return debit
		else
			return (debit-(pre_discount_debit-discount_Amount)).toString()


	}

	String md_Coupon_Discount(String testCaseId, String lineSeq) {
		int discountAmt ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);

		def discountDes = getXpathNodeValue(testCaseId, readData.getXPath("ASSIGN_SUPP_PLAN_DESCRIPTION" + lineSeq))
		def totalChargesBeforeTax = getXpathNodeValue(testCaseId, readData.getXPath("ASSIGN_SUPP_PLAN_TOTALCHARGESBEFORETAX1"))
		def clientNo = readData.TD_clientNo
		String query = "SELECT AMOUNT FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE CLIENT_NO=" + clientNo + "AND EXT_DESCRIPTION='" + discountDes + "'"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			discountAmt = resultSet.getInt(1)
		}

		query = "SELECT TAX_RATE FROM ARIACORE.TAX_RATE_VIEW where tax_id = (SELECT tax_id from ARIACORE.TAX_NAMES where client_no = " + clientNo + ")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			taxRate = rs.getString(1);
		}

		String creditReducesQuery = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = " + clientNo
		int creditReduces = 0

		resultSet = db.executePlaQuery(creditReducesQuery)
		if (resultSet.next()) {
			creditReduces = resultSet.getInt("CREDIT_REDUCES")
		}
		double taxAmt
		if(creditReduces.equals(1)) {
			//logi.logInfo(totalChargesBeforeTax + discountAmt)
			taxAmt = ((totalChargesBeforeTax.toString().toDouble() - discountAmt.toDouble()) * taxRate.toDouble()) + (totalChargesBeforeTax.toString().toDouble() - discountAmt.toDouble()) ;
		}
		else
		{
			taxAmt = (totalChargesBeforeTax.toString().toDouble() * taxRate.toString().toDouble()) + (totalChargesBeforeTax.toString().toDouble() - discountAmt.toDouble())
		}
		return taxAmt.toString()
	}

	String md_account_plan_existing_status(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("replace_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def planNo = readData.TD_existingSuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT STATUS_CD FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE SUPP_PLAN_NO=" + planNo + "AND ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_account_plan_replace_status(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("replace_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def planNo = readData.TD_replaceSuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT STATUS_CD FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE SUPP_PLAN_NO=" + planNo + "AND ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_ACCT_SUPP_PLAN_REPLACE_UNITS(String testCaseId) {
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("replace_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def planNo = readData.TD_replaceSuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE SUPP_PLAN_NO=" + planNo + "AND ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo);
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}

		return result
	}

	String md_RP_SUPP_PLAN_CREDITS(String testCaseId) {
		String result ;
		String temp = null;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("replace_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def invoiceNo = getXpathNodeValue(testCaseId, readData.getXPath("REPLACE_SUPP_PLAN_INVOICENO1"))
		def planNo = readData.TD_replaceSuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL_ Where INVOICE_NO =" + invoiceNo + "and (orig_coupon_cd is not null or orig_credit_id is not null)");
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}
		temp = result.replaceAll("-", "")
	}

	String md_RP_SUPP_PLAN_BEFORE_TAX_CREDITS(String testCaseId) {
		String result ;
		String temp = null;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("replace_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def invoiceNo = getXpathNodeValue(testCaseId, readData.getXPath("REPLACE_SUPP_PLAN_INVOICENO1"))
		def planNo = readData.TD_replaceSuppPlan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL_ Where INVOICE_NO =" + invoiceNo + "and (orig_coupon_cd is not null or orig_credit_id is not null) and service_no not in (400,401,402,405)");
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		temp = result.replaceAll("-", "")
	}

	String md_prorate_credit_rp(String testCaseId) {
		String result ;
		String result1 ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("replace_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def invoiceNo = getXpathNodeValue(testCaseId, readData.getXPath("REPLACE_SUPP_PLAN_INVOICENO1"))
		def clientNo = readData.TD_clientNo
		//def planNo = readData.TD_replaceSuppPlan
		ResultSet resultSet = db.executePlaQuery("WITH DIFF AS (select SUM(amount) as total from ariacore.credits where acct_no = (" + accountNo + ")), CR AS (SELECT SUM(DEBIT) as total FROM ARIACORE.GL_DETAIL Where INVOICE_NO = " + invoiceNo + " and orig_credit_id is NULL and orig_coupon_cd is NULL) SELECT DIFF.total - CR.total FROM   DIFF, CR")
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		ResultSet rs = db.executePlaQuery("select SUM(LEFT_TO_APPLY) from ariacore.credits where acct_no = (" + accountNo + ") AND CLIENT_NO = (" +clientNo + ")");
		if (rs.next()) {
			result1 = rs.getString(1)
		}


		if(result1==0)
		{
			return result1
		}
		else
		{
			return result
		}

	}

	String md_prorate_credit_rp1(String testCaseId) {
		String result = null ;
		String result1 ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("replace_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def invoiceNo = getXpathNodeValue(testCaseId, readData.getXPath("REPLACE_SUPP_PLAN_INVOICENO1"))
		def clientNo = readData.TD_clientNo
		//def planNo = readData.TD_replaceSuppPlan
		ResultSet resultSet = db.executePlaQuery("SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL Where INVOICE_NO =" + invoiceNo + " AND CLIENT_NO = (" +clientNo + ")");
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		return result

	}

	String md_prorate_credit_rp2(String testCaseId) {
		String result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("replace_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def invoiceNo = getXpathNodeValue(testCaseId, readData.getXPath("REPLACE_SUPP_PLAN_INVOICENO1"))
		def clientNo = readData.TD_clientNo
		//def planNo = readData.TD_replaceSuppPlan
		ResultSet resultSet = db.executePlaQuery("select SUM(amount) as total from ariacore.credits where acct_no = (" + accountNo + ") and alt_service_no_2_apply not in (400,401,402,405)")
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		return result

	}

	String md_prorate_credit_rp3(String testCaseId) {
		String result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("replace_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def invoiceNo = getXpathNodeValue(testCaseId, readData.getXPath("REPLACE_SUPP_PLAN_INVOICENO1"))
		def clientNo = readData.TD_clientNo
		//def planNo = readData.TD_replaceSuppPlan
		ResultSet resultSet = db.executePlaQuery("select SUM(amount) as total from ariacore.credits where acct_no = (" + accountNo + ") and alt_service_no_2_apply in (400,401,402,405)")
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		return result

	}

	String md_prorate_credit_rp4(String testCaseId) {
		String result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("replace_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def invoiceNo = getXpathNodeValue(testCaseId, readData.getXPath("REPLACE_SUPP_PLAN_INVOICENO1"))
		def clientNo = readData.TD_clientNo
		//def planNo = readData.TD_replaceSuppPlan
		ResultSet resultSet = db.executePlaQuery("select SUM(amount) as total from ariacore.credits where acct_no = (" + accountNo + ")")
		if (resultSet.next()) {
			result = resultSet.getString(1)
		}

		String temp = result.replaceAll("-", "")

		return temp

	}

	String md_Get_Act_Fee_RP(String testCaseId)
	{
		//int sum = 0;
		String result;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String modifySuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("replace_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		String clientNo = readData.TD_clientNo
		String planNO = readData.TD_replaceSuppPlan
		String units = readData.TD_noOfUnits
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String Query = "SELECT RATE_PER_UNIT from ARIACORE.NEW_RATE_SCHED_RATES where SCHEDULE_NO in (SELECT schedule_no FROM ARIACORE.ACCT_SUPP_PLAN_MAP where acct_no = " + accountNo + " and supp_plan_no = " + planNO + " AND client_no =" + clientNo + ") and service_no in (SELECT SERVICE_NO from ARIACORE.All_service where service_no in (SELECT SERVICE_NO from ARIACORE.PLAN_SERVICES where client_no = " + clientNo + " and plan_no= " + planNO + ") and client_no = " + clientNo + " and service_type = 'AC') and client_no = " + clientNo + "";
		//"SELECT RATE_PER_UNIT from ARIACORE.NEW_RATE_SCHED_RATES where SCHEDULE_NO in (SELECT schedule_no FROM ARIACORE.ACCT_SUPP_PLAN_MAP where acct_no = (" + accountNo + ") and supp_plan_no = (" + planNO + ") AND client_no=(" + clientNo + ")) and service_no in (SELECT SERVICE_NO from ARIACORE.All_service where service_no in(SELECT SERVICE_NO from ARIACORE.PLAN_SERVICES where client_no= (" + clientNo + ") and plan_no= (" + planNO + ")) and client_no= (" + clientNo + ") and service_type = 'AC') and client_no= (" + clientNo + ")"
		ResultSet resultSet = db.executePlaQuery(Query);

		if (resultSet.next()) {
			//sum = sum + resultSet.getInt("RATE_PER_UNIT");
			result = resultSet.getString(1)
		}

		//return Integer.toString(sum);
		return result
	}

	String md_STATE_TAX_ASP(String testCaseId){
		int result ;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		readData.fetchTestData(testId);

		def invoiceNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("ASSIGN_SUPP_PLAN_INVOICE_NO1"))
		def planNo = readData.TD_assign_supp_plan //plz change the assign sp here
		def clientNo = readData.TD_clientNo
		ResultSet resultSet = db.executePlaQuery("SELECT sum(debit) from ARIACORE.GL_DETAIL WHERE INVOICE_NO =  "+ invoiceNo+"  AND SERVICE_NO NOT IN(401,402,405) AND CLIENT_NO=" + clientNo+"");
		if (resultSet.next()) {
			result = resultSet.getInt(1)
		}
		result = result*0.06
		return result.toString()
	}


	String md_Coupon_Discount_Rec_Service_AD1(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB();
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_assign_supp_plan
		def no_units = readData.TD_num_plan_units_sp
		def coupon_code = readData.TD_coupon_code_sp

		query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			invoiceNo = resultSet.getLong(1)
		}

		String query = "select debit,usage_units,usage_rate from ariacore.gl_detail where  invoice_no="+invoiceNo+" and service_no in(select service_no from ARIACORE.All_service where service_no in (select service_no from ARIACORE.PLAN_SERVICES where plan_no="+plan_No+" and client_no = "+clientNo+") and client_no = "+clientNo+" and service_type='RC')"
		//logi.logInfo(query)

		resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			usageunits = resultSet.getLong(2)
			usagerates = resultSet.getLong(3)
			debit = resultSet.getLong(1)
		}
		pre_discount_debit = usageunits*usagerates

		query = "select Amount from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+"";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			discount_Amount = rs.getLong(1);
		}




		if(debit == (pre_discount_debit-discount_Amount))
			return "TRUE"
		else
			return (debit-(pre_discount_debit-discount_Amount)).toString()


	}


	String md_Coupon_Discount_Rec_Service(String testCaseId) {
		int pre_discount_debit;
		int discount_Amount;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB();
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);

		def invoiceNo = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_INVOICE_NO1"))
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_assign_supp_plan
		def coupon_code = readData.TD_coupon_code_sp
		String query = "select pre_discount_debit,debit from ariacore.gl_detail where  invoice_no="+invoiceNo+" and service_no in(select service_no from ARIACORE.All_service where service_no in (select service_no from ARIACORE.PLAN_SERVICES where plan_no="+plan_No+" and client_no = "+clientNo+") and client_no = "+clientNo+" and service_type='RC')"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			pre_discount_debit = resultSet.getLong(1)
			debit = resultSet.getLong(2)
		}

		query = "select Amount from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+"";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			discount_Amount = rs.getLong(1);
		}



		if(debit == (pre_discount_debit-discount_Amount))
			return "TRUE"
		else if(debit==0){
			if((pre_discount_debit-discount_Amount)<0)
				return "TRUE"
		}
		else
			return debit-(pre_discount_debit-discount_Amount).toString()


	}


	String md_Coupon_Discount_Spec_Service_AD1(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB();
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_assign_supp_plan
		def no_units = readData.TD_num_plan_units_sp
		def coupon_code = readData.TD_coupon_code_sp

		query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			invoiceNo = resultSet.getLong(1)
		}

		String query = "select debit,usage_units,usage_rate from ariacore.gl_detail where  invoice_no="+invoiceNo+" and service_no in(select key_1_val from ARIACORE.CLIENT_DR_COMPONENTS where rule_no in (select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+") and client_no="+clientNo+") and seq_num = "+no_units+""
		//return query
		//logi.logInfo(query)
		resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			usageunits = resultSet.getLong(2)
			usagerates = resultSet.getLong(3)
			debit = resultSet.getLong(1)
		}
		pre_discount_debit = usageunits*usagerates

		query = "select Amount from ariacore.CLIENT_DISCOUNT_RULES where rule_no in(select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			discount_Amount = rs.getLong(1);
		}



		if(debit == (pre_discount_debit-discount_Amount))
			return "TRUE"
		else
			return (debit-(pre_discount_debit-discount_Amount)).toString()


	}

	String md_Coupon_Discount_Spec_Service(String testCaseId) {
		int pre_discount_debit;
		int discount_Amount;
		int debit;
		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);

		def invoiceNo = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_INVOICE_NO1"))
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_assign_supp_plan
		def coupon_code = readData.TD_coupon_code_sp
		ConnectDB db = new ConnectDB();
		String query = "select pre_discount_debit from ariacore.gl_detail where  invoice_no="+invoiceNo+" and service_no in(select key_1_val from ARIACORE.CLIENT_DR_COMPONENTS where rule_no in (select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+") and client_no="+clientNo+")"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			pre_discount_debit = resultSet.getLong(1)
		}


		query = "select Amount from ariacore.CLIENT_DISCOUNT_RULES where rule_no in(select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			discount_Amount = rs.getLong(1);
		}

		query = "select debit from ariacore.gl_detail where  invoice_no="+invoiceNo+" and service_no in(select key_1_val from ARIACORE.CLIENT_DR_COMPONENTS where rule_no in (select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+") and client_no="+clientNo+")"
		//logi.logInfo(query)
		resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			debit = resultSet.getLong(1)
		}

		if(debit == (pre_discount_debit-discount_Amount))
			return "TRUE"
		else
			return (debit-(pre_discount_debit-discount_Amount)).toString()


	}


	String md_Coupon_Discount_Spec_Plan_Spec_Charge_Service_AD1(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB();
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_assign_supp_plan
		def no_units = readData.TD_num_plan_units_sp
		def coupon_code = readData.TD_coupon_code_sp

		query = "SELECT * from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			invoiceNo = resultSet.getLong(1)
		}

		String query = "select debit,usage_units,usage_rate from ariacore.gl_detail where  invoice_no="+invoiceNo+" and service_no in(select key_2_val from ARIACORE.CLIENT_DR_COMPONENTS where rule_no in (select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+") and client_no="+clientNo+") and seq_num = "+no_units+""
		//return query
		//logi.logInfo(query)
		resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			usageunits = resultSet.getLong(2)
			usagerates = resultSet.getLong(3)
			debit = resultSet.getLong(1)
		}
		pre_discount_debit = usageunits*usagerates

		query = "select Amount from ariacore.CLIENT_DISCOUNT_RULES where rule_no in(select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			discount_Amount = rs.getLong(1);
		}



		if(debit == (pre_discount_debit-discount_Amount))
			return "TRUE"
		else
			return (debit-(pre_discount_debit-discount_Amount)).toString()


	}



	String md_Coupon_Discount_Spec_Plan_Spec_Charge(String testCaseId) {
		int pre_discount_debit;
		int discount_Amount;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB();
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);

		def invoiceNo = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_INVOICE_NO1"))
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_assign_supp_plan
		def coupon_code = readData.TD_coupon_code_sp

		String query = "select pre_discount_debit,debit from ariacore.gl_detail where  invoice_no="+invoiceNo+" and service_no in(select key_1_val from ARIACORE.CLIENT_DR_COMPONENTS where rule_no in (select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+") and client_no="+clientNo+")"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			pre_discount_debit = resultSet.getLong(1)
			debit = resultSet.getLong(2)
		}


		query = "select Amount from ariacore.CLIENT_DISCOUNT_RULES where rule_no in(select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			discount_Amount = rs.getLong(1);
		}



		if(debit == (pre_discount_debit-discount_Amount))
			return "TRUE"
		else
			return (debit-(pre_discount_debit-discount_Amount)).toString()


	}


	String md_Coupon_Discount_All_Charges_Spec_Plan_offline(String testCaseId) {
		String pre_discount_debit;

		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		ConnectDB db = new ConnectDB();

		def invoiceNo = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_INVOICE_NO1"))
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_assign_supp_plan
		def coupon_code = readData.TD_coupon_code_sp
		String query = "SELECT sum(debit) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = "+invoiceNo+" AND SERVICE_NO NOT IN(401,402,405) AND CLIENT_NO="+clientNo+" and orig_coupon_cd is not NULL"

		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			pre_discount_debit = resultSet.getString(1)
		}
		return pre_discount_debit.replaceAll("[-+^]*", "");
	}





	String md_Coupon_Discount_All_Charges_Spec_Plan_offline_Ad1(String testCaseId) {
		String pre_discount_debit;

		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo
		query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			invoiceNo = resultSet.getLong(1)
		}


		def plan_No = readData.TD_assign_supp_plan
		def coupon_code = readData.TD_coupon_code_sp
		String query = "SELECT sum(debit) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = "+invoiceNo+" AND SERVICE_NO NOT IN(401,402,405) AND CLIENT_NO="+clientNo+" and orig_coupon_cd is not NULL"

		resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			pre_discount_debit = resultSet.getString(1)
		}

		pre_discount_debit.replaceAll('-', '')
		pre_discount_debit.replaceAll("-", "")

		return pre_discount_debit



	}

	String md_Coupon_Discount_All_Charges_Spec_Plan_2(String testCaseId){
		int pre_discount_debit;
		int discount_Amount;
		int debit;

		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB();
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);

		def serviceNo = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_SERVICENO2"))
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_assign_supp_plan
		def coupon_code = readData.TD_coupon_code_sp
		def schedule_no = readData.TD_altrateschedulenosp

		String query = "select RATE_PER_UNIT from ariacore.NEW_RATE_SCHED_RATES where schedule_no = "+schedule_no+" and client_no = "+clientNo+" and service_no = "+serviceNo+""
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			pre_discount_debit = resultSet.getDouble(1)

		}
		double res

		query = "select Amount,flat_percent_ind from ariacore.CLIENT_DISCOUNT_RULES where rule_no in(select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			discount_Amount = rs.getLong(1);
			if(rs.getString(2).equals("P")||rs.getString(2).equals("p")){
				res = pre_discount_debit * (discount_Amount/100)
				res = pre_discount_debit - res
			}
			else
			{
				res = pre_discount_debit - discount_Amount
			}
		}



		return res.toString()


	}


	String md_Coupon_Discount_All_Charges_Spec_Plan_1(String testCaseId) {
		int pre_discount_debit;
		int discount_Amount;
		int debit;

		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB();
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);

		def serviceNo = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_SERVICENO1"))
		def clientNo = readData.TD_clientNo
		def plan_No = readData.TD_assign_supp_plan
		def coupon_code = readData.TD_coupon_code_sp
		def schedule_no = readData.TD_altrateschedulenosp

		String query = "select RATE_PER_UNIT from ariacore.NEW_RATE_SCHED_RATES where schedule_no = "+schedule_no+" and client_no = "+clientNo+" and service_no = "+serviceNo+""
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			pre_discount_debit = resultSet.getLong(1)

		}
		long res

		query = "select Amount,flat_percent_ind from ariacore.CLIENT_DISCOUNT_RULES where rule_no in(select Rule_no from ariacore.client_discount_rules where rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd = '"+coupon_code+"' and client_no = "+clientNo+") and client_no = "+clientNo+")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			discount_Amount = rs.getLong(1);
			if(rs.getString(2).equals("P")||rs.getString(2).equals("p")){
				res = pre_discount_debit * (discount_Amount/100)
				res = pre_discount_debit - res
			}
			else
			{
				res = pre_discount_debit - discount_Amount
			}
		}



		return res.toString()


	}

	String externalTaxAvalara(String testCaseId, String serviceRateQuery) throws SQLException, ParseException
	{
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo

		def taxRate = 0.075;
		def servicerate;

		String serRateQ = db.buildQuery(serviceRateQuery, clientNo, getNodeVal(testCaseId, serviceRateQuery))
		// logi.logInfo(serRateQ);
		ResultSet resultSet = db.executePlaQuery(serRateQ)
		if (resultSet.next()) {
			servicerate = resultSet.getString(1)
		}
		// logi.logInfo('servicerate ' + servicerate);

		double amt1 = servicerate.toDouble() * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");
		// logi.logInfo('amt1 ' + amt1);
		return (Math.round(amt1*100.00)/100.00).toString();
	}

	String md_Do_Proration_Cal_Supp_ASPM(String testCaseId, String seq) {

		int tempCount = 1;
		String ret
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB();
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan_multi", "create_acct_complete")
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		readData.fetchTestData(testId);

		String billDate
		def clientNo = readData.TD_clientNo
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def ratePerUnit = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_MULTI_RATEPERUNIT" + seq))
		double noOfUnits = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_MULTI_LINEBASEUNITS" + seq)).toString().toDouble()
		def suppPlanNo = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_MULTI_PLANNO" + seq))
		if(Integer.parseInt(billingIntervalSuppPlan(suppPlanNo)) == 1) {
			billDate = "SELECT LAST_ARREARS_BILL_THRU_DATE from ARIACORE.ACCT where acct_no =" + accountNo
		} else {
			billDate = "SELECT Next_bill_date from ARIACORE.ACCT where acct_no =" + accountNo
		}

		// Get the current virtual time for the client
		ResultSet rs = db.executePlaQuery(billDate);

		String currentVirtualTime
		if(rs.next()) {
			currentVirtualTime = rs.getString(1);
		}
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		//		cal1.add(Calendar.DAY_OF_MONTH, 15);
		def totalday=0;

		if(Integer.parseInt(billingIntervalSuppPlan(suppPlanNo)) == 1) {
			totalday = cal1.getActualMaximum(Calendar.DAY_OF_MONTH);
		} else {
			totalday = totaldaysASPM(accountNo, Integer.parseInt(billingIntervalSuppPlan(suppPlanNo)), suppPlanNo)
			//			totalday++
		}

		double serviceRate = ratePerUnit.toString().toDouble()
		double daystoBill = daystoBillSuppPlan(accountNo,suppPlanNo).toString().toDouble()
		double proratedAmount = ((serviceRate/totalday.toDouble()) * daystoBill) * noOfUnits

		DecimalFormat df = new DecimalFormat("#.##");
		ret = df.format(proratedAmount).toString()
		return ret
	}

	String md_ExternalTax_Avalara_ASPM_EXP(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB();
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan_multi", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + "AND SERVICE_NO not in (401,402) AND CLIENT_NO=" + clientNo
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getString(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}

		def taxRate;
		taxRate = '0.075';

		double taxAmt = amt.toDouble() * taxRate.toDouble();

		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(taxAmt).toString()
	}

	String internalTax(String testCaseId, String serviceRateQuery) throws SQLException, ParseException
	{
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo

		String query = "SELECT TAX_RATE FROM ARIACORE.TAX_RATE_VIEW where tax_id = (SELECT tax_id from ARIACORE.TAX_NAMES where client_no = " + clientNo + ")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			taxRate = rs.getString(1);
		}

		String serRateQ = db.buildQuery(serviceRateQuery, clientNo, getNodeVal(testCaseId, serviceRateQuery))

		ResultSet resultSet = db.executePlaQuery(serRateQ)
		if (resultSet.next()) {
			servicerate = resultSet.getInt(1)
		}

		double amt1 = servicerate.toDouble() * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(amt1).toString();
	}

	String md_INTERNAL_TAX_CREDIT_REDUCES_0_CAC(String testCaseId) throws SQLException, ParseException
	{
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		//			def suppPlanNo = readData.TD_assign_supp_plan
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();

		String query = "SELECT TAX_RATE FROM ARIACORE.TAX_RATE_VIEW where tax_id = (SELECT tax_id from ARIACORE.TAX_NAMES where client_no = " + clientNo + ")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, totalAmtInt, totalAmtDisInt;
		if(rs.next()) {
			taxRate = rs.getString(1);
		}

		String totalAmtQuery = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND SERVICE_NO not in (401,402,405)"

		String totalAmt = db.buildQuery(totalAmtQuery, clientNo, getNodeVal(testCaseId, totalAmtQuery))

		ResultSet resultSet = db.executePlaQuery(totalAmt)
		if (resultSet.next()) {
			totalAmtInt = resultSet.getInt(1)
		}

		double amt1 = totalAmtInt.toDouble() * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(amt1).toString();
	}

	String md_INTERNAL_STATE_TAX_CAC(String testCaseId) throws SQLException, ParseException
	{
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		//			def suppPlanNo = readData.TD_assign_supp_plan
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();

		String totalAmtDisInt;

		String totalAmtQuery = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND SERVICE_NO in (401)"

		ResultSet resultSet = db.executePlaQuery(totalAmtQuery)
		if (resultSet.next()) {
			totalAmtDisInt = resultSet.getString(1)
		}

		return totalAmtDisInt;
	}

	String md_INTERNAL_TAX_CREDIT_REDUCES_0(String testCaseId) throws SQLException, ParseException
	{
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo

		String query = "SELECT TAX_RATE FROM ARIACORE.TAX_RATE_VIEW where tax_id = (SELECT tax_id from ARIACORE.TAX_NAMES where client_no = " + clientNo + ")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, totalAmtInt, totalAmtDisInt;
		if(rs.next()) {
			taxRate = rs.getString(1);
		}
		String invoiceNo
		String totalAmtQuery = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=<CREATE_ORDER_INVOICENO1> AND SERVICE_NO != 401"

		String totalAmt = db.buildQuery(totalAmtQuery, clientNo, getNodeVal(testCaseId, totalAmtQuery))

		ResultSet resultSet = db.executePlaQuery(totalAmt)
		if (resultSet.next()) {
			totalAmtInt = resultSet.getInt(1)
		}

		double amt1 = totalAmtInt.toDouble() * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(amt1).toString();
	}

	String md_InternalTax_ASPM_EXP(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan_multi", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def suppPlanNo = readData.TD_assign_supp_plan
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + "AND SERVICE_NO != 401 AND CLIENT_NO=" + clientNo
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}

		query = "SELECT TAX_RATE FROM ARIACORE.TAX_RATE_VIEW where tax_id = (SELECT tax_id from ARIACORE.TAX_NAMES where client_no = " + clientNo + ")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			taxRate = rs.getString(1);
		}

		double taxAmt = amt.toDouble() * taxRate.toDouble();

		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(taxAmt).toString()
	}

	String md_Service_Credit_Amt(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("create_acct_complete", "apply_service_credit")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def suppPlanNo = readData.TD_assign_supp_plan
		def accountNo = getXpathNodeValue(testCaseId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def originId = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("APPLY_SERVICE_CREDIT_CREDITID1"))
		String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= " + invoiceNo + " AND ORIG_CREDIT_ID = " + originId
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}
		return amt.replace("-", "");
	}


	String md_Get_Rec_Fee_Monthly(String testCaseId)
	{
		int sum = 0;
		String Query1 = null;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		try{

			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
			String modifySuppPlantestId = testCaseId
			String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
			readData.fetchTestData(testId);
			def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

			String client_No = readData.TD_clientNo
			String plan_NO = readData.TD_assign_supp_plan
			String units = readData.TD_num_plan_units_sp
			String rateScheduleNo = readData.TD_altrateschedulenosp
			if(rateScheduleNo==null||rateScheduleNo.isEmpty()){
				String q = "select default_rate_sched_no from ARIACORE.CLIENT_PLAN where plan_no= "+plan_NO+" AND client_no="+client_No+""
				ResultSet r1 = db.executeQuery(q);
				if(r1.next()){
					rateScheduleNo = r1.getString(1)
				}
			}
			String Query= "select * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in ("+rateScheduleNo+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no="+plan_NO+" and TIERED_PRICING_RULE_NO = 3) and client_no="+client_No+" and service_type = 'RC') and ((to_unit >="+units+" and to_unit = null) or from_unit<"+units+") and client_no="+client_No+"";
			ResultSet resultSet = db.executeQuery(Query);

			while(resultSet.next()){
				sum = sum + resultSet.getInt("RATE_PER_UNIT")
			}

			Query1= "SELECT * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in ("+rateScheduleNo+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no="+plan_NO+" and TIERED_PRICING_RULE_NO != 3) and client_no="+client_No+" and service_type = 'RC') and ((to_unit >="+units+" and to_unit = null) or from_unit<"+units+") and client_no="+client_No+"";
			ResultSet resultSet1 = db.executeQuery(Query1);

			while(resultSet1.next()){
				sum = sum + (resultSet1.getInt("RATE_PER_UNIT") * Integer.parseInt(units))
			}
		}
		catch(Exception e){

		}
		return Integer.toString(sum);
	}

	String md_Get_Rec_Fee_Anually(String testCaseId)
	{
		int sum = 0;
		String Query1 = null;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		try{
			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
			String modifySuppPlantestId = testCaseId
			String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
			readData.fetchTestData(testId);
			def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

			String client_No = readData.TD_clientNo
			String plan_NO = readData.TD_assign_supp_plan
			String units = readData.TD_num_plan_units_sp
			String rateScheduleNo = readData.TD_altrateschedulenosp
			if(rateScheduleNo==null||rateScheduleNo.isEmpty()){
				String q = "select default_rate_sched_no from ARIACORE.CLIENT_PLAN where plan_no= "+plan_NO+" AND client_no="+client_No+""
				ResultSet r1 = db.executeQuery(q);
				if(r1.next()){
					rateScheduleNo = r1.getString(1)
				}
			}
			String Query= "select * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in ("+rateScheduleNo+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no="+plan_NO+" and TIERED_PRICING_RULE_NO = 3) and client_no="+client_No+" and service_type = 'RC') and ((to_unit >="+units+" and to_unit = null) or from_unit<"+units+") and client_no="+client_No+"";
			ResultSet resultSet = db.executeQuery(Query);

			while(resultSet.next()){
				sum = sum + resultSet.getInt("RATE_PER_UNIT")
			}

			Query1= "SELECT * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in ("+rateScheduleNo+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no="+plan_NO+" and TIERED_PRICING_RULE_NO != 3) and client_no="+client_No+" and service_type = 'RC') and ((to_unit >="+units+" and to_unit = null) or from_unit<"+units+") and client_no="+client_No+"";
			ResultSet resultSet1 = db.executeQuery(Query1);

			while(resultSet1.next()){
				sum = sum + (resultSet1.getInt("RATE_PER_UNIT") * Integer.parseInt(units))
			}
			sum = sum * 12
		}
		catch(Exception e){

		}
		return Integer.toString(sum);
	}


	String md_Get_Act_Fee(String testCaseId)
	{
		int sum = 0;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		try{

			String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
			String modifySuppPlantestId = testCaseId
			readData.fetchTestData(testId);

			String client_No = readData.TD_clientNo
			String plan_NO = readData.TD_assign_supp_plan
			String units = readData.TD_num_plan_units_sp
			Query= "select * from ARIACORE.NEW_RATE_SCHED_RATES where schedule_no in (select default_rate_sched_no from ARIACORE.CLIENT_PLAN where plan_no=  "+plan_NO+" AND client_no="+client_No+") and service_no in (select service_no from ARIACORE.All_service where service_no in(select service_no from ARIACORE.PLAN_SERVICES where client_no="+client_No+" and plan_no= "+plan_NO+") and client_no="+client_No+" and service_type = 'AC') and client_no="+client_No+"";
			ResultSet resultSet = db.executeQuery(Query);

			while(resultSet.next()){
				sum = sum + (resultSet.getInt("RATE_PER_UNIT")*Integer.parseInt(units))
			}
		}
		catch(Exception e){

		}
		return Integer.toString(sum);
	}

	//Update_Master_plan

	String md_Update_Master_plan_Event_107(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete");

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo
		String event_ID;

		String query = "SELECT EVENT_ID FROM ARIACORE.ACCT_PROV2_EVENTS WHERE PROV_SEQ IN (SELECT PROV_SEQ FROM ARIACORE.ACCT_PROV2_STEPS  WHERE ACCT_NO= "+accountNo+" AND CLIENT_NO="+clientNo+") and EVENT_ID = 107 GROUP BY EVENT_ID"

		ResultSet resultSet = db.executePlaQuery(query);

		if (resultSet.next()) {
			return resultSet.getString(1);

		}else
		{
			return "No val"
		}

	}

	String md_Update_Master_plan_Event_108(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete");

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo
		String event_ID;

		String query = "SELECT EVENT_ID FROM ARIACORE.ACCT_PROV2_EVENTS WHERE PROV_SEQ IN (SELECT PROV_SEQ FROM ARIACORE.ACCT_PROV2_STEPS  WHERE ACCT_NO= "+accountNo+" AND CLIENT_NO="+clientNo+") and EVENT_ID = 108 GROUP BY EVENT_ID"

		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			return resultSet.getString(1)

		}else
		{

			return "No val"
		}

	}

	String md_Update_Master_plan(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete");

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo
		String event_ID;

		String query = "SELECT PLAN_NO FROM ARIACORE.ACCT  where acct_no = "+accountNo+" AND CLIENT_NO="+clientNo+""

		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			return resultSet.getString(1)

		}else
		{

			return "No val"
		}

	}

	String md_Update_Master_plan_Exp(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		return readData.TD_ump_master_plan_no;
	}


	String md_Update_Master_plan_Credit(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete");

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo
		String event_ID;

		String query = "SELECT sum(Amount) FROM ARIACORE.ALL_CREDITS where acct_no = "+accountNo+" AND CLIENT_NO="+clientNo+""

		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			return resultSet.getString(1)

		}else
		{

			return "No val"
		}

	}




	String md_Update_Master_plan_Credit_Downgrade(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete");

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo
		String event_ID;

		String query = "SELECT sum(total_applied+left_to_apply) FROM ARIACORE.ALL_CREDITS where acct_no = "+accountNo+" AND CLIENT_NO="+clientNo+""

		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			return resultSet.getString(1)

		}else
		{

			return "No val"
		}


	}


	String md_InternalTax_UMP(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updateMasterPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo
		def advVirtualTime = readData.TD_VerifyDate
		def suppPlanNo = readData.TD_assign_supp_plan
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def invoiceNo = getXpathNodeValue(updateMasterPlantestId, readData.getXPath("UPDATE_MASTER_PLAN_INVOICENO1"))
		//String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + "AND SERVICE_NO not in (401,402,405) AND CLIENT_NO=" + clientNo
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				amt = resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}

		query = "SELECT TAX_RATE FROM ARIACORE.TAX_RATE_VIEW where tax_id = (SELECT tax_id from ARIACORE.TAX_NAMES where client_no = " + clientNo + ")";
		ResultSet rs = db.executePlaQuery(query);
		def taxRate, servicerate;
		if(rs.next()) {
			taxRate = rs.getString(1);
		}
		String a = amt.toString()
		a.replaceAll("-", "")
		double taxAmt = a.replaceAll("-", "").toDouble()

		DecimalFormat df = new DecimalFormat("#.#");
		return df.format(taxAmt).toString()
	}

	String md_Master_Plan_Units(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updateMasterPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo

		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		//String invoiceNo = getInvoiceNo(clientNo, accountNo, advVirtualTime).toString();
		String query = "select plan_units from ariacore.acct_details where acct_no = "+accountNo+" AND CLIENT_NO=" + clientNo +""
		if(accountNo == "NoVal" ) {
			amt = "No Val ret"
		} else {

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){
				return resultSet.getInt(1);
			}
			else
			{
				amt = "No Val ret"
			}
		}


	}

	String md_Master_Plan_Units_Exp(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updateMasterPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete")

		readData.fetchTestData(testId);
		def clientNo = readData.TD_clientNo

		return readData.TD_ump_num_plan_units


	}


	String md_Master_Plan_Monthly_rec(String testCaseId) {

		String amt
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updateMasterPlantestId = testCaseId
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete")

		readData.fetchTestData(testId);
		def toUnit= readData.TD_ump_num_plan_units
		def planNo = readData.TD_ump_master_plan_no
		def clientNo = readData.TD_clientNo
		String query = "select RATE_PER_UNIT from ariacore.NEW_RATE_SCHED_RATES where schedule_no IN (select DEFAULT_RATE_SCHED_NO from ariacore.client_PLAN where client_no = "+clientNo+" AND PLAN_NO = "+planNo+") AND SERVICE_NO IN(select SERVICE_NO from ariacore.plan_services where plan_no = "+planNo+" and client_no = "+clientNo+" and FORCE_PRORATION_DELTA_VOLUME = 1)and to_unit >="+toUnit+" and from_unit<"+toUnit+""

		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()){
			return (resultSet.getInt(1)*(Integer.parseInt(toUnit)-Integer.parseInt(readData.TD_masterPlanUnits))).toString();
		}
		else
		{
			return "No Val ret"
		}

	}




	String md_Coupon_Apply(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo


		query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			invoiceNo = resultSet.getLong(1)
		}

		String query = "select * from ariacore.gl_detail where invoice_no = "+invoiceNo+" and orig_coupon_cd is not null"
		//return query
		//logi.logInfo(query)
		resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			return "TRUE"
		}else{
			return "FALSE"
		}




	}
	String md_check_supp_plan_status_terminated_UMP(String testCaseId) {
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))


		def clientNo = readData.TD_clientNo
		def cancel_supp_plan_ump = readData.TD_ump_auto_cancel_supp_plans

		String query = "select * from ariacore.acct_supp_plan_map where acct_no ="+accountNo+" and client_no= "+clientNo+" and status_cd=1"

		ResultSet resultSet = db.executePlaQuery(query);

		if(cancel_supp_plan_ump.equalsIgnoreCase("TRUE")){

			if (resultSet.next()) {
				return "FALSE"
			}else{
				return "TRUE"
			}
		}
		else{
			if (resultSet.next()) {
				return "TRUE"
			}else{
				return "FALSE"
			}
		}
	}


	String md_Coupon_Apply_uac(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_acct_complete", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo


		query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			invoiceNo = resultSet.getLong(1)
		}

		String query = "select * from ariacore.gl_detail where invoice_no = "+invoiceNo+" and orig_coupon_cd is not null"
		//return query
		//logi.logInfo(query)
		resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			return "TRUE"
		}else{
			return "FALSE"
		}

	}




	String md_is_coupon_applyed(String testCaseId) {
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updatetAcctComptestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_acct_complete", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		//def invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_ACCT_COMPLETE_INVOICE_NO"))
		def clientNo = readData.TD_clientNo
		def coupon_code = readData.TD_uac_coupon_code.toLowerCase()
		def invoiceNo
		def ad = readData.TD_uac_master_plan_assign_directive
		logi.logInfo("ad "+ad)
		if(ad.equalsIgnoreCase("1")){
			query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
			//logi.logInfo(query)
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()) {
				invoiceNo = resultSet.getLong(1)
			}
		}else{
			invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_ACCT_COMPLETE_INVOICE_NO"))
		}
		String query = "select * from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+invoiceNo+" and orig_coupon_cd = '"+coupon_code+"'"

		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			return "TRUE"
		}else{
			return "FALSE"
		}
	}


	String md_coupon_amount_all_Charge(String testCaseId) {
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updatetAcctComptestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_acct_complete", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		//def invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_ACCT_COMPLETE_INVOICE_NO"))
		def clientNo = readData.TD_clientNo
		def coupon_code = readData.TD_uac_coupon_code.toLowerCase();
		String planNo = readData.TD_masterPlanNo
		def invoiceNo
		def ad = readData.TD_uac_master_plan_assign_directive
		logi.logInfo("ad "+ad)
		if(ad.equalsIgnoreCase("1")){
			query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
			//logi.logInfo(query)
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()) {
				invoiceNo = resultSet.getLong(1)
			}
		}else{
			invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_ACCT_COMPLETE_INVOICE_NO"))
		}
		String query = "select FLAT_PERCENT_IND,AMOUNT from ariacore.client_discount_rules where client_no="+clientNo+" and rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd='"+coupon_code+"' and client_no="+clientNo+")"

		int discountAmt
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			discountAmt = resultSet.getInt(2)
			if(resultSet.getString(1).equalsIgnoreCase("F")){
				return resultSet.getInt(2)
			}
			else{
				double debitAmt;
				query = "SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+ invoiceNo +" AND SERVICE_NO NOT IN(402,102,401) AND PLAN_NO=" + planNo
				resultSet = db.executePlaQuery(query);
				if (resultSet.next()) {
					debitAmt = resultSet.getString(1).toDouble()
					discountAmt = resultSet.getInt(2)
				}
				double result = ((debitAmt * discountAmt) / 100)
				DecimalFormat df = new DecimalFormat("#.##");

				return "-" + df.format(result).toString()

			}
		}
		else{
			return "FALSE"
		}
	}

	String md_coupon_amount_all_Charge_offset(String testCaseId) {
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updatetAcctComptestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_acct_complete", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		//def invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_ACCT_COMPLETE_INVOICE_NO"))
		def clientNo = readData.TD_clientNo
		def coupon_code = readData.TD_uac_coupon_code.toLowerCase();
		String planNo = readData.TD_masterPlanNo
		def invoiceNo
		def ad = readData.TD_uac_master_plan_assign_directive
		logi.logInfo("ad "+ad)
		if(ad.equalsIgnoreCase("1")){
			query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
			//logi.logInfo(query)
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()) {
				invoiceNo = resultSet.getLong(1)
			}
		}else{
			invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_ACCT_COMPLETE_INVOICE_NO"))
		}
		String query = "select FLAT_PERCENT_IND,AMOUNT from ariacore.client_discount_rules where client_no="+clientNo+" and rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd='"+coupon_code+"' and client_no="+clientNo+")"

		int discountAmt
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			discountAmt = resultSet.getInt(2)
			if(resultSet.getString(1).equalsIgnoreCase("F")){

				double debitAmt;
				query = "SELECT DEBIT,PRE_DISCOUNT_DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+ invoiceNo +" AND SERVICE_NO NOT IN(402,102,401) AND PLAN_NO=" + planNo

				resultSet = db.executePlaQuery(query);
				if (resultSet.next()) {
					debitAmt = resultSet.getString(2).toDouble()

				}
				double result = debitAmt-discountAmt

				if(resultSet.getDouble(1)==(result))
				{
					return "TRUE"
				}
				else
				{
					return "FALSE"
				}
			}
			else{
				double debitAmt;
				query = "SELECT DEBIT,PRE_DISCOUNT_DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+ invoiceNo +" AND SERVICE_NO NOT IN(402,102,401) AND PLAN_NO=" + planNo

				resultSet = db.executePlaQuery(query);
				if (resultSet.next()) {
					debitAmt = resultSet.getString(2).toDouble()

				}
				double result = (debitAmt *( discountAmt / 100))
				result = debitAmt-result

				if(resultSet.getDouble(1)==(result))
				{
					return "TRUE"
				}
				else
				{
					return "FALSE"
				}


			}
		}
		else{
			return "FALSE"
		}
	}


	String md_coupon_amount_all_Charge_inline_percent(String testCaseId) {
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updatetAcctComptestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_acct_complete", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))


		def clientNo = readData.TD_clientNo
		def coupon_code = readData.TD_uac_coupon_code.toLowerCase();
		String planNo = readData.TD_masterPlanNo
		def invoiceNo
		def ad = readData.TD_uac_master_plan_assign_directive
		logi.logInfo("ad "+ad)
		if(ad.equalsIgnoreCase("1")){
			query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
			//logi.logInfo(query)
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()) {
				invoiceNo = resultSet.getLong(1)
			}
		}else{
			invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_ACCT_COMPLETE_INVOICE_NO"))
		}
		String query = "select FLAT_PERCENT_IND,AMOUNT from ariacore.client_discount_rules where client_no="+clientNo+" and rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd='"+coupon_code+"' and client_no="+clientNo+")"

		int discountAmt
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			discountAmt = resultSet.getInt(2)
			if(resultSet.getString(1).equalsIgnoreCase("P")){
				double debitAmt;
				query = "select Debit,Service_no from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+ invoiceNo +" and orig_coupon_cd is null and service_no not in(401,402) ORDER BY seq_num"

				resultSet = db.executePlaQuery(query);
				if (resultSet.next()) {
					debitAmt = resultSet.getDouble(1).toDouble()

				}
				double result = (debitAmt *( discountAmt / 100))


				query = "select debit from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+ invoiceNo +" and orig_coupon_cd = '"+coupon_code+"' and service_no="+resultSet.getString(2)+" ORDER BY seq_num"
				// logi.logInfo(query);

				ResultSet resultt = db.executePlaQuery(query);
				if (resultt.next()) {
					logi.logInfo("Test "+resultt.getLong(1))
					logi.logInfo("Test "+resultt.getString(1))
					logi.logInfo("Test "+resultt.getFloat(1))
					logi.logInfo("Test "+resultt.getString(1))
					logi.logInfo(resultt.getFloat(1).toString()+ " Test "+"-"+result.toString())

					if(resultt.getFloat(1).toString()=="-"+result.toString())
					{
						return "TRUE"
					}
					else
					{
						return "FALSE"
					}
				}

			}

		}
		else{
			return "FALSE"
		}
	}


	String md_coupon_amount_all_Charge_ump(String testCaseId) {
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updatetAcctComptestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		//def invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_ACCT_COMPLETE_INVOICE_NO"))
		def clientNo = readData.TD_clientNo
		def coupon_code = readData.TD_ump_coupon_code.toLowerCase();
		String planNo = readData.TD_ump_master_plan_no
		def invoiceNo
		def ad = readData.TD_ump_assignment_directive
		logi.logInfo("ad "+ad)
		if(ad.equalsIgnoreCase("1")){
			query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
			//logi.logInfo(query)
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()) {
				invoiceNo = resultSet.getLong(1)
			}
		}else{
			invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_MASTER_PLAN_INVOICENO1"))
		}
		String query = "select FLAT_PERCENT_IND,AMOUNT from ariacore.client_discount_rules where client_no="+clientNo+" and rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd='"+coupon_code+"' and client_no="+clientNo+")"

		int discountAmt
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			discountAmt = resultSet.getInt(2)
			if(resultSet.getString(1).equalsIgnoreCase("F")){
				return resultSet.getInt(2)
			}
			else{
				double debitAmt;
				query = "SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+ invoiceNo +" AND SERVICE_NO NOT IN(402,102,401) AND PLAN_NO=" + planNo
				resultSet = db.executePlaQuery(query);
				if (resultSet.next()) {
					debitAmt = resultSet.getString(1).toDouble()
					discountAmt = resultSet.getInt(2)
				}
				double result = ((debitAmt * discountAmt) / 100)
				DecimalFormat df = new DecimalFormat("#.##");

				return "-" + df.format(result).toString()

			}
		}
		else{
			return "FALSE"
		}
	}


	String md_is_coupon_applyed_ump(String testCaseId) {
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updatetAcctComptestId = testCaseId
		readData.fetchTestData(testId);
		logi.logInfo("testCaseId "+testCaseId)
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete")
		logi.logInfo("ad "+createAcctCompleteTestId)
		logi.logInfo("ad "+"test")
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		logi.logInfo("ad "+"ad")
		//def invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_ACCT_COMPLETE_INVOICE_NO"))
		def clientNo = readData.TD_clientNo
		def coupon_code = readData.TD_ump_coupon_code.toLowerCase();
		String planNo = readData.TD_ump_master_plan_no
		def invoiceNo
		def ad = readData.TD_ump_assignment_directive
		logi.logInfo("ad "+ad)
		logi.logInfo("coupon_code "+coupon_code)
		logi.logInfo("clientNo "+clientNo)
		if(ad.equalsIgnoreCase("1")){
			query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
			logi.logInfo(query)
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()) {
				invoiceNo = resultSet.getLong(1)
			}
		}else{
			invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_MASTER_PLAN_INVOICENO1"))

			logi.logInfo("invoiceNo :"+invoiceNo)
		}
		String query = "select * from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+invoiceNo+" and orig_coupon_cd = '"+coupon_code+"'"
		logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			return "TRUE"
		}else{
			return "FALSE"
		}
	}



	String md_coupon_amount_all_Charge_offset_ump(String testCaseId) {
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updatetAcctComptestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def clientNo = readData.TD_clientNo
		def coupon_code = readData.TD_ump_coupon_code.toLowerCase();
		String planNo = readData.TD_ump_master_plan_no
		def invoiceNo
		def ad = readData.TD_ump_assignment_directive
		logi.logInfo("ad "+ad)
		if(ad.equalsIgnoreCase("1")){
			query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
			logi.logInfo(query)
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()) {
				invoiceNo = resultSet.getLong(1)
			}
		}else{
			invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_MASTER_PLAN_INVOICENO1"))
		}
		logi.logInfo("invoiceNo   :"+invoiceNo)
		String query = "select FLAT_PERCENT_IND,AMOUNT from ariacore.client_discount_rules where client_no="+clientNo+" and rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd='"+coupon_code+"' and client_no="+clientNo+")"
		logi.logInfo(query)
		int discountAmt
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			discountAmt = resultSet.getInt(2)
			if(resultSet.getString(1).equalsIgnoreCase("F")){

				double debitAmt;
				query = "SELECT DEBIT,PRE_DISCOUNT_DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+ invoiceNo +" AND SERVICE_NO NOT IN(402,102,401) AND PLAN_NO=" + planNo
				logi.logInfo(query)
				resultSet = db.executePlaQuery(query);
				if (resultSet.next()) {
					debitAmt = resultSet.getString(2).toDouble()

				}
				logi.logInfo("debitAmt   "+debitAmt)
				double result = debitAmt-discountAmt
				logi.logInfo("result   "+result)
				logi.logInfo("resultSet.getDouble(1)   "+resultSet.getDouble(1))

				if(resultSet.getDouble(1)==(result))
				{
					return "TRUE"
				}
				else
				{
					return "FALSE"
				}
			}
			else{
				double debitAmt;
				query = "SELECT DEBIT,PRE_DISCOUNT_DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+ invoiceNo +" AND SERVICE_NO NOT IN(402,102,401) AND PLAN_NO=" + planNo

				resultSet = db.executePlaQuery(query);
				if (resultSet.next()) {
					debitAmt = resultSet.getString(2).toDouble()

				}
				double result = (debitAmt *( discountAmt / 100))
				result = debitAmt-result

				if(resultSet.getDouble(1)==(result))
				{
					return "TRUE"
				}
				else
				{
					return "FALSE"
				}


			}
		}
		else{
			return "FALSE"
		}
	}


	String md_coupon_amount_all_Charge_inline_percent_ump(String testCaseId) {
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updatetAcctComptestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))


		def clientNo = readData.TD_clientNo
		def coupon_code = readData.TD_ump_coupon_code.toLowerCase();
		String planNo = readData.TD_ump_master_plan_no
		def invoiceNo
		def ad = readData.TD_ump_assignment_directive
		logi.logInfo("ad "+ad)
		if(ad.equalsIgnoreCase("1")){
			query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
			logi.logInfo(query)
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()) {
				invoiceNo = resultSet.getLong(1)
			}
		}else{
			invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_MASTER_PLAN_INVOICENO1"))
		}
		String query = "select FLAT_PERCENT_IND,AMOUNT from ariacore.client_discount_rules where client_no="+clientNo+" and rule_no in(select rule_no from ariacore.coupon_discount_rule_map where coupon_cd='"+coupon_code+"' and client_no="+clientNo+")"
		logi.logInfo("invoiceNo   "+invoiceNo)
		int discountAmt
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			discountAmt = resultSet.getInt(2)
			if(resultSet.getString(1).equalsIgnoreCase("P")){
				double debitAmt;
				query = "select Debit,Service_no from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+ invoiceNo +" and orig_coupon_cd is null and service_no not in(401,402) ORDER BY seq_num"
				logi.logInfo(query)
				resultSet = db.executePlaQuery(query);
				if (resultSet.next()) {
					debitAmt = resultSet.getDouble(1).toDouble()

				}
				else{
					return "FALSE"
				}
				double result = (debitAmt *( discountAmt / 100))


				query = "select debit from ariacore.gl_detail where client_no="+clientNo+" and invoice_no="+ invoiceNo +" and orig_coupon_cd = '"+coupon_code+"' ORDER BY seq_num"
				logi.logInfo(query);

				ResultSet resultt = db.executePlaQuery(query);
				if (resultt.next()) {
					logi.logInfo("Test "+resultt.getLong(1))
					logi.logInfo("Test "+resultt.getString(1))
					logi.logInfo("Test "+resultt.getFloat(1))
					logi.logInfo("Test "+resultt.getString(1))
					logi.logInfo(resultt.getFloat(1).toString()+ " Test "+"-"+result.toString())

					if(resultt.getFloat(1).toString()=="-"+result.toString())
					{
						return "TRUE"
					}
					else
					{
						return "FALSE"
					}
				}
				else{
					return "FALSE"
				}

			}

		}
		else{
			return "FALSE"
		}
	}


	String md_Coupon_Apply_ump(String testCaseId) {
		int usageunits;
		int usagerates;
		int discount_Amount;
		int pre_discount_debit;
		int debit;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("update_master_plan", "create_acct_complete")


		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))

		def invoiceNo
		def clientNo = readData.TD_clientNo


		query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			invoiceNo = resultSet.getLong(1)
		}

		String query = "select * from ariacore.gl_detail where invoice_no = "+invoiceNo+" and orig_coupon_cd is not null"
		//return query
		//logi.logInfo(query)
		resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			return "TRUE"
		}else{
			return "FALSE"
		}

	}


	String md_LineItemAmount_ASP(String testCaseId,String itemNo) {
		String service_no = null;
		String plan_no = null;
		String no_of_units = null;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		plan_no = readData.TD_assign_Supp_Plan_no2
		no_of_units = readData.TD_num_plan_units_sp2
		def clientNo = readData.TD_clientNo
		logi.logInfo("Test plan"+plan_no)
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def invoiceNo =	getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_INVOICENO1"))
		service_no = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_MULTI_SERVICENO"+itemNo))
		logi.logInfo("Test plan"+service_no)
		String q = "select service_type from ariacore.all_service where service_no = "+service_no+" and client_no = "+clientNo;
		ResultSet resultService = db.executePlaQuery(q);
		String query = null;
		if(resultService.next()){
			if(resultService.getString(1).equalsIgnoreCase("AC")){
				query = "SELECT RATE_PER_UNIT FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN (SELECT SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP WHERE PLAN_NO="+plan_no+" AND CLIENT_NO="+clientNo+" AND SCHEDULE_NO NOT IN (-1)) AND SERVICE_NO = "+service_no;
			}else{
				query = "SELECT RATE_PER_UNIT FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN (SELECT SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP WHERE PLAN_NO="+plan_no+" AND CLIENT_NO="+clientNo+" AND SCHEDULE_NO NOT IN (-1)) AND SERVICE_NO = "+service_no+" AND to_unit >="+no_of_units+" and from_unit<"+no_of_units;
			}
		}

		ResultSet resultSet = db.executePlaQuery(query);

		logi.logInfo("Query  "+query)
		if (resultSet.next()) {
			logi.logInfo("resultSet.getInt(1)  "+resultSet.getInt(1))
			logi.logInfo("no_of_units.  "+no_of_units)

			if(resultService.getString(1).equalsIgnoreCase("AC")){
				return resultSet.getInt(1).toString();
			}else{
				return (resultSet.getInt(1)*Integer.parseInt(no_of_units)).toString();
			}


		}else{
			return "FALSE"
		}
	}

	String md_LineItemAmount_ASP_Active_Fee(String testCaseId) {
		String service_no = null;
		String plan_no = null;
		String no_of_units = null;
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String assignSuppPlantestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan", "create_acct_complete")
		plan_no = readData.TD_assign_Supp_Plan_no2
		no_of_units = readData.TD_num_plan_units_sp2
		def clientNo = readData.TD_clientNo
		logi.logInfo("Test plan"+plan_no)
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		def invoiceNo =	getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_INVOICENO1"))
		service_no = getXpathNodeValue(assignSuppPlantestId, readData.getXPath("ASSIGN_SUPP_PLAN_MULTI_SERVICENO1"))
		logi.logInfo("Test plan"+service_no)
		String q = "SELECT SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP WHERE PLAN_NO="+plan_no+" AND CLIENT_NO="+clientNo+" AND SCHEDULE_NO NOT IN (-1)"
		String query = "SELECT RATE_PER_UNIT FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN (SELECT SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP WHERE PLAN_NO="+plan_no+" AND CLIENT_NO="+clientNo+" AND SCHEDULE_NO NOT IN (-1)) AND SERVICE_NO = "+service_no;
		ResultSet resultSet = db.executePlaQuery(query);
		logi.logInfo("Query  "+query)
		if (resultSet.next()) {

			return (resultSet.getInt(1)).toString();

		}else{
			return "FALSE"
		}
	}


	String md_LineItemAmount_ASP_1(String testCaseId){
		md_LineItemAmount_ASP(testCaseId,"1")
	}

	String md_LineItemAmount_ASP_2(String testCaseId){
		md_LineItemAmount_ASP(testCaseId,"2")
	}

	String md_LineItemAmount_ASP_3(String testCaseId){
		md_LineItemAmount_ASP(testCaseId,"3")
	}

	String md_LineItemAmount_ASP_4(String testCaseId){
		md_LineItemAmount_ASP(testCaseId,"4")
	}



	String md_tax_no_of_line_asp(String testCaseId,String serviceNo) {
		logi.logInfo("Test ")
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updatetAcctComptestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan_multi", "create_acct_complete")

		logi.logInfo("createAcctCompleteTestId  "+createAcctCompleteTestId)
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		logi.logInfo("accountNo "+accountNo)
		def clientNo = readData.TD_clientNo
		logi.logInfo("TD_clientNo   "+accountNo)
		def invoiceNo
		if(updatetAcctComptestId.contains("create_acct_complete")){
			logi.logInfo("invoiceNo  asdgfhv,. :")
			//logi.logInfo(query)
			invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_MASTER_PLAN_INVOICENO1"))
			logi.logInfo("invoiceNo   :"+invoiceNo)
		}else{
			def ad = readData.TD_assignmentdirective
			logi.logInfo("ad "+ad)
			if(ad.equalsIgnoreCase("1")){
				query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
				//logi.logInfo(query)
				ResultSet resultSet = db.executePlaQuery(query);
				if (resultSet.next()) {
					invoiceNo = resultSet.getLong(1)
				}
			}else{
				invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_MASTER_PLAN_INVOICENO1"))
			}
		}
		logi.logInfo("invoiceNo   :"+invoiceNo)
		//logi.logInfo(query)


		double debitAmt;
		String   query = "SELECT count(*) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+ invoiceNo +" AND SERVICE_NO = "+serviceNo;
		//logi.logInfo(query)
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			if(resultSet.getInt(1)==1)
			{
				return "TRUE"
			}
			else
			{
				return "FALSE"
			}

		}



	}


	String md_tax_no_of_line_asp_400(String testCaseId){
		md_tax_no_of_line_asp(testCaseId,"400")
	}

	String md_tax_no_of_line_asp_401(String testCaseId){
		md_tax_no_of_line_asp(testCaseId,"401")
	}

	String md_tax_no_of_line_asp_402(String testCaseId){
		md_tax_no_of_line_asp(testCaseId,"402")
	}



	String md_tax_no_of_line_amt_asp(String testCaseId,String serviceNo) {
		logi.logInfo("tAX aMOUNT  ")
		ReadData readData = new ReadData()
		ConnectDB db = new ConnectDB()
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))
		String updatetAcctComptestId = testCaseId
		readData.fetchTestData(testId);
		String createAcctCompleteTestId = testCaseId.replaceAll("assign_supp_plan_multi", "create_acct_complete")

		logi.logInfo("createAcctCompleteTestId  "+createAcctCompleteTestId)
		def accountNo = getXpathNodeValue(createAcctCompleteTestId, readData.getXPath("CREATE_ACCT_COMPLETE_ACCTNO1"))
		logi.logInfo("accountNo "+accountNo)
		def clientNo = readData.TD_clientNo
		logi.logInfo("TD_clientNo   "+accountNo)
		def invoiceNo
		if(updatetAcctComptestId.contains("create_acct_complete")){
			logi.logInfo("invoiceNo  asdgfhv,. :")
			logi.logInfo(query)
			invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_MASTER_PLAN_INVOICENO1"))
			logi.logInfo("invoiceNo   :"+invoiceNo)
		}else{
			def ad = readData.TD_assignmentdirective
			logi.logInfo("ad "+ad)
			if(ad.equalsIgnoreCase("1")){
				query = "SELECT invoice_no from ARIACORE.GL where Acct_no = "+accountNo+" and client_no = "+clientNo+" Order by invoice_no DESC"
				//logi.logInfo(query)
				ResultSet resultSet = db.executePlaQuery(query);
				if (resultSet.next()) {
					invoiceNo = resultSet.getLong(1)
				}
			}else{
				invoiceNo =	getXpathNodeValue(updatetAcctComptestId, readData.getXPath("UPDATE_MASTER_PLAN_INVOICENO1"))
			}
		}
		logi.logInfo("invoiceNo   :"+invoiceNo)
		//logi.logInfo(query)


		double debitAmt;
		String   query = "select sum(debit) from ariacore.gl_tax_inc_detail where client_no="+clientNo+" and INVOICE_NO="+ invoiceNo +" AND SERVICE_NO = "+serviceNo;
		//logi.logInfo(query)
		int totalTax;
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			totalTax = resultSet.getLong(1);

		}
		logi.logInfo("totalTax   "+totalTax)
		query = "select debit from ariacore.gl_detail where client_no="+clientNo+" and INVOICE_NO="+ invoiceNo +" AND SERVICE_NO = "+serviceNo;
		//logi.logInfo(query)

		resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			debitAmt = resultSet.getLong(1);

		}
		logi.logInfo("debitAmt   "+debitAmt)
		if(totalTax == debitAmt){
			return "TRUE"
		}
		else
		{
			return "FALSE"
		}

	}

	String md_tax_no_of_line_amt_asp_400(String testCaseId){
		md_tax_no_of_line_amt_asp(testCaseId,"400")
	}

	String md_tax_no_of_line_amt_asp_401(String testCaseId){
		md_tax_no_of_line_amt_asp(testCaseId,"401")
	}

	String md_tax_no_of_line_amt_asp_402(String testCaseId){
		md_tax_no_of_line_amt_asp(testCaseId,"402")
	}
	
	// Refund Module related methods
	
	/**
	 * Getting all the values of 'get_reversible_invs_by_payment' api
	 * @param sequenceNo
	 * @return HashMap of all the values of specific line item
	 */
	HashMap getValueFromResponseAsHash(String sequenceNo) {
		
		// Variable declarations
		def node
		def cnodes
		def cnodeIndex
		HashMap responseData = new HashMap()

		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		logi.logInfo(testCaseCmb[0].toString())
		com.eviware.soapui.support.XmlHolder outputholder
		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains("get_reversible_invs_by_payment") && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				String linecount=getNodeCountFromResponse("get_reversible_invs_by_payment","//*/*/reversible_invoices[1]/*:reversible_invoices_row").toString()

				logi.logInfo( "Node count for seqnum "+sequenceNo+" "+linecount)
				for(int lno=1;lno<=linecount.toInteger();lno++)
				{
					String sno=outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+lno+"]/*:invoice_line_no[1]")
					logi.logInfo("Sequence number is :: "+sno)
					if(sequenceNo.equals(sno))
					{
						responseData.put("invoiced_acct_no",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+lno+"]/*:invoiced_acct_no[1]"))
						responseData.put("invoice_no",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+lno+"]/*:invoice_no[1]"))
						responseData.put("invoice_date",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+lno+"]/*:invoice_date[1]"))
						responseData.put("invoice_line_no",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+lno+"]/*:invoice_line_no[1]"))
						responseData.put("invoice_line_description",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+lno+"]/*:invoice_line_description[1]"))
						responseData.put("invoice_line_amount",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+lno+"]/*:invoice_line_amount[1]"))
						responseData.put("invoice_line_reversed_amount",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+lno+"]/*:invoice_line_reversed_amount[1]"))
						responseData.put("invoice_line_reversible_amount",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+lno+"]/*:invoice_line_reversible_amount[1]"))
						responseData.put("invoice_line_is_recur_service",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+lno+"]/*:invoice_line_is_recur_service[1]"))
						if(responseData.get("invoice_line_is_recur_service").equals("1")){
							logi.logInfo("Invoice line item - '"+lno+"' is a Recuring service - Adding recuring start date in Actual result ")
							responseData.put("invoice_line_recur_start_date",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+lno+"]/*:invoice_line_recur_start_date[1]"))
						}
						break
					}
				}

			}
		}
		logi.logInfo "The Invoice line item values for sequence - '"+sequenceNo+"' are : "+ responseData
		return responseData
	}
	
	/**
	 * Getting the tax line items of 'get_reversible_invs_by_payment' api
	 * @param sequenceNo
	 * @return HashMap of tax line items
	 */
	HashMap<String,String> getReversibleTaxValueFromResponseAsHash(String sequenceNo) {
		// Variable declarations
		def node
		def cnodes
		def cnodeIndex
		HashMap responseData = new HashMap<String,String>()

		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		logi.logInfo(testCaseCmb[0].toString())
		com.eviware.soapui.support.XmlHolder outputholder
		// Iterating with the list of response hash for 'get_reversible_invs_by_payment' api
		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains("get_reversible_invs_by_payment") && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				responseData.put("invoiced_acct_no",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+sequenceNo+"]/*:invoiced_acct_no[1]"))
				responseData.put("invoice_no",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+sequenceNo+"]/*:invoice_no[1]"))
				responseData.put("invoice_date",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+sequenceNo+"]/*:invoice_date[1]"))
				responseData.put("invoice_line_no",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+sequenceNo+"]/*:invoice_line_no[1]"))
				responseData.put("invoice_line_description",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+sequenceNo+"]/*:invoice_line_description[1]"))
				responseData.put("invoice_line_amount",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+sequenceNo+"]/*:invoice_line_amount[1]"))
				responseData.put("invoice_line_reversed_amount",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+sequenceNo+"]/*:invoice_line_reversed_amount[1]"))
				responseData.put("invoice_line_reversible_amount",outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+sequenceNo+"]/*:invoice_line_reversible_amount[1]"))
			}
		}
		logi.logInfo "The Invoice tax line item values for sequence - '"+sequenceNo+"' are : "+ responseData
		return responseData
	}

	/**
	 * Get the total tax amount from 'get_reversible_invs_by_payment' api
	 * @param sequenceNo
	 * @return total amount
	 */
	public String getReversibleTaxValueFromAPI(String sequenceNo) {
		def node
		def cnodes
		def cnodeIndex
		String responseData = ""

		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		logi.logInfo(testCaseCmb[0].toString())
		com.eviware.soapui.support.XmlHolder outputholder
		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains("get_reversible_invs_by_payment") && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				responseData=outputholder.getNodeValue("//*/*/reversible_invoices[1]/*:reversible_invoices_row["+sequenceNo+"]/*:invoice_line_reversible_amount[1]")
				break
			}
		}
		logi.logInfo "The Invoice tax line item values for sequence - '"+sequenceNo+"' are : "+ responseData
		return responseData
	}

	def md_GET_REVERSIBLE_INVOICE_SEQUENCE1(String testCaseId){
		return getValueFromResponseAsHash("1")
	}
	def md_GET_REVERSIBLE_INVOICE_SEQUENCE2(String testCaseId){
		return getValueFromResponseAsHash("2")
	}
	def md_GET_REVERSIBLE_INVOICE_SEQUENCE3(String testCaseId){
		return getValueFromResponseAsHash("3")
	}
	def md_GET_REVERSIBLE_INVOICE_SEQUENCE4(String testCaseId){
		return getValueFromResponseAsHash("4")
	}
	def md_GET_REVERSIBLE_INVOICE_SEQUENCE5(String testCaseId){
		return getValueFromResponseAsHash("5")
	}

	def md_get_reversible_invoice_by_seqNo1(String testCaseId) {
		return md_GET_REVERSIBLE_INVS_BY_PAYMENT(testCaseId,"1")
	}
	def md_get_reversible_invoice_by_seqNo2(String testCaseId) {
		return md_GET_REVERSIBLE_INVS_BY_PAYMENT(testCaseId,"2")
	}
	def md_get_reversible_invoice_by_seqNo3(String testCaseId) {
		return md_GET_REVERSIBLE_INVS_BY_PAYMENT(testCaseId,"3")
	}
	def md_get_reversible_invoice_by_seqNo4(String testCaseId) {
		return md_GET_REVERSIBLE_INVS_BY_PAYMENT(testCaseId,"4")
	}
	def md_get_reversible_invoice_by_seqNo5(String testCaseId) {
		return md_GET_REVERSIBLE_INVS_BY_PAYMENT(testCaseId,"5")
	}
	def md_get_reversible_invoice_paycredit_by_seqNo1(String testCaseId) {
		return md_GET_REVERSIBLE_INVS_BY_PAYMENT_FOR_PAYCREDIT(testCaseId,"SeqNo1")
	}
	def md_get_reversible_invoice_paycredit_by_seqNo2(String testCaseId) {
		return md_GET_REVERSIBLE_INVS_BY_PAYMENT_FOR_PAYCREDIT(testCaseId,"SeqNo2")
	}

	/**
	 * Getting details of 'get_reversible_invs_by_payment' api details from database
	 * @param testCaseId
	 * @param sequenceNumber
	 * @return all details for specific sequence
	 */
	HashMap<String,String> md_GET_REVERSIBLE_INVS_BY_PAYMENT(String testCaseId,String sequenceNumber) {
		HashMap<String, String> rowWiseMap
		ReadData readData = new ReadData()

		String createAcctCompleteTestId = testCaseId.replaceAll("get_reversible_invs_by_payment", "create_acct_complete")
		String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		logi.logInfo("Invoice number is :  "+invoiceNo)

		HashMap<String,String> getMapValue = get_reversible_invoice_line_Items(getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString(),invoiceNo,sequenceNumber)
		logi.logInfo("The Hash map got from the method for sequence No : "+sequenceNumber+" is : "+getMapValue)
		return getMapValue
	}

	/**
	 * Getting details of 'get_reversible_invs_by_payment' api details with payCredit
	 * @param testCaseId
	 * @param sequenceNumber
	 * @return all details for specific sequence
	 */
	HashMap<String,String> md_GET_REVERSIBLE_INVS_BY_PAYMENT_FOR_PAYCREDIT(String testCaseId,String sequenceNumber) {
		HashMap<String, String> rowWiseMap
		ReadData readData = new ReadData()

		String createAcctCompleteTestId = testCaseId.replaceAll("get_reversible_invs_by_payment", "create_acct_complete")
		String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		logi.logInfo("Invoice number is :  "+invoiceNo)

		// Getting the hashMap of values from a linked hashmap for all the sequence numbers
		LinkedHashMap<String, HashMap<String,String>> getMapValue = get_all_reversible_invoice_line_Items_with_payment_credit(getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1).toString(),invoiceNo)
		logi.logInfo("The entire map got from the method is : "+getMapValue)
		return getMapValue.get(sequenceNumber)
	}

	/**
	 * Verifying whether coupon is applied for a invoice
	 * @param InvoiceNo
	 * @param ClientNo
	 * @return true/false
	 */
	public Boolean isCouponApplied(String InvoiceNo,String ClientNo)
	{
		logi.logInfo( "Calling isCouponApplied")
		ConnectDB db = new ConnectDB()
		
		// Couon code query
		String coupon_code_query="select orig_coupon_cd from ariacore.gl_detail where invoice_no="+InvoiceNo+" and orig_coupon_cd is not null and client_no="+ClientNo
		ResultSet resultSet = db.executePlaQuery(coupon_code_query);
		logi.logInfo "isCouponApplied "+resultSet.next()
		return resultSet.first
	}

	/**
	 * 
	 * @param accountNo
	 * @param invoice_number
	 * @param sno
	 * @return
	 */
	public String getCouponAmount(String accountNo,String invoice_number,String sno)
	{

		logi.logInfo( "Calling getCouponAmount for SEQ Number "+sno)

		String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		String coupon_amt
		
		if(isCouponApplied(invoice_number,client_number) == true) // when coupuon applied
		{
			String coupon_amt_query="SELECT SEQ_NUM as SEQ_NO,SUM(DISTILLED_AMOUNT) as COUPON_AMT FROM (SELECT ELIGIBLE_SERVICE_NO,DISTILLED_AMOUNT FROM ariacore.all_recur_credit WHERE acct_no =    (SELECT acct_no FROM ariacore.gl WHERE invoice_no="+invoice_number+") UNION SELECT CDR.KEY_2_VAL AS ELIGIBLE_SERVICE_NO,  CASE WHEN CDRS.INLINE_OFFSET_IND='I' THEN 0      ELSE AIDD.ACTUAL_DISCOUNT_AMT END AS DISTILLED_AMOUNT FROM ARIACORE.CLIENT_DR_COMPONENTS CDR JOIN ariacore.all_invoice_discount_details AIDD ON AIDD.rule_no=CDR.rule_no JOIN ariacore.CLIENT_DISCOUNT_RULES CDRS ON CDRS.rule_no =AIDD.rule_no WHERE AIDD.invoice_no ="+invoice_number+" )a join ariacore.gl_detail gd on gd.service_no=a.ELIGIBLE_SERVICE_NO and gd.invoice_no="+invoice_number+" and gd.orig_coupon_cd is null GROUP BY SEQ_NUM"
			ConnectDB db = new ConnectDB()
			LinkedHashMap<String,String> coupons = new LinkedHashMap<String,String>();
			ResultSet rs = db.executePlaQuery(coupon_amt_query);
			while (rs.next()){
				coupons.put(rs.getString("SEQ_NO"),rs.getString("COUPON_AMT"))
			}
			logi.logInfo "Coupon Against Sequnce number"
			logi.logInfo coupons.toString()
			if(coupons.containsKey(sno))
			{
				logi.logInfo "Coupon amount is "+coupons.get(sno).toString()
				coupon_amt=coupons.get(sno)
			}
			else
			{
				logi.logInfo "No coupon applied for this sequnce number, so coupon amt is 0"
				coupon_amt="0"
			}
		}else	// when coupon is not applied to the invoice
		{
			logi.logInfo "No coupon applied for this account, so coupon amt is 0"
			coupon_amt="0"
		}
		logi.logInfo("Final coupon amt" +coupon_amt)
		return coupon_amt
	}

	/**
	 * Getting all the line items from ''
	 * @param account_number
	 * @param invoice_number
	 * @return
	 */
	public LinkedHashMap<String,HashMap<String,String>> get_all_reversible_invoice_line_Items_with_payment_credit(String account_number, String invoice_number) {
		LinkedHashMap<String,HashMap<String,String>> reversible_response_values = new LinkedHashMap<String, String>()
		HashMap<String, String> rowWiseMap
		ConnectDB databaseee = new ConnectDB()
		DecimalFormat df = new DecimalFormat("#")

		def paymentCreditBalance
		def balance

		Date date = new Date();
		ResultSet resultSet
		Calendar cal = Calendar.getInstance();

		int sequenceCount = 0;
		String[] tempValues
		List keyValues = [
			"invoice_line_no",
			"invoice_line_description",
			"invoice_line_amount",
			"invoice_line_reversed_amount",
			"invoice_line_reversible_amount",
			"invoice_line_is_recur_service",
			"invoice_line_recur_start_date",
			"invoiced_acct_no",
			"invoice_no",
			"invoice_date"
		];

		String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		
		// Sequence number query
		String seqNo_Query = "Select count(seq_num) from ariacore.gl_detail where client_no="+client_number+" and service_no not in(0,400,401) and invoice_no="+invoice_number
		resultSet = databaseee.executePlaQuery(seqNo_Query)
		if (resultSet.next()) {
			sequenceCount = resultSet.getString(1).toInteger()
		}
		logi.logInfo("Total sequence line items in the invoice : "+sequenceCount)

		// Getting the paycredit balance
		paymentCreditBalance = get_discount_through_creditcard()
		logi.logInfo("Raw creditReduce amount value is : "+paymentCreditBalance)

		// Iterating through all the sequence numbers
		for (int mainHash = 1 ; mainHash <= sequenceCount ; mainHash++) {
			rowWiseMap = new HashMap<String, String>();

			// Setting invoice number and account number into the hash
			rowWiseMap.put(keyValues.get(7), account_number)
			rowWiseMap.put(keyValues.get(8), invoice_number)

			String tempQuery = "Select seq_num,comments,debit from ariacore.gl_detail where client_no="+client_number+" and invoice_no="+invoice_number+" and seq_num="+mainHash
			resultSet = databaseee.executePlaQuery(tempQuery);

			ResultSetMetaData metaData = resultSet.getMetaData();
			int columns = metaData.getColumnCount();
			logi.logInfo("Column count in triple cc  : "+columns)

			// Setting sequence number, debit and service description into the hash
			while (resultSet.next()) {
				tempValues = new String[columns]
				for (int col = 1; col <= columns; col++) {
					String columnValue = resultSet.getString(col);
					tempValues[col-1]=columnValue;
				}
			}
			logi.logInfo("Invoice line items : "+Arrays.deepToString(tempValues))
			for (int i = 0 ; i < tempValues.size()-1 ; i++) {
				rowWiseMap.put(keyValues.get(i), tempValues[i]);
			}


			// Setting invoice line amount to hash
			String line_amount
			if(tempValues[2].contains('.'))
			{
				line_amount= (tempValues[2].toDouble()+getTaxInclusiveAmount(invoice_number,mainHash.toString()).toDouble()).toString()
			}
			else
				line_amount= (tempValues[2].toInteger()+getTaxInclusiveAmount(invoice_number,mainHash.toString()).toInteger()).toString()
			logi.logInfo("Raw invoice line amount value is : "+line_amount)
			rowWiseMap.put(keyValues.get(2), line_amount);
			
			// Setting reversedValue into hash
			String reversedValue
			String lineReversedQuery = "Select NVL(SUM(CREDIT),0) from ariacore.refund_detail where invoice_no="+invoice_number+" and gl_seq_num="+mainHash
			reversedValue = databaseee.executeQueryP2(lineReversedQuery);
			logi.logInfo("Raw reversed value is : "+reversedValue)
			rowWiseMap.put(keyValues.get(3), reversedValue)

			// Setting reversible amount in hash
			String reversibleAmtValue
			String reversibleAmtQuery
			reversibleAmtQuery = "SELECT NVL(SUM(a.REMAINING_REVERSIBLE_AMOUNT),0) TOTAL_REVERSIBLE_AMOUNT FROM(SELECT DEBIT -(SELECT NVL(SUM(CREDIT),0) REVERSED_AMOUNT FROM ARIACORE.REFUND_DETAIL RD  WHERE RD.INVOICE_NO="+invoice_number+" AND RD.GL_SEQ_NUM="+mainHash+") REMAINING_REVERSIBLE_AMOUNT FROM ARIACORE.GL_DETAIL GLD WHERE GLD.SEQ_NUM ="+mainHash+" AND GLD.INVOICE_NO="+invoice_number+" AND EXISTS  (SELECT * FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+account_number+" AND TRANSACTION_TYPE IN(2,3)))a"
			reversibleAmtValue = databaseee.executeQueryP2(reversibleAmtQuery);
			logi.logInfo("reversibleAmtValue before tax&coupon " +reversibleAmtValue)

			// Getting the tax invclusive amount
			String inclusive_amt=getTaxInclusiveAmount(invoice_number,mainHash.toString())
			logi.logInfo("Raw tax inclusive amount value is : "+inclusive_amt)
			
			// Getting the coupon amount
			String coupon_amt=getCouponAmount(account_number,invoice_number,mainHash.toString())
			logi.logInfo("Raw coupon amount value is : "+coupon_amt)

			// Calculating the final reversible amount
			def finalreversibleAmtValue
			if(reversibleAmtValue.contains('.'))
			{
				finalreversibleAmtValue=(reversibleAmtValue.toDouble() +inclusive_amt.toDouble()-coupon_amt.toDouble()).toString()
				logi.logInfo("Raw reversibleAmtValue after applying taxinclusive is : "+finalreversibleAmtValue)
			}

			else
			{
				finalreversibleAmtValue=(reversibleAmtValue.toInteger() +inclusive_amt.toInteger()-coupon_amt.toInteger()).toString()
				logi.logInfo("Raw reversibleAmtValue after applying taxinclusive is : "+finalreversibleAmtValue)
			}

			def ddiscAmt
			if(finalreversibleAmtValue.contains('.'))
			{
				ddiscAmt=(finalreversibleAmtValue.toDouble() - paymentCreditBalance.toDouble())
				logi.logInfo("Raw reversible val after pay discount is : "+ddiscAmt)

			}else{
				ddiscAmt=(reversibleAmtValue.toInteger() - paymentCreditBalance.toInteger())
				logi.logInfo("Raw reversible val after pay discount is : "+ddiscAmt)
			}

			if(ddiscAmt < 0){
				balance =  Math.abs(ddiscAmt)
				rowWiseMap.put(keyValues.get(4), "0")
				logi.logInfo("Added to hash balance: "+balance)
			} else {
				rowWiseMap.put(keyValues.get(4), ddiscAmt.toString())
				logi.logInfo("Added to hash and val is : "+rowWiseMap.get(keyValues.get(4)))
				balance = 0
			}
			paymentCreditBalance = balance
			logi.logInfo("Added to hash : "+paymentCreditBalance)

			String recuring_val
			String recursiveTypeQuery = "SELECT RECURRING FROM ariacore.services WHERE service_no in (SELECT service_no FROM ariacore.gl_detail WHERE client_no = "+client_number+" AND invoice_no ="+invoice_number+" and seq_num ="+mainHash+")"
			recuring_val = databaseee.executeQueryP2(recursiveTypeQuery);

			logi.logInfo("Raw recuring_val is : "+recuring_val)
			rowWiseMap.put(keyValues.get(5), recuring_val)

			// Date values
			String dateGetQuery = "SELECT start_date FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.SERVICES SER ON GLD.SERVICE_NO=SER.SERVICE_NO WHERE CLIENT_NO="+client_number+" AND INVOICE_NO="+invoice_number+" AND SER.RECURRING=1"
			resultSet = databaseee.executePlaQuery(dateGetQuery)
			if (resultSet.next()) {
				date = resultSet.getDate(1)
			}
			logi.logInfo("Raw date is : "+date)

			if(recuring_val.contains("1")){
				rowWiseMap.put(keyValues.get(6), date)
			}

			// Adding invoice date
			String formatDateQuery = "SELECT to_number(TO_CHAR(start_date,'mm')) || '/' || to_number(TO_CHAR(start_date,'dd')) || '/' ||to_number(TO_CHAR(start_date,'yyyy')) START_DATE FROM ariacore.gl_detail GLD JOIN ARIACORE.SERVICES SER ON SER.SERVICE_NO =GLD.SERVICE_NO WHERE client_no ="+client_number+" AND invoice_no="+invoice_number+" AND SER.RECURRING=1"
			resultSet = databaseee.executePlaQuery(formatDateQuery)
			String seqDate
			if (resultSet.next()) {
				seqDate = resultSet.getString(1)
			}
			logi.logInfo("Raw date is : "+seqDate)
			rowWiseMap.put(keyValues.get(9), seqDate)

			logi.logInfo("Individual map count :::: "+mainHash+" : "+rowWiseMap)

			// add in entire hash to main hash with sequence no as key
			reversible_response_values.put("SeqNo"+mainHash, rowWiseMap)

		}
		logi.logInfo("Final map count ::::: "+reversible_response_values)
		return reversible_response_values;
	}
	
	/**
	 * Getting the reversible line items
	 * @param account_number
	 * @param invoice_number
	 * @param sequenceNumber
	 * @return HashMap of line items details
	 */
	public HashMap<String,HashMap> get_reversible_invoice_line_Items(String account_number, String invoice_number,String sequenceNumber) {

		// Variable declarations
		HashMap<String, String> rowWiseMap
		ConnectDB databaseee = new ConnectDB()
		DecimalFormat df = new DecimalFormat("#.####")

		def paymentCreditBalance
		def balance
		Date date = new Date();
		ResultSet resultSet
		Calendar cal = Calendar.getInstance();

		int sequenceCount = 0;
		String[] tempValues
		List keyValues = [
			"invoice_line_no",
			"invoice_line_description",
			"invoice_line_amount",
			"invoice_line_reversed_amount",
			"invoice_line_reversible_amount",
			"invoice_line_is_recur_service",
			"invoice_line_recur_start_date",
			"invoiced_acct_no",
			"invoice_no",
			"invoice_date"
		];

		String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		String seqNo_Query = "Select count(seq_num) from ariacore.gl_detail where client_no="+client_number+" and invoice_no="+invoice_number+" and service_no not in (400,401,405,402)"
		resultSet = databaseee.executePlaQuery(seqNo_Query)
		if (resultSet.next()) {
			sequenceCount = resultSet.getString(1).toInteger()
		}
		logi.logInfo("Total sequence line items in the invoice : "+sequenceCount)
		paymentCreditBalance = get_discount_through_creditcard()
		logi.logInfo("Raw creditReduce amount value is : "+paymentCreditBalance)

		rowWiseMap = new HashMap();

		String invoiceLIneAmt
		String verificationVal
		String verificationValQuery = "Select NVL(SUM(CREDIT),0) from ariacore.refund_detail where invoice_no="+invoice_number+" and gl_seq_num="+sequenceNumber
		String invoiceLineAmtQuery = "Select debit from ariacore.gl_detail where client_no="+client_number+" and invoice_no="+invoice_number+" and seq_num="+sequenceNumber

		// Getting invoice line amount
		invoiceLIneAmt = databaseee.executeQueryP2(invoiceLineAmtQuery);
		logi.logInfo("Total invoice line amount is :: "+invoiceLIneAmt)

		verificationVal = databaseee.executeQueryP2(verificationValQuery);
		logi.logInfo("Total credit amount is :: "+verificationVal)

		if(invoiceLIneAmt.toFloat() > verificationVal.toFloat())
		{
			// Setting invoice number and account number
			rowWiseMap.put(keyValues.get(7), account_number)
			rowWiseMap.put(keyValues.get(8), invoice_number)

			String tempQuery = "Select seq_num,comments,debit from ariacore.gl_detail where client_no="+client_number+" and invoice_no="+invoice_number+" and seq_num="+sequenceNumber
			resultSet = databaseee.executePlaQuery(tempQuery);

			ResultSetMetaData metaData = resultSet.getMetaData();
			int columns = metaData.getColumnCount();
			logi.logInfo("Column count is  : "+columns)

			// Setting sequence number, debit and service description
			while (resultSet.next()) {
				tempValues = new String[columns]
				for (int col = 1; col <= columns; col++) {
					String columnValue = resultSet.getString(col);
					tempValues[col-1]=columnValue;
				}
			}

			logi.logInfo("Invoice line items : "+Arrays.deepToString(tempValues))

			for (int i = 0 ; i < tempValues.size()-1 ; i++) {
				rowWiseMap.put(keyValues.get(i), tempValues[i]);
			}

			String line_amount
			if(tempValues[2].contains('.'))
			{
				line_amount= (tempValues[2].toDouble()+getTaxInclusiveAmount(invoice_number,sequenceNumber.toString()).toDouble()).toString()
			}
			else
				line_amount= (tempValues[2].toInteger()+getTaxInclusiveAmount(invoice_number,sequenceNumber.toString()).toInteger()).toString()
			logi.logInfo("Invoice line amount value is : "+line_amount)
			rowWiseMap.put(keyValues.get(2), df.format(line_amount.toDouble()).toString());
			
			// Setting reversedValue to hash
			String reversedValue
			String lineReversedQuery = "Select NVL(SUM(CREDIT),0) from ariacore.refund_detail where invoice_no="+invoice_number+" and gl_seq_num="+sequenceNumber
			reversedValue = databaseee.executeQueryP2(lineReversedQuery);

			if(reversedValue.contains('.'))
			{
				if (reversedValue.split("\\.")[0].length() == 0)
					reversedValue = "0"+reversedValue
			}
			logi.logInfo("Reversed value is : "+reversedValue)
			rowWiseMap.put(keyValues.get(3), df.format(reversedValue.toDouble()).toString())

			// Setting reversible amount
			String reversibleAmtValue
			String reversibleAmtQuery

			reversibleAmtQuery = "SELECT NVL(SUM(a.REMAINING_REVERSIBLE_AMOUNT),0) TOTAL_REVERSIBLE_AMOUNT FROM(SELECT DEBIT -(SELECT NVL(SUM(CREDIT),0) REVERSED_AMOUNT FROM ARIACORE.REFUND_DETAIL RD  WHERE RD.INVOICE_NO="+invoice_number+" AND RD.GL_SEQ_NUM="+sequenceNumber+") REMAINING_REVERSIBLE_AMOUNT FROM ARIACORE.GL_DETAIL GLD WHERE GLD.SEQ_NUM ="+sequenceNumber+" AND GLD.INVOICE_NO="+invoice_number+" AND EXISTS  (SELECT * FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+account_number+" AND TRANSACTION_TYPE IN(2,3)))a"
			reversibleAmtValue = databaseee.executeQueryP2(reversibleAmtQuery);

			logi.logInfo("reversibleAmtValue before tax&coupon " +reversibleAmtValue)
			logi.logInfo("Parameters for getTaxInclusiveAmount "+invoice_number+" "+client_number+" "+sequenceNumber.toString())
			
			String inclusive_amt=getTaxInclusiveAmount(invoice_number,sequenceNumber.toString())
			logi.logInfo("Tax inclusive amount value is : "+inclusive_amt)
			String coupon_amt=getCouponAmount(account_number,invoice_number,sequenceNumber.toString())
			String finalreversibleAmtValue
			if(reversibleAmtValue.contains('.'))
			{
				finalreversibleAmtValue=(reversibleAmtValue.toDouble() +inclusive_amt.toDouble()-coupon_amt.toDouble()).toString()
				logi.logInfo("ReversibleAmtValue after applying taxinclusive is : "+finalreversibleAmtValue)
			}

			else
			{
				finalreversibleAmtValue=(reversibleAmtValue.toInteger() +inclusive_amt.toInteger()-coupon_amt.toInteger()).toString()
				logi.logInfo("ReversibleAmtValue after applying taxinclusive is : "+finalreversibleAmtValue)
			}

			def ddiscAmt
			if(finalreversibleAmtValue.contains('.'))
			{
				ddiscAmt=(finalreversibleAmtValue.toDouble() - paymentCreditBalance.toDouble())
				logi.logInfo("Raw reversible val after pay discount is : "+ddiscAmt)

			}else{
				ddiscAmt=(finalreversibleAmtValue.toInteger() - paymentCreditBalance.toInteger())
				logi.logInfo("Raw reversible val after pay discount is : "+ddiscAmt)
			}

			if(ddiscAmt < 0){	// when discount amount is less than 0
				balance =  Math.abs(ddiscAmt)
				rowWiseMap.put(keyValues.get(4), "0")
				logi.logInfo("Added to hash balance: "+balance)
			} else {	// when discount amount is greater than 0
				rowWiseMap.put(keyValues.get(4),  df.format(ddiscAmt.toString().toDouble()).toString())
				logi.logInfo("Added to hash and val is : "+rowWiseMap.get(keyValues.get(4)))
				balance = 0
			}
			paymentCreditBalance = balance
			logi.logInfo("Added to hash : "+paymentCreditBalance)

			// Setting the recurring val to the hash
			String recuring_val
			String recursiveTypeQuery = "SELECT RECURRING FROM ariacore.services WHERE service_no in (SELECT service_no FROM ariacore.gl_detail WHERE client_no = "+client_number+" AND invoice_no ="+invoice_number+" and seq_num ="+sequenceNumber+")"
			recuring_val = databaseee.executeQueryP2(recursiveTypeQuery);
			logi.logInfo("Recuring_val is : "+recuring_val)
			rowWiseMap.put(keyValues.get(5), recuring_val)

			// Setting Date values
			String dateGetQuery = "SELECT start_date FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.SERVICES SER ON GLD.SERVICE_NO=SER.SERVICE_NO WHERE CLIENT_NO="+client_number+" AND INVOICE_NO="+invoice_number+" AND SER.RECURRING=1"
			resultSet = databaseee.executePlaQuery(dateGetQuery)
			if (resultSet.next()) {
				date = resultSet.getDate(1)
			}
			logi.logInfo("Raw date is : "+date)

			if(recuring_val.contains("1")){
				rowWiseMap.put(keyValues.get(6), date)
			}

			// Adding invoice date
			String formatDateQuery = "SELECT to_number(TO_CHAR(start_date,'mm')) || '/' || to_number(TO_CHAR(start_date,'dd')) || '/' ||to_number(TO_CHAR(start_date,'yyyy')) START_DATE FROM ariacore.gl_detail GLD JOIN ARIACORE.SERVICES SER ON SER.SERVICE_NO =GLD.SERVICE_NO WHERE client_no ="+client_number+" AND invoice_no="+invoice_number+" AND SER.RECURRING=1"
			resultSet = databaseee.executePlaQuery(formatDateQuery)
			String seqDate
			if (resultSet.next()) {
				seqDate = resultSet.getString(1)
			}
			logi.logInfo("Raw date is : "+seqDate)
			rowWiseMap.put(keyValues.get(9), seqDate)

			logi.logInfo("Individual map for sequence "+sequenceNumber+" is :::: "+rowWiseMap)

		} else{	// when total invoice amount is comletely reversed

			logi.logInfo("The total line amount for line "+sequenceNumber+" is reversed completely")
		}
		return rowWiseMap;
	}


	/**
	 * Getting discounted amount after credit card payment
	 * @param testCaseId
	 * @return discounted amount
	 */
	public String md_get_discount_applied_through_creditcard_payment_api(String testCaseId){
		
		logi.logInfo "The Invoice line item after credit card discount ::  LLL"

		// Variable declarations
		String credit_Disc_Amt
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		logi.logInfo(testCaseCmb[0].toString())
		com.eviware.soapui.support.XmlHolder outputholder
		
		// Getting invoice total amount
		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains("create_acct_complete") && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				credit_Disc_Amt = outputholder.getNodeValue("//*:create_acct_completeResponse[1]/invoice_total_amount[1]")
			}
		}
		logi.logInfo "The Invoice line item after credit card discount ::  "+ credit_Disc_Amt
		return credit_Disc_Amt
	}

	/**
	 * Getting the invoice amount after credit card discount
	 * @param testCaseId
	 * @return discounted Invoice amount
	 */
	public String md_get_discount_applied_through_creditcard_payment(String testCaseId) {

		// Variable declarations
		ConnectDB dbase = new ConnectDB()
		ResultSet resSet
		String finalDiscAmt = "0"
		String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		String account_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String invoice_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String payment_Method =   getValueFromRequest("create_acct_complete","//pay_method")
		String currencyCode = getValueFromRequest("create_acct_complete","//currency_cd")
		String invoiceRecAmt = getValueFromResponse("create_acct_complete","//*:new_acct_invoice_line_items_row[2]/*:invoice_line_amount[1]")

		// Query
		String discount_Amount_Qry = "select discount_percentage from ariacore.client_payment_method where client_no = "+client_number+" and method_id ="+payment_Method
		String discount_Rule_No_Qry = "select discount_rule_no from ariacore.client_payment_method where client_no = "+client_number+" and method_id ="+payment_Method
		String invoice_qry = "SELECT DEBIT FROM ARIACORE.GL WHERE ACCT_NO="+account_No+" AND INVOICE_NO="+invoice_No
		String flatDiscountQry = "select NVL(Sum(AMOUNT),0) as flat_discount from ariacore.CLI_PAY_MTH_FLAT_DISC_CURR_MAP where CLIENT_NO = "+client_number+" and method_id = "+payment_Method+" and currency_cd ='"+currencyCode+"'"

		def flatDiscountAmount
		def totalInvoiceAmt
		def percentDiscount
		String disc_Rule_No
		boolean discAplicable = false
		
		// Getting the total invoice amount
		resSet = dbase.executePlaQuery(invoice_qry)
		if (resSet.next()) {
			totalInvoiceAmt = resSet.getObject(1)
		}
		logi.logInfo("Total invoice amount is :: "+totalInvoiceAmt)

		// Getting the discount percentage
		resSet = dbase.executePlaQuery(discount_Amount_Qry)
		if (resSet.next()) {
			percentDiscount = resSet.getObject(1)
			discAplicable = true
		}
		logi.logInfo("Discount percentage for credit card payment :: "+percentDiscount)

		// Getting discount rule number
		resSet = dbase.executePlaQuery(discount_Rule_No_Qry)
		if (resSet.next()) {
			disc_Rule_No = resSet.getString(1)
		}
		logi.logInfo("Rule no :: "+disc_Rule_No)

		// Getting the flat discount amount
		resSet = dbase.executePlaQuery(flatDiscountQry)
		if (resSet.next()) {
			flatDiscountAmount = resSet.getObject(1)
		}
		logi.logInfo("flatDiscountAmount :: "+flatDiscountAmount)

		// Calculating the final discounted amount after credit discount
		if(discAplicable == true && disc_Rule_No.toInteger() == 2){
			def percentageDisc = (totalInvoiceAmt * percentDiscount) / 100
			finalDiscAmt = totalInvoiceAmt - percentageDisc
		} else if(discAplicable == true && disc_Rule_No.toInteger() == 3){
			def percentageDisc = (invoiceRecAmt.toInteger() * percentDiscount) / 100
			finalDiscAmt = totalInvoiceAmt - percentageDisc
		}else if(disc_Rule_No.toInteger() == 1){
			finalDiscAmt = totalInvoiceAmt - flatDiscountAmount
		}
		if(isCouponApplied(invoice_No, client_number) == true){
			def coupon_amt = md_get_total_refundable_amount_after_coupon(testCaseId)
			def tempAmt = finalDiscAmt.toInteger() - coupon_amt
			logi.logInfo("Discount final :: "+tempAmt)
			finalDiscAmt = tempAmt
		}
		return finalDiscAmt
	}

	// Getting the issue refund to account line items
	HashMap md_CHECK_IRTA_LINE_ITEMS_API1(String tsetid)
	{
		return md_CHECK_IRTA_LINE_ITEMS_API(tsetid,"1")
	}

	HashMap md_CHECK_IRTA_LINE_ITEMS_DB1(String tsetid)
	{
		return  md_CHECK_IRTA_LINE_ITEMS_DB(tsetid,"1")
	}

	HashMap md_CHECK_IRTA_LINE_ITEMS_API2(String tsetid)
	{
		return md_CHECK_IRTA_LINE_ITEMS_API(tsetid,"2")
	}

	HashMap md_CHECK_IRTA_LINE_ITEMS_DB2(String tsetid)
	{
		return  md_CHECK_IRTA_LINE_ITEMS_DB(tsetid,"2")
	}


	/**
	 * Getting the line item details from issue_refund_to_acct api
	 * @param tcid
	 * @param lineno
	 * @return line items details
	 */
	def md_CHECK_IRTA_LINE_ITEMS_API(String tcid,String lineno)
	{
		logi.logInfo ("Calling Verification method  md_CHECK_IRTA_LINE_ITEMS_API")
		
		// Variable declarations
		String apiname=tcid.split("-")[2]
		def inv_no=getValueFromRequest(apiname,"//invoices_to_reverse_row["+lineno+"]/invoice_no").toString()
		def line_no=getValueFromRequest(apiname,"//invoices_to_reverse_row["+lineno+"]/invoice_line_no").toString()
		def rev_amt = getValueFromRequest(apiname,"//invoices_to_reverse_row["+lineno+"]/invoice_line_reversing_amount").toString()

		
		logi.logInfo("inv_no"+inv_no+" line_no"+line_no+" rev_amt"+rev_amt)
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap apirow = new HashMap();
		com.eviware.soapui.support.XmlHolder outputholder
		def rep_rw_cnt= getNodeCountFromResponse("issue_refund_to_acct","//*/*:reversed_invoice_lines_row").toString()
		logi.logInfo("row cnt"+rep_rw_cnt)
		logi.logInfo(xmlValues.size().toString())

		for(int i=1;i<=rep_rw_cnt.toInteger();i++)
		{

			def res_inv_no=getValueFromResponse(apiname,"//*:reversed_invoice_lines_row["+i+"]/*:invoice_no[1]").toString()
			def res_line_no = getValueFromResponse(apiname,"//*:reversed_invoice_lines_row["+i+"]/*:invoice_line_no[1]").toString()
			def res_rev_amt = getValueFromResponse(apiname,"//*:reversed_invoice_lines_row["+i+"]/*:invoice_line_reversed_amount[1]").toString()

			logi.logInfo("res_inv_no"+res_inv_no+" res_line_no"+res_line_no+" res_rev_amt"+res_rev_amt)

			if(res_inv_no.equals(inv_no) && res_line_no.equals(line_no) || res_rev_amt.equals(rev_amt) )
			{
				// Adding the line item details to the hash
				for(int j=0; j<testCaseCmb.size(); j++) {
					if(testCaseCmb[j].toString().contains('issue_refund_to_acct') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
						outputholder = xmlValues[j]
						apirow.put('INVOICE_NO', outputholder.getNodeValue("//*/*/*:reversed_invoice_lines_row["+i+"]/*:invoice_no[1]"))
						apirow.put("INVOICE_LINE_NO",outputholder.getNodeValue("//*/*/*:reversed_invoice_lines_row["+i+"]/*:invoice_line_no[1]"))
						apirow.put("INVOICE_LINE_SERVICE_NO",outputholder.getNodeValue("//*/*/*:reversed_invoice_lines_row["+i+"]/*:invoice_line_service_no[1]"))
						apirow.put("INVOICE_LINE_REVERSED_AMOUNT",outputholder.getNodeValue("//*/*/*:reversed_invoice_lines_row["+i+"]/*:invoice_line_reversed_amount[1]"))
						apirow.put("INVOICE_LINE_REVERSING_DATE",outputholder.getNodeValue("//*/*/*:reversed_invoice_lines_row["+i+"]/*:invoice_line_reversing_date[1]"))
						apirow.put("INVOICE_LINE_COMMENTS",outputholder.getNodeValue("//*/*/*:reversed_invoice_lines_row["+i+"]/*:invoice_line_comments[1]"))

					}
				}

			}

		}

		logi.logInfo(apirow.toString())
		return apirow;
	}

	/**
	 * Getting the line item details from issue_refund_to_acct from database
	 * @param tcid
	 * @param seqno
	 * @return line items details
	 */
	def md_CHECK_IRTA_LINE_ITEMS_DB(String tcid,String seqno)
	{
		logi.logInfo ("Calling Verification method  md_CHECK_IRTA_LINE_ITEMS_DB")
		String apiname=tcid.split("-")[2]
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def inv_no=getValueFromRequest(apiname,"//invoices_to_reverse_row["+seqno+"]/invoice_no")
		def line_no=getValueFromRequest(apiname,"//invoices_to_reverse_row["+seqno+"]/invoice_line_no")
		
		// Query
		String query="Select rd.invoice_no as invoice_no,rd.gl_seq_num as invoice_line_no,gd.service_no as invoice_line_service_no,rd.credit as invoice_line_reversed_amount,TO_CHAR(reversing_date,'yyyy-MM-DD') as invoice_line_reversing_date,gd.comments as invoice_line_comments  from ariacore.refund_detail rd JOIN ariacore.gl_detail gd ON rd.invoice_no=gd.invoice_no AND rd.gl_seq_num=gd.seq_num where rd.invoice_no="+inv_no+" and rd.gl_seq_num="+line_no
		ResultSet resultSet = db.executePlaQuery(query);
		ResultSetMetaData md = resultSet.getMetaData();
		int columns = md.getColumnCount();
		HashMap row = new HashMap();

		// Iterating through the columns
		while (resultSet.next()){
			for(int i=1; i<=columns; i++){
				row.put(md.getColumnName(i),resultSet.getObject(i));
			}

		}
		logi.logInfo(row.toString())
		return row
	}

	/**
	 * Get the total charges from create_order api
	 * @param tcid
	 * @return order details
	 */
	def md_CREATE_ORDER_TOTAL_CHARGES_API(String tcid)
	{
		logi.logInfo("md_CRETAE_ORDER_TOTAL_CHARGES_API")
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap apivals=new HashMap()
		com.eviware.soapui.support.XmlHolder outputholder

		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains('create_order') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				
				// Setting the values to the hash
				apivals.put("TOTAL_CHARGES_BEFORE_TAX", outputholder.getNodeValue("//*/total_charges_before_tax[1]"))
				apivals.put("TOTAL_TAX_CHARGES",outputholder.getNodeValue("//*/total_tax_charges[1]"))
				apivals.put("TOTAL_CHARGES_AFTER_TAX",outputholder.getNodeValue("//*/total_charges_after_tax[1]"))
			}
		}
		
		if(apivals.get("TOTAL_TAX_CHARGES").toString().equals("null"))
		{
			apivals.put("TOTAL_TAX_CHARGES","0")
		}
		return apivals;
	}

	/**
	 * Get the total charges from create_order database
	 * @param tcid
	 * @return order details
	 */
	def md_CREATE_ORDER_TOTAL_CHARGES_DB(String tcid)
	{
		logi.logInfo("md_CRETAE_ORDER_TOTAL_CHARGES_DB")
		
		// Variable declarations
		String apiname=tcid.split("-")[2]
		String rate
		def seqno
		def sum=0
		def tax_rate
		def tax_charges
		
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_ACCT_NO)
		def inv_no=getValueFromResponse(apiname,ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)
		String units = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNITS)
		def order_name=getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_ORDER_NAME)

		// Hash declarations
		seqs=new HashMap()
		HashMap row = new HashMap();
		
		// Query for getting rate
		String rate_qry= "Select INVIP.price from ariacore.inventory_item_prices INVIP  JOIN ariacore.INVENTORY_ITEMS INVI ON invip.item_no=invi.item_no and invip.client_no=invi.client_no where INVI.client_sku='"+order_name+"' and INVI.client_no="+client_no+" and invip.currency_cd=(SELECT CURRENCY_CD FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+ " AND CLIENT_NO="+client_no+")"
		logi.logInfo("rate_qry :"+rate_qry)

		ResultSet resultSet = db.executePlaQuery(rate_qry);

		while(resultSet.next())
		{
			rate=resultSet.getObject(1).toString()
		}
		logi.logInfo("rate" + rate.toString())

		if(rate.toString().contains("."))
		{
			charges= (units.toInteger()) * rate.toDouble()
		}
		else
		{
			charges= (units.toInteger()) * rate.toInteger()
		}

		logi.logInfo("charges"+charges)

		// Total charges before tax
		row.put("TOTAL_CHARGES_BEFORE_TAX",charges)

		// Seq num query
		String seq_qry= "Select seq_num from ARIACORE.gl_tax_detail where invoice_no="+inv_no+ " order by seq_num asc"
		ResultSet rs_seq_qry = db.executePlaQuery(seq_qry);
		int k=1;

		while (rs_seq_qry.next())
		{
			seqno = rs_seq_qry.getObject(1)
			seqs.put("tax"+k, seqno)
			k++
		}
		logi.logInfo("seqs array"+seqs)

		for(int i=1;i<=seqs.size();i++)
		{
			def s_no=seqs.get("tax"+i)
			String tax_qry = "Select tax_rate from ARIACORE.gl_tax_detail where invoice_no="+inv_no+" and seq_num="+s_no
			ResultSet rs_tax = db.executePlaQuery(tax_qry);
			while(rs_tax.next())
			{
				tax_rate = rs_tax.getObject(1)
				tax_charges= (charges * (tax_rate*100).toInteger())/100
				logi.logInfo("tax charges"+ tax_charges)
			}
			sum= sum + tax_charges
		}

		// Total charges
		logi.logInfo("total tax"+ sum)
		row.put("TOTAL_TAX_CHARGES",sum)

		def total = charges + sum
		
		// Total charges after tax
		logi.logInfo("all total "+ total)
		row.put("TOTAL_CHARGES_AFTER_TAX",total)
		return row;
	}


	/**
	 * Getting the full balance
	 * @param tcid
	 * @return
	 */
	def md_GET_FULL_BALANCE(String tcid)
	{
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def inv_amt
		def refund_amt
		def void_amt
		def payment_amt

		// Getting invoice amount value
		String inv_qry="Select amount from ariacore.acct_transaction where acct_no=" +acct_no+" and transaction_type=1"
		ResultSet resultSet_inv_qry = db.executePlaQuery(inv_qry);
		if(resultSet_inv_qry.next())
		{
			inv_amt=resultSet_inv_qry.getObject(1)
		}
		logi.logInfo("inv_amt"+inv_amt)

		// Getting Refund amount
		String refund_qry = "select sum(amount) from ariacore.acct_transaction where TRANSACTION_TYPE in (12,13) and acct_no ="+acct_no
		ResultSet resultSet_refund_qry = db.executePlaQuery(refund_qry);
		if(resultSet_refund_qry.next())
		{
			refund_amt=resultSet_refund_qry.getObject(1)
		}
		logi.logInfo("refund_amt"+refund_amt)
		
		// Getting voided amount
		String void_qry = "select sum(amount) from ARIACORE.acct_trans_void_history_view where acct_no ="+acct_no
		ResultSet resultSet_void_qry = db.executePlaQuery(void_qry);
		if(resultSet_void_qry.next())
		{
			void_amt=resultSet_void_qry.getObject(1)
		}
		logi.logInfo("void_amt"+void_amt)

		// Getting total transaction amount
		String payment_qry = "Select sum(amount) from ariacore.acct_transaction where acct_no="+acct_no+" and transaction_type in(2,3)"
		ResultSet resultSet_payment_qry = db.executePlaQuery(payment_qry);
		if(resultSet_payment_qry.next())
		{
			payment_amt=resultSet_payment_qry.getObject(1)
		}
		logi.logInfo("payment_amt"+payment_amt)
		
		// Calculating the balance amount
		def bal=(inv_amt+refund_amt+void_amt) - payment_amt
		logi.logInfo ("bal from api"+bal)
		return bal
	}

	/**
	 * Getting the full balance from database
	 * @param tcid
	 * @return balance amount
	 */
	def md_GET_FULL_BALANCE_DB (String tcid)
	{
		logi.logInfo("calling md_GET_FULL_BALANCE_DB ")

		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def total_charges
		def total_credits

		// Getting the acct total charges
		String chgqry="Select total_charges from ariacore.acct_total_charges where acct_no="+acct_no
		ResultSet resultSet_charge = db.executePlaQuery(chgqry);
		if(resultSet_charge.next())
		{
			total_charges=resultSet_charge.getObject(1)
		}
		logi.logInfo ("Total charges : "+total_charges)

		// Getting the acct credit amount
		String crdqry="Select total_credits from ariacore.acct_total_credits where acct_no="+acct_no
		ResultSet resultSet_credit= db.executePlaQuery(crdqry);
		if(resultSet_credit.next())
		{
			total_credits=resultSet_credit.getObject(1)
		}
		logi.logInfo ("Total Credits : "+total_credits)

		// Calculating balance
		def full_bal=total_charges - total_credits
		logi.logInfo ("Total Credits : "+full_bal.toString())
		return full_bal
	}

	/**
	 * Getting reversible Federal tax
	 * @param testCaseId
	 * @return federal tax
	 */
	def md_GET_REVERSIBLE_FEDERAL_TAX(String testCaseId){
		String seqNum
		seqNum = getSequenceNumberForTaxItems("400")
		return getReversibleTaxValueFromResponseAsHash(seqNum)
	}
	
	/**
	 * Getting reversible State tax
	 * @param testCaseId
	 * @return State tax
	 */
	def md_GET_REVERSIBLE_STATE_TAX(String testCaseId){
		String seqNum
		seqNum = getSequenceNumberForTaxItems("401")
		return getReversibleTaxValueFromResponseAsHash(seqNum)
	}

	/**
	 * Getting reversible State tax line details
	 * @param testCaseId
	 * @return State tax details
	 */
	def md_GET_REVERSIBLE_STATE_TAX_LINE_ITEM(String testCaseId){
		String seqNum
		seqNum = getSequenceNumberForTaxItems("401")
		return getReversibleTaxValueFromAPI(seqNum)
	}

	/**
	 * Getting reversible All tax line details
	 * @param testCaseId
	 * @return All tax details
	 */
	def md_GET_REVERSIBLE_ALL_TAX_LINE_ITEM(String testCaseId){
		String seqNum
		LinkedHashMap<String, String> taxesAPI = new LinkedHashMap<String, String>()

		for (int taxIndex=400; taxIndex<=410; taxIndex++)
		{
			seqNum = getSequenceNumberForTaxItems(taxIndex.toString())
			if (seqNum != "0" && seqNum != null)
			{
				String seqNumTaxName = db.executeQueryP2("select service_name from all_service where service_no = " + taxIndex);
				taxesAPI.put taxIndex.toString() + "-" + seqNumTaxName ,  getReversibleTaxValueFromAPI(seqNum)
			}
		}
		return taxesAPI
	}
	
	/**
	 * Getting reversible County tax
	 * @param testCaseId
	 * @return County tax
	 */
	def md_GET_REVERSIBLE_COUNTRY_TAX(String testCaseId){
		String seqNum
		seqNum = getSequenceNumberForTaxItems("402")
		return getReversibleTaxValueFromResponseAsHash(seqNum)
	}
	
	/**
	 * Getting reversible District tax
	 * @param testCaseId
	 * @return District tax
	 */
	def md_GET_REVERSIBLE_DISTRICT_TAX(String testCaseId){
		String seqNum
		seqNum = getSequenceNumberForTaxItems("405")
		return getReversibleTaxValueFromResponseAsHash(seqNum)
	}
	
	/**
	 * Getting the total refundable amount after coupon discount
	 * @param testCaseId
	 * @return Values as Hash
	 */
	def md_get_total_refundable_amount_after_coupon(String testCaseId){
		String accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo("Coupon Account number is : "+accountNo)
		String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		logi.logInfo("Coupon Invoice number is :  "+invoiceNo)
		return get_Total_Invoice_Amount_After_Coupon(accountNo,invoiceNo)
	}
	
	/**
	 * Getting the total refundable amount after coupon discount
	 * @param testCaseId
	 * @return returns the details of refundable items
	 */
	public String get_Total_Invoice_Amount_After_Coupon(String accountNo,String invoice_number)
	{
		logi.logInfo( "Calling getCouponAmount")
		
		// Variable declarations
		String totalPaymentAmount
		String totalCouponAmount
		String totalRefundedAmount

		String totalRefundableAmount
		String totalInvoiceAmountAfterCoupon = 0
		
		String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		String refund_amtExt
		String refund_amtElect
		ConnectDB db = new ConnectDB()
		
		// Query
		String totalAmtQry = "SELECT DEBIT INVOICE_AMT_BEFORE_COUPON FROM ARIACORE.GL WHERE ACCT_NO="+accountNo  //+" AND INVOICE_NO="+invoice_number
		String refund_externalPayment = "SELECT NVL(SUM(AMOUNT),0) REFUNDED_AMOUNT FROM ARIACORE.CHECK_REFUND  WHERE ACCT_NO="+accountNo
		String refund_electronicPayment = "SELECT NVL(SUM(AMOUNT),0) FROM ARIACORE.MANAGED_REFUNDS WHERE ACCT_NO="+accountNo
		String couponAmtQry = " SELECT NVL(SUM(DISTILLED_AMOUNT),0) AS COUPON_AMT FROM (SELECT ELIGIBLE_SERVICE_NO, DISTILLED_AMOUNT FROM ariacore.all_recur_credit WHERE acct_no = (SELECT acct_no FROM ariacore.gl WHERE invoice_no="+invoice_number+") UNION SELECT CDR.KEY_2_VAL AS ELIGIBLE_SERVICE_NO, CASE WHEN CDRS.INLINE_OFFSET_IND='I' THEN 0 ELSE AIDD.ACTUAL_DISCOUNT_AMT END AS DISTILLED_AMOUNT FROM ARIACORE.CLIENT_DR_COMPONENTS CDR JOIN ariacore.all_invoice_discount_details AIDD ON AIDD.rule_no=CDR.rule_no JOIN ariacore.CLIENT_DISCOUNT_RULES CDRS ON CDRS.rule_no =AIDD.rule_no WHERE AIDD.invoice_no ="+invoice_number+")a"

		// Getting total payment amount
		totalPaymentAmount = db.executeQueryP2(totalAmtQry);
		logi.logInfo "Total payment amount : "+ totalPaymentAmount

		// Getting the refund amount - External payment
		refund_amtExt = db.executeQueryP2(refund_externalPayment);
		logi.logInfo "Coupon Against External payment : "+ refund_amtExt

		// Getting the refund amount - Electronic payment
		refund_amtElect = db.executeQueryP2(refund_electronicPayment);
		logi.logInfo "Coupon Against Electronic payment : "+ refund_amtElect

		if(refund_amtExt.contains(".") || refund_amtElect.contains(".")){
			totalRefundedAmount = (refund_amtExt.toDouble() + refund_amtElect.toDouble())
		} else {
			totalRefundedAmount = (refund_amtExt.toInteger() + refund_amtElect.toInteger())
		}

		// Getting the total coupon amount
		totalCouponAmount = db.executeQueryP2(couponAmtQry);
		logi.logInfo "Total coupon applied amount : "+ totalCouponAmount

		if(totalPaymentAmount.toString().contains(".") || totalCouponAmount.contains(".")){
			totalInvoiceAmountAfterCoupon = (totalPaymentAmount.toDouble() - totalCouponAmount.toDouble() - totalRefundedAmount.toDouble())
		} else {
			totalInvoiceAmountAfterCoupon = (totalPaymentAmount.toInteger() - totalCouponAmount.toInteger() - totalRefundedAmount.toInteger())
		}

		logi.logInfo("Total refundable amount is : " +totalInvoiceAmountAfterCoupon.toString())
		return totalInvoiceAmountAfterCoupon
	}

	/**
	 * Getting the total refundable amount after coupon discount from api
	 * @param testCaseId
	 * @return returns the details of refundable items
	 */
	def md_get_refundable_payment_after_coupon_api(String lineno)
	{
		String tcid= Constant.TESTCASEID
		String refundable_amount_after_coupon
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		com.eviware.soapui.support.XmlHolder outputholder
		logi.logInfo(xmlValues.size().toString())

		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains('get_refundable_payments') && testCaseCmb[j].toString().contains(tcid)) {
				outputholder = xmlValues[j]
				refundable_amount_after_coupon = outputholder.getNodeValue("//*:get_refundable_paymentsResponse[1]/refundable_payments[1]/*:refundable_payments_row[1]/*:payment_amount[1]")
			}
		}

		logi.logInfo("Total refundable payment after coupon : "+refundable_amount_after_coupon)
		return refundable_amount_after_coupon;
	}

	def md_get_total_refundable_amount_after_coupon_api(String testCaseId){
		return md_get_refundable_payment_after_coupon_api("1")
	}

	/**
	 * Getting reversible invoice Federal tax
	 * @param testCaseId
	 * @return Federal tax
	 */
	def md_get_reversible_tax_invoice_for_federalTax(String testCaseId) {
		String seqNum
		seqNum = getSequenceNumberForTaxItems("400")
		return md_GET_REVERSIBLE_TAX_INVS_BY_PAYMENT(seqNum)
	}
	
	/**
	 * Getting reversible invoice State tax
	 * @param testCaseId
	 * @return State tax
	 */
	def md_get_reversible_tax_invoice_for_stateTax(String testCaseId) {
		String seqNum
		seqNum = getSequenceNumberForTaxItems("401")
		return md_GET_REVERSIBLE_TAX_INVS_BY_PAYMENT(seqNum)
	}

	/**
	 * Getting reversible invoice State tax line item
	 * @param testCaseId
	 * @return State tax line item
	 */
	def md_get_reversible_tax_invoice_for_stateTax_line_item(String testCaseId) {
		String seqNum
		seqNum = getSequenceNumberForTaxItems("401")
		return md_GET_REVERSIBLE_TAX_LINE_ITEM_BY_PAYMENT(seqNum)
	}

	def md_get_reversible_tax_invoice_for_ALLTax_line_item(String testCaseId) {
		String seqNum

		LinkedHashMap<String, String> taxesDB = new LinkedHashMap<String, String>()

		for (int taxIndex=400; taxIndex<=410; taxIndex++)
		{
			seqNum = getSequenceNumberForTaxItems(taxIndex.toString())
			if (seqNum != "0" && seqNum != null)
			{
				String seqNumTaxName = db.executeQueryP2("select service_name from all_service where service_no = " + taxIndex);
				taxesDB.put taxIndex.toString() + "-" + seqNumTaxName ,  md_GET_REVERSIBLE_TAX_LINE_ITEM_BY_PAYMENT(seqNum)
			}
		}
		return taxesDB
	}

	/**
	 * Getting reversible invoice County tax
	 * @param testCaseId
	 * @return County tax
	 */
	def md_get_reversible_tax_invoice_for_countryTax(String testCaseId) {
		String seqNum
		seqNum = getSequenceNumberForTaxItems("402")
		return md_GET_REVERSIBLE_TAX_INVS_BY_PAYMENT(seqNum)
	}
	
	/**
	 * Getting reversible invoice District tax
	 * @param testCaseId
	 * @return District tax
	 */
	def md_get_reversible_tax_invoice_for_districtTax(String testCaseId) {
		String seqNum
		seqNum = getSequenceNumberForTaxItems("405")
		return md_GET_REVERSIBLE_TAX_INVS_BY_PAYMENT(seqNum)
	}

	/**
	 * Getting line items for all taxes
	 * @param testCaseId
	 * @return All tax line items
	 */
	String getSequenceNumberForTaxItems(String serviceNo){
		String seqNum
		String invoice_num = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String tempQuery = "select seq_num from ariacore.gl_detail where service_no = "+serviceNo+" and invoice_no ="+invoice_num
		seqNum = db.executeQueryP2(tempQuery);
		return seqNum
	}

	/**
	 * Getting the reversible tax line items
	 * @param seqNum
	 * @return
	 */
	HashMap<String,String> md_GET_REVERSIBLE_TAX_INVS_BY_PAYMENT(String seqNum) {

		// Variable declarations
		HashMap<String, String> taxLineItemMap
		ConnectDB databasee = new ConnectDB()
		String taxableServiceNo

		logi.logInfo("Account number is : "+ getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1))
		String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		logi.logInfo("Invoice number is :  "+invoiceNo)

		String taxSequenceNo = "select service_no from ariacore.gl_detail where invoice_no = "+invoiceNo+" and seq_num ="+seqNum
		taxableServiceNo = databasee.executeQueryP2(taxSequenceNo)
		logi.logInfo("service number for is :::::::::: "+taxableServiceNo)

		taxLineItemMap = get_tax_invoice_line_Items(getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1),invoiceNo,taxableServiceNo)
		logi.logInfo("The taxable hash map got from the method is : "+taxLineItemMap)
		return taxLineItemMap
	}

	/**
	 * Getting the reversible tax line items
	 * @param seqNum
	 * @return tax line items
	 */
	public String md_GET_REVERSIBLE_TAX_LINE_ITEM_BY_PAYMENT(String seqNum) {

		String taxLineItemMap
		ConnectDB databasee = new ConnectDB()
		String taxableServiceNo

		logi.logInfo("Account number is : "+getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1))
		String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		logi.logInfo("Invoice number is :  "+invoiceNo)

		String taxSequenceNo = "select service_no from ariacore.gl_detail where invoice_no = "+invoiceNo+" and seq_num ="+seqNum
		taxableServiceNo = databasee.executeQueryP2(taxSequenceNo)
		logi.logInfo("service number for is : "+taxableServiceNo)

		taxLineItemMap = get_tax_invoice_tax_line_Item_DB(getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1),invoiceNo,taxableServiceNo)
		logi.logInfo("The taxable hash map got from the method is : "+taxLineItemMap)
		return taxLineItemMap
	}

	/**
	 * Getting the invoice tax line items from API
	 * @param account_Number
	 * @param invoice_No
	 * @param taxServiceNo
	 * @return tax items as hash
	 */
	public HashMap<String,String> get_tax_invoice_line_Items(String account_Number,String invoice_No,String taxServiceNo){

		ConnectDB taxConnection = new ConnectDB()
		String sequenceNum
		String invoiceDate

		String client_Number = Constant.mycontext.expand('${Properties#Client_No}')
		String total_Tax_Amount_Qry = "SELECT debit FROM ariacore.gl_detail WHERE client_no = "+client_Number+" AND invoice_no  = "+invoice_No+" AND service_no IN ("+taxServiceNo+")"
		String reversed_Tax_Amount_Qry = "Select NVL(SUM(CREDIT),0) from ariacore.refund_tax_detail WHERE INVOICE_NO = "+invoice_No
		String dateGetQuery = "SELECT to_number(TO_CHAR(start_date,'mm')) || '/' || to_number(TO_CHAR(start_date,'dd')) || '/' ||to_number(TO_CHAR(start_date,'yyyy')) START_DATE FROM ariacore.gl_detail GLD JOIN ARIACORE.SERVICES SER ON SER.SERVICE_NO =GLD.SERVICE_NO WHERE client_no ="+client_Number+" AND invoice_no="+invoice_No+" AND SER.RECURRING=1"
		String tax_Sequence_Query = "select seq_num from ariacore.gl_detail where invoice_no = "+invoice_No+" and service_no in ("+taxServiceNo+")"

		HashMap<String, String> invoice_Tax_Values = new HashMap<String,String>()
		List keyValues = [
			"invoiced_acct_no",
			"invoice_no",
			"invoice_date",
			"invoice_line_no",
			"invoice_line_description",
			"invoice_line_amount",
			"invoice_line_reversed_amount",
			"invoice_line_reversible_amount"
		];

		String total_Invoice_Tax_Amt
		String total_Reversed_Tax_Amt
		String remaining_Reversible_Tax_Amt
		ResultSet resultSet
		int countTax=1

		invoice_Tax_Values.put(keyValues.get(0),account_Number)
		invoice_Tax_Values.put(keyValues.get(1),invoice_No)

		// Invoice date
		invoiceDate = taxConnection.executeQueryP2(dateGetQuery)
		invoice_Tax_Values.put(keyValues.get(2),invoiceDate)
		logi.logInfo("Tax Invoice Date : "+invoiceDate)

		// Invoice line number
		sequenceNum = taxConnection.executeQueryP2(tax_Sequence_Query)
		invoice_Tax_Values.put(keyValues.get(3),sequenceNum)
		logi.logInfo("Tax invoice SequenceNum : "+sequenceNum)

		// Description
		String taxDetail_Qry = "Select comments from ariacore.gl_detail where client_no="+client_Number+" and invoice_no="+invoice_No+" and seq_num="+sequenceNum
		String description
		description = taxConnection.executeQueryP2(taxDetail_Qry)
		invoice_Tax_Values.put(keyValues.get(4),description)
		logi.logInfo("Tax description : "+description)

		// Total tax amount
		total_Invoice_Tax_Amt = taxConnection.executeQueryP2(total_Tax_Amount_Qry)
		invoice_Tax_Values.put(keyValues.get(5),total_Invoice_Tax_Amt)
		logi.logInfo(" Tax total_Invoice_Tax_Amt : "+total_Invoice_Tax_Amt)

		// Total reversed tax
		total_Reversed_Tax_Amt = taxConnection.executeQueryP2(reversed_Tax_Amount_Qry)
		invoice_Tax_Values.put(keyValues.get(6),total_Reversed_Tax_Amt)
		logi.logInfo("Tax total_Reversed_Tax_Amt : "+total_Reversed_Tax_Amt)

		// Remaining amt
		if(total_Invoice_Tax_Amt.contains('.'))
		{
			invoice_Tax_Values.put(keyValues.get(7),(total_Invoice_Tax_Amt.toDouble() - total_Reversed_Tax_Amt.toDouble()))
			logi.logInfo("Tax remaining_Reversible_Tax_Amt"+ (total_Invoice_Tax_Amt.toDouble() - total_Reversed_Tax_Amt.toDouble()).toString())
		}
		else
		{
			invoice_Tax_Values.put(keyValues.get(7),(total_Invoice_Tax_Amt.toInteger() - total_Reversed_Tax_Amt.toInteger()))
			logi.logInfo("Tax remaining_Reversible_Tax_Amt"+ (total_Invoice_Tax_Amt.toDouble() - total_Reversed_Tax_Amt.toDouble()).toString())
		}
		return invoice_Tax_Values
	}

	/**
	 * Getting the invoice tax line items from DB
	 * @param account_Number
	 * @param invoice_No
	 * @param taxServiceNo
	 * @return tax items as hash
	 */
	public String get_tax_invoice_tax_line_Item_DB(String account_Number,String invoice_No,String taxServiceNo){

		ConnectDB taxConnection = new ConnectDB()
		String sequenceNum
		String invoiceDate

		String client_Number = Constant.mycontext.expand('${Properties#Client_No}')
		String total_Tax_Amount_Qry = "SELECT debit FROM ariacore.gl_detail WHERE client_no = "+client_Number+" AND invoice_no  = "+invoice_No+" AND service_no IN ("+taxServiceNo+")"

		// Total tax amount
		String total_Invoice_Tax_Amt
		total_Invoice_Tax_Amt = taxConnection.executeQueryP2(total_Tax_Amount_Qry)
		logi.logInfo(" Tax total_Invoice_Tax_Amt : "+total_Invoice_Tax_Amt)


		// Remaining amt
		if(total_Invoice_Tax_Amt.contains('.'))
		{
			total_Invoice_Tax_Amt=(total_Invoice_Tax_Amt.toDouble().round(2)).toString()
		}
		logi.logInfo(" Tax total_Invoice_Tax_Amt : "+total_Invoice_Tax_Amt)
		return total_Invoice_Tax_Amt
	}

	def md_check_initial_contract_status(String testCaseId) {
		def acct_no=getValueFromRequest('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String query="SELECT STATUS_CD FROM ARIACORE.ACCT_PLAN_CONTRACTS WHERE ACCT_NO=" + acct_no
		logi.logInfo (query)

		ResultSet resultSet = db.executePlaQuery(query);
		while(resultSet.next()){
			if(resultSet.getString((1) != "-3") {  return "false" } }
		return "TRUE"
	}

	def md_acctMasterPlanContractStatus(String testCaseId) {
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String masterContractStatus = db.executeQueryP2("SELECT STATUS_CD from ARIACORE.ACCT_PLAN_CONTRACTS WHERE CONTRACT_NO=(SELECT CONTRACT_NO from ARIACORE.ACCT_PLAN_CONTRACTS WHERE ACCT_NO="+accountNo+" AND PLAN_NO=(select plan_no from ariacore.acct where acct_no="+accountNo+" and client_no="+clientNo+")and rownum = 1)")
		return masterContractStatus
	}

	public String md_checkEarlyCancellationCharge(String testCaseId)
	{
		logi.logInfo "Entering into md_checkEarlyCancellationCharge"
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String contractNo=Constant.mycontext.expand('${Properties#ContractNo}')
		String earlyCancellationCharge = db.executeQueryP2("SELECT early_cancel_fee from ariacore.acct_plan_contracts where client_no="+clientNo+" and acct_no="+accountNo+ " and contract_no="+contractNo)
		//String earlyCancellationCharge = db.executeQueryP2("SELECT early_cancel_fee from ariacore.acct_plan_contracts where client_no="+clientNo+" and acct_no="+accountNo+ "ORDER BY CREATE_DATE DESC" )
		logi.logInfo("earlyCancellationCharge "+earlyCancellationCharge)
		def totalInvoice
		def expInvoice = md_ExpInvoice_ByPlan(testCaseId)
		logi.logInfo("expInvoice "+expInvoice)
		def expCancellationFee=earlyCancellationCharge
		totalInvoice =  expInvoice.toDouble() + expCancellationFee.toDouble()
		logi.logInfo("totalInvoice "+totalInvoice)
		DecimalFormat df=new DecimalFormat("#.##")
		return df.format(totalInvoice.toDouble()).toString();
	}

	String md_SERVICE_CREDIT_AMT_AFTER_GEN_INVOICE(String testcaseId)
	{
		String gen_inv_no=getValueFromResponse("gen_invoice", "//*/*:invoice_no[1]")
		String service_credit_id=getValueFromResponse("apply_service_credit","//*:credit_id[1]")
		String query="SELECT (sum(debit)*-1) from ariacore.gl_detail where invoice_no="+gen_inv_no+" and orig_credit_id="+service_credit_id
		def amt
		ResultSet resultSet = db.executePlaQuery(query);
		if (resultSet.next()) {
			amt = resultSet.getLong(1)
		}

		return amt.toString()
	}
	def waitforSpecificCondition(int waitCondition) throws Exception{
		for(int delay=0;delay<=waitCondition;delay++){
			try {
				Thread.sleep(1000)
			} catch (Exception e) {
				e.printStackTrace()
			}
		}
	}

	/**
	 * Gets days between current date and bill start date
	 * @param testcaseId
	 * @return number of days
	 */
	def md_GET_DURATION_FROM_DATE(String testcaseId){
		def start_date=getValueFromResponse('get_acct_multiplan_contract',ExPathRpcEnc.GET_ACCT_MULTIPLAN_START_DATE)
		def end_date=getValueFromResponse('get_acct_multiplan_contract',ExPathRpcEnc.GET_ACCT_MULTIPLAN_END_DATE)

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate,endDate;

		startDate = df.parse(start_date);
		endDate = df.parse(end_date);
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(startDate);
		cal2.setTime(endDate);

		int diffYear = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
		return diffYear * 12 + cal2.get(Calendar.MONTH) - cal1.get(Calendar.MONTH);
	}
	String md_GET_ACCT_UNIV_CONTRACT_TYPE_NO(String testCaseId){
		return  getValueFromResponse('create_acct_universal_contract',ExPathRpcEnc.GET_ACCT_UNIV_CONTRACT_TYPE_NO)
	}

	String md_GET_ACCT_UNIV_CONTRACT_LENGTH_MONTHS(String testCaseId){
		return getValueFromResponse('create_acct_universal_contract',ExPathRpcEnc.GET_ACCT_UNIV_CONTRACT_LENGTH_MONTHS)

	}

	String md_assign_supp_plan_no(String testCaseId) {
		String result
		String flag = "FALSE"
		ConnectDB database = new ConnectDB()
		ReadData readData = new ReadData(Constant.TESTDATABOOK)
		String testId = testCaseId.substring(testCaseId.indexOf("-") + 1, testCaseId.lastIndexOf("-"))

		String supp_plan_no= getValueFromRequest("assign_supp_plan","//supp_plan_no").toString()
		int assignmentDirective = Integer.parseInt(getValueFromRequest("assign_supp_plan", "//assignment_directive"))
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		
		if(acct_no =='NoVal')
		{
			   acct_no = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		
		if(assignmentDirective>=1 && assignmentDirective<=6)	{
			ResultSet resultSet = database.executePlaQuery("select SUPP_PLAN_NO from ariacore.acct_supp_plan_map_view where acct_no="+ acct_no);
			while (resultSet.next()) {
				result = resultSet.getString(1)
				if(result.equals(supp_plan_no))
					flag='TRUE'
			}
		}
		else if (assignmentDirective>=7 && assignmentDirective<=11) {
			String query = "select PARAM_VALUE from ariacore.api_queue_detail where param_name='SUPP_PLAN_NO' and api_queue_id=(select api_queue_id from ariacore.api_queue where acct_no = "+acct_no+")"
			ResultSet resultSet = database.executeQuery(query)
			while (resultSet.next()) {
				result = resultSet.getString(1)
				if(result.equals(supp_plan_no)) {
					flag='TRUE'
				}
			}
		}
		return flag
	}

	/**
	 * Get account multiplan contract API's response verification
	 * @param testcaseId
	 * @return true or false
	 */
	def md_VERIFY_GET_ACCT_MULTIPLAN_CONTRACT_PLANS(String testcaseId){
		String client_Number = Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def contract_no=getValueFromResponse('create_acct_multiplan_contract',ExPathRpcEnc.CREATE_ACCT_MULTIPLAN_CONTRACT_CONTRACTNO)
		int i=1
		def plan_no

		String query="select PLAN_NO from ariacore.acct_multi_plan_cntrcts_plans where ACCT_NO=" + acct_no + "and contract_no=" + contract_no
		ResultSet resultSet = db.executePlaQuery(query);
		while(resultSet.next()){
			String node="//*/plan_no/*[" + i + "]/*:plan_no"
			plan_no=getValueFromResponse('get_acct_multiplan_contract', node)
			logi.logInfo("Plan Number from response and DB : " + plan_no + "   " + resultSet.getString(1))
			if (!(plan_no == resultSet.getString(1))) {
				return 'FALSE'
				i++
			}
			return 'TRUE'
		}
	}

	def md_CREATE_ORDER_INVOICE_AMT_API(String tcid)
	{

		logi.logInfo("md_CRETAE_ORDER_INVOICE_AMT_API")
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap hm_invoice=new HashMap()
		com.eviware.soapui.support.XmlHolder outputholder

		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains('create_order') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				hm_invoice.put("LINE_NO", outputholder.getNodeValue("//*/*/*:cart_invoice_line_items_row[1]/*:line_no[1]"))
				hm_invoice.put("SERVICE_NO",outputholder.getNodeValue("//*/*/*:cart_invoice_line_items_row[1]/*:service_no[1]"))
				hm_invoice.put("LINE_AMOUNT",outputholder.getNodeValue("//*/*/*:cart_invoice_line_items_row[1]/*:line_amount[1]"))
				hm_invoice.put("DESCRIPTION",outputholder.getNodeValue("//*/*/*:cart_invoice_line_items_row[1]/*:description[1]"))

			}

		}

		return hm_invoice;

	}

	def md_CREATE_ORDER_INVOICE_AMT_DB(String tcid)
	{
		logi.logInfo("md_CRETAE_ORDER_INVOICE_AMT_DB")
		def line_no
		String apiname=tcid.split("-")[2]

		def inv_no = getValueFromResponse(apiname,ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)

		String lineno_qry = "Select seq_num from ariacore.gl_detail where invoice_no="+inv_no+ " and service_no NOT IN (400,401)"
		ResultSet resultSet = db.executePlaQuery(lineno_qry);
		HashMap hm_row = new HashMap();

		while(resultSet.next())
		{
			line_no = resultSet.getObject(1)
		}

		hm_row.put("LINE_NO", line_no)

		String qry = "Select service_no,comments as description from ariacore.gl_detail  where invoice_no="+inv_no+" and seq_num="+line_no
		ResultSet rs_qry = db.executePlaQuery(qry);
		ResultSetMetaData md = rs_qry.getMetaData();
		int columns = md.getColumnCount();


		while (rs_qry.next()){
			for(int i=1; i<=columns; i++){
				hm_row.put(md.getColumnName(i),rs_qry.getObject(i));
			}

			logi.logInfo("charges in invoice mat db method" +charges )
			logi.logInfo("seq hash in invoice mat db method" +seqs)
			hm_row.put("LINE_AMOUNT",charges)
		}

		return hm_row
	}
	
	/**
	 * Calculates credit amount to be applied in the create order API
	 * @param testCaseId
	 * @return applied credit amount
	 */
	String md_CREATE_ORDER_INVOICE_AMT(String tcid)	{
		logi.logInfo("md_CREATE_ORDER_INVOICE_AMT")
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String invoiceNo = getValueFromResponse("create_order",ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO).toString()
		logi.logInfo("Create Order Invoice No : " + invoiceNo)
		String amount
		String applied_amt = db.executeQueryP2("SELECT NVL(SUM(total_applied),0) from ariacore.all_credits where acct_no=" + accountNo + " and reason_cd=200")
		String credit_id = db.executeQueryP2("SELECT credit_id from ariacore.all_credits where acct_no=" + accountNo + " and reason_cd=200")
		String credit_invoice = db.executeQueryP2("SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=(SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO=" + accountNo + ") and ORIG_CREDIT_ID="+credit_id)
		if (invoiceNo.equalsIgnoreCase("NoVal"))
			return "0"
		amount =  db.executeQueryP2("Select debit from ariacore.gl_detail where invoice_no="+invoiceNo+ " and service_no NOT IN (400,401)")
		logi.logInfo("Create Order Amount and Applied Credit amount : " +amount + " :: " + applied_amt)
		if(applied_amt != amount){
			amount = (amount.toDouble() - applied_amt.toDouble()).toString()
		}
		if(credit_invoice == "0"){
			return "0"
		}
		return amount
	}


	HashMap md_CREATE_ORDER_TAX_API1(String tcid)
	{
		return  md_CREATE_ORDER_TAX_API(1)
	}

	HashMap md_CREATE_ORDER_TAX_DB1(String tcid)
	{
		return  md_CREATE_ORDER_TAX_DB(tcid,"1")
	}

	HashMap md_CREATE_ORDER_TAX_API2(String tcid)
	{
		return  md_CREATE_ORDER_TAX_API(2)
	}

	HashMap md_CREATE_ORDER_TAX_DB2(String tcid)
	{
		return  md_CREATE_ORDER_TAX_DB(tcid,"2")
	}

	HashMap md_CREATE_ORDER_INCLUSIVE_TAX_DB1(String tcid)
	{
		return  md_CREATE_ORDER_INCLUSIVE_TAX_DB(tcid,"1")
	}
	HashMap md_CREATE_ORDER_INCLUSIVE_TAX_DB2(String tcid)
	{
		return  md_CREATE_ORDER_INCLUSIVE_TAX_DB(tcid,"2")
	}


	def md_CREATE_ORDER_TAX_API(int line_no)
	{

		logi.logInfo("md_CREATE_ORDER_TAX_API")
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap hm_tax=new HashMap()
		com.eviware.soapui.support.XmlHolder outputholder

		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains('create_order') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				hm_tax.put("LINE_NO", outputholder.getNodeValue("//*/*/*:cart_invoice_line_items_row["+(line_no+1)+"]/*:line_no[1]"))
				hm_tax.put("SERVICE_NO",outputholder.getNodeValue("//*/*/*:cart_invoice_line_items_row["+(line_no+1)+"]/*:service_no[1]"))
				hm_tax.put("LINE_AMOUNT",outputholder.getNodeValue("//*/*/*:cart_invoice_line_items_row["+(line_no+1)+"]/*:line_amount[1]"))
				hm_tax.put("DESCRIPTION",outputholder.getNodeValue("//*/*/*:cart_invoice_line_items_row["+(line_no+1)+"]/*:description[1]"))

			}

		}

		return hm_tax;

	}

	def md_CREATE_ORDER_TAX_DB(String tcid,String tax_key)
	{
		String seq_no = seqs.get("tax"+tax_key)
		logi.logInfo("tax seqnum from hm: "+seq_no)
		def tax_rate
		def tax_charges
		HashMap hm_dbtax = new HashMap();
		String apiname=tcid.split("-")[2]
		def inv_no = getValueFromResponse(apiname,ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)
		String t_qry = "Select seq_num as line_no,service_no,comments as description from ariacore.gl_detail where invoice_no ="+inv_no+" and seq_num="+seq_no
		ResultSet rs_tax = db.executePlaQuery(t_qry);
		ResultSetMetaData met = rs_tax.getMetaData();
		int columns = met.getColumnCount();
		while (rs_tax.next())
		{
			for(int i=1; i<=columns; i++)
			{
				hm_dbtax.put(met.getColumnName(i),rs_tax.getObject(i));
			}
		}

		String tr_qry = "Select tax_rate from ariacore.gl_tax_detail where invoice_no ="+inv_no+" and seq_num="+seq_no
		ResultSet rs_tr = db.executePlaQuery(tr_qry);
		while(rs_tr.next())
		{
			tax_rate=rs_tr.getObject(1)
			tax_charges= (charges * (tax_rate*100).toInteger())/100
			logi.logInfo("calculated tax amt:"+tax_charges)
			hm_dbtax.put("LINE_AMOUNT",tax_charges)
		}


		return  hm_dbtax;
	}

	/**
	 * Sets Contract Start and End Date
	 * @param Client Number,Start Date, End Date, Length
	 * @return date
	 */
	String[] setContractDates(String client_no, String startDate, String endDate, String length)
	{
		String[] dates = new String[2];

		db = new ConnectDB();
		Date date = new Date();
		Calendar now = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL";
		ResultSet resultSet = db.executeQuery(Query);
		while(resultSet.next()){
			// Get only the current Date
			date = resultSet.getDate(1);
		}
		try
		{
		if(!(length.isEmpty() && startDate.isEmpty() && endDate.isEmpty())){
			if(length!=null) {
				if(startDate.isEmpty() || startDate == "" ) {
					now.setTime(date);
				}
				else {
					now.setTime(date);
					if(startDate.contains(".")) {
						String[] days = startDate.split("\\.");
						now.add(Calendar.MONTH, days[0].toInteger());
						now.add(Calendar.DATE, days[1].toInteger());
					}
					else {
						now.add(Calendar.DATE, startDate.toInteger());
					}
				}
				dates[0] = simpleFormat.format(now.getTime()).toString();
				if(endDate.isEmpty() || endDate =="") {
					now.add(Calendar.MONTH, length.toInteger());
					now.add(Calendar.DATE, -1);
				}
				else {
					if(endDate.contains(".")) {
						String[] days = endDate.split("\\.");
						now.add(Calendar.MONTH, days[0].toInteger());
						now.add(Calendar.DATE, days[1].toInteger());
						now.add(Calendar.DATE, -1);
					}
					else {
						now.add(Calendar.DATE, endDate.toInteger());
						now.add(Calendar.DATE, -1);
					}
				}
				dates[1] = simpleFormat.format(now.getTime()).toString();
			}
			else {
				if(startDate.isEmpty() || startDate == "" ) {
					now.setTime(date);
				}
				else {
					now.setTime(date);
					if(startDate.contains(".")) {
						String[] days = startDate.split("\\.");
						now.add(Calendar.MONTH, days[0].toInteger());
						now.add(Calendar.DATE, days[1].toInteger());
					}
					else {
						now.add(Calendar.DATE, startDate.toInteger());
					}
				}
				dates[0] = simpleFormat.format(now.getTime()).toString();
				if(endDate.contains(".")) {
					String[] days = endDate.split("\\.");
					now.add(Calendar.MONTH, days[0].toInteger());
					now.add(Calendar.DATE, days[1].toInteger());
					now.add(Calendar.DATE, -1);
				}
				else {
					now.add(Calendar.DATE, endDate.toInteger());
					now.add(Calendar.DATE, -1);
				}
				dates[1] = simpleFormat.format(now.getTime()).toString();
			}
		}
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
		//db.closeConnection()
		logi.logInfo 'Dates after setting : ' +dates.toString()
		return dates;
	}

	String[] setEffectiveDate(String altBillDay)
	{
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		String day = new String();

		db = new ConnectDB();
		Date date = new Date();
		Calendar now = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL";
		ResultSet resultSet = db.executeQuery(Query);
		while(resultSet.next()){
			// Get only the current Date
			date = resultSet.getDate(1);
		}

		now.add(Calendar.DATE, Integer.parseInt(altBillDay));
		day = simpleFormat.format(now.getTime()).toString();

	}

	def md_INVOICE_TAX_LINE_ITEM_AMT3(String testcaseId)
	{
		return md_INVOICE_TAX_LINE_ITEM_AMT("3")
	}
	def md_INVOICE_TAX_LINE_ITEM_AMT4(String testcaseId)
	{
		return md_INVOICE_TAX_LINE_ITEM_AMT("4")
	}

	def md_INVOICE_TAX_LINE_ITEM_AMT(String seqno)
	{
		ConnectDB db = new ConnectDB()
		String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		String tax_line_tem_amt="select sum(before_credit_amt) from ariacore.gl_tax_detail where invoice_no= "+getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1).toString()+" AND CLIENT_NO="+client_number+" and SEQ_NUM="+seqno
		ResultSet resultSet = db.executePlaQuery(tax_line_tem_amt)
		String tax_amt
		if (resultSet.next()) {
			tax_amt = resultSet.getString(1)
		}
		resultSet.close()
		return tax_amt
	}

	def md_TAX_DEDUCTED_AFTER_REFUND(String testcaseId)
	{
		ConnectDB db = new ConnectDB()

		String query="SELECT NVL(SUM(CREDIT),0) from ariacore.refund_tax_detail WHERE INVOICE_NO ="+getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1).toString()
		ResultSet resultSet = db.executePlaQuery(query)
		String tax_deucted
		if (resultSet.next()) {
			tax_deucted = resultSet.getString(1)
		}

		return tax_deucted
	}

	//	def md_CHECK_MULTICONTRACT_RENEWAL_STATUS(String testcaseId) {
	//		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	//		def contract_no=getValueFromResponse('create_acct_multiplan_contract',ExPathRpcEnc.CREATE_ACCT_MULTIPLAN_CONTRACT_CONTRACTNO)
	//		String query="SELECT RENEWED_FROM_CONTRACT_NO FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE ACCT_NO=" + acct_no + " AND CONTRACT_NO!=" + contract_no
	//		int flag=0
	//		ResultSet resultSet = db.executePlaQuery(query);
	//		while(resultSet.next()){
	//			if (contract_no == resultSet.getString(1))
	//			{
	//				flag = flag + 0
	//			}
	//			else {
	//				flag++
	//			}
	//		}
	//		logi.logInfo("Flag1 Value : " + flag)
	//
	//		if(flag){
	//			return 'FALSE'
	//		}
	//		return 'TRUE'
	//	}

	String md_IRTA_TOTAL_TAX_AMT_API(String tcid)
	{

		String apiname=tcid.split("-")[2]
		List<String> tax_list = []
		String sum = "0.00"
		String tax_amt
		for(int t_no=400;t_no<=410;t_no++)
		{
			tax_list.add(t_no.toString())
		}
		logi.logInfo("tax_list"+tax_list)

		def cnt = getNodeCountFromResponse(apiname,"//*/*:reversed_invoice_lines_row").toString()
		logi.logInfo("total line cnt:"+cnt)
		for(int i=1;i<=cnt.toInteger();i++)
		{
			def ser_no = getValueFromResponse(apiname,"//*:reversed_invoice_lines_row["+i+"]/*:invoice_line_service_no[1]")
			logi.logInfo("ser_no:"+ser_no)
			if(tax_list.contains(ser_no)==true)
			{
				tax_amt = getValueFromResponse(apiname,"//*:reversed_invoice_lines_row["+i+"]/*:invoice_line_reversed_amount[1]")
				logi.logInfo("tax_amt:"+tax_amt)

				sum= ((sum.toDouble() + tax_amt.toDouble()).round(2)).toString()
				logi.logInfo("to double sum:"+sum)
			}
		}
		logi.logInfo sum.split("\\.").toString()
		logi.logInfo sum.split("\\.")[1]
		//		if (sum.split("\\.")[1].length() == 1)
		//			sum = sum.concat("0")

		return df.format(sum.toDouble()).toString()
	}




	String md_IRTA_TOTAL_TAX_AMT_DB(String tcid)
	{
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def inv_list = []
		def tax_rate_list = []
		def cal_tax
		def total_tax=0
		String tax_invoice_qry = "SELECT DISTINCT GLT.INVOICE_NO FROM ARIACORE.GL_TAX_DETAIL GLT JOIN ARIACORE.GL GL ON GL.INVOICE_NO=GLT.INVOICE_NO WHERE GL.ACCT_NO IN ("+acct_no+", (SELECT a2.acct_no child FROM ariacore.acct a1 JOIN ariacore.acct a2 ON a1.acct_no =a2.senior_acct_no WHERE a1.acct_no="+acct_no+"))"
		ResultSet rs_tax_invoice_qry = db.executePlaQuery(tax_invoice_qry)
		while(rs_tax_invoice_qry.next())
		{
			def inv_no=rs_tax_invoice_qry.getObject(1)
			inv_list.add(inv_no)
		}
		String invoices=inv_list.toString()
		invoices= invoices.replace("[", "")
		invoices= invoices.replace("]", "")
		logi.logInfo("invoices after replace:"+invoices)

		String tax_rate_qry="Select distinct tax_rate from ARIACORE.GL_TAX_DETAIL where invoice_no IN ("+invoices+")"
		ResultSet rs_tax_rate_qry = db.executePlaQuery(tax_rate_qry)
		while(rs_tax_rate_qry.next())
		{
			def tax_rate=rs_tax_rate_qry.getObject(1)
			tax_rate_list.add(tax_rate)
		}

		logi.logInfo("Tax applied invoices : "+inv_list)

		for(int i=0;i<inv_list.size();i++)
		{
			def invoice_no = inv_list.get(i)
			logi.logInfo("taxable invoice no"+invoice_no)


			for(int j=0;j<tax_rate_list.size();j++)
			{
				String rev_amt_qry = "Select distinct rd.credit from ARIACORE.refund_detail rd where rd.gl_seq_num IN (Select distinct gtd.taxed_seq_num from ariacore.gl_detail gld JOIN ARIACORE.GL_TAX_DETAIL gtd ON gtd.invoice_no = gld.invoice_no where gld.invoice_no="+invoice_no+"and gld.service_no NOT IN(400,401)) and rd.invoice_no="+invoice_no
				//SELECT distinct rd.credit FROM ARIACORE.refund_detail rd JOIN ariacore.gl_detail gld ON rd.gl_seq_num = gld.seq_num and rd.invoice_no = gld.invoice_no JOIN ARIACORE.GL_TAX_DETAIL gtd on rd.gl_seq_num=gtd.taxed_seq_num and rd.invoice_no = gld.invoice_no  WHERE gld.invoice_no="+invoice_no+" AND gld.service_no NOT IN(400,401)"
				//String rev_amt_qry="select distinct credit from( SELECT rd.credit as credit,row_number() over (order by rd.seq_num desc )as sno FROM ARIACORE.refund_detail rd JOIN ariacore.gl_detail gld ON rd.gl_seq_num = gld.seq_num and rd.invoice_no = gld.invoice_no JOIN ARIACORE.GL_TAX_DETAIL gtd on rd.gl_seq_num=gtd.taxed_seq_num and rd.invoice_no = gld.invoice_no WHERE gld.invoice_no="+invoice_no+"  AND gld.service_no NOT IN(400,401) )where sno=1"
				ResultSet rs_rev_amt_qry = db.executePlaQuery(rev_amt_qry)
				while(rs_rev_amt_qry.next())
				{
					def rev_amt=rs_rev_amt_qry.getObject(1)
					cal_tax = rev_amt * tax_rate_list.get(j)
					//(rev_amt * (tax_rate_list.get(j)*100).toInteger())/100
					total_tax=total_tax + cal_tax
				}

			}

		}

		return df.format(total_tax.toString().toDouble().round(2)).toString()
	}

	/**
	 * Verifies get all account contract APIs Single plan contract details
	 * @param testCaseId
	 * @return true or false
	 */
	def md_GET_ALL_ACCT_CONTRACTS_SINGLEPLAN_INACTIVE(String testcaseId){
		String client_Number = Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		//def contract_no=getValueFromResponse('create_acct_multiplan_contract',ExPathRpcEnc.CREATE_ACCT_MULTIPLAN_CONTRACT_CONTRACTNO)
		String query="SELECT PLAN_NO, TYPE_NO, CONTRACT_MONTHS, STATUS_CD, CONTRACT_NO FROM ARIACORE.ACCT_PLAN_CONTRACTS WHERE ACCT_NO=" + acct_no + " AND CLIENT_NO=" + client_Number + " AND STATUS_CD IN(-1, -2, -3, 99) ORDER BY CONTRACT_NO"
		int i=1,flag=0
		String response1,response2,response3,responseDB3,response4,response5,status_cd,length_months,type_no,contract_number
		
		logi.logInfo("Query formed : " + query)
		ResultSet resultSet = db.executePlaQuery(query);
		while(resultSet.next()){			
			 response1=getValueFromResponse('get_all_acct_contracts', "//*["+i+"]/*[local-name()='plan_no']")
			 response2=getValueFromResponse('get_all_acct_contracts', "//*["+i+"]/*[local-name()='type_no']")
			 response3=getValueFromResponse('get_all_acct_contracts', "//*["+i+"]/*[local-name()='length_months']")
			 response4=getValueFromResponse('get_all_acct_contracts', "//*["+i+"]/*[local-name()='status_code']")
			 response5=getValueFromResponse('get_all_acct_contracts', "//*["+i+"]/*[local-name()='contract_no']")
					
			if(response3.toString().equals("NoVal"))
			{   
				logi.logInfo("lengthmonths value:::::"+response3)
			    response3 = "null"
				responseDB3 = resultSet.getString(3).toString()				
			}
			else
			{
				responseDB3 = resultSet.getString(3).toString()
			}
			
			logi.logInfo("Generated responses : " + response1 + '  ' + response2 + '  ' + response3 + '  ' + response4 + '  ' + response5)
			logi.logInfo("Received from DB: " + resultSet.getString(1) + '  ' + resultSet.getString(2) + '  ' + resultSet.getString(3) + '  ' + resultSet.getString(4) + '  ' + resultSet.getString(5))
			
			if ((response1 == resultSet.getString(1)) && (response2 == resultSet.getString(2)) && (response3.toString() == responseDB3) && (response4 == resultSet.getString(4)) && (response5 == resultSet.getString(5)))
			{
				flag = flag + 0
			}
			else {
				flag++
			}
			i++
		}
		logi.logInfo("Flag1 Value : " + flag)

		if(flag){
			return 'FALSE'
		}
		return 'TRUE'
	}

	/**
	 * Verifies get all account contract APIs Multi plan contract details
	 * @param testCaseId
	 * @return TRUE or FALSE
	 */
	def md_GET_ALL_ACCT_CONTRACTS_MULTIPLAN_INACTIVE_and_ACTIVE(String testcaseId) {
		String client_Number = Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def contract_no=getValueFromResponse('create_acct_multiplan_contract',ExPathRpcEnc.CREATE_ACCT_MULTIPLAN_CONTRACT_CONTRACTNO)
		query="SELECT STATUS_CD,CONTRACT_MONTHS,TYPE_NO,CONTRACT_NO FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE ACCT_NO=" + acct_no + " AND CONTRACT_NO=" + contract_no

		int flag=0,i, j=1
		String node1,node2,node3,node4,node5,response1,response2,response3,response4,response5,status_cd,length_months,type_no,contract_number

		ResultSet resultSet1 = db.executePlaQuery(query);
		while(resultSet1.next()){
			status_cd=resultSet1.getString(1)
			length_months=resultSet1.getString(2)
			type_no=resultSet1.getString(3)
			contract_number=resultSet1.getString(4)
		}
		query="SELECT PLAN_NO FROM ARIACORE.ACCT_MULTI_PLAN_CNTRCTS_PLANS WHERE ACCT_NO=" + acct_no + " AND CONTRACT_NO=" + contract_no
		ResultSet resultSet = db.executePlaQuery(query);

		while(j > 0) {
			if (getValueFromResponse('get_all_acct_contracts', "//*/all_acct_contracts/*[" + i + "]/contract_scope") == "MULTI PLAN") {
				i=j
				j=0
				break
			}
			else if (getValueFromResponse('get_all_acct_active_contracts', "//*/all_acct_active_contracts/*[" + i + "]/contract_scope") == "MULTI PLAN") {
				i=j
				j=0
				break
			}
			else {
				i++
			}
		}

		while(resultSet.next()){
			node1="//*/all_acct_contracts/*[" + i + "]/plan_no"
			node2="//*/all_acct_contracts/*[" + i + "]/type_no"
			node3="//*/all_acct_contracts/*[" + i + "]/length_months"
			node4="//*/all_acct_contracts/*[" + i + "]/status_code"
			node5="//*/all_acct_contracts/*[" + i + "]/contract_no"
			response1=getValueFromResponse('get_all_acct_contracts', "//*/all_acct_contracts/*[" + i + "]/plan_no")
			response2=getValueFromResponse('get_all_acct_contracts', "//*/all_acct_contracts/*[" + i + "]/type_no")
			response3=getValueFromResponse('get_all_acct_contracts', "//*/all_acct_contracts/*[" + i + "]/length_months")
			response4=getValueFromResponse('get_all_acct_contracts', "//*/all_acct_contracts/*[" + i + "]/status_code")
			response5=getValueFromResponse('get_all_acct_contracts', "//*/all_acct_contracts/*[" + i + "]/contract_no")
			logi.logInfo("Generated responses 2nd Set: " + response1 + '  ' + response2 + '  ' + response3 + '  ' + response4 + '  ' + response5)
			logi.logInfo("DB value : " + resultSet.getString(1))
			logi.logInfo("Type no : " + type_no + "  Duration : " + length_months + "  Status Code : " + status_cd + "  Contract number : " + contract_number)
			if((response1==resultSet.getString(1)) && (response2==type_no) && (response3==length_months)
			&&(response4==status_cd) && (response5==contract_no)){
				flag=flag+0
			}
			else {
				flag++
			}
			i++
		}
		logi.logInfo("Flag2 Value : " + flag)
		if(!flag){
			return 'FALSE'
		}
		return 'TRUE'
	}

	HashMap md_CHECK_REFUND_REVERSAL_DETAILS_LINE_ITEMS_API1(String tsetid)
	{
		return  md_CHECK_REFUND_REVERSAL_DETAILS_LINE_ITEMS_API("1")
	}

	HashMap md_CHECK_REFUND_REVERSAL_DETAILS_LINE_ITEMS_DB1(String tsetid)
	{
		return  md_CHECK_REFUND_REVERSAL_DETAILS_LINE_ITEMS_DB("1")
	}

	HashMap md_CHECK_REFUND_REVERSAL_DETAILS_LINE_ITEMS_API2(String tsetid)
	{
		return  md_CHECK_REFUND_REVERSAL_DETAILS_LINE_ITEMS_API("2")
	}

	HashMap md_CHECK_REFUND_REVERSAL_DETAILS_LINE_ITEMS_DB2(String tsetid)
	{
		return  md_CHECK_REFUND_REVERSAL_DETAILS_LINE_ITEMS_DB("2")
	}

	def  md_CHECK_REFUND_REVERSAL_DETAILS_LINE_ITEMS_API(String lineno)
	{
		logi.logInfo("md_CHECK_REFUND_REVERSAL_DETAILS_LINE_ITEMS_API")
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap hm_rev_api=new HashMap()
		com.eviware.soapui.support.XmlHolder outputholder
		int k=1
		def rev_row_cnt = getNodeCountFromResponse("get_refund_details","//*:refund_details_row["+lineno+"]/*:invoice_reversals[1]/*:invoice_reversals_row").toString()
		logi.logInfo ("reversal row cnt in API:"+rev_row_cnt)
		for(int i=1;i<=rev_row_cnt.toInteger();i++)
		{
			for(int j=0; j<testCaseCmb.size(); j++) {
				if(testCaseCmb[j].toString().contains('get_refund_details') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
					outputholder = xmlValues[j]
					hm_rev_api.put("INVOICE_NO"+k, outputholder.getNodeValue("//*:invoice_reversals_row["+i+"]/*:invoice_no[1]"))
					hm_rev_api.put("INVOICE_LINE_NO"+k,outputholder.getNodeValue("//*:invoice_reversals_row["+i+"]/*:invoice_line_no[1]"))
					hm_rev_api.put("REVERSED_LINE_AMOUNT"+k,outputholder.getNodeValue("//*:invoice_reversals_row["+i+"]/*:reversed_line_amount[1]"))


				}

			}

			k++
		}

		return hm_rev_api;
	}
	def md_CHECK_REFUND_REVERSAL_DETAILS_LINE_ITEMS_DB(String seqno)
	{
		logi.logInfo("md_CHECK_REFUND_REVERSAL_DETAILS_LINE_ITEMS_DB")
		List<String> inv_list=[]
		HashMap hm_rev=new HashMap()
		def rev_row_cnt = getNodeCountFromResponse("get_refund_details","//*:refund_details_row["+seqno+"]/*:invoice_reversals[1]/*:invoice_reversals_row").toString()
		logi.logInfo ("reversal row cnt:"+rev_row_cnt)
		for(int i=1;i<=rev_row_cnt.toInteger();i++)
		{
			def inv_no = getValueFromResponse('get_refund_details',"//*:invoice_reversals_row["+i+"]/*:invoice_no[1]")
			inv_list.add(inv_no)
		}
		logi.logInfo("inv_list"+inv_list)
		String invoices=inv_list.toString()
		logi.logInfo("invoices:"+invoices)
		invoices= invoices.replace("[", "")
		invoices= invoices.replace("]", "")
		logi.logInfo("invoices after replace:"+invoices)
		for(int j=1;j<=inv_list.size();j++)
		{
			String crd_qry = "Select invoice_no,gl_seq_num as invoice_line_no,credit as reversed_line_amount from (SELECT invoice_no,gl_seq_num,credit,row_number() over (order by refund_no) as seqno FROM ariacore.refund_detail WHERE invoice_no IN ("+invoices+"))where seqno="+j
			ResultSet rs_crd_qry = db.executePlaQuery(crd_qry);
			ResultSetMetaData met = rs_crd_qry.getMetaData();
			int columns = met.getColumnCount();
			logi.logInfo("column cnt:"+columns)
			while (rs_crd_qry.next())
			{
				for(int k=1;k<=columns;k++)
				{
					hm_rev.put(met.getColumnName(k)+j,rs_crd_qry.getObject(k));
				}
			}
		}

		return hm_rev

	}

	public String getTaxInclusiveAmount(String InvoiceNo,String sno)
	{
		logi.logInfo( "Calling getTaxInclusiveAmount for SEQ number " +sno)
		String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		String plan_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICEPLANNO1)
		String tax_inclusive_query="select NVL(tax_inclusive_ind,0) from ARIACORE.plan_services where service_no in (select service_no from ariacore.gl_detail where invoice_no="+InvoiceNo+"  and seq_num="+sno+")  and plan_no="+plan_no
		ConnectDB db = new ConnectDB()
		ResultSet resultSet = db.executePlaQuery(tax_inclusive_query);
		String temp
		while (resultSet.next()){
			temp=resultSet.getString(1)
		}
		resultSet.close()
		String tax_inclusive_amt
		if(temp.equals("1"))
		{
			logi.logInfo "TaxInclusive Applied "
			String tax_inclusive_amt_query="select NVL(sum(DEBIT),0) from ariacore.gl_tax_detail where invoice_no="+InvoiceNo+" and taxed_seq_num="+sno
			ResultSet rs = db.executePlaQuery(tax_inclusive_amt_query);
			while (rs.next()){
				tax_inclusive_amt=rs.getString(1)
			}

			rs.close()

		}
		else
		{
			logi.logInfo "TaxInclusive Not Applied "
			tax_inclusive_amt="0"

		}
		logi.logInfo( "Tax inclusive amount for seq num  "+sno+" "+tax_inclusive_amt)
		return tax_inclusive_amt
	}

	def md_(String testcaseId) {
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def contract_no=getValueFromResponse('create_acct_multiplan_contract',ExPathRpcEnc.CREATE_ACCT_MULTIPLAN_CONTRACT_CONTRACTNO)
		String query="SELECT STATUS_CD, RENEWED_FROM_CONTRACT_NO FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE ACCT_NO=" + acct_no + " AND CONTRACT_NO!=" + contract_no
		ResultSet resultSet = db.executePlaQuery(query);
		while(resultSet.next()){
			if((resultSet.getString(1)=="1") && (resultSet.getString(2)==contract_no)) {
				return 'TRUE'
			}
			else
				return 'FALSE'
		}
	}


	public String md_get_Total_Invoice_Amount_After_Coupon_And_Pay_Credit(String testCaseId)
	{
		def totalPaymentAmount
		def totalCouponAmount
		def totalRefundedAmount

		String totalRefundableAmount
		String totalInvoiceAmountAfterCoupon
		ConnectDB db = new ConnectDB()
		ResultSet resSet
		String finalDiscAmt
		String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		String account_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String invoice_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String payment_Method =   getValueFromRequest("create_acct_complete","//pay_method")
		String currencyCode = getValueFromRequest("create_acct_complete","//currency_cd")
		String invoiceRecAmt = getValueFromResponse("create_acct_complete","//*:new_acct_invoice_line_items_row[2]/*:invoice_line_amount[1]")

		String discount_Amount_Qry = "select discount_percentage from ariacore.client_payment_method where client_no = "+client_number+" and method_id ="+payment_Method
		String discount_Rule_No_Qry = "select discount_rule_no from ariacore.client_payment_method where client_no = "+client_number+" and method_id ="+payment_Method
		String invoice_qry = "SELECT DEBIT FROM ARIACORE.GL WHERE ACCT_NO="+account_No+" AND INVOICE_NO="+invoice_No
		String flatDiscountQry = "select NVL(Sum(AMOUNT),0) as flat_discount from ariacore.CLI_PAY_MTH_FLAT_DISC_CURR_MAP where CLIENT_NO = "+client_number+" and method_id = "+payment_Method+" and currency_cd ='"+currencyCode+"'"

		String refund_externalPayment = "SELECT NVL(SUM(AMOUNT),0) REFUNDED_AMOUNT FROM ARIACORE.CHECK_REFUND  WHERE ACCT_NO="+account_No
		String refund_electronicPayment = "SELECT NVL(SUM(AMOUNT),0) FROM ARIACORE.MANAGED_REFUNDS WHERE ACCT_NO="+account_No

		//String couponAmtQry = "SELECT NVL(SUM(DISTILLED_AMOUNT),0) AS COUPON_AMT FROM (SELECT ELIGIBLE_SERVICE_NO, DISTILLED_AMOUNT FROM ariacore.all_recur_credit WHERE acct_no = (SELECT acct_no FROM ariacore.gl WHERE invoice_no="+invoice_number+") UNION SELECT CDR.KEY_2_VAL AS ELIGIBLE_SERVICE_NO,AIDD.ACTUAL_DISCOUNT_AMT AS DISTILLED_AMOUNT FROM ARIACORE.CLIENT_DR_COMPONENTS CDR JOIN ariacore.all_invoice_discount_details AIDD ON AIDD.rule_no=CDR.rule_no JOIN ariacore.CLIENT_DISCOUNT_RULES CDRS ON CDRS.rule_no =AIDD.rule_no WHERE AIDD.invoice_no ="+invoice_number+")a"
		String couponAmtQry = " SELECT SUM(DISTILLED_AMOUNT) AS COUPON_AMT FROM (SELECT ELIGIBLE_SERVICE_NO, DISTILLED_AMOUNT FROM ariacore.all_recur_credit WHERE acct_no = (SELECT acct_no FROM ariacore.gl WHERE invoice_no="+invoice_No+") UNION SELECT CDR.KEY_2_VAL AS ELIGIBLE_SERVICE_NO, CASE WHEN CDRS.INLINE_OFFSET_IND='I' THEN 0 ELSE AIDD.ACTUAL_DISCOUNT_AMT END AS DISTILLED_AMOUNT FROM ARIACORE.CLIENT_DR_COMPONENTS CDR JOIN ariacore.all_invoice_discount_details AIDD ON AIDD.rule_no=CDR.rule_no JOIN ariacore.CLIENT_DISCOUNT_RULES CDRS ON CDRS.rule_no =AIDD.rule_no WHERE AIDD.invoice_no ="+invoice_No+")a"

		String refund_amtExt
		String refund_amtElect
		int flatDiscountAmount
		def totalInvoiceAmt
		def percentDiscount
		String disc_Rule_No
		boolean discAplicable = false
		//if(isCouponApplied(invoice_number,client_number) == true)
		//{
		totalPaymentAmount = db.executeQueryP2(invoice_qry);
		logi.logInfo "Total payment amount : "+ totalPaymentAmount

		refund_amtExt = db.executeQueryP2(refund_externalPayment);
		logi.logInfo "Coupon Against External payment : "+ refund_amtExt

		refund_amtElect = db.executeQueryP2(refund_electronicPayment);
		logi.logInfo "Coupon Against Electronic payment : "+ refund_amtElect

		if(refund_amtExt.contains(".") || refund_amtElect.contains(".")){
			totalRefundedAmount = (refund_amtExt.toDouble() + refund_amtElect.toDouble())
		} else {
			totalRefundedAmount = (refund_amtExt.toInteger() + refund_amtElect.toInteger())
		}
		logi.logInfo "Total coupon applied amount : "+ totalCouponAmount

		totalCouponAmount = db.executeQueryP2(couponAmtQry);
		logi.logInfo "Total totalRefundedAmount amount : "+ totalRefundedAmount

		if(totalPaymentAmount.contains(".") || totalCouponAmount.contains(".")){
			logi.logInfo "double"
			totalInvoiceAmountAfterCoupon = (totalPaymentAmount.toDouble() - totalCouponAmount.toDouble())
		} else {
			logi.logInfo "integer"
			totalInvoiceAmountAfterCoupon = (totalPaymentAmount.toInteger() - totalCouponAmount.toInteger())
		}

		logi.logInfo("Total refundable amount is after coupon alone is : " +totalInvoiceAmountAfterCoupon.toString())

		resSet = db.executePlaQuery(discount_Amount_Qry)
		if (resSet.next()) {
			percentDiscount = resSet.getObject(1)
			discAplicable = true
		}
		//percentDiscount = dbase.executeQueryP2(discount_Amount_Qry)
		logi.logInfo("Discount percentage for credit card payment :: "+percentDiscount)

		resSet = db.executePlaQuery(discount_Rule_No_Qry)
		if (resSet.next()) {
			disc_Rule_No = resSet.getString(1)
		}
		logi.logInfo("Rule no :: "+disc_Rule_No)

		resSet = db.executePlaQuery(flatDiscountQry)
		if (resSet.next()) {
			flatDiscountAmount = resSet.getObject(1)
		}
		logi.logInfo("flatDiscountAmount :: "+flatDiscountAmount)

		if(discAplicable == true && disc_Rule_No.toInteger() == 2){
			def percentageDisc = (totalPaymentAmount.toDouble() * percentDiscount) / 100
			finalDiscAmt = percentageDisc
		} else if(discAplicable == true && disc_Rule_No.toInteger() == 3){
			def percentageDisc = (invoiceRecAmt.toInteger() * percentDiscount) / 100
			finalDiscAmt = percentageDisc
		}else if(disc_Rule_No.toInteger() == 1){
			finalDiscAmt = flatDiscountAmount
		}
		logi.logInfo("Total  pay credit is  : " +finalDiscAmt.toString())
		String totalDeductionsReduced
		if(totalInvoiceAmountAfterCoupon.toString().contains(".") || finalDiscAmt.toString().contains(".")){
			totalDeductionsReduced = (totalInvoiceAmountAfterCoupon.toDouble() - finalDiscAmt.toDouble())
		} else {
			totalDeductionsReduced = (totalInvoiceAmountAfterCoupon.toInteger() - finalDiscAmt.toInteger())
		}
		logi.logInfo("Total refundable amount is after coupon and pay credit is  : " +totalDeductionsReduced)
		return totalDeductionsReduced
	}
	public def md_get_payment_discount_details_db1(String testCaseId){
		return md_get_payment_discount_database("3")
	}
	public def md_get_payment_discount_details_db2(String testCaseId){
		return md_get_payment_discount_database("4")
	}
	public HashMap md_get_payment_discount_database(String seq_num){
		HashMap payDisc =  new HashMap<String,String>()
		ConnectDB payDB = new ConnectDB()
		ResultSet resSet
		String[] tempValues
		List tempKey = [
			"invoice_line_description",
			"invoice_line_amount"
		]

		String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		String invoice_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)

		String payDtlQry = "SELECT COMMENTS,DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO = "+client_number+" AND SEQ_NUM = "+seq_num+" AND COMMENTS LIKE '%Account Service Credit No%'"
		resSet = payDB.executePlaQuery(payDtlQry);

		ResultSetMetaData metaData = resSet.getMetaData();
		int columns = metaData.getColumnCount();
		// Setting sequence number, debit and service description
		while (resSet.next()) {
			tempValues = new String[columns]
			for (int col = 1; col <= columns; col++) {
				String columnValue = resSet.getString(col);
				tempValues[col-1]=columnValue;
			}
		}

		logi.logInfo("payment discount line items  : "+Arrays.deepToString(tempValues))

		for (int i = 0 ; i < tempValues.size() ; i++) {
			payDisc.put(tempKey.get(i), tempValues[i]);
		}
		logi.logInfo("payDisc  payDisc : "+payDisc)
		return payDisc
	}
	public def md_get_payment_discount_api1(String testCaseId){
		return get_payment_discount_api("3")
	}
	public def md_get_payment_discount_api2(String testCaseId){
		return get_payment_discount_api("4")
	}
	public HashMap get_payment_discount_api(String seq_num){
		logi.logInfo("md_get_payment_discount_api")
		ConnectDB payDBapi = new ConnectDB()
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap payment_disc=new HashMap()
		com.eviware.soapui.support.XmlHolder outputholder

		/*String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		 String invoice_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		 String seqNumQry = "SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO = "+client_number+" AND COMMENTS LIKE '%Account Service Credit No%'"
		 seqNum = payDBapi.executeQueryP2(seqNumQry) */
		logi.logInfo("seqNum seqNum : "+seq_num)

		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains('create_acct_complete') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				payment_disc.put("invoice_line_amount", outputholder.getNodeValue("//*:new_acct_invoice_line_items_row["+seq_num+"]/*:invoice_line_amount[1]"))
				payment_disc.put("invoice_line_description",outputholder.getNodeValue("//*:new_acct_invoice_line_items_row["+seq_num+"]/*:invoice_line_description[1]"))
			}
		}
		return payment_disc;
	}
	public def md_get_payment_discount_with_tax_api1(String testCaseId){
		return get_payment_discount_api("4")
	}
	public def md_get_payment_discount_with_tax_api2(String testCaseId){
		return get_payment_discount_api("5")
	}
	public def md_get_payment_discount_details_tax_db1(String testCaseId){
		return md_get_payment_discount_database("4")
	}
	public def md_get_payment_discount_details_tax_db2(String testCaseId){
		return md_get_payment_discount_database("5")
	}
	def md_CHILD_ACCT_ORDER_DETAILS_API(String lineno)
	{
		logi.logInfo("md_CHILD_ACCT_ORDER_DETAILS_API")
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap hm_ch_co_api=new HashMap()
		com.eviware.soapui.support.XmlHolder outputholder
		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains('create_order') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				hm_ch_co_api.put("ORDER_NO", outputholder.getNodeValue("//*/order_no[1]"))
				hm_ch_co_api.put("INVOICE_NO",outputholder.getNodeValue("//*/invoice_no[1]"))
				hm_ch_co_api.put("TRANSACTION_ID",outputholder.getNodeValue("//*/transaction_id[1]"))


			}
		}

		return hm_ch_co_api
	}

	def md_CHILD_ACCT_ORDER_DETAILS_DB(String seqno)
	{
		logi.logInfo("md_CHILD_ACCT_ORDER_DETAILS_DB")
		HashMap hm_ch_co_db=new HashMap()

		def acct_no=getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
		String or_qry= "Select order_no,invoice_no from ARIACORE.ORDERS where acct_no="+acct_no
		ResultSet rs_or_qry = db.executePlaQuery(or_qry);
		ResultSetMetaData met = rs_or_qry.getMetaData();
		int columns = met.getColumnCount();
		logi.logInfo("column cnt:"+columns)
		while (rs_or_qry.next())
		{
			for(int k=1;k<=columns;k++)
			{
				hm_ch_co_db.put(met.getColumnName(k),rs_or_qry.getObject(k));
			}
		}

		String tr_or_qry = "SELECT ACTR.EVENT_NO FROM ARIACORE.ACCT_TRANSACTION ACTR JOIN ARIACORE.ORDERS ORD ON actr.acct_no=ord.acct_no WHERE ord.acct_no="+acct_no+" AND ACTR.SOURCE_COMMENTS LIKE '%order no. "+hm_ch_co_db.get("ORDER_NO")+"%'"
		ResultSet rs_tr_or_qry = db.executePlaQuery(tr_or_qry);
		while(rs_tr_or_qry.next())
		{
			def trans_id=rs_tr_or_qry.getObject(1)
			hm_ch_co_db.put("TRANSACTION_ID", trans_id)
		}
		return hm_ch_co_db
	}

	String md_IS_SEPERATE_TAX_LINE_ITEM_AVAILABLE(String tcid)
	{
		logi.logInfo( "Calling md_IS_SEPERATE_TAX_LINE_ITEM_AVAILABLE")
		String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String query="SELECT count(*) from ARIACORE.refund_tax_detail where invoice_no ="+invoiceNo
		ConnectDB db1 = new ConnectDB()

		String str = db1.executeQueryP2(query)
		Boolean flag=true
		if(str.toInteger()>=0)
		{
			flag= false
		}
		logi.logInfo("Tax line item flag is"+flag.toString())
		return flag.toString().toUpperCase()

	}

	def md_assign_supp_plan_status_cd(String testcaseId) {
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String supp_plan= getValueFromRequest("assign_supp_plan","//supp_plan_no")
		String plan_status_cd="select status_cd from ariacore.acct_supp_plan_map_view where acct_no="+acct_no+" and supp_plan_no="+supp_plan+" and client_no="+client_no
		String status_cd=null
		ConnectDB database = new ConnectDB()
		ResultSet resultSet = database.executeQuery(plan_status_cd);
		while (resultSet.next()){
			status_cd=resultSet.getString(1)
		}
		resultSet.close()
		return status_cd
	}

	def md_cancel_supp_plan_status_cd(String testcaseId) {
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String supp_plan= getValueFromRequest("cancel_supp_plan","//supp_plan_no")
		String plan_status_cd="select status_cd from ariacore.acct_supp_plan_map_view where acct_no="+acct_no+" and supp_plan_no="+supp_plan+" and client_no="+client_no
		String status_cd=null
		ConnectDB database = new ConnectDB()
		ResultSet resultSet = database.executeQuery(plan_status_cd);
		while (resultSet.next()){
			status_cd=resultSet.getString(1)
		}
		resultSet.close()
		return status_cd
	}

	String md_IRTA_TOTAL_TAX_AMT_INCLUSIVE_DB(String tcid)
	{
		logi.logInfo("Calling md_CHECK_IRTA_LINE_ITEMS_TAX_DB")
		logi.logInfo("tcid"+tcid)
		double total_tax=0.00
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String invoice_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String[] org_base_amount=getAllValuesFromRequest(tcid, "//invoice_line_reversing_amount")
		List<String> all_taxes=[]
		List<String> inv_list = []
		logi.logInfo("base amount list : "+org_base_amount.toString())
		String tax_invoice_qry = "SELECT DISTINCT GLT.INVOICE_NO FROM ARIACORE.GL_TAX_DETAIL GLT JOIN ARIACORE.GL GL ON GL.INVOICE_NO=GLT.INVOICE_NO WHERE GL.ACCT_NO="+acct_no
		ResultSet rs_tax_invoice_qry = db.executePlaQuery(tax_invoice_qry)
		while(rs_tax_invoice_qry.next())
		{
			String inv_no=rs_tax_invoice_qry.getString(1)
			inv_list.add(inv_no)
		}

		logi.logInfo("Tax applied invoices : "+inv_list)

		for(invoice in inv_list )
		{
			for(org_amt in org_base_amount)
			{
				String tax_rate_qry="Select distinct round(tax_rate,2) from ARIACORE.GL_TAX_DETAIL where invoice_no ="+invoice
				List<String> tax_rate_list = []
				ResultSet rs_tax_rate_qry = db.executePlaQuery(tax_rate_qry)
				while(rs_tax_rate_qry.next())
				{
					double tax_rate=rs_tax_rate_qry.getDouble(1)
					tax_rate_list.add((tax_rate+1).toString())
				}

				logi.logInfo("tax rate list : "+tax_rate_list.toString())
				for (tax in tax_rate_list)
				{
					String newbase_amt=((org_amt.toDouble()/tax.toDouble()).trunc(2)).toString()
					logi.logInfo "new base amount "+newbase_amt
					String taxation_amt=(org_amt.toDouble()-newbase_amt.toDouble()).round(2).toString()
					logi.logInfo "taxation amount "+taxation_amt
					all_taxes.add(taxation_amt)
					org_amt=(org_amt.toDouble()-taxation_amt.toDouble()).toString()
					logi.logInfo "new org base amount "+org_amt

				}

			}
			logi.logInfo("all tax list for invoice: "+invoice+" " +all_taxes.toString())
			for(t in all_taxes)
			{

				total_tax=(total_tax+t.toDouble()).round(2)

			}

		}
		logi.logInfo("total tax amount: "+" " +total_tax.toString())

		return   df.format(total_tax.toString().toDouble()).toString()

	}

	def md_IRTA_REVERSED_INCLUSIVE_AMT_API1(String tcid)
	{
		return md_IRTA_REVERSED_INCLUSIVE_AMT(tcid,"1")
	}
	def md_IRTA_REVERSED_INCLUSIVE_AMT_API2(String tcid)
	{
		return md_IRTA_REVERSED_INCLUSIVE_AMT(tcid,"2")
	}
	def md_IRTA_REVERSED_INCLUSIVE_AMT_API3(String tcid)
	{
		return md_IRTA_REVERSED_INCLUSIVE_AMT(tcid,"3")
	}
	def md_CAC_INCLUSIVE_LINE_AMT_API1(String tcid)
	{
		return md_CAC_INCLUSIVE_LINE_AMT_API(tcid,"1")
	}
	def md_CAC_INCLUSIVE_LINE_AMT_API2(String tcid)
	{
		return md_CAC_INCLUSIVE_LINE_AMT_API(tcid,"2")
	}
	def md_CAC_INCLUSIVE_LINE_AMT_API3(String tcid)
	{
		return md_CAC_INCLUSIVE_LINE_AMT_API1(tcid,"3")
	}
	String md_CAC_INCLUSIVE_LINE_AMT_API(String tcid,String lno)
	{
		logi.logInfo( "Calling md_CAC_INCLUSIVE_LINE_AMT_API for line number "+lno)
		String org_amt
		String invoice_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String lineitem_query="select usage_units*usage_rate from ariacore.gl_detail where  service_no not in (400,401,410) and seq_num="+lno+" and invoice_no="+invoice_No
		ResultSet rs = db.executePlaQuery(lineitem_query)
		while(rs.next())
		{
			org_amt=rs.getString(1)
		}

		logi.logInfo( "org_amt from " +org_amt)

		return md_getamt_after_tax_inclusive(invoice_No,org_amt)

	}

	String md_IRTA_REVERSED_INCLUSIVE_AMT(String tcid,String lno)
	{
		logi.logInfo( "Calling md_IRTA_REVERSED_INCLUSIVE_AMT for line number "+lno)
		String invoice_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String org_amt=getAllValuesFromRequest(tcid, "//invoices_to_reverse_row["+lno+"]/invoice_line_reversing_amount")[0]
		logi.logInfo( "org_amt from " +org_amt)

		return md_getamt_after_tax_inclusive(invoice_No,org_amt)

	}

	String[] getAllValuesFromRequest(String tcid,String xpath) {

		String[] nodeValue
		List testCaseCmb = Constant.lhmAllInputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllInputResponseAsXML_Global.values().asList()

		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().equals(tcid))
			{

				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
					nodeValue = holder.getNodeValues(xpath)
				}
				else
				{
					logi.logInfo "The node "+xpath+"does not exist in  Request for "+tcid
				}
			}
		}

		return nodeValue
	}

	/**
	 * Gets actual invoice amount generated for the account
	 * @param testCaseId
	 * @return invoice amount
	 */
	public String md_ActualInvoice(String testCaseId) {
		logi.logInfo 'md_ActualInvoice Inside'
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		//String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		def accountNo = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String service  = Constant.SERVICENAME
		String advVirtualTime
		if (service.contains("^"))
			advVirtualTime = service.substring(service.indexOf("_")+1, service.indexOf("^"))
		else
			advVirtualTime = service.substring(service.indexOf("_")+1, service.length())
		String amount
		invoiceNo = getInvoiceNoP2(clientNo, accountNo, advVirtualTime).toString();

		String query = "SELECT NVL(sum(debit),0) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo
		logi.logInfo 'Query : '+ query
		if(accountNo == "NoVal" || invoiceNo == "NoVal") {
			amount = "No Val ret"
		} else {
			ConnectDB dbActInvoice = new ConnectDB();
			ResultSet resultSetActInvoice = dbActInvoice.executePlaQuery(query)
			amount= ""
			if (resultSetActInvoice.next())
				amount = resultSetActInvoice.getString(1)
			resultSetActInvoice.close()
		}
		return amount;
	}

	/**
	 * Retrieves the services and tiered pricing for the plans
	 * @param testCaseId
	 * @return HashMap of all services
	 */
	public HashMap getPlanServiceAndTieredPricing(){
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		if(acct_no == 'NoVal')
		{
			 acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO1)
		}
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" or custom_to_client_no is null) and recurring = 1) order by plan_no, service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" or custom_to_client_no is null) and recurring = 1) order by plan_no, service_no"))
		ResultSet resultSet = database.executeQuery(query)

		HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		HashMap<String,String> serviceHash

		String[] plan = new String[count]
		String[] service = new String[count]
		String[] tieredPricing = new String[count]

		int i= 0
		while(resultSet.next()){
			plan[i]=resultSet.getString(1)
			service[i]=resultSet.getString(2)
			tieredPricing[i]=resultSet.getString(3)
			i++
		}
		String plan_no
		int serviceCount=0
		for(int planCount=0;planCount<plan.length;){
			plan_no = plan[planCount]
			serviceHash = new HashMap<String,String> ()
			for(;serviceCount<service.length;serviceCount++){
				if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					planCount ++
				}
				else
					break
			}
			resultHash.put(plan_no, serviceHash)
			logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
		}
		logi.logInfo "Total Services" + resultHash.toString()
		return resultHash
	}

	/**
	 * Retrieves the services and tiered pricing for the plans for proration
	 * @param testCaseId
	 * @return HashMap of all services
	 */
	public HashMap getPlanServiceAndTieredPricingForProration(){

		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		logi.logInfo "Invoice : "+invoiceNo
		String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select plan_no from ariacore.GL_DETAIL where INVOICE_NO="+invoiceNo+" AND PRORATION_FACTOR<>1 GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and recurring = 1) order by plan_no, service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select plan_no from ariacore.GL_DETAIL where INVOICE_NO="+invoiceNo+" AND PRORATION_FACTOR<>1 GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and recurring = 1) order by plan_no, service_no"))
		ResultSet resultSet = database.executeQuery(query)

		HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		HashMap<String,String> serviceHash

		String[] plan = new String[count]
		String[] service = new String[count]
		String[] tieredPricing = new String[count]

		int i= 0
		while(resultSet.next()){
			plan[i]=resultSet.getString(1)
			service[i]=resultSet.getString(2)
			tieredPricing[i]=resultSet.getString(3)
			i++
		}
		String plan_no
		int serviceCount=0
		for(int planCount=0;planCount<plan.length;){
			plan_no = plan[planCount]
			serviceHash = new HashMap<String,String> ()
			for(;serviceCount<service.length;serviceCount++){
				if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					planCount ++
				}
				else
					break
			}
			resultHash.put(plan_no, serviceHash)
			logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
		}
		logi.logInfo "Total Services for Proration : " + resultHash.toString()
		return resultHash
	}

	/**
	 * Retrieves the services and tiered pricing for the plans for full invoicing
	 * @param testCaseId
	 * @return HashMap of all services
	 */
	public HashMap getPlanServiceAndTieredPricingForFull(){

		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		logi.logInfo "Invoice : "+invoiceNo
		String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select plan_no from ariacore.GL_DETAIL where INVOICE_NO="+invoiceNo+" AND PRORATION_FACTOR=1 GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and recurring = 1) order by plan_no, service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select plan_no from ariacore.GL_DETAIL where INVOICE_NO="+invoiceNo+" AND PRORATION_FACTOR=1 GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and recurring = 1) order by plan_no, service_no"))
		ResultSet resultSet = database.executeQuery(query)

		HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		HashMap<String,String> serviceHash

		String[] plan = new String[count]
		String[] service = new String[count]
		String[] tieredPricing = new String[count]

		int i= 0
		while(resultSet.next()){
			plan[i]=resultSet.getString(1)
			service[i]=resultSet.getString(2)
			tieredPricing[i]=resultSet.getString(3)
			i++
		}
		String plan_no
		int serviceCount=0
		for(int planCount=0;planCount<plan.length;){
			plan_no = plan[planCount]
			serviceHash = new HashMap<String,String> ()
			for(;serviceCount<service.length;serviceCount++){
				if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					planCount ++
				}
				else
					break
			}
			resultHash.put(plan_no, serviceHash)
			logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
		}
		logi.logInfo "Total Services for without Proration : " + resultHash.toString()
		return resultHash
	}

	/**
	 * Calculates invoice amount for all plans and services
	 * @param testCaseId
	 * @return Invoice Amount
	 */
	String md_ExpectedInvoice(String testCaseId) {
		logi.logInfo "Inside md_ExpectedInvoice"
		def accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		def contractNo=Constant.mycontext.expand('${Properties#ContractNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		double prorateAmount = 0
		int invoice = 0, noofUnits = 0
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)

		int totalDays = 0
		HashMap<String, HashMap<String,String>> proPlans = getPlanServiceAndTieredPricingForProration()
		HashMap<String, HashMap<String,String>> fullPlans = getPlanServiceAndTieredPricingForFull()
		ConnectDB dataConnect = new ConnectDB()
		String contractStatusQuery, contractTypeNoQuery
		int contractStatus,contractTypeNo
		if  (Constant.TESTCASEID.contains("SINGLE")){
			contractTypeNoQuery = "Select type_no from ARIACORE.ACCT_PLAN_CONTRACTS WHERE ACCT_NO   ="+accountNo+" and CONTRACT_NO = " + contractNo+" and client_no= " +clientNo
			contractStatusQuery = "Select status_cd from ARIACORE.ACCT_PLAN_CONTRACTS WHERE ACCT_NO   ="+accountNo+" and CONTRACT_NO = " + contractNo+" and client_no= " +clientNo
			contractStatus = Integer.parseInt(dataConnect.executeQueryP2(contractStatusQuery))
			contractTypeNo = Integer.parseInt(dataConnect.executeQueryP2(contractTypeNoQuery))
			logi.logInfo "Contract Status "+contractStatus
			logi.logInfo "Contract TypeNo "+contractTypeNo
		}

		if (proPlans.size()!=0){
			Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
			resultHash.putAll(proPlans)
			logi.logInfo "ResultHash Sorted(Pro) in Method: "+ resultHash.toString()
			Iterator iterator = resultHash.entrySet().iterator();
			while (iterator.hasNext())
			{
				Map.Entry item = (Map.Entry) iterator.next();
				String planKey = item.getKey()

				String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planKey + " AND CLIENT_NO= " + clientNo
				int planTypeInd = Integer.parseInt(dataConnect.executeQueryP2(planTypeQuery))
				int noOfUnits =0

				// Get Current Virtual Time
				ClientParamUtils objClient = new ClientParamUtils()
				String currentVirtualTime = objClient.getCurrentVirtualTime(clientNo)
				logi.logInfo "got answer from currentVirtualTime : "+ currentVirtualTime
				// Stores the current virtual time in the specified format
				Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);


				HashMap<String,String> value = item.getValue()
				Iterator service = value.entrySet().iterator()
				Date lastBillDate

				if(planTypeInd == 1) {
					lastBillDate =  getSuppPlanLastBillDate(planKey, accountNo)
				} else {
					lastBillDate =  getMasterPlanLastBillDays(planKey, accountNo)
				}
				int billingInterval = Integer.parseInt(dataConnect.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planKey+ " and client_no="+clientNo))
				Date planEndDate = addMonthsFromGivenDate(lastBillDate,billingInterval)
				totalDays = getDaysBetween(lastBillDate,planEndDate)

				logi.logInfo "totalDays of a plan : "+planKey + " : is : "+totalDays

				//                      dataConnect.closeConnection()

				if(planTypeInd==0)
				{
					logi.logInfo "In if playtypeID 0"
					String newQuery = "SELECT PLAN_UNITS FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO = " + accountNo
					noOfUnits = Integer.parseInt(dataConnect.executeQueryP2(newQuery))
					logi.logInfo "no_of_units for master Plan : "+noOfUnits+ " plan : " +planKey
					//Integer.parseInt(getValueFromRequest("create_acct_complete","//master_plan_units"))
				}
				else {
					String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
					String[] noOfUnitsArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
					String[] suppPlanArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")
					logi.logInfo "supp_plans :::  " + suppPlanArray.toString()
					logi.logInfo "noOfUnits :::  " + noOfUnitsArray.toString()
					if(!suppPlanArray.toString().equalsIgnoreCase("null")){
						if(Arrays.asList(suppPlanArray).contains(planKey)){
							for( int j=0;j<suppPlanArray.length;j++) {
								if(suppPlanArray[j].toString().equalsIgnoreCase(planKey)){
									noOfUnits = Integer.parseInt(noOfUnitsArray[j])
									logi.logInfo "No Of Units : "+noOfUnits+ " Plan : " +planKey
									break
								}
							}
						}
						else {
							String suppPlan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
							if(suppPlan.equalsIgnoreCase(planKey))
								noOfUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
							else{
								suppPlan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
								if(suppPlan.equalsIgnoreCase(planKey))
									noOfUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
								else
									noOfUnits = 0
							}
						}
					}
					else {
						String suppPlan = getValueFromRequest("create_acct_complete","//supp_plans")
						if(suppPlan.equalsIgnoreCase(planKey))
							noOfUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))
						else
							noOfUnits = 0
					}
				}
				Date acctBillStartDate = getBillStartDate(clientNo, accountNo)

				Calendar cal2 = new GregorianCalendar()
				cal2.setTime(acctBillStartDate);
				logi.logInfo "Done cal2: "

				int createdMonth = cal2.get(Calendar.MONTH)
				logi.logInfo "createdMonth : "+createdMonth
				int createdYear = cal2.get(Calendar.YEAR)
				logi.logInfo "createdYear : "+createdYear

				//                                cal2 = new GregorianCalendar()
				//                                cal2.setTime(currentVTDate)
				//
				//                                int currentMonth = cal2.get(Calendar.MONTH)
				//                                logi.logInfo "Current Month : "+currentMonth

				int remainingDays = getRemainingDays(contractNo, clientNo)
				//                                if(createdMonth==11)
				//                                       remainingDays=remainingDays-1

				logi.logInfo "no_of_unitsssss : "+noOfUnits+ " plan : " +planKey
				String altFee = "0"
				if  (Constant.TESTCASEID.contains("SINGLE")){
					String altFeeQuery = "SELECT NVL(sum(amount),0) FROM ariacore.acct_plan_contract_alt_fees WHERE client_no="+clientNo+" and contract_no=(SELECT contract_no FROM ariacore.acct_plan_contracts WHERE client_no="+clientNo+" AND acct_no="+accountNo+" AND plan_no = "+planKey+" and rownum=1)"
					altFee = dataConnect.executeQueryP2(altFeeQuery)
				}

				while(service.hasNext()) {
					Map.Entry servicePairs = (Map.Entry) service.next()
					String service_no = servicePairs.getKey().toString()
					int seq_no=1;
					String tieredPricing = servicePairs.getValue().toString()
					int tieredPricingInt = Integer.parseInt(tieredPricing)

					logi.logInfo "RemainingDays : " + remainingDays
					logi.logInfo "TotalDay : " + totalDays
					double prorationFactor = remainingDays/totalDays
					logi.logInfo "prorationFactor : " + prorationFactor
					int ratePerUnit = 0
					// if (!altFee.equalsIgnoreCase("0"))
					//     ratePerUnit =altFee
					logi.logInfo "AltFee : " + altFee
					switch(tieredPricingInt){
						case 1:
						// ConnectDB database2 = new ConnectDB()
							if(!altFee.equals("0"))
							{
								if ((contractStatus == 1) || (contractStatus==99 && contractTypeNo==2) || (contractStatus==0 && contractTypeNo==3))
									ratePerUnit = Integer.parseInt(altFee)
								else
									ratePerUnit = Integer.parseInt(dataConnect.executeQueryP2("SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planKey+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "))

							}
							else
								ratePerUnit = Integer.parseInt(dataConnect.executeQueryP2("SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planKey+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "))
						// database2.closeConnection()
							break;

						case 2 :
						//ConnectDB database2 = new ConnectDB()
							ratePerUnit = Integer.parseInt(dataConnect.executeQueryP2("select rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+service_no+" and plan_no = "+planKey+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
						// database2.closeConnection()
							break;

						case 3 :
						// ConnectDB database2 = new ConnectDB()
							ratePerUnit = Integer.parseInt(dataConnect.executeQueryP2("select rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+service_no+" and plan_no = "+planKey+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
							noOfUnits = 1
						// database2.closeConnection()
							break;
					}

					double prorationUnits = noOfUnits.toDouble() * prorationFactor.toDouble()
					double amt = prorationUnits.toDouble() * ratePerUnit.toDouble()
					logi.logInfo "no_of_units : "+noOfUnits+" prorationFactor : "+prorationFactor+ " prorationUnits : " +prorationUnits
					logi.logInfo "prorationUnits : "+prorationUnits+" ratePerUnit : "+service_no+ " ratePerUnit : " +ratePerUnit+" Amount : :  " + amt
					logi.logInfo "Plan : "+planKey+" Service : "+service_no+ " Tiered Pricing : " +tieredPricing+" Amount : :  " + amt
					DecimalFormat df = new DecimalFormat("#.##");
					prorateAmount = prorateAmount + (df.format(amt)).toDouble()
				}

			}
		}
		if(fullPlans.size()!=0) {

			Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
			resultHash.putAll(fullPlans)


			logi.logInfo "ResultHash Sorted(full) in Method: "+ resultHash.toString()

			logi.logInfo " Inside Full calculation (proration) " + fullPlans.toString()
			String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
			logi.logInfo "testId : "+ testId
			logi.logInfo "Invoice : "+invoiceNo

			//ConnectDB database = new ConnectDB();
			String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
			String currentVirtualTime = dataConnect.executeQueryP2(Query);

			logi.logInfo "Current Virtual Time : "+ currentVirtualTime

			// Stores the current virtual time in the specified format
			Date currentDate = simpleFormat.parse(currentVirtualTime);
			Date acctStartDate = getBillStartDate(clientNo, accountNo)

			// Stores the account created time in the specified format
			int daysInvoice = getDaysBetween(acctStartDate, currentDate)+2
			logi.logInfo "daysInvoice"+daysInvoice
			Iterator iterator = resultHash.entrySet().iterator()
			logi.logInfo "Before Key setting "

			while (iterator.hasNext()) {
				Map.Entry pairs = (Map.Entry) iterator.next()
				String planNo = pairs.getKey().toString()
				logi.logInfo "Execution for the plan :  "+ planNo
				String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo

				int planTypeInd = Integer.parseInt(dataConnect.executeQueryP2(planTypeQuery))

				logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo

				Map<String,String> valueMap = pairs.getValue()
				Map<String,String> value = new TreeMap<String,String> ()
				value.putAll(valueMap)

				int billingInterval = Integer.parseInt(dataConnect.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planNo+ "and client_no="+clientNo))
				Date planEndDate = addMonthsFromGivenDate(acctStartDate,billingInterval)
				logi.logInfo "Plan End Date : "+planEndDate
				int durationOfPlan = getDaysBetween(acctStartDate, planEndDate)
				Calendar cal2 = new GregorianCalendar()
				cal2.setTime(planEndDate)

				int planEndMonth = cal2.get(Calendar.MONTH)
				int palnEndYear = cal2.get(Calendar.MONTH)
				cal2.setTime(acctStartDate)
				int planStartMonth = cal2.get(Calendar.MONTH)
				int palnStartYear = cal2.get(Calendar.MONTH)


				Iterator service = value.entrySet().iterator()
				logi.logInfo "Invoice calculation for a plan : "+planNo

				//int noofUnits =0

				if(planTypeInd==0)
				{
					logi.logInfo "In if playtypeID 0"
					String newQuery = "SELECT PLAN_UNITS FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO = " + accountNo
					noofUnits = Integer.parseInt(dataConnect.executeQueryP2(newQuery))
				}
				else {
					logi.logInfo "In else playtypeID 0"

					String[] noOfUnits = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
					String[] suppPlans = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")

					if (suppPlans.toString().equalsIgnoreCase("null")) {
						logi.logInfo("coming to singleplan if part")
						int suppPlan = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plans"))
						noofUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))

						logi.logInfo "single_supp_plans :::  " + suppPlan
						logi.logInfo "single_plan_noOfUnits11 :::  " + noofUnits
					}
					else {
						logi.logInfo "supp_plans :::  " + suppPlans.toString()
						logi.logInfo "noOfUnits :::  " + noOfUnits.toString()
						if(Arrays.asList(suppPlans).contains(planNo)){
							for( int j=0;j<suppPlans.length;j++) {
								if(suppPlans[j].toString().equalsIgnoreCase(planNo)){
									noofUnits = Integer.parseInt(noOfUnits[j])
									logi.logInfo "no_of_units : "+noofUnits+ " plan : " +planNo
									break
								}
							}
						}
						else {
							logi.logInfo "coming into last else part"
							String supp_plan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
							if(supp_plan.equalsIgnoreCase(planNo))
								noofUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
							else{
								supp_plan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
								if(supp_plan.equalsIgnoreCase(planNo))
									noofUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
								else
									noofUnits = 0
							}
						}
					}
				}
				logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo

				while(service.hasNext()){
					Map.Entry servicePairs = (Map.Entry) service.next()
					String serviceNo = servicePairs.getKey().toString()
					logi.logInfo "Invoice calculation for a service : "+serviceNo
					int seqNo=1;
					String tieredPricing = servicePairs.getValue().toString()
					int tieredPricingInt = Integer.parseInt(tieredPricing)
					logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
					String altFee = "0"
					int serviceInvoice
					if  (Constant.TESTCASEID.contains("SINGLE")){
						String altFeeQuery = "SELECT NVL(sum(amount),0) FROM ariacore.acct_plan_contract_alt_fees WHERE client_no="+clientNo+" and contract_no=(SELECT contract_no FROM ariacore.acct_plan_contracts WHERE client_no="+clientNo+" AND acct_no="+accountNo+" AND plan_no = "+planNo+" and rownum=1)"
						altFee = dataConnect.executeQueryP2(altFeeQuery)
					}
					String query
					switch(tieredPricingInt){
						case 1:
							logi.logInfo "Case 1"
							if(!altFee.equals("0"))
							{
								if ((contractStatus == 1) || (contractStatus==99 && contractTypeNo==2) || (contractStatus==0 && contractTypeNo==3))
									query = "SELECT "+Integer.parseInt(altFee)+" * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
								else
									query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
							}
							else
								query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
							serviceInvoice = Integer.parseInt(dataConnect.executeQueryP2(query))
							logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
							invoice  = invoice+serviceInvoice
							logi.logInfo "After adding with invoice " + invoice
							seqNo++;
							break;
						case 2 :
							logi.logInfo "Case 2"
							query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"

							serviceInvoice = Integer.parseInt(dataConnect.executeQueryP2(query))
							logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
							invoice  = invoice+serviceInvoice
							logi.logInfo "After adding with invoice " + invoice
							break;
						case 3 :
							logi.logInfo "Case 3"
							query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
							serviceInvoice = Integer.parseInt(dataConnect.executeQueryP2(query))
							logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
							invoice  = invoice+serviceInvoice
							logi.logInfo "After adding with invoice " + invoice
							break;
					}
				}
			}
		}

		dataConnect.closeConnection()

		DecimalFormat df = new DecimalFormat("#.##");
		double resultInvoice = prorateAmount +  (df.format(invoice)).toDouble()
		return df.format(resultInvoice).toString();
	}

	/**
	 * Calculates the charges for the Usage
	 * @param testCaseId
	 * @return Usage charges
	 */
	public String calculateUsageAmount(String testid){
		logi.logInfo("calling calculateUsageAmount")
		def accountNo = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String service  = Constant.SERVICENAME
		logi.logInfo( "service" +service)
		String advVirtualTime
		int totalUsgAmt
		if (service.contains("^"))
			advVirtualTime = service.substring(service.indexOf("_")+1, service.indexOf("^"))
		else
			advVirtualTime = service.substring(service.indexOf("_")+1, service.length())
		logi.logInfo( "advVirtualTime" +advVirtualTime)
		String amount
		String invoiceNo = getInvoiceNoP2(clientNo, accountNo, advVirtualTime).toString();
		logi.logInfo( "invoiceNo" +invoiceNo)
		ConnectDB database = new ConnectDB();
		String query = "select service_no from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+accountNo+" GROUP BY plan_no) and client_no = "+clientNo+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+clientNo+" and usage_based = 1) order by service_no"
		String[] uservices = database.executeQueryReturnValuesAsArray(query)
		logi.logInfo("uservices "+uservices)
		for(serviceNo in uservices)
		{
			totalUsgAmt+= Integer.parseInt(database.executeQueryP2("select NVL(sum(debit),0) from ARIACORE.GL_DETAIL where invoice_no=" +invoiceNo+ " and service_no ="+serviceNo))
		}

		logi.logInfo("Total Usage plan amount"+totalUsgAmt.toString())
		return totalUsgAmt.toString()
	}

	/**
	 * Calculates Invoice charges with Usage
	 * @param testCaseId
	 * @return invoice amount with usage
	 */
	public String md_ExpInvoiceWithUsage(String testCaseId){
		//String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
		int totalInvoice
		int exp = Integer.parseInt(md_ExpInvoice_ByPlan(testCaseId))
		logi.logInfo("expInvoice "+exp)
		int usg =Integer.parseInt(calculateUsageAmount(testCaseId))
		logi.logInfo("usgInvoice "+usg)
		totalInvoice =  exp+ usg
		logi.logInfo("totalInvoice "+totalInvoice)
		return totalInvoice
	}

	/**
	 * Calculates the bill start for the given account
	 * @param Client Number, Account Number 
	 * @return bill start date
	 */
	public Date getBillStartDate(String client_no, String acct_no){
		ClientParamUtils clientParamUtils;
		Date acctCreatedDate = new Date();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String retroactiveStartDate = getValueFromRequest("create_acct_complete","//retroactive_start_date").toString()
		String alt_bill_day = getValueFromRequest("create_acct_complete","//alt_bill_day").toString()
		String acctActualStartDate = null
		ConnectDB database = new ConnectDB();
		Calendar cal = new GregorianCalendar()
		if(retroactiveStartDate.equalsIgnoreCase("NoVal")&&alt_bill_day.equalsIgnoreCase("NoVal")){
			acctActualStartDate = database.executeQueryP2("SELECT created from ariacore.acct_details where acct_no = "+ acct_no)
			logi.logInfo "Created date from DB: "+acctActualStartDate
			acctCreatedDate = simpleFormat.parse(acctActualStartDate)
			cal.setTime(acctCreatedDate);
		}
		//		else if(!(retroactive_start_date.equalsIgnoreCase("NoVal"))&&!(alt_bill_day.equalsIgnoreCase("NoVal"))){
		//			String acctCreatedDateDB = database.executeQueryP2("SELECT created from ariacore.acct_details where acct_no = "+ acct_no)
		//			acctActualStartDate =  clientParamUtils.getPastOrFutureMonthandDayFromDateGievnP2(acctCreatedDateDB, retroactive_start_date, "yyyy-MM-dd HH:mm:ss")
		//		}
		//		else if(!(retroactive_start_date.equalsIgnoreCase("NoVal"))&&(alt_bill_day.equalsIgnoreCase("NoVal"))){
		//			acctActualStartDate = database.executeQueryP2("SELECT created from ariacore.acct_details where acct_no = "+ acct_no)
		//			logi.logInfo "Created date from DB: "+acctActualStartDate
		//			acctCreatedDate = simpleFormat.parse(acctActualStartDate)
		//			cal.setTime(acctCreatedDate);
		//		}
		else if(!(retroactiveStartDate.equalsIgnoreCase("NoVal"))){
			logi.logInfo "Inside retroactiveStartDate and Retroactive start date is : " + retroactiveStartDate

			// String to Date conversion
			Date finalDate = simpleFormat.parse(retroactiveStartDate);

			String newDateString = simpleFormat.format(finalDate);
			logi.logInfo ("String sdate : "+ newDateString);

			logi.logInfo "finalDate : "+finalDate
			return finalDate


		}
		else if (!alt_bill_day.equalsIgnoreCase("NoVal")) {

			logi.logInfo "In loop Alt Bill Day not empty"
			ResultSet rs = database.executeQuery("SELECT created from ariacore.acct_details where acct_no = "+ acct_no)
			if (rs.next()){
				acctActualStartDate = rs.getDate(1)
				acctCreatedDate = rs.getDate(1)
			}

			cal.setTime(acctCreatedDate);
			logi.logInfo "Start Date / Create date from DB: "+acctActualStartDate

			int createdDay = cal.get(Calendar.DATE)
			int createdMonth = cal.get(Calendar.MONTH)
			int createdYear = cal.get(Calendar.YEAR)
			int alt_bill_day_int = Integer.parseInt(alt_bill_day)

			logi.logInfo "Alt bill Date in getBillDate : "+alt_bill_day
			logi.logInfo "Alt bill Date in alt_bill_day_int  : "+alt_bill_day_int
			logi.logInfo "createdDay : "+createdDay
			logi.logInfo "createdMonth : "+createdMonth
			logi.logInfo "createdYear : "+createdYear

			cal.set(Calendar.DATE, 1)
			cal.set(Calendar.DATE, cal.get(Calendar.DATE)-1);
			logi.logInfo "Days of Month : "+ cal.get(Calendar.DATE)
			int daysInMonth = cal.get(Calendar.DATE)
			logi.logInfo "daysInMonth : "+daysInMonth

			if(alt_bill_day_int>=createdDay && alt_bill_day_int<= daysInMonth ){
				cal.set(createdYear,createdMonth, alt_bill_day_int );
			}
			else if(alt_bill_day_int<createdDay){
				if(createdMonth==11) {
					cal.set(createdYear+1,0, alt_bill_day_int );
				}
				else {
					cal.set(createdYear,createdMonth+1, alt_bill_day_int );
				}
			}
		}

		String finalDateString = simpleFormat.format(cal.getTime())
		logi.logInfo "Bill start date : "+ finalDateString
		Date finalDate = simpleFormat.parse(finalDateString)
		return finalDate
	}

	public int getDaysBetween(Date fromDate, Date toDate) {
		return (int) ((toDate.getTime() - fromDate.getTime()) / (1000 * 60 * 60 * 24));
	}

	/**
	 * Adds given number of months to the given date
	 * @param testCaseId
	 * @return End Date
	 */
	public Date addMonthsFromGivenDate(Date dateGiven, int noOfMonths)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateGiven);
		cal.add(Calendar.MONTH,noOfMonths)
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		String dateMonthsAdded = format1.format(cal.getTime());
		Date planEndDate = format1.parse(dateMonthsAdded)
		return planEndDate
	}

	//To get the assign_supp_plan via created contract no
	/**
	 * Extracts Supp plan contract number
	 * @param testCaseId
	 * @return Contract Number
	 */
	public String md_GetAssignSuppPlanContractNo(String testCaseId)
	{
		def accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		def contractNo = db.executeQueryP2("(select CONTRACT_NO from (SELECT CONTRACT_NO ,row_number() over (order by CREATE_DATE desc) as sno FROM ARIACORE.ACCT_PLAN_CONTRACTS where acct_no="+accountNo+" and client_no="+clientNo+") where sno=1)")
		logi.logInfo "Contract No : "+ contractNo
		return contractNo.toString()
	}

	/**
	 * Calculates prorated invoice for the account 
	 * @param testCaseId
	 * @return Prorated invoice
	 */
	String md_CONTRACT_PRORATE_INVOICE_BY_PLANS(String testCaseId) {
		logi.logInfo "Inside md_CONTRACT_PRORATE_INVOICE_BY_PLANS"
		def accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		def contractNo=Constant.mycontext.expand('${Properties#ContractNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		double prorateAmount = 0
		def invoice = 0
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)

		def totalDays = 0
		HashMap<String, HashMap<String,String>> proPlans = getPlanServiceAndTieredPricingForProration()
		HashMap<String, HashMap<String,String>> fullPlans = getPlanServiceAndTieredPricingForFull()
		ConnectDB dataConnect = new ConnectDB()
		String contractStatusQuery, contractTypeNoQuery
		def contractStatus,contractTypeNo

		if (proPlans.size()!=0){
			Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
			resultHash.putAll(proPlans)
			logi.logInfo "ResultHash Sorted(Pro) in Method: "+ resultHash.toString()
			Iterator iterator = resultHash.entrySet().iterator();
			while (iterator.hasNext())
			{
				Map.Entry item = (Map.Entry) iterator.next();
				String planKey = item.getKey()
				if  (Constant.TESTCASENAME.contains("Contract"))
				{
					contractTypeNoQuery = "SELECT type_no from ARIACORE.ACCT_PLAN_CONTRACTS where contract_no=(select CONTRACT_NO from (SELECT CONTRACT_NO ,row_number() over (order by CREATE_DATE) as sno FROM ARIACORE.ACCT_PLAN_CONTRACTS where status_cd != -3 and acct_no="+accountNo+" and client_no=" +clientNo+" AND plan_no = "+planKey+") where sno=1)"
					contractStatusQuery = "SELECT status_cd from ARIACORE.ACCT_PLAN_CONTRACTS where contract_no=(select CONTRACT_NO from (SELECT CONTRACT_NO ,row_number() over (order by CREATE_DATE) as sno FROM ARIACORE.ACCT_PLAN_CONTRACTS where status_cd != -3 and acct_no="+accountNo+" and client_no=" +clientNo+" AND plan_no = "+planKey+") where sno=1)"
					contractStatus = dataConnect.executeQueryP2(contractStatusQuery);
					contractTypeNo = dataConnect.executeQueryP2(contractTypeNoQuery);
					if (contractStatus != null)
						contractStatus = contractStatus.toInteger()

					if (contractTypeNo != null)
						contractTypeNo = contractTypeNo.toInteger()
					logi.logInfo "Contract Status "+contractStatus
					logi.logInfo "Contract TypeNo "+contractTypeNo
				}

				String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planKey + " AND CLIENT_NO= " + clientNo
				int planTypeInd = Integer.parseInt(dataConnect.executeQueryP2(planTypeQuery))
				int noOfUnits =0

				// Get Current Virtual Time
				ClientParamUtils objClient = new ClientParamUtils()
				String currentVirtualTime = objClient.getCurrentVirtualTime(clientNo)
				logi.logInfo "got answer from currentVirtualTime : "+ currentVirtualTime
				// Stores the current virtual time in the specified format
				Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);


				HashMap<String,String> value = item.getValue()
				Iterator service = value.entrySet().iterator()
				Date lastBillDate

				if(planTypeInd == 1) {
					lastBillDate =  getSuppPlanLastBillDate(planKey, accountNo)
				} else {
					lastBillDate =  getMasterPlanLastBillDays(planKey, accountNo)
				}
				int billingInterval = Integer.parseInt(dataConnect.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planKey+ " and client_no="+clientNo))
				Date planEndDate = addMonthsFromGivenDate(lastBillDate,billingInterval)
				totalDays = getDaysBetween(lastBillDate, planEndDate)

				logi.logInfo "totalDays of a plan : "+planKey + " : is : "+totalDays

				//			   dataConnect.closeConnection()
				def noofUnits =0
				if(planTypeInd==0)
					noOfUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//master_plan_units"))
				else {
					String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
					String[] noOfUnitsArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
					String[] suppPlanArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")


					if (suppPlanArray.toString().equalsIgnoreCase("null")) {
						logi.logInfo("coming to singleplan if part")
						int suppPlan = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plans"))
						noofUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))

						logi.logInfo "single_supp_plans :::  " + suppPlan
						logi.logInfo "single_plan_noOfUnits11 :::  " + noofUnits
					}
					else {
						logi.logInfo "supp_plans :::  " + suppPlanArray.toString()
						logi.logInfo "noOfUnits :::  " + noOfUnitsArray.toString()
						if(Arrays.asList(suppPlanArray).contains(planKey)){
							for( int j=0;j<suppPlanArray.length;j++) {
								if(suppPlanArray[j].toString().equalsIgnoreCase(planKey)){
									noOfUnits = Integer.parseInt(noOfUnitsArray[j])
									logi.logInfo "No Of Units : "+noOfUnits+ " Plan : " +planKey
									break
								}
							}
						}
						else {
							String suppPlan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
							if(suppPlan.equalsIgnoreCase(planKey))
								noOfUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
							else{
								suppPlan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
								if(suppPlan.equalsIgnoreCase(planKey))
									noOfUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
								else
									noOfUnits = 0
							}
						}
					}
				}
				Date acctBillStartDate = getBillStartDate(clientNo, accountNo)

				Calendar cal2 = new GregorianCalendar()
				cal2.setTime(acctBillStartDate);
				logi.logInfo "Done cal2: "

				int createdMonth = cal2.get(Calendar.MONTH)
				logi.logInfo "createdMonth : "+createdMonth
				int createdYear = cal2.get(Calendar.YEAR)
				logi.logInfo "createdYear : "+createdYear

				//                                cal2 = new GregorianCalendar()
				//                                cal2.setTime(currentVTDate)
				//
				//                                int currentMonth = cal2.get(Calendar.MONTH)
				//                                logi.logInfo "Current Month : "+currentMonth

				int remainingDays = getRemainingDays(contractNo, clientNo)
				//                                if(createdMonth==11)
				//                                       remainingDays=remainingDays-1

				logi.logInfo "no_of_unitsssss : "+noOfUnits+ " plan : " +planKey
				String altFee = "0"
				if  (Constant.TESTCASENAME.contains("Contract")){
					String altFeeQuery = "SELECT NVL(sum(amount),0) from ariacore.acct_plan_contract_alt_fees where contract_no=(select CONTRACT_NO from (SELECT CONTRACT_NO ,row_number() over (order by CREATE_DATE) as sno FROM ARIACORE.ACCT_PLAN_CONTRACTS where status_cd != -3 and acct_no="+accountNo+" and client_no="+clientNo+" AND plan_no = "+planKey+") where sno=1)"
					altFee = dataConnect.executeQueryP2(altFeeQuery)
				}

				while(service.hasNext()) {
					Map.Entry servicePairs = (Map.Entry) service.next()
					String service_no = servicePairs.getKey().toString()
					int seq_no=1;
					String tieredPricing = servicePairs.getValue().toString()
					int tieredPricingInt = Integer.parseInt(tieredPricing)

					logi.logInfo "RemainingDays : " + remainingDays
					logi.logInfo "TotalDay : " + totalDays
					double prorationFactor = remainingDays/totalDays
					logi.logInfo "prorationFactor : " + prorationFactor
					def ratePerUnit = 0
					// if (!altFee.equalsIgnoreCase("0"))
					//	ratePerUnit =altFee
					logi.logInfo "AltFee : " + altFee
					switch(tieredPricingInt){
						case 1:
						// ConnectDB database2 = new ConnectDB()
							if(!altFee.equals("0"))
							{
								if ((contractStatus == 1) || (contractStatus==99 && contractTypeNo==2) || (contractStatus==0 && contractTypeNo==3))
									ratePerUnit = Integer.parseInt(altFee)
								else
									ratePerUnit = dataConnect.executeQueryP2("SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planKey+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no ")

							}
							else
								ratePerUnit = dataConnect.executeQueryP2("SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planKey+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no ")
						// database2.closeConnection()
							break;

						case 2 :
						//ConnectDB database2 = new ConnectDB()
							ratePerUnit = dataConnect.executeQueryP2("select rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+service_no+" and plan_no = "+planKey+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1")
						// database2.closeConnection()
							break;

						case 3 :
						// ConnectDB database2 = new ConnectDB()
							ratePerUnit =dataConnect.executeQueryP2("select rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+service_no+" and plan_no = "+planKey+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1")
							noOfUnits = 1
						// database2.closeConnection()
							break;
					}

					double prorationUnits = noOfUnits.toDouble() * prorationFactor.toDouble()
					double amt = prorationUnits.toDouble() * ratePerUnit.toDouble()
					logi.logInfo "no_of_units : "+noOfUnits+" prorationFactor : "+prorationFactor+ " prorationUnits : " +prorationUnits
					logi.logInfo "prorationUnits : "+prorationUnits+" ratePerUnit : "+service_no+ " ratePerUnit : " +ratePerUnit+" Amount : :  " + amt
					logi.logInfo "Plan : "+planKey+" Service : "+service_no+ " Tiered Pricing : " +tieredPricing+" Amount : :  " + amt
					DecimalFormat df = new DecimalFormat("#.##");
					prorateAmount = prorateAmount + (df.format(amt)).toDouble()
				}

			}
		}
		if(fullPlans.size()!=0) {

			Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
			resultHash.putAll(fullPlans)


			logi.logInfo "ResultHash Sorted(full) in Method: "+ resultHash.toString()

			logi.logInfo " Inside Full calculation (proration) " + fullPlans.toString()
			String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
			logi.logInfo "testId : "+ testId
			logi.logInfo "Invoice : "+invoiceNo

			//ConnectDB database = new ConnectDB();
			String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
			String currentVirtualTime = dataConnect.executeQueryP2(Query);

			logi.logInfo "Current Virtual Time : "+ currentVirtualTime

			// Stores the current virtual time in the specified format
			Date currentDate = simpleFormat.parse(currentVirtualTime);
			Date acctStartDate = getBillStartDate(clientNo, accountNo)

			// Stores the account created time in the specified format
			int daysInvoice = getDaysBetween(acctStartDate, currentDate)+2
			logi.logInfo "daysInvoice"+daysInvoice
			Iterator iterator = resultHash.entrySet().iterator()
			logi.logInfo "Before Key setting "

			while (iterator.hasNext()) {
				Map.Entry pairs = (Map.Entry) iterator.next()
				String planNo = pairs.getKey().toString()
				logi.logInfo "Execution for the plan :  "+ planNo

				if  (Constant.TESTCASENAME.contains("Contract"))
				{
					contractTypeNoQuery = "SELECT type_no from ARIACORE.ACCT_PLAN_CONTRACTS where contract_no=(select CONTRACT_NO from (SELECT CONTRACT_NO ,row_number() over (order by CREATE_DATE) as sno FROM ARIACORE.ACCT_PLAN_CONTRACTS where status_cd != -3 and acct_no="+accountNo+" and client_no=" +clientNo+" AND plan_no = "+planNo+") where sno=1)"
					contractStatusQuery = "SELECT status_cd from ARIACORE.ACCT_PLAN_CONTRACTS where contract_no=(select CONTRACT_NO from (SELECT CONTRACT_NO ,row_number() over (order by CREATE_DATE) as sno FROM ARIACORE.ACCT_PLAN_CONTRACTS where status_cd != -3 and acct_no="+accountNo+" and client_no=" +clientNo+" AND plan_no = "+planNo+") where sno=1)"
					contractStatus = dataConnect.executeQueryP2(contractStatusQuery);
					contractTypeNo = dataConnect.executeQueryP2(contractTypeNoQuery);
					if (contractStatus != null)
						contractStatus = contractStatus.toInteger()

					if (contractTypeNo != null)
						contractTypeNo = contractTypeNo.toInteger()
					logi.logInfo "Contract Status "+contractStatus
					logi.logInfo "Contract TypeNo "+contractTypeNo
				}

				String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
				String planStatusQuery, planStatusDataQuery

				int planTypeInd = Integer.parseInt(dataConnect.executeQueryP2(planTypeQuery))

				logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo

				Map<String,String> valueMap = pairs.getValue()
				Map<String,String> value = new TreeMap<String,String> ()
				value.putAll(valueMap)

				int billingInterval = Integer.parseInt(dataConnect.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planNo+ "and client_no="+clientNo))
				Date planEndDate = addMonthsFromGivenDate(acctStartDate,billingInterval)
				logi.logInfo "Plan End Date : "+planEndDate
				int durationOfPlan = getDaysBetween(acctStartDate, planEndDate)
				Calendar cal2 = new GregorianCalendar()
				cal2.setTime(planEndDate)

				int planEndMonth = cal2.get(Calendar.MONTH)
				int palnEndYear = cal2.get(Calendar.MONTH)
				cal2.setTime(acctStartDate)
				int planStartMonth = cal2.get(Calendar.MONTH)
				int palnStartYear = cal2.get(Calendar.MONTH)


				Iterator service = value.entrySet().iterator()
				logi.logInfo "Invoice calculation for a plan : "+planNo

				int noofUnits =0

				if(planTypeInd==0)
				{
					logi.logInfo "In if playtypeID 0"
					noofUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//master_plan_units"))
				}
				else {
					logi.logInfo "In else playtypeID 0"

					String[] noOfUnits = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
					String[] suppPlans = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")

					if (suppPlans.toString().equalsIgnoreCase("null")) {
						logi.logInfo("coming to singleplan if part")
						int suppPlan = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plans"))
						noofUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))

						logi.logInfo "single_supp_plans :::  " + suppPlan
						logi.logInfo "single_plan_noOfUnits11 :::  " + noofUnits
					}
					else {
						logi.logInfo "supp_plans :::  " + suppPlans.toString()
						logi.logInfo "noOfUnits :::  " + noOfUnits.toString()
						if(Arrays.asList(suppPlans).contains(planNo)){
							for( int j=0;j<suppPlans.length;j++) {
								if(suppPlans[j].toString().equalsIgnoreCase(planNo)){
									noofUnits = Integer.parseInt(noOfUnits[j])
									logi.logInfo "no_of_units : "+noofUnits+ " plan : " +planNo
									break
								}
							}
						}
						else {
							logi.logInfo "coming into last else part"
							String supp_plan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
							if(supp_plan.equalsIgnoreCase(planNo))
								noofUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
							else{
								supp_plan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
								if(supp_plan.equalsIgnoreCase(planNo))
									noofUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
								else
									noofUnits = 0
							}
						}
					}
				}
				logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo

				while(service.hasNext()){
					Map.Entry servicePairs = (Map.Entry) service.next()
					String serviceNo = servicePairs.getKey().toString()
					logi.logInfo "Invoice calculation for a service : "+serviceNo
					int seqNo=1;
					String tieredPricing = servicePairs.getValue().toString()
					int tieredPricingInt = Integer.parseInt(tieredPricing)
					logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
					String altFee = "0"
					def serviceInvoice
					if  (Constant.TESTCASENAME.contains("Contract")){
						String altFeeQuery = "SELECT NVL(sum(amount),0) from ariacore.acct_plan_contract_alt_fees where contract_no=(select CONTRACT_NO from (SELECT CONTRACT_NO ,row_number() over (order by CREATE_DATE) as sno FROM ARIACORE.ACCT_PLAN_CONTRACTS where status_cd != -3 and acct_no="+accountNo+" and client_no="+clientNo+" AND plan_no = "+planNo+") where sno=1)"
						altFee = dataConnect.executeQueryP2(altFeeQuery)
					}
					String query
					switch(tieredPricingInt){
						case 1:
							logi.logInfo "Case 1"
							if(!altFee.equals("0"))
							{
								if ((contractStatus == 1) || (contractStatus==99 && contractTypeNo==2) || (contractStatus==0 && contractTypeNo==3))
									query = "SELECT "+Integer.parseInt(altFee)+" * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
								else
									query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
							}
							else
								query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
							serviceInvoice = dataConnect.executeQueryP2(query).toDouble()
							logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
							invoice  = invoice+serviceInvoice
							logi.logInfo "After adding with invoice " + invoice
							seqNo++;
							break;
						case 2 :
							logi.logInfo "Case 2"
							query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"

							serviceInvoice = dataConnect.executeQueryP2(query).toDouble()
							logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
							invoice  =  invoice+serviceInvoice
							logi.logInfo "After adding with invoice " + invoice
							break;
						case 3 :
							logi.logInfo "Case 3"
							query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
							serviceInvoice = dataConnect.executeQueryP2(query).toDouble()
							logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
							invoice  =  invoice+serviceInvoice
							logi.logInfo "After adding with invoice " + invoice
							break;
					}
				}
			}
			//dataConnect.closeConnection()
		}
		DecimalFormat df = new DecimalFormat("#.##");
		double resultInvoice = prorateAmount +  (df.format(invoice)).toDouble()
		return df.format(resultInvoice).toString();
	}

	/**
	 * Calculates Expecetd invoice amount
	 * @param testCaseId
	 * @return Invoice Amount
	 */
	public String md_ExpInvoice_ByPlan(String testCaseId){
		logi.logInfo "Inside md_ExpInvoice_ByPlan"
		String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
		logi.logInfo "testId : "+ testId
		def accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		def contractNo=Constant.mycontext.expand('${Properties#ContractNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		double prorateAmount = 0
		def invoice = 0
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)

		int totalDays = 0
		HashMap<String, HashMap<String,String>> fullPlans = getPlanServiceAndTieredPricingForFull()
		ConnectDB dataConnect = new ConnectDB()
		String contractStatusQuery, contractTypeNoQuery
		def contractStatus, contractTypeNo
		Iterator iterator = fullPlans.entrySet().iterator();
		while (iterator.hasNext())
		{
			Map.Entry item = (Map.Entry) iterator.next();
			String planNo = item.getKey()
			if  (Constant.TESTCASENAME.contains("Contract"))
			{
				contractTypeNoQuery = "SELECT type_no from ARIACORE.ACCT_PLAN_CONTRACTS where contract_no=(select CONTRACT_NO from (SELECT CONTRACT_NO ,row_number() over (order by CREATE_DATE) as sno FROM ARIACORE.ACCT_PLAN_CONTRACTS where status_cd != -3 and acct_no="+accountNo+" and client_no=" +clientNo+" AND plan_no = "+planNo+") where sno=1)"
				contractStatusQuery = "SELECT status_cd from ARIACORE.ACCT_PLAN_CONTRACTS where contract_no=(select CONTRACT_NO from (SELECT CONTRACT_NO ,row_number() over (order by CREATE_DATE) as sno FROM ARIACORE.ACCT_PLAN_CONTRACTS where status_cd != -3 and acct_no="+accountNo+" and client_no=" +clientNo+" AND plan_no = "+planNo+") where sno=1)"
				contractStatus = dataConnect.executeQueryP2(contractStatusQuery);
				contractTypeNo = dataConnect.executeQueryP2(contractTypeNoQuery);
				if (contractStatus != null)
					contractStatus = contractStatus.toInteger()

				if (contractTypeNo != null)
					contractTypeNo = contractTypeNo.toInteger()
				logi.logInfo "Contract Status "+contractStatus
				logi.logInfo "Contract TypeNo "+contractTypeNo
			}

			String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
			def planTypeInd = dataConnect.executeQueryP2(planTypeQuery).toDouble()
			//def noOfUnits =0

			// Get Current Virtual Time
			ClientParamUtils objClient = new ClientParamUtils()
			String currentVirtualTime = objClient.getCurrentVirtualTime(clientNo)
			logi.logInfo "got answer from currentVirtualTime : "+ currentVirtualTime
			// Stores the current virtual time in the specified format
			Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);

			HashMap<String,String> value = item.getValue()
			Iterator service = value.entrySet().iterator()

			logi.logInfo "Invoice calculation for a plan : "+planNo

			def noofUnits =0

			if(planTypeInd==0)
			{
				logi.logInfo "In if playtypeID 0"
				String newQuery = "SELECT PLAN_UNITS FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO = " + accountNo
				noofUnits = dataConnect.executeQueryP2(newQuery).toDouble()
				//Integer.parseInt(getValueFromRequest("create_acct_complete","//master_plan_units"))
			}
			else {
				logi.logInfo "In else playtypeID 0"
				//String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
				String[] noOfUnitsArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
				String[] suppPlans = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")

				if (suppPlans.toString().equalsIgnoreCase("null")) {
					logi.logInfo("coming to singleplan if part")
					int suppPlan = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plans"))
					noofUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))

					logi.logInfo "single_supp_plans :::  " + suppPlan
					logi.logInfo "single_plan_noOfUnits11 :::  " + noofUnits
				}
				else {
					logi.logInfo "supp_plans :::  " + suppPlans.toString()
					logi.logInfo "noOfUnits :::  " + noOfUnitsArray.toString()
					if(Arrays.asList(suppPlans).contains(planNo)){
						for( int j=0;j<suppPlans.length;j++) {
							if(suppPlans[j].toString().equalsIgnoreCase(planNo)){
								noofUnits = Integer.parseInt(noOfUnitsArray[j])
								logi.logInfo "no_of_units : "+noofUnits+ " plan : " +planNo
								break
							}
						}
					}
					else {
						logi.logInfo "coming into last else part"
						String supp_plan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
						if(supp_plan.equalsIgnoreCase(planNo))
							noofUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
						else{
							supp_plan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
							if(supp_plan.equalsIgnoreCase(planNo))
								noofUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
							else
								noofUnits = 0
						}
					}
				}
			}

			logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo

			while(service.hasNext()){
				Map.Entry servicePairs = (Map.Entry) service.next()
				String serviceNo = servicePairs.getKey().toString()
				logi.logInfo "Invoice calculation for a service : "+serviceNo
				int seqNo=1;
				String tieredPricing = servicePairs.getValue().toString()
				int tieredPricingInt = Integer.parseInt(tieredPricing)
				logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
				String altFee = "0"
				def serviceInvoice
				if  (Constant.TESTCASENAME.contains("Contract")){
					String altFeeQuery = "SELECT NVL(sum(amount),0) from ariacore.acct_plan_contract_alt_fees where contract_no=(select CONTRACT_NO from (SELECT CONTRACT_NO ,row_number() over (order by CREATE_DATE) as sno FROM ARIACORE.ACCT_PLAN_CONTRACTS where status_cd != -3 and acct_no="+accountNo+" and client_no="+clientNo+" AND plan_no = "+planNo+") where sno=1)"
					altFee = dataConnect.executeQueryP2(altFeeQuery)
				}
				logi.logInfo "AltFee: "+altFee
				String query
				switch(tieredPricingInt)
				{
					case 1:
						logi.logInfo "Case 1 "
						if(!altFee.equals("0"))
						{
							if ((contractStatus == 1) || (contractStatus==99 && contractTypeNo==2) || (contractStatus==0 && contractTypeNo==3))
								query = "SELECT "+Integer.parseInt(altFee)+" * NVL(sum(ARHD.RECURRING_FACTOR),1) as result FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
							else
								query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
						}
						else
							query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
						serviceInvoice = dataConnect.executeQueryP2(query).toDouble()
						logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
						invoice  = invoice+serviceInvoice
						logi.logInfo "After adding with invoice " + invoice
						seqNo++;
						break;
					case 2 :
						logi.logInfo "Case 2"
						query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						serviceInvoice = dataConnect.executeQueryP2(query).toDouble()
						logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
						invoice  = invoice+serviceInvoice
						logi.logInfo "After adding with invoice " + invoice
						break;
					case 3 :
						logi.logInfo "Case 3"
						query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						serviceInvoice = dataConnect.executeQueryP2(query).toDouble()
						logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
						invoice  = invoice+serviceInvoice
						logi.logInfo "After adding with invoice " + invoice
						break;
				}
			}
		}
		String creditQuery = "SELECT NVL(SUM(AMOUNT),0) FROM ARIACORE.CREDITS WHERE ACCT_NO=" + accountNo + " AND LEFT_TO_APPLY=0"
		def creditAmount = dataConnect.executeQueryP2(creditQuery).toDouble()
		logi.logInfo("Credit Amount : " + creditAmount)
		//dataConnect.closeConnection()
		if(creditAmount!=0 && creditAmount!=null){
			DecimalFormat df=new DecimalFormat("#.##")
			float finalInvoice = new Float(invoice) - new Float(creditAmount)
			return df.format(finalInvoice).toString();
		}
		else
		{
			DecimalFormat df=new DecimalFormat("#.##")
			return df.format(invoice.toDouble()).toString();
		}
	}

	/**
	 * Gives the last bill date for the supp plan
	 * @param plan number, account number
	 * @return End Date
	 */
	Date getSuppPlanLastBillDate(String suppPlanNo, String acctNo) {

		Date lastBillDate
		String suppPlanLastBillDays = "SELECT TRUNC(AC.LAST_BILL_DATE) as LastBillDate FROM ARIACORE.ACCT_SUPP_PLAN_MAP AC WHERE ACCT_NO=" + acctNo + " AND SUPP_PLAN_NO=" + suppPlanNo
		ResultSet resultSet = db.executePlaQuery(suppPlanLastBillDays);
		if (resultSet.next()){
			lastBillDate = resultSet.getDate(1);
			logi.logInfo "Inside Last Bill Date : " + resultSet.getString(1)
		}
		return lastBillDate
	}

	/**
	 * Gives the last bill date for the Master plan
	 * @param plan number,account number
	 * @return End Date
	 */
	Date getMasterPlanLastBillDays(String planNo, String acctNo) {

		Date lastBillDate
		String masterPlanLastBillDays = "SELECT TRUNC(AC.LAST_BILL_DATE) as LastBillDate FROM ARIACORE.ACCT AC WHERE ACCT_NO=" + acctNo + " AND PLAN_NO=" + planNo
		ResultSet resultSet = db.executePlaQuery(masterPlanLastBillDays);
		if (resultSet.next()){
			lastBillDate = resultSet.getDate(1);
			logi.logInfo "Inside Last Bill Date : " + resultSet.getString(1)
		}
		return lastBillDate
	}

	/**
	 * Gives remaining contract days
	 * @param testCaseId
	 * @return number of days
	 */
	int getRemainingDays(String contractNo, String clientNo) {
		logi.logInfo "Inside getRemainingDays"
		ClientParamUtils objClient = new ClientParamUtils()
		String contractRemainingDaysQuery
		if(Constant.TESTCASEID.contains("UNIVERSALPLAN"))
			contractRemainingDaysQuery = "SELECT  ((Select TRUNC(END_DATE) from ARIACORE.ACCT_UNIVERSAL_CONTRACTS WHERE CONTRACT_NO ="+ contractNo+" ) - TRUNC(ARIACORE.ARIAVIRTUALTIME("+clientNo+")))  as days from DUAL"
		else if (Constant.TESTCASENAME.equals("MultiPlanContract"))
			contractRemainingDaysQuery = "SELECT  ((Select TRUNC(END_DATE) from ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE CONTRACT_NO ="+ contractNo+" ) - TRUNC(ARIACORE.ARIAVIRTUALTIME("+clientNo+")))  as days from DUAL"
		else if  (Constant.TESTCASENAME.contains("Contract"))
			contractRemainingDaysQuery = "SELECT  ((Select TRUNC(END_DATE) from ARIACORE.ACCT_PLAN_CONTRACTS WHERE CONTRACT_NO ="+ contractNo+" ) - TRUNC(ARIACORE.ARIAVIRTUALTIME("+clientNo+")))  as days from DUAL"

		/*String contractEndDateDB
		 ConnectDB database = new ConnectDB()
		 ResultSet resultSet = database.executePlaQuery(contractRemainingDaysQuery);
		 if (resultSet.next()){
		 contractEndDateDB = resultSet.getString(1);
		 } else {
		 contractEndDateDB = 'No Val ret'
		 }
		 resultSet.close()
		 database.closeConnection()
		 String currentVirtualTime = objClient.getCurrentVirtualTime(clientNo)
		 // Stores the current virtual time in the specified format
		 Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		 Date contractEndDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(contractEndDateDB);
		 Calendar cal1 = Calendar.getInstance();
		 cal1.setTime(currentVTDate);
		 logi.logInfo "Done with cal1: "
		 Calendar cal2 = Calendar.getInstance();
		 cal2.setTime(contractEndDate);
		 logi.logInfo "Done with cal2: "
		 int remainingDays = ((contractEndDate.getTime() - currentVTDate.getTime()) / (1000 * 60 * 60 * 24))
		 logi.logInfo "remainingDays : "+remainingDays
		 logi.logInfo "remainingDays+1 : "+(remainingDays+1)*/
		int days =  Integer.parseInt(db.executeQueryP2(contractRemainingDaysQuery))
		logi.logInfo "days : "+(days)
		return (days+1)
	}
	
	/**
	 * Calculates prorated invoice for the account with contract
	 * @param testCaseId
	 * @return Prorated invoice
	 */
	public String md_ContractProrationAmt(String testcaseid){
		DecimalFormat d = new DecimalFormat("#.##");
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		def accountNo = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def contractNo=Constant.mycontext.expand('${Properties#ContractNo}')
		ConnectDB database = new ConnectDB();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = database.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time : "+ currentVirtualTime
		//int useddays=database.executeQueryP2("SELECT (TRUNC(next_bill_date) -  trunc(plan_date)) AS days FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo).toInteger()
		int useddays=database.executeQueryP2("SELECT TRUNC(LAST_BILL_THRU_DATE+1) - (select CASE WHEN  (trunc(plan_date) >= trunc(last_recur_bill_date) or last_recur_bill_date is null ) THEN trunc(plan_date) ELSE trunc(last_recur_bill_date)  END 	FROM ariacore.acct WHERE acct_no = "+accountNo+")from ariacore.acct WHERE acct_no = "+accountNo+"  AND client_no ="+clientNo).toInteger()

		Map<String, HashMap<String,String>> hash = getPlanServiceAndTieredPricingForProration()
		Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
		resultHash.putAll(hash)
		logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()

		Iterator iterator = resultHash.entrySet().iterator()
		double invoice = 0
		double proration_factor,usage_proration_unit

		while (iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) iterator.next()
			String planNo = pairs.getKey().toString()
			logi.logInfo "Current PlanNo is going to perform:::"+ planNo
			String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
			String planStatusQuery, planStatusDataQuery,contractTypeNoQuery,contractStatusQuery,contractStatus,contractTypeNo
							
			if  (Constant.TESTCASENAME.contains("Contract"))
			{
				contractTypeNoQuery = "SELECT type_no from ARIACORE.ACCT_PLAN_CONTRACTS where contract_no=(select CONTRACT_NO from (SELECT CONTRACT_NO ,row_number() over (order by CREATE_DATE) as sno FROM ARIACORE.ACCT_PLAN_CONTRACTS where status_cd != -3 and acct_no="+accountNo+" and client_no=" +clientNo+" AND plan_no = "+planNo+") where sno=1)"
				contractStatusQuery = "SELECT status_cd from ARIACORE.ACCT_PLAN_CONTRACTS where contract_no=(select CONTRACT_NO from (SELECT CONTRACT_NO ,row_number() over (order by CREATE_DATE) as sno FROM ARIACORE.ACCT_PLAN_CONTRACTS where status_cd != -3 and acct_no="+accountNo+" and client_no=" +clientNo+" AND plan_no = "+planNo+") where sno=1)"
				contractStatus = database.executeQueryP2(contractStatusQuery);
				contractTypeNo = database.executeQueryP2(contractTypeNoQuery);
				if (contractStatus != null)
					contractStatus = contractStatus.toInteger()

				if (contractTypeNo != null)
					contractTypeNo = contractTypeNo.toInteger()
				logi.logInfo "Contract Status "+contractStatus
				logi.logInfo "Contract TypeNo "+contractTypeNo
			}
			int planTypeInd = database.executeQueryP2(planTypeQuery).toInteger()

			logi.logInfo "Plan planTypeInd::: "+planTypeInd+ " plan : " +planNo

			if(planTypeInd==1) {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT_SUPP_PLAN_MAP AC WHERE ACCT_NO   ="+accountNo+" AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date from ariacore.ACCT_SUPP_PLAN_MAP where acct_no = "+ accountNo +"AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo

			}
			else {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+" AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+ " AND CLIENT_NO= " + clientNo
		   }

			int planStatus = database.executeQueryP2(planStatusQuery).toInteger()
			logi.logInfo "Plan Status : "+planStatus+ " plan : " +planNo
			logi.logInfo "Plan planStatusDataQuery : "+planStatusDataQuery+ " plan : " +planNo


			if(planStatus!=0){
				String statusDateDB = database.executeQueryP2(planStatusDataQuery)
				logi.logInfo "Status date from DB: "+statusDateDB
				Date statusDate = simpleFormat.parse(statusDateDB)

				// Stores the current virtual time in the specified format
				Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
				logi.logInfo "planStatus : "+planStatus+ " plan : " +planNo
				logi.logInfo "statusDate.compareTo(currentVTDate) : "+statusDate.compareTo(currentVTDate)+ " plan : " +planNo


				if((planStatus==1 && statusDate.compareTo(currentVTDate)<=0)|| (planTypeInd==0)){

					Map<String,String> valueMap = pairs.getValue()
					Map<String,String> value = new TreeMap<String,String> ()
					value.putAll(valueMap)
					
					String proration_days = database.executeQueryP2("SELECT  ((Select TRUNC(END_DATE+1) from ARIACORE.ACCT_PLAN_CONTRACTS WHERE CONTRACT_NO ="+ contractNo+" ) - TRUNC(ARIACORE.ARIAVIRTUALTIME("+clientNo+")))  as days from DUAL")
					
					logi.logInfo "proration_days "+proration_days
					logi.logInfo "Contract Used Days"+useddays
					
					proration_factor= (proration_days.toDouble()/useddays.toDouble()).round(10)
					logi.logInfo "contract proration_factor"+proration_factor
					Iterator service = value.entrySet().iterator()
					logi.logInfo "Invoice calculation for a plan : "+planNo
					
					String altFee = "0"
					if  (Constant.TESTCASENAME.contains("Contract")){
						String altFeeQuery = "SELECT NVL(sum(amount),0) from ariacore.acct_plan_contract_alt_fees where contract_no=(select CONTRACT_NO from (SELECT CONTRACT_NO ,row_number() over (order by CREATE_DATE) as sno FROM ARIACORE.ACCT_PLAN_CONTRACTS where status_cd != -3 and acct_no="+accountNo+" and client_no="+clientNo+" AND plan_no = "+planNo+") where sno=1)"
						altFee = database.executeQueryP2(altFeeQuery)
					}

					while(service.hasNext()){
						Map.Entry servicePairs = (Map.Entry) service.next()
						String serviceNo = servicePairs.getKey().toString()
						logi.logInfo "Invoice calculation for a service : "+serviceNo
						String unitsquery="select recurring_factor from ariacore.all_acct_recurring_factors where ACCT_NO ="+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no="+planNo
						logi.logInfo "noofunitsQuery::"+unitsquery
						int noofUnits =database.executeQueryP2(unitsquery).toInteger()
						logi.logInfo "no_of_unitsssss: "+noofUnits+ " plan : " +planNo
						
						int seqNo=1;
						String tieredPricing = servicePairs.getValue().toString()
						int tieredPricingInt =tieredPricing.toInteger()
						logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
						double serviceInvoice
						String query
						switch(tieredPricingInt){
							case 1:
								logi.logInfo "Case 1 "
								logi.logInfo "Case 1 cs>>"+contractStatus
								logi.logInfo "Case 1 ct>>"+contractTypeNo
								usage_proration_unit = (noofUnits * proration_factor)
								logi.logInfo "usage_proration_unit::"+usage_proration_unit
								if(!altFee.equals("0"))
								{
									if ((contractStatus == "1") || (contractStatus=="99" && contractTypeNo=="2") || (contractStatus=="0" && contractTypeNo=="3"))
									{
									logi.logInfo "altfee>>"+altFee
									serviceInvoice=(usage_proration_unit * altFee.toDouble()).round(2)
									}
									else
									{
									logi.logInfo "Case1 else part"
									query = "SELECT NRSR.RATE_PER_UNIT * "+usage_proration_unit+" FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
									serviceInvoice = (database.executeQueryP2(query)).toDouble().round(2)
									}
								}
								else
								   {
									query = "SELECT NRSR.RATE_PER_UNIT * "+usage_proration_unit+" FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
									serviceInvoice = (database.executeQueryP2(query)).toDouble().round(2)
								   }
								
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								invoice  = invoice+serviceInvoice
								logi.logInfo "After adding with invoice " + invoice
								seqNo++;
								break;
							case 2 :
								logi.logInfo "Case 2"
								usage_proration_unit = (noofUnits * proration_factor)
								logi.logInfo "usage_proration_unit::"+usage_proration_unit
								query ="select "+usage_proration_unit+" * rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
								serviceInvoice = (database.executeQueryP2(query)).toDouble().round(2)
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								invoice  = invoice+serviceInvoice
								logi.logInfo "After adding with invoice " + invoice
								break;
							case 3 :
								logi.logInfo "Case 3"
								usage_proration_unit = (1*proration_factor)
								logi.logInfo "usage_proration_unit::"+usage_proration_unit
								query = "select "+usage_proration_unit+" * rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
								serviceInvoice = (database.executeQueryP2(query)).toDouble().round(2)
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								invoice  = invoice+serviceInvoice
								logi.logInfo "After adding with invoice " + invoice
								break;
						}//swith case end;
					}//service while end;
				}//plan status with time compare if end;
			}//plan status if end;
		}//plan wise iterator while end;

		//double tinvoice=(invoice*proration_factor.toDouble()).round(2)
		String nonProrationAmt=md_ExpInvoice_ByPlan(testcaseid)
		return d.format(invoice+(nonProrationAmt.toDouble())).toString()
	}

	/**
	 * Gives the number of days passed from account bill date 
	 * @param testCaseId
	 * @return Number of days
	 */
	int getNoOfDaysFromAcctBillStartDay(String clientNo, String accountNo){
		logi.logInfo "Inside getNoOfDaysFromAcctBillStartDay"
		Date acctStartDate = getBillStartDate(clientNo, accountNo)
		logi.logInfo "got answer from getBillStartDate"

		// Get Current Virtual Time
		ClientParamUtils objClient = new ClientParamUtils()
		String currentVirtualTime = objClient.getCurrentVirtualTime(clientNo)
		logi.logInfo "got answer from currentVirtualTime : "+ currentVirtualTime
		// Stores the current virtual time in the specified format
		Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		logi.logInfo "Done currentVTDate: "
		//Date acctBillStartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(acctStartDate);

		Calendar cal1 = new GregorianCalendar()
		cal1.setTime(currentVTDate);
		logi.logInfo "Done cal1: "
		Calendar cal2 = new GregorianCalendar()
		cal2.setTime(acctStartDate);
		logi.logInfo "Done cal2: "

		// Find days between them to calculate the Days from AccountCreation
		int daysFromAcctBillStartDay = ((currentVTDate.getTime() - acctStartDate.getTime()) / (1000 * 60 * 60 * 24))
		logi.logInfo "daysFromAcctBillStartDay : "+daysFromAcctBillStartDay
		int createdMonth = cal2.get(Calendar.MONTH)
		logi.logInfo "createdMonth : "+createdMonth
		int createdYear = cal2.get(Calendar.YEAR)
		logi.logInfo "createdYear : "+createdYear
		if ((createdYear%4)==0)
			return (daysFromAcctBillStartDay+4)
		else
			return (daysFromAcctBillStartDay+3)
	}

	String md_getamt_after_tax_inclusive(String invno,String amt)
	{
		String org_amt=amt
		List<String> tax_rate_list = []
		String tax_rate_qry="Select distinct round(tax_rate,2) from ARIACORE.GL_TAX_DETAIL where invoice_no ="+invno
		ResultSet rs_tax_rate_qry = db.executePlaQuery(tax_rate_qry)
		while(rs_tax_rate_qry.next())
		{
			double tax_rate=rs_tax_rate_qry.getDouble(1)
			tax_rate_list.add((tax_rate+1).toString())
		}

		logi.logInfo("tax rate list : "+tax_rate_list.toString())
		for (tax in tax_rate_list)
		{
			String newbase_amt=((org_amt.toDouble()/tax.toDouble()).trunc(2)).toString()
			logi.logInfo "new base amount "+newbase_amt
			String taxation_amt=(org_amt.toDouble()-newbase_amt.toDouble()).round(2).toString()
			logi.logInfo "taxation amount "+taxation_amt
			org_amt=(org_amt.toDouble()-taxation_amt.toDouble()).trunc(2).toString()
			logi.logInfo "new org base amount "+org_amt
		}
		return org_amt
	}

	String md_TAX_EXEMPTION_LEVEL_TOTAL_AMT(String tcid)
	{
		String taxExemLevel = getValueFromRequest("create_acct_complete","//tax_exemption_level")
		String[] invNumber = getAllValuesFromRequest(tcid,"//invoice_no")
		String[] invLineNumber = getAllValuesFromRequest(tcid,"//invoice_line_no")
		String[] invAmount = getAllValuesFromRequest(tcid,"//invoice_line_reversing_amount")

		ConnectDB db = new ConnectDB()


		//Getting Taxes
		String[] stateTaxes = getAllValuesFromRequest(tcid,"//invoice_line_no")
		String[] federalTaxes = getAllValuesFromRequest(tcid,"//invoice_line_no")
		String calculatedTax = ""
		double calcTax
		ResultSet resultSet
		String taxInd
		for (int lineIndex = 0; lineIndex<invLineNumber.size(); lineIndex++)
		{
			String service_num = getValueFromResponse("create_acct_complete","//*:new_acct_invoice_line_items_row["+ (lineIndex + 1).toString() +"]/*:invoice_service_no["+(lineIndex + 1).toString()+"]")
			String isserviceTaxable = "select TAXABLE_IND from ariacore.all_service where service_no = " + service_num
			resultSet = db.executePlaQuery(isserviceTaxable)

			taxInd= ""
			if (resultSet.next())
			{
				taxInd = resultSet.getString(1)
			}
			resultSet.close()

			federalTaxes[lineIndex] = "0.00"
			stateTaxes[lineIndex] = "0.00"


			if (taxInd == "1")
			{
				String qry = "select distinct(tax_rate) from ariacore.gl_tax_detail where invoice_no = " + invNumber[lineIndex] + " and seq_num in (select seq_num from ariacore.gl_detail where invoice_no = " + invNumber[lineIndex] + " and service_no = 401)"
				resultSet = db.executePlaQuery(qry)
				if (resultSet.next())
					stateTaxes[lineIndex] = resultSet.getString(1)


				resultSet.close()

				qry = "select distinct(tax_rate) from ariacore.gl_tax_detail where invoice_no = " + invNumber[lineIndex] + " and seq_num in (select seq_num from ariacore.gl_detail where invoice_no = " + invNumber[lineIndex] + " and service_no = 400)"
				resultSet = db.executePlaQuery(qry);

				if (resultSet.next())
					federalTaxes[lineIndex] = resultSet.getString(1)

				resultSet.close()
			}
		}

		if (taxExemLevel == "0") // Full Tax
		{
			calcTax = 0.00
			for (int lineIndex = 0; lineIndex<invLineNumber.size(); lineIndex++)
			{
				calcTax = calcTax + invAmount[lineIndex].toDouble() * stateTaxes[lineIndex].toDouble()  + invAmount[lineIndex].toDouble() * federalTaxes[lineIndex].toDouble()
			}
		}

		else if (taxExemLevel == "1") // State Tax Exemption
		{
			calcTax = 0

			for (int lineIndex = 0; lineIndex<invLineNumber.size(); lineIndex++)
			{
				calcTax = calcTax + invAmount[lineIndex].toDouble() * federalTaxes[lineIndex].toDouble()
			}

		}
		else if (taxExemLevel == "2") // Federal Tax Exemption
		{
			calcTax = 0
			for (int lineIndex = 0; lineIndex<invLineNumber.size(); lineIndex++)
			{
				calcTax = calcTax + invAmount[lineIndex].toDouble() * stateTaxes[lineIndex].toDouble()
			}
		}

		else if (taxExemLevel == "3") // No Tax
		{
			calcTax = 0.0
		}
		String calcfinalTax = calcTax.toDouble().round(2).toString()
		if (calcfinalTax.split("\\.")[1].length() == 1)
			calcfinalTax = calcfinalTax.concat("0")

		return df.format(calcfinalTax.toDouble()).toString()
	}

	String md_Calculate_Total_Invoice_State_Taxes(String tcid)
	{
		String taxExemLevel = getValueFromRequest("create_acct_complete","//tax_exemption_level")

		String invNumber = getValueFromResponse("create_acct_complete","//invoice_no")

		String[] invLineServiceNumber = getAllValuesFromResponse(tcid,"//*:invoice_service_no")
		String[] invlineAmount = getAllValuesFromResponse(tcid,"//*:invoice_line_amount")
		String[] billableServices = getAllValuesFromResponse(tcid,"//*:invoice_plan_no")
		ConnectDB db = new ConnectDB()


		//Getting Taxes
		String[] stateTaxes = getAllValuesFromResponse(tcid,"//*:invoice_service_no")
		String calculatedTax = ""
		double calcTax
		ResultSet resultSet
		String taxInd
		for (int lineIndex = 0; lineIndex<billableServices.size(); lineIndex++)
		{
			String isserviceTaxable = "select TAXABLE_IND from ariacore.all_service where service_no = " + invLineServiceNumber[lineIndex]
			resultSet = db.executePlaQuery(isserviceTaxable)

			taxInd= ""
			if (resultSet.next())
			{
				taxInd = resultSet.getString(1)
			}
			resultSet.close()


			stateTaxes[lineIndex] = "0.00"


			if (taxInd == "1")
			{
				String qry = "select distinct(tax_rate) from ariacore.gl_tax_detail where invoice_no = " + invNumber + " and seq_num in (select seq_num from ariacore.gl_detail where invoice_no = " + invNumber + " and service_no = 401)"
				resultSet = db.executePlaQuery(qry)
				if (resultSet.next())
					stateTaxes[lineIndex] = resultSet.getString(1)

				resultSet.close()

			}
		}


		calcTax = 0.0
		for (int lineIndex = 0; lineIndex<billableServices.size(); lineIndex++)
		{
			calcTax = calcTax + invlineAmount[lineIndex].toDouble() * stateTaxes[lineIndex].toDouble()
		}

		if (taxExemLevel == "1" || taxExemLevel == "3") calcTax = 0.0
		String calcfinalTax = calcTax.toDouble().round(2).toString()
		if (calcfinalTax.split("\\.")[1].length() == 1)
			calcfinalTax = calcfinalTax.concat("0")
		def tfinaltax = ((calcfinalTax.toDouble()*100).toInteger()/100).toString()
		return tfinaltax.toString()
	}

	String md_Calculate_Total_Invoice_Federal_Taxes(String tcid)
	{
		String taxExemLevel = getValueFromRequest("create_acct_complete","//tax_exemption_level")
		String invNumber = getValueFromResponse("create_acct_complete","//invoice_no")
		String[] invLineServiceNumber = getAllValuesFromResponse(tcid,"//*:invoice_service_no")
		String[] invlineAmount = getAllValuesFromResponse(tcid,"//*:invoice_line_amount")
		String[] billableServices = getAllValuesFromResponse(tcid,"//*:invoice_plan_no")
		ConnectDB db = new ConnectDB()


		//Getting Taxes
		String[] federalTaxes = getAllValuesFromResponse(tcid,"//*:invoice_service_no")
		String calculatedTax = ""
		double calcTax
		ResultSet resultSet
		String taxInd
		for (int lineIndex = 0; lineIndex<billableServices.size(); lineIndex++)
		{
			String isserviceTaxable = "select TAXABLE_IND from ariacore.all_service where service_no = " + invLineServiceNumber[lineIndex]
			resultSet = db.executePlaQuery(isserviceTaxable)

			taxInd= ""
			if (resultSet.next())
			{
				taxInd = resultSet.getString(1)
			}
			resultSet.close()

			federalTaxes[lineIndex] = "0.00"


			if (taxInd == "1")
			{

				String qry = "select distinct(tax_rate) from ariacore.gl_tax_detail where invoice_no = " + invNumber + " and seq_num in (select seq_num from ariacore.gl_detail where invoice_no = " + invNumber + " and service_no = 400)"
				resultSet = db.executePlaQuery(qry);

				if (resultSet.next())
					federalTaxes[lineIndex] = resultSet.getString(1)

				resultSet.close()
			}
		}

		calcTax = 0

		for (int lineIndex = 0; lineIndex<billableServices.size(); lineIndex++)
		{
			calcTax = calcTax + invlineAmount[lineIndex].toDouble() * federalTaxes[lineIndex].toDouble()
		}

		if (taxExemLevel == "2" || taxExemLevel == "3") calcTax = 0.0
		String calcfinalTax = calcTax.toDouble().round(2).toString()
		if (calcfinalTax.split("\\.")[1].length() == 1)
			calcfinalTax = calcfinalTax.concat("0")
		def tfinaltax = ((calcfinalTax.toDouble()*100).toInteger()/100).toString()
		return tfinaltax.toString()
	}

	def getAllValuesFromResponse(String tcid,String xpath) {

		String[] nodeValue
		//String tcid= Constant.TESTCASEID
		String suitename=Constant.TESTSUITENAME
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		//logi.logInfo(testCaseCmb[0].toString())
		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().equals(tcid)) {
				com.eviware.soapui.support.XmlHolder holder = xmlValues[j]
				if(holder.containsKey(xpath) && holder.containsValue(xpath)) {
					nodeValue = holder.getNodeValues(xpath)
				}
				else
				{
					logi.logInfo "The node "+xpath+"does not exist in  Request for "+tcid
				}
			}
		}
		return nodeValue
	}
	String md_RECORD_USAGE_CHILD_REC_NO(String tcid)
	{
		String rec_no
		def acc_no=getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String rec_qry = "SELECT rec_no from ariacore.usage where acct_no="+acc_no
		ResultSet rs_rec_qry= db.executePlaQuery(rec_qry)
		while(rs_rec_qry.next())
		{
			rec_no=rs_rec_qry.getString(1)
		}

		return rec_no
	}
	public String getCreditAmount(String acct_no)
	{
		logi.logInfo("Calling getCreditAmount for account "+acct_no)
		String allcreditamt
		String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		String credit_amt_query="select NVL(sum(amount),0) from ariacore.all_credits where acct_no="+acct_no+" and credit_type in ('S','C') and orig_coupon_cd is null and client_no="+client_number
		ResultSet rs = db.executePlaQuery(credit_amt_query);
		while (rs.next()){
			allcreditamt=rs.getString(1)

		}
		rs.close()
		logi.logInfo("Final credit amt for account " +acct_no+" "+allcreditamt)
		return allcreditamt

	}
	public String md_IRTA_EXPECTED_TAX_AMT_API(String tcid)
	{

		logi.logInfo("Calling md_IRTA_EXPECTED_TAX_AMT_API")
		String[] applied_reversal_amt = getAllValuesFromRequest(tcid,"//invoice_line_reversing_amount")
		String [] invoices=getAllValuesFromRequest(tcid,"//invoice_no")
		String invoicenos=invoices.toString().replace("[", "").replace("]", "")
		logi.logInfo("invoicenos"+invoicenos)
		double total
		for( i in applied_reversal_amt)
		{
			total=total+i.toDouble()
		}
		logi.logInfo("total amount eligible for tax"+total.toString())
		ConnectDB db = new ConnectDB()
		String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		String taxratequery= "Select sum(distinct tax_rate) from ARIACORE.gl_tax_detail where invoice_no in ( "+invoicenos+") and client_no="+client_number
		double totaltaxrate
		ResultSet rs = db.executePlaQuery(taxratequery);
		while (rs.next()){
			totaltaxrate=rs.getDouble(1)

		}
		rs.close()
		String expectedAmt=(total*totaltaxrate).toString()
		if (expectedAmt.split("\\.")[1].length() == 1)
			expectedAmt = expectedAmt.concat("0")
		return df.format(expectedAmt.toDouble()).toString()


	}
	public String get_discount_through_creditcard() {

		ConnectDB dbase = new ConnectDB()
		ResultSet resSet
		String totalDiscount = "0"
		String finalDiscAmt = "0"
		String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		String account_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String invoice_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String payment_Method =   getValueFromRequest("create_acct_complete","//pay_method")
		String currencyCode = getValueFromRequest("create_acct_complete","//currency_cd")
		String invoiceRecAmt = getValueFromResponse("create_acct_complete","//*:new_acct_invoice_line_items_row[2]/*:invoice_line_amount[1]")

		String discount_Amount_Qry = "select discount_percentage from ariacore.client_payment_method where client_no = "+client_number+" and method_id ="+payment_Method
		String discount_Rule_No_Qry = "select discount_rule_no from ariacore.client_payment_method where client_no = "+client_number+" and method_id ="+payment_Method
		String invoice_qry = "SELECT DEBIT FROM ARIACORE.GL WHERE ACCT_NO="+account_No+" AND INVOICE_NO="+invoice_No
		String flatDiscountQry = "select NVL(Sum(AMOUNT),0) as flat_discount from ariacore.CLI_PAY_MTH_FLAT_DISC_CURR_MAP where CLIENT_NO = "+client_number+" and method_id = "+payment_Method+" and currency_cd ='"+currencyCode+"'"

		def flatDiscountAmount
		def totalInvoiceAmt
		def percentDiscount
		String disc_Rule_No
		boolean discAplicable = false
		logi.logInfo ("payment_Method" +payment_Method)
		if(payment_Method != "NoVal")
		{



			resSet = dbase.executePlaQuery(invoice_qry)
			if (resSet.next()) {
				totalInvoiceAmt = resSet.getObject(1)
			}
			logi.logInfo("Total invoice amount is :: "+totalInvoiceAmt)

			resSet = dbase.executePlaQuery(discount_Amount_Qry)
			if (resSet.next()) {
				percentDiscount = resSet.getObject(1)
				discAplicable = true
			}
			//percentDiscount = dbase.executeQueryP2(discount_Amount_Qry)
			logi.logInfo("Discount percentage for credit card payment :: "+percentDiscount)

			resSet = dbase.executePlaQuery(discount_Rule_No_Qry)
			if (resSet.next()) {
				disc_Rule_No = resSet.getString(1)
			}
			//disc_Rule_No = dbase.executeQueryP2(discount_Rule_No_Qry)
			logi.logInfo("Rule no :: "+disc_Rule_No)

			resSet = dbase.executePlaQuery(flatDiscountQry)
			if (resSet.next()) {
				flatDiscountAmount = resSet.getObject(1)
			}
			logi.logInfo("flatDiscountAmount :: "+flatDiscountAmount)


			if(discAplicable == true && disc_Rule_No.toInteger() == 2){
				totalDiscount = (totalInvoiceAmt * percentDiscount) / 100
			} else if(discAplicable == true && disc_Rule_No.toInteger() == 3){
				logi.logInfo("invoiceRecAmt 2 :: "+invoiceRecAmt)
				totalDiscount = (invoiceRecAmt.toInteger() * percentDiscount) / 100
			}else if(disc_Rule_No.toInteger() == 1){
				totalDiscount = flatDiscountAmount
			}
		}
		return totalDiscount
	}

	String md_IRTA_TOTAL_TAX_AMT_CR0_DB(String tcid)
	{
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def invoice_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def tax_rate_list = []
		def cal_tax
		def total_tax=0

		String tax_rate_qry="Select distinct tax_rate from ARIACORE.GL_TAX_DETAIL where invoice_no ="+invoice_no
		ResultSet rs_tax_rate_qry = db.executePlaQuery(tax_rate_qry)
		while(rs_tax_rate_qry.next())
		{
			def tax_rate=rs_tax_rate_qry.getObject(1)
			tax_rate_list.add(tax_rate)
		}

		for(int taxItems=0;j<tax_rate_list.size();taxItems++)
		{
			String rev_amt_qry = "Select distinct rd.credit from ARIACORE.refund_detail rd where rd.gl_seq_num IN (Select distinct gtd.taxed_seq_num from ariacore.gl_detail gld JOIN ARIACORE.GL_TAX_DETAIL gtd ON gtd.invoice_no = gld.invoice_no where gld.invoice_no="+invoice_no+"and gld.service_no NOT IN(400,401)) and rd.invoice_no="+invoice_no
			ResultSet rs_rev_amt_qry = db.executePlaQuery(rev_amt_qry)
			while(rs_rev_amt_qry.next())
			{
				def rev_amt=rs_rev_amt_qry.getObject(1)
				cal_tax = rev_amt * tax_rate_list.get(taxItems)
				//(rev_amt * (tax_rate_list.get(j)*100).toInteger())/100
				total_tax=total_tax + cal_tax
			}

		}
		return total_tax.toString()
	}
	def md_GET_INVOICE_STATE_TAX(String testCaseId){
		String seqNum
		seqNum = getSequenceNumberForTaxItems("401")
		return getInvoiceTaxValueFromResponseAsHash(seqNum)
	}
	def md_GET_INVOICE_FEDERAL_TAX(String testCaseId){
		String seqNum
		seqNum = getSequenceNumberForTaxItems("400")
		return getInvoiceTaxValueFromResponseAsHash(seqNum)
	}
	def md_GET_INVOICE_COUNTRY_TAX(String testCaseId){
		String seqNum
		seqNum = getSequenceNumberForTaxItems("402")
		return getInvoiceTaxValueFromResponseAsHash(seqNum)
	}
	def md_GET_INVOICE_DISTRICT_TAX(String testCaseId){
		String seqNum
		seqNum = getSequenceNumberForTaxItems("405")
		return getInvoiceTaxValueFromResponseAsHash(seqNum)
	}
	
	def md_GET_INVOICE_CITY_TAX(String testCaseId){
		String seqNum
		seqNum = getSequenceNumberForTaxItems("403")
		return getInvoiceTaxValueFromResponseAsHash(seqNum)
	}

	HashMap<String,String> getInvoiceTaxValueFromResponseAsHash(String sequenceNo) {
		def node
		def cnodes
		def cnodeIndex
		HashMap responseData = new HashMap<String,String>()
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		logi.logInfo(testCaseCmb[0].toString())
		com.eviware.soapui.support.XmlHolder outputholder
		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains("create_acct_complete") && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				responseData.put("invoice_line_no",outputholder.getNodeValue("//*["+sequenceNo+"]/*[local-name()='invoice_line_no']"))
				responseData.put("invoice_service_no",outputholder.getNodeValue("//*["+sequenceNo+"]/*[local-name()='invoice_service_no']"))
				responseData.put("invoice_service_name",outputholder.getNodeValue("//*["+sequenceNo+"]/*[local-name()='invoice_service_name']"))
				responseData.put("invoice_line_amount",outputholder.getNodeValue("//*["+sequenceNo+"]/*[local-name()='invoice_line_amount']"))
				responseData.put("invoice_line_description",outputholder.getNodeValue("//*["+sequenceNo+"]/*[local-name()='invoice_line_description']"))
			}
		}
		logi.logInfo "The Invoice tax line item values for sequence - '"+sequenceNo+"' are : "+ responseData
		return responseData
	}

	def md_get_invoice_federalTax_db(String testCaseId) {
		String seqNum
		seqNum = getSequenceNumberForTaxItems("400")
		return md_GET_TAX_INVOICE_LINE_ITEMS(seqNum)
	}
	def md_get_invoice_stateTax_db(String testCaseId) {
		String seqNum
		seqNum = getSequenceNumberForTaxItems("401")
		return md_GET_TAX_INVOICE_LINE_ITEMS(seqNum)
	}
	def md_get_invoice_countryTax_db(String testCaseId) {
		String seqNum
		seqNum = getSequenceNumberForTaxItems("402")
		return md_GET_TAX_INVOICE_LINE_ITEMS(seqNum)
	}
	def md_get_invoice_districtTax_db(String testCaseId) {
		String seqNum
		seqNum = getSequenceNumberForTaxItems("405")
		return md_GET_TAX_INVOICE_LINE_ITEMS(seqNum)
	}
	
	def md_get_invoice_cityTax_db(String testCaseId) {
		String seqNum
		seqNum = getSequenceNumberForTaxItems("403")
		return md_GET_TAX_INVOICE_LINE_ITEMS(seqNum)
	}

	HashMap<String,String> md_GET_TAX_INVOICE_LINE_ITEMS(String seqNum) {

		HashMap<String, String> taxLineItemMap
		ConnectDB databasee = new ConnectDB()
		String taxableServiceNo

		logi.logInfo("Account number is : "+Constant.mycontext.expand('${Properties#AccountNo}').toString())
		String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)

		String taxSequenceNo = "select service_no from ariacore.gl_detail where invoice_no = "+invoiceNo+" and seq_num ="+seqNum
		taxableServiceNo = databasee.executeQueryP2(taxSequenceNo)

		taxLineItemMap = get_tax_invoice_line_Items_DB(Constant.mycontext.expand('${Properties#AccountNo}').toString(),invoiceNo,taxableServiceNo)
		logi.logInfo("The taxable hash map got from the method is : "+taxLineItemMap)
		return taxLineItemMap
	}

	public HashMap<String,String> get_tax_invoice_line_Items_DB(String account_Number,String invoice_No,String taxServiceNo){

		ConnectDB taxConnection = new ConnectDB()
		DecimalFormat df=new DecimalFormat("#.##")
		String sequenceNum
		String serviceNo

		String client_Number = Constant.mycontext.expand('${Properties#Client_No}')
		String total_Tax_Amount_Qry = "SELECT debit FROM ariacore.gl_detail WHERE client_no = "+client_Number+" AND invoice_no  = "+invoice_No+" AND service_no IN ("+taxServiceNo+")"
		String tax_Sequence_Query = "select seq_num from ariacore.gl_detail where invoice_no = "+invoice_No+" and service_no in ("+taxServiceNo+")"
		String tax_ServiceNo_Query = "select service_no from ariacore.gl_detail where invoice_no = "+invoice_No+" and service_no in ("+taxServiceNo+")"

		HashMap<String, String> invoice_Tax_Values = new HashMap<String,String>()
		List keyValues = [
			"invoice_line_no",
			"invoice_service_no",
			"invoice_service_name",
			"invoice_line_amount",
			"invoice_line_description"
		];

		String total_Invoice_Tax_Amt
		ResultSet resultSet
		int countTax=1

		// Invoice line number
		sequenceNum = taxConnection.executeQueryP2(tax_Sequence_Query)
		invoice_Tax_Values.put(keyValues.get(0),sequenceNum)
		logi.logInfo("Tax invoice SequenceNum : "+sequenceNum)

		serviceNo = taxConnection.executeQueryP2(tax_ServiceNo_Query)
		invoice_Tax_Values.put(keyValues.get(1),serviceNo)
		logi.logInfo("Tax invoice serviceNo : "+serviceNo)

		// Description
		String taxDetail_Qry = "Select comments from ariacore.gl_detail where client_no="+client_Number+" and invoice_no="+invoice_No+" and seq_num="+sequenceNum
		String description
		description = taxConnection.executeQueryP2(taxDetail_Qry)
		invoice_Tax_Values.put(keyValues.get(2),description)
		logi.logInfo("Tax description : "+description)

		// Total tax amount
		
		total_Invoice_Tax_Amt = taxConnection.executeQueryP2(total_Tax_Amount_Qry)
		invoice_Tax_Values.put(keyValues.get(3),df.format(total_Invoice_Tax_Amt.toDouble()).toString())
		logi.logInfo(" Tax total_Invoice_Tax_Amt : "+total_Invoice_Tax_Amt)

		invoice_Tax_Values.put(keyValues.get(4),description)

		return invoice_Tax_Values
	}
	public HashMap md_verify_coupon_line_items(String testCaseId){
		HashMap<String,String> coupon_Hash = new HashMap<String,String>()
		String couponDtlQry = ""
		return ""
	}

	def md_CREATE_ORDER_INCLUSIVE_TOTAL_CHARGES_DB(String tcid)
	{
		logi.logInfo("md_CREATE_ORDER_INCLUSIVE_TOTAL_CHARGES_DB")
		String apiname=tcid.split("-")[2]
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_ACCT_NO)
		String inv_no=getValueFromResponse(apiname,ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)
		String units = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNITS)
		logi.logInfo("units" + units.toString())
		def order_name=getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_ORDER_NAME)
		String rate
		def seqno
		def sum=0
		def tax_rate
		def tax_charges
		def org_amt

		seqs=new HashMap()


		HashMap row = new HashMap();
		String rate_qry= "Select INVIP.price from ariacore.inventory_item_prices INVIP  JOIN ariacore.INVENTORY_ITEMS INVI ON invip.item_no=invi.item_no and invip.client_no=invi.client_no where INVI.client_sku='"+order_name+"' and INVI.client_no="+client_no+" and invip.currency_cd=(SELECT CURRENCY_CD FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+ " AND CLIENT_NO="+client_no+")"
		logi.logInfo("rate_qry :"+rate_qry)

		ResultSet resultSet = db.executePlaQuery(rate_qry);

		while(resultSet.next())
		{
			rate=resultSet.getObject(1).toString()
		}
		logi.logInfo("rate" + rate.toString())

		if(rate.toString().contains("."))
		{
			org_amt= (units.toInteger()) * rate.toDouble()


		}
		else
		{
			org_amt= (units.toInteger()) * rate.toInteger()

		}
		order_amt=org_amt


		logi.logInfo("org amt"+org_amt)
		inc_charges= md_getamt_after_tax_inclusive(inv_no,org_amt.toString())

		row.put("TOTAL_CHARGES_BEFORE_TAX",inc_charges)


		String seq_qry= "Select seq_num from ARIACORE.gl_tax_detail where invoice_no="+inv_no+ " order by seq_num asc"
		ResultSet rs_seq_qry = db.executePlaQuery(seq_qry);
		int k=1;

		while (rs_seq_qry.next())
		{

			seqno = rs_seq_qry.getObject(1)
			seqs.put("tax"+k, seqno)
			k++

		}
		logi.logInfo("seqs array"+seqs)

		for(int i=1;i<=seqs.size();i++)
		{
			def s_no=seqs.get("tax"+i)

			String tax_qry = "Select tax_rate from ARIACORE.gl_tax_detail where invoice_no="+inv_no+" and seq_num="+s_no

			ResultSet rs_tax = db.executePlaQuery(tax_qry);

			while(rs_tax.next())
			{
				tax_rate = rs_tax.getDouble(1)
				logi.logInfo("now tax rate"+tax_rate)
				def final_base=(org_amt.toDouble()/(tax_rate+1)).trunc(2)
				logi.logInfo("final_base"+final_base)
				tax_charges=(org_amt.toDouble()-final_base.toDouble()).round(2)
				logi.logInfo("tax_charges"+tax_charges)
				org_amt=final_base
				logi.logInfo("new org amt"+org_amt)
				//tax_charges= (charges * (tax_rate*100).toInteger())/100
				//logi.logInfo("tax charges"+ tax_charges)
			}

			sum= (sum + tax_charges).round(2)
		}

		logi.logInfo("total tax"+ sum)
		row.put("TOTAL_TAX_CHARGES",sum)

		def total = (inc_charges.toDouble() + sum).toInteger()

		logi.logInfo("all total "+ total)
		row.put("TOTAL_CHARGES_AFTER_TAX",total)

		return row;


	}

	def md_CREATE_ORDER_INCLUSIVE_INVOICE_AMT_DB(String tcid)
	{
		logi.logInfo("md_CREATE_ORDER_INCLUSIVE_INVOICE_AMT_DB")
		def line_no
		String apiname=tcid.split("-")[2]

		def inv_no = getValueFromResponse(apiname,ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)

		String lineno_qry = "Select seq_num from ariacore.gl_detail where invoice_no="+inv_no+ " and service_no NOT IN (400,401)"
		ResultSet resultSet = db.executePlaQuery(lineno_qry);
		HashMap hm_row = new HashMap();

		while(resultSet.next())
		{
			line_no = resultSet.getObject(1)
		}

		hm_row.put("LINE_NO", line_no)

		String qry = "Select service_no,comments as description from ariacore.gl_detail  where invoice_no="+inv_no+" and seq_num="+line_no
		ResultSet rs_qry = db.executePlaQuery(qry);
		ResultSetMetaData md = rs_qry.getMetaData();
		int columns = md.getColumnCount();


		while (rs_qry.next()){
			for(int i=1; i<=columns; i++){
				hm_row.put(md.getColumnName(i),rs_qry.getObject(i));
			}

			logi.logInfo("charges in invoice mat db method" +charges )
			logi.logInfo("seq hash in invoice mat db method" +seqs)
			hm_row.put("LINE_AMOUNT",inc_charges)
		}

		return hm_row
	}

	def md_CREATE_ORDER_INCLUSIVE_TAX_DB(String tcid,String tax_key)
	{
		logi.logInfo("md_CREATE_ORDER_INCLUSIVE_TAX_DB")
		String seq_no = seqs.get("tax"+tax_key)
		logi.logInfo("tax seqnum from hm: "+seq_no)
		def tax_charges
		def amt
		String rate
		HashMap hm_dbtax = new HashMap();
		String apiname=tcid.split("-")[2]
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		String inv_no=getValueFromResponse(apiname,ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)

		String t_qry = "Select seq_num as line_no,service_no,comments as description from ariacore.gl_detail where invoice_no ="+inv_no+" and seq_num="+seq_no
		ResultSet rs_tax = db.executePlaQuery(t_qry);
		ResultSetMetaData met = rs_tax.getMetaData();
		int columns = met.getColumnCount();
		while (rs_tax.next())
		{
			for(int i=1; i<=columns; i++)
			{
				hm_dbtax.put(met.getColumnName(i),rs_tax.getObject(i));
			}
		}

		String tr_qry = "Select tax_rate from ariacore.gl_tax_detail where invoice_no ="+inv_no+" and seq_num="+seq_no
		ResultSet rs_tr1 = db.executePlaQuery(tr_qry);
		while(rs_tr1.next())
		{
			def tax_rate = rs_tr1.getObject(1)
			logi.logInfo("inc tax rate"+tax_rate)
			logi.logInfo("order amt"+order_amt)
			def base=(order_amt.toDouble()/(tax_rate+1)).trunc(2)
			tax_charges=(order_amt.toDouble()-base.toDouble()).round(2)
			logi.logInfo("calculated tax amt:"+tax_charges)
			hm_dbtax.put("LINE_AMOUNT",tax_charges)
			order_amt=base
			logi.logInfo("now the amt is:"+order_amt)

		}


		return  hm_dbtax;
	}
	public String md_GET_COUPON_APPLIED_REMAINING_LINE_AMOUNT1(String testCaseId)
	{
		return md_GET_COUPON_APPLIED_REMAINING_LINE_AMOUNT(testCaseId,"1")
	}

	public String md_GET_COUPON_APPLIED_REMAINING_LINE_AMOUNT2(String testCaseId)
	{
		return md_GET_COUPON_APPLIED_REMAINING_LINE_AMOUNT(testCaseId,"2")
	}
	public String md_GET_COUPON_APPLIED_REMAINING_LINE_AMOUNT(String testCaseId,String lno)
	{
		logi.logInfo("Calling md_GET_COUPON_APPLIED_REMAINING_LINE_AMOUNT ")
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		String account_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String invoice_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)

		String service_no=getValueFromResponse("create_acct_complete","//*/*/ns1:*["+lno+"]/ns1:invoice_service_no[1]")
		String plan_no=getValueFromResponse("create_acct_complete","//*/*/ns1:*["+lno+"]/ns1:invoice_plan_no[1]")
		String master_plan_units=getValueFromRequest("create_acct_complete","//master_plan_units")

		String proration_factor_query="select proration_factor from ariacore.gl_detail where invoice_no = "+invoice_No+" and seq_num=2"
		String seq_num
		def coupon_amt
		def actual_line_amt
		def remaining_amt

		String coupon_amt_query="SELECT SEQ_NUM as SEQ_NO,SUM(DISTILLED_AMOUNT) as COUPON_AMT FROM (SELECT ELIGIBLE_SERVICE_NO,DISTILLED_AMOUNT FROM ariacore.all_recur_credit WHERE acct_no =    (SELECT acct_no FROM ariacore.gl WHERE invoice_no="+invoice_No+") UNION SELECT CDR.KEY_2_VAL AS ELIGIBLE_SERVICE_NO,  CASE WHEN CDRS.INLINE_OFFSET_IND='O' THEN 0      ELSE AIDD.ACTUAL_DISCOUNT_AMT END AS DISTILLED_AMOUNT FROM ARIACORE.CLIENT_DR_COMPONENTS CDR JOIN ariacore.all_invoice_discount_details AIDD ON AIDD.rule_no=CDR.rule_no JOIN ariacore.CLIENT_DISCOUNT_RULES CDRS ON CDRS.rule_no =AIDD.rule_no WHERE AIDD.invoice_no ="+invoice_No+" )a join ariacore.gl_detail gd on gd.service_no=a.ELIGIBLE_SERVICE_NO and gd.invoice_no="+invoice_No+" AND gd.ORIG_COUPON_CD IS NULL GROUP BY SEQ_NUM"
		ConnectDB db = new ConnectDB()
		ResultSet rs = db.executePlaQuery(coupon_amt_query);
		while (rs.next()){
			seq_num=rs.getString("SEQ_NO")
			coupon_amt=rs.getObject("COUPON_AMT")
			logi.logInfo "Coupon applied sequence number is "+seq_num
			logi.logInfo "Coupon amount applied for the above sequence number is "+coupon_amt
		}
		rs.close();
		String units= db.executeQueryP2(proration_factor_query)
		//String actual_line_amt_query="SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_No+" AND SEQ_NUM="+seq_num
		String actual_line_amt_query="SELECT RATE_PER_UNIT *"+ master_plan_units +"* "+units+" FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SERVICE_NO="+service_no+" AND SCHEDULE_NO=(SELECT RATE_SCHEDULE_NO FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = "+account_No+" AND PLAN_NO="+plan_no+" AND SERVICE_NO="+service_no+" AND RATE_SEQ_NO=1 AND CLIENT_NO="+client_no+") AND RATE_SEQ_NO = 1 AND CLIENT_NO="+client_no
		actual_line_amt=db.executeQueryP2(actual_line_amt_query)
		logi.logInfo "Actual line item amount before coupon is "+actual_line_amt

		remaining_amt = (actual_line_amt.toDouble() - coupon_amt.toDouble())
		logi.logInfo("Remaining line amount after coupon is :: "+remaining_amt)

		remaining_amt=remaining_amt.toDouble().round(2)
		logi.logInfo("Remaining line amount after coupon is :: "+remaining_amt)
		return df.format(remaining_amt.toString().toDouble()).toString()

	}
	String md_setPaymentCreditAmountToZeroAfterCAC(String testCaseId){

		InputMethods iMethod = new InputMethods()
		ReadData rData = new ReadData()
		boolean sflag = false

		List clientIds = rData.getClientListP2()
		logi.logInfo("The list of client ids are :: "+clientIds)

		for (int cliList=0; cliList<clientIds.size(); cliList++){
			logi.logInfo ("Calling md_UPDATE_PAYMENT_DISCOUNT for client "+clientIds.get(cliList))
			//md_UPDATE_PAYMENT_DISCOUNT_param_+1,1,5,usd,3285073
			if(iMethod.update_payment_discount_flat_and_commit("1", "1", "0", "usd",clientIds.get(cliList)) == "No Val"){
				logi.logInfo "All clients are reset with zero pay credit discount"
				sflag = true
			}
		}
		return sflag.toString().toUpperCase()
	}
	String md_ISSUE_REFUND_TO_ACCT_TAX_DB(String tcid)
	{
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def invoice_No = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)

		def cal_tax
		def total_tax=0
		def tax_rate_list = []

		String tax_rate_qry="Select distinct tax_rate from ARIACORE.GL_TAX_DETAIL where invoice_no IN ("+invoice_No+")"
		ResultSet rs_tax_rate_qry = db.executePlaQuery(tax_rate_qry)
		while(rs_tax_rate_qry.next())
		{
			def tax_rate=rs_tax_rate_qry.getObject(1)
			tax_rate_list.add(tax_rate)
		}

		for(int j=0;j<tax_rate_list.size();j++)
		{
			String rev_amt_qry = "Select distinct rd.credit from ARIACORE.refund_detail rd where rd.gl_seq_num IN (Select distinct gtd.taxed_seq_num from ariacore.gl_detail gld JOIN ARIACORE.GL_TAX_DETAIL gtd ON gtd.invoice_no = gld.invoice_no where gld.invoice_no="+invoice_No+" and gld.service_no NOT IN(400,401)) and rd.invoice_no="+invoice_No
			ResultSet rs_rev_amt_qry = db.executePlaQuery(rev_amt_qry)
			while(rs_rev_amt_qry.next())
			{
				def rev_amt=rs_rev_amt_qry.getObject(1)
				cal_tax = rev_amt * tax_rate_list.get(j)
				total_tax=total_tax + cal_tax
			}

		}


		return df.format(total_tax.toString().toDouble()).toString()
	}
	HashMap md_CHECK_IRTA_LINE_ITEMS_API3(String tsetid)
	{
		return md_CHECK_IRTA_LINE_ITEMS_API(tsetid,"3")
	}

	HashMap md_CHECK_IRTA_LINE_ITEMS_DB3(String tsetid)
	{
		return  md_CHECK_IRTA_LINE_ITEMS_DB(tsetid,"3")
	}

	def amountToCollect(String accountNo)
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String result;
		String query = "SELECT BALANCE FROM ARIACORE.ACCT_BALANCE WHERE ACCT_NO= "+accountNo+ " AND CLIENT_NO = "+ clientNo
		logi.logInfo "query "+query
		ConnectDB dbConnection = new ConnectDB()
		String amount = dbConnection.executeQueryP2(query)
		dbConnection.closeConnection()

		return amount
	}

	/**
	 * Retrieves proration amount from DB to check with API response
	 * @param testCaseId
	 * @return Debit amount
	 */
	def md_RESPONSE_PRORATION_AMOUNT(String testcaseId){
		String account_No =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String invoice_no_query = "SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO=" + account_No
		String invoice_No=db.executeQueryP2(invoice_no_query)
		String debit_amount_query = "SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoice_No
		String debit_amount
		ResultSet rs = db.executePlaQuery(debit_amount_query)
		if(rs.next()){
			debit_amount = rs.getString(1)
		}
		return debit_amount
	}

	/**
	 * Verifies applied credit in assign supp plan
	 * @param testCaseId
	 * @return applied credit
	 */
	String md_VERIFY_APPLIED_CREDIT_IN_INVOICE_ASSIGN_SUPP_PLAN(String testCaseId)
	{

		logi.logInfo "Inside  : : md_VERIFY_APPLIED_CREDIT_IN_INVOICE_ASSIGN_SUPP_PLAN  for test case : : "+ testCaseId
		String apiName=testCaseId.split("-")[2]
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String coupon_code
		logi.logInfo "ApI Name : : " + apiName
		if(apiName.contains("^")){
			String service =apiName.substring(apiName.indexOf('^')+1)
			coupon_code = getValueFromRequest(service,"//coupon_code")
		}
		else {
			coupon_code = getValueFromRequest("apply_coupon_to_acct.a","//coupon_code")
		}
		logi.logInfo "coupon_code : : " +coupon_code
		String invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		String applied_amount = db.executeQueryP2("SELECT (SUM(DEBIT)*-1) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND (ORIG_COUPON_CD='" + coupon_code + "' or orig_credit_comments is not null)")
	}

	/**
	 * Calculates Federal tax for CR-0
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_FEDERAL_TAX_CREDIT_REDUCES_0_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		// Getting Tax Rate for federal tax
		def taxRate = db.executeQueryP2("SELECT NVL((rate),0) from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no =  " + clientNo).toString()
		if(taxRate.isEmpty())
		{
			taxRate = 0
		}
		double taxAmount = serviceInvoiceAmount.toDouble() * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(taxAmount).toString();
	}

	/**
	 * Calculates State tax for CR-0
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_STATE_TAX_CREDIT_REDUCES_0_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		// Getting Tax Rate for state tax
		def taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no =  " + clientNo).toString()
		double taxAmount = serviceInvoiceAmount.toDouble() * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(taxAmount).toString();
	}

	/**
	 * Calculates Federal tax for CR-1
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_FEDERAL_TAX_CREDITAMT_BY_ID_CREDIT_REDUCES_1_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		// Getting Tax Rate for federal tax
		def taxRate = db.executeQueryP2("SELECT NVL((rate),0) from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no =  " + clientNo).toString()
		logi.logInfo "Invoice :" + serviceInvoiceAmount
		double creditAmount =md_CREDIT_AMT_BY_CREDITID(testCaseId).toDouble()
		logi.logInfo "Invoice creditAmount :" + creditAmount
		double invoiceAfterCredits = serviceInvoiceAmount.toDouble() - creditAmount
		double taxAmount = invoiceAfterCredits * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(taxAmount).toString();
	}

	/**
	 * Calculates State tax for CR-1
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_STATE_TAX_CREDITAMT_BY_ID_CREDIT_REDUCES_1_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		// Getting Tax Rate for state tax
		def taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no =  " + clientNo).toString()
		logi.logInfo "Invoice :" + serviceInvoiceAmount
		double creditAmount =md_CREDIT_AMT_BY_CREDITID(testCaseId).toDouble()
		logi.logInfo "Invoice creditAmount :" + creditAmount
		double invoiceAfterCredits = serviceInvoiceAmount.toDouble() - creditAmount
		double taxAmount = invoiceAfterCredits * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(taxAmount).toString();
	}

	/**
	 * Calculates Federal tax for CR-1
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_FEDERAL_TAX_CREDIT_REDUCES_1_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		// Getting Tax Rate for federal tax
		def taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no =  " + clientNo).toString()
		logi.logInfo "Invoice :" + serviceInvoiceAmount
		double creditAmount =md_CREDIT_AMT_BY_INVOICE(testCaseId).toDouble()
		logi.logInfo "Invoice creditAmount :" + creditAmount
		double invoiceAfterCredits = serviceInvoiceAmount.toDouble() - creditAmount
		double taxAmount = invoiceAfterCredits * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(taxAmount).toString();
	}

	/**
	 * Calculates State tax for CR-1
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_STATE_TAX_CREDIT_REDUCES_1_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		// Getting Tax Rate for state tax
		def taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no =  " + clientNo).toString()
		logi.logInfo "Invoice :" + serviceInvoiceAmount
		double creditAmount =md_CREDIT_AMT_BY_INVOICE(testCaseId).toDouble()
		logi.logInfo "Invoice creditAmount :" + creditAmount
		double invoiceAfterCredits = serviceInvoiceAmount.toDouble() - creditAmount
		double taxAmount = invoiceAfterCredits * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(taxAmount).toString();
	}
	
	/**
	 * Calculates Federal tax for CR-1 with auto service credit
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_FEDERAL_TAX_AUTO_SERVICE_CREDIT_REDUCES_1_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		// Getting Tax Rate for federal tax
		def taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no =  " + clientNo).toString()
		logi.logInfo "Invoice :" + serviceInvoiceAmount
		double creditAmount =md_CREATE_ORDER_INVOICE_AMT(testCaseId).toDouble()
		logi.logInfo "Invoice creditAmount :" + creditAmount
		double invoiceAfterCredits = serviceInvoiceAmount.toDouble() - creditAmount
		double taxAmount = invoiceAfterCredits * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(taxAmount).toString();
	}

	/**
	 * Calculates State tax for CR-1 for auto service credit
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_STATE_TAX_AUTO_SERVICE_CREDIT_REDUCES_1_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		// Getting Tax Rate for state tax
		def taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no =  " + clientNo).toString()
		logi.logInfo "Invoice :" + serviceInvoiceAmount
		double creditAmount = md_CREATE_ORDER_INVOICE_AMT(testCaseId).toDouble()
		logi.logInfo "Invoice creditAmount :" + creditAmount
		double invoiceAfterCredits = serviceInvoiceAmount.toDouble() - creditAmount
		double taxAmount = invoiceAfterCredits * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(taxAmount).toString();
	}

	/**
	 * Calculates Federal tax for CR-1 with usage
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_FEDERAL_TAX_WITH_USAGE_SERVICE_CREDIT_REDUCES_1_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		// Getting Tax Rate for federal tax
		def taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no =  " + clientNo).toString()
		double taxAmount = serviceInvoiceAmount.toDouble() * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(taxAmount).toString();
	}

	/**
	 * Calculates State tax for CR-1 with usage
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_STATE_TAX_WITH_USAGE_SERVICE_CREDIT_REDUCES_1_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		// Getting Tax Rate for federal tax
		def taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no =  " + clientNo).toString()
		double taxAmount = serviceInvoiceAmount.toDouble() * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(taxAmount).toString();
	}
	/*String md_FEDERAL_TAX_WITH_COUPONS_CREDIT_REDUCES_0_P2(String testCaseId) throws SQLException, ParseException
	 {
	 String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
	 String serviceInvoiceAmount = md_serviceInvoiceAmountIncludingCouponReduction(testCaseId)
	 // Getting Tax Rate for federal tax
	 def taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no =  " + clientNo).toString()
	 double taxAmount = serviceInvoiceAmount.toDouble() * taxRate.toDouble();
	 DecimalFormat df = new DecimalFormat("#.##");
	 return df.format(taxAmount).toString();
	 }
	 String md_STATE_TAX_WITH_COUPONS_CREDIT_REDUCES_0_P2(String testCaseId) throws SQLException, ParseException
	 {
	 String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
	 String serviceInvoiceAmount = md_serviceInvoiceAmountIncludingCouponReduction(testCaseId)
	 // Getting Tax Rate for federal tax
	 def taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no =  " + clientNo).toString()
	 double taxAmount = serviceInvoiceAmount.toDouble() * taxRate.toDouble();
	 DecimalFormat df = new DecimalFormat("#.##");
	 return df.format(taxAmount).toString();
	 }*/

	/**
	 * Calculates Federal tax for CR-1 with Coupons
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_FEDERAL_TAX_WITH_COUPONS_CREDIT_REDUCES_1_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String serviceInvoiceAmount = md_serviceInvoiceAmountIncludingCouponReduction(testCaseId)
		// Getting Tax Rate for federal tax
		def taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no =  " + clientNo).toString()
		double taxAmount = serviceInvoiceAmount.toDouble() * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(taxAmount).toString();
	}

	/**
	 * Calculates State tax for CR-1 with Coupons
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_STATE_TAX_WITH_COUPONS_CREDIT_REDUCES_1_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String serviceInvoiceAmount = md_serviceInvoiceAmountIncludingCouponReduction(testCaseId)
		// Getting Tax Rate for federal tax
		def taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no =  " + clientNo).toString()
		double taxAmount = serviceInvoiceAmount.toDouble() * taxRate.toDouble();
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(taxAmount).toString();
	}

	/**
	 * Calculates State tax for tax inclusinve clients
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_STATE_TAX_INCLUSIVE_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		logi.logInfo "Invoice No : "+invoiceNo
		// Getting tax rate from db
		String taxRate = db.executeQueryP2("SELECT SUM(distinct(tax_rate)) FROM ARIACORE.GL_TAX_DETAIL WHERE invoice_no = "+ invoiceNo + " AND seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = "+ invoiceNo + " AND service_no =401)")
		logi.logInfo "TaxRate : "+taxRate
		String invoice_incl_tax = md_serviceInvoiceAmountDifferenceTaxInclusive(testCaseId, taxRate)
		logi.logInfo "Invoice is  : " + invoice_incl_tax
		double stateTax = (invoice_incl_tax.toDouble())*(taxRate.toDouble())
		return new DecimalFormat("#.#").format(stateTax).toString()
	}
	
	String md_STATE_TAX_INCLUSIVE_vertex(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		logi.logInfo "Invoice No : "+invoiceNo
		// Getting tax rate from db
		String taxRate = db.executeQueryP2("SELECT SUM(distinct(tax_rate)) FROM ARIACORE.GL_TAX_DETAIL WHERE invoice_no = "+ invoiceNo + " AND seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = "+ invoiceNo + " AND service_no =401)")
		logi.logInfo "TaxRate : "+taxRate
		String invoice_incl_tax = md_serviceInvoiceAmountDifferenceTaxInclusive(testCaseId, taxRate)
		logi.logInfo "Invoice is  : " + invoice_incl_tax
		double stateTax = (invoice_incl_tax.toDouble())*(taxRate.toDouble())
		return new DecimalFormat("#.##").format(stateTax).toString()
	}
	
	/**
	 * Calculates State tax for tax inclusinve clients with orders
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_STATE_TAX_INCLUSIVE_P3(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		logi.logInfo "Invoice No : "+invoiceNo
		// Getting tax rate from db
		String taxRate = db.executeQueryP2("SELECT SUM(distinct(tax_rate)) FROM ARIACORE.GL_TAX_DETAIL WHERE invoice_no = "+ invoiceNo + " AND seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = "+ invoiceNo + " AND service_no =401)")
		logi.logInfo "TaxRate : "+taxRate
		String invoice_incl_tax = md_serviceInvoiceAmountDifferenceTaxInclusive(testCaseId, taxRate)
		logi.logInfo "Invoice is  : " + invoice_incl_tax
		double creditAmount = md_CREATE_ORDER_INVOICE_AMT(testCaseId).toDouble()
		logi.logInfo "Credit Amount applied is  : " + creditAmount
		invoice_incl_tax = (invoice_incl_tax.toDouble() - creditAmount).toString()
		double stateTax = (invoice_incl_tax.toDouble() + 0.1)*(taxRate.toDouble())
		return new DecimalFormat("#.##").format(stateTax).toString()
	}
	
	/**
	 * Calculates City tax for tax inclusinve clients
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_CITY_TAX_INCLUSIVE_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		logi.logInfo "Invoice No : "+invoiceNo
		// Getting tax rate from db
		String taxRate = db.executeQueryP2("SELECT SUM(distinct(tax_rate)) FROM ARIACORE.GL_TAX_DETAIL WHERE invoice_no = "+ invoiceNo + " AND seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = "+ invoiceNo + " AND service_no =403)")
		logi.logInfo "TaxRate : "+taxRate
		String invoice_incl_tax = md_serviceInvoiceAmountDifferenceTaxInclusive(testCaseId, taxRate)
		logi.logInfo "Invoice is  : " + invoice_incl_tax
		double cityTax = (invoice_incl_tax.toDouble())*(taxRate.toDouble())
		return new DecimalFormat("#.#").format(cityTax).toString()
	}
	
	/**
	 * Calculates City tax for tax inclusinve clients with orders
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_CITY_TAX_INCLUSIVE_P3(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		logi.logInfo "Invoice No : "+invoiceNo
		// Getting tax rate from db
		String taxRate = db.executeQueryP2("SELECT SUM(distinct(tax_rate)) FROM ARIACORE.GL_TAX_DETAIL WHERE invoice_no = "+ invoiceNo + " AND seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = "+ invoiceNo + " AND service_no =403)")
		logi.logInfo "TaxRate : "+taxRate
		String invoice_incl_tax = md_serviceInvoiceAmountDifferenceTaxInclusive(testCaseId, taxRate)
		logi.logInfo "Invoice is  : " + invoice_incl_tax
		double creditAmount = md_CREATE_ORDER_INVOICE_AMT(testCaseId).toDouble()
		logi.logInfo "Credit Amount applied is  : " + creditAmount
		invoice_incl_tax = (invoice_incl_tax.toDouble() - creditAmount).toString()
		double cityTax = (invoice_incl_tax.toDouble())*(taxRate.toDouble())
		return new DecimalFormat("#.##").format(cityTax).toString()
	}
	
	/**
	 * Calculates District tax for tax inclusinve clients
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_DISTRICT_TAX_INCLUSIVE_P2(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		logi.logInfo "Invoice No : "+invoiceNo
		// Getting tax rate from db
		String taxRate = db.executeQueryP2("SELECT SUM(distinct(tax_rate)) FROM ARIACORE.GL_TAX_DETAIL WHERE invoice_no = "+ invoiceNo + " AND seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = "+ invoiceNo + " AND service_no =405)")
		logi.logInfo "TaxRate : "+taxRate
		String invoice_incl_tax = md_serviceInvoiceAmountDifferenceTaxInclusive(testCaseId, taxRate)
		logi.logInfo "Invoice is  : " + invoice_incl_tax
		double districtTax = (invoice_incl_tax.toDouble())*(taxRate.toDouble())
		return new DecimalFormat("#.##").format(districtTax).toString()
	}
	
	/**
	 * Calculates District tax for tax inclusinve clients with orders
	 * @param testCaseId
	 * @return Tax amount
	 */
	String md_DISTRICT_TAX_INCLUSIVE_P3(String testCaseId) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		logi.logInfo "Invoice No : "+invoiceNo
		// Getting tax rate from db
		String taxRate = db.executeQueryP2("SELECT SUM(distinct(tax_rate)) FROM ARIACORE.GL_TAX_DETAIL WHERE invoice_no = "+ invoiceNo + " AND seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = "+ invoiceNo + " AND service_no =405)")
		logi.logInfo "TaxRate : "+taxRate
		String invoice_incl_tax = md_serviceInvoiceAmountDifferenceTaxInclusive(testCaseId, taxRate)
		logi.logInfo "Invoice is  : " + invoice_incl_tax
		double creditAmount = md_CREATE_ORDER_INVOICE_AMT(testCaseId).toDouble()
		logi.logInfo "Credit Amount applied is  : " + creditAmount
		invoice_incl_tax = (invoice_incl_tax.toDouble() - creditAmount).toString()
		double districtTax = (invoice_incl_tax.toDouble())*(taxRate.toDouble())
		return new DecimalFormat("#.##").format(districtTax).toString()
	}
	
	/**
	 * Calculates Credit amount reduced by Credit ID
	 * @param testCaseId
	 * @return Credit amount
	 */
	String md_CREDIT_AMT_BY_CREDITID(String testcaseId)
	{
		logi.logInfo('Inside md_CREDIT_AMT_BY_CREDITID ' )
		String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		String serviceCreditId=db.executeQueryP2("SELECT credit_id from ariacore.all_credits where acct_no="+accountNo+" and credit_type='S' and reason_cd=1")
		String amt =db.executeQueryP2("SELECT NVL((sum(debit)*-1),0) from ariacore.gl_detail where invoice_no="+invoiceNo+" and orig_credit_id="+serviceCreditId)
		return amt
	}

	/**
	 * Calculates Credit amount reduced by Invoice
	 * @param testCaseId
	 * @return Credit amount
	 */
	String md_CREDIT_AMT_BY_INVOICE(String testcaseId)
	{
		logi.logInfo('Inside md_CREDIT_AMT_BY_INVOICE ' )
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		//String serviceCreditId=db.executeQueryP2("SELECT credit_id from ariacore.all_credits where acct_no="+accountNo+" and credit_type='S' and reason_cd=1")
		String amt = db.executeQueryP2("SELECT NVL((sum(debit)*-1),0) as CREDIT from ariacore.gl_detail where invoice_no="+invoiceNo+" and usage_units is null and orig_charge_seq_num || orig_recurring_credit_no is not null")
		return amt
	}

	/**
	 * Calculates Credit amount reduced by Invoice
	 * @param testCaseId
	 * @return Credit amount
	 */
	String md_AUTO_SERVICE_CREDIT_AMT(String testcaseId)
	{
		logi.logInfo('Inside md_AUTO_SERVICE_CREDIT_AMT ' )
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		String amt
		amt = db.executeQueryP2("SELECT NVL((sum(debit)*-1),0) as CREDIT from ariacore.gl_detail where invoice_no="+invoiceNo+" and usage_units is null and orig_credit_id is not null")
		return amt
	}

	/**
	 * Calculates Invoice with credit and tax reduced for CR-0
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoiceKeyAPI_CR0(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceKeyAPI_CR0 "

		// Getting Account and Client numbers from property and second maximum invoice number (to calculate the serivce, federal and state tax deducations)
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" and invoice_no <> (SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo +")")
		logi.logInfo "Invoice No : "+invoiceNo
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)

		// Getting plan number based the operation operation performed
		String planNo = ""
		String service  = Constant.SERVICENAME
		if (service.contains("^"))
			service = service.substring(service.indexOf("^")+1, service.length())
		else
			service = service.substring(0, service.length())
		logi.logInfo "service API  :: "+ service

		if(service.equalsIgnoreCase("replace_supp_plan"))
			planNo = getValueFromRequest(service,"//existing_supp_plan_no").toString()
		else if(service.equalsIgnoreCase("create_advanced_service_credit"))
			planNo = getValueFromRequest(service,"//eligible_plan_no").toString()
		else
			planNo = getValueFromRequest(service,"//supp_plan_no").toString()
		logi.logInfo "planNo :: "+planNo

		// Checking for the type of plan
		String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
		int planTypeInd = Integer.parseInt(db.executeQueryP2(planTypeQuery))
		// Getting plans involved in current invoice
		HashMap<String, Integer> services = getPlanServiceAndTieredPricingByPlan(planNo, invoiceNo)

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = db.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time before calculating State Taxes: "+ currentVirtualTime

		// Calculating the proration amount
		int usedDays=db.executeQueryP2("SELECT ((SELECT TRUNC(BILL_DATE) FROM ariacore.GL WHERE acct_no =  "+accountNo+" AND invoice_no ="+invoiceNo+" AND client_no ="+clientNo+" )- TRUNC(plan_date) ) AS days FROM ariacore.acct WHERE acct_no =  "+accountNo+" AND client_no ="+clientNo).toInteger()
		logi.logInfo "DaysInvoice / USED : "+(usedDays)
		int billingInterval = Integer.parseInt(db.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planNo+ "and client_no="+clientNo))
		Date lastbilldate
		ResultSet rs=db.executePlaQuery("SELECT TRUNC(last_bill_date) FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo)
		rs.next()
		lastbilldate=rs.getDate(1)
		Date virtualstartdate = subractMonthsFromGivenDate(lastbilldate,billingInterval)
		logi.logInfo "Virtualstartdate : "+virtualstartdate
		int durationOfPlan = getDaysBetween(virtualstartdate,lastbilldate )
		logi.logInfo "Duration of a plan : "+planNo + " :: "+durationOfPlan
		logi.logInfo "DaysInvoice / used : "+usedDays

		if(durationOfPlan==31){
			usedDays+=1
		}else if(durationOfPlan == 28){
			usedDays-=2
		}else if(durationOfPlan == 29){
			usedDays-=1
		}
		double prorationFactor=usedDays/durationOfPlan.toDouble()
		logi.logInfo "ProrationFactor : " + prorationFactor

		// Getting Tax Rate for federal tax
		def taxRateFederal = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no =  " + clientNo).toString()
		// Getting Tax Rate for state tax
		def taxRateState = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no =  " + clientNo).toString()
		double federalDeduction, stateDeduction

		Iterator iterator = services.entrySet().iterator()
		double totalServiceProrated
		DecimalFormat df = new DecimalFormat("#.##");

		// Iterating though plans
		while (iterator.hasNext()) {
			logi.logInfo "Inside while for Service"
			Map.Entry pairs = (Map.Entry) iterator.next()
			String serviceNo = pairs.getKey().toString()

			// Calculating service amount based on the plan, service and tiered pricing
			logi.logInfo "Calculating service amount for Plan : "+planNo +" and Service : "+ serviceNo
			String tieredPricing = pairs.getValue().toString()
			logi.logInfo "Calculating service amount for Plan : "+planNo +" and Service : "+ serviceNo+" and Tiered Pricing "+ tieredPricing
			int tieredPricingInt = Integer.parseInt(tieredPricing)
			logi.logInfo "Calculating service amount for Plan : "+planNo +" and Service : "+ serviceNo +" and Tiered Pricing "+ tieredPricingInt
			int serviceInvoice = md_getServiceAmount(testCaseId, planNo, serviceNo, tieredPricingInt)
			logi.logInfo "Method Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice

			// Calculating corresponding service deduction amount
			double serviceProrated = serviceInvoice.toDouble() * prorationFactor
			logi.logInfo "Prorated amount for Service : "+ serviceNo +" is "+ serviceProrated
			totalServiceProrated = totalServiceProrated+serviceProrated
			logi.logInfo "Total Prorated amount till Service : "+ serviceNo +" is "+ serviceProrated

			// Calculating federal deduction amount for corresponding service
			double federalDeductionForService = serviceProrated * taxRateFederal.toDouble()
			federalDeductionForService = df.format(federalDeductionForService).toString().toDouble()
			logi.logInfo "Prorated federal tax amount for Service : "+ serviceNo +" is "+federalDeductionForService
			federalDeduction = federalDeduction +federalDeductionForService
			logi.logInfo "Total Prorated federal tax amount till Service : "+ serviceNo +" is "+ federalDeduction

			// Calculating state deduction amount for corresponding service
			double stateDeductionForService = serviceProrated * taxRateState.toDouble()
			stateDeductionForService = df.format(stateDeductionForService).toString().toDouble()
			logi.logInfo "Prorated state tax amount for Service : "+ serviceNo +" is "+stateDeductionForService
			stateDeduction = stateDeduction+ stateDeductionForService
			logi.logInfo "Total Prorated state tax amount till Service : "+ serviceNo +" is "+ stateDeduction
		}
		logi.logInfo ' Total FederalDeduction : ' + federalDeduction + " Total StateDeduction : "+stateDeduction + " Total ServiceDeduction : "+ df.format(totalServiceProrated)
		double totalDeduction = federalDeduction+ stateDeduction + totalServiceProrated
		logi.logInfo 'TotalDeduction : ' + totalDeduction

		// Calculating service, federal and state tax amount
		String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		String federalTax = md_FEDERAL_TAX_CREDIT_REDUCES_0_P2(testCaseId)
		String stateTax =  md_STATE_TAX_CREDIT_REDUCES_0_P2(testCaseId)
		double currentFederalTax =federalTax.toDouble()
		double currentStateTax =stateTax.toDouble()
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+currentFederalTax + " CurrentStateTax : "+ currentStateTax + " TotalDeduction(based on the operation performed + federal and state deduction) :"+ totalDeduction
		// Subtracting the deduction amount from the amount calcualted(service+federal+state amount) to get current invoice amount
		double totalInvoice = serviceInvoiceAmount.toDouble() + currentFederalTax + currentStateTax - totalDeduction
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice).toString();
	}

	public Date subractMonthsFromGivenDate(Date dateGiven, int noOfMonths)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateGiven);
		cal.add(Calendar.MONTH,noOfMonths*-1)
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		String dateMonthsAdded = format1.format(cal.getTime());
		Date planEndDate = format1.parse(dateMonthsAdded)
		return planEndDate
	}

	/**
	 * Calculates Invoice with credit and tax reduced for CR-1
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoiceKeyAPI_CR1(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceKeyAPI_CR1 "
		// Getting Account and Client numbers from property and second maximum invoice number (to calculate the serivce, federal and state tax deducations)
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" and invoice_no <> (SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo +")")
		logi.logInfo "Invoice No : "+invoiceNo
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)

		// Getting plan number based the operation operation performed
		String planNo = ""
		String service  = Constant.SERVICENAME
		if (service.contains("^"))
			service = service.substring(service.indexOf("^")+1, service.length())
		else
			service = service.substring(0, service.length())
		logi.logInfo "service API  :: "+ service

		if(service.equalsIgnoreCase("replace_supp_plan"))
			planNo = getValueFromRequest(service,"//existing_supp_plan_no").toString()
		else if(service.equalsIgnoreCase("create_advanced_service_credit"))
			planNo = getValueFromRequest(service,"//eligible_plan_no").toString()
		else
			planNo = getValueFromRequest(service,"//supp_plan_no").toString()
		logi.logInfo "planNo :: "+planNo

		// Checking for the type of plan
		String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
		int planTypeInd = Integer.parseInt(db.executeQueryP2(planTypeQuery))
		// Getting plans involved in current invoice
		HashMap<String, Integer> services = getPlanServiceAndTieredPricingByPlan(planNo, invoiceNo)

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = db.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time before calculating State Taxes: "+ currentVirtualTime

		/*Calculating the proration amount
		 * test
		 */
		int usedDays=db.executeQueryP2("SELECT ((SELECT TRUNC(BILL_DATE) FROM ariacore.GL WHERE acct_no =  "+accountNo+" AND invoice_no ="+invoiceNo+" AND client_no ="+clientNo+" )- TRUNC(plan_date) ) AS days FROM ariacore.acct WHERE acct_no =  "+accountNo+" AND client_no ="+clientNo).toInteger()
		logi.logInfo "DaysInvoice / USED : "+(usedDays)
		int billingInterval = Integer.parseInt(db.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planNo+ "and client_no="+clientNo))
		Date lastbilldate
		ResultSet rs=db.executePlaQuery("SELECT TRUNC(last_bill_date) FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo)
		rs.next()
		lastbilldate=rs.getDate(1)
		Date virtualstartdate = subractMonthsFromGivenDate(lastbilldate,billingInterval)
		logi.logInfo "Virtualstartdate : "+virtualstartdate
		int durationOfPlan = getDaysBetween(virtualstartdate,lastbilldate )
		logi.logInfo "Duration of a plan : "+planNo + " :: "+durationOfPlan
		logi.logInfo "DaysInvoice / used : "+usedDays

		if(durationOfPlan==31){
			usedDays+=1
		}else if(durationOfPlan == 28){
			usedDays-=2
		}else if(durationOfPlan == 29){
			usedDays-=1
		}
		double prorationFactor=usedDays/durationOfPlan.toDouble()
		logi.logInfo "ProrationFactor : " + prorationFactor

		// Getting Tax Rate for federal tax
		def taxRateFederal = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no =  " + clientNo).toString()
		// Getting Tax Rate for state tax
		def taxRateState = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no =  " + clientNo).toString()
		double federalDeduction, stateDeduction

		Iterator iterator = services.entrySet().iterator()
		double totalServiceProrated
		DecimalFormat df = new DecimalFormat("#.##");

		// Iterating though plans
		while (iterator.hasNext()) {
			logi.logInfo "Inside while for Service"
			Map.Entry pairs = (Map.Entry) iterator.next()
			String serviceNo = pairs.getKey().toString()

			// Calculating service amount based on the plan, service and tiered pricing
			logi.logInfo "Calculating service amount for Plan : "+planNo +" and Service : "+ serviceNo
			String tieredPricing = pairs.getValue().toString()
			logi.logInfo "Calculating service amount for Plan : "+planNo +" and Service : "+ serviceNo+" and Tiered Pricing "+ tieredPricing
			int tieredPricingInt = Integer.parseInt(tieredPricing)
			logi.logInfo "Calculating service amount for Plan : "+planNo +" and Service : "+ serviceNo +" and Tiered Pricing "+ tieredPricingInt
			int serviceInvoice = md_getServiceAmount(testCaseId, planNo, serviceNo, tieredPricingInt)
			logi.logInfo "Method Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice

			// Calculating corresponding service deduction amount
			double serviceProrated = serviceInvoice.toDouble() * prorationFactor
			logi.logInfo "Prorated amount for Service : "+ serviceNo +" is "+ serviceProrated
			totalServiceProrated = totalServiceProrated+serviceProrated
			logi.logInfo "Total Prorated amount till Service : "+ serviceNo +" is "+ serviceProrated

			// Getting eligible plan and service number to calculate service prorated invoice
			String eligiblePlanNo = db.executeQueryP2("SELECT eligible_plan_no from ariacore.credits where credit_id = (SELECT min(credit_id) from ariacore.all_credits where acct_no="+accountNo+" and credit_type='S')")
			if(planNo.equalsIgnoreCase(eligiblePlanNo)){
				String eligibleServiceNo = db.executeQueryP2("SELECT eligible_service_no from ariacore.credits where credit_id = (SELECT min(credit_id) from ariacore.all_credits where acct_no="+accountNo+" and credit_type='S')")
				if(serviceNo.equalsIgnoreCase(eligibleServiceNo)){
					double creditAmount = db.executeQueryP2("SELECT amount from ariacore.credits where credit_id = (SELECT min(credit_id) from ariacore.all_credits where acct_no="+accountNo+" and credit_type='S')").toDouble()
					logi.logInfo "Tax Calculation : Credit Amount :  "+ creditAmount
					logi.logInfo "Tax Calculation : serviceInvoice :  "+ serviceInvoice
					serviceProrated = (serviceInvoice.toDouble() - creditAmount) * prorationFactor
					logi.logInfo "Tax Calculation : Prorated amount for Service : "+ serviceNo +" is "+ serviceProrated
				}
			}

			// Calculating federal deduction amount for corresponding service
			double federalDeductionForService = serviceProrated * taxRateFederal.toDouble()
			federalDeductionForService = df.format(federalDeductionForService).toString().toDouble()
			logi.logInfo "Prorated federal tax amount for Service : "+ serviceNo +" is "+federalDeductionForService
			federalDeduction = federalDeduction +federalDeductionForService
			logi.logInfo "Total Prorated federal tax amount till Service : "+ serviceNo +" is "+ federalDeduction

			// Calculating state deduction amount for corresponding service
			double stateDeductionForService = serviceProrated * taxRateState.toDouble()
			stateDeductionForService = df.format(stateDeductionForService).toString().toDouble()
			logi.logInfo "Prorated state tax amount for Service : "+ serviceNo +" is "+stateDeductionForService
			stateDeduction = stateDeduction+ stateDeductionForService
			logi.logInfo "Total Prorated state tax amount till Service : "+ serviceNo +" is "+ stateDeduction
		}
		logi.logInfo ' Total FederalDeduction : ' + federalDeduction + " Total StateDeduction : "+stateDeduction + " Total ServiceDeduction : "+ df.format(totalServiceProrated)
		double totalDeduction = federalDeduction+ stateDeduction + totalServiceProrated
		logi.logInfo 'TotalDeduction : ' + totalDeduction

		// Calculating service, federal and state tax amount
		double serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId).toDouble()
		double federalTax = md_FEDERAL_TAX_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		double stateTax =  md_STATE_TAX_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax + " TotalDeduction(based on the operation performed + federal and state deduction) :"+ totalDeduction
		// Subtracting the deduction amount from the amount calculated(service+federal+state amount) to get current invoice amount
		double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax - totalDeduction
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice).toString();
	}

	/**
	 * Calculates Invoice with credit and tax reduced for CR-0
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoiceCreditAmtKeyAPI_CR1(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceCreditAmtKeyAPI_CR1 "
		// Getting Account and Client numbers from property and second maximum invoice number (to calculate the serivce, federal and state tax deducations)
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" and invoice_no <> (SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo +")")
		logi.logInfo "Invoice No : "+invoiceNo
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)

		// Getting plan number based the operation operation performed
		String planNo = ""
		String service  = Constant.SERVICENAME
		if (service.contains("^"))
			service = service.substring(service.indexOf("^")+1, service.length())
		else
			service = service.substring(0, service.length())
		logi.logInfo "service API  :: "+ service

		if(service.equalsIgnoreCase("replace_supp_plan"))
			planNo = getValueFromRequest(service,"//existing_supp_plan_no").toString()
		else if(service.equalsIgnoreCase("create_advanced_service_credit"))
			planNo = getValueFromRequest(service,"//eligible_plan_no").toString()
		else
			planNo = getValueFromRequest(service,"//supp_plan_no").toString()
		logi.logInfo "planNo :: "+planNo

		// Checking for the type of plan
		String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
		int planTypeInd = Integer.parseInt(db.executeQueryP2(planTypeQuery))
		// Getting plans involved in current invoice
		HashMap<String, Integer> services = getPlanServiceAndTieredPricingByPlan(planNo, invoiceNo)

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = db.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time before calculating State Taxes: "+ currentVirtualTime

		/*Calculating the proration amount
		 * test
		 */
		int usedDays=db.executeQueryP2("SELECT ((SELECT TRUNC(BILL_DATE) FROM ariacore.GL WHERE acct_no =  "+accountNo+" AND invoice_no ="+invoiceNo+" AND client_no ="+clientNo+" )- TRUNC(plan_date) ) AS days FROM ariacore.acct WHERE acct_no =  "+accountNo+" AND client_no ="+clientNo).toInteger()
		logi.logInfo "DaysInvoice / USED : "+(usedDays)
		int billingInterval = Integer.parseInt(db.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planNo+ "and client_no="+clientNo))
		Date lastbilldate
		ResultSet rs=db.executePlaQuery("SELECT TRUNC(last_bill_date) FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo)
		rs.next()
		lastbilldate=rs.getDate(1)
		Date virtualstartdate = subractMonthsFromGivenDate(lastbilldate,billingInterval)
		logi.logInfo "Virtualstartdate : "+virtualstartdate
		int durationOfPlan = getDaysBetween(virtualstartdate,lastbilldate )
		logi.logInfo "Duration of a plan : "+planNo + " :: "+durationOfPlan
		logi.logInfo "DaysInvoice / used : "+usedDays

		if(durationOfPlan==31){
			usedDays+=1
		}else if(durationOfPlan == 28){
			usedDays-=2
		}else if(durationOfPlan == 29){
			usedDays-=1
		}
		double prorationFactor=usedDays/durationOfPlan.toDouble()
		logi.logInfo "ProrationFactor : " + prorationFactor

		// Getting Tax Rate for federal tax
		def taxRateFederal = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no =  " + clientNo).toString()
		// Getting Tax Rate for state tax
		def taxRateState = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no =  " + clientNo).toString()
		double federalDeduction, stateDeduction

		Iterator iterator = services.entrySet().iterator()
		double totalServiceProrated
		DecimalFormat df = new DecimalFormat("#.##");

		// Iterating though plans
		while (iterator.hasNext()) {
			logi.logInfo "Inside while for Service"
			Map.Entry pairs = (Map.Entry) iterator.next()
			String serviceNo = pairs.getKey().toString()

			// Calculating service amount based on the plan, service and tiered pricing
			logi.logInfo "Calculating service amount for Plan : "+planNo +" and Service : "+ serviceNo
			String tieredPricing = pairs.getValue().toString()
			logi.logInfo "Calculating service amount for Plan : "+planNo +" and Service : "+ serviceNo+" and Tiered Pricing "+ tieredPricing
			int tieredPricingInt = Integer.parseInt(tieredPricing)
			logi.logInfo "Calculating service amount for Plan : "+planNo +" and Service : "+ serviceNo +" and Tiered Pricing "+ tieredPricingInt
			int serviceInvoice = md_getServiceAmount(testCaseId, planNo, serviceNo, tieredPricingInt)
			logi.logInfo "Method Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice

			// Calculating corresponding service deduction amount
			double serviceProrated = serviceInvoice.toDouble() * prorationFactor
			logi.logInfo "Prorated amount for Service : "+ serviceNo +" is "+ serviceProrated
			totalServiceProrated = totalServiceProrated+serviceProrated
			logi.logInfo "Total Prorated amount till Service : "+ serviceNo +" is "+ serviceProrated

			// Getting eligible plan and service number to calculate service prorated invoice
			String eligiblePlanNo = db.executeQueryP2("SELECT eligible_plan_no from ariacore.credits where credit_id = (SELECT min(credit_id) from ariacore.all_credits where acct_no="+accountNo+" and credit_type='S')")
			if(planNo.equalsIgnoreCase(eligiblePlanNo)){
				String eligibleServiceNo = db.executeQueryP2("SELECT eligible_service_no from ariacore.credits where credit_id = (SELECT min(credit_id) from ariacore.all_credits where acct_no="+accountNo+" and credit_type='S')")
				if(serviceNo.equalsIgnoreCase(eligibleServiceNo)){
					double creditAmount = db.executeQueryP2("SELECT amount from ariacore.credits where credit_id = (SELECT min(credit_id) from ariacore.all_credits where acct_no="+accountNo+" and credit_type='S')").toDouble()
					logi.logInfo "Tax Calculation : Credit Amount :  "+ creditAmount
					logi.logInfo "Tax Calculation : serviceInvoice :  "+ serviceInvoice
					serviceProrated = (serviceInvoice.toDouble() - creditAmount) * prorationFactor
					logi.logInfo "Tax Calculation : Prorated amount for Service : "+ serviceNo +" is "+ serviceProrated
				}
			}

			// Calculating federal deduction amount for corresponding service
			double federalDeductionForService = serviceProrated * taxRateFederal.toDouble()
			federalDeductionForService = df.format(federalDeductionForService).toString().toDouble()
			logi.logInfo "Prorated federal tax amount for Service : "+ serviceNo +" is "+federalDeductionForService
			federalDeduction = federalDeduction +federalDeductionForService
			logi.logInfo "Total Prorated federal tax amount till Service : "+ serviceNo +" is "+ federalDeduction

			// Calculating state deduction amount for corresponding service
			double stateDeductionForService = serviceProrated * taxRateState.toDouble()
			stateDeductionForService = df.format(stateDeductionForService).toString().toDouble()
			logi.logInfo "Prorated state tax amount for Service : "+ serviceNo +" is "+stateDeductionForService
			stateDeduction = stateDeduction+ stateDeductionForService
			logi.logInfo "Total Prorated state tax amount till Service : "+ serviceNo +" is "+ stateDeduction
		}
		logi.logInfo ' Total FederalDeduction : ' + federalDeduction + " Total StateDeduction : "+stateDeduction + " Total ServiceDeduction : "+ df.format(totalServiceProrated)
		double totalDeduction = federalDeduction+ stateDeduction + totalServiceProrated
		logi.logInfo 'TotalDeduction : ' + totalDeduction

		// Calculating service, federal and state tax amount
		int serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		double federalTax = md_FEDERAL_TAX_CREDITAMT_BY_ID_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		double stateTax =  md_STATE_TAX_CREDITAMT_BY_ID_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax + " TotalDeduction(based on the operation performed + federal and state deduction) :"+ totalDeduction
		// Subtracting the deduction amount from the amount calculated(service+federal+state amount) to get current invoice amount
		double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax - totalDeduction
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice).toString();
	}

	/**
	 * Calculates Invoice with credit and tax reduced for CR-0 with orders
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoiceAutoServiceCreditKeyAPI_CR0(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceAutoServiceCreditKeyAPI_CR0 "
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		// Calculating service, federal and state tax amount
		String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		double federalTax = md_FEDERAL_TAX_CREDIT_REDUCES_0_P2(testCaseId).toDouble()
		double stateTax =  md_STATE_TAX_CREDIT_REDUCES_0_P2(testCaseId).toDouble()
		// Calculating auto service credit amount
		double creditAmount = md_CREATE_ORDER_INVOICE_AMT(testCaseId).toDouble()
		// Subtracting the auto service credit amount from the amount calculated(service+federal+state amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount.toDouble() + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax + " AutoServiceCredit Amount :"+ creditAmount
		double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax - creditAmount
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice).toString();
	}

	/**
	 * Calculates Invoice with credit and tax reduced for CR-1 with orders
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoiceAutoServiceCreditKeyAPI_CR1(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceAutoServiceCreditKeyAPI_CR1 "
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		// Calculating service, federal and state tax amount
		String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
		double federalTax = md_FEDERAL_TAX_AUTO_SERVICE_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		double stateTax =  md_STATE_TAX_AUTO_SERVICE_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		// Calculating auto service credit amount
		double creditAmount =md_CREATE_ORDER_INVOICE_AMT(testCaseId).toDouble()
		// Subtracting the auto service credit amount from the amount calculated(service+federal+state amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount.toDouble() + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax + " AutoServiceCredit Amount :"+ creditAmount
		double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax - creditAmount
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice).toString();
	}
	
	/**
	 * Calculates Invoice with credit and tax reduced for CR-0 with coupons
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoiceAutoServiceCreditAndCouponKeyAPI_CR0(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceAutoServiceCreditAndCouponKeyAPI_CR0 "
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		// Calculating service, federal and state tax amount
		String serviceInvoiceAmount = md_serviceInvoiceAmountIncludingCouponReduction(testCaseId)
		double federalTax = md_FEDERAL_TAX_CREDIT_REDUCES_0_P2(testCaseId).toDouble()
		double stateTax =  md_STATE_TAX_CREDIT_REDUCES_0_P2(testCaseId).toDouble()
		// Adding the amount calculated(service+federal+state amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax
		double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice).toString();
	}

	/**
	 * Calculates Invoice with credit and tax reduced for CR-1 with coupons
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoiceAutoServiceCreditAndCouponKeyAPI_CR1(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceAutoServiceCreditAndCouponKeyAPI_CR1 "
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		// Calculating service, federal and state tax amount
		String serviceInvoiceAmount = md_serviceInvoiceAmountIncludingCouponReduction(testCaseId)
		double federalTax = md_FEDERAL_TAX_WITH_COUPONS_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		double stateTax =  md_STATE_TAX_WITH_COUPONS_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		
		// Adding the amount calculated(service+federal+state amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax
		double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice).toString();
	}
	
	/**
	 * Retrieves services for the plans
	 * @param plan number, invoice number
	 * @return HashMap of services
	 */
	public HashMap getPlanServiceAndTieredPricingByPlan(String planNo, String invoiceNo){
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		logi.logInfo "Invoice : "+invoiceNo
		String query = "select service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no = "+planNo+" and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and recurring = 1) order by  service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(service_no) from ariacore.plan_services where plan_no = "+planNo+" and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and recurring = 1) order by service_no"))
		ResultSet resultSet = database.executeQuery(query)

		HashMap<String,Integer> resultHash = new HashMap<String,Integer> ()

		String[] service = new String[count]
		int[] tieredPricing = new int[count]

		int i= 0
		while(resultSet.next()){
			service[i]=resultSet.getString(1)
			tieredPricing[i]=resultSet.getInt(2)
			i++
		}

		for(int serviceCount=0;serviceCount<service.length;serviceCount++){
			resultHash.put(service[serviceCount], tieredPricing[serviceCount])
			logi.logInfo "Tiered Pricing for a sercice : "+service[serviceCount] + " is " + resultHash.get(service[serviceCount])
		}
		logi.logInfo "Total Services and its TieredPricing : " + resultHash.toString()
		return resultHash
	}

	/**
	 * Calculates Invoice with credit and tax reduced
	 * @param testCaseId
	 * @return Invoice amount
	 */
	int md_getServiceAmount(String testCaseId, String planNo, String serviceNo, int tieredPricing) {
		logi.logInfo "Inside md_getServiceAmount"
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
		logi.logInfo "TestId : "+ testId
		logi.logInfo "Execution for the plan :  "+ planNo
		// Checking for the type of plan
		String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
		int planTypeInd = Integer.parseInt(db.executeQueryP2(planTypeQuery))
		logi.logInfo "Plan type : "+planTypeInd+ " plan : " +planNo
		int serviceInvoice
		int noOfUnits =0
		// Getting plan units
		if(planTypeInd==0){
			logi.logInfo "Master Plan : "+ planNo
			String newQuery = "SELECT PLAN_UNITS FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO = " + accountNo
			noOfUnits = Integer.parseInt(db.executeQueryP2(newQuery))
		}
		else {
			logi.logInfo "Supp Plan : "+ planNo
			
			String[] noOfUnitArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
			String[] suppPlans = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")
			if (!suppPlans.toString().equalsIgnoreCase("null")) {
				logi.logInfo("Array of supp plan")
				logi.logInfo "supp_plans :::  " + suppPlans.toString()
				logi.logInfo "noOfUnits :::  " + noOfUnitArray.toString()
				if(Arrays.asList(suppPlans).contains(planNo)){
					for( int j=0;j<suppPlans.length;j++) {
						if(suppPlans[j].toString().equalsIgnoreCase(planNo)){
							noOfUnits = Integer.parseInt(noOfUnitArray[j])
							logi.logInfo "no_of_units : "+noOfUnits+ " plan : " +planNo
							break
						}
					}
				}
			}
			else {
				logi.logInfo("single supp plan")
				String suppPlan = getValueFromRequest("create_acct_complete","//supp_plans").toString()
				if ((!suppPlan.toString().equalsIgnoreCase("null"))&& suppPlan.equalsIgnoreCase(planNo)) {
					noOfUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))
					
					String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
					int noOfUnits2 = Integer.parseInt(db.executeQueryP2(supp_plan_unit_query))
					
					if(noOfUnits2 > noOfUnits) {
						noOfUnits = noOfUnits2
					}
					
					logi.logInfo "single_supp_plan :::  " + suppPlan
					logi.logInfo "single_plan_noOfUnits :::  " + noOfUnits
				}
				else {
					logi.logInfo "assign or replace supp plan"
					String supp_plan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
					if(supp_plan.equalsIgnoreCase(planNo))
						noOfUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
					else{
						supp_plan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
						if(supp_plan.equalsIgnoreCase(planNo))
							noOfUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
						else {
							String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
							noOfUnits = Integer.parseInt(db.executeQueryP2(supp_plan_unit_query))
						}
					}
				}
			}
		}
		logi.logInfo "No of Units : "+noOfUnits+ " of a Plan : " +planNo

		String isServiceTaxableQuery = "SELECT TAXABLE_IND from ariacore.all_service where service_no = " +serviceNo
		String isServiceTaxable = db.executeQueryP2(isServiceTaxableQuery)
		logi.logInfo "Service "+ serviceNo+" taxable/not : "+isServiceTaxable
		String zeroDollarPlanCheckQuery = "select distinct trunc(sum(rate_per_unit)) as RatePerUnit from ariacore.new_rate_sched_rates where schedule_no in (select schedule_no from ariacore.plan_rate_sched_map where plan_no = "+planNo+" and client_no="+clientNo+") and service_no = "+serviceNo+" and client_no="+clientNo
		int isZeroDollar = Integer.parseInt(db.executeQueryP2(zeroDollarPlanCheckQuery))
		logi.logInfo "Service "+ serviceNo+" Zero Dollar/not : "+isZeroDollar

		//Checking the service is taxable and non-zero dollar plan or not
		if(isServiceTaxable.equalsIgnoreCase("1") && (isZeroDollar!=0)){
			logi.logInfo "Service is taxable : "+serviceNo
			int seqNo=1;
			logi.logInfo "Invoice calculation for a TP : "+tieredPricing
			String query
			switch(tieredPricing){
				case 1:
					logi.logInfo "Case 1"
					query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
					serviceInvoice = Integer.parseInt(db.executeQueryP2(query))
					logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
					seqNo++;
					break;
				case 2 :
					logi.logInfo "Case 2"
					query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"

					serviceInvoice = Integer.parseInt(db.executeQueryP2(query))
					logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
					break;
				case 3 :
					logi.logInfo "Case 3"
					query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
					serviceInvoice = Integer.parseInt(db.executeQueryP2(query))
					logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
					break;
			}
		}
		else
			logi.logInfo "Service is not taxable : "+ serviceNo
		logi.logInfo "Returning amoount for Service : "+ serviceNo + " : " + serviceInvoice
		return serviceInvoice
	}
	
	/**
	 * Calculates Invoice with credit and tax inclusive reduced
	 * @param tax percent, amount
	 * @return Invoice amount
	 */
	String md_serviceAmountAfterTaxInclusive(String taxPercent,String amount) {
		logi.logInfo "Inside md_serviceAmountAfterTaxInclusive"
		logi.logInfo "Taxation Percent : "+taxPercent
		logi.logInfo "Amount : "+ amount
		String serviceAmount=((amount.toDouble()/(taxPercent.toDouble()+1).toDouble()).trunc(2)).toString()
		logi.logInfo "ServiceAmount : "+serviceAmount
		return serviceAmount
	}

	/**
	 * Calculates Invoice difference with credit and tax inclusive reduced
	 * @param tax percent, amount
	 * @return Invoice amount
	 */
	String md_serviceAmountAfterTaxInclusive_Difference(String invoiceNo,String amount) {
		logi.logInfo "Inside md_serviceAmountAfterTaxInclusive_Difference"
		logi.logInfo "Invoice Number is : "+invoiceNo
		logi.logInfo "Amount : "+ amount
		String taxPercent=db.executeQueryP2("SELECT 1+SUM(distinct(tax_rate)) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + invoiceNo + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + invoiceNo + " AND service_no in (401,403,405))")
		logi.logInfo "tax Percent calculated : "+ taxPercent
		String serviceAmountCalc = ((amount.toDouble()/taxPercent.toDouble()).trunc(2)).toString()
		logi.logInfo "ServiceAmount : "+serviceAmountCalc
		return serviceAmountCalc
//		String taxationAmount=((amount.toDouble()-serviceAmountCalc.toDouble()).trunc(2)).toString()
//		logi.logInfo "Taxation amount :  "+taxationAmount
//		return taxationAmount
	}
	
	/**
	 * Calculates Invoice with credit and tax inclusive
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_serviceInvoiceAmountTaxInclusive(String testCaseId) {
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
		logi.logInfo "testId : "+ testId
		def invoice = 0
		String invoiceNo;
		ConnectDB db = new ConnectDB();
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = db.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time before calculating State Taxes: "+ currentVirtualTime
		// Getting maximum invoice number from db
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		logi.logInfo "Invoice No : "+invoiceNo
		// Getting tax rate from db 
		String taxRate = db.executeQueryP2("SELECT SUM(DISTINCT tax_rate) FROM ARIACORE.GL_TAX_DETAIL WHERE invoice_no = "+ invoiceNo)
		logi.logInfo "TaxRate : "+taxRate
		// Getting the plan(s) involved for billing
		HashMap<String, HashMap<String, String>> plans = getPlanServiceAndTieredPricingByInvoice(invoiceNo)
		Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
		resultHash.putAll(plans)
		logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()

		Iterator iterator = resultHash.entrySet().iterator()
		logi.logInfo "Before Key setting "
		// Iterating through Plans
		while (iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) iterator.next()
			String planNo = pairs.getKey().toString()
			logi.logInfo "Execution for the plan :  "+ planNo
			String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
			int planTypeInd = Integer.parseInt(db.executeQueryP2(planTypeQuery))
			logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo

			int noOfUnits =0
			// Checking for the type of plan and getting no of units
			if(planTypeInd==0) {
				logi.logInfo "Master Plan : "+ planNo
				String newQuery = "SELECT PLAN_UNITS FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO = " + accountNo
				noOfUnits = Integer.parseInt(db.executeQueryP2(newQuery))
			}
			else {
				logi.logInfo "Supp Plan : "+ planNo
				String[] noOfUnitArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
				String[] suppPlans = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")

				if (!suppPlans.toString().equalsIgnoreCase("null")) {
					logi.logInfo("Array of supp plan")
					logi.logInfo "supp_plans :::  " + suppPlans.toString()
					logi.logInfo "noOfUnits :::  " + noOfUnitArray.toString()
					if(Arrays.asList(suppPlans).contains(planNo)){
						for( int j=0;j<suppPlans.length;j++) {
							if(suppPlans[j].toString().equalsIgnoreCase(planNo)){
								noOfUnits = Integer.parseInt(noOfUnitArray[j])
								logi.logInfo "no_of_units : "+noOfUnits+ " plan : " +planNo
								break
							}
						}
					}
				}
				else {
					logi.logInfo("single supp plan")
					String suppPlan = getValueFromRequest("create_acct_complete","//supp_plans").toString()
					if ((!suppPlan.toString().equalsIgnoreCase("null"))&& suppPlan.equalsIgnoreCase(planNo)) {
						noOfUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))
						
						String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
						int noOfUnits2 = Integer.parseInt(db.executeQueryP2(supp_plan_unit_query))
						
						if(noOfUnits2 > noOfUnits) {
							noOfUnits = noOfUnits2
						}
						
						logi.logInfo "single_supp_plan :::  " + suppPlan
						logi.logInfo "single_plan_noOfUnits :::  " + noOfUnits
					}
					else {
						logi.logInfo "assign or replace supp plan"
						String supp_plan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
						if(supp_plan.equalsIgnoreCase(planNo))
							noOfUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
						else{
							supp_plan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
							if(supp_plan.equalsIgnoreCase(planNo))
								noOfUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
							else {
								String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
								noOfUnits = Integer.parseInt(db.executeQueryP2(supp_plan_unit_query))
							}
						}
					}
				}
			}

			logi.logInfo "No of Units : "+noOfUnits+ " of a plan : " +planNo

			Map<String,String> valueMap = pairs.getValue()
			Map<String,String> value = new TreeMap<String,String> ()
			value.putAll(valueMap)
			Iterator service = value.entrySet().iterator()
			logi.logInfo "Invoice calculation for a plan : "+planNo
			// Iterating through Plans
			while(service.hasNext()){
				Map.Entry servicePairs = (Map.Entry) service.next()
				String serviceNo = servicePairs.getKey().toString()
				logi.logInfo "Invoice calculation for a service : "+serviceNo
				String isServiceTaxableQuery = "SELECT TAXABLE_IND from ariacore.all_service where service_no = " +serviceNo
				String isServiceTaxable = db.executeQueryP2(isServiceTaxableQuery)
				logi.logInfo "Service "+ serviceNo+" taxable/not : "+isServiceTaxable
				String zeroDollarPlanCheckQuery = "select distinct trunc(sum(rate_per_unit)) as RatePerUnit from ariacore.new_rate_sched_rates where schedule_no in (select schedule_no from ariacore.plan_rate_sched_map where plan_no = "+planNo+" and client_no="+clientNo+") and service_no = "+serviceNo+" and client_no="+clientNo
				int isZeroDollar = Integer.parseInt(db.executeQueryP2(zeroDollarPlanCheckQuery))
				logi.logInfo "Service "+ serviceNo+" Zero Dollar/not : "+isZeroDollar

				//Checking the service is taxable and non-zero dollar plan or not
				if(isServiceTaxable.equalsIgnoreCase("1") && (isZeroDollar!=0)){
					logi.logInfo "Service is taxable : "+serviceNo
					int seqNo=1;
					String tieredPricing = servicePairs.getValue().toString()
					int tieredPricingInt = Integer.parseInt(tieredPricing)
					logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
					int serviceInvoice
					String serviceInvoiceTaxInclusive
					String query
					int upgraded_units
					if(testCaseId.contains("update_master_plan")){
						String units_query = "select recurring_factor,rate_per_unit from ariacore.acct_rates_history_details where acct_no = " + accountNo + " and client_no = " + clientNo + " and plan_no = " + planNo + " and service_no=" + serviceNo + " and (" + noOfUnits + " between from_unit and to_unit)"
						ResultSet rs = db.executePlaQuery(units_query)
						int max_units=noOfUnits, min_units=0
						while(rs.next()){
							int db_units = Integer.parseInt(rs.getString(1))
							if(db_units >= max_units) {
								max_units = db_units
							}
							else {
								min_units = db_units
							}
							if(Integer.parseInt(rs.getString(2)) == 0)
								max_units=min_units=0
						}
						upgraded_units = max_units-min_units
						logi.logInfo "Units Difference : " + upgraded_units
					}
					switch(tieredPricingInt){
						case 1:
							logi.logInfo "Case 1"
							if(testCaseId.contains("update_master_plan"))
								query = "SELECT NRSR.RATE_PER_UNIT * " + upgraded_units + " as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
							else
								query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
							seqNo++;
							break;
						case 2 :
							logi.logInfo "Case 2"
							if(testCaseId.contains("update_master_plan"))
								query ="select rate_per_unit*" + upgraded_units + " from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
							else
								query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
							break;
						case 3 :
							logi.logInfo "Case 3"
							if(testCaseId.contains("update_master_plan"))
							query = "select " + upgraded_units + "*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
							else
								query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
							break;
					}
					serviceInvoice = Integer.parseInt(db.executeQueryP2(query))
					serviceInvoiceTaxInclusive = md_serviceAmountAfterTaxInclusive(taxRate,serviceInvoice.toString())
					logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
					invoice  = invoice.toDouble()+serviceInvoiceTaxInclusive.toDouble()
					logi.logInfo "After adding with invoice " + invoice

				}
				else
					logi.logInfo "Service is not taxable : "+ serviceNo
			}
		}
		
		logi.logInfo "Service amount return : "+ invoice.toDouble().trunc(2).toString()
		return invoice.toDouble().trunc(2).toString()
	}

	/**
	 * Calculates Invoice with difference and tax inclusive reduced
	 * @param testCaseId, tax rate
	 * @return Invoice amount
	 */
	String md_serviceInvoiceAmountDifferenceTaxInclusive(String testCaseId, String taxRate) {
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
		logi.logInfo "testId : "+ testId
		def invoice = 0
		String invoiceNo;
		ConnectDB db = new ConnectDB();
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = db.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time before calculating State Taxes: "+ currentVirtualTime
		// Getting maximum invoice number from db
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		logi.logInfo "Invoice No : "+invoiceNo
		// Getting the plan(s) involved for billing
		HashMap<String, HashMap<String, String>> plans = getPlanServiceAndTieredPricingByInvoice(invoiceNo)
		Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
		resultHash.putAll(plans)
		logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()

		Iterator iterator = resultHash.entrySet().iterator()
		logi.logInfo "Before Key setting "
		// Iterating through Plans
		while (iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) iterator.next()
			String planNo = pairs.getKey().toString()
			logi.logInfo "Execution for the plan :  "+ planNo
			String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
			int planTypeInd = Integer.parseInt(db.executeQueryP2(planTypeQuery))
			logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo

			int noOfUnits =0
			// Checking for the type of plan and getting no of units
			if(planTypeInd==0) {
				logi.logInfo "Master Plan : "+ planNo
				String newQuery = "SELECT PLAN_UNITS FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO = " + accountNo
				noOfUnits = Integer.parseInt(db.executeQueryP2(newQuery))
			}
			else {
				logi.logInfo "Supp Plan : "+ planNo
				String[] noOfUnitArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
				String[] suppPlans = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")

				if (!suppPlans.toString().equalsIgnoreCase("null")) {
					logi.logInfo("Array of supp plan")
					logi.logInfo "supp_plans :::  " + suppPlans.toString()
					logi.logInfo "noOfUnits :::  " + noOfUnitArray.toString()
					if(Arrays.asList(suppPlans).contains(planNo)){
						for( int j=0;j<suppPlans.length;j++) {
							if(suppPlans[j].toString().equalsIgnoreCase(planNo)){
								noOfUnits = Integer.parseInt(noOfUnitArray[j])
								logi.logInfo "no_of_units : "+noOfUnits+ " plan : " +planNo
								break
							}
						}
					}
				}
				else {
					logi.logInfo("single supp plan")
					String suppPlan = getValueFromRequest("create_acct_complete","//supp_plans").toString()
					if ((!suppPlan.toString().equalsIgnoreCase("null"))&& suppPlan.equalsIgnoreCase(planNo)) {
						noOfUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))

						logi.logInfo "single_supp_plan :::  " + suppPlan
						logi.logInfo "single_plan_noOfUnits :::  " + noOfUnits
					}
					else {
						logi.logInfo "assign or replace supp plan"
						String supp_plan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
						if(supp_plan.equalsIgnoreCase(planNo))
							noOfUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
						else{
							supp_plan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
							if(supp_plan.equalsIgnoreCase(planNo))
								noOfUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
							else {
								String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
								noOfUnits = Integer.parseInt(db.executeQueryP2(supp_plan_unit_query))
							}
						}
					}
				}
			}

			logi.logInfo "No of Units : "+noOfUnits+ " of a plan : " +planNo

			Map<String,String> valueMap = pairs.getValue()
			Map<String,String> value = new TreeMap<String,String> ()
			value.putAll(valueMap)
			Iterator service = value.entrySet().iterator()
			logi.logInfo "Invoice calculation for a plan : "+planNo
			// Iterating through Plans
			while(service.hasNext()){
				Map.Entry servicePairs = (Map.Entry) service.next()
				String serviceNo = servicePairs.getKey().toString()
				logi.logInfo "Invoice calculation for a service : "+serviceNo
				String isServiceTaxableQuery = "SELECT TAXABLE_IND from ariacore.all_service where service_no = " +serviceNo
				String isServiceTaxable = db.executeQueryP2(isServiceTaxableQuery)
				logi.logInfo "Service "+ serviceNo+" taxable/not : "+isServiceTaxable
				String zeroDollarPlanCheckQuery = "select distinct trunc(sum(rate_per_unit)) as RatePerUnit from ariacore.new_rate_sched_rates where schedule_no in (select schedule_no from ariacore.plan_rate_sched_map where plan_no = "+planNo+" and client_no="+clientNo+") and service_no = "+serviceNo+" and client_no="+clientNo
				int isZeroDollar = Integer.parseInt(db.executeQueryP2(zeroDollarPlanCheckQuery))
				logi.logInfo "Service "+ serviceNo+" Zero Dollar/not : "+isZeroDollar

				//Checking the service is taxable and non-zero dollar plan or not
				if(isServiceTaxable.equalsIgnoreCase("1") && (isZeroDollar!=0)){
					logi.logInfo "Service is taxable : "+serviceNo
					int seqNo=1;
					String tieredPricing = servicePairs.getValue().toString()
					int tieredPricingInt = Integer.parseInt(tieredPricing)
					logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
					int serviceInvoice
					String serviceInvoiceTaxInclusive
					String query
					switch(tieredPricingInt){
						case 1:
							logi.logInfo "Case 1"
							query = "SELECT NRSR.RATE_PER_UNIT * "+noOfUnits+" as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
							seqNo++;
							break;
						case 2 :
							logi.logInfo "Case 2"
							query ="select rate_per_unit * "+noOfUnits+" from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
							break;
						case 3 :
							logi.logInfo "Case 3"
							query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
							break;
					}
					serviceInvoice = Integer.parseInt(db.executeQueryP2(query))
					logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
					serviceInvoiceTaxInclusive = md_serviceAmountAfterTaxInclusive_Difference(invoiceNo,serviceInvoice.toString())
					logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
					invoice  = invoice.toDouble()+serviceInvoiceTaxInclusive.toDouble()
					logi.logInfo "After adding with invoice " + invoice

				}

				else
					logi.logInfo "Service is not taxable : "+ serviceNo
			}
		}
		return invoice.trunc(2).toString()
	}
	
	/**
	 * Calculates Invoice with credit including coupons (Single coupons)
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_serviceInvoiceAmountIncludingCouponReduction(String testCaseId) {
		logi.logInfo "Inside md_serviceInvoiceAmountIncludingCouponReduction"
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
		logi.logInfo "testId : "+ testId
		def invoice = 0
		String invoiceNo;
		ConnectDB db = new ConnectDB();
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = db.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time before calculating State Taxes: "+ currentVirtualTime
		// Getting maximum invoice number from db
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		logi.logInfo "Invoice No : "+invoiceNo
		// Getting the plan(s) involved for billing
		HashMap<String, HashMap<String, String>> plans = getPlanServiceAndTieredPricingByInvoice(invoiceNo)
		Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
		resultHash.putAll(plans)
		logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()

		// Getting coupon code from request
		logi.logInfo "Test Case Id : : " + testCaseId
		String apiName=testCaseId.split("-")[2]
		logi.logInfo "ApI Name : : " + apiName
		if(apiName.contains("^")){
			logi.logInfo "ApI Name inside : : " + apiName
			apiName=apiName.substring(apiName.indexOf('^')+1)
			logi.logInfo "ApI Name out : : " + apiName
		}
		logi.logInfo "ApI Name : : " + apiName
		String couponCode = getValueFromRequest(apiName,"//coupon_code").toString()
		// Getting Coupon applied Service no from DB
		String couponService = db.executeQueryP2("SELECT key_1_val FROM ariacore.client_dr_components WHERE rule_no = (SELECT rule_no FROM ariacore.coupon_discount_rule_map WHERE Coupon_cd = '"+couponCode+"' AND client_no = "+ clientNo+")")
		logi.logInfo "CouponService : " + couponService
		Iterator iterator = resultHash.entrySet().iterator()
		logi.logInfo "Before Key setting "
		// Iterating through Plans
		while (iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) iterator.next()
			String planNo = pairs.getKey().toString()
			logi.logInfo "Execution for the plan :  "+ planNo
			String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
			int planTypeInd = Integer.parseInt(db.executeQueryP2(planTypeQuery))
			logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo

			int noOfUnits =0
			// Checking for the type of plan and getting no of units
			if(planTypeInd==0) {
				logi.logInfo "Master Plan : "+ planNo
				String newQuery = "SELECT PLAN_UNITS FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO = " + accountNo
				noOfUnits = Integer.parseInt(db.executeQueryP2(newQuery))
			}
			else {
				logi.logInfo "Supp Plan : "+ planNo
				String[] noOfUnitArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
				String[] suppPlans = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")

				if (!suppPlans.toString().equalsIgnoreCase("null")) {
					logi.logInfo("Array of supp plan")
					logi.logInfo "supp_plans :::  " + suppPlans.toString()
					logi.logInfo "noOfUnits :::  " + noOfUnitArray.toString()
					if(Arrays.asList(suppPlans).contains(planNo)){
						for( int j=0;j<suppPlans.length;j++) {
							if(suppPlans[j].toString().equalsIgnoreCase(planNo)){
								noOfUnits = Integer.parseInt(noOfUnitArray[j])
								logi.logInfo "no_of_units : "+noOfUnits+ " plan : " +planNo
								break
							}
						}
					}
				}
				else {
					logi.logInfo("single supp plan")
					String suppPlan = getValueFromRequest("create_acct_complete","//supp_plans").toString()
					if ((!suppPlan.toString().equalsIgnoreCase("null"))&& suppPlan.equalsIgnoreCase(planNo)) {
						noOfUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))

						logi.logInfo "single_supp_plan :::  " + suppPlan
						logi.logInfo "single_plan_noOfUnits :::  " + noOfUnits
					}
					else {
						logi.logInfo "assign or replace supp plan"
						String supp_plan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
						if(supp_plan.equalsIgnoreCase(planNo))
							noOfUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
						else{
							supp_plan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
							if(supp_plan.equalsIgnoreCase(planNo))
								noOfUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
							else {
								String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
								noOfUnits = Integer.parseInt(db.executeQueryP2(supp_plan_unit_query))
							}
						}
					}
				}
			}

			logi.logInfo "No of Units : "+noOfUnits+ " of a plan : " +planNo

			Map<String,String> valueMap = pairs.getValue()
			Map<String,String> value = new TreeMap<String,String> ()
			value.putAll(valueMap)
			Iterator service = value.entrySet().iterator()
			logi.logInfo "Invoice calculation for a plan : "+planNo
			// Iterating through Plans
			while(service.hasNext()){
				Map.Entry servicePairs = (Map.Entry) service.next()
				String serviceNo = servicePairs.getKey().toString()
				logi.logInfo "Invoice calculation for a service : "+serviceNo
				String isServiceTaxableQuery = "SELECT TAXABLE_IND from ariacore.all_service where service_no = " +serviceNo
				String isServiceTaxable = db.executeQueryP2(isServiceTaxableQuery)
				logi.logInfo "Service "+ serviceNo+" taxable/not : "+isServiceTaxable
				String zeroDollarPlanCheckQuery = "select distinct trunc(sum(rate_per_unit)) as RatePerUnit from ariacore.new_rate_sched_rates where schedule_no in (select schedule_no from ariacore.plan_rate_sched_map where plan_no = "+planNo+" and client_no="+clientNo+") and service_no = "+serviceNo+" and client_no="+clientNo
				int isZeroDollar = Integer.parseInt(db.executeQueryP2(zeroDollarPlanCheckQuery))
				logi.logInfo "Service "+ serviceNo+" Zero Dollar/not : "+isZeroDollar

				//Checking the service is taxable and non-zero dollar plan or not
				if(isServiceTaxable.equalsIgnoreCase("1") && (isZeroDollar!=0)){
					logi.logInfo "Service is taxable : "+serviceNo
					int seqNo=1;
					String tieredPricing = servicePairs.getValue().toString()
					int tieredPricingInt = Integer.parseInt(tieredPricing)
					logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
					int serviceInvoice
					String serviceInvoiceTaxInclusive
					String query
					switch(tieredPricingInt){
						case 1:
							logi.logInfo "Case 1"
							query = "SELECT NRSR.RATE_PER_UNIT * "+noOfUnits+" as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
							seqNo++;
							break;
						case 2 :
							logi.logInfo "Case 2"
							query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
							break;
						case 3 :
							logi.logInfo "Case 3"
							query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
							break;
					}
					serviceInvoice = Integer.parseInt(db.executeQueryP2(query))
					logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
					if(couponService.equalsIgnoreCase(serviceNo)){
						char couponType = db.executeQueryP2("SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (SELECT rule_no FROM ariacore.coupon_discount_rule_map WHERE Coupon_cd = '"+couponCode+"' AND client_no = "+ clientNo+")")
						logi.logInfo "CouponType : " + couponType
						if(couponType=='F'){
							int couponAmount = Integer.parseInt(db.executeQueryP2("SELECT amount FROM ariacore.client_discount_rules WHERE RULE_NO = (SELECT rule_no FROM ariacore.coupon_discount_rule_map WHERE Coupon_cd = '"+couponCode+"' AND client_no = "+ clientNo+")"))
							logi.logInfo "CouponAmount : " + couponAmount
						serviceInvoice = serviceInvoice - couponAmount
						}
					}
					invoice  = invoice.toDouble()+serviceInvoice.toDouble()
					logi.logInfo "After adding with invoice " + invoice
				}

				else
					logi.logInfo "Service is not taxable : "+ serviceNo
			}
		}
		logi.logInfo "Before Service Credit Amount Deduction : " + invoice
		// Calculating auto service credit amount to deduct from service amount
		double creditAmount = md_CREATE_ORDER_INVOICE_AMT(testCaseId).toDouble()
		logi.logInfo "Service Credit Amount : " + creditAmount
		invoice  = invoice.toDouble()- creditAmount
		logi.logInfo "After Service Credit Amount Deduction : " + invoice
		return invoice.trunc(2).toString()
	}
	
	/**
	 * Calculates Invoice for available plans and services
	 * @param testCaseId
	 * @return Invoice amount
	 */
	def md_serviceInvoiceAmount(String testCaseId) {
		String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
		logi.logInfo "testId : "+ testId
		def invoice = 0
		String invoiceNo;
		ConnectDB db = new ConnectDB();
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = db.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time before calculating State Taxes: "+ currentVirtualTime
		// Getting maximum invoice number from db
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		logi.logInfo "Invoice No : "+invoiceNo
		// Getting the plan(s) involved for billing
		HashMap<String, HashMap<String, String>> plans = getPlanServiceAndTieredPricingByInvoice(invoiceNo)
		Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
		resultHash.putAll(plans)
		logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()

		Iterator iterator = resultHash.entrySet().iterator()
		logi.logInfo "Before Key setting "
		
		String external_tax_client = "SELECT METHOD_CD FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO=" + clientNo
		external_tax_client = db.executeQueryP2(external_tax_client)
		logi.logInfo("External Client ID = " +external_tax_client)
		
		// Iterating through Plans
		while (iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) iterator.next()
			String planNo = pairs.getKey().toString()
			logi.logInfo "Execution for the plan :  "+ planNo
			String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
			int planTypeInd = Integer.parseInt(db.executeQueryP2(planTypeQuery))
			logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo

			int noOfUnits =0
			// Checking for the type of plan and getting no of units
			if(planTypeInd==0) {
				logi.logInfo "Master Plan : "+ planNo
				String newQuery = "SELECT PLAN_UNITS FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO = " + accountNo
				noOfUnits = Integer.parseInt(db.executeQueryP2(newQuery))
			}
			else {
				logi.logInfo "Supp Plan : "+ planNo
				String[] noOfUnitArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
				String[] suppPlans = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")

				if (!suppPlans.toString().equalsIgnoreCase("null")) {
					logi.logInfo("Array of supp plan")
					logi.logInfo "supp_plans :::  " + suppPlans.toString()
					logi.logInfo "noOfUnits :::  " + noOfUnitArray.toString()
					if(Arrays.asList(suppPlans).contains(planNo)){
						for( int j=0;j<suppPlans.length;j++) {
							if(suppPlans[j].toString().equalsIgnoreCase(planNo)){
								noOfUnits = Integer.parseInt(noOfUnitArray[j])
								logi.logInfo "no_of_units : "+noOfUnits+ " plan : " +planNo
								break
							}
						}
					}
				}
				else {
					logi.logInfo("single supp plan")
					String suppPlan = getValueFromRequest("create_acct_complete","//supp_plans").toString()
					if ((!suppPlan.toString().equalsIgnoreCase("null"))&& suppPlan.equalsIgnoreCase(planNo)) {
						noOfUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))
						
						String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
						int noOfUnits2 = Integer.parseInt(db.executeQueryP2(supp_plan_unit_query))
						
						if(noOfUnits2 > noOfUnits) {
							noOfUnits = noOfUnits2
						}
						
						logi.logInfo "single_supp_plan :::  " + suppPlan
						logi.logInfo "single_plan_noOfUnits :::  " + noOfUnits
					}
					else {
						logi.logInfo "assign or replace supp plan"
						String supp_plan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
						if(supp_plan.equalsIgnoreCase(planNo))
							noOfUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
						else{
							supp_plan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
							if(supp_plan.equalsIgnoreCase(planNo))
								noOfUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
							else {
								String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
								noOfUnits = Integer.parseInt(db.executeQueryP2(supp_plan_unit_query))
							}
						}
					}
				}
			}

			logi.logInfo "No of Units : "+noOfUnits+ " of a plan : " +planNo

			Map<String,String> valueMap = pairs.getValue()
			Map<String,String> value = new TreeMap<String,String> ()
			value.putAll(valueMap)
			Iterator service = value.entrySet().iterator()
			logi.logInfo "Invoice calculation for a plan : "+planNo
			// Iterating through Plans
			while(service.hasNext()){
				Map.Entry servicePairs = (Map.Entry) service.next()
				String serviceNo = servicePairs.getKey().toString()
				logi.logInfo "Invoice calculation for a service : "+serviceNo
				String isServiceTaxableQuery = "SELECT TAXABLE_IND from ariacore.all_service where service_no = " +serviceNo
				String isServiceTaxable = db.executeQueryP2(isServiceTaxableQuery)
				logi.logInfo "Service "+ serviceNo+" taxable/not : "+isServiceTaxable
				String zeroDollarPlanCheckQuery = "select distinct trunc(sum(rate_per_unit)) as RatePerUnit from ariacore.new_rate_sched_rates where schedule_no in (select schedule_no from ariacore.plan_rate_sched_map where plan_no = "+planNo+" and client_no="+clientNo+") and service_no = "+serviceNo+" and client_no="+clientNo
				int isZeroDollar = Integer.parseInt(db.executeQueryP2(zeroDollarPlanCheckQuery))
				logi.logInfo "Service "+ serviceNo+" Zero Dollar/not : "+isZeroDollar

				//Checking the service is taxable and non-zero dollar plan or not
				//if(isServiceTaxable.equalsIgnoreCase("1") && (isZeroDollar!=0)){
					logi.logInfo "Service is taxable : "+serviceNo
					int seqNo=1;
					String tieredPricing = servicePairs.getValue().toString()
					int tieredPricingInt = Integer.parseInt(tieredPricing)
					logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
					def serviceInvoice
					String query
					int upgraded_units
					if(testCaseId.contains("update_master_plan")) {
						String units_query = "select recurring_factor,rate_per_unit from ariacore.acct_rates_history_details where acct_no = " + accountNo + " and client_no = " + clientNo + " and plan_no = " + planNo + " and service_no=" + serviceNo + " and (" + noOfUnits + " between from_unit and to_unit)"
						ResultSet rs = db.executePlaQuery(units_query)
						int max_units=noOfUnits, min_units=0
						while(rs.next()){
							int db_units = Integer.parseInt(rs.getString(1))
							if(db_units >= max_units) {
								max_units = db_units
							}
							else {
								min_units = db_units
							}
							if(Integer.parseInt(rs.getString(2)) == 0)
								max_units=min_units=0
						}
						upgraded_units = max_units-min_units
						logi.logInfo "Units Difference : " + upgraded_units
					}
					switch(tieredPricingInt){
						case 1:
							logi.logInfo "Case 1"
							if((testCaseId.contains("update_master_plan")) && (!testCaseId.contains("^update_master_plan")))
								query = "SELECT NRSR.RATE_PER_UNIT * " + upgraded_units + " as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
							//else if ((testCaseId.contains("update_acct_complete")) && (!testCaseId.contains("^update_acct_complete")))
							//	query = "SELECT NRSR.RATE_PER_UNIT * " + (prorationFactor*noOfUnits) + " as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
							else
								query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
							seqNo++;
							break;
						case 2 :
							logi.logInfo "Case 2"
							if((testCaseId.contains("update_master_plan")) && (!testCaseId.contains("^update_master_plan")))
								query ="select rate_per_unit*" + upgraded_units +" from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
							//else if ((testCaseId.contains("update_acct_complete")) && (!testCaseId.contains("^update_acct_complete")))
							//	query = "select rate_per_unit*" + (prorationFactor*noOfUnits) + " from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
							else
								//query ="select NVL(SUM(rate_per_unit*recurring_factor),0) from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
								query = "SELECT case when sum(a.act_debit) is null then 0 else sum(a.act_debit) end as res from (SELECT (rate_per_unit*recurring_factor) AS act_debit FROM ariacore.acct_rates_history_details WHERE acct_no = "+accountNo+" AND client_no = "+clientNo+" AND service_no = "+serviceNo+" AND plan_no = "+planNo+" AND ("+noOfUnits+" BETWEEN from_unit AND to_unit) AND rownum = 1 ORDER BY rate_hist_seq_no DESC)a"
							break;
						case 3 :
							logi.logInfo "Case 3"
							if((testCaseId.contains("update_master_plan")) && (!testCaseId.contains("^update_master_plan")))
								query = "select 1*" + upgraded_units +" from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
							//else if ((testCaseId.contains("update_acct_complete")) && (!testCaseId.contains("^update_acct_complete")))
							//	query = "select 1*" + (prorationFactor*noOfUnits) + " from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
							else
								query = "SELECT case when sum(a.act_debit) is null then 0 else sum(a.act_debit) end as res from (SELECT rate_per_unit AS act_debit FROM ariacore.acct_rates_history_details WHERE acct_no = "+accountNo+" AND client_no = "+clientNo+" AND service_no = "+serviceNo+" AND plan_no = "+planNo+" AND ("+noOfUnits+" BETWEEN from_unit AND to_unit) AND rownum = 1 ORDER BY rate_hist_seq_no DESC)a"
							break;
					}
					logi.logInfo "Before adding with invoice " + invoice
					serviceInvoice = (db.executeQueryP2(query)).toString().toDouble();
					logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
					
					if (external_tax_client == "6" ) // Vertex Tax Method
					{
					logi.logInfo "Entered Tax Line for Vertex "
					String taxrate = "SELECT TAX_RATE FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO = "+invoiceNo+ "AND ROWNUM = 1"
					taxrate = db.executeQueryP2(taxrate)
					serviceInvoice  = serviceInvoice.toDouble()/(taxrate.toDouble()+1)
					logi.logInfo "After adding with Tax Service Amount = " + invoice
					}
					
					invoice  = invoice.toDouble()+serviceInvoice.toDouble()
					logi.logInfo "After adding with invoice " + invoice
				//}

				//else
					//logi.logInfo "Service is not taxable : "+ serviceNo
			}
		}
		DecimalFormat df=new DecimalFormat("#.##")
		return df.format(invoice.toDouble()).toString();
	}


	public HashMap getPlanServiceAndTieredPricingByInvoice(String invoiceNum){
		logi.logInfo "Inside getPlanServiceAndTieredPricingByInvoice "
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		logi.logInfo "Invoice : "+invoiceNum
		String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select plan_no from ariacore.GL_DETAIL where INVOICE_NO="+invoiceNum+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and recurring = 1) order by plan_no, service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select plan_no from ariacore.GL_DETAIL where INVOICE_NO="+invoiceNum+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and recurring = 1) order by plan_no, service_no"))
		ResultSet resultSet = database.executeQuery(query)

		HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		HashMap<String,String> serviceHash

		String[] plan = new String[count]
		String[] service = new String[count]
		String[] tieredPricing = new String[count]

		int i= 0
		while(resultSet.next()){
			plan[i]=resultSet.getString(1)
			service[i]=resultSet.getString(2)
			tieredPricing[i]=resultSet.getString(3)
			i++
		}
		String plan_no
		int serviceCount=0
		for(int planCount=0;planCount<plan.length;){
			plan_no = plan[planCount]
			serviceHash = new HashMap<String,String> ()
			for(;serviceCount<service.length;serviceCount++){
				if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					planCount ++
				}
				else
					break
			}
			resultHash.put(plan_no, serviceHash)
			logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
		}
		logi.logInfo "Total Services for Proration : " + resultHash.toString()
		return resultHash
	}

	/**
	 * Calculates Federal and state taxes for the first invoice
	 * @param testCaseId
	 * @return tax amount
	 */
	String md_FEDERAL_STATE_TAX_FIRST_INVOICE(String testCaseId){
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		String invoice_no=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		String invoice_without_activation = db.executeQueryP2("select sum(debit) from ariacore.gl_detail where invoice_no=" + invoice_no + " and proration_factor=1")
		String federal_taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no = " + clientNo)
		String state_taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no = " + clientNo)

		double federal_taxAmount = invoice_without_activation.toDouble() * federal_taxRate.toDouble();
		double state_taxAmount = invoice_without_activation.toDouble() * state_taxRate.toDouble();
		double taxAmount = federal_taxAmount + state_taxAmount;
		DecimalFormat df = new DecimalFormat("#.##");

		return df.format(taxAmount).toString();
	}
	
	/**
	 * Calculates prorated amount from credit
	 * @param testCaseId
	 * @return applied amount
	 */
	String md_proration_result_amount(String testCaseId){
		logi.logInfo('Inside md_proration_result_amount ' )
		ConnectDB db=new ConnectDB()
        String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		String result
		int credit_cnt = db.executeQueryP2("Select count(distinct credit_id) from ariacore.all_credits WHERE acct_no ="+accountNo).toInteger()
		if(credit_cnt > 1)
			result = db.executeQueryP2("SELECT SUM(amount) - (SELECT amount FROM ariacore.all_credits where acct_no = "+accountNo+" AND credit_id  = (select MIN(credit_id) from  ariacore.all_credits where acct_no = "+accountNo+" )) as amount FROM ariacore.all_credits WHERE acct_no = "+accountNo+" group by acct_no")
		else
			result = db.executeQueryP2("Select NVL(sum(amount),0) FROM ariacore.all_credits where acct_no ="+accountNo+" and credit_id= (SELECT case when MIN(credit_id) is null then  0 else MIN(credit_id) end  FROM ariacore.all_credits WHERE acct_no ="+accountNo+")").toString()
		
		if(result.equals("null"))
			result=0
		return "-"+result
	}

	String md_VERIFY_SUPP_PLAN_EVENT_112_P2(String testCaseId) {
		return md_VERIFY_SUPP_PLAN_EVENTP2(testCaseId, "112")
	}


	String md_VERIFY_SUPP_PLAN_EVENTP2(String testCaseId, String event) {
		logi.logInfo('Inside md_VERIFY_SUPP_PLAN_EVENTP2 ' )
		String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String result =  db.executeQueryP2("SELECT EVENT_ID FROM ARIACORE.ACCT_PROV2_EVENTS WHERE PROV_SEQ IN (SELECT PROV_SEQ FROM ARIACORE.ACCT_PROV2_STEPS  WHERE ACCT_NO=" + accountNo + " AND CLIENT_NO=" + clientNo + ") and EVENT_ID = "+ event + " GROUP BY EVENT_ID")
		return result
	}

	public String md_GetDateFromAccountCreation(String testCaseId) {
		logi.logInfo(" Inside md_GetDateFromAccountCreation " )

		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')

		Calendar now = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

		Date acctStartDate = getBillStartDate(clientNo, accountNo)
		now.setTime(acctStartDate);

		String date1 = dateFormat.format(now.getTime());
		return date1;
	}

	/**
	 * Calculates Invoice with Tax and credit reduced for external clients
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoice_ExternalClient_TaxInclusive_CR_Yes(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoice_ExternalClient_TaxInclusive_CR_Yes"
		// Calculating service, federal and state tax amount
		String serviceInvoiceAmount = md_serviceInvoiceAmountTaxInclusive(testCaseId)
		logi.logInfo 'Service : amount : ' + serviceInvoiceAmount
		int serviceInvoiceActivationAmount = md_serviceInvoiceActivationAmount(testCaseId)
		double stateTax =  md_STATE_TAX_INCLUSIVE_P2(testCaseId).toDouble()
		double cityTax = md_CITY_TAX_INCLUSIVE_P2(testCaseId).toDouble()
		double districtTax = md_DISTRICT_TAX_INCLUSIVE_P2(testCaseId).toDouble()
		double creditAmount = md_CREATE_ORDER_INVOICE_AMT(testCaseId).toDouble()
		// Adding Service (Recurring and Activation), State, City, District amount to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount   +' ServiceActivation Amount : ' + serviceInvoiceActivationAmount + " CurrentStateTax : "+ stateTax + " CurrentCityTax : "+cityTax + " CurrentDistrictTax : "+districtTax 
		double totalInvoice = serviceInvoiceAmount.toDouble()  +serviceInvoiceActivationAmount.toDouble() + stateTax +cityTax +  districtTax - creditAmount
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice.round()).toString();
	}
	
	/**
	 * Calculates Invoice with Tax and credit reduced for external clients including activation
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoice_ExternalClient_Credit_Activation_Inclusive_TaxInclusive_CR_Yes(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoice_ExternalClient_Credit_Activation_Inclusive_TaxInclusive_CR_Yes"
		// Calculating service, federal and state tax amount
		String serviceInvoiceAmount = md_serviceInvoiceAmountTaxInclusive(testCaseId)
		logi.logInfo 'Service : amount : ' + serviceInvoiceAmount
		int serviceInvoiceActivationAmount = md_serviceInvoiceActivationAmount(testCaseId)
		double stateTax =  md_STATE_TAX_INCLUSIVE_P2(testCaseId).toDouble()
		double cityTax = md_CITY_TAX_INCLUSIVE_P2(testCaseId).toDouble()
		double districtTax = md_DISTRICT_TAX_INCLUSIVE_P2(testCaseId).toDouble()
//		double creditAmount = md_CREATE_ORDER_INVOICE_AMT(testCaseId).toDouble()
		int creditAmount = Integer.parseInt(md_CREDIT_AMT_BY_CREDITID(testCaseId))
		logi.logInfo 'Credit Amount in ExternalClient_TaxInclusive: ' + creditAmount
		// Adding Service (Recurring and Activation), State, City, District amount to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount   +' ServiceActivation Amount : ' + serviceInvoiceActivationAmount + " CurrentStateTax : "+ stateTax + " CurrentCityTax : "+cityTax + " CurrentDistrictTax : "+districtTax
		double totalInvoice = serviceInvoiceAmount.toDouble()  +serviceInvoiceActivationAmount.toDouble() + stateTax +cityTax +  districtTax - creditAmount
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice.round()).toString();
	}
	
	/**
	 * Calculates Invoice with Tax and credit reduced for external clients including service credits
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoice_ExternalClient_TaxInclusive_Usage_ServiceCredit_CR_Yes(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoice_ExternalClient_TaxInclusive_Usage_ServiceCredit_CR_Yes"
		// Calculating service, federal and state tax amount
		String serviceInvoiceAmount = md_serviceInvoiceAmountTaxInclusive(testCaseId)
		double stateTax =  md_STATE_TAX_INCLUSIVE_P2(testCaseId).toDouble()
		double cityTax = md_CITY_TAX_INCLUSIVE_P2(testCaseId).toDouble()
		double districtTax = md_DISTRICT_TAX_INCLUSIVE_P2(testCaseId).toDouble()
		
		// Calculating deduction amount
		int creditAmount = Integer.parseInt(md_CREDIT_AMT_BY_CREDITID(testCaseId))
		logi.logInfo 'Credit Amount in ExternalClient_TaxInclusive: ' + creditAmount
		// Calculating usage amount
		UsageVerificationMethods usageMethods = new UsageVerificationMethods()
		double usageAmount = usageMethods.md_ExpUsage_Amt_With_Auto_Rate(testCaseId).toDouble()
		
		// Adding Service (Recurring), Usage, Service Credits, State, City, District taxes amount to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount   +' Usage Amount : ' + usageAmount + " CurrentStateTax : "+ stateTax + " CurrentCityTax : "+cityTax + " CurrentDistrictTax : "+districtTax + " Credit Amount : "+ creditAmount
		double totalInvoice = serviceInvoiceAmount.toDouble()  +usageAmount.toDouble() + stateTax +cityTax +  districtTax - creditAmount.toDouble()
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice.round()).toString();
	}
	
	/**
	 * Calculates Invoice with Tax inclusive and credit reduced for external clients
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoiceAutoServiceCredit_ExternalClient_TaxInclusive_CR_Yes(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceAutoServiceCredit_ExternalClient_TaxInclusive_CR_Yes "
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		// Calculating service, federal and state tax amount
		String serviceInvoiceAmount = md_serviceInvoiceAmountTaxInclusive(testCaseId)
		logi.logInfo 'Service : amount : ' + serviceInvoiceAmount
		double stateTax =  md_STATE_TAX_INCLUSIVE_P2(testCaseId).toDouble()
		double cityTax = md_CITY_TAX_INCLUSIVE_P2(testCaseId).toDouble()
		double districtTax = md_DISTRICT_TAX_INCLUSIVE_P2(testCaseId).toDouble()
		// Calculating auto service credit amount
		double creditAmount = md_CREATE_ORDER_INVOICE_AMT(testCaseId).toDouble()
		// Subtracting the auto service credit amount from the amount calculated(Service, State, City, District amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentStateTax : "+ stateTax + " CurrentCityTax : "+cityTax + " CurrentDistrictTax : "+districtTax + " AutoServiceCredit Amount :"+ creditAmount
		double totalInvoice = serviceInvoiceAmount.toDouble() + stateTax +cityTax + districtTax - creditAmount
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice.round()).toString();
	}
	
	/**
	 * Calculates Invoice with Tax and credit reduced for external clients with tax exclusive
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoiceAutoServiceCredit_ExternalClient_TaxExclusive_CR_Yes(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceAutoServiceCredit_ExternalClient_TaxExclusive_CR_Yes "
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		// Calculating service, federal and state tax amount
		String serviceInvoiceAmount = md_serviceInvoiceAmountTaxInclusive(testCaseId)
		logi.logInfo 'Service : amount : ' + serviceInvoiceAmount
		double stateTax =  md_STATE_TAX_INCLUSIVE_P3(testCaseId).toDouble()
		double cityTax = md_CITY_TAX_INCLUSIVE_P3(testCaseId).toDouble()
		double districtTax = md_DISTRICT_TAX_INCLUSIVE_P3(testCaseId).toDouble()
		// Calculating auto service credit amount
		double creditAmount = md_CREATE_ORDER_INVOICE_AMT(testCaseId).toDouble()
		// Subtracting the auto service credit amount from the amount calculated(Service, State, City, District amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentStateTax : "+ stateTax + " CurrentCityTax : "+cityTax + " CurrentDistrictTax : "+districtTax + " AutoServiceCredit Amount :"+ creditAmount
		double totalInvoice = serviceInvoiceAmount.toDouble() + stateTax +cityTax + districtTax - creditAmount
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice.round()).toString();
	}
	
	/**
	 * Calculates Invoice with Tax exclusive and credit reduced for external clients with coupon 
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoiceAutoServiceCredit_ExternalClient_TaxExclusive_CouponReduced_CR_Yes(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceAutoServiceCredit_ExternalClient_TaxExclusive_CR_Yes "
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		// Calculating service, federal and state tax amount
		String serviceInvoiceAmount = md_serviceInvoiceAmountTaxInclusive(testCaseId)
		logi.logInfo 'Service : amount : ' + serviceInvoiceAmount
		double stateTax =  md_STATE_TAX_INCLUSIVE_P3(testCaseId).toDouble()
		double cityTax = md_CITY_TAX_INCLUSIVE_P3(testCaseId).toDouble()
		double districtTax = md_DISTRICT_TAX_INCLUSIVE_P3(testCaseId).toDouble()
		// Calculating auto service credit amount
		double orderCreditAmount = md_CREATE_ORDER_INVOICE_AMT(testCaseId).toDouble()
		String creditAmount = md_coupon_amount_reduction(testCaseId)
		// Subtracting the auto service credit amount from the amount calculated(Service, State, City, District amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentStateTax : "+ stateTax + " CurrentCityTax : "+cityTax + " CurrentDistrictTax : "+districtTax + " AutoServiceCredit Amount :"+ creditAmount.toDouble() + "Order Credit : " + orderCreditAmount
		double totalInvoice = serviceInvoiceAmount.toDouble() + stateTax +cityTax + districtTax - creditAmount.toDouble() - orderCreditAmount
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice.round()).toString();
	}
	
	/**
	 * Calculates Amount to be reduced for the coupon
	 * @param testCaseId
	 * @return Coupon amount
	 */
	String md_coupon_amount_reduction(testCaseId) {
		logi.logInfo "Inside md_coupon_amount_reduction"
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		logi.logInfo "Test Case Id : : " + testCaseId
		String apiName=testCaseId.split("-")[2]
		logi.logInfo "ApI Name : : " + apiName
		if(apiName.contains("^")){
			logi.logInfo "ApI Name inside : : " + apiName
			apiName=apiName.substring(apiName.indexOf('^')+1)
			logi.logInfo "ApI Name out : : " + apiName
		}
		logi.logInfo "ApI Name : : " + apiName
		String couponCode = getValueFromRequest(apiName,"//coupon_code").toString()
		logi.logInfo "CouponCode is : " + couponCode
		
		String planQuery = "SELECT plan_no from ariacore.all_acct_active_plans where acct_no=" + accountNo
		String[] pln = db.executeQueryReturnValuesAsArray(planQuery)
		int planUnits = 0
		String plan_service_flag
		for(int planNo = 0;planNo<pln.length;planNo++){
			logi.logInfo "Plan loop" + pln[planNo]
			plan_service_flag = db.executeQueryP2("SELECT count(plan_no) from ariacore.plan_services where plan_no=" + pln[planNo] + " and client_no = " + clientNo + " and service_no in (SELECT key_1_val FROM ariacore.client_dr_components WHERE rule_no = (SELECT rule_no FROM ariacore.coupon_discount_rule_map WHERE Coupon_cd = '"+couponCode+"' AND client_no = "+ clientNo+"))")
			if(plan_service_flag == "1") {
				logi.logInfo "Plan service loop" + pln[planNo]
				planUnits++
			}
		}

		logi.logInfo("Total plans the account having : " + planUnits)
		String couponAmount = db.executeQueryP2("select amount from ariacore.client_discount_rules where client_no=" + clientNo + " and client_rule_id=(select client_rule_id FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = 3285359 and coupon_cd = '" + couponCode + "'))")
		double couponDiscount = couponAmount.toDouble() * planUnits
		logi.logInfo("Total Discount Amount : " + couponDiscount)
		return couponDiscount.toString()
	}
	
	/**
	 * Calculates Invoice with Activation CR-0
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoiceWithActivationAndDiscount_CR0(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceWithActivationWithoutDiscount_CR0"
		// Calculating service, federal and state tax amount
		double serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId).toDouble()
		double serviceInvoiceActivationAmount = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
		double federalTax = md_FEDERAL_TAX_CREDIT_REDUCES_0_P2(testCaseId).toDouble()
		double stateTax =  md_STATE_TAX_CREDIT_REDUCES_0_P2(testCaseId).toDouble()
		// Calculating deduction amount
		int creditAmount = Integer.parseInt(md_CREDIT_AMT_BY_CREDITID(testCaseId))
		// Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount +' ServiceActivation Amount : ' + serviceInvoiceActivationAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax + " CreditAmount : "+ creditAmount
		double totalInvoice = serviceInvoiceAmount.toDouble() + serviceInvoiceActivationAmount.toDouble() + federalTax + stateTax  - creditAmount.toDouble()
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice).toString();
	}

	/**
	 * Calculates Invoice with Activation CR-1
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoiceWithActivationAndDiscount_CR1(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceWithActivationWithoutDiscount_CR1"
		// Calculating service, federal and state tax amount
		double serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId).toDouble()
		double serviceInvoiceActivationAmount = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
		double federalTax = md_FEDERAL_TAX_CREDITAMT_BY_ID_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		double stateTax =  md_STATE_TAX_CREDITAMT_BY_ID_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		// Calculating deduction amount
		int creditAmount = Integer.parseInt(md_CREDIT_AMT_BY_CREDITID(testCaseId))
		// Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount +' ServiceActivation Amount : ' + serviceInvoiceActivationAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax + " CreditAmount : "+ creditAmount
		double totalInvoice = serviceInvoiceAmount.toDouble() +serviceInvoiceActivationAmount.toDouble()+ federalTax + stateTax  - creditAmount.toDouble()
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice).toString();
	}
	
	/**
	 * Calculates Invoice with Activation CR-1 and discount
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_ExpectedInvoiceWithActivationAndDiscount_ID_CR1(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceWithActivationWithoutDiscount_CR1"
		// Calculating service, federal and state tax amount
		double serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId).toDouble()
		double serviceInvoiceActivationAmount = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
		double federalTax = md_FEDERAL_TAX_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		double stateTax =  md_STATE_TAX_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		// Calculating deduction amount
		int creditAmount = Integer.parseInt(md_CREDIT_AMT_BY_CREDITID(testCaseId))
		// Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount +' ServiceActivation Amount : ' + serviceInvoiceActivationAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax + " CreditAmount : "+ creditAmount
		double totalInvoice = serviceInvoiceAmount.toDouble() +serviceInvoiceActivationAmount.toDouble()+ federalTax + stateTax  - creditAmount.toDouble()
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice).toString();
	}

	/**
	 * Calculates Invoice with Activation
	 * @param testCaseId
	 * @return Invoice amount
	 */
	int md_serviceInvoiceActivationAmount(String testCaseId) {
		logi.logInfo "Inside md_serviceInvoiceActivationAmount"
		//String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")		
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
		logi.logInfo " TestId : "+ testId
		int invoice = 0

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = db.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time before calculating State Taxes: "+ currentVirtualTime

		// Getting maximum invoice number from db
		invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		logi.logInfo "Invoice No : "+invoiceNo

		// Getting plans involved in invoice
		HashMap<String, HashMap<String, String>> plans = getPlanServiceAndTieredPricingForActivationByInvoice(invoiceNo)
		Map<String, Map<String,Integer>> resultHash = new TreeMap<String, TreeMap<String,Integer>> ()
		resultHash.putAll(plans)
		logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()

		// Getting Multiply Activation Fee Flag from db for a cleint 
		String multiplyActivationFeeFlag =db.executeQueryP2("SELECT param_val FROM ariacore.ARIA_CLIENT_PARAMS WHERE client_no ="+ clientNo+" AND param_name  = 'MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT'")
		logi.logInfo "Multiply Activation Fee Flag : "+multiplyActivationFeeFlag +" for a Client : " +clientNo
		
		Iterator iterator = resultHash.entrySet().iterator()
		logi.logInfo "Before Key setting "
		// Iterating through Plans
		while (iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) iterator.next()
			String planNo = pairs.getKey().toString()
			logi.logInfo "Execution for the plan :  "+ planNo
			String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
			int planTypeInd = Integer.parseInt(db.executeQueryP2(planTypeQuery))
			logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo

			Map<String,String> valueMap = pairs.getValue()
			Map<String,String> value = new TreeMap<String,String> ()
			value.putAll(valueMap)

			Iterator service = value.entrySet().iterator()
			logi.logInfo "Invoice calculation for a plan : "+planNo
			int noofUnits =0
			if(planTypeInd==0)
			{
				logi.logInfo "Master Plan : "+ planNo
				String newQuery = "SELECT PLAN_UNITS FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO = " + accountNo
				noofUnits = Integer.parseInt(db.executeQueryP2(newQuery))
			}
			else {
				logi.logInfo("single supp plan")
				String suppPlan = getValueFromRequest("create_acct_complete","//supp_plans").toString()
				if ((!suppPlan.toString().equalsIgnoreCase("null"))&& suppPlan.equalsIgnoreCase(planNo)) {
					noofUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))

					logi.logInfo "single_supp_plan ::: " + suppPlan
					logi.logInfo "single_plan_noOfUnits ::: " + noofUnits
				}
				else {
					logi.logInfo "assign or replace supp plan"
					String supp_plan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
					if(supp_plan.equalsIgnoreCase(planNo))
						noofUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
					else{
						supp_plan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
						if(supp_plan.equalsIgnoreCase(planNo))
							noofUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
						else
							noofUnits = 0
					}
				}
			}

			logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo
			while(service.hasNext()){
				Map.Entry servicePairs = (Map.Entry) service.next()
				String serviceNo = servicePairs.getKey().toString()
				logi.logInfo "Invoice calculation for a service : "+serviceNo

				int seqNo=1;
				//String tieredPricing = servicePairs.getValue().toString()
				int tieredPricingInt = servicePairs.getValue() //Integer.parseInt(tieredPricing)
				logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
				int serviceInvoice
				String query
				switch(tieredPricingInt){
					case 1:
						logi.logInfo "Case 1"
						logi.logInfo "multiplyActivationFeeFlag = " + multiplyActivationFeeFlag
						if (multiplyActivationFeeFlag.toString()!="null" )
						{
							if(multiplyActivationFeeFlag.equalsIgnoreCase("TRUE"))
								query = "SELECT NRSR.RATE_PER_UNIT*"+noofUnits+" as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
							else
								query = "SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
						}
						else
							query = "SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "

						serviceInvoice = Integer.parseInt(db.executeQueryP2(query))
						logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
						invoice  = invoice+serviceInvoice
						logi.logInfo "After adding with invoice " + invoice
						seqNo++;
						break;
				}
			}
		}
		return invoice
	}



	public HashMap getPlanServiceAndTieredPricingForActivationByInvoice(String invoiceNum){
		logi.logInfo "Inside getPlanServiceAndTieredPricingForActivationByInvoice "
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		logi.logInfo "Invoice : "+invoiceNum
		String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select plan_no from ariacore.GL_DETAIL where INVOICE_NO="+invoiceNum+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and is_setup = 1) order by plan_no, service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select plan_no from ariacore.GL_DETAIL where INVOICE_NO="+invoiceNum+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and is_setup = 1) order by plan_no, service_no"))
		ResultSet resultSet = database.executeQuery(query)

		HashMap<String, HashMap<String,Integer>> resultHash = new HashMap<String, HashMap<String,Integer>> ()
		HashMap<String,String> serviceHash

		String[] plan = new String[count]
		String[] service = new String[count]
		int[] tieredPricing = new int[count]

		int i= 0
		while(resultSet.next()){
			plan[i]=resultSet.getString(1)
			service[i]=resultSet.getString(2)
			tieredPricing[i]=resultSet.getInt(3)
			i++
		}
		String plan_no
		int serviceCount=0
		for(int planCount=0;planCount<plan.length;){
			plan_no = plan[planCount]
			serviceHash = new HashMap<String,String> ()
			for(;serviceCount<service.length;serviceCount++){
				if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					planCount ++
				}
				else
					break
			}
			resultHash.put(plan_no, serviceHash)
			logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
		}
		logi.logInfo "Total Services for Proration : " + resultHash.toString()
		return resultHash
	}

	/**
	 * Calculates reduced coupon amount
	 * @param testCaseId
	 * @return Coupon amount
	 */
	String md_coupon_amount(String testCaseId)
	{
		logi.logInfo "Inside md_coupon_amount"

		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String query1, query2
		ConnectDB dbConnection = new ConnectDB()
		query1="SELECT invoice_no from (SELECT invoice_no ,row_number() over (order by invoice_no desc ) as sno FROM ARIACORE.GL where acct_no=" +accountNo+ ")"
		String invoice_No = dbConnection.executeQueryP2(query1)
		logi.logInfo "Invoice no is" +invoice_No
		query2=" SELECT (SUM(DISTILLED_AMOUNT)*-1) AS COUPON_AMT FROM (SELECT ELIGIBLE_SERVICE_NO, DISTILLED_AMOUNT FROM ariacore.all_recur_credit WHERE acct_no = (SELECT acct_no FROM ariacore.gl WHERE invoice_no="+invoice_No+") UNION SELECT CDR.KEY_2_VAL AS ELIGIBLE_SERVICE_NO, CASE WHEN CDRS.INLINE_OFFSET_IND='I' THEN 0 ELSE AIDD.ACTUAL_DISCOUNT_AMT END AS DISTILLED_AMOUNT FROM ARIACORE.CLIENT_DR_COMPONENTS CDR JOIN ariacore.all_invoice_discount_details AIDD ON AIDD.rule_no=CDR.rule_no JOIN ariacore.CLIENT_DISCOUNT_RULES CDRS ON CDRS.rule_no =AIDD.rule_no WHERE AIDD.invoice_no ="+invoice_No+")a"
		String coupon_amount = dbConnection.executeQueryP2(query2)
		if(coupon_amount==null)
		{
			return 0
		}
		else
		{
		logi.logInfo "Coupon amount is" +coupon_amount
		dbConnection.closeConnection()
		return coupon_amount
		}
	}
	
	/**
	 * Calculates Invoice charges  CR-0
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_invoice_total_amount_CR0(String testCaseId)
	{
		logi.logInfo "Inside md_invoice_total_amount_CR1"
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		int fedTax=Integer.parseInt(md_Calculate_Total_Invoice_Federal_Taxes(testCaseId))
		logi.logInfo "Fed tax is :" +fedTax
		int stateTax=Integer.parseInt(md_Calculate_Total_Invoice_State_Taxes(testCaseId))
		logi.logInfo "State tax is :" +stateTax
		int couponAmount=Integer.parseInt(md_coupon_amount(testCaseId))
		String IVBT=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICEAMOUNTBEFORETAX)
		int invoiceAmountBeforeTax = Integer.parseInt(IVBT)
		logi.logInfo "Invoice amount before tax : " +invoiceAmountBeforeTax
		int totalTax=fedTax + stateTax
		logi.logInfo "Total tax amount is : " +totalTax
		int invoiceAmountAfterTax=(invoiceAmountBeforeTax+totalTax)+couponAmount
		logi.logInfo "Invoice amount After Tax is :" +invoiceAmountAfterTax
		return invoiceAmountAfterTax.toString()
	}

	/**
	 * Calculates Invoice Charges for CR-1
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_invoice_total_amount_CR1(String testCaseId)
	{
		logi.logInfo "Inside md_invoice_total_amount_CR1"
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		int fedTax=Integer.parseInt(md_Calculate_Total_Invoice_Federal_Taxes(testCaseId))
		logi.logInfo "Fed tax is :" +fedTax
		int stateTax=Integer.parseInt(md_Calculate_Total_Invoice_State_Taxes(testCaseId))
		logi.logInfo "State tax is :" +stateTax
		int couponAmount=Integer.parseInt(md_coupon_amount(testCaseId))
		String IVBT=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICEAMOUNTBEFORETAX)
		int invoiceAmountBeforeTax = Integer.parseInt(IVBT)
		logi.logInfo "Invoice amount before tax : " +invoiceAmountBeforeTax
		int totalTax=fedTax + stateTax
		logi.logInfo "Total tax amount is : " +totalTax
		int invoiceAmountAfterTax=(invoiceAmountBeforeTax+couponAmount)+totalTax
		logi.logInfo "Invoice amount After Tax is :" +invoiceAmountAfterTax
		return invoiceAmountAfterTax.toString()
	}

	String md_acct_unpaid_charges(String testCaseId)
	{
		String flag="FALSE"
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String query="SELECT unpaid_amount from ariacore.acct_unpaid_charges where acct_no="+accountNo+ "and client_no=" +clientNo
		ConnectDB dbConnection = new ConnectDB()
		String unpaid_amount=dbConnection.executeQueryP2(query)
		if(unpaid_amount==null)
			flag="TRUE"
		return flag
	}

	String[] first_bill_date(String client_no, String startDate)
	{
		String[] dates = new String[2];

		db = new ConnectDB();
		Date date = new Date();
		Calendar now = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL";
		ResultSet resultSet = db.executeQuery(Query);
		while(resultSet.next()){
			// Get only the current Date
			date = resultSet.getDate(1);
		}
		if(!(startDate.isEmpty())){

			if(startDate.isEmpty() || startDate == "" ) {
				now.setTime(date);
			}
			else {
				now.setTime(date);
				if(startDate.contains(".")) {
					String[] days = startDate.split("\\.");
					now.add(Calendar.MONTH, Integer.parseInt(days[0]));
					now.add(Calendar.DATE, Integer.parseInt(days[1]));
				}
				else {
					now.add(Calendar.DATE, Integer.parseInt(startDate));
				}
			}
			dates[0] = simpleFormat.format(now.getTime()).toString();
			logi.logInfo "Dates[0]" +dates[0]

			//db.closeConnection()
			logi.logInfo "Dates after setting : " +dates.toString()

		}
		return dates;
	}
	
	/**
	 * Calculates days between the order
	 * @param testCaseId
	 * @return interval days
	 */
	String md_order_interval_days(String testCaseId)
	{
		String apiname=testCaseId.split("-")[2]
		logi.logInfo "Inside md_order_interval_days"
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String billing_interval_units=getValueFromRequest(apiname,"//billing_interval_units").toString()
		String billing_interval_type=getValueFromRequest(apiname,"//billing_interval_type").toString()
		logi.logInfo "Billing interval type is :" +billing_interval_type
		logi.logInfo "Billing interval units :" +billing_interval_units
		int interval_days

		switch(billing_interval_type)
		{
			case 'M':interval_days=30*(Integer.parseInt(billing_interval_units))
				return interval_days
				break;

			case 'W':interval_days=7*(Integer.parseInt(billing_interval_units))
				return interval_days
				break;

			case 'D':interval_days=1*(Integer.parseInt(billing_interval_units))
				return interval_days
				break;

			case 'Y':interval_days=365*(Integer.parseInt(billing_interval_units))
				return interval_days
				break;

			case 'Q':interval_days=(91*(Integer.parseInt(billing_interval_units)))
				return interval_days
				break;
		}

	}

	/**
	 * Calculates Credit applied to the invoice
	 * @param testCaseId
	 * @return applied credit amount
	 */
	String md_ACCOUNT_CREDIT_AVAILABLE(String testcaseId)
	{
		logi.logInfo('Inside md_ACCOUNT_CREDIT_AVAILABLE' )
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		String serviceCreditId=db.executeQueryP2("SELECT credit_id from ariacore.all_credits where acct_no="+accountNo+" and credit_type='S' and reason_cd=1")
		String amt =db.executeQueryP2("SELECT NVL((sum(debit)*-1),0) from ariacore.gl_detail where invoice_no="+invoiceNo+" and orig_credit_id="+serviceCreditId)
		return amt
	}
	
	/**
	 * Calculates Multiple Credit applied to the invoice
	 * @param testCaseId
	 * @return applied credit amount
	 */
	String md_ACCOUNT_MULTIPLE_CREDIT_APPLIED(String testcaseId)
	{
		logi.logInfo('Inside md_ACCOUNT_MULTIPLE_CREDIT_AVAILABLE' )
		String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
		String CreditIdQuery="SELECT credit_id from ariacore.all_credits where acct_no="+accountNo+" and credit_type='S' and left_to_apply=0 and reason_cd in (1,97,200) order by credit_id"
		
		String serviceCreditId
		double amt=0
		List<String> credit_ids=[]
		ResultSet resultSet = db.executePlaQuery(CreditIdQuery)
		while(resultSet.next())
		{
			logi.logInfo("Id is : " + resultSet.getString(1))
			credit_ids.add(resultSet.getString(1))
		}
		for(c in credit_ids) {
			amt = (db.executeQueryP2("SELECT NVL((sum(debit)*-1),0) from ariacore.gl_detail where invoice_no="+invoiceNo+" and orig_credit_id="+c)).toDouble() + amt
			logi.logInfo("After adding : " + amt + "    ::   " + serviceCreditId)
		}
		
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(amt).toString();
	}
	
	/**
	 * Checks the optional qualifier values are set
	 * @param testCaseId
	 * @return true or false
	 */
	
		String md_optional_qualifier_name(String testCaseId)
		{
			logi.logInfo"Inside md_optional_qualifier_name"
			String flag
			String name
			String var1,var2
			int count=0
			String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			String qualifier_name1=getValueFromRequest("create_acct_complete","//optional_transaction_qualifiers_row[1]/qualifier_name[1]")
			String qualifier_name2=getValueFromRequest("create_acct_complete","//optional_transaction_qualifiers_row[2]/qualifier_name[1]")
			logi.logInfo "qualifier_name1 : "+qualifier_name1+  " qualifier_name2 : "+qualifier_name2
			String query_name = "SELECT qualifier_name from ariacore.acct_qualifiers where acct_no= "+accountNo+ "and client_no= "+clientNo
			
			ResultSet resultSet = db.executePlaQuery(query_name)
			while(resultSet.next())
			{
				logi.logInfo "Inside ResultSet"
				if(count==0)
				{
				var1=resultSet.getString(1)
				logi.logInfo "Var1 : " +var1
				count++
				}
				else{
				var2=resultSet.getString(1)
				logi.logInfo  "var2 : " +var2
				}
			}
			if((qualifier_name1.equals(var1))&&(qualifier_name2.equals(var2)))
				{
				flag='TRUE'
				}
			else
			{
				flag='FALSE'
			}
			
			return flag
		}
		
		/**
		 * Checks the optional qualifier values are set
		 * @param testCaseId
		 * @return true or false
		 */
		String md_optional_qualifier_value(String testCaseId)
		{
			logi.logInfo"Inside md_optional_qualifier_value"
			String flag
			String name
			String var1,var2
			int count=0
			String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			String qualifier_value1=getValueFromRequest("create_acct_complete","//optional_transaction_qualifiers_row[1]/qualifier_value[1]")
			String qualifier_value2=getValueFromRequest("create_acct_complete","//optional_transaction_qualifiers_row[2]/qualifier_value[1]")
			logi.logInfo "qualifier_value1 : "+qualifier_value1+  " qualifier_name2 : "+qualifier_value2
			String query_value = "SELECT qualifier_value from ariacore.acct_qualifiers where acct_no= "+accountNo+ "and client_no= "+clientNo
			logi.logInfo "Qualifier_name" +query_value
			ResultSet resultSet = db.executePlaQuery(query_value)
			while(resultSet.next())
			{
				logi.logInfo "Inside ResultSet"
				if(count==0)
				{
				var1=resultSet.getString(1)
				logi.logInfo "Var1 : " +var1
				count++
				}
				else{
				var2=resultSet.getString(1)
				logi.logInfo  "var2 : " +var2
				}
			}
			if((qualifier_value1.equals(var1))&&(qualifier_value2.equals(var2)))
				{
				flag='TRUE'
				}
			else
			{
				flag='FALSE'
			}
			
			return flag
	}

	/**
	 * Calculates the pending credits to be applied
	 * @param testCaseId
	 * @return pending credit amount
	 */
	String md_left_to_apply(String testCaseId)
	{
		int count=0
		double credit
		logi.logInfo "Inside md_left_to_apply"
		String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		//String credit_query = "SELECT SUM(LEFT_TO_APPLY) FROM ARIACORE.CREDITS WHERE ACCT_NO=" +accountNo+ " and reason_cd not in 200"
		String invoice_query = "SELECT debit FROM ARIACORE.gl WHERE ACCT_NO=" +accountNo+ "AND CLIENT_NO=" +clientNo+ "order by invoice_no desc"
		//String CQ=db.executeQueryP2(credit_query)
		//double credit=CQ.toDouble()
		String OQ=db.executeQueryP2(invoice_query)
		double order = OQ.toDouble()
		logi.logInfo "Order amount " +order
		credit=(md_proration_result_amount(testCaseId)).toDouble()
		logi.logInfo "Credit amount "+credit
		credit=(credit+order)*-1
		logi.logInfo "Returning the value......."
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(credit).toString()
	}
	
	/**
	 * Calculates the credits  applied
	 * @param testCaseId
	 * @return applied credit amount
	 */
	String md_credit_reduced_after_tax(String testCaseId)
	{
		logi.logInfo "Inside md_credit_reduced_after_tax"
		double invoice_sum=0.0
		double total_amount
		double credit_amount
		double tax_amount
		List ser_nos=[]
	    def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String plan_no=getValueFromRequest("assign_supp_plan","//supp_plan_no")
		String query1="SELECT distinct service_no from ariacore.gl_detail where invoice_no=(SELECT max(INVOICE_NO)  FROM ARIACORE.GL WHERE ACCT_NO="+accountNo+") and start_date is not null"
		ResultSet resultSet=db.executePlaQuery(query1)
		logi.logInfo "Executed service_no query..."
		while(resultSet.next())
		{
			logi.logInfo "Inside resultSet"
			String ser_no= resultSet.getString(1)
			ser_nos.add(ser_no)
		}
		logi.logInfo("ser_nos list is:"+ser_nos.toString())
		int length=ser_nos.size()
		logi.logInfo "Length of ser_nos : " +length
		for(int i=0;i<ser_nos.size();i++)
		{
			logi.logInfo("i value:"+i)
			logi.logInfo("print ser_no: "+ser_nos.get(i).toString())
			String query2="SELECT sum(debit) from ariacore.gl_detail where invoice_no=(SELECT max(INVOICE_NO)  FROM ARIACORE.GL WHERE ACCT_NO="+accountNo+") and service_no="+ser_nos.get(i).toString()
			invoice_sum=invoice_sum+((db.executeQueryP2(query2)).toDouble())
	
			logi.logInfo "Sum 1 " +invoice_sum
		}
		logi.logInfo "invoice_sum " +invoice_sum
		String query3="SELECT sum(debit) from ariacore.gl_detail where invoice_no=(SELECT max(INVOICE_NO)  FROM ARIACORE.GL WHERE ACCT_NO="+accountNo+") and service_no=0"
		String CA=db.executeQueryP2(query3)
		if(CA==null)
		{
			credit_amount=0
		}
		else
		{
		credit_amount=CA.toDouble()
		logi.logInfo "credit_amount " +credit_amount
		}
		String query4 = "SELECT sum(debit) from ariacore.gl_detail where service_no in (400,401,402,403,405,406) and invoice_no=(SELECT max(INVOICE_NO)  FROM ARIACORE.GL WHERE ACCT_NO="+accountNo+")"
		String TA=db.executeQueryP2(query4)
		if(TA==null)
		{
			tax_amount=0
		}
		else
		{
		tax_amount=TA.toDouble()
		logi.logInfo "tax_amount " +tax_amount
		}
		total_amount=((invoice_sum+credit_amount)+tax_amount)
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(total_amount).toString()

	}
	/**
	 * Calculates the amount to be reduced for the coupon applied
	 * @param testCaseId
	 * @return coupon amount
	 */
		String md_EXPECTED_COUPON_CREDIT_TEMPLATE_AMOUNT(String testcaseId)
		{
			String apiname=testcaseId.split("-")[2]
			logi.logInfo("Service 1 is : " + apiname)
			if(apiname.contains("@")) {
				String service=apiname.split("@")[1]
				apiname=service
			}
			logi.logInfo("Service is : " + apiname)
			String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			String coupon_code = getValueFromRequest(apiname,"//coupon_code")
			String recurring_credit_template_no = db.executeQueryP2("select recurring_credit_template_no from ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP where coupon_cd='" + coupon_code + "' and client_no=" + clientNo)
			String flat_amount = db.executeQueryP2("select flat_amount from ariacore.RECURRING_CREDIT_TEMPLATES where recurring_credit_template_no=" + recurring_credit_template_no + " and client_no=" + clientNo)
			return flat_amount
		}
		
		/**
		 * Calculates the pending credits added
		 * @param testCaseId
		 * @return added credit amount
		 */
		String md_COUPON_CREDIT_ADDED(String testcaseId)
		{
			String apiname=testcaseId.split("-")[2]
			logi.logInfo("Service 1 is : " + apiname)
			if(apiname.contains("@")) {
				String service=apiname.split("@")[1]
				apiname=service
			}
			logi.logInfo("Service is : " + apiname)
			String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			String coupon_code = getValueFromRequest(apiname,"//coupon_code")
			String added_credit_amount = db.executeQueryP2("SELECT amount from ariacore.all_credits where acct_no=" + accountNo + " and orig_coupon_cd='" + coupon_code + "'")
			return added_credit_amount
		}
		
		/**
		 * Calculates the applied credits
		 * @param testCaseId
		 * @return applied credit amount
		 */
		String md_VERIFY_APPLIED_CREDIT_IN_INVOICE(String testcaseId)
		{
			String apiname=testcaseId.split("-")[2]
			String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			String coupon_code
			if(apiname.contains("@")){
				String service=apiname.split("@")[1]
				coupon_code = getValueFromRequest(service,"//coupon_code")
			}
			else {
				coupon_code = getValueFromRequest("apply_coupon_to_acct.b","//coupon_code")
			}
			String invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
			String applied_amount = db.executeQueryP2("SELECT (SUM(DEBIT)*-1) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=" + invoiceNo + " AND (ORIG_COUPON_CD='" + coupon_code + "' or orig_credit_comments is not null)")
			return applied_amount
		}
		
		/**
		 * Calculates the statement amount for the account hierarchy
		 * @param testCaseId
		 * @return statement amount
		 */
		String md_VERIFY_PARENT_CHILE_STATEMENT(String testcaseId) 
		{
			String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			logi.logInfo("Account no 1 : " + accountNo)
			def first_statement = db.executeQueryP2("SELECT NVL(SUM(DEBIT),0) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " + accountNo + ")")
			
			String accountNo2=Constant.mycontext.expand('${Properties#AccountNo2}')
			logi.logInfo("Account no 2 : " + accountNo2)
			def second_account_statement = db.executeQueryP2("SELECT NVL(SUM(DEBIT),0) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " + accountNo2 + ")")
			
			double final_amount = first_statement.toDouble() + second_account_statement.toDouble()
			
			
			DecimalFormat df = new DecimalFormat("#.##");
			return df.format(final_amount).toString();
		}
		
		/**
		 * Calculates the credits after tax
		 * @param testCaseId
		 * @return tax amount
		 */
		String md_credit_reduced_after_tax_vertex(String testCaseId)
		{
			logi.logInfo "Inside md_credit_reduced_after_tax"
			double invoice_sum=0.0
			double total_amount
			double credit_amount
			double tax_amount
			List ser_nos=[]
			String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			String plan_no=getValueFromRequest("assign_supp_plan","//supp_plan_no")
			String query1="SELECT distinct service_no from ariacore.gl_detail where invoice_no=(SELECT max(INVOICE_NO)  FROM ARIACORE.GL WHERE ACCT_NO="+accountNo+") and service_no not in (0,400,401,402,403,405)"
			ResultSet resultSet=db.executePlaQuery(query1)
			logi.logInfo "Executed service_no query..."
			while(resultSet.next())
			{
				logi.logInfo "Inside resultSet"
				String ser_no= resultSet.getString(1)
				ser_nos.add(ser_no)
			}
			logi.logInfo("ser_nos list is:"+ser_nos.toString())
			int length=ser_nos.size()
			logi.logInfo "Length of ser_nos : " +length
			for(int i=0;i<ser_nos.size();i++)
			{
				logi.logInfo("i value:"+i)
				logi.logInfo("print ser_no: "+ser_nos.get(i).toString())
				String query2="SELECT sum(debit) from ariacore.gl_detail where invoice_no=(SELECT max(INVOICE_NO)  FROM ARIACORE.GL WHERE ACCT_NO="+accountNo+") and service_no="+ser_nos.get(i).toString()
				invoice_sum=invoice_sum+((db.executeQueryP2(query2)).toDouble())
		
				logi.logInfo "Sum 1 " +invoice_sum
			}
			logi.logInfo "invoice_sum " +invoice_sum
			String query3="SELECT sum(debit) from ariacore.gl_detail where invoice_no=(SELECT max(INVOICE_NO)  FROM ARIACORE.GL WHERE ACCT_NO="+accountNo+") and service_no=0"
			String CA=db.executeQueryP2(query3)
			if(CA==null)
			{
				credit_amount=0
			}
			else
			{
				credit_amount=CA.toDouble()
				logi.logInfo "credit_amount " +credit_amount
			}
			String query4 = "SELECT sum(debit) from ariacore.gl_detail where service_no in (400,401,402,403,405,406) and invoice_no=(SELECT max(INVOICE_NO)  FROM ARIACORE.GL WHERE ACCT_NO="+accountNo+")"
			String TA=db.executeQueryP2(query4)
			if(TA==null)
			{
				tax_amount=0
			}
			else
			{
				tax_amount=TA.toDouble()
				logi.logInfo "tax_amount " +tax_amount
			}
			total_amount=((invoice_sum+credit_amount)+tax_amount)
			DecimalFormat df = new DecimalFormat("#.##");
			return df.format(total_amount).toString()
	
		}
		
		/**
		 * Calculates the line items to be checked with get acct preview statement API
		 * @param testCaseId
		 * @return HashMap with line items
		 */
		def md_get_acct_preview_statement_serivce_InvoiceAmt(String testCaseId) {
			String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
			DecimalFormat df1 = new DecimalFormat("#.##");
			logi.logInfo "testId : "+ testId
			def invoice = 0, individual_invoice=0
			String invoiceNo;
			ConnectDB db = new ConnectDB();
			String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
			String currentVirtualTime = db.executeQueryP2(Query);
			logi.logInfo "Current Virtual Time before calculating State Taxes: "+ currentVirtualTime
			// Getting maximum invoice number from db
			invoiceNo=db.executeQueryP2("SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+" AND CLIENT_NO="+ clientNo)
			logi.logInfo "Invoice No : "+invoiceNo
			String prev_invoice = db.executeQueryP2("SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoiceNo)
			// Getting the plan(s) involved for billing
			HashMap<String, HashMap<String, String>> plans = getPlanServiceAndTieredPricingByInvoice(invoiceNo)
			Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
			resultHash.putAll(plans)
			logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
			
			HashMap<String, String> finalResult = new HashMap<String, String>()
			finalResult.put("Account Number", accountNo)
			Iterator iterator = resultHash.entrySet().iterator()
			logi.logInfo "Before Key setting "
			// Iterating through Plans
			while (iterator.hasNext()) {
				Map.Entry pairs = (Map.Entry) iterator.next()
				String planNo = pairs.getKey().toString()
				logi.logInfo "Execution for the plan :  "+ planNo
				String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
				int planTypeInd = Integer.parseInt(db.executeQueryP2(planTypeQuery))
				logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo
	
				int noOfUnits =0
				// Checking for the type of plan and getting no of units
				if(planTypeInd==0) {
					logi.logInfo "Master Plan : "+ planNo
					String newQuery = "SELECT PLAN_UNITS FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO = " + accountNo
					noOfUnits = Integer.parseInt(db.executeQueryP2(newQuery))
				}
				else {
					logi.logInfo "Supp Plan : "+ planNo
					String[] noOfUnitArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
					String[] suppPlans = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")
	
					if (!suppPlans.toString().equalsIgnoreCase("null")) {
						logi.logInfo("Array of supp plan")
						logi.logInfo "supp_plans :::  " + suppPlans.toString()
						logi.logInfo "noOfUnits :::  " + noOfUnitArray.toString()
						if(Arrays.asList(suppPlans).contains(planNo)){
							for( int j=0;j<suppPlans.length;j++) {
								if(suppPlans[j].toString().equalsIgnoreCase(planNo)){
									noOfUnits = Integer.parseInt(noOfUnitArray[j])
									logi.logInfo "no_of_units : "+noOfUnits+ " plan : " +planNo
									break
								}
							}
						}
					}
					else {
						logi.logInfo("single supp plan")
						String suppPlan = getValueFromRequest("create_acct_complete","//supp_plans").toString()
						if ((!suppPlan.toString().equalsIgnoreCase("null"))&& suppPlan.equalsIgnoreCase(planNo)) {
							noOfUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))
							
							String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
							int noOfUnits2 = Integer.parseInt(db.executeQueryP2(supp_plan_unit_query))
							
							if(noOfUnits2 > noOfUnits) {
								noOfUnits = noOfUnits2
							}
							
							logi.logInfo "single_supp_plan :::  " + suppPlan
							logi.logInfo "single_plan_noOfUnits :::  " + noOfUnits
						}
						else {
							logi.logInfo "assign or replace supp plan"
							String supp_plan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
							if(supp_plan.equalsIgnoreCase(planNo))
								noOfUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
							else{
								supp_plan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
								if(supp_plan.equalsIgnoreCase(planNo))
									noOfUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
								else {
									String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
									noOfUnits = Integer.parseInt(db.executeQueryP2(supp_plan_unit_query))
								}
							}
						}
					}
				}
	
				logi.logInfo "No of Units : "+noOfUnits+ " of a plan : " +planNo
	
				Map<String,String> valueMap = pairs.getValue()
				Map<String,String> value = new TreeMap<String,String> ()
				value.putAll(valueMap)
				Iterator service = value.entrySet().iterator()
				logi.logInfo "Invoice calculation for a plan : "+planNo
				// Iterating through Plans
				while(service.hasNext()){
					Map.Entry servicePairs = (Map.Entry) service.next()
					String serviceNo = servicePairs.getKey().toString()
					logi.logInfo "Invoice calculation for a service : "+serviceNo
					String isServiceTaxableQuery = "SELECT TAXABLE_IND from ariacore.all_service where service_no = " +serviceNo
					String isServiceTaxable = db.executeQueryP2(isServiceTaxableQuery)
					logi.logInfo "Service "+ serviceNo+" taxable/not : "+isServiceTaxable
					String zeroDollarPlanCheckQuery = "select distinct trunc(sum(rate_per_unit)) as RatePerUnit from ariacore.new_rate_sched_rates where schedule_no in (select schedule_no from ariacore.plan_rate_sched_map where plan_no = "+planNo+" and client_no="+clientNo+") and service_no = "+serviceNo+" and client_no="+clientNo
					int isZeroDollar = Integer.parseInt(db.executeQueryP2(zeroDollarPlanCheckQuery))
					logi.logInfo "Service "+ serviceNo+" Zero Dollar/not : "+isZeroDollar
	
					//Checking the service is taxable and non-zero dollar plan or not
					if(isServiceTaxable.equalsIgnoreCase("1") && (isZeroDollar!=0)){
						String planName,serviceName
						planName=db.executeQueryP2("select plan_name from ariacore.acct_details where acct_no = " + accountNo)
						logi.logInfo "Plan Name  :: "+planName
						serviceName=db.executeQueryP2("select comments from ariacore.services where service_no=" + serviceNo + " and custom_to_client_no=" + clientNo)
						logi.logInfo "Service Name  :: "+serviceName
						logi.logInfo "Service is taxable : "+serviceNo
						int seqNo=1;
						String tieredPricing = servicePairs.getValue().toString()
						int tieredPricingInt = Integer.parseInt(tieredPricing)
						logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
						def serviceInvoice
						String query
						int upgraded_units
						if(testCaseId.contains("update_master_plan")) {
							String units_query = "select recurring_factor,rate_per_unit from ariacore.acct_rates_history_details where acct_no = " + accountNo + " and client_no = " + clientNo + " and plan_no = " + planNo + " and service_no=" + serviceNo + " and (" + noOfUnits + " between from_unit and to_unit)"
							ResultSet rs = db.executePlaQuery(units_query)
							int max_units=noOfUnits, min_units=0
							while(rs.next()){
								int db_units = Integer.parseInt(rs.getString(1))
								if(db_units >= max_units) {
									max_units = db_units
								}
								else {
									min_units = db_units
								}
								if(Integer.parseInt(rs.getString(2)) == 0)
									max_units=min_units=0
							}
							upgraded_units = max_units-min_units
							logi.logInfo "Units Difference : " + upgraded_units
						}
						switch(tieredPricingInt){
							case 1:
								logi.logInfo "Case 1"
								if((testCaseId.contains("update_master_plan")) && (!testCaseId.contains("^update_master_plan")))
									query = "SELECT NRSR.RATE_PER_UNIT * " + upgraded_units + " as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
								//else if ((testCaseId.contains("update_acct_complete")) && (!testCaseId.contains("^update_acct_complete")))
								//	query = "SELECT NRSR.RATE_PER_UNIT * " + (prorationFactor*noOfUnits) + " as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
								else
									query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
								seqNo++;
								break;
							case 2 :
								logi.logInfo "Case 2"
								if((testCaseId.contains("update_master_plan")) && (!testCaseId.contains("^update_master_plan")))
									query ="select rate_per_unit*" + upgraded_units +" from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
								//else if ((testCaseId.contains("update_acct_complete")) && (!testCaseId.contains("^update_acct_complete")))
								//	query = "select rate_per_unit*" + (prorationFactor*noOfUnits) + " from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
								else
									query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
								break;
							case 3 :
								logi.logInfo "Case 3"
								if((testCaseId.contains("update_master_plan")) && (!testCaseId.contains("^update_master_plan")))
									query = "select 1*" + upgraded_units +" from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
								//else if ((testCaseId.contains("update_acct_complete")) && (!testCaseId.contains("^update_acct_complete")))
								//	query = "select 1*" + (prorationFactor*noOfUnits) + " from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
								else
									query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noOfUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
								break;
						}
						logi.logInfo "Before adding with invoice " + invoice
						serviceInvoice = (db.executeQueryP2(query)).toString().toDouble();
						logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
						if(serviceInvoice != 0)
						{
							individual_invoice = invoice
							finalResult.put(planName + " " + serviceName, df1.format(serviceInvoice).toString())
						}
						invoice  = invoice.toDouble()+serviceInvoice.toDouble()
						logi.logInfo "After adding with invoice " + invoice
					}
	
					else
						logi.logInfo "Service is not taxable : "+ serviceNo
				}
			}
			def federalTaxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no =  " + clientNo).toString()
			def stateTaxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no =  " + clientNo).toString()
			double federalTax = invoice.toDouble() * federalTaxRate.toDouble()
			double StateTax = invoice.toDouble() * stateTaxRate.toDouble()
			finalResult.put("Federal Taxes", df1.format(federalTax).toString())
			finalResult.put("State Taxes", df1.format(StateTax).toString())
			double total_invoice = invoice + federalTax + StateTax
			double prev_inv = prev_invoice.toDouble() + total_invoice
			finalResult.put("Invoice", df1.format(total_invoice).toString())
			String invoice_amt
			ResultSet rs = db.executePlaQuery("select acct_no,userid from ariacore.acct_details where senior_acct_no = " + accountNo +" and client_no=" + clientNo + " and resp_level_cd=2")
			logi.logInfo "Result Set result : "+ rs
			while(rs.next()){
				logi.logInfo "in rs loop : "
				String acct_no_child, child_userid
				int child_count = 0
				logi.logInfo "in rs inside loop : "
				acct_no_child = rs.getString("acct_no")
				child_userid = rs.getString("userid")
				logi.logInfo "Child Acct Number : " + acct_no_child
				invoice_amt = db.executeQueryP2("SELECT NVL(SUM(DEBIT), 0) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=(SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO=" + acct_no_child + ")")
				logi.logInfo "invoice in side loop : "+ invoice_amt
				logi.logInfo "values to be added : " + prev_inv + "::" + invoice_amt.toDouble() + "::" + total_invoice
				prev_inv =  prev_inv + total_invoice //prev_inv + invoice_amt.toDouble() + total_invoice
				logi.logInfo "added prev invoice : "+ prev_inv
				//logi.logInfo "added prev invoice : "+ (df1.format(invoice_amt.toDouble())).toString()
				String temp = "Balance transfer from acct " + acct_no_child.toString() + " (User ID " + child_userid + ")"
				logi.logInfo("Concatenated : " + temp)
				//finalResult.put(temp.concat((child_count++).toString()), df1.format(invoice_amt.toDouble()))
				finalResult.put(temp, df1.format(invoice_amt.toDouble()))
				finalResult.put("Invoice", df1.format(prev_inv).toString())
				
				//finalResult.put(temp.concat((child_count++).toString()), df1.format(total_invoice).toString())
				finalResult.put(temp, df1.format(total_invoice).toString())
				prev_inv = prev_inv + invoice_amt.toDouble()
			}
			finalResult.put("Balance Due", df1.format(prev_inv).toString())
			logi.logInfo "Expected result Hash : "+ finalResult.sort()
			return finalResult.sort()
		}
		
		/**
		 * Calculates the line items generated with the get acct preview statement API
		 * @param testCaseId
		 * @return HashMap with line items
		 */
		def md_get_acct_preview_statement_response(String testCaseId) {
			String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			String out_statement = getValueFromResponse("get_acct_preview_statement", ExPathRpcEnc.GET_ACCT_PREVIEW_STATEMENT_OUT_STATEMENT)
			logi.logInfo("out statement : " + out_statement)
			out_statement=out_statement.replaceAll('&nbsp;', "")
			out_statement=out_statement.replaceAll('<br>',"<br />")
//			out_statement=out_statement.replaceAll('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">', "")
//			out_statement=out_statement.replaceAll('<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">', "")
//			out_statement=out_statement.replaceAll('</html>"', '</html>"""')
//			out_statement=out_statement.replaceFirst("\n", "")
//			out_statement=out_statement.replaceAll('"<html>', '"""<html>')
			
			String out_statement_body_splitted = out_statement.split("""<body>""")[1]
			String out_statement_footer_splitted = out_statement_body_splitted.split("""<p class="footer">This is a""")[0]
			logi.logInfo("Center content : " + out_statement_footer_splitted)
			/*String prefix_statement = '''<html>
<head>

<title></title>

<style media="screen" type="text/css">
    body { font-family: Helvetica, Arial, sans-serif; font-size: 11px; margin:0; padding:0; }
    p {  }
    p.footer { font-size: 80%; font-color: #000 }

    table { border: none; padding: 0px; cell-padding: 0; cell-spacing: 0; }
    table.box { border: 1px solid #000; width: 100% ; background-color: #EEE }
    tr { padding: 0px; }
    tr.branded { border-bottom: solid 1px #000; border-top: solid 1px #000; background-color: #EEE; }
    tr.lineitem { border-bottom: solid 1px #000; padding: 2px; }
    th { text-align: left; vertical-align: bottom; padding: 5px; font-weight: bold; }
    th.inline { text-align: left; font-weight: bold; padding: 0px }
    th.branded { border-bottom: solid 1px #000; border-top: solid 1px #000; background-color: #EEE; }
    td { }
    td.totals { border-bottom: solid 1px #000; background-color: #FFF; font-weight: bold; padding: 5px; }
    td.totals-rightalign { border-bottom: solid 1px #000; background-color: #FFF; text-align: right; font-weight: bold; padding: 5px; }

    #logo {margin-bottom: 13px;}
    #content {width: 650px; margin: 0 auto; }
</style>
<style media="print" type="text/css">
    body { font-family: Helvetica, Arial, sans-serif; font-size: 11px; margin:0; padding:0; }
    p { line-height: 1.5em; }
    p.footer { font-size: 80%; font-color: #EEE }

    table { border: none; padding: 0px; cell-padding: 0; cell-spacing: 0; }
    table.box { border: 1px solid #000; width: 100% ; background-color: #EEE }
    tr { padding: 0px; }
    tr.branded { border-bottom: solid 1px #000; border-top: solid 1px #000; background-color: #EEE; }
    tr.lineitem { border-bottom: solid 1px #000; padding: 2px; }
    th { text-align: left; vertical-align: bottom; padding: 5px; font-weight: bold; }
    th.inline { text-align: left; font-weight: bold; padding: 0px }
    th.branded { border-bottom: solid 1px #000; border-top: solid 1px #000; background-color: #EEE; }
    td { }
    td.totals { border-bottom: solid 1px #000; background-color: #FFF; font-weight: bold; padding: 5px; }
    td.totals-rightalign { border-bottom: solid 1px #000; background-color: #FFF; text-align: right; font-weight: bold; padding: 5px; }

    #logo {margin-bottom: 13px;}
    #content {width: 650px; margin: 0 auto; }
</style>

</head>

<body>'''*/
			
			String prefix_statement = '''<html>
<head>
<title>
</title>

<style media="all" type="text/css">
    body { font-family: Helvetica, Arial, sans-serif; font-size: 11px; margin:0; padding:0; }
    p.footer { font-size: 80%; font-color: #000 }
    table { border: none; padding: 0px; padding: 5px; spacing: 0px; }
    table.box { border: 1px solid #000; width: 100% ; background-color: #EEE }
    th { text-align: left; vertical-align: bottom; padding: 5px; font-weight: bold; }
    th.inline { text-align: left; font-weight: bold; padding: 0px }
    th.branded { border-bottom: solid 1px #000; border-top: solid 1px #000; background-color: #EEE; text-align:left}
    td.branded { border-bottom: solid 1px #000; border-top: solid 1px #000; background-color: #EEE; text-align:left}
    td.lineitem { border-bottom: solid 1px #000; padding: 2px; }
    td.totals { border-bottom: solid 1px #000; background-color: #FFF; font-weight: bold; padding: 5px; }
    td.totals-rightalign { border-bottom: solid 1px #000; background-color: #FFF; text-align: right; font-weight: bold; padding: 5px; }
    
    #logo {margin-bottom: 13px;}
    #content {width: 650px; margin: 0 auto; }
</style>
<style media="print" type="text/css">
@page {
    size : portrait
    margin-top: .32in;
    margin-left: .5in;
    margin-right: .5in;
    margin-bottom: .32in;
    @bottom-right {
        content: counter(page) " of " counter(pages);
    }
}
@page :first {
    margin-left: 1in;
    margin-bottom: .43in;
}
@page rotated {
    size: landscape
}
.rotated { page : rotated }
</style>

</head>

<body>'''

			String suffix_statement = '''<p class="footer">This is a total of all charges due, both previous balance and current charges. If you have questions regarding these charges, please contact API Phase II - KeyAPI-CR0_1A Customer Service:</p>
            
<p class="footer">By Email<br />
<a href="mailto:">
</a>
</p>

<p class="footer">By Phone ()<br />
</p>
            
<p class="footer">To view or update your account information, please visit <a href="">
</a>.</p>

</div>

</body>

</html>'''
			
			String new_statement = (prefix_statement.concat(out_statement_footer_splitted)).concat(suffix_statement)
			logi.logInfo("out statement changed: " + new_statement)
			HashMap <String, String> resultHash = new HashMap<String, String>()
			def doc = new XmlSlurper().parseText(new_statement)
			logi.logInfo("out statement doc: " + doc)
			DecimalFormat df1 = new DecimalFormat("#.##");
			String th_text
			def account_no
			int i=0, child_acct=0
			double gst=0, tax=0, total_invoice=0 
			doc.depthFirst().collect { it }.findAll { it.name() == "tr" }.each {
				logi.logInfo("in depth loop")
				i++
			    String td_text = "${it.text()}" 
			    String amt
			    if(i != 1) {
					logi.logInfo "in loop"
					logi.logInfo td_text + "\n"
			      if (td_text.contains("Account Number:"))
			      {
			        String s = td_text.replace(":", "_")
					account_no = s.split("_")[1]
			        logi.logInfo "Account number extracted : "+account_no
					resultHash.put("Account Number", account_no)
			      }
				  if (td_text.contains("Total (excl GST)"))
				  {
					String s = td_text.replace(":", "_")
					amt = s.split("_")[1]
					gst = amt.toDouble()
					logi.logInfo  "GST Extracted : " + gst
				  }
				  if (td_text.contains("GST Amount Payable"))
				  {
					String s = td_text.replace(":", "_")
					amt = s.split("_")[1]
					tax = amt.toDouble()
					logi.logInfo  "Tax Extracted : " + tax
				  }
				  total_invoice = gst+tax
				  
				  if (td_text.contains("Balance Due"))
				  {
					String s = td_text.replace(":", "_")
					amt = s.split("_")[1]
					amt = amt.replace(",", "")
					//double balance_due = amt.toDouble()
					logi.logInfo  "Balance Due Extracted : " + amt
					resultHash.put("Balance Due", df1.format(amt.toDouble()))
				  }	  
				}
			}
//			int child_acct_count = 0
//			doc.depthFirst().collect { it }.findAll { it.@class == "lineitem" }.each { linkitem->
//				if(linkitem.text().contains("Invoice xfr")) {
//					logi.logInfo  "in XFR loop "
//					resultHash.put(linkitem.td[0].text().trim()+(child_acct_count).toString(),df1.format((linkitem.td[linkitem.td.size()-1].text().trim()).toDouble()))
//					//total_invoice = total_invoice + (df1.format((linkitem.td[linkitem.td.size()-1].text().trim()).toDouble())).toDouble()
//					child_acct_count++
//					child_acct = 1
//				}
//				else {
//					logi.logInfo  "in LineItem loop "
//					resultHash.put(linkitem.td[1].text().trim(),df1.format((linkitem.td[linkitem.td.size()-1].text().trim()).toDouble()))
//				}
//			}
			int rows
			doc.depthFirst().collect { it }.findAll { it.@id == "content" }.each { linkitem->
				rows = linkitem.table[1].tr.size()
				rows = rows.toInteger()-9+2
				logi.logInfo("No of rows to be considered : " + rows)
				String item_text
				for(i=2; i<rows;i++) {
				  item_text = linkitem.table[1].tr[i].td[0].text().trim()
				  if(item_text.contains("Balance transfer")) {
					child_acct=1
				  }
				  resultHash.put(item_text, df1.format((linkitem.table[1].tr[i].td[4].text().trim()).toDouble()))
				}
				if(linkitem.table[1].tr.text().contains("Total Charges This Statement:")) {
					logi.logInfo "Total charged loop"
					resultHash.put("Invoice", (df1.format(linkitem.table[1].tr[rows+4].td[4].text().trim().toDouble())).toString())
				}
			  }
			
//			if(child_acct){
//				String prev_invoice = db.executeQueryP2("SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=(SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+accountNo+")")
//				logi.logInfo  "child acct invoice loop " + prev_invoice
//				total_invoice = prev_invoice.toDouble() + total_invoice
//				logi.logInfo  "after child acct added  " + total_invoice
//			}
//			resultHash.put("Invoice", df1.format(total_invoice).toString())
			logi.logInfo  "Final result hashmap : " + resultHash.sort()
			return resultHash.sort()
			//return account_no.toString()
		}
		
		/**
		 * Calculates the line items generated with the get acct preview statement API for XML output
		 * @param testCaseId
		 * @return HashMap with line items
		 */
		def md_get_acct_preview_statement_response_XML(String testCaseId) {
			String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			String out_statement = getValueFromResponse("get_acct_preview_statement", ExPathRpcEnc.GET_ACCT_PREVIEW_STATEMENT_OUT_STATEMENT)
			logi.logInfo("out statement : " + out_statement)
			String prefix = '"""'
			//out_statement = (prefix.concat(out_statement)).concat('"""')
			logi.logInfo("out statement after adding quotes: " + out_statement)
			HashMap <String, String> resultHash = new HashMap<String, String>()
			def doc = new XmlSlurper().parseText(out_statement)
			logi.logInfo("out statement doc: " + doc)
			DecimalFormat df1 = new DecimalFormat("#.##");
			double sum = 0
			resultHash.put("Account Number", doc.statement_header.aria_acct_no.text().trim())
			String bal_due = doc.statement_header.statement_total.text().trim()
			resultHash.put("Balance Due", df1.format(bal_due.toDouble()))
			doc.statement_detail.line_item.each {
				String sp = "${it.description}".trim()
				sp = sp.replaceAll("\\s+", " ")
				String taxes = "${it.extended_rate}"
				resultHash.put(sp, df1.format(taxes.toDouble()))
				sum = ("${it.extended_rate}".trim()).toDouble() + sum
			  }
			logi.logInfo("Final Invocie is " + df1.format(sum))
			resultHash.put("Invoice", df1.format(sum).toString())
			return resultHash.sort()
		}
		
		/**
		 * Calculates the Invoice amount with service credit with CR-0 with activation
		 * @param testCaseId
		 * @return Invocie amount
		 */
		String md_ExpectedInvoiceWithServiceCredit_CR0(String testCaseId){
			logi.logInfo "Inside md_ExpectedInvoiceWithServiceCredit_CR0"
			String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			// Calculating service, federal and state tax amount
			String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
			//int serviceInvoiceActivationAmount = md_serviceInvoiceActivationAmount(testCaseId)
			double federalTax = md_FEDERAL_TAX_CREDIT_REDUCES_0_P2(testCaseId).toDouble()
			double stateTax =  md_STATE_TAX_CREDIT_REDUCES_0_P2(testCaseId).toDouble()
			// Calculating deduction amount
			String creditAmount = md_ACCOUNT_CREDIT_AVAILABLE(testCaseId)
			String activation_flg = db.executeQueryP2("select count(service_no) from ariacore.gl_detail where invoice_no=(SELECT max(invoice_no) from ariacore.gl where acct_no = " + accountNo + ") and comments like '%ACTIVATION%'")
			logi.logInfo("Activation flag set : " + activation_flg)
			double activation_amt = 0
			if(activation_flg == "1") {
				activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
				logi.logInfo "Activation amount : " + (activation_amt)/2
				activation_amt = activation_amt/2
			}
			else if(activation_flg != "0" && activation_flg != "1") {
				activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
				logi.logInfo "Activation amount : " + activation_amt
			}
			// Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
			logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax + " CreditAmount : "+ creditAmount + "Activation Amount : " + activation_amt
			double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax  - creditAmount.toDouble() + activation_amt
			logi.logInfo 'Total Invoice : ' + totalInvoice
			String tot_inv = df.format(totalInvoice).toString()
			if(tot_inv.contains(",")){
				tot_inv = tot_inv.replaceAll(",", "")
			}
			return tot_inv
		}
		
		/**
		 * Calculates the Invoice amount with multiple service credit with CR-0
		 * @param testCaseId
		 * @return Invocie amount
		 */
		String md_ExpectedInvoiceWithMultipleServiceCredit_CR0(String testCaseId){
			logi.logInfo "Inside md_ExpectedInvoiceWithServiceCredit_CR0"
			String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			double federalTax = 0
			// Calculating service, federal and state tax amount
			String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
			//int serviceInvoiceActivationAmount = md_serviceInvoiceActivationAmount(testCaseId)
			String client_qry = "SELECT METHOD_CD FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO="+ clientNo
			String Client_cd= db.executeQueryP2(client_qry)
			logi.logInfo "client_cd " + Client_cd
			if(Client_cd !="6")
			{
				 federalTax = md_FEDERAL_TAX_CREDIT_REDUCES_0_P2(testCaseId).toDouble()
			}
			double stateTax =  md_STATE_TAX_CREDIT_REDUCES_0_P2(testCaseId).toDouble()
			// Calculating deduction amount
			String creditAmount = md_ACCOUNT_MULTIPLE_CREDIT_APPLIED(testCaseId)
			String activation_flg = db.executeQueryP2("select count(service_no) from ariacore.gl_detail where invoice_no=(SELECT max(invoice_no) from ariacore.gl where acct_no = " + accountNo + ") and comments like '%ACTIVATION%'")
			logi.logInfo("Activation flag set : " + activation_flg)
			double activation_amt = 0
			if(activation_flg == "1") {
				activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
				logi.logInfo "Activation amount : " + (activation_amt)/2
				activation_amt = activation_amt/2
			}
			else if(activation_flg != "0" && activation_flg != "1") {
				activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
				logi.logInfo "Activation amount : " + activation_amt
			}
			// Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
			logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax + " CreditAmount : "+ creditAmount + "Activation Amount : " + activation_amt
			double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax  - creditAmount.toDouble() + activation_amt
			logi.logInfo 'Total Invoice : ' + totalInvoice
			String tot_inv = df.format(totalInvoice).toString()
			if(tot_inv.contains(",")){
				tot_inv = tot_inv.replaceAll(",", "")
			}
			return tot_inv
		}
		
		/**
		 * Calculates the Invoice amount with service credit with CR-1
		 * @param testCaseId
		 * @return Invocie amount
		 */
		String md_ExpectedInvoiceWithServiceCredit_CR1(String testCaseId){
			logi.logInfo "Inside md_ExpectedInvoiceWithServiceCredit_CR0"
			String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
			// Calculating service, federal and state tax amount
			String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
			//int serviceInvoiceActivationAmount = md_serviceInvoiceActivationAmount(testCaseId)
			double federalTax = md_FEDERAL_TAX_CREDITAMT_BY_ID_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
			double stateTax =  md_STATE_TAX_CREDITAMT_BY_ID_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
			// Calculating deduction amount
			String creditAmount = md_ACCOUNT_CREDIT_AVAILABLE(testCaseId)
			String activation_flg = db.executeQueryP2("select count(service_no) from ariacore.gl_detail where invoice_no=(SELECT max(invoice_no) from ariacore.gl where acct_no = " + accountNo + ") and comments like '%ACTIVATION%'")
			logi.logInfo("Activation flag set : " + activation_flg)
			double activation_amt = 0
			if(activation_flg == "1") {
				activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
				logi.logInfo "Activation amount : " + (activation_amt)/2
				activation_amt = activation_amt/2
			}
			else if(activation_flg != "0" && activation_flg != "1") {
				activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
				logi.logInfo "Activation amount : " + activation_amt
			}
			// Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
			logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax + " CreditAmount : "+ creditAmount + "Activation Amount : " + activation_amt
			double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax  - creditAmount.toDouble() + activation_amt
			logi.logInfo 'Total Invoice : ' + totalInvoice
			String tot_inv = df.format(totalInvoice).toString()
			if(tot_inv.contains(",")){
				tot_inv = tot_inv.replaceAll(",", "")
			}
			return tot_inv
		}
		
		/**
		 * Calculates the Invoice amount with multiple service credit with CR-1
		 * @param testCaseId
		 * @return Invocie amount
		 */
		String md_ExpectedInvoiceWithMultipleServiceCredit_CR1(String testCaseId){
			logi.logInfo "Inside md_ExpectedInvoiceWithServiceCredit_CR0"
			String accountNo=getValueFromResponse('create_acct_complete',"//acct_no")
			// Calculating service, federal and state tax amount
			String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
			//int serviceInvoiceActivationAmount = md_serviceInvoiceActivationAmount(testCaseId)
			double federalTax = md_FEDERAL_TAX_CREDITAMT_BY_ID_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
			double stateTax =  md_STATE_TAX_CREDITAMT_BY_ID_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
			// Calculating deduction amount
			String creditAmount = md_ACCOUNT_MULTIPLE_CREDIT_APPLIED(testCaseId)
			String activation_flg = db.executeQueryP2("select count(service_no) from ariacore.gl_detail where invoice_no=(SELECT max(invoice_no) from ariacore.gl where acct_no = " + accountNo + ") and comments like '%ACTIVATION%'")
			logi.logInfo("Activation flag set : " + activation_flg)
			double activation_amt = 0
			if(activation_flg == "1") {
				activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
				logi.logInfo "Activation amount : " + (activation_amt)/2
				activation_amt = activation_amt/2
			}
			else if(activation_flg != "0" && activation_flg != "1") {
				activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
				logi.logInfo "Activation amount : " + activation_amt
			}
			// Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
			logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax + " CreditAmount : "+ creditAmount + "Activation Amount : " + activation_amt
			double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax  - creditAmount.toDouble() + activation_amt
			logi.logInfo 'Total Invoice : ' + totalInvoice
			String tot_inv = df.format(totalInvoice).toString()
			if(tot_inv.contains(",")){
				tot_inv = tot_inv.replaceAll(",", "")
			}
			return tot_inv
		}
		
		/**
		 * Calculates the Invoice amount with service credit with No tax
		 * @param testCaseId
		 * @return Invocie amount
		 */
		String md_ExpectedInvoiceWithServiceCredit_NoTax(String testCaseId){
			logi.logInfo "Inside md_ExpectedInvoiceWithServiceCredit_CR0"
			String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
			// Calculating service, federal and state tax amount
			String serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId)
			// Calculating deduction amount
			String creditAmount = md_ACCOUNT_MULTIPLE_CREDIT_APPLIED(testCaseId)
			String activation_flg = db.executeQueryP2("select count(service_no) from ariacore.gl_detail where invoice_no=(SELECT max(invoice_no) from ariacore.gl where acct_no = " + accountNo + ") and comments like '%ACTIVATION%'")
			logi.logInfo("Activation flag set : " + activation_flg)
			double activation_amt = 0
			if(activation_flg == "1") {
				activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
				logi.logInfo "Activation amount : " + (activation_amt-150)
				activation_amt = activation_amt-400
			}
			else if(activation_flg != "0" && activation_flg != "1") {
				activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
				logi.logInfo "Activation amount : " + activation_amt
			}
			// Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
			logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CreditAmount : "+ creditAmount + "Activation Amount : " + activation_amt
			double totalInvoice = serviceInvoiceAmount.toDouble() - creditAmount.toDouble() + activation_amt
			logi.logInfo 'Total Invoice : ' + totalInvoice
			String tot_inv = df.format(totalInvoice).toString()
			if(tot_inv.contains(",")){
				tot_inv = tot_inv.replaceAll(",", "")
			}
			return tot_inv
		}
		
		public String md_Round_Off(String testcaseid)
		{
			String acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			return md_Round_Off(testcaseid,acct_no)
			logi.logInfo("Enter into the overloading")
		}
		public String md_Round_Off(String testcaseid,String acct_no)
		{
			DecimalFormat d = new DecimalFormat("#.##########");
			ConnectDB database = new ConnectDB();
			String query= "select proc_pymnt_id from payment_auth where acct_no="+ acct_no
			String round_of =database.executeQueryP2(query)
			logi.logInfo("Round off value"+round_of)
			database.closeConnection()
			return (round_of)
		}
		
		
		public String md_Suffix_cc(String testcaseid)
		{
			String card_no=getValueFromRequest("authorize_electronic_payment","//*/cc_number").toString()
			logi.logInfo("Card no"+ card_no)
			String suf_card =card_no.substring(12)
			return (suf_card)
				
		}
		
		// Admin Tools Verification Methods
		
		def getValueFromHttpRequestValue(String apiname,String xpath) {
			def nodeValue = "NoVal"
			String tcid= Constant.TESTCASEID
			String suitename=Constant.TESTSUITENAME
			List testCaseCmb = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
			List httpValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
			
			for(int j=0; j<testCaseCmb.size(); j++) {
				if(testCaseCmb[j].toString().equals(suitename+"-"+tcid+"-"+apiname)) {
					com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = httpValues[j]
					nodeValue = request.getPropertyValue(xpath)
				}
			}
			if(nodeValue == "" || nodeValue == 'NoVal')
			{
				nodeValue = null
			}
			logi.logInfo("Returned Node value is : " + nodeValue + " for the xpath : " + xpath)
			return nodeValue
		}
		
		String md_Verify_PlanDetails(String testcaseid) {
			
			String plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
			def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
			String planDetailsQuery = "SELECT * FROM ARIACORE.CLIENT_PLAN where PLAN_NO= '" + plan_no + "'"
			String dunningPlanQuery = "SELECT DUNNING_PLAN_NO from dunning_plan_assignment where PLAN_NO=" + plan_no + " AND CLIENT_NO=" + client_no
			String result
			ConnectDB db = new ConnectDB()
			logi.logInfo("Before Executing Query " )
			HashMap rs = db.executeQueryP2(planDetailsQuery);
			Constant.lhmResultSet.putAll(db.executeQueryP2(planDetailsQuery))
			Constant.lhmResultSet.put("DUNNING_PLAN_NO", db.executeQueryP2(dunningPlanQuery))
			return getPropertyListContains("plan_group", testcaseid)
		}
		
		String md_VerifyPlanGroups(String testcaseid) {
			
			List planGroupsId = getPropertyListContains("plan_group", testcaseid)
			def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
			String planGroupQuery = "select * from ARIACORE.PLAN_CHANGE_GRP WHERE CLIENT_NO = " + client_no + " AND GROUP_NO in (SELECT GROUP_NO from ARIACORE.PLAN_CHANGE_MAP WHERE CLIENT_NO = " + client_no + " and PLAN_NO = " + getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1) + ")"
			ResultSet result = db.executePlaQuery(planGroupQuery)
			int i = 0
			while (result.next()) {
				Constant.lhmResultSet.putAt("plan_group[" + i + "]" , result.getString("GROUP_NO"))
				Constant.lhmResultSet.putAt("plan_group_id[" + i + "]" , result.getString("CLIENT_PLAN_CHANGE_GROUP_ID"))
				i++
			}
			logi.logInfo(" Plan Group Query " + planGroupQuery)
			return "pass"
		}
		
		String md_VerifyServiceDetails(String testcaseid) {
			List planGroupsId = getPropertyListContains("plan_group", testcaseid)
			def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
			def plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
			String serviceDetailsQuery = "SELECT * FROM ARIACORE.PLAN_SERVICES JOIN ARIACORE.SERVICES ON ARIACORE.PLAN_SERVICES.SERVICE_NO = ARIACORE.SERVICES.SERVICE_NO AND ARIACORE.PLAN_SERVICES.client_no = ARIACORE.SERVICES.custom_to_client_no JOIN ARIACORE.CHART_OF_ACCTS ON ARIACORE.SERVICES.COA_ID = ARIACORE.CHART_OF_ACCTS.COA_ID AND ARIACORE.SERVICES.custom_to_client_no = ARIACORE.CHART_OF_ACCTS.custom_to_client_no WHERE ARIACORE.PLAN_SERVICES.PLAN_NO = " + plan_no + " AND ARIACORE.PLAN_SERVICES.CLIENT_NO = " + client_no + " AND ARIACORE.CHART_OF_ACCTS.COA_ID IN (SELECT COA_ID FROM ARIACORE.SERVICES WHERE SERVICE_NO IN (SELECT SERVICE_NO FROM ARIACORE.PLAN_SERVICES WHERE PLAN_NO = " + plan_no + " AND CLIENT_NO = " + client_no + ")) ORDER BY ARIACORE.PLAN_SERVICES.SERVICE_NO"
	//		String serviceDetailsQuery = "SELECT * FROM ARIACORE.PLAN_SERVICES LEFT OUTER JOIN ARIACORE.SERVICES ON ARIACORE.PLAN_SERVICES.SERVICE_NO=ARIACORE.SERVICES.SERVICE_NO AND ARIACORE.PLAN_SERVICES.CLIENT_NO = ARIACORE.SERVICES.CUSTOM_TO_CLIENT_NO WHERE ARIACORE.PLAN_SERVICES.PLAN_NO=" + plan_no + " AND ARIACORE.PLAN_SERVICES.CLIENT_NO=" + client_no /*+ " ORDER BY ARIACORE.PLAN_SERVICES.SERVICE_NO"*/
			logi.logInfo(" Plan Group Query " + serviceDetailsQuery)
			ResultSet result = db.executePlaQuery(serviceDetailsQuery)
			int i = 0
			while (result.next()) {
				logi.logInfo("Iteration ----------------------------------------------------------------------------------------")
				String service_no = result.getString("SERVICE_NO")
				Constant.lhmResultSet.putAt("service[" + i + "][service_no]" , service_no)
				Constant.lhmResultSet.putAt("service[" + i + "][name]" , result.getString("COMMENTS"))
				Constant.lhmResultSet.putAt("service[" + i + "][client_service_id]" , result.getString("CLIENT_SERVICE_ID"))
				Constant.lhmResultSet.putAt("service[" + i + "][gl_cd]" , result.getString("CLIENT_COA_CODE"))
				String serType = getServiceType(result.getString("RECURRING"), result.getString("USAGE_BASED"), result.getString("IS_CANCELLATION"), result.getString("IS_MIN_FEE"), result.getString("IS_ORDER_BASED"))
				Constant.lhmResultSet.putAt("service[" + i + "][service_type]" , serType)
				Constant.lhmResultSet.putAt("service[" + i + "][pricing_rule]" , getPricingRule(result.getString("TIERED_PRICING_RULE_NO")))
				Constant.lhmResultSet.putAt("service[" + i + "][usage_type]" , result.getString("USAGE_TYPE"))
				i++
			}
	//		databasee.closeConnection()
			logi.logInfo(" Plan Group Query " + serviceDetailsQuery)
			return "pass"
		}
		
		String getServiceType(String recurring, String usageBased, String isCancellation, isMinFee, isOrderBased) {
			String serviceType = "Invalid Service Type"
			if(recurring.equals("1") && usageBased.equals("0") && isCancellation.equals("0") && isMinFee.equals("0") && isOrderBased.equals("0")) {
				logi.logInfo("Recurring")
				serviceType = "Recurring"
			} else if(recurring.equals("0") && usageBased.equals("0") && isCancellation.equals("0") && isMinFee.equals("0") && isOrderBased.equals("0")) {
				logi.logInfo("Activation")
				serviceType = "Activation"
			} else if(recurring.equals("0") && usageBased.equals("0") && isCancellation.equals("1") && isMinFee.equals("0") && isOrderBased.equals("0")) {
				logi.logInfo("Cancellation")
				serviceType = "Cancellation"
			} else if(recurring.equals("0") && usageBased.equals("0") && isCancellation.equals("0") && isMinFee.equals("1") && isOrderBased.equals("0")) {
				logi.logInfo("Minimum Fee")
				serviceType = "Minimum Fee"
			} else if(recurring.equals("0") && usageBased.equals("0") && isCancellation.equals("0") && isMinFee.equals("0") && isOrderBased.equals("1")) {
				logi.logInfo("Order-Based")
				serviceType = "Order-Based"
			} else if(recurring.equals("0") && usageBased.equals("1") && isCancellation.equals("0") && isMinFee.equals("0") && isOrderBased.equals("0")) {
				logi.logInfo("Usage-Based")
				serviceType = "Usage-Based"
			}
			return serviceType
		}
		
		String getPricingRule(String tierPricingRule) {
			String pricingRule = "Invalid Pricing Rule"
			if(tierPricingRule.equals("1")) {
				logi.logInfo("Standard")
				pricingRule = "Standard"
			} else if(tierPricingRule.equals("2")) {
				logi.logInfo("Volume Discount")
				pricingRule = "Volume Discount"
			} else if(tierPricingRule.equals("3")) {
				logi.logInfo("Flat Rate Per Tier")
				pricingRule = "Flat Rate Per Tier"
			}
			return pricingRule
		}
		
		
		String md_Verify_PlanDetalisActual(String testCaseId) {
			
			String plan_name = getValueFromHttpRequestValue("create_new_plan", "plan_name").toString()
		}
		
		List getPropertyListContains(String propertyName, String testCaseId) {
			
			List testCaseCmbRequest = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
			List requestValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
			List<String> propertyList = []
			String nodeValue
			for(int k=0; k<testCaseCmbRequest.size(); k++) {
				if(testCaseCmbRequest[k].toString().equals(testCaseId)) {
					com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = requestValues[k]
					for(prp in request.getPropertyList()) {
						if(prp.name.contains(propertyName)) {
							propertyList.add(prp.name)
						}
					}
				}
			}
			return propertyList
		}
		
		public HashMap getPlanServiceAndTieredPricingUsageCharges(){
			def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String client_no=Constant.mycontext.expand('${Properties#Client_No}')
			ConnectDB database = new ConnectDB();
			String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+"  and usage_based=1 ) order by plan_no, service_no"
			int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and usage_based=1 ) order by plan_no, service_no"))
			ResultSet resultSet = database.executeQuery(query)

			HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
			HashMap<String,String> serviceHash

			String[] plan = new String[count]
			String[] service = new String[count]
			String[] tieredPricing = new String[count]

			int i= 0
			while(resultSet.next()){
				plan[i]=resultSet.getString(1)
				service[i]=resultSet.getString(2)
				tieredPricing[i]=resultSet.getString(3)
				i++
			}
			String plan_no
			int serviceCount=0
			for(int planCount=0;planCount<plan.length;){
				plan_no = plan[planCount]
				serviceHash = new HashMap<String,String> ()
				for(;serviceCount<service.length;serviceCount++){
					if(plan_no.equalsIgnoreCase(plan[serviceCount])){
						serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
						planCount ++
					}
					else
						break
				}
				resultHash.put(plan_no, serviceHash)
				logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
			}
			logi.logInfo "Total Services" + resultHash.toString()
			return resultHash
		}
		public HashMap getPlanServiceAndTieredPricingAllCharges(){
			def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String client_no=Constant.mycontext.expand('${Properties#Client_No}')
			ConnectDB database = new ConnectDB();
			String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" ) order by plan_no, service_no"
			int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" ) order by plan_no, service_no"))
			ResultSet resultSet = database.executeQuery(query)

			HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
			HashMap<String,String> serviceHash

			String[] plan = new String[count]
			String[] service = new String[count]
			String[] tieredPricing = new String[count]

			int i= 0
			while(resultSet.next()){
				plan[i]=resultSet.getString(1)
				service[i]=resultSet.getString(2)
				tieredPricing[i]=resultSet.getString(3)
				i++
			}
			String plan_no
			int serviceCount=0
			for(int planCount=0;planCount<plan.length;){
				plan_no = plan[planCount]
				serviceHash = new HashMap<String,String> ()
				for(;serviceCount<service.length;serviceCount++){
					if(plan_no.equalsIgnoreCase(plan[serviceCount])){
						serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
						planCount ++
					}
					else
						break
				}
				resultHash.put(plan_no, serviceHash)
				logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
			}
			logi.logInfo "Total Services" + resultHash.toString()
			return resultHash
		}
		
		//Payment
		// Get the Actual Invoice amount from the DB directly using single query - Gen_invoice
	
		def md_GEN_INVOICE_PENDING_AMT(String testCaseId)
		{
			logi.logInfo("Inside md_geninvoicependingAmt")
	
			String client_no = Constant.mycontext.expand('${Properties#Client_No}')
			logi.logInfo("Client No = "+client_no)
			String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			logi.logInfo("acct_no = "+acct_no)
			String gen_inv_no=getValueFromResponse('gen_invoice',ExPathRpcEnc.GEN_INVOICE_INVOICENO1).toString()
			logi.logInfo("gen_inv_no new = "+gen_inv_no)
			ConnectDB db = new ConnectDB();
			String query1 = "SELECT amount FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO= "+acct_no+" AND transaction_type=1 and source_no= "+gen_inv_no+" and CLIENT_NO="+client_no
			String geninvamount = db.executeQueryP2(query1)
			logi.logInfo("gen_inv_no Amount = "+geninvamount.toDouble())
	
			return df.format(geninvamount.toDouble()).toString()
		}
	
		
		def md_GEN_INVOICE_PENDING_AMT_NEW(String testCaseId)
		{
			logi.logInfo("Inside md_geninvoicependingAmt")
	
			String client_no = Constant.mycontext.expand('${Properties#Client_No}')
			logi.logInfo("Client No = "+client_no)
			String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			logi.logInfo("acct_no = "+acct_no)
			String gen_inv_no=getValueFromResponse('gen_invoice.a',ExPathRpcEnc.GEN_INVOICE_INVOICENO1).toString()
			logi.logInfo("gen_inv_no new = "+gen_inv_no)
			ConnectDB db = new ConnectDB();
			String query1 = "SELECT amount FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO= "+acct_no+" AND transaction_type=1 and source_no= "+gen_inv_no+" and CLIENT_NO="+client_no
			String geninvamount = db.executeQueryP2(query1)
			logi.logInfo("gen_inv_no Amount = "+geninvamount.toDouble())
	
			return df.format(geninvamount.toDouble()).toString()
		}
		
	//new
		def md_invoice_amount_with_cash_credit (String testCaseId)
		{
			logi.logInfo("Inside md_invoice_amount_with_cash_credit")
	
			String client_no = Constant.mycontext.expand('${Properties#Client_No}')
			logi.logInfo("Client No = "+client_no)
			String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			logi.logInfo("acct_no = "+acct_no)
			ConnectDB db = new ConnectDB();
			
			String query1 ="select (debit - credit) from ariacore.all_invoices where invoice_no = (SELECT max(invoice_no) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+" AND CLIENT_NO="+client_no+") "
			String invamount = db.executeQueryP2(query1)
			String query2 = "SELECT amount from ariacore.all_credits where acct_no= "+acct_no+" and credit_type='C' and client_no="+client_no
			String cash_credit = db.executeQueryP2(query2)
			String total_amount = cash_credit.toDouble() + invamount.toDouble()
		
			logi.logInfo("gen_inv_no Amount = "+invamount.toDouble())
			logi.logInfo("gen_inv_no Amount = "+total_amount.toDouble())
	
			return df.format(total_amount.toDouble()).toString()
		}
		
			
		// Get the Expected Invoice amount from the DB directly using single query - Gen_invoice
		def md_GEN_INVOICE_PENDING_AMT_EXP(String testCaseId)
		{
			logi.logInfo("Calling md_geninvoicependingExpAmt")
	
			String client_no = Constant.mycontext.expand('${Properties#Client_No}')
			logi.logInfo("Client No = "+client_no)
			ConnectDB db = new ConnectDB();
			String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			logi.logInfo("acct_no = "+acct_no)
			String gen_inv_no=getValueFromResponse('gen_invoice',ExPathRpcEnc.GEN_INVOICE_INVOICENO1).toString()
			logi.logInfo("gen_inv_no = "+gen_inv_no)
			String geninvamount
			String invamtwithoutact
			double creditamt=db.executeQueryP2("select NVL(sum(credit),0) from ariacore.gl where acct_no="+acct_no+" and client_no="+client_no+" and invoice_no="+gen_inv_no).toString().toDouble()
			def actinvamount = md_serviceInvoiceActivationAmount(testCaseId)
			logi.logInfo("Activation Amount = "+actinvamount.toDouble())
	
			invamtwithoutact = md_serviceInvoiceAmount(testCaseId)
			logi.logInfo("inv_no Amount without Activation  = "+invamtwithoutact.toDouble())
			geninvamount = invamtwithoutact.toDouble() + actinvamount.toDouble()-creditamt
			logi.logInfo("Final Amount without Alt Bill date = "+geninvamount.toDouble())
	
	
			return df.format(geninvamount.toDouble()).toString()
		}
		
		
		// Get the Expected Invoice amount from the DB directly using single query - Gen_invoice
		def md_GEN_INVOICE_PENDING_AMT_EXP_NEW(String testCaseId)
		{
			logi.logInfo("Calling md_geninvoicependingExpAmt")
	
			String client_no = Constant.mycontext.expand('${Properties#Client_No}')
			logi.logInfo("Client No = "+client_no)
			String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			logi.logInfo("acct_no = "+acct_no)
			String gen_inv_no=getValueFromResponse('gen_invoice.a',ExPathRpcEnc.GEN_INVOICE_INVOICENO1).toString()
			logi.logInfo("gen_inv_no = "+gen_inv_no)
			String geninvamount
			String invamtwithoutact
	
			def actinvamount = md_serviceInvoiceActivationAmount(testCaseId)
			logi.logInfo("Activation Amount = "+actinvamount.toDouble())
	
			invamtwithoutact = md_serviceInvoiceAmount(testCaseId)
			logi.logInfo("inv_no Amount without Activation  = "+invamtwithoutact.toDouble())
			geninvamount = invamtwithoutact.toDouble() + actinvamount.toDouble()
			logi.logInfo("Final Amount without Alt Bill date = "+geninvamount.toDouble())
	
	
			return df.format(geninvamount.toDouble()).toString()
		}
		public String md_IS_BALANCE_DUE_COLLECTED(String tcid)
		{
			String success = "FALSE"
			String client_no = Constant.mycontext.expand('${Properties#Client_No}')
			String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String due_mode_api=getValueFromRequest('settle_account_balance',"//force_balance_scope")
			ConnectDB db = new ConnectDB();
			String due_mode
			String paid_bal
			String due_mode_db = db.executeQueryP2("Select param_val from ariacore.aria_client_params where param_name='AUTO_COLLECT_BALANCE_SCOPE' and client_no="+client_no)
			//String due_mode = db.executeQueryP2("Select param_val from ariacore.aria_client_params where param_name='AUTO_COLLECT_ACCT_BALANCE_SCOPE' and client_no="+client_no)
			String ser_order = Constant.lhmServiceOrderList.get(Constant.TESTCASEID).toString()
			logi.logInfo("ser_order:"+ser_order)

			if(due_mode_api=="NoVal")
			due_mode=due_mode_db
			else
			due_mode=due_mode_api
			logi.logInfo("Final due mode:"+due_mode)
/*			if(due_mode.equals("C"))
			{
				if(!ser_order.contains("VTA_AD"))
				{
					due_mode="F"
				}
			}*/
			if(due_mode.equals("F"))
			{
				logi.logInfo("Due balance mode is FULL")
				
				String acct_bal = library.Constants.Constant.lhmacctBalance.get(acct_no).toString();
				logi.logInfo("Acct Balance is:"+acct_bal)
				String transactionId = getValueFromResponse("settle_account_balance","//*:settle_account_balanceResponse[1]/transaction_id[1]")
				logi.logInfo "final tran id : "+transactionId
				String ctransactionId = getValueFromResponse("collect_from_account","//*:collect_from_accountResponse[1]/proc_payment_id[1]")
				logi.logInfo "final ctran id : "+ctransactionId
				if(transactionId!="NoVal")
				 paid_bal = db.executeQueryP2("Select amount from ariacore.payments where acct_no="+acct_no+" and payment_id ="+transactionId).toString()
				else
				 paid_bal = db.executeQueryP2("Select amount from ariacore.payments where acct_no="+acct_no+" and proc_pymnt_id ="+ctransactionId).toString()
				logi.logInfo("paid Balance is:"+paid_bal)
				
				if(acct_bal.equals(paid_bal))
				{
					success = "TRUE"
				}
			}
			else if(due_mode.equals("C"))
			{
				logi.logInfo("Due balance mode is Current")
				//FIFO
				String bal_amt = db.executeQueryP2("Select debit from ariacore.gl where acct_no="+acct_no+" and invoice_no = (Select min(invoice_no) from ariacore.gl where acct_no="+acct_no+")").toString()
				logi.logInfo("Current Balance is:"+bal_amt)
				
                String transactionId = getValueFromResponse("settle_account_balance","//*:settle_account_balanceResponse[1]/transaction_id[1]")
				logi.logInfo "final tran id : "+transactionId
				String ctransactionId = getValueFromResponse("collect_from_account","//*:collect_from_accountResponse[1]/proc_payment_id[1]")
				logi.logInfo "final ctran id : "+ctransactionId
				if(transactionId!="NoVal")
				paid_bal = db.executeQueryP2("Select amount from ariacore.payments where acct_no="+acct_no+" and payment_id ="+transactionId).toString()
				else
				 paid_bal = db.executeQueryP2("Select amount from ariacore.payments where acct_no="+acct_no+" and proc_pymnt_id ="+ctransactionId).toString()
				logi.logInfo("paid Balance is:"+paid_bal)
				
				if(bal_amt.equals(paid_bal))
				{
					success = "TRUE"
				}
			}
		
			return success
		}
		
		public String md_COLLECT_FROM_ACCOUNT_PROCPAYMENTID(String tcid)
		{
			ConnectDB db = new ConnectDB();
			String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String proc_pay_id = db.executeQueryP2("SELECT MAX(PROC_PYMNT_ID) FROM PAYMENTS WHERE ACCT_NO ="+acct_no).toString()
			
			return proc_pay_id
		}
		
		public String md_SETTLED_ACCT_BALANCE (String tcid)
		{
			ConnectDB db = new ConnectDB();
			String transactionId = getValueFromResponse("settle_account_balance","//*:settle_account_balanceResponse[1]/transaction_id[1]")
			String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String paid_bal = db.executeQueryP2("Select amount from ariacore.payments where acct_no="+acct_no+" and payment_id ="+transactionId).toString()
			logi.logInfo("paid Balance is:"+paid_bal)
			 
			 return paid_bal
		}
		
		public String md_BALANCE_TO_BE_PAID(String tcid)
		{
			String client_no = Constant.mycontext.expand('${Properties#Client_No}')
			String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			ConnectDB db = new ConnectDB();
			String expected_bal=""
			String due_mode_api=getValueFromRequest('settle_account_balance',"//force_balance_scope")
			String due_mode
			//String due_mode = db.executeQueryP2("Select param_val from ariacore.aria_client_params where param_name='AUTO_COLLECT_ACCT_BALANCE_SCOPE' and client_no="+client_no).toString();
			String due_mode_db = db.executeQueryP2("Select param_val from ariacore.aria_client_params where param_name='AUTO_COLLECT_BALANCE_SCOPE' and client_no="+client_no).toString();
			
			String ser_order = Constant.lhmServiceOrderList.get(Constant.TESTCASEID).toString()
			logi.logInfo("ser_order:"+ser_order)
			
			if(due_mode_api=="NoVal")
			due_mode=due_mode_db
			else
			due_mode=due_mode_api
			logi.logInfo("Final due mode:"+due_mode)
/*			if(due_mode.equals("C"))
			{
				if(!ser_order.contains("VTA_AD"))
				{
					due_mode="F"
				}
			}
			*/
			if(due_mode.equals("F"))
			{
				logi.logInfo("Due balance mode is Full")
				
				expected_bal = library.Constants.Constant.lhmacctBalance.get(acct_no).toString();
				logi.logInfo("Acct Balance is:"+expected_bal)
			
			}
			else if(due_mode.equals("C"))
			{
				logi.logInfo("Due balance mode is Current")
				//FIFO
				expected_bal = db.executeQueryP2("Select debit from ariacore.gl where acct_no="+acct_no+" and invoice_no = (Select min(invoice_no) from ariacore.gl where acct_no="+acct_no+")").toString()
				logi.logInfo("Current Balance is:"+expected_bal)
			}
			
			return expected_bal
		}
String md_VerifySuppPlanMap(String testcaseid) {
			
			List planGroupsId = getPropertyListContains("plan_group", testcaseid)
			for(prp in planGroupsId) {
				logi.logInfo("****************************************************" + prp)
			}
			def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
			String planMapQuery = "SELECT * FROM ARIACORE.PLAN_SUPP_PLAN_MAP WHERE CLIENT_NO=" + client_no + " AND MASTER_PLAN_NO = " + getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
			ResultSet result = db.executePlaQuery(planMapQuery)
			int i = 0
			while (result.next()) {
				Constant.lhmResultSet.putAt("child_plans[" + i + "]" , result.getString("SUPP_PLAN_NO"))
				i++
			}
			logi.logInfo(" Plan Group Query " + planMapQuery)
			return "pass"
		}
		

		
String md_VerifySuppFieldValuesExp(String testcaseid) {
			def plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
			String serviceDetailsQuery = "SELECT * FROM CLIENT_PLAN_SUPP_FIELDS_VAL WHERE CLIENT_NO=3285206 AND PLAN_NO = " + plan_no + " ORDER BY FIELD_NO";
			Map<String, List<String>> suppFieldValues = new TreeMap<String, List<String>>();
			logi.logInfo("Before query Flat Rate Psssssssssssssssssssssssser Tier")
			ResultSet rs = db.executePlaQuery(serviceDetailsQuery);
			logi.logInfo("Flat Rate Psssssssssssssssssssssssser Tier")
			while(rs.next()) {
				if(!suppFieldValues.containsKey(rs.getString("FIELD_NO"))) {
					List<String> valueList = new ArrayList<>();
					valueList.add(rs.getString("VALUE_TEXT"));
					suppFieldValues.put(rs.getString("FIELD_NO") , valueList);
				} else {
					suppFieldValues.get(rs.getString("FIELD_NO")).add(rs.getString("VALUE_TEXT"));
				}
			}
			
			return suppFieldValues.toString()
		}
		
		String md_VerifySuppFieldValuesAct(String testcaseid) {
			
			Map<String, List<String>> suppFieldValues = new TreeMap<String, List<String>>();
			
			int i = 0
			while(!getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_no]").equals(null)) {
				logi.logInfo(" Supp Field  - " + getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_no]"))
				int j =0
				List<String> valueList = new ArrayList<>();
				while(!getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]").equals(null)) {
					logi.logInfo(" Supp Values - " + getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]"))
					
					valueList.add(getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]"));
					j++
				}
				suppFieldValues.put(getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_no]"), valueList )
				i++
			}
			
			return suppFieldValues.toString()
		}
		
		public String getDaysToAdvance(String acct_no)
		{
			logi.logInfo "Entered into getDaysToAdvance "
			ConnectDB db=new ConnectDB()
			String apiname="create_acct_complete"
			def acctno=getValueFromResponse(apiname,ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String created_date_q="select created from ariacore.acct WHERE ACCT_NO="+acctno
			def created_date = db.executeQueryP2(created_date_q)
			def retro_date = getValueFromRequest(apiname,"//retroactive_start_date")
			def client_no = getValueFromRequest(apiname,"//client_no")
			String plan_no_q1 = "select plan_no from ariacore.acct where acct_no="+acctno+"  and client_no="+client_no
			String plan_no=db.executeQueryP2(plan_no_q1)
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			Calendar now = Calendar.getInstance();
	
			if(!retro_date.equals("NoVal"))
			{
				created_date=retro_date
			}
			else{
				created_date=created_date
			}
	
			Date date = dateFormat.parse(created_date);
			logi.logInfo " created date set as " +created_date
			now.setTime(date)
	
			String bill_interval_q="select billing_interval from ariacore.client_plan where plan_no="+plan_no+" and client_no="+client_no
			String bill_interval=db.executeQueryP2(bill_interval_q)
			logi.logInfo " bill interval is " + bill_interval
			def lag_name,actual_bill_date,new_bill_date
	
			if(bill_interval.equals("1"))
				lag_name="MONTHLY_BILL_LAG_DAYS"
			else if(bill_interval.equals("3"))
				lag_name="QUARTERLY_BILL_LAG_DAYS"
			else if(bill_interval.equals("6"))
				lag_name="SEMI_ANNUAL_BILL_LAG_DAYS"
			else if(bill_interval.equals("12"))
				lag_name="ANNUAL_BILL_LAG_DAYS"
			else
				lag_name="CUSTOM_BILL_LAG_DAYS"
	
			String bill_lag_q="Select NVL(param_val,0) from ariacore.aria_client_params where client_no ="+client_no+" and param_name='"+lag_name+"'"
			ResultSet resultSet =db.executePlaQuery(bill_lag_q);
			String clientLagDays = "0";
			if(resultSet.next())
			{
				clientLagDays=resultSet.getString(1)
			}			
			resultSet.close();
			logi.logInfo "clientLagDays" + clientLagDays
			//if(bill_lag_day == null)
				//bill_lag_day=0
			
			String cur_time=dateFormat.format(now.getTime())	
			int bill_inter=Integer.parseInt(bill_interval)
			int invoiceCount=db.executeQueryP2("Select count(*) from ariacore.gl where acct_no="+acctno).toInteger()
			logi.logInfo "invoiceCount :: "+invoiceCount
			 if(invoiceCount >=0)
			now.add(Calendar.MONTH,bill_inter)
			logi.logInfo "Date after adding bill interval is " + dateFormat.format(now.getTime())
			
			//calcuating pay method lag days
			String paymethodLagDays_q= "select NVL(lag_days,0) from ARIACORE.CLIENT_PAYMENT_METHOD  where client_no=" + client_no+" and method_id=(select pay_method from ariacore.acct where acct_no="+acctno +")"
			ResultSet rs =db.executePlaQuery(paymethodLagDays_q);
			
			String paymethodLagDays = "0";
			if(rs.next())
			{
				paymethodLagDays=rs.getString(1)
			}
			rs.close();
			logi.logInfo"paymethodLagDays :: "+paymethodLagDays.toString()
			
			
			String finaldaystoAD
		   if(paymethodLagDays.toInteger() != 0)
			 finaldaystoAD =  paymethodLagDays.toInteger()
		   else
		   finaldaystoAD =  clientLagDays

	
			int bill_lag=Integer.parseInt(finaldaystoAD)
			now.add(Calendar.DATE,bill_lag)
			logi.logInfo "Date after adding bill lag day is " + dateFormat.format(now.getTime())
	
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-YYYY");
			String date1 = format1.format(now.getTime());
			logi.logInfo " new bill date after adding bill interval and bill lag day is  " + format1.format(now.getTime())
			int daystoadvance = getDaysBetween(dateFormat.parse(created_date),now.getTime())
			return daystoadvance
		}
		
		


	/*
	public HashMap getPlanServiceAndTieredPricing(){
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" OR custom_to_client_no IS NULL) and recurring = 1) order by plan_no, service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" OR custom_to_client_no IS NULL) and recurring = 1) order by plan_no, service_no"))
		ResultSet resultSet = database.executeQuery(query)
		HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		HashMap<String,String> serviceHash

		String[] plan = new String[count]
		String[] service = new String[count]
		String[] tieredPricing = new String[count]

		int i= 0
		while(resultSet.next()){
			plan[i]=resultSet.getString(1)
			service[i]=resultSet.getString(2)
			tieredPricing[i]=resultSet.getString(3)
			i++
		}
		String plan_no
		int serviceCount=0
		for(int planCount=0;planCount<plan.length;){
			plan_no = plan[planCount]
			serviceHash = new HashMap<String,String> ()
			for(;serviceCount<service.length;serviceCount++){
				if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					
					logi.logInfo "Plan : "+ plan_no
					
					logi.logInfo "Services of Plan : "+ serviceHash
					planCount ++
				}
				else
					break
				
			}
			resultHash.put(plan_no, serviceHash)
			logi.logInfo "999999999999 "+resultHash
			
		}
		logi.logInfo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"+resultHash
		return resultHash
	}
	*/
	public HashMap getPlanServiceAndTieredPricing_CreateDate(){
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" and create_date in (select plan_date from ariacore.acct where acct_no = "+acct_no+") GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" OR custom_to_client_no IS NULL) and recurring = 1) order by plan_no, service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" and create_date in (select plan_date from ariacore.acct where acct_no = "+acct_no+") GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" OR custom_to_client_no IS NULL) and recurring = 1) order by plan_no, service_no"))
		ResultSet resultSet = database.executeQuery(query)
		HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		HashMap<String,String> serviceHash

		String[] plan = new String[count]
		String[] service = new String[count]
		String[] tieredPricing = new String[count]

		int i= 0
		while(resultSet.next()){
			plan[i]=resultSet.getString(1)
			service[i]=resultSet.getString(2)
			tieredPricing[i]=resultSet.getString(3)
			i++
		}
		String plan_no
		int serviceCount=0
		for(int planCount=0;planCount<plan.length;){
			plan_no = plan[planCount]
			serviceHash = new HashMap<String,String> ()
			for(;serviceCount<service.length;serviceCount++){
				if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					
					logi.logInfo "Plan : "+ plan_no
					
					logi.logInfo "Services of Plan : "+ serviceHash
					planCount ++
				}
				else
					break
				
			}
			resultHash.put(plan_no, serviceHash)
			logi.logInfo "999999999999 "+resultHash
			
		}
		logi.logInfo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"+resultHash
		return resultHash
	}

	public HashMap getPlanServiceAndTieredPricing_Not_In_CreateDate(){
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" and create_date not in (select plan_date from ariacore.acct where acct_no = "+acct_no+") GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" OR custom_to_client_no IS NULL) and recurring = 1) order by plan_no, service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" and create_date not in (select plan_date from ariacore.acct where acct_no = "+acct_no+") GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" OR custom_to_client_no IS NULL) and recurring = 1) order by plan_no, service_no"))
		ResultSet resultSet = database.executeQuery(query)
		HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		HashMap<String,String> serviceHash

		String[] plan = new String[count]
		String[] service = new String[count]
		String[] tieredPricing = new String[count]

		int i= 0
		while(resultSet.next()){
			plan[i]=resultSet.getString(1)
			service[i]=resultSet.getString(2)
			tieredPricing[i]=resultSet.getString(3)
			i++
		}
		String plan_no
		int serviceCount=0
		for(int planCount=0;planCount<plan.length;){
			plan_no = plan[planCount]
			serviceHash = new HashMap<String,String> ()
			for(;serviceCount<service.length;serviceCount++){
				if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					
					logi.logInfo "Plan : "+ plan_no
					
					logi.logInfo "Services of Plan : "+ serviceHash
					planCount ++
				}
				else
					break
				
			}
			resultHash.put(plan_no, serviceHash)
			logi.logInfo "999999999999 "+resultHash
			
		}
		logi.logInfo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"+resultHash
		return resultHash
	}

	
	
	public String md_checkInvoice(String testCaseId){
		
				String client_no=Constant.mycontext.expand('${Properties#Client_No}')
				String acct_no=Constant.mycontext.expand('${Properties#AccountNo}')
				logi.logInfo "check invoice begins"
				ConnectDB database = new ConnectDB();
				SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
				String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL"
				String currentVirtualTime = database.executeQueryP2(Query);
				
				
				logi.logInfo "Current Virtual Time : "+ currentVirtualTime
		
				// Stores the current virtual time in the specified format
				Date currentDate = simpleFormat.parse(currentVirtualTime);
				logi.logInfo "Cureent VTTttttttttt"+currentDate
				Date acctStartDate = getBillStartDate(client_no, acct_no)
				
				logi.logInfo "AccccccccccccctStartDate"+acctStartDate
				// Stores the account created time in the specified format
				int daysInvoice1 = getDaysBetween(acctStartDate, currentDate)
		
				logi.logInfo "daysInvoice: "+daysInvoice1
				HashMap<String, HashMap<String,String>> resultHash
				
				
				resultHash = getPlanServiceAndTieredPricing()
				
				Iterator iterator = resultHash.entrySet().iterator()
				int invoice = 0
		
				while (iterator.hasNext()) {
					//Uvais
					int daysInvoice = daysInvoice1
					Map.Entry pairs = (Map.Entry) iterator.next()
					String plan_no = pairs.getKey().toString()
		
					int index = Integer.parseInt(database.executeQueryP2("select distinct supp_plan_ind from ariacore.client_plan where plan_no ="+plan_no+" and client_no = "+client_no))
					int no_of_units =0
					if(index==0)
						no_of_units = Integer.parseInt(getValueFromRequest("create_acct_complete","//master_plan_units"))
					else {
						String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
						String[] noOfUnits = getSuppPlanUnitsforAcct(acct_no,client_no) 		//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
						String[] supp_plans = getSuppPlansforAcct(acct_no,client_no)//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")
						logi.logInfo "supp_plans.length : "+supp_plans.length
						for( int i=0;i<supp_plans.length;i++) {
							logi.logInfo "supp_plans.length1 : "+supp_plans[i]
							logi.logInfo "supp_plans.length 2: "+plan_no
							if(supp_plans[i].toString().equalsIgnoreCase(plan_no)){
								logi.logInfo "no of plan "+noOfUnits[i]
								no_of_units = Integer.parseInt(noOfUnits[i])
								break
							}
						}
					}
		
					HashMap<String,String> value = pairs.getValue()
					int billing_interval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ plan_no+ "and client_no="+client_no))
					logi.logInfo "Services and Tiered Pricing in a plan : "+plan_no +" are "+ value.toString()
					logi.logInfo "Billing Interval of a plan : "+plan_no +" :  "+ billing_interval
					Date planEndDate = addMonthsFromGivenDate(acctStartDate,billing_interval)
					logi.logInfo "AccccccccccccctStartDate"+acctStartDate
					logi.logInfo "DurationDateeeeeeeee "+planEndDate
					
					int durationOfPlan = getDaysBetween(acctStartDate, planEndDate)
					logi.logInfo "daysInvoice: "+daysInvoice
					logi.logInfo "durationOfPlan : "+plan_no +" :  "+  durationOfPlan
					//Uvais
					int billlagdays = Integer.parseInt(getBillLagDays(billing_interval,client_no))
					logi.logInfo "billlagdays: "+billlagdays
					daysInvoice = daysInvoice + (billlagdays*-1)
					logi.logInfo "daysInvoice: "+daysInvoice
					//Uvais end
					if(daysInvoice>=durationOfPlan||daysInvoice>=(durationOfPlan-1)){
						Iterator service = value.entrySet().iterator()
						logi.logInfo "Invoice calculation for a plan : "+plan_no
						while(service.hasNext()){
							Map.Entry servicePairs = (Map.Entry) service.next()
							String service_no = servicePairs.getKey().toString()
							logi.logInfo "Invoice calculation for a service : "+service_no
							int seq_no=1;
							String tieredPricing = servicePairs.getValue().toString()
		
							int tieredPricingInt = Integer.parseInt(tieredPricing)
							logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
							switch(tieredPricingInt){
								case 1:
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "))
									invoice  = invoice+Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									seq_no++;
									break;
								case 2 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and (to_unit >="+no_of_units+" and from_unit<"+no_of_units+") order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
								case 3 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and (to_unit >="+no_of_units+" and from_unit<"+no_of_units+") order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
							}
						}
					}
					else
						logi.logInfo 'Else loop'
				}
				return invoice
			}
	
	public String md_checkInvoice_ByDate(String testCaseId,String acctCreate){

		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		String acct_no=Constant.mycontext.expand('${Properties#AccountNo}')
		logi.logInfo "check invoice begins"
		ConnectDB database = new ConnectDB();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL"
		String currentVirtualTime = database.executeQueryP2(Query);
		
		
		logi.logInfo "Current Virtual Time : "+ currentVirtualTime

		// Stores the current virtual time in the specified format
		Date currentDate = simpleFormat.parse(currentVirtualTime);
		logi.logInfo "Cureent VTTttttttttt"+currentDate
		Date acctStartDate = getBillStartDate(client_no, acct_no)
		
		logi.logInfo "AccccccccccccctStartDate"+acctStartDate
		// Stores the account created time in the specified format
		int daysInvoice1 = getDaysBetween(acctStartDate, currentDate)

		logi.logInfo "daysInvoice: "+daysInvoice1
		HashMap<String, HashMap<String,String>> resultHash
		if(acctCreate.equalsIgnoreCase("acct_created_date")){
		resultHash = getPlanServiceAndTieredPricing_CreateDate()
		}
		if(acctCreate.equalsIgnoreCase("acct_created_date_not")){
		resultHash = getPlanServiceAndTieredPricing_Not_In_CreateDate()
		}
		Iterator iterator = resultHash.entrySet().iterator()
		int invoice = 0

		while (iterator.hasNext()) {
			//Uvais
			int daysInvoice = daysInvoice1
			Map.Entry pairs = (Map.Entry) iterator.next()
			String plan_no = pairs.getKey().toString()

			int index = Integer.parseInt(database.executeQueryP2("select distinct supp_plan_ind from ariacore.client_plan where plan_no ="+plan_no+" and client_no = "+client_no))
			int no_of_units =0
			if(index==0)
				no_of_units = Integer.parseInt(getValueFromRequest("create_acct_complete","//master_plan_units"))
			else {
				String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
				String[] noOfUnits = getSuppPlanUnitsforAcct(acct_no,client_no) 		//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
				String[] supp_plans = getSuppPlansforAcct(acct_no,client_no)//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")

				for( int i=0;i<supp_plans.length;i++) {
					if(supp_plans[i].toString().equalsIgnoreCase(plan_no)){
						logi.logInfo "no of plan "+noOfUnits[i]
						no_of_units = Integer.parseInt(noOfUnits[i])
						break
					}
				}
			}

			HashMap<String,String> value = pairs.getValue()
			int billing_interval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ plan_no+ "and client_no="+client_no))
			logi.logInfo "Services and Tiered Pricing in a plan : "+plan_no +" are "+ value.toString()
			logi.logInfo "Billing Interval of a plan : "+plan_no +" :  "+ billing_interval
			Date planEndDate = addMonthsFromGivenDate(acctStartDate,billing_interval)
			logi.logInfo "AccccccccccccctStartDate"+acctStartDate
			logi.logInfo "DurationDateeeeeeeee "+planEndDate
			
			int durationOfPlan = getDaysBetween(acctStartDate, planEndDate)
			logi.logInfo "daysInvoice: "+daysInvoice
			logi.logInfo "durationOfPlan : "+plan_no +" :  "+  durationOfPlan
			//Uvais
			int billlagdays = Integer.parseInt(getBillLagDays(billing_interval,client_no))
			logi.logInfo "billlagdays: "+billlagdays
			daysInvoice = daysInvoice + (billlagdays*-1)
			logi.logInfo "daysInvoice: "+daysInvoice
			//Uvais end
			if(daysInvoice>=durationOfPlan||daysInvoice>=(durationOfPlan-1)){
				Iterator service = value.entrySet().iterator()
				logi.logInfo "Invoice calculation for a plan : "+plan_no
				while(service.hasNext()){
					Map.Entry servicePairs = (Map.Entry) service.next()
					String service_no = servicePairs.getKey().toString()
					logi.logInfo "Invoice calculation for a service : "+service_no
					int seq_no=1;
					String tieredPricing = servicePairs.getValue().toString()

					int tieredPricingInt = Integer.parseInt(tieredPricing)
					logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
					switch(tieredPricingInt){
						case 1:
							logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "))
							invoice  = invoice+Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "))
							logi.logInfo "After adding with invoice " + invoice
							seq_no++;
							break;
						case 2 :
							logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
							invoice  = invoice + Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
							logi.logInfo "After adding with invoice " + invoice
							break;
						case 3 :
							logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
							invoice  = invoice + Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
							logi.logInfo "After adding with invoice " + invoice
							break;
					}
				}
			}
			else
				logi.logInfo 'Else loop'
		}
		return invoice
	}

	public String md_checkInvoice_CreatedDatePlan(String testCaseId)
	{
		md_checkInvoice_ByDate(testCaseId,"acct_created_date")
	}
	
	public String md_checkInvoice_NotinCreatedDatePlan(String testCaseId)
	{
		md_checkInvoice_ByDate(testCaseId,"acct_created_date_not")
	}
	
	public String getBillLagDays(int billing_interval,String client_no){
		logi.logInfo "check invoice begins"
		ConnectDB database = new ConnectDB();
		String query= null;
		switch(billing_interval)
		{
			case 1: logi.logInfo "Plan billing interval was monthly"
			query = "select PARAM_VAL from ariacore.all_client_params where client_no="+client_no+" and param_name='MONTHLY_BILL_LAG_DAYS'"
			break;
			case 3: logi.logInfo "Plan billing interval was Quarterly"
			query = "select PARAM_VAL from ariacore.all_client_params where client_no="+client_no+" and param_name='QUARTERLY_BILL_LAG_DAYS'"
			break;
			case 6: logi.logInfo "Plan billing interval was semi annual"
			query = "select PARAM_VAL from ariacore.all_client_params where client_no="+client_no+" and param_name='SEMI_ANNUAL_BILL_LAG_DAYS'"
			break;
			case 12: logi.logInfo "Plan billing interval was annual"
			query = "select PARAM_VAL from ariacore.all_client_params where client_no="+client_no+" and param_name='ANNUAL_BILL_LAG_DAYS'"
			break;
			default: logi.logInfo "Plan billing interval was custom"
			query = "select PARAM_VAL from ariacore.all_client_params where client_no="+client_no+" and param_name='CUSTOM_BILL_LAG_DAYS'"
			break;
		}
		
		return database.executeQueryP2(query)
	}

	public String[] getSuppPlanUnitsforAcct(String acctNo,String clientNo){
		logi.logInfo "check invoice begins"
		ConnectDB database = new ConnectDB();
		String query= "select supp_plan_recurring_factor from ariacore.acct_supp_plan_map where acct_no = "+acctNo+" and status_cd = 1 and client_no="+clientNo+" order by supp_plan_no";
		return database.executeQueryReturnValuesAsArray(query)
	}
	
	public String[] getSuppPlansforAcct(String acctNo,String clientNo){
		logi.logInfo "check invoice begins"
		ConnectDB database = new ConnectDB();
		String query= "select supp_plan_no from ariacore.acct_supp_plan_map where acct_no = "+acctNo+" and status_cd = 1 and client_no="+clientNo+" order by supp_plan_no";
		return database.executeQueryReturnValuesAsArray(query)
	}
	
	
	
	public String md_checkInvoice_full_Invoice(String testCaseId){
		
				String client_no=Constant.mycontext.expand('${Properties#Client_No}')
				String acct_no=Constant.mycontext.expand('${Properties#AccountNo}')
				logi.logInfo "check invoice begins"
				ConnectDB database = new ConnectDB();
				SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
				String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL"
				String currentVirtualTime = database.executeQueryP2(Query);
				
				
				logi.logInfo "Current Virtual Time : "+ currentVirtualTime
		
				// Stores the current virtual time in the specified format
				Date currentDate = simpleFormat.parse(currentVirtualTime);
				logi.logInfo "Cureent VTTttttttttt"+currentDate
				Date acctStartDate = getBillStartDate(client_no, acct_no)
				
				logi.logInfo "AccccccccccccctStartDate"+acctStartDate
				// Stores the account created time in the specified format
				int daysInvoice1 = getDaysBetween(acctStartDate, currentDate)
		
				logi.logInfo "daysInvoice: "+daysInvoice1
				HashMap<String, HashMap<String,String>> resultHash
				
				
				resultHash = getPlanServiceAndTieredPricing()
				
				Iterator iterator = resultHash.entrySet().iterator()
				int invoice = 0
		
				while (iterator.hasNext()) {
					//Uvais
					int daysInvoice = daysInvoice1
					Map.Entry pairs = (Map.Entry) iterator.next()
					String plan_no = pairs.getKey().toString()
		
					int index = Integer.parseInt(database.executeQueryP2("select distinct supp_plan_ind from ariacore.client_plan where plan_no ="+plan_no+" and client_no = "+client_no))
					int no_of_units =0
					if(index==0)
						no_of_units = Integer.parseInt(getValueFromRequest("create_acct_complete","//master_plan_units"))
					else {
						String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
						String[] noOfUnits = getSuppPlanUnitsforAcct(acct_no,client_no) 		//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
						String[] supp_plans = getSuppPlansforAcct(acct_no,client_no)//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")
						logi.logInfo "supp_plans.length : "+supp_plans.length
						for( int i=0;i<supp_plans.length;i++) {
							logi.logInfo "supp_plans.length1 : "+supp_plans[i]
							logi.logInfo "supp_plans.length 2: "+plan_no
							if(supp_plans[i].toString().equalsIgnoreCase(plan_no)){
								logi.logInfo "no of plan "+noOfUnits[i]
								no_of_units = Integer.parseInt(noOfUnits[i])
								break
							}
						}
					}
		
					HashMap<String,String> value = pairs.getValue()
					int billing_interval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ plan_no+ "and client_no="+client_no))
					logi.logInfo "Services and Tiered Pricing in a plan : "+plan_no +" are "+ value.toString()
					logi.logInfo "Billing Interval of a plan : "+plan_no +" :  "+ billing_interval
					Date planEndDate = addMonthsFromGivenDate(acctStartDate,billing_interval)
					logi.logInfo "AccccccccccccctStartDate"+acctStartDate
					logi.logInfo "DurationDateeeeeeeee "+planEndDate
					
					int durationOfPlan = getDaysBetween(acctStartDate, planEndDate)
					logi.logInfo "daysInvoice: "+daysInvoice
					logi.logInfo "durationOfPlan : "+plan_no +" :  "+  durationOfPlan
					//Uvais
					int billlagdays = Integer.parseInt(getBillLagDays(billing_interval,client_no))
					logi.logInfo "billlagdays: "+billlagdays
					daysInvoice = daysInvoice + (billlagdays*-1)
					logi.logInfo "daysInvoice: "+daysInvoice
					//Uvais end
					
						Iterator service = value.entrySet().iterator()
						logi.logInfo "Invoice calculation for a plan : "+plan_no
						while(service.hasNext()){
							Map.Entry servicePairs = (Map.Entry) service.next()
							String service_no = servicePairs.getKey().toString()
							logi.logInfo "Invoice calculation for a service : "+service_no
							int seq_no=1;
							String tieredPricing = servicePairs.getValue().toString()
		
							int tieredPricingInt = Integer.parseInt(tieredPricing)
							logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
							switch(tieredPricingInt){
								case 1:
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "))
									invoice  = invoice+Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									seq_no++;
									break;
								case 2 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and (to_unit >="+no_of_units+" and from_unit<"+no_of_units+") order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
								case 3 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and (to_unit >="+no_of_units+" and from_unit<"+no_of_units+") order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
							}
						}
					
				}
				return invoice
			}
	
	
	
	public String md_checkInvoicerProrated(String testCaseId){
		
				String client_no=Constant.mycontext.expand('${Properties#Client_No}')
				String acct_no=Constant.mycontext.expand('${Properties#AccountNo}')
				logi.logInfo "check invoice begins"
				ConnectDB database = new ConnectDB();
				SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
				String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL"
				String currentVirtualTime = database.executeQueryP2(Query);
				
				
				logi.logInfo "Current Virtual Time : "+ currentVirtualTime
		
				// Stores the current virtual time in the specified format
				Date currentDate = simpleFormat.parse(currentVirtualTime);
				Calendar cal = Calendar.getInstance();
				cal.setTime(currentDate);
				int numDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
				logi.logInfo "Cureent VTTttttttttt"+currentDate
				ResultSet rs;
				rs = database.executeQuery("SELECT NEXT_BILL_DATE FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no)//getBillStartDate(client_no, acct_no)
				rs.next();
					Date acctBillDate = simpleFormat.parse(rs.getString(1));
				
				logi.logInfo "Acct Bill date  :"+acctBillDate
				// Stores the account created time in the specified format
				int daysInvoice1 = getDaysBetween(currentDate, acctBillDate)
		
				logi.logInfo "daysInvoice: "+daysInvoice1
				HashMap<String, HashMap<String,String>> resultHash
				
				
				resultHash = getPlanServiceAndTieredPricing()
				
				Iterator iterator = resultHash.entrySet().iterator()
				int invoice = 0
				int durationOfPlan
				int daysInvoice
				while (iterator.hasNext()) {
					//Uvais
					daysInvoice = daysInvoice1
					Map.Entry pairs = (Map.Entry) iterator.next()
					String plan_no = pairs.getKey().toString()
		
					int index = Integer.parseInt(database.executeQueryP2("select distinct supp_plan_ind from ariacore.client_plan where plan_no ="+plan_no+" and client_no = "+client_no))
					int no_of_units =0
					if(index==0)
						no_of_units = Integer.parseInt(getValueFromRequest("create_acct_complete","//master_plan_units"))
					else {
						String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
						String[] noOfUnits = getSuppPlanUnitsforAcct(acct_no,client_no) 		//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
						String[] supp_plans = getSuppPlansforAcct(acct_no,client_no)//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")
						logi.logInfo "supp_plans.length : "+supp_plans.length
						for( int i=0;i<supp_plans.length;i++) {
							logi.logInfo "supp_plans.length1 : "+supp_plans[i]
							logi.logInfo "supp_plans.length 2: "+plan_no
							if(supp_plans[i].toString().equalsIgnoreCase(plan_no)){
								logi.logInfo "no of plan "+noOfUnits[i]
								no_of_units = Integer.parseInt(noOfUnits[i])
								break
							}
						}
					}
		
					HashMap<String,String> value = pairs.getValue()
					int billing_interval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ plan_no+ "and client_no="+client_no))
					logi.logInfo "Services and Tiered Pricing in a plan : "+plan_no +" are "+ value.toString()
					logi.logInfo "Billing Interval of a plan : "+plan_no +" :  "+ billing_interval
					Date planEndDate = addMonthsFromGivenDate(acctBillDate,billing_interval)
					logi.logInfo "AccccccccccccctStartDate"+acctBillDate
					logi.logInfo "DurationDateeeeeeeee "+planEndDate
					
					durationOfPlan = getDaysBetween(acctBillDate, planEndDate)
					logi.logInfo "daysInvoice: "+daysInvoice
					logi.logInfo "durationOfPlan : "+plan_no +" :  "+  durationOfPlan
					//Uvais
					int billlagdays = Integer.parseInt(getBillLagDays(billing_interval,client_no))
					logi.logInfo "billlagdays: "+billlagdays
					daysInvoice = daysInvoice + (billlagdays*-1)
					logi.logInfo "daysInvoice: "+daysInvoice
					//Uvais end
					//if(daysInvoice>=durationOfPlan||daysInvoice>=(durationOfPlan-1)){
						Iterator service = value.entrySet().iterator()
						logi.logInfo "Invoice calculation for a plan : "+plan_no
						while(service.hasNext()){
							Map.Entry servicePairs = (Map.Entry) service.next()
							String service_no = servicePairs.getKey().toString()
							logi.logInfo "Invoice calculation for a service : "+service_no
							int seq_no=1;
							String tieredPricing = servicePairs.getValue().toString()
		
							int tieredPricingInt = Integer.parseInt(tieredPricing)
							logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
							switch(tieredPricingInt){
								case 1:
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "))
									invoice  = invoice+Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									seq_no++;
									break;
								case 2 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and (to_unit >="+no_of_units+" and from_unit<"+no_of_units+") order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
								case 3 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and (to_unit >="+no_of_units+" and from_unit<"+no_of_units+") order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
							}
						}
					/*}
					else
						logi.logInfo 'Else loop'*/
				}
				double pCharge = (invoice *(daysInvoice/numDays)).toDouble()				
				DecimalFormat df = new DecimalFormat("#.##");
				return df.format(pCharge.trunc(2)).toString()
				
			}
	
	
	
	def md_assign_supp_plan_invoice_item(String testcaseId,int lineItem) {
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		ConnectDB database = new ConnectDB();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL"
		String currentVirtualTime = database.executeQueryP2(Query);
		
		
		logi.logInfo "Current Virtual Time : "+ currentVirtualTime

		Date currentDate = simpleFormat.parse(currentVirtualTime);
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		
		
		int numDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		logi.logInfo "Cureent VTTttttttttt"+currentDate
		ResultSet rs;
		rs = database.executeQuery("SELECT NEXT_BILL_DATE FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no)//getBillStartDate(client_no, acct_no)
		rs.next();
			Date acctBillDate = simpleFormat.parse(rs.getString(1));
			
		rs = database.executeQuery("SELECT LAST_BILL_DATE FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no)//getBillStartDate(client_no, acct_no)
		rs.next();
			Date lastBillDate = simpleFormat.parse(rs.getString(1));
			
		logi.logInfo "Acct Bill date  :"+acctBillDate
		// Stores the account created time in the specified format
		int daysInvoice1 = getDaysBetween(currentDate, acctBillDate)
		numDays = getDaysBetween(lastBillDate, acctBillDate)
		logi.logInfo "daysInvoice: "+daysInvoice1
		logi.logInfo "tot days: "+numDays
		logi.logInfo "As line Item verify acct_no :" + acct_no
		String supp_plan= getValueFromRequest("assign_supp_plan","//supp_plan_no")
		String supp_plan_units= getValueFromRequest("assign_supp_plan","//num_plan_units")
		String supp_plan_service = null;
		if(lineItem==1)
			supp_plan_service= getValueFromResponse("assign_supp_plan",ExPathRpcEnc.ASP_INVOICE_LINE_ITEM_SERVICE_1)
		else if(lineItem==2)
			supp_plan_service= getValueFromResponse("assign_supp_plan",ExPathRpcEnc.ASP_INVOICE_LINE_ITEM_SERVICE_2)
		String plan_Rate_Per_Unit="SELECT RATE_PER_UNIT FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN(SELECT SCHEDULE_NO FROM ARIACORE.CLIENT_PLAN_DFLT_RATE_SCHEDS WHERE plan_no = "+supp_plan+") AND SERVICE_NO IN ("+supp_plan_service+") AND from_unit<="+supp_plan_units+" and (to_unit>="+supp_plan_units+" or to_unit is null)"
		logi.logInfo "As line Item verify supp_plan :" + supp_plan
		logi.logInfo "As line Item verify supp_plan_units :" + supp_plan_units
		logi.logInfo "As line Item verify supp_plan_service :" + supp_plan_service
		logi.logInfo "As line Item verify plan_Rate_Per_Unit :" + plan_Rate_Per_Unit
		double status_cd;
		
		String st_code = database.executeQueryP2(plan_Rate_Per_Unit)
		status_cd=st_code.toDouble()*supp_plan_units.toDouble()
		status_cd = status_cd*(daysInvoice1/numDays)
		status_cd = status_cd.round(2)
		String[] a = status_cd.toString().split("\\.");
		if(a[1]=="0"){
			logi.logInfo "a[0] :" +a[0].toString()
			return a[0].toString()
		}
		logi.logInfo "As line Item verify status_cd :" + status_cd
		return status_cd.toString()
	}
	
	
	def md_assign_supp_plan_invoice_item_1(String testcaseId){
		return md_assign_supp_plan_invoice_item(testcaseId,1)
	}
	
	def md_assign_supp_plan_invoice_item_2(String testcaseId){
		return md_assign_supp_plan_invoice_item(testcaseId,2)
	}
	
	
	def md_replace_supp_plan_invoice_item(String testcaseId,int lineItem) {
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		ConnectDB database = new ConnectDB();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL"
		String currentVirtualTime = database.executeQueryP2(Query);
		
		
		logi.logInfo "Current Virtual Time : "+ currentVirtualTime

		Date currentDate = simpleFormat.parse(currentVirtualTime);
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		
		
		int numDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		logi.logInfo "Cureent VTTttttttttt"+currentDate
		ResultSet rs;
		rs = database.executeQuery("SELECT NEXT_BILL_DATE FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no)//getBillStartDate(client_no, acct_no)
		rs.next();
			Date acctBillDate = simpleFormat.parse(rs.getString(1));
			
		rs = database.executeQuery("SELECT LAST_BILL_DATE FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no)//getBillStartDate(client_no, acct_no)
		rs.next();
			Date lastBillDate = simpleFormat.parse(rs.getString(1));
			
		logi.logInfo "Acct Bill date  :"+acctBillDate
		// Stores the account created time in the specified format
		int daysInvoice1 = getDaysBetween(currentDate, acctBillDate)
		numDays = getDaysBetween(lastBillDate, acctBillDate)
		logi.logInfo "daysInvoice: "+daysInvoice1
		logi.logInfo "tot days: "+numDays
		logi.logInfo "As line Item verify acct_no :" + acct_no
		String supp_plan= getValueFromRequest("replace_supp_plan","//new_supp_plan_no")
		String supp_plan_units= getValueFromRequest("replace_supp_plan","//num_plan_units")
		String supp_plan_service = null;
		if(lineItem==1)
			supp_plan_service= getValueFromResponse("replace_supp_plan",ExPathRpcEnc.ASP_INVOICE_LINE_ITEM_SERVICE_1)
		else if(lineItem==2)
			supp_plan_service= getValueFromResponse("replace_supp_plan",ExPathRpcEnc.ASP_INVOICE_LINE_ITEM_SERVICE_2)
		String plan_Rate_Per_Unit="SELECT RATE_PER_UNIT FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN(SELECT SCHEDULE_NO FROM ARIACORE.CLIENT_PLAN_DFLT_RATE_SCHEDS WHERE plan_no = "+supp_plan+") AND SERVICE_NO IN ("+supp_plan_service+") AND from_unit<="+supp_plan_units+" and (to_unit>="+supp_plan_units+" or to_unit is null)"
		logi.logInfo "As line Item verify supp_plan :" + supp_plan
		logi.logInfo "As line Item verify supp_plan_units :" + supp_plan_units
		logi.logInfo "As line Item verify supp_plan_service :" + supp_plan_service
		logi.logInfo "As line Item verify plan_Rate_Per_Unit :" + plan_Rate_Per_Unit
		double status_cd;
		
		String st_code = database.executeQueryP2(plan_Rate_Per_Unit)
		status_cd=st_code.toDouble()*supp_plan_units.toDouble()
		status_cd = status_cd*(daysInvoice1/numDays)
		status_cd = status_cd.round(2)
		String[] a = status_cd.toString().split("\\.");
		if(a[1]=="0"){
			logi.logInfo "a[0] :" +a[0].toString()
			return a[0].toString()
		}
		logi.logInfo "As line Item verify status_cd :" + status_cd
		return status_cd.toString()
	}
	
	
	def md_replace_supp_plan_invoice_item_1(String testcaseId){
		return md_replace_supp_plan_invoice_item(testcaseId,1)
	}
	
	def md_replace_supp_plan_invoice_item_2(String testcaseId){
		return md_replace_supp_plan_invoice_item(testcaseId,2)
	}
	
	def md_modify_supp_plan_invoice_item(String testcaseId,int lineItem) {
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		ConnectDB database = new ConnectDB();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL"
		String currentVirtualTime = database.executeQueryP2(Query);
		
		
		logi.logInfo "Current Virtual Time : "+ currentVirtualTime

		Date currentDate = simpleFormat.parse(currentVirtualTime);
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		
		
		int numDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		logi.logInfo "Cureent VTTttttttttt"+currentDate
		ResultSet rs;
		rs = database.executeQuery("SELECT NEXT_BILL_DATE FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no)//getBillStartDate(client_no, acct_no)
		rs.next();
			Date acctBillDate = simpleFormat.parse(rs.getString(1));
			
		rs = database.executeQuery("SELECT LAST_BILL_DATE FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no)//getBillStartDate(client_no, acct_no)
		rs.next();
			Date lastBillDate = simpleFormat.parse(rs.getString(1));
			
		logi.logInfo "Acct Bill date  :"+acctBillDate
		// Stores the account created time in the specified format
		int daysInvoice1 = getDaysBetween(currentDate, acctBillDate)
		numDays = getDaysBetween(lastBillDate, acctBillDate)
		logi.logInfo "daysInvoice: "+daysInvoice1
		logi.logInfo "tot days: "+numDays
		logi.logInfo "As line Item verify acct_no :" + acct_no
		String supp_plan= getValueFromRequest("modify_supp_plan","//supp_plan_no")
		String supp_plan_units= getValueFromRequest("modify_supp_plan","//num_plan_units")
		String supp_plan_service = null;
		if(lineItem==1)
			supp_plan_service= getValueFromResponse("modify_supp_plan",ExPathRpcEnc.ASP_INVOICE_LINE_ITEM_SERVICE_1)
		else if(lineItem==2)
			supp_plan_service= getValueFromResponse("modify_supp_plan",ExPathRpcEnc.ASP_INVOICE_LINE_ITEM_SERVICE_2)
		String plan_Rate_Per_Unit="SELECT RATE_PER_UNIT FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN(SELECT SCHEDULE_NO FROM ARIACORE.CLIENT_PLAN_DFLT_RATE_SCHEDS WHERE plan_no = "+supp_plan+") AND SERVICE_NO IN ("+supp_plan_service+") AND from_unit<="+supp_plan_units+" and (to_unit>="+supp_plan_units+" or to_unit is null)"
		logi.logInfo "As line Item verify supp_plan :" + supp_plan
		logi.logInfo "As line Item verify supp_plan_units :" + supp_plan_units
		logi.logInfo "As line Item verify supp_plan_service :" + supp_plan_service
		logi.logInfo "As line Item verify plan_Rate_Per_Unit :" + plan_Rate_Per_Unit
		double status_cd;
		
		String st_code = database.executeQueryP2(plan_Rate_Per_Unit)
		status_cd=st_code.toDouble()*supp_plan_units.toDouble()
		status_cd = status_cd*(daysInvoice1/numDays)
		status_cd = status_cd.round(2)
		String[] a = status_cd.toString().split("\\.");
		if(a[1]=="0"){
			logi.logInfo "a[0] :" +a[0].toString()
			return a[0].toString()
		}
		logi.logInfo "As line Item verify status_cd :" + status_cd
		return status_cd.toString()
	}
	
	
	def md_modify_supp_plan_invoice_item_1(String testcaseId){
		return md_modify_supp_plan_invoice_item(testcaseId,1)
	}
	
	def md_modify_supp_plan_invoice_item_2(String testcaseId){
		return md_modify_supp_plan_invoice_item(testcaseId,2)
	}
	
	
	
	def md_update_master_plan_invoice_item(String testcaseId,int lineItem) {
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		ConnectDB database = new ConnectDB();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL"
		String currentVirtualTime = database.executeQueryP2(Query);
		
		
		logi.logInfo "Current Virtual Time : "+ currentVirtualTime

		Date currentDate = simpleFormat.parse(currentVirtualTime);
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		
		
		int numDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		logi.logInfo "Cureent VTTttttttttt"+currentDate
		ResultSet rs;
		rs = database.executeQuery("SELECT NEXT_BILL_DATE FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no)//getBillStartDate(client_no, acct_no)
		rs.next();
			Date acctBillDate = simpleFormat.parse(rs.getString(1));
			
		rs = database.executeQuery("SELECT LAST_BILL_DATE FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no)//getBillStartDate(client_no, acct_no)
		rs.next();
			Date lastBillDate = simpleFormat.parse(rs.getString(1));
			
		logi.logInfo "Acct Bill date  :"+acctBillDate
		// Stores the account created time in the specified format
		int daysInvoice1 = getDaysBetween(currentDate, acctBillDate)
		numDays = getDaysBetween(lastBillDate, acctBillDate)
		logi.logInfo "daysInvoice: "+daysInvoice1
		logi.logInfo "tot days: "+numDays
		logi.logInfo "As line Item verify acct_no :" + acct_no
		String supp_plan= getValueFromRequest("update_master_plan","//master_plan_no")
		String supp_plan_units= getValueFromRequest("update_master_plan","//num_plan_units")
		String supp_plan_service = null;
		if(lineItem==1)
			supp_plan_service= getValueFromResponse("update_master_plan",ExPathRpcEnc.UMP_INVOICE_LINE_ITEM_SERVICE_1)
		else if(lineItem==2)
			supp_plan_service= getValueFromResponse("update_master_plan",ExPathRpcEnc.UMP_INVOICE_LINE_ITEM_SERVICE_2)
		String plan_Rate_Per_Unit="SELECT RATE_PER_UNIT FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN(SELECT SCHEDULE_NO FROM ARIACORE.CLIENT_PLAN_DFLT_RATE_SCHEDS WHERE plan_no = "+supp_plan+") AND SERVICE_NO IN ("+supp_plan_service+") AND from_unit<="+supp_plan_units+" and (to_unit>="+supp_plan_units+" or to_unit is null)"
		logi.logInfo "As line Item verify supp_plan :" + supp_plan
		logi.logInfo "As line Item verify supp_plan_units :" + supp_plan_units
		logi.logInfo "As line Item verify supp_plan_service :" + supp_plan_service
		logi.logInfo "As line Item verify plan_Rate_Per_Unit :" + plan_Rate_Per_Unit
		double status_cd;
		
		String st_code = database.executeQueryP2(plan_Rate_Per_Unit)
		status_cd=st_code.toDouble()*supp_plan_units.toDouble()
		status_cd = status_cd*(daysInvoice1/numDays)
		status_cd = status_cd.round(2)
		String[] a = status_cd.toString().split("\\.");
		if(a[1]=="0"){
			logi.logInfo "a[0] :" +a[0].toString()
			return a[0].toString()
		}
		logi.logInfo "As line Item verify status_cd :" + status_cd
		return status_cd.toString()
	}
	
	
	def md_update_master_plan_invoice_item_1(String testcaseId){
		return md_update_master_plan_invoice_item(testcaseId,1)
	}
	
	def md_update_master_plan_invoice_item_2(String testcaseId){
		return md_update_master_plan_invoice_item(testcaseId,2)
	}
	
	def md_invoice_no_pending(String testcaseId,int rowInt){
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		def acct_no
		switch(rowInt){
		case 1:acct_no=getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO1)
		break;
		case 2:acct_no=getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO2)
		break;
		case 3:acct_no=getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO3)
		break;
		case 4:acct_no=getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO4)
		break;
		case 5:acct_no=getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO5)
		break;
		}
		
		return database.executeQueryP2("SELECT INVOICE_NO FROM ARIACORE.GL_PENDING WHERE ACCT_NO= "+acct_no+" AND CLIENT_NO="+client_no)
		
	}
	
	def md_invoice_no_pending_1(String testcaseId){
		return md_invoice_no_pending(testcaseId, 1)
	}
	
	def md_invoice_no_pending_2(String testcaseId){
		return md_invoice_no_pending(testcaseId, 2)
	}
	
	def md_invoice_no_pending_3(String testcaseId){
		return md_invoice_no_pending(testcaseId, 3)
	}
	
	def md_invoice_no_pending_4(String testcaseId){
		return md_invoice_no_pending(testcaseId, 4)
	}
	
	def md_invoice_no_pending_5(String testcaseId){
		return md_invoice_no_pending(testcaseId, 5)
	}
	
	
	
	def md_invoice_no(String testcaseId,int rowInt){
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		def acct_no
		switch(rowInt){
		case 1:acct_no=getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO1)
		break;
		case 2:acct_no=getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO2)
		break;
		case 3:acct_no=getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO3)
		break;
		case 4:acct_no=getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO4)
		break;
		case 5:acct_no=getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO5)
		break;
		}
		
		return database.executeQueryP2("SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO= "+acct_no+" AND CLIENT_NO="+client_no)
		
	}
	
	def md_invoice_no1(String testcaseId){
		return md_invoice_no(testcaseId, 1)
	}
	
	def md_invoice_no2(String testcaseId){
		return md_invoice_no(testcaseId, 2)
	}
	
	def md_invoice_no3(String testcaseId){
		return md_invoice_no(testcaseId, 3)
	}
	
	def md_invoice_no4(String testcaseId){
		return md_invoice_no(testcaseId, 4)
	}
	
	def md_invoice_no5(String testcaseId){
		return md_invoice_no(testcaseId, 5)
	}
	
	
	def md_Parent_Acct_No(int seqno)
	{
		logi.logInfo "md_Parent_Acct_No Started"
		def acct_no
		if(seqno == 1)
			acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
		if(seqno == 2)
			acct_no=getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
			if(seqno == 3)
			acct_no=getValueFromResponse('create_acct_complete.b',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
		logi.logInfo "acct_no from response :" + acct_no
		return acct_no
	}
		
	def md_Parent_Child_Acct_No_1(String testcaseId){
		return md_Parent_Acct_No(1)
	}
	
	def md_Parent_Child_Acct_No_2(String testcaseId){
		return md_Parent_Acct_No(2)
	}
	def md_Parent_Child_Acct_No_3(String testcaseId){
		return md_Parent_Acct_No(3)
	}
	
	def md_Parent_Acct_No_DB(int seqno)
	{
		logi.logInfo "md_Parent_Acct_No_DB Started"
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		def acct_no = null
		if(seqno == 1){
			acct_no = getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
		}
		if(seqno == 2){
			acct_no=getValueFromResponse('create_acct_complete.b',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
		}
		return database.executeQueryP2("SELECT SENIOR_ACCT_NO FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no)
	}
	
	def md_Parent_Child_Acct_No_DB_1(String testcaseId){
		return md_Parent_Acct_No_DB(1)
	}
	
	def md_Parent_Child_Acct_No_DB_2(String testcaseId){
		return md_Parent_Acct_No_DB(2)
	}
	
	public String md_checkInvoice_Child_Acct(String testCaseId){
		
				String client_no=Constant.mycontext.expand('${Properties#Client_No}')
				String acct_no=Constant.mycontext.expand('${Properties#AccountNo}')
				acct_no = getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
				logi.logInfo "check invoice begins"
				ConnectDB database = new ConnectDB();
				SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
				String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL"
				String currentVirtualTime = database.executeQueryP2(Query);
				
				
				logi.logInfo "Current Virtual Time : "+ currentVirtualTime
		
				// Stores the current virtual time in the specified format
				Date currentDate = simpleFormat.parse(currentVirtualTime);
				logi.logInfo "Cureent VTTttttttttt"+currentDate
				Date acctStartDate = getBillStartDate(client_no, acct_no)
				
				logi.logInfo "AccccccccccccctStartDate"+acctStartDate
				// Stores the account created time in the specified format
				int daysInvoice1 = getDaysBetween(acctStartDate, currentDate)
		
				logi.logInfo "daysInvoice: "+daysInvoice1
				HashMap<String, HashMap<String,String>> resultHash
				
				
				resultHash = getPlanServiceAndTieredPricing_Child()
				
				Iterator iterator = resultHash.entrySet().iterator()
				int invoice = 0
		
				while (iterator.hasNext()) {
					//Uvais
					int daysInvoice = daysInvoice1
					Map.Entry pairs = (Map.Entry) iterator.next()
					String plan_no = pairs.getKey().toString()
		
					int index = Integer.parseInt(database.executeQueryP2("select distinct supp_plan_ind from ariacore.client_plan where plan_no ="+plan_no+" and client_no = "+client_no))
					int no_of_units =0
					if(index==0)
						no_of_units = Integer.parseInt(getValueFromRequest("create_acct_complete.a","//master_plan_units"))
					else {
						String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
						String[] noOfUnits = getSuppPlanUnitsforAcct(acct_no,client_no) 		//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
						String[] supp_plans = getSuppPlansforAcct(acct_no,client_no)//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")
						logi.logInfo "supp_plans.length : "+supp_plans.length
						for( int i=0;i<supp_plans.length;i++) {
							logi.logInfo "supp_plans.length1 : "+supp_plans[i]
							logi.logInfo "supp_plans.length 2: "+plan_no
							if(supp_plans[i].toString().equalsIgnoreCase(plan_no)){
								logi.logInfo "no of plan "+noOfUnits[i]
								no_of_units = Integer.parseInt(noOfUnits[i])
								break
							}
						}
					}
		
					HashMap<String,String> value = pairs.getValue()
					int billing_interval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ plan_no+ "and client_no="+client_no))
					logi.logInfo "Services and Tiered Pricing in a plan : "+plan_no +" are "+ value.toString()
					logi.logInfo "Billing Interval of a plan : "+plan_no +" :  "+ billing_interval
					Date planEndDate = addMonthsFromGivenDate(acctStartDate,billing_interval)
					logi.logInfo "AccccccccccccctStartDate"+acctStartDate
					logi.logInfo "DurationDateeeeeeeee "+planEndDate
					
					int durationOfPlan = getDaysBetween(acctStartDate, planEndDate)
					logi.logInfo "daysInvoice: "+daysInvoice
					logi.logInfo "durationOfPlan : "+plan_no +" :  "+  durationOfPlan
					//Uvais
					int billlagdays = Integer.parseInt(getBillLagDays(billing_interval,client_no))
					logi.logInfo "billlagdays: "+billlagdays
					daysInvoice = daysInvoice + (billlagdays*-1)
					logi.logInfo "daysInvoice: "+daysInvoice
					//Uvais end
					
						Iterator service = value.entrySet().iterator()
						logi.logInfo "Invoice calculation for a plan : "+plan_no
						while(service.hasNext()){
							Map.Entry servicePairs = (Map.Entry) service.next()
							String service_no = servicePairs.getKey().toString()
							logi.logInfo "Invoice calculation for a service : "+service_no
							int seq_no=1;
							String tieredPricing = servicePairs.getValue().toString()
		
							int tieredPricingInt = Integer.parseInt(tieredPricing)
							logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
							switch(tieredPricingInt){
								case 1:
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "))
									invoice  = invoice+Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									seq_no++;
									break;
								case 2 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and (to_unit >="+no_of_units+" and from_unit<"+no_of_units+") order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
								case 3 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and (to_unit >="+no_of_units+" and from_unit<"+no_of_units+") order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
							}
						}
					
				}
				return invoice
			}
	public HashMap getPlanServiceAndTieredPricing_Child(){
		def acct_no = getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" or custom_to_client_no is null) and recurring = 1) order by plan_no, service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" or custom_to_client_no is null) and recurring = 1) order by plan_no, service_no"))
		ResultSet resultSet = database.executeQuery(query)

		HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		HashMap<String,String> serviceHash

		String[] plan = new String[count]
		String[] service = new String[count]
		String[] tieredPricing = new String[count]

		int i= 0
		while(resultSet.next()){
			plan[i]=resultSet.getString(1)
			service[i]=resultSet.getString(2)
			tieredPricing[i]=resultSet.getString(3)
			i++
		}
		String plan_no
		int serviceCount=0
		for(int planCount=0;planCount<plan.length;){
			plan_no = plan[planCount]
			serviceHash = new HashMap<String,String> ()
			for(;serviceCount<service.length;serviceCount++){
				if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					planCount ++
				}
				else
					break
			}
			resultHash.put(plan_no, serviceHash)
			logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
		}
		logi.logInfo "Total Services" + resultHash.toString()
		return resultHash
	}
	
	def md_Child_Acct_Invoice_DB(int seqno)
	{
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		def acct_no = null
		if(seqno == 1){
			acct_no = getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
		}
		if(seqno == 2){
			acct_no=getValueFromResponse('create_acct_complete.b',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
		}
		String invoice = database.executeQueryP2("SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no)
		return database.executeQueryP2("select sum(debit) from ariacore.gl_detail where invoice_no="+invoice+" and service_no not in (400,401,402,405)")
	}
	
	def md_Child_Acct_Invoice_DB_1(String testcaseId)
	{
		return md_Child_Acct_Invoice_DB(1)
	}
	
	def md_Child_Acct_Invoice_DB_2(String testcaseId)
	{
		return md_Child_Acct_Invoice_DB(2)
	}
	
	
	def md_Child_Acct_No_DB(String testcaseId)
	{
		logi.logInfo "md_Child_Acct_No_DB Started"
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		def acct_no = null
		
			acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
		
		return database.executeQueryP2("SELECT CHILD_ACCT_NO FROM ARIACORE.ALL_CHILD_ACCTS WHERE PARENT_ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no)
	}
	
	def md_Child_Acct_No_DB_row2(String testcaseId)
	{
		logi.logInfo "md_Child_Acct_No_DB Started"
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		def acct_no = null
		
			acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
		
		return database.executeQueryP2("SELECT CHILD_ACCT_NO FROM ARIACORE.ALL_CHILD_ACCTS WHERE PARENT_ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no +" ORDER BY CHILD_ACCT_NO DESC")
	}
	
	def md_Child_Acct_No_DB_grand(String testcaseId)
	{
		logi.logInfo "md_Child_Acct_No_DB Started"
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		def acct_no = null
		
			acct_no = getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
		
		return database.executeQueryP2("SELECT CHILD_ACCT_NO FROM ARIACORE.ALL_CHILD_ACCTS WHERE PARENT_ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no +" ORDER BY CHILD_ACCT_NO DESC")
	}
	
	def md_Child_Acct_No_DB_1(String testcaseId)
	{
		logi.logInfo "md_Child_Acct_No_DB Started"
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		def acct_no = null
		
			acct_no = getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
		
		return database.executeQueryP2("SELECT CHILD_ACCT_NO FROM ARIACORE.ALL_CHILD_ACCTS WHERE PARENT_ACCT_NO="+acct_no+" AND CLIENT_NO="+client_no)
	}
	
	
	String md_check_invoice_line_item_discount(String testcaseId, String seqNo){
		logi.logInfo "Check Invoice discount Line Item"
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		def acct_no = null
		double pre_discount_debit = 0
		def units =getValueFromRequest('create_acct_complete',"//master_plan_units")
			acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
			String query1 = "select gl_charge_seq_num,FLAT_PERCENT_IND,PERCENTAGE_AMT from ariacore.gl_dtl_discount_dtl where invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+acct_no+") AND GL_CHARGE_SEQ_NUM="+seqNo
			ResultSet rs //= db.executePlaQuery(query1);
			
			
			String query = "select rate_per_unit from ariacore.NEW_RATE_SCHED_RATES where schedule_no in(select default_rate_sched_no from ariacore.client_plan where plan_no in(select plan_no from ariacore.gl_detail where invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+acct_no+") and seq_num="+seqNo+")) and service_no in (select service_no from ariacore.gl_detail where invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+acct_no+") and seq_num="+seqNo+") and (to_unit >="+units+" or to_unit is null) and from_unit<"+units
						//logi.logInfo(query)
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()) {
				pre_discount_debit = resultSet.getDouble(1)
	
			}
			double res=0
			logi.logInfo "pre_discount_debit  "+pre_discount_debit
			rs = db.executePlaQuery(query1);
			while(rs.next()) {
				logi.logInfo "rs.getString(2)  "+rs.getString(2)
				if(rs.getString(2).equals("P")||rs.getString(2).equals("p")){
					logi.logInfo "rs.getInt(3)  "+rs.getInt(3)
					res = pre_discount_debit * (rs.getInt(3)/100)
					pre_discount_debit = pre_discount_debit - res
					logi.logInfo "res  "+pre_discount_debit
				}
				else
				{
					pre_discount_debit = pre_discount_debit - rs.getInt(3)
					logi.logInfo "res  "+pre_discount_debit
				}
			}
			if(pre_discount_debit.toString().contains(".0")){
				int res1 = pre_discount_debit.toInteger()
				return res1
			}
			DecimalFormat df = new DecimalFormat("#.######");
			return df.format(pre_discount_debit.trunc(6)).toString()
			//return pre_discount_debit.toString()
	}
	
	String md_check_invoice_line_item_discount_1(String testcaseId){
		return md_check_invoice_line_item_discount(testcaseId,"1")
	}
	
	
	String md_check_invoice_line_item_discount_2(String testcaseId){
		return md_check_invoice_line_item_discount(testcaseId,"2")
	}
	
	String md_check_invoice_line_item_discount_3(String testcaseId){
		return md_check_invoice_line_item_discount(testcaseId,"3")
	}
	
	
	String md_check_invoice_line_Amount_Flat_discount(String testcaseId, String seqNo){
		logi.logInfo "Check Invoice discount Line Item"
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		def acct_no = null
		int pre_discount_debit = 0
		def units =getValueFromRequest('create_acct_complete',"//master_plan_units")
			acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
			String query1 = "select gl_charge_seq_num,FLAT_PERCENT_IND,PERCENTAGE_AMT,ACTUAL_DISCOUNT_AMT from ariacore.gl_dtl_discount_dtl where invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+acct_no+") AND GL_CHARGE_SEQ_NUM="+seqNo
			ResultSet rs //= db.executePlaQuery(query1);
			
			
			String query = "select rate_per_unit from ariacore.NEW_RATE_SCHED_RATES where schedule_no in(select default_rate_sched_no from ariacore.client_plan where plan_no in(select plan_no from ariacore.gl_detail where invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+acct_no+") and seq_num="+seqNo+")) and service_no in (select service_no from ariacore.gl_detail where invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+acct_no+") and seq_num="+seqNo+") and (to_unit >="+units+" or to_unit is null) and from_unit<"+units
						//logi.logInfo(query)
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()) {
				pre_discount_debit = resultSet.getInt(1)
	
			}
			String ser = database.executeQueryP2("select Service_type from ariacore.all_service where service_no in (select service_no from ariacore.gl_detail where invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+acct_no+") and seq_num="+seqNo+")")
			if(ser.equals("AC"))
			{
				units="1";
			}
			int res=0
			logi.logInfo "pre_discount_debit  "+pre_discount_debit
			rs = db.executePlaQuery(query1);
			while(rs.next()) {
				logi.logInfo "rs.getString(2)  "+rs.getString(2)
				if(rs.getString(2).equals("P")||rs.getString(2).equals("p")){
					logi.logInfo "rs.getInt(3)  "+rs.getInt(3)
					res = pre_discount_debit * (rs.getInt(3)/100)
					res = pre_discount_debit - res
					logi.logInfo "res  "+(res*units.toInteger())
				}
				else
				{
					res = (pre_discount_debit*units.toInteger()) - rs.getInt(4)
					logi.logInfo "res  "+res
				}
			}
			
			return res.toString()
	}
	
	
	String md_check_invoice_line_Amount_Flat_discount_1(String testcaseId){
		return md_check_invoice_line_Amount_Flat_discount(testcaseId,"1")
	}
	
	
	String md_check_invoice_line_Amount_Flat_discount_2(String testcaseId){
		return md_check_invoice_line_Amount_Flat_discount(testcaseId,"2")
	}
	
	String md_check_invoice_line_Amount_Flat_discount_3(String testcaseId){
		return md_check_invoice_line_Amount_Flat_discount(testcaseId,"3")
	}
	
	String md_bill_date_verification(String testcaseId,String dateColumn){
		logi.logInfo "Verification of Next bill date"
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB();
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
		def splanNo = getValueFromRequest('assign_supp_plan',"//supp_plan_no")
		String syncOverride = getValueFromRequest('assign_supp_plan',"//sync_mstr_bill_dates_override")
		Date currentVirtualTime
		logi.logInfo "syncOverride   "+syncOverride
		logi.logInfo "syncOverride   "+syncOverride
		if(splanNo=="NoVal"){
			logi.logInfo "assign supp plan multi "
			syncOverride = getValueFromRequest('assign_supp_plan_multi',"//sync_mstr_bill_dates_override")
			logi.logInfo "syncOverride   "+syncOverride
			List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
			List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
			logi.logInfo(testCaseCmb[0].toString())
			com.eviware.soapui.support.XmlHolder outputholder
			for(int j=0; j<testCaseCmb.size(); j++) {
				if(testCaseCmb[j].toString().contains("assign_supp_plan_multi") && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
					outputholder = xmlValues[j]
			String linecount=getNodeCountFromRequest("assign_supp_plan_multi","//*/supp_plans_to_assign/*:supp_plans_to_assign_row").toString()
			logi.logInfo "linecount   "+linecount
			for(int i = 1;i<=linecount.toInteger();i++){
				
				 
				 def tempPlan = getValueFromRequest('assign_supp_plan_multi',"//*/supp_plans_to_assign/supp_plans_to_assign_row["+i+"]/supp_plan_no")
				 logi.logInfo "sno7   "+tempPlan
				 splanNo = tempPlan
						 }
			}
				}
		}
		if(syncOverride == "NoVal"){
			syncOverride = db.executeQueryP2("select param_val from ariacore.aria_client_params where client_no = "+client_no+" and param_name = 'SYNC_MSTR_BILL_DATES_ON_1ST_SUPP'")
		}
		if(syncOverride == "1"){
			ResultSet rs = db.executePlaQuery("select created from ariacore.acct where acct_no=" + acct_no );
			
			//String currentVirtualTime = null;
			if(rs.next()) {
				currentVirtualTime = rs.getDate(1);
			}
			rs.close()
		}
		else{
			ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL");
				
			if(rs.next()) {
				currentVirtualTime = rs.getDate(1);
			}
			rs.close()
		}
		String sbillingInterval_query="select billing_interval from ariacore.client_plan where plan_no in ("+splanNo+") and client_no="+client_no
		def sbillingInterval=db.executeQueryP2(sbillingInterval_query)
		
		//String currentVirtualTime = clientUtil.getCurrentVirtualTimeInHours(client_no);
		logi.logInfo "Next Bill date1  "+currentVirtualTime
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
		String date = format1.format(currentVirtualTime);
		
		logi.logInfo "Next Bill date 2 "+date
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(currentVirtualTime);
		switch(dateColumn){
			case "NextBillDate":
			
				logi.logInfo "Next Bill date3  "+date
				cal1.add(Calendar.MONTH, sbillingInterval.toInteger())
				String formatted = format1.format(cal1.getTime())
				logi.logInfo "Next Bill date  "+formatted
				return formatted
				break;
			case "LastBillDate":
			case "LASTRECURBILLDATE":
				format1 = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
				String date1 = format1.format(currentVirtualTime);
				return date1
				break;
			case "BillDate":
				format1 = new SimpleDateFormat("dd", Locale.ENGLISH);
				date = format1.format(currentVirtualTime);
				return date
				break;
			case "LastBillThru":
				cal1.add(Calendar.DATE, -1);
				cal1.add(Calendar.MONTH, 1);
				format1 = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
				String formatted = format1.format(cal1.getTime());
				logi.logInfo "last_bill_thru_date  "+formatted
				return formatted;
				break;
			
				
		}
		
	}
	String md_last_recur_bill_date(String testcaseId){
		logi.logInfo "Verification of Next bill date"
		return md_bill_date_verification(testcaseId, "LASTRECURBILLDATE")
	}
	String md_next_bill_date(String testcaseId){
		logi.logInfo "Verification of Next bill date"
		/*String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB();
		
		ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL");
		Date currentVirtualTime
		//String currentVirtualTime = null;
		if(rs.next()) {
			currentVirtualTime = rs.getDate(1);
		}
		rs.close()
		//String currentVirtualTime = clientUtil.getCurrentVirtualTimeInHours(client_no);
		logi.logInfo "Next Bill date1  "+currentVirtualTime
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
		String date = format1.format(currentVirtualTime);
		
		logi.logInfo "Next Bill date 2 "+date
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(currentVirtualTime);
		logi.logInfo "Next Bill date3  "+date
		cal1.add(Calendar.MONTH, 1);
		String formatted = format1.format(cal1.getTime());
		logi.logInfo "Next Bill date  "+formatted*/
		return md_bill_date_verification(testcaseId, "NextBillDate")
	}
	
	String md_last_bill_date(String testcaseId){
		logi.logInfo "Verification of Next bill date"
		return md_bill_date_verification(testcaseId, "LastBillDate")
		/*String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB();
		
		ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL");
		Date currentVirtualTime
		//String currentVirtualTime = null;
		if(rs.next()) {
			currentVirtualTime = rs.getDate(1);
		}
		rs.close()
		//String currentVirtualTime = clientUtil.getCurrentVirtualTimeInHours(client_no);
		logi.logInfo "Next Bill date1  "+currentVirtualTime
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
		String date = format1.format(currentVirtualTime);
		return date*/
		
	}
	String md_bill_day(String testcaseId){
		logi.logInfo "Verification of Next bill date"
		return md_bill_date_verification(testcaseId, "BillDate")
		/*String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB();
		
		ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL");
		Date currentVirtualTime
		//String currentVirtualTime = null;
		if(rs.next()) {
			currentVirtualTime = rs.getDate(1);
		}
		rs.close()
		//String currentVirtualTime = clientUtil.getCurrentVirtualTimeInHours(client_no);
		logi.logInfo "Next Bill date1  "+currentVirtualTime
		SimpleDateFormat format1 = new SimpleDateFormat("dd", Locale.ENGLISH);
		String date = format1.format(currentVirtualTime);*/
		
		
	}
	
	String md_last_bill_thru_date(String testcaseId){
		logi.logInfo "Verification of Next bill date"
		return md_bill_date_verification(testcaseId, "LastBillThru")
		/*String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB();
		
		ResultSet rs = db.executePlaQuery("SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL");
		Date currentVirtualTime
		//String currentVirtualTime = null;
		if(rs.next()) {
			currentVirtualTime = rs.getDate(1);
		}
		rs.close()
		//String currentVirtualTime = clientUtil.getCurrentVirtualTimeInHours(client_no);
		logi.logInfo "last_bill_thru_date1  "+currentVirtualTime
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
		String date = format1.format(currentVirtualTime);
		
		logi.logInfo "last_bill_thru_date 2 "+date
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(currentVirtualTime);
		logi.logInfo "last_bill_thru_date3  "+date
		cal1.add(Calendar.DATE, -1);
		cal1.add(Calendar.MONTH, 1);
		String formatted = format1.format(cal1.getTime());
		logi.logInfo "last_bill_thru_date  "+formatted
		return formatted;*/
	}

	String md_bill_date_verification_Create(String testcaseId,String dateColumn){
		logi.logInfo "Verification of Next bill date"
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB();
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_CHILD_ACCTNO1)
		def splanNo = getValueFromRequest('assign_supp_plan',"//supp_plan_no")
		String syncOverride = getValueFromRequest('assign_supp_plan',"//sync_mstr_bill_dates_override")
		Date currentVirtualTime
		logi.logInfo "syncOverride   "+syncOverride
		if(splanNo=="NoVal"){
			logi.logInfo "assign supp plan multi "
			syncOverride = getValueFromRequest('assign_supp_plan_multi',"//sync_mstr_bill_dates_override")
			logi.logInfo "syncOverride   "+syncOverride
			List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
			List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
			logi.logInfo(testCaseCmb[0].toString())
			com.eviware.soapui.support.XmlHolder outputholder
			for(int j=0; j<testCaseCmb.size(); j++) {
				if(testCaseCmb[j].toString().contains("assign_supp_plan_multi") && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
					outputholder = xmlValues[j]
			String linecount=getNodeCountFromRequest("assign_supp_plan_multi","//*/supp_plans_to_assign/*:supp_plans_to_assign_row").toString()
			logi.logInfo "linecount   "+linecount
			for(int i = 1;i<=linecount.toInteger();i++){
				
				 
				 def tempPlan = getValueFromRequest('assign_supp_plan_multi',"//*/supp_plans_to_assign/supp_plans_to_assign_row["+i+"]/supp_plan_no")
				 logi.logInfo "sno7   "+tempPlan
				 splanNo = tempPlan
						 }
			}
				}
		}
			ResultSet rs = db.executePlaQuery("select created from ariacore.acct where acct_no=" + acct_no );
			
			//String currentVirtualTime = null;
			if(rs.next()) {
				currentVirtualTime = rs.getDate(1);
			}
			rs.close()
				
		String sbillingInterval_query="select billing_interval from ariacore.client_plan where plan_no="+splanNo+" and client_no="+client_no
		def sbillingInterval=db.executeQueryP2(sbillingInterval_query)
		
		//String currentVirtualTime = clientUtil.getCurrentVirtualTimeInHours(client_no);
		logi.logInfo "Next Bill date1  "+currentVirtualTime
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
		String date = format1.format(currentVirtualTime);
		
		logi.logInfo "Next Bill date 2 "+date
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(currentVirtualTime);
		switch(dateColumn){
			case "NextBillDate":
			
				logi.logInfo "Next Bill date3  "+date
				cal1.add(Calendar.MONTH, sbillingInterval.toInteger())
				String formatted = format1.format(cal1.getTime())
				logi.logInfo "Next Bill date  "+formatted
				return formatted
				break;
			case "LastBillDate":
			case "LASTRECURBILLDATE":
				format1 = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
				String date1 = format1.format(currentVirtualTime);
				return date1
				break;
			case "BillDate":
				format1 = new SimpleDateFormat("dd", Locale.ENGLISH);
				date = format1.format(currentVirtualTime);
				return date
				break;
			case "LastBillThru":
				cal1.add(Calendar.DATE, -1);
				cal1.add(Calendar.MONTH, 1);
				format1 = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
				String formatted = format1.format(cal1.getTime());
				logi.logInfo "last_bill_thru_date  "+formatted
				return formatted;
				break;
			
				
		}
		
	}
	
	
	
	String md_last_recur_bill_date_create(String testcaseId){
		logi.logInfo "Verification of Next bill date"
		return md_bill_date_verification_Create(testcaseId, "LASTRECURBILLDATE")
	}
	String md_next_bill_date_create(String testcaseId){
		logi.logInfo "Verification of Next bill date"
		
		return md_bill_date_verification_Create(testcaseId, "NextBillDate")
	}
	
	String md_last_bill_date_create(String testcaseId){
		logi.logInfo "Verification of Next bill date"
		return md_bill_date_verification_Create(testcaseId, "LastBillDate")
				
	}
	String md_bill_day_create(String testcaseId){
		logi.logInfo "Verification of Next bill date"
		return md_bill_date_verification_Create(testcaseId, "BillDate")
			
		
	}
	
	String md_last_bill_thru_date_create(String testcaseId){
		logi.logInfo "Verification of Next bill date"
		return md_bill_date_verification_Create(testcaseId, "LastBillThru")
		
	}
	
	def md_verifyValidatePaymentProcessorAct(String tcid){
		HashMap doDollarAct = new HashMap<String,String>()
		
		// Proc payment id
		doDollarAct.put("procPaymentId","null")
		
		// Proc AVS response
		doDollarAct.put("procAvsResponse","null")
		
		// Proc CVV response
		doDollarAct.put("procCvvResponse","null")
		
		// Proc CAVV response
		doDollarAct.put("procCAvvResponse","null")
		
		// Proc status code
		doDollarAct.put("procStatusCode","null")
		
		// Proc auth  code
		doDollarAct.put("procAuthCode","null")
		
		// FInal hash
		logi.logInfo("Final hash is : "+doDollarAct)
		return doDollarAct
	}
	
	def md_verifyValidatePaymentProcessorExp(String tcid){
		HashMap doDollarExp = new HashMap<String,String>()
		ConnectDB db = new ConnectDB()
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo("Account number is : "+acct_no)
		
		// Queries
		String procPaymentIdQry = "SELECT PROC_PYMNT_ID FROM ARIACORE.PAYMENTS WHERE ACCT_NO ="+acct_no
		String procAvsResponseQry = " SELECT PROC_VALIDATE_ADDRESS FROM ARIACORE.PAYMENTS WHERE ACCT_NO ="+acct_no
		String procCvvResponseQry = "SELECT PROC_VALIDATE_CVV FROM ARIACORE.PAYMENTS WHERE ACCT_NO ="+acct_no
		String procCAvvResponseQry = "SELECT PROC_CARD_SECURE FROM ARIACORE.PAYMENTS WHERE ACCT_NO ="+acct_no
		String procStatusCodeQry = "SELECT MAX(STATUS_CODE) FROM ARIACORE.PAYMENTS WHERE ACCT_NO ="+acct_no
		String procAuthCodeQry = "SELECT MAX(AUTH_CODE) FROM ARIACORE.PAYMENTS WHERE ACCT_NO ="+acct_no
		
		// Proc payment id
		def procPaymentId = db.executeQueryP2(procPaymentIdQry)
		logi.logInfo("Proc payment id is : "+procPaymentId)
		doDollarExp.put("procPaymentId",procPaymentId)
		
		// Proc AVS response
		def procAvsResponse = db.executeQueryP2(procAvsResponseQry)
		logi.logInfo("Proc AVS Response is : "+procAvsResponse)
		doDollarExp.put("procAvsResponse",procAvsResponse)
		
		// Proc CVV response
		def procCvvResponse = db.executeQueryP2(procCvvResponseQry)
		logi.logInfo("Proc CVV Response is : "+procCvvResponse)
		doDollarExp.put("procCvvResponse",procCvvResponse)
		
		// Proc CAVV response
		def procCAvvResponse = db.executeQueryP2(procCAvvResponseQry)
		logi.logInfo("Proc CAVV Response is : "+procCAvvResponse)
		doDollarExp.put("procCAvvResponse",procCAvvResponse)
		
		// Proc status code
		def procStatusCode = db.executeQueryP2(procStatusCodeQry)
		logi.logInfo("Proc status code is : "+procStatusCode)
		doDollarExp.put("procStatusCode",procStatusCode)
		
		// Proc auth  code
		def procAuthCode = db.executeQueryP2(procAuthCodeQry)
		logi.logInfo("Proc auth code is : "+procAuthCode)
		doDollarExp.put("procAuthCode",procAuthCode)
		
		// FInal hash
		logi.logInfo("Final hash is : "+doDollarExp)
		return doDollarExp
	}
	
	def md_verifyAuthorizeElectronicPaymentExp(String tcid){
		HashMap doDollarExp = new HashMap<String,String>()
		ConnectDB db = new ConnectDB()
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo("Account number is : "+acct_no)
		
		// Queries
		String procPaymentIdQry = "SELECT PROC_PYMNT_ID FROM ARIACORE.PAYMENT_AUTH WHERE ACCT_NO ="+acct_no
		String procAvsResponseQry = " SELECT PROC_VALIDATE_ADDRESS FROM ARIACORE.PAYMENT_AUTH WHERE ACCT_NO ="+acct_no
		String procCvvResponseQry = "SELECT PROC_VALIDATE_CVV FROM ARIACORE.PAYMENT_AUTH WHERE ACCT_NO ="+acct_no
		String procCAvvResponseQry = "SELECT PROC_CARD_SECURE FROM ARIACORE.PAYMENT_AUTH WHERE ACCT_NO ="+acct_no
		String procStatusCodeQry = "SELECT MAX(STATUS_CODE) FROM ARIACORE.PAYMENT_AUTH WHERE ACCT_NO ="+acct_no
		String procAuthCodeQry = "SELECT MAX(AUTH_CODE) FROM ARIACORE.PAYMENT_AUTH WHERE ACCT_NO ="+acct_no
		
		// Proc payment id
		def procPaymentId = db.executeQueryP2(procPaymentIdQry)
		logi.logInfo("Proc payment id is : "+procPaymentId)
		doDollarExp.put("procPaymentId",procPaymentId)
		
		// Proc AVS response
		def procAvsResponse = db.executeQueryP2(procAvsResponseQry)
		logi.logInfo("Proc AVS Response is : "+procAvsResponse)
		doDollarExp.put("procAvsResponse",procAvsResponse)
		
		// Proc CVV response
		def procCvvResponse = db.executeQueryP2(procCvvResponseQry)
		logi.logInfo("Proc CVV Response is : "+procCvvResponse)
		doDollarExp.put("procCvvResponse",procCvvResponse)
		
		// Proc CAVV response
		def procCAvvResponse = db.executeQueryP2(procCAvvResponseQry)
		logi.logInfo("Proc CAVV Response is : "+procCAvvResponse)
		doDollarExp.put("procCAvvResponse",procCAvvResponse)
		
		// Proc status code
		def procStatusCode = db.executeQueryP2(procStatusCodeQry)
		logi.logInfo("Proc status code is : "+procStatusCode)
		doDollarExp.put("procStatusCode",procStatusCode)
		
		// Proc auth  code
		def procAuthCode = db.executeQueryP2(procAuthCodeQry)
		logi.logInfo("Proc auth code is : "+procAuthCode)
		doDollarExp.put("procAuthCode",procAuthCode)
		
		// FInal hash
		logi.logInfo("Final hash is : "+doDollarExp)
		return doDollarExp
	}
	
	/**
	 * Returns payment id 
	 * @param tcid
	 * @return paymentid
	 */
	public String  md_verifyPaymentId (String tcid)
	{
		ConnectDB db = new ConnectDB()
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo("Account number is : "+acct_no)		
		String procPaymentIdQry = "SELECT PROC_PYMNT_ID FROM ARIACORE.PAYMENT_AUTH WHERE ACCT_NO ="+acct_no
		String procPaymentId = db.executeQueryP2(procPaymentIdQry)
		return procPaymentId
		
	}
	
	/**
	 * Getting the expected bill thru date of the account
	 * @param testCaseId
	 * @return String
	 */
	public String md_VerifyResettedBillDateAfterDunning(String testCaseId){

		boolean returnType = false
		ReadData objRD =new ReadData()
		ConnectDB db = new ConnectDB()
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-YYYY")

		String apiname = testCaseId.split("-")[2]
		String localTcId = testCaseId.split("-")[1]
		String account_number = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String plan_no = getValueFromRequest("create_acct_complete","//master_plan_no")
		String client_number = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo("Plan no is : "+plan_no)

		String nextBillDate
		String createdDate
		String nextBillDateQry = "select to_char(NEXT_BILL_DATE,'dd-MM-YYYY') from ariacore.acct where acct_no="+account_number
		String bill_int_qry ="SELECT distinct billing_interval FROM ariacore.client_plan WHERE plan_no="+plan_no+" AND client_no="+client_number

		nextBillDate = db.executeQueryP2(nextBillDateQry)
		logi.logInfo(" nextBillDate actUAL :: "+nextBillDate )
		
		
		int billing_int = db.executeQueryP2(bill_int_qry).toInteger()
		logi.logInfo("plan billing interval"+billing_int)

		Date date = new Date();
		Calendar now = Calendar.getInstance();

		ResultSet resultSet = db.executeQuery("select ariacore.ariavirtualtime("+client_number+") from dual");
		while(resultSet.next()){
			date = resultSet.getDate(1);
		}
		logi.logInfo("plan billing date ::: "+date.toString())
		
		now.setTime(date);
		now.add(Calendar.MONTH, billing_int)
		
		now.add(Calendar.DAY_OF_MONTH, -6)
		
		Date forDate = now.getTime()
		String formattedDate = format1.format(forDate)
		logi.logInfo("formattedDate : "+formattedDate)
		
		return formattedDate
	}
	def md_custom_rate_schedule_API(String testcaseid) {
		LinkedHashMap <String, String> resultHash = new LinkedHashMap<String, String>()
		String plancount=getNodeCountFromResponse("get_queued_service_plans","//*/*/*:queued_plans_row").toString()
		logi.logInfo("Plan Count : " + plancount)
		String ratecount = getNodeCountFromResponse("get_queued_service_plans","//*/queued_plans[1]/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row").toString()
		logi.logInfo("Plan Count : " + plancount)
		for(int i=1;i<=ratecount.toInteger();i++){
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_Rate seq_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_From Unit_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_from_unit[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_To Unit_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_to_unit[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_Rate_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_per_unit[1]"))
		}
		/*resultHash.put("Rate seq2", getValueFromResponse("get_queued_service_plans", ExPathRpc.get_queued_cutom_rate_seq2))
		resultHash.put("From Unit2", getValueFromResponse("get_queued_service_plans", ExPathRpc.get_queued_cutom_rate_from2))
		resultHash.put("Rate2", getValueFromResponse("get_queued_service_plans", ExPathRpc.get_queued_cutom_rate2))*/
		logi.logInfo("Rate schedule hash map : " + resultHash)
		return resultHash
	}
	
	def md_custom_rate_schedule_Input(String testcaseid) {
		LinkedHashMap <String, String> resultHash = new LinkedHashMap<String, String>()
		String plancount=getNodeCountFromResponse("get_queued_service_plans","//*/*/*:queued_plans_row").toString()
		logi.logInfo("Plan Count : " + plancount)
		String ratecount = getNodeCountFromResponse("get_queued_service_plans","//*/queued_plans[1]/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row").toString()
		logi.logInfo("Plan Count : " + plancount)
		for(int i=1;i<=ratecount.toInteger();i++){
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_Rate seq_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_seq_no[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_From Unit_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_from_unit[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_To Unit_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_to_unit[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_Rate_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_per_unit[1]"))
		}
		/*resultHash.put("Rate seq2", getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row[2]/custom_rate_seq_no[1]"))
		resultHash.put("From Unit2", getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row[2]/custom_rate_from_unit[1]"))
		resultHash.put("Rate2", getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row[2]/custom_rate_per_unit[1]"))*/
		logi.logInfo("Rate schedule input hash map : " + resultHash)
		return resultHash
	}
	
	def md_custom_rate_schedule_Input_modify(String testcaseid) {
		LinkedHashMap <String, String> resultHash = new LinkedHashMap<String, String>()
		String plancount=getNodeCountFromResponse("get_queued_service_plans","//*/*/*:queued_plans_row").toString()
		logi.logInfo("Plan Count : " + plancount)
		String ratecount = getNodeCountFromResponse("get_queued_service_plans","//*/queued_plans[1]/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row").toString()
		logi.logInfo("Plan Count : " + plancount)
		for(int i=1;i<=ratecount.toInteger();i++){
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_Rate seq_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('modify_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_seq_no[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_From Unit_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('modify_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_from_unit[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_To Unit_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('modify_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_to_unit[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_Rate_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('modify_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_per_unit[1]"))
		}
		/*resultHash.put("Rate seq2", getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row[2]/custom_rate_seq_no[1]"))
		resultHash.put("From Unit2", getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row[2]/custom_rate_from_unit[1]"))
		resultHash.put("Rate2", getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row[2]/custom_rate_per_unit[1]"))*/
		logi.logInfo("Rate schedule input hash map : " + resultHash)
		return resultHash
	}
	
	def md_custom_rate_schedule_Input_replace(String testcaseid) {
		LinkedHashMap <String, String> resultHash = new LinkedHashMap<String, String>()
		String plancount=getNodeCountFromResponse("get_queued_service_plans","//*/*/*:queued_plans_row").toString()
		logi.logInfo("Plan Count : " + plancount)
		String ratecount = getNodeCountFromResponse("get_queued_service_plans","//*/queued_plans[1]/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row").toString()
		logi.logInfo("Plan Count : " + plancount)
		for(int i=1;i<=ratecount.toInteger();i++){
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_Rate seq_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('replace_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_seq_no[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_From Unit_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('replace_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_from_unit[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_To Unit_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('replace_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_to_unit[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_Rate_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('replace_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_per_unit[1]"))
		}
		/*resultHash.put("Rate seq2", getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row[2]/custom_rate_seq_no[1]"))
		resultHash.put("From Unit2", getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row[2]/custom_rate_from_unit[1]"))
		resultHash.put("Rate2", getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row[2]/custom_rate_per_unit[1]"))*/
		logi.logInfo("Rate schedule input hash map : " + resultHash)
		return resultHash
	}
	
	def md_custom_rate_schedule_Input_ump(String testcaseid) {
		LinkedHashMap <String, String> resultHash = new LinkedHashMap<String, String>()
		String plancount=getNodeCountFromResponse("get_queued_service_plans","//*/*/*:queued_plans_row").toString()
		logi.logInfo("Plan Count : " + plancount)
		String ratecount = getNodeCountFromResponse("get_queued_service_plans","//*/queued_plans[1]/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row").toString()
		logi.logInfo("Plan Count : " + plancount)
		for(int i=1;i<=ratecount.toInteger();i++){
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_Rate seq_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('update_master_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_seq_no[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_From Unit_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('update_master_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_from_unit[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_To Unit_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('update_master_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_to_unit[1]"))
		resultHash.put(getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_service_no[1]")+"_Service_Rate_"+getValueFromResponse("get_queued_service_plans", "//*/*/*:queued_plans_row[1]/*:custom_rate[1]/*:custom_rate_row["+i+"]/*:custom_rate_seq_no[1]"), getValueFromRequest('update_master_plan',"//new_acct_custom_rates/new_acct_custom_rates_row["+i+"]/custom_rate_per_unit[1]"))
		}
		/*resultHash.put("Rate seq2", getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row[2]/custom_rate_seq_no[1]"))
		resultHash.put("From Unit2", getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row[2]/custom_rate_from_unit[1]"))
		resultHash.put("Rate2", getValueFromRequest('assign_supp_plan',"//new_acct_custom_rates/new_acct_custom_rates_row[2]/custom_rate_per_unit[1]"))*/
		logi.logInfo("Rate schedule input hash map : " + resultHash)
		return resultHash
	}
	
	/**
	 * Verifies get all account active contract APIs Single plan contract details
	 * @param testCaseId
	 * @return true or false
	 */
	def md_GET_ALL_ACCT_ACTIVE_CONTRACTS_SINGLEPLAN(String testcaseId){
		String client_Number = Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)		
		String query="SELECT PLAN_NO, TYPE_NO, CONTRACT_MONTHS, STATUS_CD, CONTRACT_NO FROM ARIACORE.ACCT_PLAN_CONTRACTS WHERE ACCT_NO=" + acct_no + " AND CLIENT_NO=" + client_Number + " AND STATUS_CD NOT IN(-1, -2, -3, 99) ORDER BY CONTRACT_NO"
		int i=1,flag=0
		String response1,response2,response3,response4,response5,status_cd,length_months,type_no,contract_number

		logi.logInfo("Query formed : " + query)
		ResultSet resultSet = db.executePlaQuery(query);
		while(resultSet.next()){	
			   response1=getValueFromResponse('get_all_acct_active_contracts', "//*["+i+"]/*[local-name()='plan_no']")
			   response2=getValueFromResponse('get_all_acct_active_contracts', "//*["+i+"]/*[local-name()='type_no']")
			   response3=getValueFromResponse('get_all_acct_active_contracts', "//*["+i+"]/*[local-name()='length_months']")
			   response4=getValueFromResponse('get_all_acct_active_contracts', "//*["+i+"]/*[local-name()='status_code']")
			   response5=getValueFromResponse('get_all_acct_active_contracts', "//*["+i+"]/*[local-name()='contract_no']")
			   logi.logInfo("Generated responses : " + response1 + '  ' + response2 + '  ' + response3 + '  ' + response4 + '  ' + response5)
			   logi.logInfo("Received from DB: " + resultSet.getString(1) + '  ' + resultSet.getString(2) + '  ' + resultSet.getString(3) + '  ' + resultSet.getString(4) + '  ' + resultSet.getString(5))

			   if ((response1 == resultSet.getString(1)) && (response2 == resultSet.getString(2)) &&
			   (response3 == resultSet.getString(3)) && (response4 == resultSet.getString(4)) && (response5 == resultSet.getString(5)))
			   {
					 flag = flag + 0
			   }
			   else {
					 flag++
			   }
			   i++
		}
		logi.logInfo("Flag1 Value : " + flag)

		if(flag){
			   return 'FALSE'
		}
		return 'TRUE'
 }
	
	
	
	public String md_checkInvoiceAmount_CAH(String testCaseId,int lineItem){
		
				String client_no=Constant.mycontext.expand('${Properties#Client_No}')
				def acct_no
				switch(lineItem){
			case 1:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO1)
			break;
			case 2:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO2)
			break;
			case 3:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO3)
			break;
			case 4:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO4)
			break;
			case 5:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO5)
			break;
		}
				logi.logInfo "check invoice begins"
				ConnectDB database = new ConnectDB();
				SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
				String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL"
				String currentVirtualTime = database.executeQueryP2(Query);
				
				
				logi.logInfo "Current Virtual Time : "+ currentVirtualTime
		
				// Stores the current virtual time in the specified format
				Date currentDate = simpleFormat.parse(currentVirtualTime);
				logi.logInfo "Cureent VTTttttttttt"+currentDate
				Date acctStartDate = getBillStartDate(client_no, acct_no)
				
				logi.logInfo "AccccccccccccctStartDate"+acctStartDate
				// Stores the account created time in the specified format
				int daysInvoice1 = getDaysBetween(acctStartDate, currentDate)
		
				logi.logInfo "daysInvoice: "+daysInvoice1
				HashMap<String, HashMap<String,String>> resultHash
				
				switch(lineItem){
					case 1:
					resultHash = getPlanServiceAndTieredPricing(1)
					break;
					case 2:
					resultHash = getPlanServiceAndTieredPricing(2)
					break;
					case 3:
					resultHash = getPlanServiceAndTieredPricing(3)
					break;
					case 4:
					resultHash = getPlanServiceAndTieredPricing(4)
					break;
					case 5:
					resultHash = getPlanServiceAndTieredPricing(5)
					break;
				}
				
				
				Iterator iterator = resultHash.entrySet().iterator()
				int invoice = 0
		
				while (iterator.hasNext()) {
					//Uvais
					int daysInvoice = daysInvoice1
					Map.Entry pairs = (Map.Entry) iterator.next()
					String plan_no = pairs.getKey().toString()
		
					int index = Integer.parseInt(database.executeQueryP2("select distinct supp_plan_ind from ariacore.client_plan where plan_no ="+plan_no+" and client_no = "+client_no))
					int no_of_units =0
					if(index==0){
						switch(lineItem){
							case 1:
								no_of_units = Integer.parseInt(getValueFromRequest("create_acct_hierarchy","//a1_master_plan_units"))
							break;
							case 2:
								no_of_units = Integer.parseInt(getValueFromRequest("create_acct_hierarchy","//a2_master_plan_units"))
							break;
							case 3:
								no_of_units = Integer.parseInt(getValueFromRequest("create_acct_hierarchy","//a3_master_plan_units"))
							break;
							case 4:
								no_of_units = Integer.parseInt(getValueFromRequest("create_acct_hierarchy","//a4_master_plan_units"))
							break;
							case 5:
								no_of_units = Integer.parseInt(getValueFromRequest("create_acct_hierarchy","//a5_master_plan_units"))
							break;
						}
					}
					
					else {
						String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
						String[] noOfUnits = getSuppPlanUnitsforAcct(acct_no,client_no) 		//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
						String[] supp_plans = getSuppPlansforAcct(acct_no,client_no)//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")
						logi.logInfo "supp_plans.length : "+supp_plans.length
						for( int i=0;i<supp_plans.length;i++) {
							logi.logInfo "supp_plans.length1 : "+supp_plans[i]
							logi.logInfo "supp_plans.length 2: "+plan_no
							if(supp_plans[i].toString().equalsIgnoreCase(plan_no)){
								logi.logInfo "no of plan "+noOfUnits[i]
								no_of_units = Integer.parseInt(noOfUnits[i])
								break
							}
						}
					}
		
					HashMap<String,String> value = pairs.getValue()
					int billing_interval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ plan_no+ "and client_no="+client_no))
					logi.logInfo "Services and Tiered Pricing in a plan : "+plan_no +" are "+ value.toString()
					logi.logInfo "Billing Interval of a plan : "+plan_no +" :  "+ billing_interval
					Date planEndDate = addMonthsFromGivenDate(acctStartDate,billing_interval)
					logi.logInfo "AccccccccccccctStartDate"+acctStartDate
					logi.logInfo "DurationDateeeeeeeee "+planEndDate
					
					int durationOfPlan = getDaysBetween(acctStartDate, planEndDate)
					logi.logInfo "daysInvoice: "+daysInvoice
					logi.logInfo "durationOfPlan : "+plan_no +" :  "+  durationOfPlan
					//Uvais
					int billlagdays = Integer.parseInt(getBillLagDays(billing_interval,client_no))
					logi.logInfo "billlagdays: "+billlagdays
					daysInvoice = daysInvoice + (billlagdays*-1)
					logi.logInfo "daysInvoice: "+daysInvoice
					//Uvais end
					
						Iterator service = value.entrySet().iterator()
						logi.logInfo "Invoice calculation for a plan : "+plan_no
						while(service.hasNext()){
							Map.Entry servicePairs = (Map.Entry) service.next()
							String service_no = servicePairs.getKey().toString()
							logi.logInfo "Invoice calculation for a service : "+service_no
							int seq_no=1;
							String tieredPricing = servicePairs.getValue().toString()
		
							int tieredPricingInt = Integer.parseInt(tieredPricing)
							logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
							String service_type = database.executeQueryP2("select SERVICE_TYPE from ariacore.all_service where (client_no = "+client_no+" or client_no is null) and service_no="+ service_no )
							if(service_type.equalsIgnoreCase("AC")){
								tieredPricingInt = 4
							}
							switch(tieredPricingInt){
								case 1:
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "))
									invoice  = invoice+Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									seq_no++;
									break;
								case 2 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and (to_unit >="+no_of_units+" and from_unit<"+no_of_units+") order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
								case 3 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and (to_unit >="+no_of_units+" and from_unit<"+no_of_units+") order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
								case 4 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("select recurring_factor*rate_per_unit from ariacore.acct_rates_history_details where acct_no =  "+acct_no+"  and client_no = "+client_no+" and service_no = "+service_no+" and plan_no ="+plan_no+" order by rate_hist_seq_no desc"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select recurring_factor*rate_per_unit from ariacore.acct_rates_history_details where acct_no =  "+acct_no+"  and client_no = "+client_no+" and service_no = "+service_no+" and plan_no ="+plan_no+" order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
							}
						}
					
				}
				return invoice
			}
	public String md_checkSecoundInvoiceAmount_CAH(String testCaseId,int lineItem)
	{
		
				String client_no=Constant.mycontext.expand('${Properties#Client_No}')
				def acct_no
				switch(lineItem){
			case 1:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO1)
			break;
			case 2:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO2)
			break;
			case 3:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO3)
			break;
			case 4:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO4)
			break;
			case 5:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO5)
			break;
		}
				logi.logInfo "check invoice begins"
				ConnectDB database = new ConnectDB();
				SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
				String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + client_no + ") from DUAL"
				String currentVirtualTime = database.executeQueryP2(Query);
				
				
				logi.logInfo "Current Virtual Time : "+ currentVirtualTime
		
				// Stores the current virtual time in the specified format
				Date currentDate = simpleFormat.parse(currentVirtualTime);
				logi.logInfo "Cureent VTTttttttttt"+currentDate
				Date acctStartDate = getBillStartDate(client_no, acct_no)
				
				logi.logInfo "AccccccccccccctStartDate"+acctStartDate
				// Stores the account created time in the specified format
				int daysInvoice1 = getDaysBetween(acctStartDate, currentDate)
		
				logi.logInfo "daysInvoice: "+daysInvoice1
				HashMap<String, HashMap<String,String>> resultHash
				
				switch(lineItem){
					case 1:
					resultHash = getPlanServiceAndTieredPricing1(1)
					break;
					case 2:
					resultHash = getPlanServiceAndTieredPricing1(2)
					break;
					case 3:
					resultHash = getPlanServiceAndTieredPricing1(3)
					break;
					case 4:
					resultHash = getPlanServiceAndTieredPricing1(4)
					break;
					case 5:
					resultHash = getPlanServiceAndTieredPricing1(5)
					break;
				}
				
				
				Iterator iterator = resultHash.entrySet().iterator()
				int invoice = 0
		
				while (iterator.hasNext()) {
					//Uvais
					int daysInvoice = daysInvoice1
					Map.Entry pairs = (Map.Entry) iterator.next()
					String plan_no = pairs.getKey().toString()
		
					int index = Integer.parseInt(database.executeQueryP2("select distinct supp_plan_ind from ariacore.client_plan where plan_no ="+plan_no+" and client_no = "+client_no))
					int no_of_units =0
					if(index==0){
						switch(lineItem){
							case 1:
								no_of_units = Integer.parseInt(getValueFromRequest("create_acct_hierarchy","//a1_master_plan_units"))
							break;
							case 2:
								no_of_units = Integer.parseInt(getValueFromRequest("create_acct_hierarchy","//a2_master_plan_units"))
							break;
							case 3:
								no_of_units = Integer.parseInt(getValueFromRequest("create_acct_hierarchy","//a3_master_plan_units"))
							break;
							case 4:
								no_of_units = Integer.parseInt(getValueFromRequest("create_acct_hierarchy","//a4_master_plan_units"))
							break;
							case 5:
								no_of_units = Integer.parseInt(getValueFromRequest("create_acct_hierarchy","//a5_master_plan_units"))
							break;
						}
					}
					
					else {
						String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)
						String[] noOfUnits = getSuppPlanUnitsforAcct(acct_no,client_no) 		//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
						String[] supp_plans = getSuppPlansforAcct(acct_no,client_no)//getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")
						logi.logInfo "supp_plans.length : "+supp_plans.length
						for( int i=0;i<supp_plans.length;i++) {
							logi.logInfo "supp_plans.length1 : "+supp_plans[i]
							logi.logInfo "supp_plans.length 2: "+plan_no
							if(supp_plans[i].toString().equalsIgnoreCase(plan_no)){
								logi.logInfo "no of plan "+noOfUnits[i]
								no_of_units = Integer.parseInt(noOfUnits[i])
								break
							}
						}
					}
		
					HashMap<String,String> value = pairs.getValue()
					int billing_interval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ plan_no+ "and client_no="+client_no))
					logi.logInfo "Services and Tiered Pricing in a plan : "+plan_no +" are "+ value.toString()
					logi.logInfo "Billing Interval of a plan : "+plan_no +" :  "+ billing_interval
					Date planEndDate = addMonthsFromGivenDate(acctStartDate,billing_interval)
					logi.logInfo "AccccccccccccctStartDate"+acctStartDate
					logi.logInfo "DurationDateeeeeeeee "+planEndDate
					
					int durationOfPlan = getDaysBetween(acctStartDate, planEndDate)
					logi.logInfo "daysInvoice: "+daysInvoice
					logi.logInfo "durationOfPlan : "+plan_no +" :  "+  durationOfPlan
					//Uvais
					int billlagdays = Integer.parseInt(getBillLagDays(billing_interval,client_no))
					logi.logInfo "billlagdays: "+billlagdays
					daysInvoice = daysInvoice + (billlagdays*-1)
					logi.logInfo "daysInvoice: "+daysInvoice
					//Uvais end
					
						Iterator service = value.entrySet().iterator()
						logi.logInfo "Invoice calculation for a plan : "+plan_no
						while(service.hasNext()){
							Map.Entry servicePairs = (Map.Entry) service.next()
							String service_no = servicePairs.getKey().toString()
							logi.logInfo "Invoice calculation for a service : "+service_no
							int seq_no=1;
							String tieredPricing = servicePairs.getValue().toString()
		
							int tieredPricingInt = Integer.parseInt(tieredPricing)
							logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
							String service_type = database.executeQueryP2("select SERVICE_TYPE from ariacore.all_service where (client_no = "+client_no+" or client_no is null) and service_no="+ service_no )
							if(service_type.equalsIgnoreCase("AC")){
								tieredPricingInt = 4
							}
							switch(tieredPricingInt){
								case 1:
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "))
									invoice  = invoice+Integer.parseInt(database.executeQueryP2("SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+acct_no+"  AND ARHD.PLAN_NO ="+plan_no+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+client_no+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+client_no+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									seq_no++;
									break;
								case 2 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and (to_unit >="+no_of_units+" and from_unit<"+no_of_units+") order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
								case 3 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and ("+no_of_units+" between from_unit and to_unit or to_unit is null) and rownum = 1"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+acct_no+" and client_no = "+client_no+" and service_no = "+service_no+" and plan_no = "+plan_no+" and (to_unit >="+no_of_units+" and from_unit<"+no_of_units+") order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
								case 4 :
									logi.logInfo "invoice for plan : "+plan_no +" Service : "+ service_no +" is "+ Integer.parseInt(database.executeQueryP2("select recurring_factor*rate_per_unit from ariacore.acct_rates_history_details where acct_no =  "+acct_no+"  and client_no = "+client_no+" and service_no = "+service_no+" and plan_no ="+plan_no+" order by rate_hist_seq_no desc"))
									invoice  = invoice + Integer.parseInt(database.executeQueryP2("select recurring_factor*rate_per_unit from ariacore.acct_rates_history_details where acct_no =  "+acct_no+"  and client_no = "+client_no+" and service_no = "+service_no+" and plan_no ="+plan_no+" order by rate_hist_seq_no desc"))
									logi.logInfo "After adding with invoice " + invoice
									break;
							}
						}
					
				}
				return invoice
			}
	
	
	public HashMap getPlanServiceAndTieredPricing(int lineItem){
		
		def acct_no;
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		switch(lineItem){
			case 1:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO1)
			break;
			case 2:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO2)
			break;
			case 3:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO3)
			break;
			case 4:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO4)
			break;
			case 5:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO5)
			break;
		}
		
		
		ConnectDB database = new ConnectDB();
		String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" or custom_to_client_no is null) and usage_based!=1) order by plan_no, service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" or custom_to_client_no is null) and usage_based!=1) order by plan_no, service_no"))
		ResultSet resultSet = database.executeQuery(query)

		HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		HashMap<String,String> serviceHash

		String[] plan = new String[count]
		String[] service = new String[count]
		String[] tieredPricing = new String[count]

		int i= 0
		while(resultSet.next()){
			plan[i]=resultSet.getString(1)
			service[i]=resultSet.getString(2)
			tieredPricing[i]=resultSet.getString(3)
			i++
		}
		String plan_no
		int serviceCount=0
		for(int planCount=0;planCount<plan.length;){
			plan_no = plan[planCount]
			serviceHash = new HashMap<String,String> ()
			for(;serviceCount<service.length;serviceCount++){
				if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					planCount ++
				}
				else
					break
			}
			resultHash.put(plan_no, serviceHash)
			logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
		}
		logi.logInfo "Total Services" + resultHash.toString()
		return resultHash
	}
	public HashMap getPlanServiceAndTieredPricing1(int lineItem){
		
		def acct_no;
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		switch(lineItem){
			case 1:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO1)
			break;
			case 2:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO2)
			break;
			case 3:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO3)
			break;
			case 4:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO4)
			break;
			case 5:
			acct_no = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO5)
			break;
		}
		
		
		ConnectDB database = new ConnectDB();
		String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" or custom_to_client_no is null) and recurring=1) order by plan_no, service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select plan_no from ariacore.acct_service_map where acct_no = "+acct_no+" GROUP BY plan_no) and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where (custom_to_client_no = "+client_no+" or custom_to_client_no is null) and recurring=1) order by plan_no, service_no"))
		ResultSet resultSet = database.executeQuery(query)

		HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		HashMap<String,String> serviceHash

		String[] plan = new String[count]
		String[] service = new String[count]
		String[] tieredPricing = new String[count]

		int i= 0
		while(resultSet.next()){
			plan[i]=resultSet.getString(1)
			service[i]=resultSet.getString(2)
			tieredPricing[i]=resultSet.getString(3)
			i++
		}
		String plan_no
		int serviceCount=0
		for(int planCount=0;planCount<plan.length;){
			plan_no = plan[planCount]
			serviceHash = new HashMap<String,String> ()
			for(;serviceCount<service.length;serviceCount++){
				if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					planCount ++
				}
				else
					break
			}
			resultHash.put(plan_no, serviceHash)
			logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
		}
		logi.logInfo "Total Services" + resultHash.toString()
		return resultHash
	}
public String md_checkInvoiceAmount_CAH_1(String testCaseId)
{
	return md_checkInvoiceAmount_CAH(testCaseId,1);
}
public String md_checkInvoiceAmount_CAH_2(String testCaseId)
{
	return md_checkInvoiceAmount_CAH(testCaseId,2);
}
public String md_checkInvoiceAmount_CAH_3(String testCaseId)
{
	return md_checkInvoiceAmount_CAH(testCaseId,3);
}
public String md_checkInvoiceAmount_CAH_4(String testCaseId)
{
	return md_checkInvoiceAmount_CAH(testCaseId,4);
}
public String md_checkInvoiceAmount_CAH_5(String testCaseId)
{
	return md_checkInvoiceAmount_CAH(testCaseId,5);
}

//Secound invoice Amount

public String md_checkSecondInvoiceAmount_CAH_1(String testCaseId)
{
	return md_checkSecoundInvoiceAmount_CAH(testCaseId,1);
}
public String md_checkSecondInvoiceAmount_CAH_2(String testCaseId)
{
	return md_checkSecoundInvoiceAmount_CAH(testCaseId,2);
}
public String md_checkSecondInvoiceAmount_CAH_3(String testCaseId)
{
	return md_checkSecoundInvoiceAmount_CAH(testCaseId,3);
}
public String md_checkSecondInvoiceAmount_CAH_4(String testCaseId)
{
	return md_checkSecoundInvoiceAmount_CAH(testCaseId,4);
}
public String md_checkSecondInvoiceAmount_CAH_5(String testCaseId)
{
	return md_checkSecoundInvoiceAmount_CAH(testCaseId,5);
}



public String md_senior_acct_no_exp(String testCaseId,int lineItem){
	def senior_acct_no_exp
	switch(lineItem){
	case 1:
	senior_acct_no_exp = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO1)
	break;
	case 2:
	senior_acct_no_exp = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO1)
	break;
	case 3:
	senior_acct_no_exp = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO2)
	break;
	case 4:
	senior_acct_no_exp = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO3)
	break;
	case 5:
	senior_acct_no_exp = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO4)
	break;
	}
	return senior_acct_no_exp
}

public String md_senior_acct_no_exp_1(String testCaseId)
{
	return md_senior_acct_no_exp(testCaseId,1);
}
public String md_senior_acct_no_exp_2(String testCaseId)
{
	return md_senior_acct_no_exp(testCaseId,2);
}
public String md_senior_acct_no_exp_3(String testCaseId)
{
	return md_senior_acct_no_exp(testCaseId,3);
}
public String md_senior_acct_no_exp_4(String testCaseId)
{
	return md_senior_acct_no_exp(testCaseId,4);
}
public String md_senior_acct_no_exp_5(String testCaseId)
{
	return md_senior_acct_no_exp(testCaseId,5);
}

public String md_senior_acct_no_act(String testCaseId,int lineItem){
	def senior_acct_no_act
	ConnectDB database = new ConnectDB();
	switch(lineItem){
	case 1:
	senior_acct_no_act = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO1)
	
	break;
	case 2:
	senior_acct_no_act = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO2)
	break;
	case 3:
	senior_acct_no_act = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO3)
	break;
	case 4:
	senior_acct_no_act = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO4)
	break;
	case 5:
	senior_acct_no_act = getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO5)
	break;
	}
	senior_acct_no_act = database.executeQueryP2("select senior_acct_no from ariacore.acct where acct_no="+senior_acct_no_act)
	return senior_acct_no_act
}

public String md_senior_acct_no_act_1(String testCaseId)
{
	return md_senior_acct_no_act(testCaseId,1);
}
public String md_senior_acct_no_act_2(String testCaseId)
{
	return md_senior_acct_no_act(testCaseId,2);
}
public String md_senior_acct_no_act_3(String testCaseId)
{
	return md_senior_acct_no_act(testCaseId,3);
}
public String md_senior_acct_no_act_4(String testCaseId)
{
	return md_senior_acct_no_act(testCaseId,4);
}
public String md_senior_acct_no_act_5(String testCaseId)
{
	return md_senior_acct_no_act(testCaseId,5);
}

def md_gl_detail_line_items(String testcaseid) {
	LinkedHashMap <String, String> resultHash = new LinkedHashMap<String, String>()
	ConnectDB db = new ConnectDB()
	
	String account_number = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	String invoiceQuery = "select max(invoice_no) from ariacore.gl where acct_no=" + account_number
	def invoiceNo = db.executeQueryP2(invoiceQuery)
	String Query = "select seq_num,service_no,debit,Comments from ariacore.gl_detail where invoice_no="+invoiceNo+" and plan_no is null"
	ResultSet resultSet = db.executeQuery(Query)
	while(resultSet.next()){
	resultHash.put(resultSet.getString(4), resultSet.getString(3))	
	}
	logi.logInfo("Rate schedule input hash map : " + resultHash)
	return resultHash
}


def md_gl_tax_detail_line_items(String testcaseid) {
	LinkedHashMap <String, String> resultHash = new LinkedHashMap<String, String>()
	ConnectDB db = new ConnectDB()
	
	String account_number = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	String invoiceQuery = "select max(invoice_no) from ariacore.gl where acct_no=" + account_number
	def invoiceNo = db.executeQueryP2(invoiceQuery)
	String Query = "select seq_num,service_no,debit,Comments from ariacore.gl_detail where invoice_no="+invoiceNo+" and plan_no is null"
	ResultSet resultSet = db.executeQuery(Query)
	while(resultSet.next()){
		Query= "Select sum(debit) from ariacore.gl_tax_detail where invoice_no="+invoiceNo+" and seq_num="+	resultSet.getString(1)
		ResultSet rs = db.executeQuery(Query)
		resultHash.put(resultSet.getString(4), rs.getString(1))
	}
	logi.logInfo("Rate schedule input hash map : " + resultHash)
	return resultHash
}

/**
 * Verifies account's all credits
 * @param testCaseId
 * @return hash
 */
def md_VERIFY_ALL_ACCOUNT_CREDIT_AMOUNTS(String testcaseId){

	logi.logInfo("Inside Method md_VERIFY_ALL_ACCOUNT_CREDIT_AMOUNTS")
	// Getting the account number and client id
	def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	def Client_No = getValueFromRequest("create_acct_complete","//client_no")
	
	// Getting the prorated plan
	ConnectDB databaseee = new ConnectDB()
	String prorated_plan_no
	String current_api = Constant.SERVICENAME
	Date last_bill_thru_date
	
	//Supp Plan
	if (current_api.contains('supp')){
		if(current_api.contains('replace')){
			prorated_plan_no = databaseee.executeQueryP2('select supp_plan_no from (select * from acct_supp_plan_map_view where acct_no = '+ acct_no +' and client_no = '+ Client_No + ' and TERMINATE_DATE is not null order by update_date desc) where rownum=1');
			logi.logInfo("Prorated supp plan number: " + prorated_plan_no)
		}
		else
		{
			prorated_plan_no = databaseee.executeQueryP2('select supp_plan_no from (select supp_plan_no from acct_supp_plan_hist where acct_no = '+acct_no+' and client_no = '+Client_No+' order by update_date desc) where rownum=1');
			logi.logInfo("Prorated supp plan number: " + prorated_plan_no)
		}
		//String accountNo = acct_no
		//String clientNo = Client_No
		//useddays=database.executeQueryP2("SELECT TRUNC(LAST_BILL_DATE) - (select CASE WHEN  (trunc(plan_date) >= trunc(last_recur_bill_date) or last_recur_bill_date is null ) THEN trunc(plan_date) ELSE trunc(last_recur_bill_date)  END 	FROM ariacore.acct_supp_plan_map_view WHERE acct_no = "+accountNo+" and supp_plan_no = "+ prorated_plan_no + ") as used_days from ariacore.acct_supp_plan_map_view WHERE acct_no = "+accountNo+" and supp_plan_no = " + prorated_plan_no + " AND client_no ="+clientNo).toInteger()
		//logi.logInfo("Supp plan used days: " + useddays.toString())
	}
	//Master Plan
	else{
		prorated_plan_no = databaseee.executeQueryP2('select old_plan from acct_plan_hist where acct_no = '+acct_no+' and client_no = '+Client_No+' and to_date is null ');
		logi.logInfo("Prorated master plan number: " + prorated_plan_no)
		//String accountNo = acct_no
		//String clientNo = Client_No
		//useddays=database.executeQueryP2("SELECT TRUNC(LAST_BILL_THRU_DATE+1) - (select CASE WHEN  (trunc(plan_date) >= trunc(last_recur_bill_date) or last_recur_bill_date is null ) THEN trunc(plan_date) ELSE trunc(last_recur_bill_date)  END 	FROM ariacore.acct WHERE acct_no = "+accountNo+")from ariacore.acct WHERE acct_no = "+accountNo+"  AND client_no ="+clientNo).toInteger()
		//logi.logInfo("Master plan used days: " + useddays.toString())
		last_bill_thru_date=databaseee.executeQueryP2("SELECT TRUNC(LAST_BILL_THRU_DATE+1) FROM ariacore.acct WHERE acct_no = "+acct_no+" AND client_no ="+Client_No).toDate()
	}
	
	//Check for any other invoices
	String recent_invoice = databaseee.executeQueryP2("select max(invoice_no) from gl_detail where plan_no = "+prorated_plan_no+" and invoice_no not in (select invoice_no from gl_detail where plan_no = "+prorated_plan_no+" and orig_charge_invoice_no in(select invoice_no from gl where acct_no = "+acct_no+" and client_no = "+Client_No+")) and invoice_no in (select invoice_no from gl where acct_no = "+acct_no+" and client_no="+Client_No+")")
	
	if (recent_invoice == null){
		// NO Invoices before for the prorated plan so credit is 0
		logi.logInfo("No invoices for the plan")
		return '0'
	}
	else
	{
		logi.logInfo("Recent invoice for the prorated plan: " + recent_invoice)
		//Calculate Net Amount for the prorated plan
		//String rc_service_amount = databaseee.executeQueryP2("select sum(debit) from gl_detail where invoice_no = "+recent_invoice+" and plan_no = "+prorated_plan_no+" and client_no = "+Client_No+" and service_no in (select service_no from plan_services where plan_no = "+prorated_plan_no+" and service_no in (select service_no from all_service where service_type= 'RC'))")
		//String total_tax_for_rc = databaseee.executeQueryP2("select sum(debit) from gl_tax_detail where taxed_seq_num in(select seq_num from gl_detail where invoice_no = "+recent_invoice+" and plan_no = "+prorated_plan_no+" and client_no = "+Client_No+" and service_no in (select service_no from plan_services where plan_no = "+prorated_plan_no+" and service_no in (select service_no from all_service where service_type= 'RC'))) and invoice_no ="+recent_invoice)
		//double total_charge_amount = rc_service_amount.toDouble() + total_tax_for_rc.toDouble()
		//Coupon amount
		
		//Getting eligible sequence numbers for credit from the recent invoice of the prorated plan number
		ResultSet resultSet = databaseee.executePlaQuery("SELECT seq_num FROM gl_detail WHERE invoice_no = "+recent_invoice+" AND debit > 0 AND client_no    = "+Client_No+" and plan_no = "+prorated_plan_no+" AND (service_no IN (SELECT service_no FROM plan_services WHERE plan_no   = "+prorated_plan_no+" AND service_no IN (    (SELECT service_no FROM all_service WHERE service_type= 'RC'  ))  ))  ORDER BY seq_num");
		ArrayList<String> eligble_seq_numbers = new ArrayList<String>();
		while (resultSet.next()){
			eligble_seq_numbers.add resultSet.getString(1)
		}
		logi.logInfo("Eligible invoice line numbers for credit - " + eligble_seq_numbers.toString())
		
		//Proration Factor
		double unused_days =  databaseee.executeQueryP2("select (trunc(end_date) - (SELECT trunc(ARIACORE.ARIAVIRTUALTIMESTAMP("+Client_No+")) from DUAL) + 1) as unused_days from gl_detail where client_no="+Client_No+" and invoice_no = "+recent_invoice+" and plan_no = "+prorated_plan_no+" and start_date is not null and rownum=1 ").toDouble()
		//databaseee.executeQueryP2("select (trunc(proration_related_end_date) - trunc(proration_related_start_date) + 1) as unused_days from all_credits where acct_no = "+acct_no+" and orig_charge_invoice_no = "+recent_invoice+" and proration_related_start_date is not null and rownum=1")
		double total_cycle_days = databaseee.executeQueryP2("select (trunc(end_date) - trunc(start_date) + 1) as total_cycle_days from gl_detail where client_no="+Client_No+" and invoice_no = "+recent_invoice+" and plan_no = "+prorated_plan_no+" and start_date is not null and rownum=1 ").toDouble()
		double prorated_factor = unused_days / total_cycle_days
		logi.logInfo("Proration factor: " + prorated_factor.toString())
		logi.logInfo("Proration factor, rounded: " + prorated_factor.round(10).toString())
		//Iterating through the eligible sequence numbers
		//Credit for the eligible recurring line items and tax line items will be put in to hash
		LinkedHashMap<String,String> calculated_creadit_hash = new LinkedHashMap<String,String>();
		//int rep_seq_num =1
		//char rep_seq_num = 'A'
		
		String external_tax_client = databaseee.executeQueryP2("SELECT METHOD_CD FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO=" + Client_No)
		
		for(int es_index=0 ; es_index<eligble_seq_numbers.size ; es_index++)
		{
			String netAmount =  databaseee.executeQueryP2("select debit - (select NVL(SUM(amt),0) from gl_credit_detail where invoice_no = "+recent_invoice+" and credited_seq_num = "+eligble_seq_numbers.get(es_index)+") from gl_detail where invoice_no = "+recent_invoice+" and client_no = "+Client_No+" and seq_num = " + eligble_seq_numbers.get(es_index))
			
			double creditAmount
			logi.logInfo ("Net amount for invoice line item: " + eligble_seq_numbers.get(es_index).toString() + " = " + netAmount.toString() )
			DecimalFormat df = new DecimalFormat("#.##");
			if (netAmount.toDouble() <= 0) {
				creditAmount = 0
				calculated_creadit_hash.put(eligble_seq_numbers[es_index] + '. Credit For Invoice Line Item ' + eligble_seq_numbers.get(es_index) + " - " + databaseee.executeQueryP2("select comments from gl_detail where client_no="+Client_No+" and invoice_no = "+recent_invoice+" and seq_num =  " + eligble_seq_numbers.get(es_index)), '0')
				//rep_seq_num = rep_seq_num+1
			}
			else
			{
				//creditAmount = (netAmount.toDouble() * (prorated_factor.round(10)))
				creditAmount =((external_tax_client == "10" || external_tax_client == "13") && external_tax_client != null) ? (netAmount.toDouble() * (prorated_factor.round(10))).trunc(2):(netAmount.toDouble() * (prorated_factor.round(10))).round(2)
				calculated_creadit_hash.put(eligble_seq_numbers[es_index] + ". Credit For Invoice Line Item " + eligble_seq_numbers.get(es_index) + " - " + databaseee.executeQueryP2("select comments from gl_detail where client_no="+Client_No+" and invoice_no = "+recent_invoice+" and seq_num =  " + eligble_seq_numbers.get(es_index)), df.format(creditAmount).toString())
				//rep_seq_num = rep_seq_num+1
			}

			//Check for tax lines
			ResultSet tax_lines = databaseee.executePlaQuery("select seq_num, sum(debit) from gl_tax_detail where invoice_no = "+recent_invoice+" and taxed_seq_num = "+eligble_seq_numbers.get(es_index)+" group by seq_num order by seq_num desc")
			ArrayList<String> tax_seq_numbers = new ArrayList<String>();
			ArrayList<String> tax_seq_numbers_amt = new ArrayList<String>();
			while (tax_lines.next()){
				tax_seq_numbers.add tax_lines.getString(1)
				tax_seq_numbers_amt.add tax_lines.getString(2)
			}
			
			for(int tx_index=0 ; tx_index<tax_seq_numbers.size ; tx_index++){
				double taxAmount = ((external_tax_client == "10" || external_tax_client == "13") && external_tax_client != null) ? (tax_seq_numbers_amt.get(tx_index).toDouble() * (prorated_factor.round(10))).trunc(2):(tax_seq_numbers_amt.get(tx_index).toDouble() * (prorated_factor.round(10))).round(2)
				// (tax_seq_numbers_amt.get(tx_index).toDouble() * (prorated_factor.round(10))).trunc(2)
				logi.logInfo ("Tax Amount: " + taxAmount.toString())
				//calculated_creadit_hash.put(rep_seq_num.toString() + ". Credit For Invoice Line Item " + tax_seq_numbers.get(tx_index) + " - " + databaseee.executeQueryP2("select comments from gl_detail where client_no="+Client_No+" and invoice_no = "+recent_invoice+" and seq_num =  " + tax_seq_numbers.get(tx_index)), df.format(taxAmount).toString())
				calculated_creadit_hash.put(eligble_seq_numbers[es_index]+ ". Credit For Invoice Line Item " + tax_seq_numbers.get(tx_index) + " - " + databaseee.executeQueryP2("select comments from gl_detail where client_no="+Client_No+" and invoice_no = "+recent_invoice+" and seq_num =  " + tax_seq_numbers.get(tx_index)), df.format(taxAmount).toString())
				//rep_seq_num = rep_seq_num + 1
			}
			
		}
		
		logi.logInfo ("Calculated credit hash ==> " + calculated_creadit_hash.toString())
		return calculated_creadit_hash.sort()
	}
}

/**
 * Verifies account's all credits
 * @param testCaseId
 * @return hash
 */
def md_VERIFY_ALL_ACCOUNT_CREDIT_AMOUNTS_DB(String testcaseId){

	logi.logInfo("Inside Method md_VERIFY_ALL_ACCOUNT_CREDIT_AMOUNTS_DB")
	// Getting the account number and client id
	def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	def Client_No = getValueFromRequest("create_acct_complete","//client_no")
	DecimalFormat df = new DecimalFormat("#.##");
	// Getting the prorated plan
	ConnectDB databaseee = new ConnectDB()
	String prorated_plan_no
	String current_api = Constant.SERVICENAME
	
	//Supp Plan
	if (current_api.contains('supp')){
		if(current_api.contains('replace')){
			prorated_plan_no = databaseee.executeQueryP2('select supp_plan_no from (select * from acct_supp_plan_map_view where acct_no = '+ acct_no +' and client_no = '+ Client_No + ' and TERMINATE_DATE is not null order by update_date desc) where rownum=1');
			logi.logInfo("Prorated supp plan numberDB: " + prorated_plan_no)
		}
		else
		{
			prorated_plan_no = databaseee.executeQueryP2('select supp_plan_no from (select supp_plan_no from acct_supp_plan_hist where acct_no = '+acct_no+' and client_no = '+Client_No+' order by update_date desc) where rownum=1');
			logi.logInfo("Prorated supp plan numberDB: " + prorated_plan_no)
		}
	}
	//Master Plan
	else{
		prorated_plan_no = databaseee.executeQueryP2('select old_plan from acct_plan_hist where acct_no = '+acct_no+' and client_no = '+Client_No+' and to_date is null ');
		logi.logInfo("Prorated master plan number: " + prorated_plan_no)
	}
		
	//Check for any other invoices
	String recent_invoice = databaseee.executeQueryP2('select max(invoice_no) from gl_detail where plan_no = '+prorated_plan_no+' and invoice_no not in (select invoice_no from gl_detail where plan_no = '+prorated_plan_no+' and orig_charge_invoice_no in(select invoice_no from gl where acct_no = '+acct_no+' and client_no = '+Client_No+')) and invoice_no in (select invoice_no from gl where acct_no = '+acct_no+' and client_no='+Client_No+')')
	
	if (recent_invoice == null){
		// NO Invoices before for the prorated plan so credit is 0
		logi.logInfo("No invoice for the plan")
		return '0'
	}
	else {
		logi.logInfo("Recent invoice for the plan: " + recent_invoice)
		
		//Getting elgible service line items seq no from all credits table
		int ctr = -1
		def elgible_seq_numbers = []
		elgible_seq_numbers  = Arrays.asList(databaseee.executeQueryReturnValuesAsArray("select orig_charge_seq_num from all_credits  where acct_no = "+acct_no+" and TO_CHAR(create_date,'dd-MON-yyyy')=(SELECT TO_CHAR(ariacore.ariavirtualtimestamp("+Client_No+"),'dd-MON-yyyy') FROM dual) and orig_charge_invoice_no = "+recent_invoice+" and proration_factor is not null order by credit_id"))
		
		//Getting credited line items from all credits table
		ResultSet resultSet = databaseee.executePlaQuery("select credit_id from all_credits  where acct_no = "+acct_no+" and TO_CHAR(create_date,'dd-MON-yyyy')=(SELECT TO_CHAR(ariacore.ariavirtualtimestamp("+Client_No+"),'dd-MON-yyyy') FROM dual) and orig_charge_invoice_no = "+recent_invoice+" order by credit_id");
		ArrayList<String> all_credit_seq_numbers = new ArrayList<String>();
		while (resultSet.next()){
			all_credit_seq_numbers.add resultSet.getString(1)
		}
		LinkedHashMap<String,String> db_creadited_hash = new LinkedHashMap<String,String>();
		//int rep_seq_num = 1
		//char rep_seq_num = 'A'
		for(int ac_index=0 ; ac_index<all_credit_seq_numbers.size ; ac_index++)
		{
			
			ResultSet creditedAmount =  databaseee.executePlaQuery("select orig_charge_seq_num,amount from all_credits  where acct_no = "+acct_no+" and orig_charge_invoice_no = "+recent_invoice+" and credit_id = " + all_credit_seq_numbers.get(ac_index))
			creditedAmount.next()
			String ca_seq_num = creditedAmount.getString(1)
			String ca_seq_num_amt = creditedAmount.getString(2)
			//db_creadited_hash.put(rep_seq_num.toString() + ". Credit For Invoice Line Item " + ca_seq_num + " - " + databaseee.executeQueryP2("select comments from gl_detail where client_no="+Client_No+" and invoice_no = "+recent_invoice+" and seq_num =  " + ca_seq_num), df.format(ca_seq_num_amt.toDouble()).toString())
			if(elgible_seq_numbers.contains(ca_seq_num) ){
				logi.logInfo("To the sequence num values ")
				ctr++
			}
			db_creadited_hash.put(elgible_seq_numbers.get(ctr)+ ". Credit For Invoice Line Item " + ca_seq_num + " - " + databaseee.executeQueryP2("select comments from gl_detail where client_no="+Client_No+" and invoice_no = "+recent_invoice+" and seq_num =  " + ca_seq_num), df.format(ca_seq_num_amt.toDouble()).toString())
			//rep_seq_num= rep_seq_num + 1
		}
		
		logi.logInfo ("Credits DB hash ==> " + db_creadited_hash.toString())
		return db_creadited_hash.sort()
		
	}
}

public String md_GET_ACCT_NO_FROM_USERID(String testcaseId)
{
	logi.logInfo ("Calling md_GET_ACCT_NO_FROM_USERID")
	String userid = getValueFromRequest("create_acct_complete","//userid")
	String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
	ConnectDB db = new ConnectDB()	
	String acct_no=db.executeQueryP2("select acct_no from ariacore.acct where userid='"+userid+"' and client_no="+clientNo)
	return acct_no
}
public String md_GET_CHILD_ACCT_NO_FROM_USERID(String testcaseId)
{
	logi.logInfo ("Calling md_GET_CHILD_ACCT_NO_FROM_USERID")
	String userid = getValueFromRequest("create_acct_complete.a","//userid")
	String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
	ConnectDB db = new ConnectDB()
	String acct_no=db.executeQueryP2("select acct_no from ariacore.acct where userid='"+userid+"' and client_no="+clientNo)
	return acct_no
}

/**
 * Verifies the sprint name
 * @param testCaseId
 * @return sprintName as string
 */
public String md_get_sprint_name(String testcaseId)
{
	logi.logInfo ("Calling md_get_sprint_name")
	ConnectDB dbConnection = new ConnectDB()
	String sprintDetails, sprintName
	sprintDetails = dbConnection.executeQueryP2("select param_val from ariacore.aria_global_params WHERE param_name = 'current_system_version'")
	if(sprintDetails.contains("_"))
	sprintName = sprintDetails.split('_')[2].replace(")","")
	else if(sprintDetails.contains("("))
	sprintName = sprintDetails.replace("(","").replace(")","")
	else
	sprintName= sprintDetails
	return sprintName
}

def md_get_request_client_receipt_id(String testcaseId) {
	String rec_id = getValueFromRequest("record_usage","//client_receipt_id")
	return rec_id.toLowerCase().trim()
}


public String  md_Is_Auto_Service_Credit_Applied_On_Order(String tcid)
{
	logi.logInfo("Calling md_Is_Auto_Service_Credit_Applied_On_Order")	
	String flg="FALSE"
	String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
	String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	String query="SELECT count(*)  from ariacore.credits where reason_cd=200 and client_no=%s and acct_no=%s"
	ConnectDB db = new ConnectDB()
	int credit_count=db.executeQueryP2(String.format(query,clientNo,acct_no)).toInteger()
	if(credit_count>0)
	flg="TRUE"
	return flg
}

def md_get_request_client_receipt_id_bulk_(String testcaseId) {
	String rec_id = getValueFromRequest("record_usage","//client_receipt_id")
	return rec_id.toLowerCase().trim()
}



public String md_UMP_UNIT_CHANGE_CREDIT_AMOUNT(String tcid)
{
	logi.logInfo("inside md_UPDATE_MASTER_PLAN_CREDIT_AMOUNT")
	DecimalFormat d = new DecimalFormat("#.##########");
	String apiname=tcid.split("-")[2]
	//String acct_no = getValueFromRequest(apiname,"//acct_no")
	String acct_no = getValueFromResponse("create_acct_complete","//acct_no")
	//String client_no = getValueFromRequest(apiname,"//client_no")
	String client_no=Constant.mycontext.expand('${Properties#Client_No}')
	String credit_amt
	Date assigned_date
	Date unassigned_date
	ConnectDB db = new ConnectDB()
	
	String plan_no = db.executeQueryP2("SELECT old_plan FROM ariacore.acct_plan_hist WHERE acct_no ="+acct_no+" AND old_plan =0").toString()
	logi.logInfo("old plan no:"+plan_no)
	String date_qry = "select trunc(from_date),trunc(to_date) FROM ariacore.acct_plan_hist WHERE old_plan="+plan_no+" and acct_no ="+acct_no
	ResultSet rs_date_qry = db.executePlaQuery(date_qry)
	while(rs_date_qry.next())
	{
		assigned_date = rs_date_qry.getDate(1)
		unassigned_date = rs_date_qry.getDate(2)
	}
	
	

	logi.logInfo("plan assigned date:"+assigned_date.toString())
	logi.logInfo("plan plan unassigned date:"+unassigned_date.toString())
	plan_no = db.executeQueryP2("SELECT old_plan FROM ariacore.acct_plan_hist WHERE acct_no ="+acct_no+" AND old_plan !=0").toString()
	logi.logInfo("old plan no:"+plan_no)
	int inv_count = db.executeQueryP2("SELECT count(invoice_no) FROM ariacore.gl  WHERE acct_no="+acct_no+" and plan_no="+plan_no).toString().toInteger()
	if(!inv_count.equals("0"))
	{
		if(inv_count > 2)
		{
			logi.logInfo " Inside inv_count "
			String assigned_date_qry = "SELECT trunc(due_date) FROM ariacore.gl WHERE acct_no="+acct_no+" and plan_no="+plan_no+" and invoice_no=(SELECT max(invoice_no) FROM ariacore.gl WHERE acct_no="+acct_no+" and plan_no="+plan_no+")"
			ResultSet rs= db.executePlaQuery(assigned_date_qry)
			while(rs.next())
			{
				assigned_date = rs.getDate(1)
			}
			logi.logInfo("new assigned date"+assigned_date.toString())
		}
		int used_days= getDaysBetween(assigned_date,unassigned_date)+1
		logi.logInfo("plan used days:"+used_days)

		String bill_int_qry ="SELECT distinct billing_interval FROM ariacore.client_plan WHERE plan_no="+plan_no+" AND client_no="+client_no
		int billing_int = db.executeQueryP2(bill_int_qry).toInteger()
		logi.logInfo("plan billing interval"+billing_int)
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		
		Calendar now = Calendar.getInstance();
		now.setTime(assigned_date);
		now.add(Calendar.MONTH,billing_int);
		String tem1 = sdf.format(now.getTime()).toString()
		logi.logInfo("after adding billing interval:"+tem1) //now.getTime()).toString())
		Date next_bill_date =(Date) sdf.parse(tem1)
		logi.logInfo("next bill date:"+next_bill_date)
		int billing_days = getDaysBetween(assigned_date,next_bill_date)

		logi.logInfo("plan billing cycle days:"+billing_days)
		String amt = db.executeQueryP2("Select NVL(SUM(debit),0) from ariacore.gl_detail where invoice_no= (Select min(invoice_no) from ariacore.gl where acct_no="+acct_no+ "and plan_no="+plan_no+") and proration_factor IS NOT NULL").toString()
		logi.logInfo("total billing cycle plan amt:"+amt)
		String used_days_plan_amt = ((amt.toDouble()/billing_days.toDouble())*used_days.toDouble()).toString()
		logi.logInfo("used_days_plan_amt:"+used_days_plan_amt)
		credit_amt= d.format((amt.toDouble() - used_days_plan_amt.toDouble()).toDouble().round(2)).toString()
	}
	
	else
		credit_amt=0
	logi.logInfo("credit_amt:"+credit_amt)
	
	return credit_amt
}


def md_get_request_plan_no(String testcaseId) {
	String rec_id = getValueFromRequest("create_acct_complete","//master_plan_no")
	return rec_id.toLowerCase().trim()
}

/**
 *
 * @param testcaseid
 * @return
 */
def md_update_inventory_stock_item_from_DB(String testcaseid)
{
	LinkedHashMap<String,String> updateInvStockItem = new LinkedHashMap<String, String>()
	String invItemNo = getValueFromResponse("update_inventory_item_stock", ExPathRpc.UPDATE_INVENTORY_ITEM_STOCK_OUTITEMNO1)
	//String client_no = getValueFromHttpRequestValue("update_inventory_item_stock", "client_no")
	String client_no = Constant.mycontext.expand('${Properties#Client_No}')
	logi.logInfo("Inventory Item No and Client number : " + invItemNo + " :: " + client_no)
	ResultSet rs = db.executeQuery("SELECT * from ariacore.INVENTORY_ITEMS WHERE ITEM_NO ='"+invItemNo+"' AND CLIENT_NO = "+client_no+"")
	while(rs.next())
	 {
		updateInvStockItem.put("Inventory Item No",rs.getString('ITEM_NO'))
		updateInvStockItem.put("Inventory Item SKU",rs.getString('CLIENT_SKU'))		
		updateInvStockItem.put("Inventory Item Client ID",rs.getString('CLIENT_ITEM_ID'))
		updateInvStockItem.put("Inventory Item Stock Level",rs.getString('STOCK_LEVEL'))
	 }
	 logi.logInfo("Hash map result is : " +updateInvStockItem)
	return updateInvStockItem.sort()
}

/**
 *
 * @param testcaseid
 * @return
 */
def md_update_inventory_stock_item_from_API(String testcaseid)
{
	LinkedHashMap<String,String> updateInvStockItem = new LinkedHashMap<String, String>()
	String invItemNo = getValueFromResponse("update_inventory_item_stock", ExPathRpc.UPDATE_INVENTORY_ITEM_STOCK_OUTITEMNO1)
	String client_no = getValueFromHttpRequestValue("update_inventory_item_stock", "client_no")	
		updateInvStockItem.put("Inventory Item No",getValueFromResponse("update_inventory_item_stock", ExPathRpc.UPDATE_INVENTORY_ITEM_STOCK_OUTITEMNO1))
		updateInvStockItem.put("Inventory Item SKU",getValueFromResponse("update_inventory_item_stock", ExPathRpc.UPDATE_INVENTORY_ITEM_STOCK_OUTCLIENTSKU1))		
		updateInvStockItem.put("Inventory Item Client ID",getValueFromResponse("update_inventory_item_stock", ExPathRpc.UPDATE_INVENTORY_ITEM_STOCK_OUTITEMNO1))
		updateInvStockItem.put("Inventory Item Stock Level",getValueFromResponse("update_inventory_item_stock", ExPathRpc.UPDATE_INVENTORY_ITEM_STOCK_NEW_STOCK_LEVEL1))
	 logi.logInfo("Hash map result is : " +updateInvStockItem)
	return updateInvStockItem.sort()
}

def md_get_all_acct_contracts_api(String testcaseId)
{
	
		logi.logInfo("Inside method md_get_invoice_details_api")
		// Getting the account number and client id
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		
		DecimalFormat df = new DecimalFormat("#.##");
		
		String contractNo  = getValueFromResponse('get_all_acct_contracts',ExPathRpcEnc.GET_ALL_ACCT_CONTRACTS_CONTRACT_NO)
		logi.logInfo("Contract No => "+contractNo)
		row.put("Contract No",contractNo.toString())
			
		String typeNo  = getValueFromResponse('get_all_acct_contracts',ExPathRpcEnc.GET_ALL_ACCT_CONTRACTS_CONTRACT_TYPE_NO)
		logi.logInfo("Type No => "+typeNo)
		row.put("Type No",typeNo.toString())
		
		String lengthMonths  = getValueFromResponse('get_all_acct_contracts',ExPathRpcEnc.GET_ALL_ACCT_CONTRACTS_CONTRACT_LENGTH_MONTHS)
		logi.logInfo("Length Months => "+lengthMonths)
		row.put("Length Months",lengthMonths.toString())

		String startDate  = getValueFromResponse('get_all_acct_contracts',ExPathRpcEnc.GET_ALL_ACCT_CONTRACTS_CONTRACT_START_DATE)
		logi.logInfo("Start Date => "+startDate.toString())
		row.put("Start Date",startDate.toString())
		
		String endDate  = getValueFromResponse('get_all_acct_contracts',ExPathRpcEnc.GET_ALL_ACCT_CONTRACTS_CONTRACT_END_DATE)
		logi.logInfo("End Date => "+endDate.toString())
		row.put("End Date",endDate.toString())
		
		String clientPlanId  = getValueFromResponse('get_all_acct_contracts',ExPathRpcEnc.GET_ALL_ACCT_CONTRACTS_CLIENT_PLAN_ID)
		logi.logInfo("Client Plan Id => "+clientPlanId.toString())
		row.put("Client Plan Id",clientPlanId.toString())
		
		logi.logInfo("md_get_all_acct_contracs => "+row)
		return row.sort()
	
}

def md_get_all_acct_contracts_DB(String testcaseId)
{
	
		logi.logInfo("Inside method md_get_all_acct_contracts_DB")
		// Getting the account number and client id
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		
		DecimalFormat df = new DecimalFormat("#.##");
		
		String contracNo_Q =  "SELECT contract_no from ariacore.acct_plan_contracts where client_no="+clientNo+" and acct_no="+accountNo
		String contractNo = db.executeQueryP2(contracNo_Q)
		logi.logInfo("Contract No  => "+contractNo.toString())
		row.put("Contract No",contractNo.toString())
		
		String typeNo_Q =  "SELECT TYPE_NO from ARIACORE.ACCT_PLAN_CONTRACTS where acct_no="+accountNo+ "AND CLIENT_NO="+clientNo
		String typeNo = db.executeQueryP2(typeNo_Q)
		logi.logInfo("Type No  => "+typeNo.toString())
		row.put("Type No",typeNo.toString())
		
		String lengthMonths_Q =  "SELECT NVL(to_char(contract_months),'NO NODE') from ariacore.acct_plan_contracts where client_no="+clientNo+" and acct_no= "+accountNo
		String lengthMonths = db.executeQueryP2(lengthMonths_Q)
		logi.logInfo("Length Months  => "+lengthMonths.toString())
		row.put("Length Months",lengthMonths.toString())
		
		String startDate_Q =  "SELECT to_number(to_char(start_date,'yyyy')) || '-' || to_char(start_date,'mm')|| '-' ||to_char(start_date,'dd') as START_DATE from ariacore.acct_plan_contracts where client_no="+clientNo+" and acct_no="+accountNo
		String startDate = db.executeQueryP2(startDate_Q)
		logi.logInfo("Start Date  => "+startDate.toString())
		row.put("Start Date",startDate.toString())
		
		String endDate_Q =  "SELECT to_number(to_char(end_date,'yyyy')) || '-' || to_char(end_date,'mm')|| '-' ||to_char(end_date,'dd') as END_DATE from ariacore.acct_plan_contracts where client_no= "+clientNo+ " and acct_no= "+accountNo
		String endDate = db.executeQueryP2(endDate_Q)
		logi.logInfo("End Date  => "+endDate.toString())
		row.put("End Date",endDate.toString())
		
		String clientPlanId_Q =  "SELECT PLAN_NAME FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO= "+accountNo+" and client_no = "+clientNo
		String clientPlanId = db.executeQueryP2(clientPlanId_Q)
		logi.logInfo("Client Plan Id  => "+endDate.toString())
		row.put("Client Plan Id",clientPlanId.toString())
		
		

		logi.logInfo("md_get_acct_details_all_DB => "+row)
		return row.sort()
							
}

def md_get_acct_plan_contract_api(String testcaseId)
{
	
		logi.logInfo("Inside method md_get_acct_plan_contract_api")
		// Getting the account number and client id
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		
		DecimalFormat df = new DecimalFormat("#.##");
		
		String contractNo  = getValueFromResponse('get_acct_plan_contract',ExPathRpcEnc.GET_ACCT_PLAN_CONTRACT_CONTRACT_NO1)
		logi.logInfo("Contract No => "+contractNo)
		row.put("Contract No",contractNo.toString())
			
		String typeNo  = getValueFromResponse('get_acct_plan_contract',ExPathRpcEnc.GET_ACCT_PLAN_CONTRACT_TYPE_NO1)
		logi.logInfo("Type No => "+typeNo)
		row.put("Type No",typeNo.toString())
		
		String lengthMonths  = getValueFromResponse('get_acct_plan_contract',ExPathRpcEnc.GET_ACCT_PLAN_CONTRACT_LENGTH_MONTHS1)
		logi.logInfo("Length Months => "+lengthMonths)
		row.put("Length Months",lengthMonths.toString())

		String startDate  = getValueFromResponse('get_acct_plan_contract',ExPathRpcEnc.GET_ACCT_PLAN_CONTRACT_START_DATE)
		logi.logInfo("Start Date => "+startDate.toString())
		row.put("Start Date",startDate.toString())
		
		String endDate  = getValueFromResponse('get_acct_plan_contract',ExPathRpcEnc.GET_ACCT_PLAN_CONTRACT_END_DATE)
		logi.logInfo("End Date => "+endDate.toString())
		row.put("End Date",endDate.toString())
		
		String createDate  = getValueFromResponse('get_acct_plan_contract',ExPathRpcEnc.GET_ACCT_PLAN_CONTRACT_CREATE_DATE)
		logi.logInfo("Create Date => "+createDate.toString())
		row.put("Create Date",createDate.toString())
		
		String updateDate  = getValueFromResponse('get_acct_plan_contract',ExPathRpcEnc.GET_ACCT_PLAN_CONTRACT_UPDATE_DATE)
		logi.logInfo("Update Date => "+updateDate.toString())
		row.put("Update Date",updateDate.toString())
		
		String createComments  = getValueFromResponse('get_acct_plan_contract',ExPathRpcEnc.GET_ACCT_PLAN_CONTRACT_CREATE_COMMENTS1)
		logi.logInfo("Create Comments => "+createComments.toString())
		row.put("Create Comments",createComments.toString())
		
		
		logi.logInfo("md_get_acct_plan_contract_api => "+row)
		return row.sort()
	
}

def md_get_acct_plan_contract_DB(String testcaseId)
{
	
		logi.logInfo("Inside method md_get_all_acct_contracts_DB")
		// Getting the account number and client id
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		
		DecimalFormat df = new DecimalFormat("#.##");
		
		String contracNo_Q =  "SELECT contract_no from ariacore.acct_plan_contracts where client_no="+clientNo+" and acct_no="+accountNo
		String contractNo = db.executeQueryP2(contracNo_Q)
		logi.logInfo("Contract No  => "+contractNo.toString())
		row.put("Contract No",contractNo.toString())
		
		String typeNo_Q =  "SELECT TYPE_NO from ARIACORE.ACCT_PLAN_CONTRACTS where acct_no="+accountNo+ "AND CLIENT_NO="+clientNo
		String typeNo = db.executeQueryP2(typeNo_Q)
		logi.logInfo("Type No  => "+typeNo.toString())
		row.put("Type No",typeNo.toString())
		
		String lengthMonths_Q =  "SELECT NVL(to_char(contract_months),'NO NODE') from ariacore.acct_plan_contracts where client_no="+clientNo+" and acct_no= "+accountNo
		String lengthMonths = db.executeQueryP2(lengthMonths_Q)
		logi.logInfo("Length Months  => "+lengthMonths.toString())
		row.put("Length Months",lengthMonths.toString())
		
		String startDate_Q =  "SELECT to_number(to_char(start_date,'yyyy')) || '-' || to_char(start_date,'mm')|| '-' ||to_char(start_date,'dd') as START_DATE from ariacore.acct_plan_contracts where client_no="+clientNo+" and acct_no="+accountNo
		String startDate = db.executeQueryP2(startDate_Q)
		logi.logInfo("Start Date  => "+startDate.toString())
		row.put("Start Date",startDate.toString())
		
		String endDate_Q =  "SELECT to_number(to_char(end_date,'yyyy')) || '-' || to_char(end_date,'mm')|| '-' ||to_char(end_date,'dd') as END_DATE from ariacore.acct_plan_contracts where client_no= "+clientNo+ " and acct_no= "+accountNo
		String endDate = db.executeQueryP2(endDate_Q)
		logi.logInfo("End Date  => "+endDate.toString())
		row.put("End Date",endDate.toString())
		
		String createDate_Q =  "SELECT to_number(to_char(create_date,'yyyy')) || '-' || to_char(create_date,'mm')|| '-' ||to_char(create_date,'dd') as CREATE_DATE from ariacore.acct_plan_contracts where client_no="+clientNo+" and acct_no="+accountNo
		String createDate = db.executeQueryP2(createDate_Q)
		logi.logInfo("Create Date  => "+createDate.toString())
		row.put("Create Date",createDate.toString())
		
		String updateDate_Q =  "SELECT to_number(to_char(update_date,'yyyy')) || '-' || to_char(update_date,'mm')|| '-' ||to_char(update_date,'dd') as UPDATE_DATE from ariacore.acct_plan_contracts where client_no= "+clientNo+ " and acct_no= "+accountNo
		String updateDate = db.executeQueryP2(updateDate_Q)
		logi.logInfo("Update Date  => "+updateDate.toString())
		row.put("Update Date",updateDate.toString())
		
		String createComments_Q =  "SELECT CREATE_COMMENTS from ariacore.acct_plan_contracts where client_no= "+clientNo+ " and acct_no= "+accountNo
		String createComments = db.executeQueryP2(createComments_Q)
		logi.logInfo("Create Comments  => "+createComments.toString())
		row.put("Create Comments",createComments.toString())

		logi.logInfo("md_get_acct_details_all_DB => "+row)
		return row.sort()
							
}

def md_get_acct_plans_api(String testcaseId)
{
	
		logi.logInfo("Inside method md_get_acct_plans_api")
		// Getting the account number and client id
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		
		DecimalFormat df = new DecimalFormat("#.##");
		
		String planNo  = getValueFromResponse('get_acct_plans',ExPathRpcEnc.GET_ACCT_PLANS_PLANNO1)
		logi.logInfo("Plan No => "+planNo)
		row.put("Plan No",planNo.toString())
			
		String planName  = getValueFromResponse('get_acct_plans',ExPathRpcEnc.GET_ACCT_PLANS_PLANNAME1)
		logi.logInfo("Plan Name => "+planName)
		row.put("Plan Name",planName.toString())
		
		String planDate  = getValueFromResponse('get_acct_plans',ExPathRpcEnc.GET_ACCT_PLANS_PLANDATE)
		logi.logInfo("Plan Date =>" +planDate.toString())
		row.put("Plan Date",planDate.toString())

		String lastBillDate  = getValueFromResponse('get_acct_plans',ExPathRpcEnc.GET_ACCT_PLANS_LAST_BILL_DATE)
		logi.logInfo("Last Bill Date => "+lastBillDate.toString())
		row.put("Last Bill Date",lastBillDate.toString())
		
		String nextBillDate  = getValueFromResponse('get_acct_plans',ExPathRpcEnc.GET_ACCT_PLANS_NEXT_BILL_DATE)
		logi.logInfo("Next Bill Date => "+nextBillDate.toString())
		row.put("Next Bill Date",nextBillDate.toString())
		
		String billThruDate  = getValueFromResponse('get_acct_plans',ExPathRpcEnc.GET_ACCT_PLANS_BILL_THRU_DATE)
		logi.logInfo("Bill Thru Date => "+billThruDate.toString())
		row.put("Bill Thru Date",billThruDate.toString())
		
		String currencyCode  = getValueFromResponse('get_acct_plans',ExPathRpcEnc.GET_ACCT_PLANS_CURRENCY_CODE)
		logi.logInfo("Currency Code => "+currencyCode.toString())
		row.put("Currency Code",currencyCode.toString())
		
		String billDay  = getValueFromResponse('get_acct_plans',ExPathRpcEnc.GET_ACCT_PLANS_BILL_DAY)
		logi.logInfo("Bill Day => "+billDay.toString())
		row.put("Bill Day",billDay.toString())
		
		String rateScheduleNo  = getValueFromResponse('get_acct_plans',ExPathRpcEnc.GET_ACCT_PLANS_RATE_SCHEDULE_NO)
		logi.logInfo("Rate Schedule No => "+rateScheduleNo.toString())
		row.put("Rate Schedule No",rateScheduleNo.toString())
		
		
		logi.logInfo("md_get_acct_plans_api => "+row)
		return row.sort()
	
}

def md_get_acct_plans_DB(String testcaseId)
{
	
		logi.logInfo("Inside method md_get_acct_plans_DB")
		// Getting the account number and client id
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		
		DecimalFormat df = new DecimalFormat("#.##");
		
		String planNo_Q =  "SELECT PLAN_NO FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=  "+invoice_no+ "and client_no = "+clientNo
		String planNo = db.executeQueryP2(planNo_Q)
		logi.logInfo("Plan No  => "+planNo.toString())
		row.put("Plan No",planNo.toString())
		
		String planName_Q =  "SELECT PLAN_NAME FROM ARIACORE.CLIENT_PLAN WHERE PLAN_NO=  "+planNo+ "and client_no = "+clientNo
		String planName = db.executeQueryP2(planName_Q)
		logi.logInfo("Plan Name  => "+planName.toString())
		row.put("Plan Name",planName.toString())
		
		String planDate_Q =  "SELECT to_number(to_char(plan_date,'yyyy')) || '-' || to_char(plan_date,'mm')|| '-' ||to_char(plan_date,'dd') as PLAN_DATE from ariacore.acct_details where client_no="+clientNo+" and acct_no="+accountNo
		String planDate = db.executeQueryP2(planDate_Q)
		logi.logInfo("Plan Date  => "+planDate.toString())
		row.put("Plan Date",planDate.toString())
		
		String lastBillDate_Q =  "SELECT to_number(to_char(last_bill_date,'yyyy')) || '-' || to_char(last_bill_date,'mm')|| '-' ||to_char(last_bill_date,'dd') as LAST_BILL_DATE from ariacore.acct_details where client_no="+clientNo+" and acct_no="+accountNo
		String lastBillDate = db.executeQueryP2(lastBillDate_Q)
		logi.logInfo("Last Bill Date  => "+lastBillDate.toString())
		row.put("Last Bill Date",lastBillDate.toString())
		
		String nextBillDate_Q =  "SELECT to_number(to_char(next_bill_date,'yyyy')) || '-' || to_char(next_bill_date,'mm')|| '-' ||to_char(next_bill_date,'dd') as NEXT_BILL_DATE from ariacore.acct_details where client_no="+clientNo+" and acct_no="+accountNo
		String nextBillDate = db.executeQueryP2(nextBillDate_Q)
		logi.logInfo("Next Bill Date  => "+nextBillDate.toString())
		row.put("Next Bill Date",nextBillDate.toString())
		
		String billThruDate_Q =  "SELECT to_number(to_char(last_bill_thru_date,'yyyy')) || '-' || to_char(last_bill_thru_date,'mm')|| '-' ||to_char(last_bill_thru_date,'dd') as LAST_BILL_THRU_DATE from ariacore.acct_details where client_no="+clientNo+" and acct_no="+accountNo
		String billThruDate = db.executeQueryP2(billThruDate_Q)
		logi.logInfo("Bill Thru Date  => "+billThruDate.toString())
		row.put("Bill Thru Date",billThruDate.toString())
		
		String currencyCode_Q =  "SELECT currency_cd from ariacore.acct_details where client_no= "+clientNo+ " and acct_no= "+accountNo
		String currencyCode = db.executeQueryP2(currencyCode_Q)
		logi.logInfo("Currency Code  => "+currencyCode.toString())
		row.put("Currency Code",currencyCode.toString())
		
		String billDay_Q =  "SELECT bill_day from ariacore.acct_details where client_no= "+clientNo+ " and acct_no= "+accountNo
		String billDay = db.executeQueryP2(billDay_Q)
		logi.logInfo("Bill Day  => "+billDay.toString())
		row.put("Bill Day",billDay.toString())
		
		String rateSchedule_Q =  "SELECT RATE_SCHEDULE_NO FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = "+accountNo+ "AND RATE_SEQ_NO=1 AND CLIENT_NO= "+clientNo
		String rateScheduleNo = db.executeQueryP2(rateSchedule_Q)
		logi.logInfo("Rate Schedule No  => "+rateScheduleNo.toString())
		row.put("Rate Schedule No",rateScheduleNo.toString())


		logi.logInfo("md_get_acct_plans_DB => "+row)
		return row.sort()
							
}

def md_get_all_acct_active_contract_api(String testcaseId)
{
	
		logi.logInfo("Inside md_get_all_acct_active_contract_api")
		// Getting the account number and client id
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		String contractNo1  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_CONTRACT_NO)
		logi.logInfo("The value of ContractNo111111: "+contractNo1)
		if(contractNo1=='NoVal')
		{
			logi.logInfo("ContractNo1=NoVal ifffffffffff ")
			contractNo1="No Node"
			logi.logInfo("ContractNo1: "+contractNo1)
		}
		logi.logInfo("Contract No1 => "+contractNo1)
		row.put("Contract No1",contractNo1.toString())
		
		
		String planNo1=getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_CLIENT_PLAN_NO1)
		logi.logInfo("The planNo of ContractNo111111: "+planNo1)
		if(planNo1=='NoVal')
		{
			logi.logInfo("planNo1=NoVal ifffffffff ")
			planNo1="No Node"
			logi.logInfo("planNo1: "+planNo1)
		}
		logi.logInfo("Plan No1 => "+planNo1)
		row.put("Plan No1",planNo1.toString())
		
		String statusCd1  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_STATUS_CD)
		logi.logInfo("The status cd of ContractNo111111: "+statusCd1)
		if(statusCd1=='NoVal')
		{
			logi.logInfo("statusCd1=NoVal ifffffffff ")
			statusCd1="No Node"
			logi.logInfo("statusCd1: "+statusCd1)
		}
		logi.logInfo("Status cd1 => "+statusCd1)
		row.put("Status cd1",statusCd1.toString())
		
		
		String contractNo2  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_CONTRACT_NO2)
		logi.logInfo("The value of ContractNo22222222: "+contractNo2)
		if(contractNo2=='NoVal')
		{
			logi.logInfo("ContractNo2=NoVal ifffffffff: ")
			contractNo2="No Node"
			logi.logInfo("ContractNo2: "+contractNo2)
		}
		logi.logInfo("Contract No2 => "+contractNo2)
		row.put("Contract No2",contractNo2.toString())
		
		String planNo2=getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_CLIENT_PLAN_NO2)
		logi.logInfo("The planNo of ContractNo222222: "+planNo2)
		if(planNo2=='NoVal')
		{
			logi.logInfo("planNo2=NoVal ifffffffff ")
			planNo2="No Node"
			logi.logInfo("planNo2: "+planNo2)
		}
		logi.logInfo("Plan No2 => "+planNo2)
		row.put("Plan No2",planNo2.toString())
		
		String statusCd2  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_STATUS_CD2)
		logi.logInfo("The status cd of ContractNo22222222: "+statusCd2)
		if(statusCd2=='NoVal')
		{
			logi.logInfo("statusCd2=NoVal iffffffffff "+statusCd2)
			statusCd2="No Node"
			logi.logInfo("statusCd2: "+statusCd2)
		}
		logi.logInfo("Status cd2 => "+statusCd2)
		row.put("Status cd2",statusCd2.toString())
		
		
		
		logi.logInfo("md_get_all_acct_active_contract_api => "+row)
		return row.sort()

}


def md_get_all_acct_active_contract_DB(String testcaseId)
{
	
		logi.logInfo("Inside method md_get_all_acct_active_contract_DB")
		// Getting the account number and client id
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		ConnectDB db = new ConnectDB()
		String contractNo1_Q="SELECT contract_no from (SELECT contract_no,row_number() over (order by contract_no) as seqnum from ariacore.acct_plan_contracts where acct_no="+accountNo+" and status_cd=1) where seqnum=1"
		String contractNo1 = db.executeQueryP2(contractNo1_Q)
		logi.logInfo ("ContractNo11111111111111111111 :"+contractNo1)
		if(contractNo1==null)
		{
			logi.logInfo ("ContractNo111111 inside iffffffffff :")
			
			contractNo1="No Node"
			logi.logInfo ("ContractNo1 :"+contractNo1)
		}
		logi.logInfo("Contract No1  => "+contractNo1.toString())
		row.put("Contract No1",contractNo1.toString())
		
		String PlanNo1_Q="SELECT plan_no from (select plan_no,row_number() over (order by contract_no) as seqnum from ariacore.acct_plan_contracts where acct_no="+accountNo+") where seqnum=1"
		String PlanNo1 = db.executeQueryP2(PlanNo1_Q)
		logi.logInfo ("Plan no contractNo111111111 :"+PlanNo1)
		if(PlanNo1==null)
		{
			logi.logInfo ("PlanNo1111111111 inside ifffffffffff :")
			PlanNo1="No Node"
			logi.logInfo ("PlanNo1 :"+PlanNo1)
		}
		logi.logInfo("Plan No1  => "+PlanNo1.toString())
		row.put("Plan No1",PlanNo1.toString())
		
		String status_cd1_Q="SELECT status_cd from (SELECT status_cd,row_number() over (order by contract_no) as seqnum from ariacore.acct_plan_contracts where acct_no="+accountNo+") where seqnum=1"
		String status_cd1 = db.executeQueryP2(status_cd1_Q)
		logi.logInfo ("Status code contractNo111111111 :"+status_cd1)
		if(status_cd1==null)
		{
			logi.logInfo ("status_cd1111111111 inside ifffffffffff :")
			status_cd1="No Node"
			logi.logInfo ("status_cd1 :"+status_cd1)
		}
		logi.logInfo("Status cd1  => "+status_cd1.toString())
		row.put("Status cd1",status_cd1.toString())
		
		String contractNo2_Q="SELECT contract_no from (SELECT contract_no,row_number() over (order by contract_no) as seqnum from ariacore.acct_plan_contracts where acct_no="+accountNo+" and status_cd=1) where seqnum=2"
		String contractNo2 = db.executeQueryP2(contractNo2_Q)
		logi.logInfo ("ContractNo22222222222 :"+contractNo2)
		if(contractNo2==null)
		{
			logi.logInfo ("ContractNo2 inside ifffffffff :")
			contractNo2="No Node"
			logi.logInfo ("ContractNo2 :"+contractNo2)
		}
		logi.logInfo("Contract No2  => "+contractNo2.toString())
		row.put("Contract No2",contractNo2.toString())
		
		String PlanNo2_Q="SELECT plan_no from (select plan_no,row_number() over (order by contract_no) as seqnum from ariacore.acct_plan_contracts where acct_no="+accountNo+") where seqnum=2"
		String PlanNo2 = db.executeQueryP2(PlanNo2_Q)
		logi.logInfo ("Plan no contractNo2222222222222 :"+PlanNo2)
		if(PlanNo2==null)
		{
			logi.logInfo ("PlanNo222222222 inside ifffffffffff :")
			PlanNo2="No Node"
			logi.logInfo ("PlanNo2 :"+PlanNo1)
		}
		logi.logInfo("Plan No2  => "+PlanNo2.toString())
		row.put("Plan No2",PlanNo2.toString())
		
		
		String status_cd2_Q="SELECT status_cd from (SELECT status_cd,row_number() over (order by contract_no) as seqnum from ariacore.acct_plan_contracts where acct_no="+accountNo+") where seqnum=2"
		String status_cd2 = db.executeQueryP2(status_cd2_Q)
		logi.logInfo ("Status_code of contractNo222222222222222 :"+status_cd2)
		if(status_cd2==null)
		{
			logi.logInfo ("status_cd2 inside iffffffffff :"+status_cd2)
			status_cd2="No Node"
			logi.logInfo ("status_cd2 :"+status_cd2)
		}
		logi.logInfo("Status cd2  => "+status_cd2.toString())
		row.put("Status cd2",status_cd2.toString())
		
		logi.logInfo("md_get_all_acct_active_contract_DB => "+row)
		return row.sort()
		
		
}

def md_get_all_acct_active_contract_multiplan_DB1(String testcaseId)
{
	
		logi.logInfo("Inside method md_get_all_acct_active_contract_multiplan_DB")
		// Getting the account number and client id
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		ConnectDB db = new ConnectDB()
		String contractNo1_Q="SELECT contract_no from (SELECT contract_no,row_number() over (order by contract_no) as seqnum from ARIACORE.ACCT_MULTI_PLAN_CNTRCTS_PLANS where acct_no="+accountNo+") where seqnum=1"
		String contractNo1 = db.executeQueryP2(contractNo1_Q)
		logi.logInfo ("ContractNo11111111111111111111 :"+contractNo1)
		if(contractNo1==null)
		{
			logi.logInfo ("ContractNo111111 inside iffffffffff :")
			
			contractNo1="No Node"
			logi.logInfo ("ContractNo1 :"+contractNo1)
		}
		logi.logInfo("Contract No1  => "+contractNo1.toString())
		row.put("Contract No1",contractNo1.toString())
		
		String PlanNo1_Q="SELECT plan_no from (select plan_no,row_number() over (order by plan_no) as seqnum from ariacore.ACCT_MULTI_PLAN_CNTRCTS_PLANS where acct_no="+accountNo+") where seqnum=1"
		String PlanNo1 = db.executeQueryP2(PlanNo1_Q)
		logi.logInfo ("Plan no contractNo111111111 :"+PlanNo1)
		if(PlanNo1==null)
		{
			logi.logInfo ("PlanNo1111111111 inside ifffffffffff :")
			PlanNo1="No Node"
			logi.logInfo ("PlanNo1 :"+PlanNo1)
		}
		logi.logInfo("Plan No1  => "+PlanNo1.toString())
		row.put("Plan No1",PlanNo1.toString())
		
		
		String status_cd1_Q="SELECT status_cd from ARIACORE.ACCT_MULTI_PLAN_CONTRACTS where acct_no="+accountNo
		String status_cd1 = db.executeQueryP2(status_cd1_Q)
		logi.logInfo ("Status code contractNo111111111 :"+status_cd1)
		if(status_cd1==null)
		{
			logi.logInfo ("status_cd1111111111 inside ifffffffffff :")
			status_cd1="No Node"
			logi.logInfo ("status_cd1 :"+status_cd1)
		}
		logi.logInfo("Status cd1  => "+status_cd1.toString())
		row.put("Status cd1",status_cd1.toString())
		
		
		
		String contractNo2_Q="SELECT contract_no from (SELECT contract_no,row_number() over (order by contract_no) as seqnum from ARIACORE.ACCT_MULTI_PLAN_CNTRCTS_PLANS where acct_no="+accountNo+") where seqnum=2"
		String contractNo2 = db.executeQueryP2(contractNo2_Q)
		logi.logInfo ("ContractNo22222222222 :"+contractNo2)
		if(contractNo2==null)
		{
			logi.logInfo ("ContractNo2 inside ifffffffff :")
			contractNo2="No Node"
			logi.logInfo ("ContractNo2 :"+contractNo2)
		}
		logi.logInfo("Contract No2  => "+contractNo2.toString())
		row.put("Contract No2",contractNo2.toString())
		
		String PlanNo2_Q="SELECT plan_no from (select plan_no,row_number() over (order by contract_no) as seqnum from ariacore.ACCT_MULTI_PLAN_CNTRCTS_PLANS where acct_no="+accountNo+") where seqnum=2"
		String PlanNo2 = db.executeQueryP2(PlanNo2_Q)
		logi.logInfo ("Plan no contractNo2222222222222 :"+PlanNo2)
		if(PlanNo2==null)
		{
			logi.logInfo ("PlanNo222222222 inside ifffffffffff :")
			PlanNo2="No Node"
			logi.logInfo ("PlanNo2 :"+PlanNo1)
		}
		logi.logInfo("Plan No2  => "+PlanNo2.toString())
		row.put("Plan No2",PlanNo2.toString())
		
		String status_cd2_Q="SELECT status_cd from ARIACORE.ACCT_MULTI_PLAN_CONTRACTS where acct_no="+accountNo
		String status_cd2 = db.executeQueryP2(status_cd2_Q)
		logi.logInfo ("Status_code of contractNo222222222222222 :"+status_cd2)
		if(status_cd2==null)
		{
			logi.logInfo ("status_cd2 inside iffffffffff :"+status_cd2)
			status_cd2="No Node"
			logi.logInfo ("status_cd2 :"+status_cd2)
		}
		logi.logInfo("Status cd2  => "+status_cd2.toString())
		row.put("Status cd2",status_cd2.toString())
		
		
		String contractNo3_Q="SELECT contract_no from (SELECT contract_no,row_number() over (order by contract_no) as seqnum from ARIACORE.ACCT_MULTI_PLAN_CNTRCTS_PLANS where acct_no="+accountNo+") where seqnum=3"
		String contractNo3 = db.executeQueryP2(contractNo3_Q)
		logi.logInfo ("ContractNo333333 :"+contractNo3)
		if(contractNo3==null)
		{
			logi.logInfo ("ContractNo333333 inside ifffffffff :")
			contractNo3="No Node"
			logi.logInfo ("ContractNo3 :"+contractNo3)
		}
		logi.logInfo("Contract No3  => "+contractNo3.toString())
		row.put("Contract No3",contractNo3.toString())
		
		String PlanNo3_Q="SELECT plan_no from (select plan_no,row_number() over (order by contract_no) as seqnum from ariacore.ACCT_MULTI_PLAN_CNTRCTS_PLANS where acct_no="+accountNo+") where seqnum=3"
		String PlanNo3 = db.executeQueryP2(PlanNo3_Q)
		logi.logInfo ("Plan no contractNo333333333 :"+PlanNo3)
		if(PlanNo3==null)
		{
			logi.logInfo ("PlanNo33333 inside ifffffffffff :")
			PlanNo3="No Node"
			logi.logInfo ("PlanNo3 :"+PlanNo3)
		}
		logi.logInfo("Plan No3  => "+PlanNo3.toString())
		row.put("Plan No3",PlanNo3.toString())
		
		String status_cd3_Q="SELECT status_cd from ARIACORE.ACCT_MULTI_PLAN_CONTRACTS where acct_no="+accountNo
		String status_cd3 = db.executeQueryP2(status_cd3_Q)
		logi.logInfo ("Status_code of contractNo33333333333333 :"+status_cd3)
		if(status_cd3==null)
		{
			logi.logInfo ("status_cd33 inside iffffffffff :"+status_cd3)
			status_cd3="No Node"
			logi.logInfo ("status_cd3 :"+status_cd3)
		}
		logi.logInfo("Status cd3  => "+status_cd3.toString())
		row.put("Status cd3",status_cd3.toString())
		
		
		
		logi.logInfo("md_get_all_acct_active_contract_DB => "+row)
		return row.sort()
		
		
}

def md_get_all_acct_active_contract_multiplan_DB(String testcaseId)
{
	
		logi.logInfo("Inside method md_get_all_acct_active_contract_multiplan_DB")
		// Getting the account number and client id
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		ConnectDB db = new ConnectDB()
		String contractNo1_Q="SELECT contract_no from (SELECT contract_no,row_number() over (order by contract_no) as seqnum from ARIACORE.ACCT_MULTI_PLAN_CONTRACTS where acct_no="+accountNo+" and status_cd=1) where seqnum=1"
		String contractNo1 = db.executeQueryP2(contractNo1_Q)
		logi.logInfo ("ContractNo11111111111111111111 :"+contractNo1)
		if(contractNo1==null)
		{
			logi.logInfo ("ContractNo111111 inside iffffffffff :")
			
			contractNo1="No Node"
			logi.logInfo ("ContractNo1 :"+contractNo1)
		}
		logi.logInfo("Contract No1  => "+contractNo1.toString())
		row.put("Contract No1",contractNo1.toString())
		
		String PlanNo1_Q="SELECT plan_no from (select plan_no,row_number() over (order by plan_no) as seqnum from ariacore.ACCT_MULTI_PLAN_CNTRCTS_PLANS where acct_no="+accountNo+") where seqnum=1"
		String PlanNo1 = db.executeQueryP2(PlanNo1_Q)
		logi.logInfo ("Plan no contractNo111111111 :"+PlanNo1)
		if(PlanNo1==null)
		{
			logi.logInfo ("PlanNo1111111111 inside ifffffffffff :")
			PlanNo1="No Node"
			logi.logInfo ("PlanNo1 :"+PlanNo1)
		}
		logi.logInfo("Plan No1  => "+PlanNo1.toString())
		row.put("Plan No1",PlanNo1.toString())
		
		
		String status_cd1_Q="SELECT status_cd from ARIACORE.ACCT_MULTI_PLAN_CONTRACTS where acct_no="+accountNo
		String status_cd1 = db.executeQueryP2(status_cd1_Q)
		logi.logInfo ("Status code contractNo111111111 :"+status_cd1)
		if(status_cd1==null)
		{
			logi.logInfo ("status_cd1111111111 inside ifffffffffff :")
			status_cd1="No Node"
			logi.logInfo ("status_cd1 :"+status_cd1)
		}
		logi.logInfo("Status cd1  => "+status_cd1.toString())
		row.put("Status cd1",status_cd1.toString())
		
		
		
		String contractNo2_Q="SELECT contract_no from (SELECT contract_no,row_number() over (order by contract_no) as seqnum from ARIACORE.ACCT_MULTI_PLAN_CONTRACTS where acct_no="+accountNo+" and status_cd=1) where seqnum=1"
		String contractNo2 = db.executeQueryP2(contractNo2_Q)
		logi.logInfo ("ContractNo22222222222 :"+contractNo2)
		if(contractNo2==null)
		{
			logi.logInfo ("ContractNo2 inside ifffffffff :")
			contractNo2="No Node"
			logi.logInfo ("ContractNo2 :"+contractNo2)
		}
		logi.logInfo("Contract No2  => "+contractNo2.toString())
		row.put("Contract No2",contractNo2.toString())
		
		String PlanNo2_Q="SELECT plan_no from (select plan_no,row_number() over (order by contract_no) as seqnum from ariacore.ACCT_MULTI_PLAN_CNTRCTS_PLANS where acct_no="+accountNo+") where seqnum=2"
		String PlanNo2 = db.executeQueryP2(PlanNo2_Q)
		logi.logInfo ("Plan no contractNo2222222222222 :"+PlanNo2)
		if(PlanNo2==null)
		{
			logi.logInfo ("PlanNo222222222 inside ifffffffffff :")
			PlanNo2="No Node"
			logi.logInfo ("PlanNo2 :"+PlanNo1)
		}
		logi.logInfo("Plan No2  => "+PlanNo2.toString())
		row.put("Plan No2",PlanNo2.toString())
		
		String status_cd2_Q="SELECT status_cd from ARIACORE.ACCT_MULTI_PLAN_CONTRACTS where acct_no="+accountNo
		String status_cd2 = db.executeQueryP2(status_cd2_Q)
		logi.logInfo ("Status_code of contractNo222222222222222 :"+status_cd2)
		if(status_cd2==null)
		{
			logi.logInfo ("status_cd2 inside iffffffffff :"+status_cd2)
			status_cd2="No Node"
			logi.logInfo ("status_cd2 :"+status_cd2)
		}
		logi.logInfo("Status cd2  => "+status_cd2.toString())
		row.put("Status cd2",status_cd2.toString())
		
		
		String contractNo3_Q="SELECT contract_no from (SELECT contract_no,row_number() over (order by contract_no) as seqnum from ARIACORE.ACCT_MULTI_PLAN_CONTRACTS where acct_no="+accountNo+" and status_cd=1) where seqnum=1"
		String contractNo3 = db.executeQueryP2(contractNo3_Q)
		logi.logInfo ("ContractNo333333 :"+contractNo3)
		if(contractNo3==null)
		{
			logi.logInfo ("ContractNo333333 inside ifffffffff :")
			contractNo3="No Node"
			logi.logInfo ("ContractNo3 :"+contractNo3)
		}
		logi.logInfo("Contract No3  => "+contractNo3.toString())
		row.put("Contract No3",contractNo3.toString())
		
		String PlanNo3_Q="SELECT plan_no from (select plan_no,row_number() over (order by contract_no) as seqnum from ariacore.ACCT_MULTI_PLAN_CNTRCTS_PLANS where acct_no="+accountNo+") where seqnum=3"
		String PlanNo3 = db.executeQueryP2(PlanNo3_Q)
		logi.logInfo ("Plan no contractNo333333333 :"+PlanNo3)
		if(PlanNo3==null)
		{
			logi.logInfo ("PlanNo33333 inside ifffffffffff :")
			PlanNo3="No Node"
			logi.logInfo ("PlanNo3 :"+PlanNo3)
		}
		logi.logInfo("Plan No3  => "+PlanNo3.toString())
		row.put("Plan No3",PlanNo3.toString())
		
		String status_cd3_Q="SELECT status_cd from ARIACORE.ACCT_MULTI_PLAN_CONTRACTS where acct_no="+accountNo
		String status_cd3 = db.executeQueryP2(status_cd3_Q)
		logi.logInfo ("Status_code of contractNo33333333333333 :"+status_cd3)
		if(status_cd3==null)
		{
			logi.logInfo ("status_cd33 inside iffffffffff :"+status_cd3)
			status_cd3="No Node"
			logi.logInfo ("status_cd3 :"+status_cd3)
		}
		logi.logInfo("Status cd3  => "+status_cd3.toString())
		row.put("Status cd3",status_cd3.toString())
		
		
		
		logi.logInfo("md_get_all_acct_active_contract_DB => "+row.sort())
		return row.sort()
		
		
}

def md_get_all_acct_active_contract_multiplan_api(String testcaseId)
{
	
		logi.logInfo("Inside md_get_all_acct_active_contract_api")
		// Getting the account number and client id
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		String contractNo1  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_CONTRACT_NO)
		logi.logInfo("The value of ContractNo111111: "+contractNo1)
		if(contractNo1=='NoVal')
		{
			logi.logInfo("ContractNo1=NoVal ifffffffffff ")
			contractNo1="No Node"
			logi.logInfo("ContractNo1: "+contractNo1)
		}
		logi.logInfo("Contract No1 => "+contractNo1)
		row.put("Contract No1",contractNo1.toString())
		
		String planNo1=getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_CLIENT_PLAN_NO1)
		logi.logInfo("The planNo of ContractNo111111: "+planNo1)
		if(planNo1=='NoVal')
		{
			logi.logInfo("planNo1=NoVal ifffffffff ")
			planNo1="No Node"
			logi.logInfo("planNo1: "+planNo1)
		}
		logi.logInfo("Plan No1 => "+planNo1)
		row.put("Plan No1",planNo1.toString())
		
		String statusCd1  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_STATUS_CD)
		logi.logInfo("The status cd of ContractNo111111: "+statusCd1)
		if(statusCd1=='NoVal')
		{
			logi.logInfo("statusCd1=NoVal ifffffffff ")
			statusCd1="No Node"
			logi.logInfo("statusCd1: "+statusCd1)
		}
		logi.logInfo("Status cd1 => "+statusCd1)
		row.put("Status cd1",statusCd1.toString())
		
		
		
		String contractNo2  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_CONTRACT_NO2)
		logi.logInfo("The value of ContractNo22222222: "+contractNo2)
		if(contractNo2=='NoVal')
		{
			logi.logInfo("ContractNo2=NoVal ifffffffff: ")
			contractNo2="No Node"
			logi.logInfo("ContractNo2: "+contractNo2)
		}
		logi.logInfo("Contract No2 => "+contractNo2)
		row.put("Contract No2",contractNo2.toString())
		
		String planNo2=getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_CLIENT_PLAN_NO3)
		logi.logInfo("The planNo of ContractNo222222: "+planNo2)
		if(planNo2=='NoVal')
		{
			logi.logInfo("planNo2=NoVal ifffffffff ")
			planNo2="No Node"
			logi.logInfo("planNo2: "+planNo2)
		}
		logi.logInfo("Plan No2 => "+planNo2)
		row.put("Plan No2",planNo2.toString())
		
		String statusCd2  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_STATUS_CD2)
		logi.logInfo("The status cd of ContractNo22222222: "+statusCd2)
		if(statusCd2=='NoVal')
		{
			logi.logInfo("statusCd2=NoVal iffffffffff "+statusCd2)
			statusCd2="No Node"
			logi.logInfo("statusCd2: "+statusCd2)
		}
		logi.logInfo("Status cd2 => "+statusCd2)
		row.put("Status cd2",statusCd2.toString())
		
		
		
		String contractNo3  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_CONTRACT_NO3)
		logi.logInfo("The value of ContractNo333333333333: "+contractNo3)
		if(contractNo3=='NoVal')
		{
			logi.logInfo("ContractNo3=NoVal ifffffffff: ")
			contractNo3="No Node"
			logi.logInfo("ContractNo3: "+contractNo3)
		}
		logi.logInfo("Contract No3 => "+contractNo3)
		row.put("Contract No3",contractNo3.toString())
		
		String planNo3=getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_CLIENT_PLAN_NO2)
		logi.logInfo("The planNo of ContractNo3333: "+planNo3)
		if(planNo3=='NoVal')
		{
			logi.logInfo("planNo3=NoVal ifffffffff ")
			planNo3="No Node"
			logi.logInfo("planNo3: "+planNo3)
		}
		logi.logInfo("Plan No3 => "+planNo3)
		row.put("Plan No3",planNo3.toString())
		
		String statusCd3  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_STATUS_CD3)
		logi.logInfo("The status cd of ContractNo333333: "+statusCd3)
		if(statusCd3=='NoVal')
		{
			logi.logInfo("statusCd3=NoVal iffffffffff "+statusCd3)
			statusCd3="No Node"
			logi.logInfo("statusCd3: "+statusCd3)
		}
		logi.logInfo("Status cd3 => "+statusCd3)
		row.put("Status cd3",statusCd3.toString())
		
		
		
		logi.logInfo("md_get_all_acct_active_contract_api => "+row.sort())
		return row.sort()

}

def md_get_all_acct_active_contract_universal_api(String testcaseId)
{
	
		logi.logInfo("Inside md_get_all_acct_active_contract_api")
		// Getting the account number and client id
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		String contractNo1  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_CONTRACT_NO)
		logi.logInfo("The value of ContractNo111111: "+contractNo1)
		if(contractNo1=='NoVal')
		{
			logi.logInfo("ContractNo1=NoVal ifffffffffff ")
			contractNo1="No Node"
			logi.logInfo("ContractNo1: "+contractNo1)
		}
		logi.logInfo("Contract No1 => "+contractNo1)
		row.put("Contract No1",contractNo1.toString())
		
		
		String statusCd1  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_STATUS_CD)
		logi.logInfo("The status cd of ContractNo111111: "+statusCd1)
		if(statusCd1=='NoVal')
		{
			logi.logInfo("statusCd1=NoVal ifffffffff ")
			statusCd1="No Node"
			logi.logInfo("statusCd1: "+statusCd1)
		}
		logi.logInfo("Status cd1 => "+statusCd1)
		row.put("Status cd1",statusCd1.toString())
		
		String contractNo2  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_CONTRACT_NO2)
		logi.logInfo("The value of ContractNo22222222: "+contractNo2)
		if(contractNo2=='NoVal')
		{
			logi.logInfo("ContractNo2=NoVal ifffffffff: ")
			contractNo2="No Node"
			logi.logInfo("ContractNo2: "+contractNo2)
		}
		logi.logInfo("Contract No2 => "+contractNo2)
		row.put("Contract No2",contractNo2.toString())
				
		String statusCd2  = getValueFromResponse('get_all_acct_active_contracts',ExPathRpcEnc.GET_ALL_ACCT_ACTIVE_CONTRACTS_STATUS_CD2)
		logi.logInfo("The status cd of ContractNo22222222: "+statusCd2)
		if(statusCd2=='NoVal')
		{
			logi.logInfo("statusCd2=NoVal iffffffffff "+statusCd2)
			statusCd2="No Node"
			logi.logInfo("statusCd2: "+statusCd2)
		}
		logi.logInfo("Status cd2 => "+statusCd2)
		row.put("Status cd2",statusCd2.toString())
		
		logi.logInfo("md_get_all_acct_active_contract_api => "+row)
		return row.sort()


}


def md_get_all_acct_active_contract_universal_DB(String testcaseId)
{
	
		logi.logInfo("Inside method md_get_all_acct_active_contract_DB")
		// Getting the account number and client id
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		ConnectDB db = new ConnectDB()
		String contractNo1_Q="SELECT contract_no from (select contract_no, row_number() over (order by contract_no) as seqnum from ariacore.acct_universal_contracts where acct_no= "+accountNo+" and status_cd=1) where seqnum=1"
		String contractNo1 = db.executeQueryP2(contractNo1_Q)
		logi.logInfo ("ContractNo11111111111111111111 :"+contractNo1)
		if(contractNo1==null)
		{
			logi.logInfo ("ContractNo111111 inside iffffffffff :")
			
			contractNo1="No Node"
			logi.logInfo ("ContractNo1 :"+contractNo1)
		}
		logi.logInfo("Contract No1  => "+contractNo1.toString())
		row.put("Contract No1",contractNo1.toString())
		
		
		String status_cd1_Q="SELECT status_cd from (select status_cd, row_number() over (order by contract_no) as seqnum from ariacore.acct_universal_contracts where acct_no="+accountNo+") where seqnum=1"
		String status_cd1 = db.executeQueryP2(status_cd1_Q)
		logi.logInfo ("Status code contractNo111111111 :"+status_cd1)
		if(status_cd1==null)
		{
			logi.logInfo ("status_cd1111111111 inside ifffffffffff :")
			status_cd1="No Node"
			logi.logInfo ("status_cd1 :"+status_cd1)
		}
		logi.logInfo("Status cd1  => "+status_cd1.toString())
		row.put("Status cd1",status_cd1.toString())
		
		String contractNo2_Q="SELECT contract_no from (select contract_no, row_number() over (order by contract_no) as seqnum from ariacore.acct_universal_contracts where acct_no= "+accountNo+" and status_cd=1) where seqnum=2"
		String contractNo2 = db.executeQueryP2(contractNo2_Q)
		logi.logInfo ("ContractNo2222222222222 :"+contractNo2)
		if(contractNo2==null)
		{
			logi.logInfo ("ContractNo22222222222222 inside iffffffffff :")
			
			contractNo2="No Node"
			logi.logInfo ("ContractNo2 :"+contractNo2)
		}
		logi.logInfo("Contract No2  => "+contractNo2.toString())
		row.put("Contract No2",contractNo2.toString())
		
		
		String status_cd2_Q="SELECT status_cd from (select status_cd, row_number() over (order by contract_no) as seqnum from ariacore.acct_universal_contracts where acct_no="+accountNo+") where seqnum=2"
		String status_cd2 = db.executeQueryP2(status_cd2_Q)
		logi.logInfo ("Status code contractNo2222222222222 :"+status_cd2)
		if(status_cd2==null)
		{
			logi.logInfo ("status_cd22222222222 inside ifffffffffff :")
			status_cd2="No Node"
			logi.logInfo ("status_cd2 :"+status_cd2)
		}
		logi.logInfo("Status cd2  => "+status_cd2.toString())
		row.put("Status cd2",status_cd2.toString())
		
		logi.logInfo("md_get_all_acct_active_contract_DB => "+row)
		return row.sort()
		
		
}

def md_getLineBaseUnitsFromDB(String tcid){
	logi.logInfo ("Calling md_getLineBaseUnitsFromDB")
	
	ConnectDB db = new ConnectDB()	
	UsageVerificationMethods u=new UsageVerificationMethods(db)
	
	LinkedHashMap lbuDB = new LinkedHashMap<String,LinkedHashMap<String,String>>()
	LinkedHashMap innerHash = new LinkedHashMap<String,String>()
	
	String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
	String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	
	String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();	
	logi.logInfo ("Invoic NO = "+invoiceNo)
	
	String countQry = "select count(*) as base_units from ariacore.gl_detail where invoice_no = %s and usage_units is not null"
	String query = "Select a.seq_num AS LINE_NO,a.service_no,a.usage_units as line_base_units from (select seq_num,service_no,usage_units , row_number() over (order by seq_num ASC) AS custom_seq_num from ariacore.gl_detail where invoice_no = %s and usage_units is not null)a where custom_seq_num = %s"
	
	int lineCount = db.executeQueryP2(String.format(countQry,invoiceNo)).toInteger()
	logi.logInfo ("Line Count is : "+lineCount)
	
	for(int rows = 1; rows<=lineCount; rows++){
		innerHash = db.executeQueryP2(String.format(query,invoiceNo,rows))
		logi.logInfo ("Inner hash for row - "+rows+" is : "+innerHash.toString())
		
		lbuDB.put("LineItem_"+rows,innerHash.sort())
	}
	
	return lbuDB.sort()
}

def md_getLineBaseUnitsFromApi(String tcid)
{
	logi.logInfo ("Calling md_getLineBaseUnitsFromApi")
	List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
	List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
	LinkedHashMap apirow = new LinkedHashMap<String,LinkedHashMap<String,String>>();
	ConnectDB db = new ConnectDB()
	UsageVerificationMethods u=new UsageVerificationMethods(db)

	com.eviware.soapui.support.XmlHolder outputholder
	
	int totalEntries = getNodeCountFromResponse("assign_supp_plan", "//supp_plan_line_items_row/*:line_base_units").toInteger()
	List seqList = []
	
	String acct_no=getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
	String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	
	String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
	logi.logInfo ("Invoic No = "+invoiceNo)
	
	String qry = "select seq_num from ariacore.gl_detail where invoice_no = %s and usage_units is not null"
	seqList = Arrays.asList(db.executeQueryReturnValuesAsArray(String.format(qry, invoiceNo)))	
	logi.logInfo("seqList is : "+seqList)
	
		for(int rows=1;rows<=seqList.size();rows++){
			logi.logInfo("Inside for loop")
			LinkedHashMap innerapirow = new LinkedHashMap<String,String>();
			
			for(int j=0; j<testCaseCmb.size(); j++) {
				if(testCaseCmb[j].toString().contains('assign_supp_plan') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
					logi.logInfo "Inside inner for loop "+j
					outputholder = xmlValues[j]
					innerapirow.put("LINE_NO",outputholder.getNodeValue("//*/*/*:supp_plan_line_items_row["+seqList.get(rows-1)+"]/*:line_no[1]"))
					innerapirow.put('SERVICE_NO', outputholder.getNodeValue("//*/*/*:supp_plan_line_items_row["+seqList.get(rows-1)+"]/*:service_no[1]"))
					innerapirow.put("LINE_BASE_UNITS",outputholder.getNodeValue("//*/*/*:supp_plan_line_items_row["+seqList.get(rows-1)+"]/*:line_base_units[1]"))
				}
			}
			apirow.put("LineItem_"+rows,innerapirow.sort())
			logi.logInfo(apirow.toString())
		}
	return apirow.sort();
}
def md_get_payment_details_api(String testcaseId, String seqNo)
{
	
		logi.logInfo("Inside method md_get_payment_details_api")
		String acct_no = getValueFromRequest("get_acct_payment_methods","//acct_no")
		String noOfSeq =  "SELECT count(*) FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no
		String totalSeq = db.executeQueryP2(noOfSeq)
		// Getting the account number and client id
		LinkedHashMap<String,String> paymentDetails = new LinkedHashMap<String, String>()
		logi.logInfo("1111111")
			paymentDetails.put("Seq No",getValueFromResponse("get_acct_payment_methods", "//*/*/*:acct_pay_methods_row["+seqNo+"]/*:seq_no[1]"))
			logi.logInfo("11111112")
				paymentDetails.put("Pay method id",getValueFromResponse("get_acct_payment_methods", "//*/*/*:acct_pay_methods_row["+seqNo+"]/*:pay_method_id[1]"))
			paymentDetails.put("Pay method name",getValueFromResponse("get_acct_payment_methods", "//*/*/*:acct_pay_methods_row["+seqNo+"]/*:pay_method_name[1]"))
			logi.logInfo("11111113")
			paymentDetails.put("From date time",getValueFromResponse("get_acct_payment_methods", "//*/*/*:acct_pay_methods_row["+seqNo+"]/*:from_date_time[1]"))
			logi.logInfo("11111114")
			paymentDetails.put("To date time",getValueFromResponse("get_acct_payment_methods", "//*/*/*:acct_pay_methods_row["+seqNo+"]/*:to_date_time[1]"))
		 logi.logInfo("Hash map result is : " +paymentDetails)
		return paymentDetails
}

def md_get_payment_details_DB(String testcaseId, String seqNo)
{
	
		logi.logInfo("Inside method md_get_payment_details_DB")
		String acct_no = getValueFromRequest("get_acct_payment_methods","//acct_no")
		String noOfSeq =  "SELECT count(*) FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no
		String totalSeq = db.executeQueryP2(noOfSeq)
		// Getting the account number and client id
		String detailsQuery = "SELECT SEQ_NUM,PAY_METHOD FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no+" AND SEQ_NUM="+seqNo
		ResultSet resultSet = db.executePlaQuery(detailsQuery);
		resultSet.next()
		String payMethod_Query = "SELECT METHOD_NAME FROM ARIACORE.PAYMENT_METHODS WHERE METHOD_ID="+resultSet.getString("PAY_METHOD")
		LinkedHashMap<String,String> paymentDetailsDB = new LinkedHashMap<String, String>()
			paymentDetailsDB.put("Seq No",resultSet.getString("SEQ_NUM"))
			paymentDetailsDB.put("Pay method id",resultSet.getString("PAY_METHOD"))
			paymentDetailsDB.put("Pay method name",db.executeQueryP2(payMethod_Query))
			logi.logInfo("Inside seqhjh")
			paymentDetailsDB.put("From date time",db.executeQueryP2("SELECT to_char(EFFECTIVE_DATE, 'yyyy-mm-dd HH:MI:SS')AS FROM_DATE FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no+" AND SEQ_NUM="+seqNo))
			logi.logInfo("Inside seq ")
			logi.logInfo("Inside seq "+seqNo)
			logi.logInfo("Inside seq "+totalSeq)
			if(seqNo==totalSeq){
				logi.logInfo("Present ")
				paymentDetailsDB.put("To date time","Present")
			}else{			
			paymentDetailsDB.put("To date time",db.executeQueryP2("SELECT to_char(EFFECTIVE_DATE, 'yyyy-mm-dd HH:MI:SS')AS TO_DATE FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no+" AND SEQ_NUM="+((seqNo.toInteger())+1)))
			}
		 logi.logInfo("Hash map result is : " +paymentDetailsDB)
		return paymentDetailsDB
}

def md_get_payment_details_DB11(String testcaseId)
{
	return md_get_payment_details_DB(testcaseId,"1")
}

def md_get_payment_details_DB12(String testcaseId)
{
	return md_get_payment_details_DB(testcaseId,"2")
}

def md_get_payment_details_DB13(String testcaseId)
{
	return md_get_payment_details_DB(testcaseId,"3")
}

def md_get_payment_details_DB14(String testcaseId)
{
	return md_get_payment_details_DB(testcaseId,"4")
}

def md_get_payment_details_api1(String testcaseId)
{
	return md_get_payment_details_api(testcaseId,"1")
}

def md_get_payment_details_api2(String testcaseId)
{
	return md_get_payment_details_api(testcaseId,"2")}

def md_get_payment_details_api3(String testcaseId)
{
	return md_get_payment_details_api(testcaseId,"3")}
def md_get_payment_details_api4(String testcaseId)
{
	return md_get_payment_details_api(testcaseId,"4")}
def md_replace_supp_plan_status_cd(String testcaseId) {
	String client_no = Constant.mycontext.expand('${Properties#Client_No}')
	def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	String supp_plan= getValueFromRequest("replace_supp_plan","//new_supp_plan_no")
	String plan_status_cd="select status_cd from ariacore.acct_supp_plan_map_view where acct_no="+acct_no+" and supp_plan_no="+supp_plan+" and client_no="+client_no
	String status_cd=null
	ConnectDB database = new ConnectDB()
	ResultSet resultSet = database.executeQuery(plan_status_cd);
	while (resultSet.next()){
		status_cd=resultSet.getString(1)
	}
	resultSet.close()
	return status_cd
}
def md_verify_custom_rates_API(String testcaseID)
{
	String client_no = Constant.mycontext.expand('${Properties#Client_No}')
	def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	def totalRow = getNodeCountFromRequest("assign_supp_plan_multi","//new_acct_custom_rates/new_acct_custom_rates_row")
	logi.logInfo(" total no of rows in the given request " +totalRow)
	LinkedHashMap apirow = new LinkedHashMap<String,String>();
	for(int i = 1;i<totalRow;i++)
	{
	String suppPlanNo = getValueFromRequest("assign_supp_plan_multi","//new_acct_custom_rates_row[" + i + "]/custom_rate_plan_no")
	String serviceNo = getValueFromRequest("assign_supp_plan_multi","//new_acct_custom_rates_row[" + i + "]/custom_rate_service_no")
	String rateSeqNo = getValueFromRequest("assign_supp_plan_multi","//new_acct_custom_rates_row[" + i + "]/custom_rate_seq_no")
	String fromUnit = getValueFromRequest("assign_supp_plan_multi","//new_acct_custom_rates_row[" + i + "]/custom_rate_from_unit")
	String toUnit = (((getValueFromRequest("assign_supp_plan_multi","//new_acct_custom_rates_row[" + i + "]/custom_rate_to_unit"))=="NoVal")?null:(getValueFromRequest("assign_supp_plan_multi","//new_acct_custom_rates_row[" + i + "]/custom_rate_to_unit")))
	String rate = getValueFromRequest("assign_supp_plan_multi","//new_acct_custom_rates_row[" + i + "]/custom_rate_per_unit")
	
	apirow.put("Plan No: "+suppPlanNo+" Service No: "+serviceNo+" and Rate tier: "+rateSeqNo,fromUnit+ " to "+toUnit+" - "+rate)
	}
	logi.logInfo("Returned hashmap from API "+apirow.sort())
	return apirow.sort();
}
def md_verify_custom_rates_DB(String testcaseID)
{
	LinkedHashMap apirow = new LinkedHashMap<String,String>();
	String client_no = Constant.mycontext.expand('${Properties#Client_No}')
	def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	String queryCustomRates = "select * from ariacore.acct_rates where acct_no="+acct_no+" and rate_schedule_no is null"
	ResultSet rs = db.executePlaQuery(queryCustomRates)
	logi.logInfo("Plan name from DB :")
	while(rs.next())
	{
		
		logi.logInfo("Plan name from DB :"+rs.getString("PLAN_NO"))
		apirow.put("Plan No: "+rs.getString("PLAN_NO")+" Service No: "+rs.getString("SERVICE_NO")+" and Rate tier: "+rs.getString("RATE_SEQ_NO"),rs.getString("FROM_UNIT")+ " to "+rs.getString("TO_UNIT")+" - "+rs.getString("RATE_PER_UNIT"))
	}
	logi.logInfo("Returned hashmap from API "+apirow.sort())
	return apirow.sort();
}


def md_verifyStmtEmailListAPI(String testcaseID)
{
	String client_no = Constant.mycontext.expand('${Properties#Client_No}')
	def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	String apiname=testcaseID.split("-")[2]
	LinkedHashMap apirow = new LinkedHashMap<String,String>();
	
	String cc = getValueFromRequest(apiname,"//stmnt_email_list_cc")
	String bcc = getValueFromRequest(apiname,"//stmnt_email_list_bcc")
	
	logi.logInfo("cc: "+cc)
	logi.logInfo("bcc :"+bcc)
	String[] ccArray = cc.split("\\|")
	String[] bccArray = bcc.split("\\|")
	
	logi.logInfo("Plan name from DB :"+ccArray.length)
	for(int i = 0; i < ccArray.length; i++){
	  apirow.put("Statement email list CC_"+(i+1), ccArray[i].toString());
	}
	for(int i = 0; i < bccArray.length; i++){
	  apirow.put("Statement email list BCC_"+(i+1), bccArray[i].toString());
	}
	
	logi.logInfo("Returned hashmap from API "+apirow.sort())
	return apirow.sort();
}
def md_verifyStmtEmailListDB(String testcaseID)
{
	LinkedHashMap apirow = new LinkedHashMap<String,String>();
	String client_no = Constant.mycontext.expand('${Properties#Client_No}')
	def acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	String queryStmtMailList = "select EMAIL,IS_CCLIST,IS_BCCLIST from ariacore.ACCT_ADDITIONAL_EMAILS where acct_no = "+acct_no
	ResultSet rs = db.executePlaQuery(queryStmtMailList)
	logi.logInfo("Stmt Email list from DB :")
	int ccCount = 1,bccCount = 1
	while(rs.next())
	{
		
		if(rs.getInt(2)==1){
			apirow.put("Statement email list CC_"+ccCount,rs.getString(1))
			ccCount++
		}
		else if(rs.getInt(3)==1){
			apirow.put("Statement email list BCC_"+bccCount,rs.getString(1))
			bccCount++
		}
		else
			apirow.put("Statement email list BCC",Error)
	}
	logi.logInfo("Returned hashmap from API "+apirow.sort())
	return apirow.sort();
}
}
