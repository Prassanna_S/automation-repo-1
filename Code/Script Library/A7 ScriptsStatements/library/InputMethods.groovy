package library
import java.lang.reflect.Method
import java.sql.ResultSet
import java.sql.ResultSetMetaData
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.LinkedHashMap;
import java.util.List;

import library.Constants.Constant;
import library.Constants.ExPathRpcEnc;
import library.Constants.ExPathRpc;
import library.Constants.ExPathDoc;
import library.Constants.ExPathAdminToolsApi;
import library.Constants.Constant.RESULT_TYPE;
import library.GenericLibrary.Common

class InputMethods  {


	VerificationMethods vm =new VerificationMethods()
	LogResult logi = new LogResult(Constant.logFilePath)
	static ConnectDB db = null;
	//String Query = null



	String md_GET_PAYMENT_AMT(String mode,LinkedHashMap m) {
		logi.logInfo ("Calling md_GET_PAYMENT_AMT")
		boolean childInv=false
		if (mode.contains("CHILD_"))
		{
			mode=mode.split("_")[1]
			childInv=true
		}
		logi.logInfo ("Payment Mode: "+mode)
		String type
		String type_prefix
		String line_no
		if (mode.contains(","))
		{
			type=mode.split("\\,")[0]
			type_prefix=mode.split("\\,")[1]
			line_no=type.replace('INVL', '')
		}
		else
		{

			type=mode
			type_prefix=mode
		}
		logi.logInfo ("type  "+type)
		logi.logInfo ("type_prefix  "+type_prefix)
		
		def acct_no
		if (childInv==false)
		acct_no=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		else
		acct_no=vm.getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo ("Getting Invoice Amount for account: "+ acct_no)
		String query="SELECT  NVL((SUM(amount)-SUM(applied_amount)),0) AS amount_to_be_paid FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO ="+acct_no+ " AND client_no="+client_no+ " AND TRANSACTION_TYPE IN (1,4) "
		//String inv_line_tem_query="select debit from  ARIACORE.gl_detail where invoice_no=(SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+" AND CLIENT_NO="+client_no+") "+ " and seq_num="+line_no
		String inv_line_tem_query="select debit - (select NVL(sum(actual_discount_amt),0) from ariacore.all_invoice_discount_details where invoice_no=(SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+" AND CLIENT_NO="+client_no+") "+" and gl_charge_seq_num="+line_no+") as invoiceamount from  ARIACORE.gl_detail where invoice_no=(SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+" AND CLIENT_NO="+client_no+")  and seq_num="+line_no
		def acct_balance
		String payment_amount

		String strQuery

		ConnectDB db = new ConnectDB()

		// Collection Group
		String query_collection_group="SELECT  AMOUNT AS amount_to_be_paid FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO ="+acct_no+ " AND client_no="+client_no+ " AND TRANSACTION_TYPE IN (3) "
		ResultSet rscg = db.executePlaQuery(query_collection_group);
		def cg_paid_value

		if (rscg.next())
			cg_paid_value = rscg.getObject(1)
		rscg.close()
		
		if(type.contains('TAXL')==true)
		{
			line_no=type.replace('TAXL', '')
			String tquery="select NVL(debit,0) + (select NVL(sum(debit),0)  from ariacore.gl_tax_detail where invoice_no=(SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+" AND CLIENT_NO="+client_no+") and taxed_seq_num="+line_no+") as LINE_AMT_AFTER_TAX from ariacore.gl_detail where client_no="+client_no+ "and seq_num="+line_no+" and  invoice_no=(SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+" AND CLIENT_NO="+client_no+")"
			ResultSet resultSet = db.executePlaQuery(tquery);
			if (resultSet.next()){

				acct_balance = resultSet.getObject(1)
				logi.logInfo "Invoice amount with tax: " + acct_balance.toString()
			}
			resultSet.close()
		}
		
		else if(type.contains('INVL'))
		{
		
			ResultSet resultSet_lineitem = db.executePlaQuery(inv_line_tem_query);
			if (resultSet_lineitem.next()){

				acct_balance = resultSet_lineitem.getObject(1)
				logi.logInfo "Invoice Line Item " + line_no + " amount: " + acct_balance.toString()
			}
			resultSet_lineitem.close()
		}
		else if(type.contains('SURL'))
		{
			line_no=type.split('SURL')[1]
			//String surlineItemQuery="select debit from (select NVL((usage_rate*usage_units),0) as debit , row_number() over(order by seq_num) as sno from  ARIACORE.gl_detail where invoice_no=(SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+") and service_no in ( select service_no from ariacore.all_service where is_surcharge=1 and client_no="+client_no+")) where sno="+line_no
			String surlineItemQuery="select NVL(actual_surcharge_amt,0) from ariacore.GL_DTL_SURCHARGE_DTL where invoice_no=(SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+") and surcharge_application_seq_num="+line_no
			ResultSet resultSet_surlineitem = db.executePlaQuery(surlineItemQuery);
			if (resultSet_surlineitem.next()){

				acct_balance = resultSet_surlineitem.getObject(1)
				logi.logInfo "surcharge Line Item " + line_no + " amount: " + acct_balance.toString()
			}
			resultSet_surlineitem.close()
			logi.logInfo "Invoice amount with surcharge: " + acct_balance.toString()
		}
		else if(type.contains('INVL')==false)
		{
			
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){

				acct_balance = resultSet.getObject(1)
			}
			if (cg_paid_value != null)
				acct_balance = cg_paid_value
			logi.logInfo "Invoice amount: " + acct_balance.toString()
			resultSet.close()
		}
		if(type_prefix.contains("+"))
		{

			def t=type_prefix.replace("+", "")
			t=t.toInteger()
			payment_amount=(acct_balance +t ).toString()
		}
		else if(type_prefix.contains("-"))
		{
			def t=type_prefix.replace("-", "")
			t=t.toInteger()
			payment_amount=(acct_balance - t).toString()
		}

		else if(type_prefix.contains("%"))
		{

			def t=type_prefix.replace("%", "")
			if(t.contains('.'))
				t=t.toDouble()
			else
				t=t.toInteger()
			payment_amount=((acct_balance*t)/100).toString()
		}

		else if(type_prefix.equals("INV"))
		{

			payment_amount=acct_balance.toString()
		}
		else if(type_prefix.contains("TAXL"))
		{

			payment_amount=acct_balance.toString()
		}

		else
		{
			payment_amount=""
		}

		//db.closeConnection();
		logi.logInfo("Amount to pay as per the test case: " +payment_amount)
		return payment_amount


	}



	String md_GET_CASHCREDIT_AMT(String mode,LinkedHashMap m) {
		logi.logInfo ("Calling md_GET_CASHCREDIT_AMT")
		logi.logInfo ("Credit Type: " + mode)
		ConnectDB db = new ConnectDB()
		ConnectDB db1 = new ConnectDB()
		ResultSet resultSet_lineitem
		String type
		String type_prefix
		String line_no
		String acct_no=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		if (mode.contains(","))
		{

			type=mode.split("\\,")[0]

			type_prefix=mode.split("\\,")[1]

			line_no=type.replace('INVL', '')

			String inv_line_tem_query="SELECT debit from  ARIACORE.gl_detail where invoice_no=(SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+" AND CLIENT_NO="+client_no+") "+
					" and seq_num="+line_no
			resultSet_lineitem= db.executePlaQuery(inv_line_tem_query);
		}
		else
		{

			type=mode
			type_prefix=mode
		}

		def invoiceAmount
		def credit_toapply

		if(type.contains('INVL'))
		{

			if (resultSet_lineitem.next()){
				invoiceAmount = resultSet_lineitem.getObject(1)
				resultSet_lineitem.close()
			}

		}


		logi.logInfo ("Doing cash credit for the account: "+acct_no)
		String query_inv_amt= "SELECT amount as INVOICE_AMOUNT  FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+"  and client_no="+client_no+" and  TRANSACTION_TYPE=1"

		String strQuery

		ResultSet resultSet = db.executePlaQuery(query_inv_amt);
		if(type.contains('INVL') == false)
		{
			if (resultSet.next()){

				invoiceAmount = resultSet.getObject(1)
				resultSet.close()
			}
		}


		logi.logInfo ("Invoice Amount "+invoiceAmount)

		if(type_prefix.contains("+"))
		{
			def t=type_prefix.replace("+", "")
			t=t.toInteger()
			credit_toapply=(invoiceAmount +t).toString()
		}
		else if(type_prefix.contains("-"))
		{
			def t=type_prefix.replace("+", "")
			t=t.toInteger()
			credit_toapply=(invoiceAmount - t).toString()
		}

		else if(type_prefix.contains("%"))
		{
			def t=type_prefix.replace("%", "")
			if(t.contains('.'))
				t=t.toDouble()
			else
				t=t.toInteger()
			credit_toapply=((invoiceAmount*t)/100).toString()

		}

		else if(type_prefix.contains("INV"))
		{

			credit_toapply=invoiceAmount.toString()
		}

		else
		{
			credit_toapply=""
		}

		//db.closeConnection();
		logi.logInfo("Amount to credit: " +credit_toapply.toString())
		return credit_toapply.toString()


	}


	String md_GET_SERVICECREDIT_AMT(String type,LinkedHashMap m)
	{
		logi.logInfo("Calling md_GET_SERVICECREDIT_AMT")
		//logi.logInfo(vm.getValueFromRequest("create_acct_complete", "//master_plan_no"))
		//logi.logInfo(vm.getValueFromRequest("create_acct_complete", "//master_plan_units[1]"))

		String masterPlanNo= Constant.lhmAllServcieDataAsXML_Global.get("create_acct_complete").get("master_plan_no")
		String masterPlanUnits=Constant.lhmAllServcieDataAsXML_Global.get("create_acct_complete").get("master_plan_units")
		String acct_no=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')

		String query_service_details="SELECT service_no as SERVICE_NO,service_type as SERVICE_TYPE from ARIACORE.all_service where SERVICE_NO in (SELECT service_no  from ARIACORE.ACCT_RATES_HISTORY_DETAILS where acct_no= "+acct_no+")"

		ConnectDB db = new ConnectDB()

		ResultSet serviceResultSet = db.executePlaQuery(query_service_details);
		ResultSetMetaData md = serviceResultSet.getMetaData();

		int columns = md.getColumnCount();
		LinkedHashMap <String,String>services = new LinkedHashMap<String,String>();

		while (serviceResultSet.next()){

			for(int i=1; i<=columns; i++){

				services.put(serviceResultSet.getString("SERVICE_NO"),serviceResultSet.getString("SERVICE_TYPE"))
				//logi.logInfo services.toString()
			}

		}

		def expected_inv_amt=0
		String query_inv_amt
		def to_credit

		services.each { k, v ->

			if(v.toString()=="AC")
			{
				query_inv_amt="SELECT round(RATE_PER_UNIT * 1,2) FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SERVICE_NO="+k+" AND SCHEDULE_NO=(SELECT RATE_SCHEDULE_NO FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO ="+acct_no+"  AND PLAN_NO= "+masterPlanNo+" AND "+
						" SERVICE_NO="+k+" AND RATE_SEQ_NO=1 AND CLIENT_NO="+client_no+") AND RATE_SEQ_NO = 1 AND CLIENT_NO="+client_no
			}

			else
			{
				query_inv_amt="SELECT round(RATE_PER_UNIT * "+masterPlanUnits+",2) FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SERVICE_NO="+k+" AND SCHEDULE_NO=(SELECT RATE_SCHEDULE_NO FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO ="+acct_no+"  AND PLAN_NO= "+masterPlanNo+" AND "+
						" SERVICE_NO="+k+" AND RATE_SEQ_NO=1 AND CLIENT_NO="+client_no+") AND RATE_SEQ_NO = 1 AND CLIENT_NO="+client_no
			}



			ResultSet invResultSet = db.executePlaQuery(query_inv_amt);
			while(invResultSet.next())
			{
				def amt=invResultSet.getObject(1)
				expected_inv_amt=expected_inv_amt+amt
			}


		}
		logi.logInfo "Account Number: " + acct_no
		logi.logInfo "Calculated invoice amount: " +expected_inv_amt

		if(type.contains("%"))
		{
			def t=type.replace("%", "")
			if(t.contains('.'))
				t=t.toDouble()
			else
				t=t.toInteger()
			to_credit=((expected_inv_amt*t)/100).toString()

		}
		logi.logInfo "Amount for service credit: " +to_credit.toString()

		return to_credit.toString()
	}


	String md_GET_TRANSACTION_ID(String type,LinkedHashMap m) {
		logi.logInfo ("Calling md_GET_TRANSACTION_ID")

		if (type == "DB")
		{
			//select EVENT_NO from ariacore.acct_transaction where acct_no = 10480316 and transaction_type=3
			def acct_no=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String query= "SELECT event_no FROM (SELECT event_no,row_number() over(order by create_date DESC) AS seqno from ariacore.acct_transaction WHERE transaction_type=3 AND acct_no ="+acct_no+") WHERE seqno=1"
			ConnectDB db = new ConnectDB()
			ResultSet resultSet = db.executePlaQuery(query);
			def tran_id
			if (resultSet.next()){

				tran_id = resultSet.getObject(1);
				resultSet.close()
			}
			else
			{
				tran_id = "No value returned"
			}
			logi.logInfo "Electronic Payment Transaction ID: " + tran_id.toString()
			return tran_id
		}
		else if(type == "INV")
		{
			def acct_no=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String query= "SELECT event_no FROM (SELECT event_no,row_number() over(order by create_date DESC) AS seqno from ariacore.acct_transaction WHERE transaction_type=1 AND acct_no ="+acct_no+") WHERE seqno=1"
			ConnectDB db = new ConnectDB()
			ResultSet resultSet = db.executePlaQuery(query);
			def tran_id
			if (resultSet.next()){

				tran_id = resultSet.getObject(1);
				resultSet.close()
			}
			else
			{
				tran_id = "No value returned"
			}
			logi.logInfo "Electronic Payment Transaction ID: " + tran_id.toString()
			return tran_id
		}

		else if(type == "INV_CHILD")
		{
			def acct_no=vm.getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String query= "SELECT event_no FROM (SELECT event_no,row_number() over(order by create_date DESC) AS seqno from ariacore.acct_transaction WHERE transaction_type=1 AND acct_no ="+acct_no+") WHERE seqno=1"
			ConnectDB db = new ConnectDB()
			ResultSet resultSet = db.executePlaQuery(query);
			def tran_id
			if (resultSet.next()){

				tran_id = resultSet.getObject(1);
				resultSet.close()
			}
			else
			{
				tran_id = "No value returned"
			}
			logi.logInfo "Electronic Payment Transaction ID: " + tran_id.toString()
			return tran_id
		}
		else if (type == "DBASC")
		{
			def acct_no=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String query= "SELECT event_no FROM (SELECT event_no,row_number() over(order by create_date ASC) AS seqno from ariacore.acct_transaction WHERE transaction_type=3 AND acct_no ="+acct_no+") WHERE seqno=1"
			ConnectDB db = new ConnectDB()
			ResultSet resultSet = db.executePlaQuery(query);
			def tran_id
			if (resultSet.next()){

				tran_id = resultSet.getObject(1);
				resultSet.close()
			}
			else
			{
				tran_id = "No value returned"
			}
			logi.logInfo "Electronic Payment Transaction ID: " + tran_id.toString()
			return tran_id
		}
		
		else if (type == "Credit")
		{
			def acct_no=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String query= "SELECT event_no FROM (SELECT event_no,row_number() over(order by create_date ASC) AS seqno from ariacore.acct_transaction WHERE transaction_type=10 AND acct_no ="+acct_no+") WHERE seqno=1"
			ConnectDB db = new ConnectDB()
			ResultSet resultSet = db.executePlaQuery(query);
			def tran_id
			if (resultSet.next()){

				tran_id = resultSet.getObject(1);
				resultSet.close()
			}
			else
			{
				tran_id = "No value returned"
			}
			logi.logInfo "Electronic Payment Transaction ID: " + tran_id.toString()
			return tran_id
		}
	
		else
		{
			logi.logInfo ("Payment Type: " + type)

			String s_name=type.split(":")[0]
			String type1=type.split(":")[1]

			String api=s_name+'.'+type1
			logi.logInfo ("API: "+api)
			def tran_id=vm.getValueFromResponse(api,"//transaction_id")
			logi.logInfo (" Transaction ID taken for this transaction: "+tran_id)
			//String query= "select balance from ariacore.acct_balance where acct_no="+acct_no.toString()



			return tran_id
		}


	}

	String md_GET_LINE_NUMBER(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_LINE_NUMBER")
		logi.logInfo("parameter"+type)
		String lineno
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		if(type.equals("COUPON"))
		{
			String c_code=vm.getValueFromRequest("create_acct_complete", "//*/*:coupon_codes[1]").toString()
			logi.logInfo("Coupon code Applied: "+c_code)
			String inv_no = vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
			logi.logInfo("Generated Invoice No: " + inv_no)
			String query= "Select seq_num from ARIACORE.gl_detail where invoice_no="+inv_no+" and comments Like '%"+c_code+"%'"
			//Alternate query - Select gld.seq_NUM from ARIACORE.gl_detail gld join ariacore.gl_credit_detail glcd on gld.invoice_no=glcd.invoice_no and gld.seq_num=glcd.gl_dtl_credit_seq_num where gld.invoice_no=12224941
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next())
			{
				lineno = resultSet.getString(1);
			}
			logi.logInfo("Coupon Line Number in the Invoice:"+lineno)
		}
		else if(type.equals("USAGE"))
		{
			String inv
			String use_type=vm.getValueFromRequest("record_usage", "//usage_type").toString()
			String acc_no=vm.getValueFromRequest("record_usage", "//acct_no").toString()
			String inv_qry="SELECT invoice_no from ariacore.gl where acct_no="+acc_no+"and rownum=1 order by bill_date desc"
			ResultSet rs_inv_qry= db.executePlaQuery(inv_qry);
			while(rs_inv_qry.next())
			{
				inv=rs_inv_qry.getString(1)
			}
			String seq_qry="Select seq_num from ariacore.gl_detail where invoice_no="+inv+" and usage_type ="+use_type
			ResultSet rs_seq_qry= db.executePlaQuery(seq_qry);
			while(rs_seq_qry.next())
			{
				lineno=rs_seq_qry.getString(1)
			}

		}
		else if(type.equals("RECUR"))
		{
			
			String acct_no=vm.getValueFromResponse("create_acct_complete", "//acct_no").toString()
		
			String inv = db.executeQueryP2("Select max(invoice_no) from ariacore.gl where acct_no="+acct_no+" and client_no="+client_no)
			lineno= db.executeQueryP2("Select glt.seq_num from ariacore.gl_detail glt JOIN ariacore.services ser ON ser.service_no=glt.service_no where glt.invoice_no="+inv+" and glt.client_no=ser.custom_to_client_no and ser.recurring=1 and glt.client_no="+client_no +" and rownum=1")

		}
		else if(type.contains('SURL'))
		{
			String lno=type.split('SURL')[1]
			logi.logInfo ("lno :"+lno)
			String acct_no=vm.getValueFromResponse("create_acct_complete", "//acct_no").toString()
			logi.logInfo ("acct_no :"+acct_no)
			//String surlineItemQuery="select seq_num from (select seq_num,debit, row_number() over(order by seq_num) as sno from  ARIACORE.gl_detail where invoice_no=(SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+") and service_no in ( select service_no from ariacore.all_service where is_surcharge=1 and client_no="+client_no+")) where sno="+lno
			String surlineItemQuery="select gl_surcharge_seq_num from ariacore.GL_DTL_SURCHARGE_DTL where invoice_no=(SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+")  and surcharge_application_seq_num="+lno
			logi.logInfo ("surlineItemQuery :"+surlineItemQuery)
			lineno = db.executeQueryP2(surlineItemQuery)
			
		}


		return lineno

	}

	String md_GET_TRANSACTION_AMT(String type, LinkedHashMap m) {
		logi.logInfo ("Calling md_GET_TRANSACTION_AMT")
		ConnectDB db = new ConnectDB()
		//lit:record_external_payment[1]/payment_amount[1]

		logi.logInfo ("Payment Type: "+type)
		String api=null
		String prefix_api=type.split("\\,")[0]
		String type_prefix=type.split("\\,")[1]
		if(type.contains(prefix_api))
		{
			api="record_external_payment"+'.'+prefix_api
		}

		logi.logInfo("Payment amount from record external api request" +vm.getValueFromRequest(api, "//*/*:payment_amount[1]").toString())
		String paid_amt_from_input=vm.getValueFromRequest(api, "//*/*:payment_amount[1]").toString()
		def tran_id=vm.getValueFromResponse(api,"//transaction_id")
		logi.logInfo ("Transaction id taken for account: "+tran_id)
		//String query= "select balance from ariacore.acct_balance where acct_no="+acct_no.toString()
		def paid_amt
		String to_refund_amt
		def refunded_amt
		def total_amt
		def inv_amt
		def invasc_amt
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)

		if(tran_id.toString() == "NoVal"){
			logi.logInfo("Getting transaction id for collection group payment")
			String collectionTransQry = "select event_no from ariacore.acct_transaction where acct_no = "+acct_no+" and transaction_type =3"

			ResultSet resultSet_transId = db.executePlaQuery(collectionTransQry);
			if (resultSet_transId.next()){
				tran_id = resultSet_transId.getObject(1);
				logi.logInfo("Transaction id taken for collection group : "+tran_id)
			}
			resultSet_transId.close()

		}

		String query_refunded_amount="select nvl(sum(amount),0) from ariacore.check_refund where acct_no="+acct_no

		String query_total_amount= "select sum(act.APPLIED_AMOUNT) from  ARIACORE.ACCT_TRANSACTION act "+
				" where act.acct_no="+acct_no+" and  act.TRANSACTION_TYPE in(2,3) and act.client_no="+client_no+" and act.event_no="+tran_id

		String query_inv_amount="SELECT amount AS INVOICE_AMOUNT FROM (SELECT amount,row_number() over (order by create_date DESC) AS seqno FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+" AND TRANSACTION_TYPE = 1) where seqno=1"



		ResultSet resultSet_refund = db.executePlaQuery(query_refunded_amount);
		if (resultSet_refund.next())
		{
			refunded_amt = resultSet_refund.getObject(1);
			resultSet_refund.close()
		}


		ResultSet resultSet_total = db.executePlaQuery(query_total_amount);
		if (resultSet_total.next()){
			total_amt = resultSet_total.getObject(1);
			resultSet_total.close()
		}

		ResultSet resultSet_inv = db.executePlaQuery(query_inv_amount);
		if (resultSet_inv.next()){
			inv_amt = resultSet_inv.getObject(1);
			resultSet_inv.close()
		}


		if (total_amt != null && refunded_amt != null)
			paid_amt=total_amt-refunded_amt


		logi.logInfo("paid_amt is :: "+paid_amt.toString())
		if(type_prefix.contains("+"))
		{

			to_refund_amt=(paid_amt + type_prefix.replace("+", "").toInteger()).toString()
		}
		else if(type_prefix.contains("-"))
		{
			to_refund_amt=(paid_amt - type_prefix.replace("-", "").toInteger()).toString()
		}

		else if(type_prefix.contains("%"))
		{
			def t=type_prefix.replace("%", "")
			if(t.contains('.'))
			{
				t=t.toDouble()
				to_refund_amt=((paid_amt*t)/100).toString()
			}

			else
			{
				t=t.toInteger()
				to_refund_amt=(((paid_amt*100*t).toInteger())/10000).toString()
			}



		}

		else if(type_prefix.contains("PAY"))
		{
			to_refund_amt=paid_amt.toString()
		}
		else if(type_prefix.contains("INV"))
		{
			to_refund_amt=inv_amt.toString()
		}
		else if(type_prefix.contains("ASC"))
		{
			String qry_amt="SELECT amount AS INVOICE_AMOUNT FROM (SELECT amount,row_number() over (order by create_date ASC) AS seqno FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+" AND TRANSACTION_TYPE = 1) where seqno=1"
			ResultSet rs_qry_amt = db.executePlaQuery(qry_amt);
			while(rs_qry_amt.next())
			{
				invasc_amt = rs_qry_amt.getObject(1)
			}

			to_refund_amt=invasc_amt.toString()
		}
		else if(type_prefix.contains("EXTERNAL"))
		{
			to_refund_amt=paid_amt_from_input.toString()
		}


		else
		{
			to_refund_amt=""
		}

		//db.closeConnection();
		logi.logInfo("Amount to refund: " +to_refund_amt)
		return to_refund_amt

	}



	String md_GET_INV_NO(String txt,LinkedHashMap m)

	{
		logi.logInfo ("Inside md_GET_INV_NO")
		String invoiceno
		String acct_no
		if(txt.contains("DB"))
		{
			if(txt.contains("."))
			{
				String pref=txt.split("\\.")[1]
				logi.logInfo("pref:"+pref)
				String api="create_acct_complete.".concat(pref)
				acct_no=vm.getValueFromResponse(api,ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			}

			else
			{
				acct_no=vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			}
			String query="SELECT invoice_no from ariacore.gl where acct_no="+acct_no+" and rownum=1 order by bill_date desc "
			ConnectDB db = new ConnectDB()
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next())
			{
				invoiceno = resultSet.getString(1);
			}
			resultSet.close();
		}
		else
		{
			invoiceno=vm.getValueFromResponse(txt,ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		}

		logi.logInfo ("Invoice No: "+invoiceno)
		return invoiceno

	}

	String md_GET_INV_LINE_REV_AMT(String type,LinkedHashMap<String,String> m)

	{
		String inv_no = m.get("invoices_to_reverse|invoice_no")
		logi.logInfo("Invoice Number: "+inv_no)
		String line_no = m.get("invoices_to_reverse|invoice_line_no")
		logi.logInfo("Invoice Line No: "+line_no)


		String[] txt_array = type.split(":line")
		type = txt_array[0]
		int line = txt_array[1].toInteger()


		inv_no = inv_no.split("\\|")[line]
		line_no = line_no.split("\\|")[line]


		String query= "Select NVL(sum(debit),0) from ariacore.all_invoice_details where invoice_no="+inv_no+" and seq_num="+line_no


		ConnectDB db = new ConnectDB()

		ResultSet resultSet = db.executePlaQuery(query);
		def amt
		String rev_amt

		if (resultSet.next())
		{
			amt = resultSet.getObject(1);
			amt = (amt < 0 ? -amt : amt);
		}
		logi.logInfo("Invoice Line Amount: " + amt.toString())

		if(type.contains("REV"))		{
			rev_amt=amt.toString()
		}

		else if(type.contains("+"))		{
			def t=type.replace("+", "")
			t=t.toInteger()
			rev_amt=(amt + t.toInteger()).toString()
		}

		else if(type.contains("-"))		{
			def t=type.replace("+", "")
			t=t.toInteger()
			rev_amt=(amt + t.toInteger()).toString()
		}

		else if(type.contains("%"))		{
			def t=type.replace("%", "")
			if(t.contains('.'))
				t=t.toDouble()
			else
				t=t.toInteger()

			rev_amt=((amt*t)/100).toString()
		}

		else
		{
			rev_amt=""		}

		logi.logInfo("Amount to Reverse: " + rev_amt)
		return rev_amt

	}



	def invokeInputMethod(String mname, LinkedHashMap rowval)
	{
		logi.logInfo ("Calling InvokeInputMethod")
		String methodname=null
		String[] tmp
		Object[] parameters = new Object[2]
		if(mname.contains("_param_"))
		{

			tmp =mname.split("_param_");
			methodname=tmp[0];
			parameters[0]=tmp[1]
			parameters[1]=rowval
		}
		else
		{
			methodname=mname
		}

		logi.logInfo("Invoking method: "+methodname)


		Class[] parametersTypes = new Class[2]
		parametersTypes[0] = String.class;
		parametersTypes[1] = LinkedHashMap.class;
		try
		{
			Method method = this.class.getDeclaredMethod(methodname.toString(), parametersTypes);
			String nodeValue = method.invoke(new InputMethods(), parameters);
			return nodeValue

		}catch(Exception e)
		{
			logi.logInfo(e.toString());
			System.out.println (e.toString());
		}

	}
	public String getTransactionIdForCollection(String acct_Num){
		ConnectDB db = new ConnectDB()
		String query = "select event_no from ariacore.acct_transaction where acct_no ="+acct_Num+" and transaction_type=3";
		String transId
		transId = db.executeQueryP2(query)
		logi.logInfo("Transaction id for collection : "+transId)
		return transId
	}
	public boolean archiveExistingAccounts(String client_Number){
		boolean successFlag = false
		db = new ConnectDB();
		String archiveAccountQry = "UPDATE ARIACORE.ACCT set status_cd = -99 where client_no = "+client_Number+" and status_cd not in (-99)"

		if(db.statement.executeUpdate(archiveAccountQry) > 0){
			successFlag = true
		}
		return successFlag
	}

	public void archiveAllExistingAccounts(List clientNumber){
		int clientCount = clientNumber.size()
		boolean archivedFlag
		for(int i=0; i<clientCount; i++){
			archivedFlag = archiveExistingAccounts(clientNumber.get(i))
			if(archivedFlag == true){
				logi.logInfo "Archived all the existing accounts in client : "+clientNumber.get(i)
			} else{
				logi.logInfo "Unable to Archive the existing accounts in client : "+clientNumber.get(i)
			}
		}
	}

	
	
	def md_GET_ACCT_NO(String type,LinkedHashMap<String,String> m)
	{
		logi.logInfo("calling md_GET_ACCT_NO ")
		def accno
		logi.logInfo("type:"+type)

		if(type.equalsIgnoreCase("parent")==true)
		{
			logi.logInfo("Parent acctno:")
			accno=vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		}
		else if(type.equalsIgnoreCase("child")==true)
		{
			accno=vm.getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		}

		else if(type.equalsIgnoreCase("child2")==true)
		{
			accno=vm.getValueFromResponse("create_acct_complete.b",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		}

		else{
			accno=111234
			}

		return accno
	}
	
	
	
	String md_GET_REV_AMT_AFTER_COUPON(String s,LinkedHashMap seq_no)
	{
		logi.logInfo("Entering in to method  ::md_GET_REV_AMT_AFTER_COUPON ")
		String account_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String invoice_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String seq_num
		String coupon_amt
		String actual_line_amt
		String remaining_rev_amt
		String coupon_amt_query="SELECT SEQ_NUM as SEQ_NO,SUM(DISTILLED_AMOUNT) as COUPON_AMT FROM (SELECT ELIGIBLE_SERVICE_NO,DISTILLED_AMOUNT FROM ariacore.all_recur_credit WHERE acct_no =    (SELECT acct_no FROM ariacore.gl WHERE invoice_no="+invoice_No+") UNION SELECT CDR.KEY_2_VAL AS ELIGIBLE_SERVICE_NO,  CASE WHEN CDRS.INLINE_OFFSET_IND='I' THEN 0      ELSE AIDD.ACTUAL_DISCOUNT_AMT END AS DISTILLED_AMOUNT FROM ARIACORE.CLIENT_DR_COMPONENTS CDR JOIN ariacore.all_invoice_discount_details AIDD ON AIDD.rule_no=CDR.rule_no JOIN ariacore.CLIENT_DISCOUNT_RULES CDRS ON CDRS.rule_no =AIDD.rule_no WHERE AIDD.invoice_no ="+invoice_No+" )a join ariacore.gl_detail gd on gd.service_no=a.ELIGIBLE_SERVICE_NO and gd.invoice_no="+invoice_No+" AND gd.ORIG_COUPON_CD IS NULL GROUP BY SEQ_NUM"
		ConnectDB db = new ConnectDB()
		ResultSet rs = db.executePlaQuery(coupon_amt_query);
		while (rs.next()){
			seq_num=rs.getString("SEQ_NO")
			coupon_amt=rs.getString("COUPON_AMT")
			logi.logInfo "Coupon applied sequence number is "+seq_num
			logi.logInfo "Coupon amount applied for the above sequence number is "+coupon_amt
		}
		String actual_line_amt_query="SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_No+" AND SEQ_NUM="+seq_num
		actual_line_amt=db.executeQueryP2(actual_line_amt_query)
		logi.logInfo "Actual line item amount before coupon is "+actual_line_amt
		if(actual_line_amt.toString().contains(".") || coupon_amt.contains("."))
		{
			remaining_rev_amt = (actual_line_amt.toDouble() - coupon_amt.toDouble())
		}
		else {
			remaining_rev_amt = (actual_line_amt.toInteger() - coupon_amt.toInteger())
		}
		logi.logInfo("Remaining reversible amount after coupon is :: "+remaining_rev_amt)
		return remaining_rev_amt
	}

	String md_GET_REV_COUPON_APPLIED_LINE_ITEM(String s,LinkedHashMap seq_no)
	{
		logi.logInfo("Entering in to method  ::md_GET_REV_COUPON_APPLIED_LINE_ITEM ")
		String account_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String invoice_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String seq_num
		String seq_num_query="SELECT SEQ_NUM as SEQ_NO FROM (SELECT ELIGIBLE_SERVICE_NO,DISTILLED_AMOUNT FROM ariacore.all_recur_credit WHERE acct_no =    (SELECT acct_no FROM ariacore.gl WHERE invoice_no="+invoice_No+") UNION SELECT CDR.KEY_2_VAL AS ELIGIBLE_SERVICE_NO,  CASE WHEN CDRS.INLINE_OFFSET_IND='I' THEN 0      ELSE AIDD.ACTUAL_DISCOUNT_AMT END AS DISTILLED_AMOUNT FROM ARIACORE.CLIENT_DR_COMPONENTS CDR JOIN ariacore.all_invoice_discount_details AIDD ON AIDD.rule_no=CDR.rule_no JOIN ariacore.CLIENT_DISCOUNT_RULES CDRS ON CDRS.rule_no =AIDD.rule_no WHERE AIDD.invoice_no ="+invoice_No+" )a join ariacore.gl_detail gd on gd.service_no=a.ELIGIBLE_SERVICE_NO and gd.invoice_no="+invoice_No+" AND gd.ORIG_COUPON_CD IS NULL GROUP BY SEQ_NUM"
		ConnectDB db = new ConnectDB()
		ResultSet rs = db.executePlaQuery(seq_num_query);
		while (rs.next()){
			seq_num=rs.getString("SEQ_NO")
			logi.logInfo "Coupon applied sequence number is "+seq_num
		}
		return seq_num
	}

	public String md_UPDATE_PAYMENT_DISCOUNT(String type, LinkedHashMap m) {
		logi.logInfo ("Calling md_UPDATE_PAYMENT_DISCOUNT")
		String methodCalled
		boolean successFlag = false

		String discountRule
		String discountPercentage
		String flatDiscount
		String discountAmount
		String currencyCode
		String methodId
		String clientNum
		String fin_string
		String[] valuesForInput

		logi.logInfo ("TYPE IS :: "+type)
		if(type.contains("#")){
			fin_string = type.replace("#", "")
			valuesForInput = fin_string.split(",")
			discountRule = valuesForInput[0]
			discountPercentage = valuesForInput[1]
			methodId = valuesForInput[2]
			clientNum = valuesForInput[3]

			methodCalled = update_payment_discount_percentage_and_commit(discountRule, discountPercentage, methodId, clientNum)
		} else if(type.contains("+")){
			fin_string = type.replace("+", "")
			valuesForInput = fin_string.split(",")

			discountRule = valuesForInput[0]
			methodId = valuesForInput[1]
			flatDiscount = valuesForInput[2]
			currencyCode = valuesForInput[3]
			clientNum = valuesForInput[4]

			methodCalled = update_payment_discount_flat_and_commit(discountRule, methodId, flatDiscount, currencyCode,clientNum)
		}
		return ""
	}

	public String update_payment_discount_percentage_and_commit(String discRule, String discPercent, String methodId,String clientNum){
		boolean successFlag = false

		ConnectDB dbase = new ConnectDB()
		String updateQuery = "UPDATE ARIACORE.CLIENT_PAYMENT_METHOD SET DISCOUNT_RULE_NO="+discRule+",DISCOUNT_PERCENTAGE="+discPercent+" WHERE METHOD_ID="+methodId+" AND CLIENT_NO ="+clientNum
		logi.logInfo "Executing Query" + updateQuery
		dbase.connection.commit();
		if(dbase.statement.executeUpdate(updateQuery) > 0){
			successFlag = true
		}
		dbase.connection.commit();
		logi.logInfo("Updated the payment discount with discount rule :: '"+discRule+"' and discount percentage :: '"+discPercent+"' and method id as :: '"+methodId)
		return "No Val"
	}

	public String update_payment_discount_flat_and_commit(String discRule, String methodId, String flatDiscount, String currencyCode,String clientNum){
		boolean successFlag1 = false
		boolean successFlag2 = false
		ConnectDB dbase = new ConnectDB()

		String updateQuery = "UPDATE ARIACORE.CLIENT_PAYMENT_METHOD SET DISCOUNT_RULE_NO="+discRule+",DISCOUNT_PERCENTAGE=null WHERE METHOD_ID="+methodId+" AND CLIENT_NO ="+clientNum
		dbase.connection.commit();
		logi.logInfo "Executing Query" + updateQuery
		if(dbase.statement.executeUpdate(updateQuery) > 0){
			successFlag1 = true
			logi.logInfo("Flat query 1 executed ")
		}

		String updateFlatQuery = "update ariacore.CLI_PAY_MTH_FLAT_DISC_CURR_MAP set amount = "+flatDiscount+" WHERE METHOD_ID="+methodId+" AND CLIENT_NO ="+clientNum+" and currency_cd = '"+currencyCode+"'"
		logi.logInfo "Executing Query" + updateFlatQuery
		if(dbase.statement.executeUpdate(updateFlatQuery) > 0){
			successFlag2 = true
			logi.logInfo("Flat query 2 executed ")
		}

		dbase.connection.commit();
		logi.logInfo("Updated the payment discount with discount rule :: '"+discRule+"' and flat discount amount :: '"+flatDiscount+"' and method id as :: '"+methodId+" for currency :: "+currencyCode)

		return "No Val"
	}

	public String md_GET_REV_AMT_AFTER_COUPON_CR1(String s,LinkedHashMap seq_no)
	{
		def tax_rate_list = []
		def cal_tax
		def total_tax=0

		logi.logInfo("Entering in to method  ::md_GET_REV_AMT_AFTER_COUPON ")
		String account_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String invoice_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String seq_num
		String coupon_amt
		String actual_line_amt
		def remaining_rev_amt

		String coupon_amt_query="SELECT SEQ_NUM as SEQ_NO,SUM(DISTILLED_AMOUNT) as COUPON_AMT FROM (SELECT ELIGIBLE_SERVICE_NO,DISTILLED_AMOUNT FROM ariacore.all_recur_credit WHERE acct_no =    (SELECT acct_no FROM ariacore.gl WHERE invoice_no="+invoice_No+") UNION SELECT CDR.KEY_2_VAL AS ELIGIBLE_SERVICE_NO,  CASE WHEN CDRS.INLINE_OFFSET_IND='I' THEN 0      ELSE AIDD.ACTUAL_DISCOUNT_AMT END AS DISTILLED_AMOUNT FROM ARIACORE.CLIENT_DR_COMPONENTS CDR JOIN ariacore.all_invoice_discount_details AIDD ON AIDD.rule_no=CDR.rule_no JOIN ariacore.CLIENT_DISCOUNT_RULES CDRS ON CDRS.rule_no =AIDD.rule_no WHERE AIDD.invoice_no ="+invoice_No+" )a join ariacore.gl_detail gd on gd.service_no=a.ELIGIBLE_SERVICE_NO and gd.invoice_no="+invoice_No+" AND gd.ORIG_COUPON_CD IS NULL GROUP BY SEQ_NUM"
		ConnectDB db = new ConnectDB()
		ResultSet rs = db.executePlaQuery(coupon_amt_query);
		while (rs.next()){
			seq_num=rs.getString("SEQ_NO")
			coupon_amt=rs.getString("COUPON_AMT")
			logi.logInfo "Coupon applied sequence number is "+seq_num
			logi.logInfo "Coupon amount applied for the above sequence number is "+coupon_amt
		}
		String actual_line_amt_query="SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_No+" AND SEQ_NUM="+seq_num
		actual_line_amt=db.executeQueryP2(actual_line_amt_query)
		logi.logInfo "Actual line item amount before coupon is "+actual_line_amt
		if(actual_line_amt.toString().contains(".") || coupon_amt.contains("."))
		{
			remaining_rev_amt = (actual_line_amt.toDouble() - coupon_amt.toDouble())
		}
		else {
			remaining_rev_amt = (actual_line_amt.toInteger() - coupon_amt.toInteger())
		}
		logi.logInfo("Remaining reversible amount after coupon is :: "+remaining_rev_amt)

		String tax_rate_qry="Select distinct tax_rate from ARIACORE.GL_TAX_DETAIL where invoice_no IN ("+invoice_No+")"
		ResultSet rs_tax_rate_qry = db.executePlaQuery(tax_rate_qry)
		while(rs_tax_rate_qry.next())
		{
			def tax_rate=rs_tax_rate_qry.getObject(1)
			tax_rate_list.add(tax_rate)
		}

		logi.logInfo("tax_rate_list :: "+tax_rate_list)
		def tempVal =0
		for(int j=0;j<tax_rate_list.size();j++)
		{
			cal_tax = remaining_rev_amt  * tax_rate_list.get(j)
			tempVal = tempVal+cal_tax
		}
		total_tax = remaining_rev_amt - cal_tax
		logi.logInfo("Remaining reversible amount after coupon and tax is :: "+total_tax)

		def tfinalAmt = ((total_tax.toDouble()*100).toInteger()/100).toString()

		return tfinalAmt
	}

	public String md_GET_REV_AMT_AFTER_COUPON_CR0(String s,LinkedHashMap seq_no)
	{
		def tax_rate_list = []
		def cal_tax
		def total_tax=0

		logi.logInfo("Entering in to method  ::md_GET_REV_AMT_AFTER_COUPON ")
		String account_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String invoice_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String seq_num
		def coupon_amt
		def actual_line_amt
		def remaining_rev_amt

		String coupon_amt_query="SELECT SEQ_NUM as SEQ_NO,SUM(DISTILLED_AMOUNT) as COUPON_AMT FROM (SELECT ELIGIBLE_SERVICE_NO,DISTILLED_AMOUNT FROM ariacore.all_recur_credit WHERE acct_no =    (SELECT acct_no FROM ariacore.gl WHERE invoice_no="+invoice_No+") UNION SELECT CDR.KEY_2_VAL AS ELIGIBLE_SERVICE_NO,  CASE WHEN CDRS.INLINE_OFFSET_IND='I' THEN 0      ELSE AIDD.ACTUAL_DISCOUNT_AMT END AS DISTILLED_AMOUNT FROM ARIACORE.CLIENT_DR_COMPONENTS CDR JOIN ariacore.all_invoice_discount_details AIDD ON AIDD.rule_no=CDR.rule_no JOIN ariacore.CLIENT_DISCOUNT_RULES CDRS ON CDRS.rule_no =AIDD.rule_no WHERE AIDD.invoice_no ="+invoice_No+" )a join ariacore.gl_detail gd on gd.service_no=a.ELIGIBLE_SERVICE_NO and gd.invoice_no="+invoice_No+" AND gd.ORIG_COUPON_CD IS NULL GROUP BY SEQ_NUM"
		ConnectDB db = new ConnectDB()
		ResultSet rs = db.executePlaQuery(coupon_amt_query);
		while (rs.next()){
			seq_num=rs.getString("SEQ_NO")
			coupon_amt=rs.getObject("COUPON_AMT")
			logi.logInfo "Coupon applied sequence number is "+seq_num
			logi.logInfo "Coupon amount applied for the above sequence number is "+coupon_amt
		}
		String actual_line_amt_query="SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_No+" AND SEQ_NUM="+seq_num
		actual_line_amt=db.executeQueryP2(actual_line_amt_query)
		logi.logInfo "Actual line item amount before coupon is "+actual_line_amt

		/*String tax_rate_qry="Select distinct tax_rate from ARIACORE.GL_TAX_DETAIL where invoice_no IN ("+invoice_No+")"
		 ResultSet rs_tax_rate_qry = db.executePlaQuery(tax_rate_qry)
		 while(rs_tax_rate_qry.next())
		 {
		 def tax_rate=rs_tax_rate_qry.getObject(1)
		 tax_rate_list.add(tax_rate)
		 }
		 logi.logInfo("tax_rate_list :: "+tax_rate_list)
		 def tempVal =0
		 for(int j=0;j<tax_rate_list.size();j++)
		 {
		 cal_tax = actual_line_amt.toDouble()  * tax_rate_list.get(j)
		 tempVal = tempVal+cal_tax
		 }
		 logi.logInfo("Tax amount alone is :: "+tempVal)
		 total_tax = actual_line_amt.toDouble() - tempVal
		 logi.logInfo("Remaining reversible amount after tax is :: "+total_tax)*/


		remaining_rev_amt = (actual_line_amt.toDouble() - coupon_amt.toDouble())
		logi.logInfo("Remaining reversible amount after coupon is :: "+remaining_rev_amt)

		def tfinalAmt = ((remaining_rev_amt.toDouble()*100).toInteger()/100).toString()

		return tfinalAmt
	}

	String setPaymentCreditAmountToZero(List clientIds){

		for (int cliList=0; cliList<clientIds.size(); cliList++){
			logi.logInfo ("Calling md_UPDATE_PAYMENT_DISCOUNT for client "+clientIds.get(cliList))
			update_payment_discount_flat_and_commit("1", "1", "0", "usd",clientIds.get(cliList))
		}
		return "No Val"
	}
	
	String md_setPaymentCreditAmountToZeroAfterCAC1(String testCaseId){

		ReadData rData = new ReadData()
		boolean sflag = false

		List clientIds = rData.getClientListP2()
		logi.logInfo("The list of client ids are :: "+clientIds)

		for (int cliList=0; cliList<clientIds.size(); cliList++){
			logi.logInfo ("Calling md_UPDATE_PAYMENT_DISCOUNT for client "+clientIds.get(cliList))
			//md_UPDATE_PAYMENT_DISCOUNT_param_+1,1,5,usd,3285073
			if(update_payment_discount_flat_and_commit("1", "1", "0", "usd",clientIds.get(cliList)) == "No Val"){
				logi.logInfo "All clients are reset with zero pay credit discount"
				sflag = true
			}
		}
		return "No Val"
	}

	String md_Update_DisabledIndicator_To_Account(String type){

		boolean successFlag = false

		String account_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')

		ConnectDB dbase = new ConnectDB()
		String updateQuery = "update ariacore.billing_info set disabled_ind ="+type+" where acct_no = "+account_No+" AND CLIENT_NO ="+client_no
		logi.logInfo "Executing Query" + updateQuery
		dbase.connection.commit();
		if(dbase.statement.executeUpdate(updateQuery) > 0){
			successFlag = true
			logi.logInfo("Updated the disabled indicator to 1")
		}
		dbase.connection.commit();
		return "No Val"
	}

	public String md_UPDATE_DISABLED_IND(String type, LinkedHashMap m) {
		logi.logInfo ("Calling md_UPDATE_PAYMENT_DISCOUNT")
		String methodCalled
		boolean successFlag = false

		String disabledInd

		logi.logInfo ("TYPE IS :: "+type)
		disabledInd = type.trim()
		logi.logInfo "Disabled indicator is  :: "+type
		methodCalled = md_Update_DisabledIndicator_To_Account(type)

		return ""
	}

	public String md_GET_RECORDED_USAGE_NO(String type,LinkedHashMap m)
	{
		logi.logInfo "Calling md_GET_RECORDED_USAGE_NO"
		String rec_no=vm.getValueFromResponse("record_usage."+type,ExPathRpcEnc.RECORD_USAGE_USAGERECNO1)
		logi.logInfo "Recorded usage no is "+ rec_no
		return rec_no
	}

	public String md_GET_DATE(String date_param,LinkedHashMap m)
	{
		logi.logInfo("parameter:"+date_param)
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		String dte
		ConnectDB db = new ConnectDB()
		if(date_param.equalsIgnoreCase("ACCT"))
		{
			String acct_no=m.get("account_no")
			String created_qry="Select TO_CHAR(created,'yyyy-MM-dd') from ariacore.acct where acct_no="+acct_no
			ResultSet rs_created_qry = db.executePlaQuery(created_qry)

			while(rs_created_qry.next())
			{
				dte=rs_created_qry.getString(1)
			}
			logi.logInfo("acc Date:"+dte)
		}

		else if(date_param.equalsIgnoreCase("VIRTUAL"))
		{

			String vt_qry = "Select TO_CHAR(ARIACORE.ARIAVIRTUALTIME("+client_no+"),'yyyy-MM-dd') from DUAL"
			ResultSet rs_vt_qry = db.executePlaQuery(vt_qry)

			while(rs_vt_qry.next())
			{
				dte=rs_vt_qry.getString(1)
			}
			logi.logInfo("virtual Date:"+dte)
		}

		else if(date_param.contains("VIRTUAL-"))
		{
			logi.logInfo("inside virtual- "+date_param)
			logi.logInfo("date_param.split-0"+ date_param.split("-")[0])
			logi.logInfo("date_param.split-1"+ date_param.split("-")[1])
			String dte_format = date_param.split("-")[1]
			logi.logInfo("dte_format:"+dte_format)
			String vt_qry = "Select TO_CHAR(ARIACORE.ARIAVIRTUALTIME("+client_no+"),'"+dte_format+"') from DUAL"
			ResultSet rs_vt_qry = db.executePlaQuery(vt_qry)

			while(rs_vt_qry.next())
			{
				dte=rs_vt_qry.getString(1)
			}

			logi.logInfo("Formatted Date:"+dte)
		}

		else if(date_param.contains("ACCT#"))
		{
			Date date1 = new Date();
			Calendar now = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			logi.logInfo("inside ACCT- "+date_param)
			logi.logInfo("date_param.split-0 "+ date_param.split("#")[0])
			logi.logInfo("date_param.split-1 "+ date_param.split("#")[1])
			String numberOfDays = date_param.split("#")[1]
			String acct_no=m.get("acct_no")
			logi.logInfo "Got account no from input hash " +acct_no
			String created_qry1="Select created from ariacore.acct where acct_no="+acct_no
			ResultSet rs_created_qry1 = db.executePlaQuery(created_qry1)
			logi.logInfo " Query executed"
			while(rs_created_qry1.next())
			{
				date1=rs_created_qry1.getDate(1)
				logi.logInfo " Date " + date1
			}
			now.setTime(date1)
			now.add(Calendar.DAY_OF_MONTH, Integer.parseInt(numberOfDays))
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			dte = format1.format(now.getTime()).toString()
		}

		else
		{
			logi.logInfo("else part")
			Date temp_date
			String vt_qry = "Select ARIACORE.ARIAVIRTUALTIME("+client_no+") from DUAL"
			ResultSet rs_vt_qry = db.executePlaQuery(vt_qry)

			while(rs_vt_qry.next())
			{
				temp_date=rs_vt_qry.getDate(1)
			}
			logi.logInfo("temp date:"+ temp_date)
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")
			Calendar now = Calendar.getInstance();
			now.setTime(temp_date);
			now.add(Calendar.DAY_OF_MONTH, date_param.toInteger());
			logi.logInfo("after adding days:"+sdf.format(now.getTime()).toString() )
			dte = sdf.format(now.getTime()).toString()

		}

		logi.logInfo("new Date:"+dte)

		return dte
	}

	public String md_Get_Usage_Parent_rec_no(String api_prefix,LinkedHashMap m)
	{
		logi.logInfo "Calling md_Get_Usage_Parent_rec_no"
		String api="record_usage"+"."+api_prefix
		logi.logInfo "Taking rec number from "+api
		String parent_rec_no=vm.getValueFromResponse(api,ExPathRpcEnc.RECORD_USAGE_USAGERECNO1)
		return parent_rec_no

	}
	public String md_GET_CLIENT_REC_ID(String type,LinkedHashMap m)
	{
		logi.logInfo "Calling md_GET_CLIENT_REC_ID"
		String client_rec_id=vm.getValueFromRequest("record_usage."+type,"//client_record_id")
		logi.logInfo "Client id recorded is "+ client_rec_id
		return client_rec_id
	}


	public String md_GET_USERID(String level,LinkedHashMap m)
	{
		logi.logInfo "Calling md_GET_USERID"
		String userid

		if(level.equalsIgnoreCase("parent"))
		{
			userid =vm.getValueFromResponse("create_acct_complete","//out_userid")
		}
		else if (level.equalsIgnoreCase("child"))
		{
			userid =vm.getValueFromResponse("create_acct_complete.a","//out_userid")
		}


		return userid
	}

	public void reset_All_Clients_Time_to_Zero(List clientIds){
		logi.logInfo "Calling reset_All_Clients_Time_to_Zero"
		boolean isSuccessful
		ConnectDB db = new ConnectDB()
		ClientParamUtils cpUtil = new ClientParamUtils()
		ResultSet rs
		String virtualTimeQry = "Select TO_CHAR (ARIACORE.ariavirtualtimestamp(%s), 'DD/MM/YYYY HH24:mi:ss') from dual"

		SimpleDateFormat parseFormat = new SimpleDateFormat("DD/MM/YYYY HH:mm:ss")
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH")
		String current_date
		String current_date_after_update

		logi.logInfo("The client ids selected to set zero date are :: "+clientIds.toString())

		for(int clId = 0; clId<clientIds.size(); clId++){
			rs = db.executePlaQuery(String.format(virtualTimeQry,clientIds.get(clId)))

			if(rs.next()){
				current_date = rs.getString(1)
			}
			logi.logInfo "date from database for client "+clientIds.get(clId) +" is : "+current_date
			Date date = parseFormat.parse(current_date)
			String currentTime = timeFormat.format(date)
			logi.logInfo "current time for client "+clientIds.get(clId) +" is : "+currentTime

			int diffTime = 24-currentTime.toInteger()+13
			logi.logInfo "current time differnence for client "+clientIds.get(clId) +" is : "+diffTime

			cpUtil.advanceOffsetHours(clientIds.get(clId), diffTime)

			current_date_after_update = db.executeQueryP2(String.format(virtualTimeQry,clientIds.get(clId)))
			logi.logInfo "date from database for client "+clientIds.get(clId) +" after time update is : "+current_date_after_update
		}

	}
	
	public String md_specific_charge_transaction_id()
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String account_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		
		String result;
		String query = "SELECT Event_no from ariacore.acct_transaction where acct_no=" +account_No+ "and transaction_type=2"
		logi.logInfo "query "+query
		ConnectDB dbConnection = new ConnectDB()
		result = dbConnection.executeQueryP2(query)
		dbConnection.closeConnection()

		return result

	}

	//Key APIs - Record External Payment
	public String md_specific_charge_transaction_id(String type,LinkedHashMap m)
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String account_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String result;
		logi.logInfo "Type: " +type

		String query = "select event_no from (SELECT event_no,row_number() over (order by event_no ) as seqnum from ariacore.acct_transaction where acct_no=" +account_No+ " and transaction_type=1) where seqnum=" +type
		logi.logInfo "query "+query
		ConnectDB dbConnection = new ConnectDB()
		result = dbConnection.executeQueryP2(query)

		dbConnection.closeConnection()

		return result

	}

	String md_payment_amount_transaction(String type,LinkedHashMap m)
	{
		logi.logInfo(type)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB dbConnection = new ConnectDB()
		String account_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		//String transction_query = "SELECT EVENT_NO FROM ARIACORE.acct_transaction WHERE ACCT_NO=" +account_No+ "AND TRANSACTION_TYPE=1 ORDER BY EVENT_NO DESC"

		//String transaction =dbConnection.executeQueryP2(transction_query)
		//logi.logInfo "EventNo is : "+transaction
		String query = "SELECT SUM(AMOUNT) FROM ARIACORE.acct_transaction WHERE ACCT_NO= "+account_No+ " AND TRANSACTION_TYPE=1"
		logi.logInfo "Amount is " +query

		String amount = dbConnection.executeQueryP2(query)
		dbConnection.closeConnection()
		if(type==2)
		{
			return (amount.toInteger()*2).toString()
		}

		return amount
	}

	String md_payment_totalamount(String type,LinkedHashMap m)
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String account_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String result;
		String query = "SELECT BALANCE FROM ARIACORE.ACCT_BALANCE WHERE ACCT_NO= "+account_No+ " AND CLIENT_NO = "+ clientNo
		logi.logInfo "query "+query
		ConnectDB dbConnection = new ConnectDB()
		String amount = dbConnection.executeQueryP2(query)
		dbConnection.closeConnection()

		return amount
	}

	String md_void_transction_id(String type,LinkedHashMap m)
	{
		logi.logInfo "Inside md_void_transction_id "
		logi.logInfo "Type "+type
		String account_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String query ="select event_no from (SELECT event_no,row_number() over (order by event_no ) as seqnum from ariacore.acct_transaction where acct_no=" +account_No+ " and transaction_type=2) where seqnum=" +type

		ConnectDB dbConnection=new ConnectDB()
		String transaction_id=dbConnection.executeQueryP2(query)
		dbConnection.closeConnection()
		return transaction_id

	}

	String md_GET_PENDING_INVOICE_NO(String txt,LinkedHashMap m)

	{
		logi.logInfo ("Inside md_GET_PENDING_INVOICE_NO")
		String invoiceno
		String acct_no
		ConnectDB db = new ConnectDB()
		if(txt.contains("DB"))
		{
			if(txt.contains("."))
			{
				String pref=txt.split("\\.")[1]
				logi.logInfo("pref:"+pref)
				String api="create_acct_complete.".concat(pref)
				acct_no=vm.getValueFromResponse(api,ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			}

			else
			{
				acct_no=vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			}
			String query="SELECT invoice_no from ariacore.gl where acct_no="+acct_no+" and rownum=1 order by bill_date desc "

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next())
			{
				invoiceno = resultSet.getString(1);
			}
			resultSet.close();
		}

		else if(txt.contains("inv"))
		{
			acct_no=vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			invoiceno=db.executeQueryP2("select invoice_no from ariacore.gl_pending where acct_no="+acct_no).toString()
		}

		else
		{
			invoiceno=vm.getValueFromResponse(txt,ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		}

		logi.logInfo ("Invoice No: "+invoiceno)
		return invoiceno

	}
	public void reset_client_clock_to_particular_day(String clientId, String day, String month){
		logi.logInfo "Calling reset_client_clock_to_particular_day..."
		boolean isSuccessful
		ConnectDB db = new ConnectDB()
		ClientParamUtils cpUtil = new ClientParamUtils()
		ResultSet rs
		String virtualTimeQry = "Select TO_CHAR (ARIACORE.ariavirtualtimestamp(%s), 'DD/MM/YYYY HH24:mi:ss') from dual"

		logi.logInfo("The client ids selected to set new date :: "+clientId)


		rs = db.executePlaQuery(String.format(virtualTimeQry,clientId))
		String current_date
		if(rs.next()){
			current_date = rs.getString(1)
		}

		logi.logInfo "Current date from database for client "+clientId +" is : "+current_date

		String daystoadv = db.executeQueryP2("select (to_date(concat('"+ day +"/"+month + "/'"+", to_char((EXTRACT(YEAR FROM trunc(ARIACORE.ARIAVIRTUALTIMEstamp("+clientId+")))+1))), 'DD/MM/YYYY') - trunc(ARIACORE.ARIAVIRTUALTIMEstamp("+clientId+"))) * 24 as days from dual")

		logi.logInfo "Hrs to advance for "+clientId +" is : "+daystoadv
		cpUtil.advanceOffsetHours(clientId, daystoadv.toLong())

		String current_date_after_update = db.executeQueryP2(String.format(virtualTimeQry,clientId))
		logi.logInfo "New date set for the client "+clientId +" after time update is : "+current_date_after_update
	}

	String md_GetInvoiceTransId(String type,LinkedHashMap m) {

		logi.logInfo ("Calling md_GetInvoiceTransId")
		def inv_tran_id
		if (type == "DB")
		{
			def acct_no=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String query= "SELECT event_no FROM (SELECT event_no,row_number() over(order by create_date DESC) AS seqno from ariacore.acct_transaction WHERE transaction_type=1 AND acct_no ="+acct_no+") WHERE seqno=1"
			ConnectDB db = new ConnectDB()
			ResultSet resultSet = db.executePlaQuery(query);

			if (resultSet.next()){

				inv_tran_id = resultSet.getObject(1);
				resultSet.close()
			}
		}
		else if (type == "CHILD")
		{
			def acct_no=vm.getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String query= "SELECT event_no FROM (SELECT event_no,row_number() over(order by create_date DESC) AS seqno from ariacore.acct_transaction WHERE transaction_type=1 AND acct_no ="+acct_no+") WHERE seqno=1"
			ConnectDB db = new ConnectDB()
			ResultSet resultSet = db.executePlaQuery(query);

			if (resultSet.next()){

				inv_tran_id = resultSet.getObject(1);
				resultSet.close()
			}
		}
		else if (type == "WRITEOFF")
		{
			def acct_no=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String query= "SELECT event_no FROM (SELECT event_no,row_number() over(order by create_date DESC) AS seqno from ariacore.acct_transaction WHERE transaction_type=6 AND acct_no ="+acct_no+") WHERE seqno=1"
			ConnectDB db = new ConnectDB()
			ResultSet resultSet = db.executePlaQuery(query);

			if (resultSet.next()){

				inv_tran_id = resultSet.getObject(1);
				resultSet.close()
			}
		}
		logi.logInfo "Invoice Transaction ID: " + inv_tran_id.toString()
		return inv_tran_id
	}

	String md_GET_STANDING_USAGE_RECNO(String type,LinkedHashMap<String,String> m)
	{
		logi.logInfo "Calling md_getStandingUsageId"
		String standingRecNo
		String acct_no
		if(type.equals("parent")){
			acct_no = vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		} else if(type.equals("child")){
			acct_no = vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		}

		ConnectDB db = new ConnectDB()
		String query = "SELECT standing_usage_no from ariacore.standing_usage where acct_no ="+acct_no
		standingRecNo = db.executeQueryP2(query)
		return standingRecNo
	}

	public String md_GET_BALANCE_AMT(String type,LinkedHashMap<String,String> m)
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String acct_no=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String amount="";
		ConnectDB db = new ConnectDB();
		
		String due_mode = db.executeQueryP2("Select param_val from ariacore.aria_client_params where param_name='AUTO_COLLECT_BALANCE_SCOPE' and client_no="+clientNo)
		//String due_mode = db.executeQueryP2("Select param_val from ariacore.aria_client_params where param_name='AUTO_COLLECT_ACCT_BALANCE_SCOPE' and client_no="+clientNo)

		if(due_mode.equals("F"))
		{
			logi.logInfo("Due balance mode is FULL")
			amount = db.executeQueryP2("Select balance from ariacore.acct_balance where acct_no="+acct_no).toString()

		}
		else if(due_mode.equals("C"))
		{
			logi.logInfo("Due balance mode is Current")
			amount = db.executeQueryP2("Select debit from ariacore.gl where acct_no="+acct_no+" and invoice_no = (Select min(invoice_no) from ariacore.gl where acct_no="+acct_no+")").toString()
		}

		return amount;
	}



	def md_User_Id(String type,LinkedHashMap<String,String> m)
	{
		logi.logInfo("calling md_GET_ACCT_NO ")
		def userid
		logi.logInfo("type:"+type)
		if(type.equalsIgnoreCase("parent")==true)
		{
			logi.logInfo("Parent acctno:")
			userid=vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_OUTUSERID1)
		}
		else if(type.equalsIgnoreCase("child")==true)
		{
			userid=vm.getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_OUTUSERID1)
		}
		else if(type.equalsIgnoreCase("Valid")==true)
		{
			userid=vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_OUTUSERID1)
			def client_no = vm.getValueFromRequest("create_acct_complete","//client_no")
			userid = db.executeQueryP2("select userid from ariacore.acct where userid not in('"+userid+"') and client_no="+client_no).toString()
		}


		else
			userid = "InvalidUserid"

		return userid
	}

	def md_Client_Acct_Id(String type,LinkedHashMap<String,String> m)
	{
		logi.logInfo("calling md_GET_ACCT_NO ")
		def userid
		logi.logInfo("type:"+type)

		if(type.equalsIgnoreCase("parent")==true)
		{
			logi.logInfo("Parent acctno:")
			userid=vm.getValueFromRequest("create_acct_complete","//client_acct_id")
		}
		else if(type.equalsIgnoreCase("child")==true)
		{
			userid=vm.getValueFromRequest("create_acct_complete.a","//client_acct_id")
		}
		else if(type.equalsIgnoreCase("child2")==true)
		{
			userid=vm.getValueFromRequest("create_acct_complete.b","//client_acct_id")
		}
		else if(type.equalsIgnoreCase("Valid")==true)
		{
			userid=vm.getValueFromRequest("create_acct_complete","//client_acct_id")
			def client_no = vm.getValueFromRequest("create_acct_complete","//client_no")
			userid = db.executeQueryP2("select client_acct_id from ariacore.acct where client_acct_id not in('"+userid+"') and client_no="+client_no).toString()
		}
		else
			userid = "InvalidClientAcctid"

		return userid
	}
	

	public void reset_All_Clients_Time_to_Current(List clientIds){
		logi.logInfo "Calling reset_All_Clients_Time_to_Current"
		boolean isSuccessful
		ConnectDB db = new ConnectDB()
		ClientParamUtils cpUtil = new ClientParamUtils()
		ResultSet rs
		String virtualTimeQry = "Select TO_CHAR (ARIACORE.ariavirtualtimestamp(%s), 'DD/MM/YYYY HH24:mi:ss') from dual"

		SimpleDateFormat parseFormat = new SimpleDateFormat("DD/MM/YYYY HH:mm:ss")
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH")
		String current_date
		String current_date_after_update

		

		for(int clId = 0; clId<clientIds.size(); clId++){
			rs = db.executePlaQuery(String.format(virtualTimeQry,clientIds.get(clId)))

			if(rs.next()){
				current_date = rs.getString(1)
			}


			cpUtil.advanceOffsetHoursToCurrentTime(clientIds.get(clId))

			current_date_after_update = db.executeQueryP2(String.format(virtualTimeQry,clientIds.get(clId)))
			logi.logInfo "date from database for client "+clientIds.get(clId) +" after time update is : "+current_date_after_update
		}

	}
	
	String md_get_trans_id(String type,LinkedHashMap<String,String> m)
	{
		logi.logInfo "Inside md_get_trans_id "
		logi.logInfo "Type "+type
		String account_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String query ="select event_no from (SELECT event_no,row_number() over (order by event_no ) as seqnum from ariacore.acct_transaction where acct_no=" +account_No+ " and transaction_type=1) where seqnum=" +type

		ConnectDB dbConnection=new ConnectDB()
		String transaction_id=dbConnection.executeQueryP2(query)
		dbConnection.closeConnection()
		return transaction_id

	}
	
	String md_get_payment_sum(String type,LinkedHashMap m)
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB dbConnection = new ConnectDB()
		String account_No =   vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		//String transction_query = "SELECT EVENT_NO FROM ARIACORE.acct_transaction WHERE ACCT_NO=" +account_No+ "AND TRANSACTION_TYPE=1 ORDER BY EVENT_NO DESC"

		//String transaction =dbConnection.executeQueryP2(transction_query)
		//logi.logInfo "EventNo is : "+transaction
		String query = "SELECT SUM(AMOUNT) FROM ARIACORE.acct_transaction WHERE ACCT_NO= "+account_No+ " AND TRANSACTION_TYPE=1"
		logi.logInfo "Amount is " +query

		String amount = dbConnection.executeQueryP2(query)
		dbConnection.closeConnection()
		double amt=amount.toDouble()
		logi.logInfo "The amount is " +amt.toString()
		return amt.toString()
	}
	
	public String md_get_charge_transaction_id(String type,LinkedHashMap m)
	{
		def accountNo=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			
			if(accountNo =='NoVal')
			{
				   accountNo = vm.getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
			}
		String result;
		String query = "SELECT max(Event_no) from ariacore.acct_transaction where acct_no=" +accountNo+ "and transaction_type=2"
		logi.logInfo "query "+query
		ConnectDB dbConnection = new ConnectDB()
		result = dbConnection.executeQueryP2(query)
		dbConnection.closeConnection()

		return result

	}
	
	String md_GET_CHILD_PAYMENT_AMT(String mode,LinkedHashMap m) {
		logi.logInfo ("Calling md_GET_CHILD_PAYMENT_AMT")
		logi.logInfo ("Payment Mode: "+mode)
		String type
		String type_prefix
		String line_no
		if (mode.contains(","))
		{
			type=mode.split("\\,")[0]
			type_prefix=mode.split("\\,")[1]
			line_no=type.replace('INVL', '')
		}
		else
		{

			type=mode
			type_prefix=mode
		}

		def acct_no=vm.getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)

		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo ("Getting Invoice Amount for account: "+ acct_no)
		String query="SELECT  NVL((SUM(amount)-SUM(applied_amount)),0) AS amount_to_be_paid FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO ="+acct_no+ " AND client_no="+client_no+ " AND TRANSACTION_TYPE IN (1,4) "
		String inv_line_tem_query="select debit from  ARIACORE.gl_detail where invoice_no=(SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+" AND CLIENT_NO="+client_no+") "+
				" and seq_num="+line_no

		def acct_balance
		String payment_amount

		String strQuery

		ConnectDB db = new ConnectDB()

		// Collection Group
		String query_collection_group="SELECT  AMOUNT AS amount_to_be_paid FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO ="+acct_no+ " AND client_no="+client_no+ " AND TRANSACTION_TYPE IN (3) "
		ResultSet rscg = db.executePlaQuery(query_collection_group);
		def cg_paid_value

		if (rscg.next())
			cg_paid_value = rscg.getObject(1)
		rscg.close()
		
		if(type.contains('INVL')==false)
		{
			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next()){

				acct_balance = resultSet.getObject(1)
			}
			if (cg_paid_value != null)
				acct_balance = cg_paid_value
			logi.logInfo "Invoice amount: " + acct_balance.toString()
			resultSet.close()
		}
		if(type.contains('TAXL')==true)
		{
			line_no=type.replace('TAXL', '')
			String tquery="select NVL(debit,0) + (select NVL(sum(debit),0)  from ariacore.gl_tax_detail where invoice_no=(SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+" AND CLIENT_NO="+client_no+") and taxed_seq_num="+line_no+") as LINE_AMT_AFTER_TAX from ariacore.gl_detail where client_no="+client_no+ "and seq_num="+line_no+" and  invoice_no=(SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+" AND CLIENT_NO="+client_no+")"
			ResultSet resultSet = db.executePlaQuery(tquery);
			if (resultSet.next()){

				acct_balance = resultSet.getObject(1)
				logi.logInfo "Invoice amount with tax: " + acct_balance.toString()
			}
			resultSet.close()
		}

		if(type.contains('INVL'))
		{
			ResultSet resultSet_lineitem = db.executePlaQuery(inv_line_tem_query);
			if (resultSet_lineitem.next()){

				acct_balance = resultSet_lineitem.getObject(1)
				logi.logInfo "Invoice Line Item " + line_no + " amount: " + acct_balance.toString()
			}
			resultSet_lineitem.close()
		}




		if(type_prefix.contains("+"))
		{

			def t=type_prefix.replace("+", "")
			t=t.toInteger()
			payment_amount=(acct_balance +t ).toString()
		}
		else if(type_prefix.contains("-"))
		{
			def t=type_prefix.replace("-", "")
			t=t.toInteger()
			payment_amount=(acct_balance - t).toString()
		}

		else if(type_prefix.contains("%"))
		{

			def t=type_prefix.replace("%", "")
			if(t.contains('.'))
				t=t.toDouble()
			else
				t=t.toInteger()
			payment_amount=((acct_balance*t)/100).toString()
		}

		else if(type_prefix.equals("INV"))
		{

			payment_amount=acct_balance.toString()
		}
		else if(type_prefix.contains("TAXL"))
		{

			payment_amount=acct_balance.toString()
		}

		else
		{
			payment_amount=""
		}

		//db.closeConnection();
		logi.logInfo("Amount to pay as per the test case: " +payment_amount)
		return payment_amount


	}
	
	String md_GET_CHILD_TRANSACTION_ID(String type,LinkedHashMap m) {
		logi.logInfo ("Calling md_GET_CHILD_TRANSACTION_ID")

		if (type == "DB")
		{
			//select EVENT_NO from ariacore.acct_transaction where acct_no = 10480316 and transaction_type=3
			def acct_no=vm.getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String query= "SELECT event_no FROM (SELECT event_no,row_number() over(order by create_date DESC) AS seqno from ariacore.acct_transaction WHERE transaction_type=3 AND acct_no ="+acct_no+") WHERE seqno=1"
			ConnectDB db = new ConnectDB()
			ResultSet resultSet = db.executePlaQuery(query);
			def tran_id
			if (resultSet.next()){

				tran_id = resultSet.getObject(1);
				resultSet.close()
			}
			else
			{
				tran_id = "No value returned"
			}
			logi.logInfo "Electronic Payment Transaction ID: " + tran_id.toString()
			return tran_id
		}
		else if(type == "INV")
		{
			def acct_no=vm.getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String query= "SELECT event_no FROM (SELECT event_no,row_number() over(order by create_date DESC) AS seqno from ariacore.acct_transaction WHERE transaction_type=1 AND acct_no ="+acct_no+") WHERE seqno=1"
			ConnectDB db = new ConnectDB()
			ResultSet resultSet = db.executePlaQuery(query);
			def tran_id
			if (resultSet.next()){

				tran_id = resultSet.getObject(1);
				resultSet.close()
			}
			else
			{
				tran_id = "No value returned"
			}
			logi.logInfo "Electronic Payment Transaction ID: " + tran_id.toString()
			return tran_id
		}

		else if (type == "DBASC")
		{
			def acct_no=vm.getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			String query= "SELECT event_no FROM (SELECT event_no,row_number() over(order by create_date ASC) AS seqno from ariacore.acct_transaction WHERE transaction_type=3 AND acct_no ="+acct_no+") WHERE seqno=1"
			ConnectDB db = new ConnectDB()
			ResultSet resultSet = db.executePlaQuery(query);
			def tran_id
			if (resultSet.next()){

				tran_id = resultSet.getObject(1);
				resultSet.close()
			}
			else
			{
				tran_id = "No value returned"
			}
			logi.logInfo "Electronic Payment Transaction ID: " + tran_id.toString()
			return tran_id
		}
		else
		{
			logi.logInfo ("Payment Type: " + type)

			String s_name=type.split(":")[0]
			String type1=type.split(":")[1]

			String api=s_name+'.'+type1
			logi.logInfo ("API: "+api)
			def tran_id=vm.getValueFromResponse(api,"//transaction_id")
			logi.logInfo (" Transaction ID taken for this transaction: "+tran_id)
			//String query= "select balance from ariacore.acct_balance where acct_no="+acct_no.toString()



			return tran_id
		}


	}
	
	String md_GET_CHILD_TRANSACTION_AMT(String type, LinkedHashMap m) {
		logi.logInfo ("Calling md_GET_CHILD_TRANSACTION_AMT")
		ConnectDB db = new ConnectDB()
		//lit:record_external_payment[1]/payment_amount[1]

		logi.logInfo ("Payment Type: "+type)
		String api=null
		String prefix_api=type.split("\\,")[0]
		String type_prefix=type.split("\\,")[1]
		if(type.contains(prefix_api))
		{
			api="record_external_payment"+'.'+prefix_api
		}

		logi.logInfo("Payment amount from record external api request" +vm.getValueFromRequest(api, "//*/*:payment_amount[1]").toString())
		String paid_amt_from_input=vm.getValueFromRequest(api, "//*/*:payment_amount[1]").toString()
		def tran_id=vm.getValueFromResponse(api,"//transaction_id")
		logi.logInfo ("Transaction id taken for account: "+tran_id)
		//String query= "select balance from ariacore.acct_balance where acct_no="+acct_no.toString()
		def paid_amt
		String to_refund_amt
		def refunded_amt
		def total_amt
		def inv_amt
		def invasc_amt
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=vm.getValueFromResponse('create_acct_complete.a',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)

		if(tran_id.toString() == "NoVal"){
			logi.logInfo("Getting transaction id for collection group payment")
			String collectionTransQry = "select event_no from ariacore.acct_transaction where acct_no = "+acct_no+" and transaction_type =3"

			ResultSet resultSet_transId = db.executePlaQuery(collectionTransQry);
			if (resultSet_transId.next()){
				tran_id = resultSet_transId.getObject(1);
				logi.logInfo("Transaction id taken for collection group : "+tran_id)
			}
			resultSet_transId.close()

		}

		String query_refunded_amount="select nvl(sum(amount),0) from ariacore.check_refund where acct_no="+acct_no

		String query_total_amount= "select sum(act.APPLIED_AMOUNT) from  ARIACORE.ACCT_TRANSACTION act "+
				" where act.acct_no="+acct_no+" and  act.TRANSACTION_TYPE in(2,3) and act.client_no="+client_no+" and act.event_no="+tran_id

		String query_inv_amount="SELECT amount AS INVOICE_AMOUNT FROM (SELECT amount,row_number() over (order by create_date DESC) AS seqno FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+" AND TRANSACTION_TYPE = 1) where seqno=1"



		ResultSet resultSet_refund = db.executePlaQuery(query_refunded_amount);
		if (resultSet_refund.next())
		{
			refunded_amt = resultSet_refund.getObject(1);
			resultSet_refund.close()
		}


		ResultSet resultSet_total = db.executePlaQuery(query_total_amount);
		if (resultSet_total.next()){
			total_amt = resultSet_total.getObject(1);
			resultSet_total.close()
		}

		ResultSet resultSet_inv = db.executePlaQuery(query_inv_amount);
		if (resultSet_inv.next()){
			inv_amt = resultSet_inv.getObject(1);
			resultSet_inv.close()
		}


		if (total_amt != null && refunded_amt != null)
			paid_amt=total_amt-refunded_amt


		logi.logInfo("paid_amt is :: "+paid_amt.toString())
		if(type_prefix.contains("+"))
		{

			to_refund_amt=(paid_amt + type_prefix.replace("+", "").toInteger()).toString()
		}
		else if(type_prefix.contains("-"))
		{
			to_refund_amt=(paid_amt - type_prefix.replace("-", "").toInteger()).toString()
		}

		else if(type_prefix.contains("%"))
		{
			def t=type_prefix.replace("%", "")
			if(t.contains('.'))
			{
				t=t.toDouble()
				to_refund_amt=((paid_amt*t)/100).toString()
			}

			else
			{
				t=t.toInteger()
				to_refund_amt=(((paid_amt*100*t).toInteger())/10000).toString()
			}



		}

		else if(type_prefix.contains("PAY"))
		{
			to_refund_amt=paid_amt.toString()
		}
		else if(type_prefix.contains("INV"))
		{
			to_refund_amt=inv_amt.toString()
		}
		else if(type_prefix.contains("ASC"))
		{
			String qry_amt="SELECT amount AS INVOICE_AMOUNT FROM (SELECT amount,row_number() over (order by create_date ASC) AS seqno FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO ="+acct_no+" AND client_no ="+client_no+" AND TRANSACTION_TYPE = 1) where seqno=1"
			ResultSet rs_qry_amt = db.executePlaQuery(qry_amt);
			while(rs_qry_amt.next())
			{
				invasc_amt = rs_qry_amt.getObject(1)
			}

			to_refund_amt=invasc_amt.toString()
		}
		else if(type_prefix.contains("EXTERNAL"))
		{
			to_refund_amt=paid_amt_from_input.toString()
		}


		else
		{
			to_refund_amt=""
		}

		//db.closeConnection();
		logi.logInfo("Amount to refund: " +to_refund_amt)
		return to_refund_amt

	}
	String md_GET_CHILD_INV_NO(String txt,LinkedHashMap m)
	
	{
			logi.logInfo ("Inside md_GET_CHILD_INV_NO")
			String invoiceno
			String acct_no
			if(txt.contains("DB"))
			{
				if(txt.contains("."))
				{
					String pref=txt.split("\\.")[1]
					logi.logInfo("pref:"+pref)
					String api="create_acct_complete.".concat(pref)
					acct_no=vm.getValueFromResponse(api,ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
				}
	
				else
				{
					acct_no=vm.getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
				}
				String query="SELECT invoice_no from ariacore.gl where acct_no="+acct_no+" and rownum=1 order by bill_date desc "
				ConnectDB db = new ConnectDB()
				ResultSet resultSet = db.executePlaQuery(query);
				if (resultSet.next())
				{
					invoiceno = resultSet.getString(1);
				}
				resultSet.close();
			}
			else
			{
				invoiceno=vm.getValueFromResponse(txt,ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
			}
	
			logi.logInfo ("Invoice No: "+invoiceno)
			return invoiceno
	
	}
	String md_GET_CHILD_LINE_NUMBER(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_CHILD_LINE_NUMBER")
		logi.logInfo("parameter"+type)
		String lineno
		if(type.equals("COUPON"))
		{
			String c_code=vm.getValueFromRequest("create_acct_complete.a", "//*/*:coupon_codes[1]").toString()
			logi.logInfo("Coupon code Applied: "+c_code)
			String inv_no = vm.getValueFromResponse("create_acct_complete.a",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
			logi.logInfo("Generated Invoice No: " + inv_no)
			String query= "Select seq_num from ARIACORE.gl_detail where invoice_no="+inv_no+" and comments Like '%"+c_code+"%'"
			//Alternate query - Select gld.seq_NUM from ARIACORE.gl_detail gld join ariacore.gl_credit_detail glcd on gld.invoice_no=glcd.invoice_no and gld.seq_num=glcd.gl_dtl_credit_seq_num where gld.invoice_no=12224941

			ConnectDB db = new ConnectDB()

			ResultSet resultSet = db.executePlaQuery(query);
			if (resultSet.next())
			{
				lineno = resultSet.getString(1);
			}
			logi.logInfo("Coupon Line Number in the Invoice:"+lineno)
		}
		else if(type.equals("USAGE"))
		{
			String inv
			String use_type=vm.getValueFromRequest("record_usage", "//usage_type").toString()
			String acc_no=vm.getValueFromRequest("record_usage", "//acct_no").toString()
			String inv_qry="SELECT invoice_no from ariacore.gl where acct_no="+acc_no+"and rownum=1 order by bill_date desc"
			ConnectDB db = new ConnectDB()
			ResultSet rs_inv_qry= db.executePlaQuery(inv_qry);
			while(rs_inv_qry.next())
			{
				inv=rs_inv_qry.getString(1)
			}
			String seq_qry="Select seq_num from ariacore.gl_detail where invoice_no="+inv+" and usage_type ="+use_type
			ResultSet rs_seq_qry= db.executePlaQuery(seq_qry);
			while(rs_seq_qry.next())
			{
				lineno=rs_seq_qry.getString(1)
			}

		}
		else if(type.equals("RECUR"))
		{
			
			String acct_no=vm.getValueFromResponse("create_acct_complete.a", "//acct_no").toString()
			String client_no=Constant.mycontext.expand('${Properties#Client_No}')
			
			ConnectDB db = new ConnectDB()
			String inv = db.executeQueryP2("Select max(invoice_no) from ariacore.gl where acct_no="+acct_no+" and client_no="+client_no)
			lineno= db.executeQueryP2("Select glt.seq_num from ariacore.gl_detail glt JOIN ariacore.services ser ON ser.service_no=glt.service_no where glt.invoice_no="+inv+" and glt.client_no=ser.custom_to_client_no and ser.recurring=1 and glt.client_no="+client_no +" and rownum=1")

		}


		return lineno

	}
	public String md_GET_SURCHARGE_RATE_SCHEDULE(String api,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_SURCHARGE_RATE_SCHEDULE")
		logi.logInfo("parameter"+api)
		String rateschno
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		rateschno=vm.getValueFromRequest(api, "//rate_schedule_no").toString()
		return rateschno
	}
	//_param_create_acc_complete
	
	//Getting plan number in form client identifier
	
	String md_GET_PLAN_NO(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_PLAN_NO")
		String planNo = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String planNum = "SELECT PLAN_NO FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_NO = "+client_no+" AND CLIENT_PLAN_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for plan no : "+planNum)
		ConnectDB db = new ConnectDB()
		planNo = db.executeQueryP2(planNum)
		logi.logInfo("Plan No Is  :::: "+planNo)
		return planNo
	}

	String md_GET_PLAN_NAME(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_PLAN_NAME")
		String planName = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String planQuery = "SELECT PLAN_NAME FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_NO = "+client_no+" AND CLIENT_PLAN_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for plan name : "+planQuery)
		ConnectDB db = new ConnectDB()
		planName = db.executeQueryP2(planQuery)
		logi.logInfo("Plan Name Is  :::: "+planName)
		return planName
	}
	
	
	String md_GET_SERVICE_NO(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_SERVICE_NO")
		String serviceNo = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String serviceNum = "SELECT SERVICE_NO FROM ARIACORE.CLIENT_SERVICE WHERE CLIENT_NO = "+client_no+" AND CLIENT_SERVICE_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for service no : "+serviceNum)
		ConnectDB db = new ConnectDB()
		serviceNo = db.executeQueryP2(serviceNum)
		logi.logInfo("service No Is  :::: "+serviceNo)
		return serviceNo
	}
	
	String md_GET_SERVICE_NAME(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_SERVICE_NAME")
		String serviceName = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String serviceNames = "SELECT SERVICE_NAME FROM ARIACORE.CLIENT_SERVICE WHERE CLIENT_NO = "+client_no+" AND CLIENT_SERVICE_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for service no : "+serviceNames)
		ConnectDB db = new ConnectDB()
		serviceName = db.executeQueryP2(serviceNames)
		logi.logInfo("service No Is  :::: "+serviceName)
		return serviceName
	}
	
	String md_GET_INVENTORY_NO(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_INVENTORY_NO")
		String itemNo = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String itemNum = "SELECT ITEM_NO FROM ARIACORE.INVENTORY_ITEMS WHERE CLIENT_NO = "+client_no+" AND CLIENT_ITEM_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for Inventory no : "+itemNum)
		ConnectDB db = new ConnectDB()
		itemNo = db.executeQueryP2(itemNum)
		logi.logInfo("Plan No Is  :::: "+itemNo)
		return itemNo
	}
	
	String md_GET_INVENTORY_NAME(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_INVENTORY_NAME")
		String itemName = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String itemNames = "SELECT LABEL FROM ARIACORE.INVENTORY_ITEMS WHERE CLIENT_NO = "+client_no+" AND CLIENT_ITEM_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for Inventory names : "+itemNames)
		ConnectDB db = new ConnectDB()
		itemName = db.executeQueryP2(itemNames)
		logi.logInfo("Plan No Is  :::: "+itemName)
		return itemName
	}
	
	String md_GET_INVENTORY_CLIENT_SKU(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_INVENTORY_CLIENT_SKU")
		String itemName = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String itemNames = "SELECT CLIENT_SKU FROM ARIACORE.INVENTORY_ITEMS WHERE CLIENT_NO = "+client_no+" AND CLIENT_ITEM_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for Inventory names : "+itemNames)
		ConnectDB db = new ConnectDB()
		itemName = db.executeQueryP2(itemNames)
		logi.logInfo("Plan No Is  :::: "+itemName)
		return itemName
	}
	
	String md_GET_PLANGROUP_NO(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_PLANGROUP_NO")
		String grpNo = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String groupNo = "SELECT GROUP_NO FROM ARIACORE.PLAN_CHANGE_GRP WHERE CLIENT_NO = "+client_no+" AND CLIENT_PLAN_CHANGE_GROUP_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for group no : "+groupNo)
		ConnectDB db = new ConnectDB()
		grpNo = db.executeQueryP2(groupNo)
		logi.logInfo("Group No Is  :::: "+grpNo)
		return grpNo
	}
	
	String md_GET_PLANGROUP_NAME(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_PLANGROUP_NAME")
		String grpName = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String groupName = "SELECT GROUP_NAME FROM ARIACORE.PLAN_CHANGE_GRP WHERE CLIENT_NO = "+client_no+" AND CLIENT_PLAN_CHANGE_GROUP_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for group name : "+groupName)
		ConnectDB db = new ConnectDB()
		grpName = db.executeQueryP2(groupName)
		logi.logInfo("Group Name Is  :::: "+grpName)
		return grpName
	}
	
	String md_GET_PROMOPLAN_NAME(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_PROMOPLAN_NAME")
		String ppName = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String keyName = "SELECT KEY_NAME FROM ARIACORE.PLAN_TYPES WHERE CLIENT_NO = "+client_no+" AND CLIENT_PLAN_TYPE_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for Promo Plan Name is : "+keyName)
		ConnectDB db = new ConnectDB()
		ppName = db.executeQueryP2(keyName)
		logi.logInfo("Promotional Plan Set Name Is  :::: "+ppName)
		return ppName
	}
	
	String md_GET_PROMOTION_NO(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_PROMOTION_NO")
		String proNo = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String keyNo = "SELECT PLAN_TYPE_NO FROM ARIACORE.promotions WHERE CLIENT_NO = "+client_no+" AND PROMO_CD = '"+client_identifier+"'"
		
		logi.logInfo("Query for Promo No is : "+keyNo)
		ConnectDB db = new ConnectDB()
		proNo = db.executeQueryP2(keyNo)
		logi.logInfo("Promotion No Is  :::: "+proNo)
		return proNo
	}
	
	String md_GET_CREDIT_NAME(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_CREDIT_NAME")
		String creditName = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String keyName = "SELECT LABEL FROM ARIACORE.RECURRING_CREDIT_TEMPLATES WHERE CLIENT_NO = "+client_no+" AND CLIENT_CREDIT_TEMPLATE_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for Credit Template Name is : "+keyName)
		ConnectDB db = new ConnectDB()
		creditName = db.executeQueryP2(keyName)
		logi.logInfo("Credit Template Name Is  :::: "+creditName)
		return creditName
	}
	
	String md_GET_CREDIT_NO(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_CREDIT_NAME")
		String creditName = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String keyName = "SELECT RECURRING_CREDIT_TEMPLATE_NO FROM ARIACORE.RECURRING_CREDIT_TEMPLATES WHERE CLIENT_NO = "+client_no+" AND CLIENT_CREDIT_TEMPLATE_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for Credit Template Name is : "+keyName)
		ConnectDB db = new ConnectDB()
		creditName = db.executeQueryP2(keyName)
		logi.logInfo("Credit Template Name Is  :::: "+creditName)
		return creditName
	}
	
	String md_GET_COA_CD(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_COA_CD")
		String coaCD = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String coaName = "SELECT COA_CODE FROM ARIACORE.CLIENT_CHART_OF_ACCOUNT WHERE CLIENT_NO = "+client_no+" AND COA_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for COA CD is: "+coaName)
		ConnectDB db = new ConnectDB()
		coaCD = db.executeQueryP2(coaName)
		logi.logInfo("COA CD Is  :::: "+coaCD)
		return coaCD
	}
	
	String md_GET_RULE_NO(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_RULE_NO")
		String ruleID = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String ruleQuery = "SELECT RULE_NO FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE CLIENT_NO = "+client_no+" AND CLIENT_RULE_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for Rule no is: "+ruleQuery)
		ConnectDB db = new ConnectDB()
		ruleID = db.executeQueryP2(ruleQuery)
		logi.logInfo("Rule no Is  :::: "+ruleID)
		return ruleID
	}
	
	String md_GET_BUNDLE_NO(String type,LinkedHashMap m)
	{
		 logi.logInfo("method md_GET_BUNDLE_NO")
		   String bundleID = null
		   String tcid= Constant.TESTCASEID
		   String client_no=Constant.lhmClientList.get(tcid)
		   String [] temp
		   //temp = type.split(":")
		   
		   logi.logInfo("Getting client no1 using client defined identifier : "+type)
		   //String client_no = type.split("\\_")[0]
		   //String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		   logi.logInfo(client_no)
		   //String client_identifier = type.split("\\_")[1]
		   String client_identifier = type
		   logi.logInfo(client_identifier)
		   logi.logInfo("Getting client no using client defined identifier : "+type)
		String bundleQuery = "SELECT BUNDLE_NO FROM ARIACORE.CLIENT_DISCOUNT_RULE_BUNDLES WHERE CLIENT_NO = "+client_no+" AND CLIENT_BUNDLE_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for Bundle ID is: "+bundleQuery)
		ConnectDB db = new ConnectDB()
		bundleID = db.executeQueryP2(bundleQuery)
		logi.logInfo("Bundle no Is  :::: "+bundleID)
		return bundleID
	}
	
	
	String md_GET_ORDER_SKU(String type,LinkedHashMap m)
	{
		   logi.logInfo("method md_GET_ORDER_SKU")
		   String planNo = null
		   String tcid= Constant.TESTCASEID
		   String client_no=Constant.lhmClientList.get(tcid)
		   String [] temp
		   //temp = type.split(":")
		   
		   logi.logInfo("Getting client no1 using client defined identifier : "+type)
		   //String client_no = type.split("\\_")[0]
		   //String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		   logi.logInfo(client_no)
		   //String client_identifier = type.split("\\_")[1]
		   String client_identifier = type
		   logi.logInfo(client_identifier)
		   logi.logInfo("Getting client no using client defined identifier : "+type)
		   String planNum = "select CLIENT_SKU from ariacore.inventory_items where CLIENT_NO = "+client_no+" AND CLIENT_ITEM_ID = '"+client_identifier+"'"
		   logi.logInfo("Queryyyyyyyy : "+planNum)
		   ConnectDB db = new ConnectDB()
		   planNo = db.executeQueryP2(planNum)
		   return planNo
	}

	
	String md_GET_PLAN_GROUP_NO(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_PLAN_NO")
		String plangrpNo = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String planGrpNum = "SELECT GROUP_NO FROM ARIACORE.PLAN_CHANGE_GRP WHERE CLIENT_NO = "+client_no+" AND CLIENT_PLAN_CHANGE_GROUP_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for plan no : "+planGrpNum)
		ConnectDB db = new ConnectDB()
		plangrpNo = db.executeQueryP2(planGrpNum)
		logi.logInfo("Plan No Is  :::: "+plangrpNo)
		return plangrpNo
	}
	
	
		
	String md_GET_RATE_SCHEDULE_NO(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_RATE_SCHEDULE_NO")
		String planRateSch = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String planRateSchedule = "SELECT DEFAULT_RATE_SCHED_NO FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_NO = "+client_no+" AND CLIENT_PLAN_ID = '"+client_identifier+"'"
		
		logi.logInfo("Query for plan rate schedule no : "+planRateSchedule)
		ConnectDB db = new ConnectDB()
		planRateSch = db.executeQueryP2(planRateSchedule)
		logi.logInfo("Plan RATE SCHEDULE No Is  :::: "+planRateSch)
		return planRateSch
	}
	
	
	String md_GET_PRODUCT_FIELD_NO(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_PRODUCT_FIELD_NO")
		String fieldNumber = null
		String tcid= Constant.TESTCASEID
		String client_no=Constant.lhmClientList.get(tcid)
		String [] temp
		//temp = type.split(":")
		
		logi.logInfo("Getting client no1 using client defined identifier : "+type)
		//String client_no = type.split("\\_")[0]
		//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(client_no)
		//String client_identifier = type.split("\\_")[1]
		String client_identifier = type
		logi.logInfo(client_identifier)
		logi.logInfo("Getting client no using client defined identifier : "+type)
		String fieldNo = "SELECT FIELD_NO FROM ARIACORE.CLIENT_OBJ_SUPP_FIELDS WHERE CLIENT_NO = "+client_no+" AND FIELD_NAME = '"+client_identifier+"'"
		
		logi.logInfo("Query for plan rate schedule no : "+fieldNo)
		ConnectDB db = new ConnectDB()
		fieldNumber = db.executeQueryP2(fieldNo)
		logi.logInfo("Plan RATE SCHEDULE No Is  :::: "+fieldNumber)
		return fieldNumber
	}
	public String md_Generate_cli_Receipt_Id(String level,LinkedHashMap m)
	{
		logi.logInfo "Calling md_Generate_cli_Receipt_Id"
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		//String tcid= Constant.TESTCASEID
		//String client_no=Constant.lhmClientList.get(tcid)
		logi.logInfo "Client _no " +client_no
		String client_receipt_id = null;
		library.GenericLibrary.Common c1 = new library.GenericLibrary.Common()
		switch(level){
			case "valid":
			client_receipt_id = "autoClientReceipId"+c1.getCurrentTime()
			client_receipt_id = client_receipt_id.replaceAll('_','')
			break;
			case "Invalid":
			client_receipt_id = "autoClientReceipId^%53@"+library.GenericLibrary.Common.getCurrentTime()
			client_receipt_id = client_receipt_id.replaceAll('_','')
			break;
			case "50":
			client_receipt_id = "automationreceiptIdentifierAutomatio"+library.GenericLibrary.Common.getCurrentTime()
			client_receipt_id = client_receipt_id.replaceAll('_','')
			break;
			case "Max":
			client_receipt_id = "automationreceiptIdentifierAutomation"+library.GenericLibrary.Common.getCurrentTime()
			client_receipt_id = client_receipt_id.replaceAll('_','')
			break;
			case "exist":
			String client_recp_id = "SELECT RECEIPT_ID FROM ARIACORE.ACCT_API_RECEIPTS WHERE CLIENT_NO = "+client_no+""
			ConnectDB db = new ConnectDB()
			client_receipt_id = db.executeQueryP2(client_recp_id)
			break;
			case "splChar":
			client_receipt_id = "autoClientReceipId-"+c1.getCurrentTime()
			client_receipt_id = client_receipt_id.replaceAll('_','')
			break;
			case "splChar>":
			client_receipt_id = "autoClientReceipId>"+c1.getCurrentTime()
			client_receipt_id = client_receipt_id.replaceAll('_','')
			break;
			case "splChar<":
			client_receipt_id = "autoClientReceipId<"+c1.getCurrentTime()
			client_receipt_id = client_receipt_id.replaceAll('_','')
			break;
		}
		logi.logInfo "Generated client Receipt Id: "+ client_receipt_id
		return client_receipt_id
	}
	
	String md_GET_TRANSACTION_ID_TYPE2(String type,LinkedHashMap m) {
		logi.logInfo ("Calling md_GET_TRANSACTION_ID_TYPE2")
		
		//select EVENT_NO from ariacore.acct_transaction where acct_no = 10480316 and transaction_type=2
		def acct_no=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String query= "Select Event_no from ariacore.acct_transaction where transaction_type=2 and acct_no="+acct_no+""
		ConnectDB db = new ConnectDB()
		ResultSet resultSet = db.executePlaQuery(query);
		def tran_id
		if (resultSet.next()){
		
		tran_id = resultSet.getObject(1);
		resultSet.close()
		}
		
		logi.logInfo "Electronic Payment Transaction ID: " + tran_id.toString()
		return tran_id
		
	}
	
	public String md_void_transaction_id_dunning(String type,LinkedHashMap m)
	{
		logi.logInfo "Inside md_void_transaction_id"
		def accountNo=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			
			if(accountNo =='NoVal')
			{
				   accountNo = vm.getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
			}
		String result,result1;
		String query = "SELECT Event_no from ariacore.acct_transaction where acct_no=" +accountNo+ "and transaction_type=2 order by event_no asc"
		logi.logInfo "query "+query
		ConnectDB dbConnection = new ConnectDB()
		result = dbConnection.executeQueryP2(query)
		
		String stmt_query="SELECT STATEMENT_NO FROM ARIACORE.ACCT_TRANSACTION WHERE EVENT_NO= "+result+ " AND ACCT_NO= "+accountNo
		String stmt_no=dbConnection.executeQueryP2(stmt_query)
		logi.logInfo "Statement noooooooooooo "+stmt_no
		if(stmt_no.toInteger()==-1)
		{
			logi.logInfo "Inside ifffffffffffffff"
			String query1 = "SELECT Event_no from ariacore.acct_transaction where acct_no=" +accountNo+ " and transaction_type=2 and statement_no not in -1"
			logi.logInfo "query "+query1
			result1 = dbConnection.executeQueryP2(query1)
			logi.logInfo "Resultttttttttttttt "+result
			return result1
		}
		else
		{
		logi.logInfo "Resultttttttttttttt "+result
		return result
		}

	}
	
	
	String md_GET_INV_NO_REC_EXT_PAY(String txt,LinkedHashMap m)
	
		{
			logi.logInfo ("Inside md_GET_INV_NO")
			String invoiceno
			String acct_no
			String client_no=Constant.mycontext.expand('${Properties#Client_No}')
			acct_no = vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			logi.logInfo ("Acct no : "+acct_no)
			switch(txt){
				case "create_acct_complete":
				invoiceno=vm.getValueFromResponse(txt,ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
				break;
				case "update_master_plan":
				invoiceno=vm.getValueFromResponse(txt,ExPathRpcEnc.UPDATE_MASTER_PLAN_INVOICENO1)
				break;
				case "invalid":
				invoiceno="1231234"
				break;
				case "NotInAcct":
				String query_Invoice = "SELECT INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO NOT IN("+acct_no+") AND CLIENT_NO = "+client_no+" AND ROWNUM = 1"
				ConnectDB db = new ConnectDB()
				invoiceno = db.executeQueryP2(query_Invoice)
				break;
			}
	
			logi.logInfo ("Invoice No: "+invoiceno)
			return invoiceno
	
		}
		String md_GET_ACCT_GRP(String type,LinkedHashMap m)
		{
			logi.logInfo("method md_GET_ACCT_GRP")
			String cag = null
			String tcid= Constant.TESTCASEID
			String client_no=Constant.lhmClientList.get(tcid)
			String [] temp
			//temp = type.split(":")
			
			logi.logInfo("Getting client no1 using client defined identifier : "+type)
			//String client_no = type.split("\\_")[0]
			//String client_no = Constant.mycontext.expand('${Properties#Client_No}')
			logi.logInfo(client_no)
			//String client_identifier = type.split("\\_")[1]
			String client_identifier = type
			logi.logInfo(client_identifier)
			logi.logInfo("Getting client no using client defined identifier : "+type)
			String planNum = "SELECT group_no FROM ARIACORE.client_acct_groups WHERE CLIENT_NO = "+client_no+" AND group_name = '"+client_identifier+"'"
			
			logi.logInfo("Query for plan no : "+planNum)
			ConnectDB db = new ConnectDB()
			cag = db.executeQueryP2(planNum)
			logi.logInfo("CAG No Is  :::: "+cag)
			return cag
		}
		
		String md_src_transaction_id(String type,LinkedHashMap m)
		{
			logi.logInfo "md_src_transaction_id"
			def accountNo=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
				
				if(accountNo =='NoVal')
				{
					   accountNo = vm.getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
				}
			String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
			ConnectDB db = new ConnectDB()
			String invoice_no=db.executeQueryP2(invoiceQuery)
			return invoice_no
	}
	
		String md_contract_no(String type,LinkedHashMap m)
		{
			logi.logInfo "md_src_transaction_id"
			def accountNo=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
				
				if(accountNo =='NoVal')
				{
					   accountNo = vm.getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
				}
			String contractQuery="SELECT contract_no from ariacore.acct_plan_contracts where client_no= "+clientNo+ "and acct_no= "+accountNo+"order by CREATE_DATE DESC"
			ConnectDB db = new ConnectDB()
			String contract_no=db.executeQueryP2(contractQuery)
			return contract_no.toString()
		}
		
		String md_contract_no_multi(String type,LinkedHashMap m)
		{
			logi.logInfo "md_src_transaction_id"
			def accountNo=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
				
				if(accountNo =='NoVal')
				{
					   accountNo = vm.getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
				}
			String contractQuery="SELECT contract_no from ariacore.acct_multi_plan_contracts where client_no= "+clientNo+ "and acct_no= "+accountNo+"order by CREATE_DATE DESC"
			ConnectDB db = new ConnectDB()
			String contract_no=db.executeQueryP2(contractQuery)
			return contract_no.toString()
		}
		String md_get_invoice_no(String type,LinkedHashMap<String,String> m)
		{
			ConnectDB dbConnection=new ConnectDB()
			logi.logInfo "Inside md_get_invoice_no "
			logi.logInfo "Type "+type
			String client_no=Constant.mycontext.expand('${Properties#Client_No}')
			logi.logInfo "Type 1"+type
			String acct_no = vm.getValueFromResponse("create_acct_complete_m",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			logi.logInfo "Type 2"+type
			String invoice_no = dbConnection.executeQueryP2("Select max(invoice_no) from ariacore.all_invoices where acct_no="+acct_no+" and client_no="+client_no)
			logi.logInfo "Type 3"+type
			dbConnection.closeConnection()
			return invoice_no
	
		}
		
		String md_get_plan_no(String type,LinkedHashMap<String,String> m)
		{
			ConnectDB dbConnection=new ConnectDB()
			logi.logInfo "Inside md_get_plan_no "
			logi.logInfo "Type "+type
			String client_no=Constant.mycontext.expand('${Properties#Client_No}')
			String plan_no = vm.getValueFromResponse("create_new_plan",ExPathRpcEnc.CREATE_NEW_PLAN_PLANNO1)
			return plan_no
		}
		
		String md_get_client_plan_id(String type,LinkedHashMap<String,String> m)
		{
			ConnectDB dbConnection=new ConnectDB()
			logi.logInfo "Inside md_get_client_plan_id "
			logi.logInfo "Type "+type
			String client_no=Constant.mycontext.expand('${Properties#Client_No}')
			String plan_no = vm.getValueFromResponse("create_new_plan",ExPathRpcEnc.CREATE_NEW_PLAN_PLANNO1)
			String client_plan_id = db.executeQueryP2("Select client_plan_id from ariacore.client_plan where plan_no = "+plan_no+" and client_no ="+client_no )
			dbConnection.closeConnection()
			return client_plan_id
	
		}
		
		String md_getAriaXMLStatementNo(String type,LinkedHashMap<String,String> m)
		{
					logi.logInfo "md_getAriaXMLStatementN0"
					VerificationMethods vm =new VerificationMethods()
					ConnectDB db = new ConnectDB()
					String accountNo = vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
					logi.logInfo "Plan No" + accountNo
					String q = "Select statement_no from ariacore.acct_statement where acct_no=" +accountNo
					logi.logInfo "Stemt No" + q
					String Stmt_no= db.executeQueryP2(q)
					logi.logInfo "Statement No" + Stmt_no
					String xml_no = db.executeQueryP2("Select xml_statement_no from ariacore.xml_document where statement_no ="+ Stmt_no )
					logi.logInfo "xml_no No" + xml_no
					return xml_no
		}
		String md_get_futuretime(String type,LinkedHashMap<String,String> m)
		{
					logi.logInfo "md_get_futuretime"
					VerificationMethods vm =new VerificationMethods()
					String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
					ConnectDB db = new ConnectDB()
					String accountNo = vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
					logi.logInfo "Plan No" + accountNo
					String vt_qry="SELECT TO_CHAR(ARIACORE.ARIAVIRTUALTIME("+clientNo+")+" + type + ", 'YYYY-MM-DD')  FROM DUAL"
					logi.logInfo "Stemt No" + vt_qry
					String vt_time= db.executeQueryP2(vt_qry)
					logi.logInfo "vt_time No" + vt_time
					return vt_time
		}
		String md_GET_DISPUTE_NO(String txt,LinkedHashMap m)
		
			{
				logi.logInfo ("Inside md_GET_DISPUTE_NO")
				String dispute
				String acct_no
				if(txt.contains("DB"))
				{
					if(txt.contains("."))
					{
						String pref=txt.split("\\.")[1]
						logi.logInfo("pref:"+pref)
						String api="create_acct_complete.".concat(pref)
						acct_no=vm.getValueFromResponse(api,ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
					}
		
					else
					{
						acct_no=vm.getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
					}
					String query="SELECT source_no FROM ariacore.acct_transaction  WHERE acct_no = "+acct_no+" and transaction_type in(6)"
					ConnectDB db = new ConnectDB()
					ResultSet resultSet = db.executePlaQuery(query);
					if (resultSet.next())
					{
						dispute = resultSet.getString(1);
					}
					resultSet.close();
				}
				else
				{
					dispute=vm.getValueFromResponse(txt,ExPathRpcEnc.CREATE_WRETEOFF_OR_DISPUTE_REC_NO)
				}
		
				logi.logInfo ("Dispute No: "+dispute)
				return dispute
		
			}
			
			
			public String md_XML_STATEMENT_NO(String txt,LinkedHashMap m)
			{
				   logi.logInfo ("Calling md_XML_STATEMENT_NO")
				   int eventTimeout = 0
				   def acct_no=vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
				   String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
				   logi.logInfo ("Calling Client No "+clientNo)
				   logi.logInfo ("Calling Acct No"+acct_no)
				   ConnectDB db = new ConnectDB()
				   String xml_statment =db.executeQueryP2("SELECT XML_STATEMENT_NO from ariacore.xml_document where client_no="+clientNo+" and acct_no="+acct_no)
				   logi.logInfo ("Calling 1st time query "+xml_statment)
				   if(xml_statment == '' || xml_statment == null) {
					   logi.logInfo ("Calling inside if loop"+xml_statment)
					//  xml_statment =db.executeQueryP2("SELECT XML_STATEMENT_NO from ariacore.xml_document where client_no="+clientNo+" and acct_no="+acct_no)
					  logi.logInfo ("Calling 2nd time query"+xml_statment)
					  while(eventTimeout < 60) {
						  logi.logInfo ("while before sleep"+eventTimeout)
						   Thread.sleep(1000)
						   logi.logInfo ("while after sleep"+eventTimeout)
					  xml_statment =db.executeQueryP2("SELECT XML_STATEMENT_NO from ariacore.xml_document where client_no="+clientNo+" and acct_no="+acct_no)
						   eventTimeout++
						   logi.logInfo ("timeout count"+eventTimeout)
					   }
				   }
				   logi.logInfo ("XML Statment: "+xml_statment)
				   return xml_statment
			}
			
			
			
			def md_GET_ACCT_NO_m(String type,LinkedHashMap<String,String> m)
			{
				logi.logInfo("calling md_GET_ACCT_NO_M ")
				String service2=library.Constants.Constant.CSERVICE
				def accno
				logi.logInfo("type:"+type)
		
					String xpath1=ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M+"["+type+"]/ns1:acct_no[1]";
					
					
					logi.logInfo("type:"+xpath1)
					accno =vm.getValueFromResponse(service2,xpath1)
					if(accno==null||accno.toString().equalsIgnoreCase("noval"))
					{
						 xpath1="//*/ns1:out_acct"+"["+type+"]/ns1:acct_no[1]";
						
						logi.logInfo("type:"+xpath1)
						accno =vm.getValueFromResponse(service2,xpath1)
						
						
					}
					
					
					if(accno==null||accno.toString().equalsIgnoreCase("noval"))
					{
						xpath1=ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M+"[1]/ns1:out_acct_row["+type+"]/ns1:acct_no[1]";
						logi.logInfo("type:"+xpath1)
						accno =vm.getValueFromResponse(service2,xpath1)
						if(accno==null||accno.toString().equalsIgnoreCase("noval"))
						{
							xpath1="//*/ns1:out_acct"+"[1]/ns1:out_acct_row["+type+"]/ns1:acct_no[1]";
							logi.logInfo("type:"+xpath1)
							accno =vm.getValueFromResponse(service2,xpath1)
							
						}
					}
					
					logi.logInfo("Xpath :"+xpath1)
				return accno
			}
			
			def md_GET_ACCT_NO2_m(String type,LinkedHashMap<String,String> m)
			{
				logi.logInfo("calling md_GET_ACCT_NO_M ")
				String service2="create_acct_complete_m+"
				def accno
				logi.logInfo("type:"+type)
		
				if(type.contains("#"))
				{
					service2=service2+type.split("#")[1]
					type=type.split("#")[0]
				}
					String xpath1=ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M+"["+type+"]/ns1:acct_no[1]";
					
					
					logi.logInfo("type:"+xpath1)
					accno =vm.getValueFromResponse(service2,xpath1)
					if(accno==null||accno.toString().equalsIgnoreCase("noval"))
					{
						 xpath1="//*/ns1:out_acct"+"["+type+"]/ns1:acct_no[1]";
						
						logi.logInfo("type:"+xpath1)
						accno =vm.getValueFromResponse(service2,xpath1)
						
						
					}
					
					
					if(accno==null||accno.toString().equalsIgnoreCase("noval"))
					{
						xpath1=ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M+"[1]/ns1:out_acct_row["+type+"]/ns1:acct_no[1]";
						logi.logInfo("type:"+xpath1)
						accno =vm.getValueFromResponse(service2,xpath1)
						if(accno==null||accno.toString().equalsIgnoreCase("noval"))
						{
							xpath1="//*/ns1:out_acct"+"[1]/ns1:out_acct_row["+type+"]/ns1:acct_no[1]";
							logi.logInfo("type:"+xpath1)
							accno =vm.getValueFromResponse(service2,xpath1)
							
						}
					}
					
					logi.logInfo("Xpath :"+xpath1)
				return accno
			}
			
			def md_GET_MPI_NO(String mode,LinkedHashMap<String,String> m) throws Exception
			{
				//if the account has only one acct, dont mention the acct index and only mention the MPI index
				def MPINO
				try{
				logi.logInfo("calling md_MPI_NO ")
				
				logi.logInfo("input string :"+mode)
				String Acct,MPI
				String Xpath;
				
				if (mode.contains(","))
				{
		
					Acct=mode.split("\\,")[0]
					MPI=mode.split("\\,")[1]
					Xpath=ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M+"["+Acct+"]"+ExPathRpcEnc.CREATE_ACCT_COMPLETE_MPINO_M+"_row["+MPI+"]/*:plan_instance_no[1]";
					MPINO =vm.getValueFromResponse("create_acct_complete_m",Xpath)
					if(MPINO==null||MPI.toString().equalsIgnoreCase("noval"))
					{
						Xpath=ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M+"[1]/*:out_acct_row["+Acct+"]"+ExPathRpcEnc.CREATE_ACCT_COMPLETE_MPINO_M+"[1]/*:master_plans_assigned_row["+MPI+"]/*:plan_instance_no[1]";
						MPINO =vm.getValueFromResponse("create_acct_complete_m",Xpath)
					}
					
				}
				else
				{
					MPI=mode
					Xpath=ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M+"["+MPI+"]/ns1:plan_instance_no[1]"
					
				}
				
				logi.logInfo("Xpath : "+Xpath)
				
				
				if(MPINO==null||MPI.toString().equalsIgnoreCase("noval")){throw NullPointerException}
				
				}catch (Exception e)
				{    	logi.logInfo(" There is no such MPI present ")	}
				return MPINO
			}
			
			def md_GET_CHILD_ACCT_NO_M(String mode,LinkedHashMap<String,String> m) throws Exception
			{	def CANO
				try{
				logi.logInfo("md_GET_CHILD_ACCT_NO ")
				
				logi.logInfo("input string :"+mode)
				String Acct,CA
				String Xpath;
				
				if (mode.contains(","))
				{
		
					Acct=mode.split("\\,")[0]
					CA=mode.split("\\,")[1]
					Xpath=ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M+"[1]/ns1:out_acct_row["+Acct+"]/ns1:out_acct[1]/*:out_acct_row["+CA+"]/ns1:acct_no[1]";
					logi.logInfo("md_GET_CHILD_ACCT_NO "+Xpath)
					CANO =vm.getValueFromResponse("create_acct_complete_m",Xpath)
					if(CANO==null||CANO.toString().equalsIgnoreCase("noval"))
					{
						Xpath="//*/ns1:out_acct"+"[1]/ns1:out_acct_row["+Acct+"]/ns1:out_acct[1]/*:out_acct_row["+CA+"]/ns1:acct_no[1]";
						logi.logInfo("md_GET_CHILD_ACCT_NO "+Xpath)
						CANO =vm.getValueFromResponse("create_acct_complete_m",Xpath)
					}
					
				
				}else{ throw NullPointerException}
			
				logi.logInfo("md_GET_CHILD_ACCT_NO "+Xpath)
				
				
				if(CANO==null||CANO.toString().equalsIgnoreCase("noval")){throw NullPointerException}
				
				
				logi.logInfo("cano got :"+CANO)
				}catch (Exception e)
				{    	logi.logInfo(" There is no such CA present or Parent acct index not mentioned ")	}
				return CANO
			}
			
			def md_GET_CHILD_ACCT_MPI_M(String mode,LinkedHashMap<String,String> m) throws Exception
			{	def CMPI
				try{
				logi.logInfo("md_GET_CHILD_ACCT_MPI_M ")
				
				logi.logInfo("input string :"+mode)
				String Acct,CA,MPI
				String Xpath;
				
				if (mode.contains(","))
				{
		
					Acct=mode.split("\\,")[0]
					CA=mode.split("\\,")[1]
					MPI=mode.split("\\,")[2]
					Xpath=ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M+"[1]/ns1:out_acct_row["+Acct+"]/ns1:out_acct[1]/*:out_acct_row["+CA+"]/ns1:master_plans_assigned[1]/ns1:master_plans_assigned_row["+MPI+"]/ns1:plan_instance_no[1]";
					logi.logInfo("md_GET_CHILD_ACCT_NO "+Xpath)
					CMPI =vm.getValueFromResponse("create_acct_complete_m",Xpath)
					if(CMPI==null||CMPI.toString().equalsIgnoreCase("noval"))
					{
						Xpath="//*/ns1:out_acct"+"[1]/ns1:out_acct_row["+Acct+"]/ns1:out_acct[1]/*:out_acct_row["+CA+"]/ns1:master_plans_assigned[1]/ns1:master_plans_assigned_row["+MPI+"]/ns1:plan_instance_no[1]";
						logi.logInfo("md_GET_CHILD_ACCT_NO "+Xpath)
						CMPI =vm.getValueFromResponse("create_acct_complete_m",Xpath)
					}
					
					if(CMPI==null||CMPI.toString().equalsIgnoreCase("noval"))
					{
						Xpath=ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M+"[1]/ns1:out_acct_row["+Acct+"]/ns1:out_acct[1]/*:out_acct_row["+CA+"]/ns1:master_plans_assigned["+MPI+"]/ns1:plan_instance_no[1]";
						 logi.logInfo("md_GET_CHILD_ACCT_NO "+Xpath)
						 CMPI =vm.getValueFromResponse("create_acct_complete_m",Xpath)
						 if(CMPI==null||CMPI.toString().equalsIgnoreCase("noval"))
						 {
							 Xpath="//*/ns1:out_acct"+"[1]/ns1:out_acct_row["+Acct+"]/ns1:out_acct[1]/*:out_acct_row["+CA+"]/ns1:master_plans_assigned["+MPI+"]/ns1:plan_instance_no[1]";
							 logi.logInfo("md_GET_CHILD_ACCT_NO "+Xpath)
							 CMPI =vm.getValueFromResponse("create_acct_complete_m",Xpath)
						 }
					}
				
				}else{ throw NullPointerException}
			
				logi.logInfo("md_GET_CHILD_ACCT_MPI_M "+Xpath)
				
				
				if(CMPI==null||CMPI.toString().equalsIgnoreCase("noval")){throw NullPointerException}
				
				
				logi.logInfo("CMPI got :"+CMPI)
				}catch (Exception e)
				{    	logi.logInfo(" There is no such CMPI present or No such node ")	}
				return CMPI
			}

			def md_GET_CHILD_PI_M(String mode,LinkedHashMap<String,String> m) throws Exception
			{	//Pass the parent plan instance as input to get the child plan instance
				def CPI
				
				logi.logInfo("md_GET_CHILD_PI_M ")
				
				logi.logInfo("input string :"+mode)
				String Parent,index,Query
				if (mode.contains(","))
				{
		
					Parent=mode.split("\\,")[0]
					index=mode.split("\\,")[1]
					
				}
				else
				{
					Parent=mode
					index="1"
				}
				ConnectDB db = new ConnectDB()
				Query="SELECT a.CHILD FROM (SELECT plan_instance_no  AS CHILD, row_number()over(order by plan_instance_no DESC) AS seq FROM ariacore.plan_instance WHERE parent_plan_instance_no="+Parent+" ) a WHERE a.seq="+index
				CPI=db.executeQueryP2(Query)
				logi.logInfo("md_GET_CHILD_PI_M : "+CPI)
				return CPI
			}
			
			def md_GET_CPI_M(String mode,LinkedHashMap<String,String> m) throws Exception
			{
				def PI
				
				logi.logInfo("md_GET_CPI_M ")
				
				logi.logInfo("input string :"+mode)
				int c=0;
				String Parent=mode.split("\\,")[0],index
				logi.logInfo("before loop")
				for (int i = 0; i < mode.length(); i++)
				{
					if (mode.charAt(i) == ',')
					{
						c++;
					}
				}
				
				
				
				for(int i=1;i<c;i++)
		
				{
					Parent=Parent+","+mode.split("\\,")[i]
				 }
				
					index=mode.split("\\,")[c]
					logi.logInfo("second for loop over")
					PI=md_GET_CHILD_PI_M(md_GET_CHILD_ACCT_MPI_M(Parent, m)+","+index,m)
					
					logi.logInfo("Parent :"+Parent)
					logi.logInfo("index:"+index)
					
					return PI
			}
			
			String md_invoice_m(String input,LinkedHashMap<String,String> m) throws Exception
			{
				logi.logInfo("into md_invoice_m" + input)
				db=new ConnectDB()
				return db.executeQueryP2("select a.invoice_no from (SELECT ROW_NUMBER()OVER(ORDER BY invoice_no) as seq, invoice_no from ariacore.gl where acct_no="+md_GET_ACCT_OR_INSTANCE_NO_m(input.split("-")[0],m).toString()+")a where a.seq="+input.split("-")[1]).toString()
			}
			
			def md_GET_ACCT_OR_INSTANCE_NO_m(String input,LinkedHashMap<String,String> m) throws Exception
				
			
{
	 logi.logInfo("into md_GET_INSTANCE_NO_M")
String Query
String instance
String temp=""


String[] format=input.split (",")
String acct_no=md_GET_ACCT_NO_m(input.split (",")[0],m).toString()
if(acct_no.equals("NoVal"))acct_no=library.Constants.Constant.UniqueID.get(library.Constants.Constant.TESTCASEID+'_1')
instance=acct_no
logi.logInfo instance
int count=0;
for (int i = 0; i < input.length(); i++)
{
	 if (input.charAt(i) == ',')
	 {
			count++;
	 }
}


int length=count+1

for(int start=1;start<length;start++)
{
	 String unformatted=format[start]
	 ConnectDB db=new ConnectDB()
	 
	 String node_number=unformatted.substring(0,(unformatted.length()-1))
	 
	 String node_identifier=unformatted.substring((unformatted.length()-1))
	 
	 logi.logInfo node_number
	 logi.logInfo node_identifier
	 
	try{ if(node_identifier.equalsIgnoreCase("a"))
	 {      logi.logInfo "into a"
			Query = "SELECT a.CHILD_ACCT FROM   (SELECT acct_no as CHILD_ACCT, row_number() over(order by acct_no) AS seq  FROM ariacore.acct   WHERE senior_acct_no="+instance+"  )a WHERE a.seq="+node_number
			logi.logInfo Query
		   instance=db.executeQueryP2(Query).toString()
			logi.logInfo instance
	 }}catch(Exception e){}
	 try { if(node_identifier.equalsIgnoreCase("m"))
	 {   logi.logInfo "into m"
			Query="SELECT a.MPI FROM (select pi.plan_instance_no as MPI, row_number() over(order by pi.client_no) AS seq from ariacore.plan_instance pi join ariacore.plan_instance_master pim on pi.plan_instance_no=pim.plan_instance_no where pi.acct_no ="+instance+" )a WHERE a.seq="+node_number
			logi.logInfo Query
			instance=db.executeQueryP2(Query).toString()
			logi.logInfo instance
	 }}catch(Exception e){}
	try{ if(node_identifier.equalsIgnoreCase("s"))
	 {      logi.logInfo "into s"
			   Query="SELECT a.CHILD FROM (SELECT plan_instance_no  AS CHILD, row_number()over(order by client_no) AS seq FROM ariacore.plan_instance WHERE parent_plan_instance_no="+instance+")a WHERE a.seq="+node_number
			instance=db.executeQueryP2(Query).toString()
			//instance=md_GET_CHILD_PI_M(instance,m)
			logi.logInfo instance
	 }}catch(Exception e){}
	 try{ if(node_identifier.equalsIgnoreCase("i"))
	 {      logi.logInfo "into i"
			 Query="SELECT a.ins FROM (SELECT plan_unit_instance_no as ins, row_number() over(order by client_no) AS seq FROM ariacore.PLAN_INST_UNIT_INSTANCES WHERE plan_instance_no="+instance+")a WHERE a.seq="+node_number
			 instance=db.executeQueryP2(Query).toString()
			   logi.logInfo instance
	 }}catch(Exception e){}


}
if(instance == 'NoVal' || instance == null)
instance=library.Constants.Constant.UniqueID.get(library.Constants.Constant.TESTCASEID+'_'+input)
else library.Constants.Constant.UniqueID.put(library.Constants.Constant.TESTCASEID+'_'+input,instance)
return instance
	 
}



def md_GET_ACCT_OR_INSTANCE_NO2_m(String input,LinkedHashMap<String,String> m) throws Exception
					 
		{
			  logi.logInfo("into md_GET_INSTANCE_NO2_M")
		String Query
		String instance
		String temp=""
		if(input.contains("#"))
		{
			temp=input.split("#")[1];
			input=input.split("#")[0];
		}
		
		
		String[] format=input.split (",")
		String acct_no=md_GET_ACCT_NO2_m(input.split (",")[0]+"#"+temp,m).toString()
		instance=acct_no
		logi.logInfo instance
		int count=0;
		for (int i = 0; i < input.length(); i++)
		{
			  if (input.charAt(i) == ',')
			  {
					 count++;
			  }
		}
		
		
		int length=count+1
		
		for(int start=1;start<length;start++)
		{
			  String unformatted=format[start]
			  ConnectDB db=new ConnectDB()
			  
			  String node_number=unformatted.substring(0,(unformatted.length()-1))
			  
			  String node_identifier=unformatted.substring((unformatted.length()-1))
			  
			  logi.logInfo node_number
			  logi.logInfo node_identifier
			  
			  if(node_identifier.equalsIgnoreCase("a"))
			  {      logi.logInfo "into a"
					 Query = "SELECT a.CHILD_ACCT FROM   (SELECT acct_no as CHILD_ACCT, row_number() over(order by acct_no) AS seq  FROM ariacore.acct   WHERE senior_acct_no="+instance+"  )a WHERE a.seq="+node_number
					 logi.logInfo Query
					instance=db.executeQueryP2(Query).toString()
					 logi.logInfo instance
			  }
			  if(node_identifier.equalsIgnoreCase("m"))
			  {   logi.logInfo "into m"
					Query="SELECT a.MPI FROM (select pi.plan_instance_no as MPI, row_number() over(order by pi.client_no) AS seq from ariacore.plan_instance pi join ariacore.plan_instance_master pim on pi.plan_instance_no=pim.plan_instance_no where pi.acct_no ="+instance+" )a WHERE a.seq="+node_number
					 logi.logInfo Query
					 instance=db.executeQueryP2(Query).toString()
					 logi.logInfo instance
			  }
			  if(node_identifier.equalsIgnoreCase("s"))
			  {      logi.logInfo "into s"
					  Query="SELECT a.CHILD FROM (SELECT plan_instance_no  AS CHILD, row_number()over(order by client_no) AS seq FROM ariacore.plan_instance WHERE parent_plan_instance_no="+instance+")a WHERE a.seq="+node_number
					  instance=db.executeQueryP2(Query).toString()
				  // instance=md_GET_CHILD_PI_M(instance,m)
					 logi.logInfo instance
			  }
			  if(node_identifier.equalsIgnoreCase("i"))
			  {      logi.logInfo "into i"
					  Query="SELECT a.ins FROM (SELECT plan_unit_instance_no as ins, row_number() over(order by client_no) AS seq FROM ariacore.PLAN_INST_UNIT_INSTANCES WHERE plan_instance_no="+instance+")a WHERE a.seq="+node_number
					  instance=db.executeQueryP2(Query).toString()
						logi.logInfo instance
			  }

		}
		return instance
			  
		}

					 

				def md_GET_BG_NO_M(String input,LinkedHashMap<String,String> m) throws Exception
				{
					 def BGNO
					 ConnectDB db=new ConnectDB()
					 String instance = md_GET_ACCT_OR_INSTANCE_NO_m(input,m).toString()
					 String Query
					 Query ="select BILLING_GROUP_NO from ariacore.acct_billing_group where  acct_no in ((Select acct_no from ariacore.plan_instance where PLAN_INSTANCE_NO = "+instance+"))"
					 logi.logInfo Query
					 BGNO=db.executeQueryP2(Query).toString()
					 return BGNO
				}
				
				def md_GET_BG_ID_M(String input,LinkedHashMap<String,String> m) throws Exception
				{
					 def BGID
					 ConnectDB db=new ConnectDB()
					 String instance = md_GET_ACCT_OR_INSTANCE_NO_m(input,m).toString()
					 String Query
					 Query ="select profile_cdid from ariacore.acct_billing_group where  acct_no in ((Select acct_no from ariacore.plan_instance where PLAN_INSTANCE_NO = "+instance+"))"
					 logi.logInfo Query
					 BGID=db.executeQueryP2(Query).toString()
					 return BGID
				}
				
	 
				
				def md_GET_M(String input,LinkedHashMap<String,String> m) throws Exception
				{
					 def val
					 logi.logInfo "into md_GET_M"
					 logi.logInfo input
					 String Coloumn_name= input.split("\\!")[0].replaceAll('\\-','\\_')
					 String Table_name= input.split("\\!")[1].replaceAll('\\-','\\_')
					 String Param=input.split("\\!")[2]  //.replaceAll('\\#','\\,')
					 ConnectDB db=new ConnectDB()
					 String instance = md_GET_ACCT_OR_INSTANCE_NO_m(Param,m).toString()
					 String Query
					 Query ="select "+Coloumn_name+" from ariacore."+Table_name+" where acct_no = "+instance
					 logi.logInfo Query
					 val=db.executeQueryP2(Query).toString()
					 if(val.toString().equalsIgnoreCase("null"))
					 {
						   Query ="select "+Coloumn_name+" from ariacore."+Table_name+" where acct_no in ((Select acct_no from ariacore.plan_instance where PLAN_INSTANCE_NO =  "+instance+"))"
						   logi.logInfo Query
						   val=db.executeQueryP2(Query).toString()
					  }
					 return val
				}
				
				/**
				 * to produce a unique to set as input to any API by timestamp
				 * @param input
				 * @return the unique is as input+timestamp
				 * @throws Exception
				 */
				def md_unique_id_m(String input,LinkedHashMap<String,String> m) throws Exception
				{
					 def UID
					 UID=input+"_"+library.GenericLibrary.Common.getCurrentTime()
					 library.Constants.Constant.UniqueID.put(input,library.GenericLibrary.Common.getCurrentTime())
					 return UID
				}
				/**
				 * it is uses to get the same time stamp to reuse it from the above module md_unique_id_m
				 * @param input
				 * @return the UID
				 * @throws Exception
				 */
				def md_get_UniqueID_m(String input,LinkedHashMap<String,String> m) throws Exception{
					def UID
					return input+"_"+library.Constants.Constant.UniqueID.get(input)
				}
				def md_GET_PAYMENT_METHOD_TYPE_M(String input,LinkedHashMap<String,String> m) throws Exception
				{
						 def TYPID
						 ConnectDB db=new ConnectDB()
						 String acct_no = md_GET_ACCT_NO_m(input.split (",")[0],m).toString()
						 String Query
						 Query ="select PAY_METHOD_ID from ariacore.PAYMENTS where acct_no = "+ acct_no
						 logi.logInfo Query
						 TYPID=db.executeQueryP2(Query).toString()
						 return TYPID
				}

				def md_GET_PAYMENT_METHOD_ID_M(String input,LinkedHashMap<String,String> m) throws Exception
				{
						 def METHID
						 ConnectDB db=new ConnectDB()
						 String acct_no = md_GET_ACCT_NO_m(input.split (",")[0],m).toString()
						 String Query
						 Query ="select PAYMENT_METHOD_CDID from ariacore.billing_info where acct_no = "+ acct_no
						 logi.logInfo Query
						 METHID=db.executeQueryP2(Query).toString()
						  return METHID
				}
		/**
		 * Used to get all the senior accounts for now
		 * in approach to get all the acct that is generated during create_acct_complete_m
		 * @return acct_nums
		 * @param no need input and the given are dummy
		 */
	ArrayList<String> md_GET_ALL_ACCT_NO_m(String input,LinkedHashMap<String,String> m)
	{
		logi.logInfo("calling md_GET_ALL_ACCT_NO_M ")
		EOMVerificationMethods EOM = new EOMVerificationMethods();
		HashMap hh=EOM.md_getSpecificnodeAsList_m("ns1:acct_no")
		ArrayList<String> acct_nums = new ArrayList<String>(hh.values());
		logi.logInfo acct_nums
		return acct_nums
	}
	
	String md_GET_ALL_ACCT_NO2_m(String input,LinkedHashMap<String,String> m)
	{
		logi.logInfo("calling md_GET_ALL_ACCT_NO_M ")
		EOMVerificationMethods EOM = new EOMVerificationMethods();
		HashMap hh=EOM.md_getSpecificnodeAsList_m("ns1:acct_no")
		logi.logInfo "just after getting the value"
		//logi.logInfo ""+hh.values().toString()
		if(!hh.isEmpty())
		{
			ArrayList<String> acct_nums = new ArrayList<String>(hh.values());
			logi.logInfo ""+acct_nums.toString()
			return acct_nums.toString()
		}
		else return ""
	}

	public int md_get_acct_count_m(String empty){
		logi.logInfo("into md_get_acct_count_m")
		LinkedHashMap<String,String> inner = new LinkedHashMap<String,String>()
		int acct_count=md_GET_ALL_ACCT_NO_m(empty,inner).size()
		logi.logInfo("method caught accts "+acct_count)
		return Integer.parseInt(acct_count.toString())
		
	}
	
	String md_GET_DUNNING_NO(String input,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_PLAN_NO")
		String planNo = null
		String tcid= Constant.TESTCASEID
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo(clientNo)
		String acct_no = md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
		logi.logInfo("Account No : "+acct_no)
		String planNum = "select dunning_group_no from (select dunning_group_no,rownum as rnum from ariacore.acct_dunning_group where acct_no = "+	acct_no + "order by dunning_group_no) where rnum= " +input
		logi.logInfo("Query for plan no : "+planNum)
		ConnectDB db = new ConnectDB()
		planNo = db.executeQueryP2(planNum)
		logi.logInfo("Plan No Is  :::: "+planNo)
		return planNo
	}

	String md_get_start_date_m(String acct_hierarchy,LinkedHashMap<String,String> m)
	{
		ConnectDB db = new ConnectDB()
		logi.logInfo "**** into md_get_start_date **** "
		
					
		String mpi = md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		logi.logInfo("Given mpi no:" + mpi)
		String mpi_query="Select TO_CHAR(last_bill_date,'YYYY-MM-DD') from ariacore.plan_instance where PLAN_INSTANCE_NO="+mpi
		String mpi_start_date=db.executeQueryP2(mpi_query)
		
		logi.logInfo("Start dateeeeee is " + mpi_start_date)
		
		return mpi_start_date
		
		
		
		}
	
	/**
	 * @param input
	 * @param m(this method is particular for case EOM-30302)
	 * @return billing_group_no of MPI 1
	 */
	public String md_get_billing_group_no(String input,LinkedHashMap<String,String> m){
	 ConnectDB db = new ConnectDB()
		logi.logInfo "in the billing group1"
		String acct_no = md_GET_ACCT_OR_INSTANCE_NO_m(input.split (",")[0],m).toString()
		logi.logInfo "mpi no" +acct_no
		String query = "select billing_group_no from ariacore.plan_instance_master where plan_instance_no in (select plan_instance_no from ariacore.plan_instance where acct_no = "+acct_no+" and rownum=1)"
		logi.logInfo query
		String billing_group_no = db.executeQueryP2(query).toString();
		logi.logInfo "billing group no : "+billing_group_no
		return billing_group_no
	}
	
	
	public def md_Get_Plan_Instance_No_m(String input,LinkedHashMap<String,String> m)
	{
	   logi.logInfo("********md_Get_Plan_Instance_No_m Start**************")
	   VerificationMethods vm = new VerificationMethods()
	   String intp=""
	   logi.logInfo "Printing input val"+input
	   if(input!=null&&input.contains("+")){intp=input}
		logi.logInfo "Print mpi no after assigning the plan"+"assign_acct_plan_m"+intp
	   int plan_instance = Integer.parseInt(vm.getValueFromResponse("assign_acct_plan_m"+intp,ExPathRpc.PLAN_INSTANCE_NO).toString())
	   logi.logInfo "mpi no after assigning the plan" +plan_instance
	   return plan_instance
	   logi.logInfo("********md_Get_Plan_Instance_No_m Ends**************")
	}
	
	public def md_Get_Supp_Plan_Inst_No_m(String plan_no,LinkedHashMap<String,String> m)
	{
	   logi.logInfo("********md_Get_Supp_Plan_Inst_No_m Start**************")
	   String acct_no = md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
	   String query = "SELECT plan_instance_no from ariacore.plan_instance where plan_no = "+plan_no+" and acct_no ="+acct_no
	   logi.logInfo query
	   String plan_instance_no = db.executeQueryP2(query).toString();
	   logi.logInfo "plan_instance_no : "+plan_instance_no
	   return plan_instance_no
	   logi.logInfo("********md_Get_Supp_Plan_Inst_No_m Ends**************")
	}
	
	public String md_Get_billing_group_no_plan_inst(String input,LinkedHashMap<String,String> m){
		logi.logInfo("********md_Get_billing_group_no_plan_inst Start**************")
		ConnectDB db = new ConnectDB()
		String a = md_Get_Plan_Instance_No_m(input,m);
		logi.logInfo "PlanInstance_no" + a
		   String query = "SELECT billing_group_no from ariacore.plan_instance_master where plan_instance_no = "+a
		   logi.logInfo query
		   String billing_group_no = db.executeQueryP2(query).toString();
		   logi.logInfo "billing group no : "+billing_group_no
		   return billing_group_no
	   }
	
	String md_get_plan_unit_instance_no_m(String acct_hierarchy1,LinkedHashMap<String,String> m)
	{
	   
		logi.logInfo("~~~~~~~~~~~~~~~~~~~~~~ INTO md_get_plan_unit_instance_no_m ~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		 logi.logInfo("acct_hierarchy1:::"+acct_hierarchy1)
	   
		String acct_hierarchy=acct_hierarchy1.split("#")[0]
		String seq=acct_hierarchy1.split("#")[1]
		
		logi.logInfo("~~acct_hierarchy :"+acct_hierarchy+"    seq"+seq)
		
		ConnectDB dbb = new ConnectDB()
		
			InputMethods im=new InputMethods()
			def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		
			String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
			String mpi_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
			logi.logInfo("acct_no"+acct_no)
			
		//	String plan_inst_q="SELECT A.PLAN_INSTANCE_NO FROM (select PLAN_INSTANCE_NO,ROW_NUMBER() OVER (ORDER BY PLAN_INSTANCE_NO)AS SEQ  FROM ARIACORE.PLAN_INSTANCE_VIEW WHERE ACCT_NO="+acct_no+")A WHERE A.SEQ="+seq

			 //String plan_inst_no = dbb.executeQueryP2(plan_inst_q)
			// String plan_inst_unit_q="SELECT A.PLAN_UNIT_INSTANCE_NO FROM(select PLAN_UNIT_INSTANCE_NO,ROW_NUMBER() OVER (ORDER BY CLIENT_NO) as SEQ from ariacore.PLAN_INST_UNIT_INSTANCES where client_no ="+client_no +" and plan_instance_no ="+plan_inst_no+")A WHERE A.SEQ="+seq
			
			String plan_inst_unit_q="SELECT A.PLAN_UNIT_INSTANCE_NO FROM(select PLAN_UNIT_INSTANCE_NO,ROW_NUMBER() OVER (ORDER BY CLIENT_NO) as SEQ from ariacore.PLAN_INST_UNIT_INSTANCES where client_no ="+client_no +" and plan_instance_no ="+mpi_no+")A WHERE A.SEQ="+seq
			String plan_inst_unit = dbb.executeQueryP2(plan_inst_unit_q)
		   
			logi.logInfo("--------> plan_inst_unit :"+plan_inst_unit)
			return plan_inst_unit
		
	}
			  
	 public String md_get_invoice_no_m(String input,LinkedHashMap<String,String> m){
		
		ConnectDB db = new ConnectDB()
		logi.logInfo "string input : "+input
		String plan_instance = md_GET_ACCT_OR_INSTANCE_NO_m(input,m).toString()
		String query = "select max(invoice_no) as invoice_no from ariacore.gl_detail where CLIENT_NO="+Constant.client_no+" and  master_plan_instance_no = " + plan_instance
		String invoice_no = db.executeQueryP2(query)
		if(invoice_no!=null)return invoice_no
		query = "select max(invoice_no) as invoice_no from ariacore.gl_pending_detail where CLIENT_NO="+Constant.client_no+" and master_plan_instance_no = " + plan_instance
		invoice_no = db.executeQueryP2(query)
		return invoice_no
  }
	 
	 public String md_get_invoice_no_base_m(String input,LinkedHashMap<String,String> m){
		 
		 ConnectDB db = new ConnectDB()
		 logi.logInfo "string input : "+input
		 String plan_instance = md_GET_ACCT_OR_INSTANCE_NO2_m(input,m).toString()
		 String query = "select max(invoice_no) as invoice_no from ariacore.gl_detail where CLIENT_NO="+Constant.client_no+" and  master_plan_instance_no = " + plan_instance
		 String invoice_no = db.executeQueryP2(query)
		 if(invoice_no!=null)return invoice_no
		 query = "select max(invoice_no) as invoice_no from ariacore.gl_pending_detail where CLIENT_NO="+Constant.client_no+" and master_plan_instance_no = " + plan_instance
		 invoice_no = db.executeQueryP2(query)
		 return invoice_no
   }
	public String md_get_bg_noDB_m(String input,LinkedHashMap<String,String> m)
	{
		ConnectDB db = new ConnectDB()
		String acct_no = md_GET_ACCT_OR_INSTANCE_NO_m(input.split ("#")[0],m).toString()
		String query = "select billing_group_no from (select row_number() over (order by client_no) as seq,billing_group_no from ariacore.acct_billing_group where acct_no="+acct_no+") where seq="+input.split ("#")[1]
		String billing_group_no = db.executeQueryP2(query).toString();
		return billing_group_no
	}
	
	public def md_get_bg_no_m(String input,LinkedHashMap<String,String> m)
	{
		return vm.getValueFromResponse("create_acct_complete_m", "//ns1:create_acct_complete_mResponse[1]/out_acct[1]/ns1:out_acct_row[1]/ns1:billing_errors[1]/ns1:billing_errors_row["+input+"]/ns1:billing_group_no[1]").toString()
	}


	
	public String md_DG_no_m(String input,LinkedHashMap<String,String> m)
	{
		ConnectDB db = new ConnectDB()
		return db.executeQueryP2("select dunning_group_no from ariacore.acct_dunning_group where DUNNING_GROUP_CDID='"+md_get_UniqueID_m(input,m).toString()+"'").toString()
				
	}
	public String md_DG_no_from_acct_m(String input,LinkedHashMap<String,String> m)
	{
		ConnectDB db = new ConnectDB()
		return db.executeQueryP2("select dunning_group_no from (select row_number() over (order by client_no) as seq,dunning_group_no from ariacore.acct_dunning_group where acct_no="+md_GET_ACCT_OR_INSTANCE_NO_m(input.split ("#")[0],m).toString()+") where seq="+input.split ("#")[1]).toString()
				
	}
	
	String md_get_contract_no(String acct_hierarchy,LinkedHashMap<String,String> m)
	{
		ConnectDB dbb = new ConnectDB()
		
			InputMethods im=new InputMethods()
			def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		
			String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
			String contract_no_count_q="Select count(CONTRACT_NO) from ariacore.ACCT_UNIVERSAL_CONTRACTS where  ACCT_NO="+acct_no
			String contract_no_count=dbb.executeQueryP2(contract_no_count_q)
			int flag=0
			String db_to_execute=null
			if(contract_no_count.toInteger() <=0)
			{
			String contract_no_count1_q="Select count(CONTRACT_NO) from ariacore.ACCT_MULTI_PLAN_CONTRACTS where  ACCT_NO="+acct_no
			String contract_no_count1=dbb.executeQueryP2(contract_no_count1_q)
			flag=1
			}
			
			if (flag==1)
			 db_to_execute="ariacore.ACCT_MULTI_PLAN_CONTRACTS"
			else
			 db_to_execute="ariacore.ACCT_UNIVERSAL_CONTRACTS"
			
			 
			 String contract_no_q="Select CONTRACT_NO from "+db_to_execute+" where  ACCT_NO="+acct_no
			 String contract_no=dbb.executeQueryP2(contract_no_q)
			 
			 return contract_no
			
			
			
	}
	public String md_invoiceNO_from_CAC_m (String input,LinkedHashMap<String,String> m)
	{
		return vm.getValueFromResponse("create_acct_complete_m", "//*/out_acct[1]/ns1:out_acct_row[1]/ns1:invoice_info[1]/ns1:invoice_info_row[1]/ns1:invoice_no["+input+"]").toString()
	}
	
	public String md_VTdate_m(String numberOfDays,LinkedHashMap<String,String> m)
	{
		logi.logInfo "into md_daysVT_m "+numberOfDays
		
		db = new ConnectDB();
		Date date = new Date();
		Calendar now = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + Constant.client_no + ") from DUAL";
		ResultSet resultSet = db.executeQuery(Query);
		while(resultSet.next()){
			// Get only the current Date

			date = resultSet.getDate(1);
		}
		now.setTime(date);
		if(numberOfDays.contains("-")){
			if(numberOfDays.contains(".")) {
				String[] days = numberOfDays.split("\\.");
				now.add(Calendar.MONTH, Integer.parseInt(days[0]));
				now.add(Calendar.DATE, -(Integer.parseInt(days[1])));
			}
			else
				now.add(Calendar.DATE,Integer.parseInt(numberOfDays));
		}
		else{
			if(numberOfDays.contains(".")) {
				String[] days = numberOfDays.split("\\.");
				now.add(Calendar.MONTH, Integer.parseInt(days[0]));
				now.add(Calendar.DATE, Integer.parseInt(days[1]));
			}
			else
				now.add(Calendar.DATE, Integer.parseInt(numberOfDays));
		}

		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String date1 = format1.format(now.getTime());
		//db.closeConnection()
		return date1;
	}
	public String md_get_ddate_noDB_m(String input,LinkedHashMap<String,String> m)
	{
			ConnectDB db = new ConnectDB()
			String acct_no = md_GET_ACCT_OR_INSTANCE_NO_m(input,null).toString()
			String query = "select TO_CHAR(created,'YYYY-MM-DD') from ariacore.acct where acct_no = "+acct_no
			String billing_group_no = db.executeQueryP2(query).toString();
			return billing_group_no
	}
	public String md_get_usage_recno_DB_m(String input,LinkedHashMap<String,String> m)
	{
			ConnectDB db = new ConnectDB()
			String acct_no = md_GET_ACCT_OR_INSTANCE_NO_m(input,null).toString()
			String query = "select max(REC_NO) from ariacore.usage where acct_no = "+acct_no
			String billing_group_no = db.executeQueryP2(query).toString();
			return billing_group_no
	}
	/**
	 * method to get the latest created contract
	 * @param contract created API
	 */
	String md_get_instance_contract_no_m(String input,LinkedHashMap<String,String> m)
	{
		 if(vm.getValueFromResponse(input,"//*/contract_no[1]").toString()!="NoVal")
		 return vm.getValueFromResponse(input,"//*/contract_no[1]").toString()
		 else return vm.getValueFromResponse(input,"//*/out_acct[1]/ns1:out_acct_row[1]/ns1:contract_details[1]/ns1:contract_details_row[1]/ns1:contract_no[1]").toString()
		 
	}
	
	public String md_get_invoice_no2_m(String input,LinkedHashMap<String,String> m){
		
		ConnectDB db = new ConnectDB()
		logi.logInfo "string input : "+input
		String plan_instance = md_GET_ACCT_OR_INSTANCE_NO2_m(input,m).toString()
		String query = "select max(invoice_no) as invoice_no from ariacore.gl_detail where CLIENT_NO="+Constant.client_no+" and  master_plan_instance_no = " + plan_instance
		String invoice_no = db.executeQueryP2(query)
		if(invoice_no!=null)return invoice_no
		query = "select max(invoice_no) as invoice_no from ariacore.gl_pending_detail where CLIENT_NO="+Constant.client_no+" and master_plan_instance_no = " + plan_instance
		invoice_no = db.executeQueryP2(query)
		return invoice_no
  }
	def md_GET_BG_NO2_M(String input,LinkedHashMap<String,String> m) throws Exception
	{
		 def BGNO
		 ConnectDB db=new ConnectDB()
		 String instance = md_GET_ACCT_OR_INSTANCE_NO2_m(input,m).toString()
		 String Query
		 Query ="select BILLING_GROUP_NO from ariacore.acct_billing_group where  acct_no in ((Select acct_no from ariacore.plan_instance where PLAN_INSTANCE_NO = "+instance+"))"
		 logi.logInfo Query
		 BGNO=db.executeQueryP2(Query).toString()
		 return BGNO
	}
	
	/**
	 * it is uses to get the same time stamp to reuse it from the above module md_unique_id_m
	 * @param input
	 * @return the UID
	 * @throws Exception
	 */
	def md_get_UniqueID_data_m(String input,LinkedHashMap<String,String> m) throws Exception{
		
		return library.Constants.Constant.UniqueID.get(input)
	}
	
	/**
	 * this is very much specific for usage module please dont use this
	 * @param input
	 * @return the acct_no
	 * @throws Exception
	 */
	def md_get_acct_frm_PI_m(String input,LinkedHashMap<String,String> m)
	{
		ConnectDB db = new ConnectDB()
		
		return db.executeQueryP2("SELECT acct_no from ariacore.Plan_instance where PLAN_INSTANCE_NO="+md_get_UniqueID_data_m(library.Constants.Constant.TESTCASEID+"_"+input,null)+" and CLIENT_NO= "+Constant.client_no).toString()
	}
	
	public String md_get_invoice_duplicate_no_m(String input,LinkedHashMap<String,String> m){
		//this method is specific to usage module please dont use it
		ConnectDB db = new ConnectDB()
		logi.logInfo "string input : "+input
		String plan_instance = md_GET_ACCT_OR_INSTANCE_NO_m(input,m).toString()
		String query = "select max(invoice_no) as invoice_no from ariacore.gl_detail where CLIENT_NO="+Constant.client_no+" and  master_plan_instance_no = " + md_get_UniqueID_data_m(library.Constants.Constant.TESTCASEID+"_"+input,null)
		String invoice_no = db.executeQueryP2(query)
		if(invoice_no!=null)return invoice_no
		query = "select max(invoice_no) as invoice_no from ariacore.gl_pending_detail where CLIENT_NO="+Constant.client_no+" and master_plan_instance_no = " + md_get_UniqueID_data_m(library.Constants.Constant.TESTCASEID+"_"+input,null)
		invoice_no = db.executeQueryP2(query)
		return invoice_no
  }
	
		public String md_Trans_ID_from_acct_m(String input,LinkedHashMap<String,String> m)
		{
			   logi.logInfo("******* into md_Trans_ID_from_acct_m *******")
			   ConnectDB db = new ConnectDB()
			   String acct_no=md_GET_ACCT_OR_INSTANCE_NO_m(input.split ("#")[0],m).toString()
			   logi.logInfo("acct_no "+acct_no)
			   
			   String Trans_ID="select max(EVENT_NO) from ariacore.ACCT_TRANSACTION where ACCT_NO="+acct_no
			   String Trans_ID1=db.executeQueryP2(Trans_ID)
			   logi.logInfo("Trans_ID "+Trans_ID1)
			   return Trans_ID1
			   
							
		}
		
	
	
	
	public String md_DG_No_from_acct_m(String input,LinkedHashMap<String,String> m)
		{
			   logi.logInfo("******* into md_DG_No_from_acct_m *******")
			   ConnectDB db = new ConnectDB()
			   String acct_no=md_GET_ACCT_OR_INSTANCE_NO_m(input.split ("#")[0],m).toString()
			   logi.logInfo("acct_no "+acct_no)
			   
		
			   String dgno="select a.DUNNING_GROUP_NO from(SELECT DUNNING_GROUP_NO,row_number() over(order by CLIENT_NO ) as seq from ariacore.acct_dunning_group where acct_no="+acct_no+")a where a.seq="+input.split("#")[1]
			   logi.logInfo("dgno "+dgno)
			   
			   String DunningNo=db.executeQueryP2(dgno)
			   logi.logInfo("Dunning_no "+DunningNo)
			   return DunningNo
			   
							
		}
	
		
		public  String md_xml_stmt_no_m(String input,LinkedHashMap<String,String> m)
		{
			   InputMethods im = new InputMethods()
			   logi.logInfo("In md_xml_stmt_no_m ")
			   //client no
			  // def client_no=Constant.mycontext.expand('${Properties#Client_No}')
			  // logi.logInfo("CLINET "+client_no)
			   //acct no
			  //def acct_no=vm.getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
			   def acct_no= im.md_GET_ACCT_OR_INSTANCE_NO_m(input,null).toString()
	
			   //db connetion
			   logi.logInfo("ACCT NOOO "+acct_no)
			   ConnectDB db = new ConnectDB()
			   
			   String xml_statementno="select XML_STATEMENT_NO from ariacore.xml_document where acct_no="+acct_no
			   String statement_no = db.executeQueryP2(xml_statementno)
			   logi.logInfo("statement_no "+statement_no)
			   
			   return statement_no
			   
			   
		}
	 /**
			* this method is for passing the query string to the ObjQuery API
			* @param the short form of the field name , multiple field can be passed
			* @return all the required fields to be in the query string
			*/
		   public String md_query_string_for_get_coupon_history_m(String param,LinkedHashMap<String,String> m)
		   {
			   ConnectDB db = new ConnectDB()
			   logi.logInfo "string input : "+param
			   String field_names = param.split("#")[0];
			   String acct_no=md_GET_ACCT_OR_INSTANCE_NO_m(param.split("#")[1],m)
			   String MPI=md_GET_ACCT_OR_INSTANCE_NO_m(param.split("#")[1],m);
			   String []field = field_names.split(",");
			   logi.logInfo "Field Value "+field.toString()
			   String fields_needed = "";
			   String output="";
			   for(String fld : field)
			   {
				   
					   if(fld.contains("acctno"))  fields_needed+=", "+"act.acct_no";
					   if(fld.contains("usrid"))   fields_needed+=", "+"act.userid as user_id";
					   if(fld.contains("cactid"))  fields_needed+=", "+"act.client_acct_id";
					   if(fld.contains("pin"))     fields_needed+=", "+"pi.master_plan_instance_no as master_plan_instance_id";
					   if(fld.contains("cpii"))    fields_needed+=", "+"pi.plan_instance_cdid as client_master_plan_instance_id";
					   if(fld.contains("ccrd"))     fields_needed+=", "+"TO_CHAR(picm.create_date, 'YYYY-MM-DD') as coupon_create_date";
					   if(fld.contains("ccld"))     fields_needed+=", "+"TO_CHAR(picm.CANCEL_DATE, 'YYYY-MM-DD') as coupon_cancel_date";
					   if(fld.contains("ccd"))      fields_needed+=", "+"picm.coupon_cd";
				   
			   }
			   fields_needed=fields_needed.replaceFirst(",", "")
			   logi.logInfo "Running Input fields Query "+fields_needed
			   logi.logInfo "Running Input fields Query "
			   String Query = null
			   if(Constant.TESTCASEID.equals("TC_OBJ_001")){
			   Query = "SELECT "+fields_needed+" FROM ariacore.ACCT_COUPONS ac JOIN ariacore.plan_instance pi ON pi.acct_no =ac.acct_no JOIN ariacore.acct act ON pi.acct_no = act.acct_no WHERE act.client_no = "+library.Constants.Constant.client_no+" and ac.acct_no = "+acct_no+""
			   }
			   else
			   {
				   Query = "SELECT "+fields_needed+" FROM ariacore.Plan_Inst_Coupon_Map picm JOIN ariacore.plan_instance pi ON pi.plan_instance_no =picm.plan_instance_no JOIN ariacore.acct act ON pi.acct_no = act.acct_no WHERE act.client_no = "+library.Constants.Constant.client_no+" and picm.plan_instance_no = "+MPI+""
			   }
			   //String Query = "SELECT "+fields_needed+" FROM ariacore.Plan_Inst_Coupon_Map picm JOIN ariacore.plan_instance pi ON pi.plan_instance_no =picm.plan_instance_no AND pi.client_no  = picm.client_no JOIN ariacore.acct act ON pi.acct_no = act.acct_no WHERE act.client_no ="+library.Constants.Constant.client_no+" and pi.master_plan_instance_no="+MPI+""
			   ResultSet resultSet = db.executePlaQuery(Query)
			   ResultSetMetaData md = resultSet.getMetaData()
			   int columns = md.getColumnCount();
			   
			   while (resultSet.next())
			   {
				   
				   for(int u=1; u<=columns; ++u)
				   {
					   output+=" and ";
					   if(resultSet.getObject(u)!=null)
					   {
						   if(Constant.TESTCASEID.equals("TC_OBJ_006"))
						   {
							   output+=md.getColumnName(u).toLowerCase()+" < "+resultSet.getObject(u);
						   }
						   else if(md.getColumnName(u).toLowerCase().equals("coupon_cancel_date") || md.getColumnName(u).toLowerCase().equals("coupon_create_date"))
						   {
							   output+=md.getColumnName(u).toLowerCase()+" > "+resultSet.getObject(u);
						   }
						   
						   else
						   {
						   output+=md.getColumnName(u).toLowerCase()+" = "+resultSet.getObject(u);
						   }
					   }
				   }
				   
			   }
			   output=output.replaceFirst("and", "")
			   output=output.trim();
			   logi.logInfo "query string : "+output
			   return output;
		   }
		  
		  
		  /**
			* this method is for passing the query string to the ObjQuery API
			* @param the short form of the field name , multiple field can be passed
			* @return all the required fields to be in the query string
			*/
			 public String md_query_string_for_get_invoice_information_m(String param,LinkedHashMap<String,String> m)
			 {
				 logi.logInfo "********md_query_string_for_get_invoice_information_m Starts**********"
				   ConnectDB db = new ConnectDB()
				   logi.logInfo "string input : "+param
				   String field_names = param.split("#")[0];
				   String MPI=md_GET_ACCT_OR_INSTANCE_NO_m(param.split("#")[1],m);
				   String []field = field_names.split(",");
				   String fields_needed = "";
				   String output="";
				   logi.logInfo "Before entering loop"
				   for(String fld : field)
				   {
							   if(fld.contains("invno"))   fields_needed+=", "+" ai.invoice_no";
							   if(fld.contains("acctno"))  fields_needed+=", "+" ai.acct_no";
							   if(fld.contains("usrid"))   fields_needed+=", "+" act.userid as user_id";
							   if(fld.contains("cactid"))  fields_needed+=", "+" act.CLIENT_ACCT_ID";
							   if(fld.contains("amt"))  fields_needed+=", "+" ai.debit as amount";
							  // if(fld.contains("it"))      fields_needed+=", "+" ai.REAL_PEND_IND as invoice_type";
							   if(fld.contains("billdt"))  fields_needed+=", "+" TO_CHAR(ai.bill_date, 'YYYY-MM-DD') as bill_date";
							   if(fld.contains("duedt"))  fields_needed+=", "+" TO_CHAR(ai.due_date, 'YYYY-MM-DD') as due_date";
							   if(fld.contains("paiddt"))  fields_needed+=", "+" TO_CHAR(ai.paid_date, 'YYYY-MM-DD') as paid_date";
							   if(fld.contains("tldue"))  fields_needed+=", "+" ai.total_due";
							   if(fld.contains("lstupd"))  fields_needed+=", "+" TO_CHAR(ai.UPDATE_DATE, 'YYYY-MM-DD') AS last_updated";
							   if(fld.contains("stmtno"))  fields_needed+=", "+" actst.STATEMENT_NO as aria_statement_no";
							   if(fld.contains("actstmtseq"))  fields_needed+=", "+" actst.acct_statement_seq_str";
							   if(fld.contains("secactstseq"))  fields_needed+=", "+" actst.second_acct_statement_seq_str";
							   if(fld.contains("csl"))     fields_needed+=", "+" ai.CUSTOM_STATUS_LABEL";
							   if(fld.contains("cn"))      fields_needed+=", "+" ai.CLIENT_NOTES";
							   if(fld.contains("invtypcd"))      fields_needed+=", "+" ai.invoice_type_code as invoice_type_cd";
							   if(fld.contains("transtype"))      fields_needed+=", "+"tt.DESCRIPTION   AS TRANSACTION_TYPE";
							   if(fld.contains("mpib"))    fields_needed+=", "+" aid.master_plan_instance_no as master_plan_instance_id";
							   if(fld.contains("cmpid"))   fields_needed+=", "+" pi.PLAN_INSTANCE_CDID as client_master_plan_instance_id";
							   if(fld.contains("itid"))      fields_needed+=", "+" atn.EVENT_NO as invoice_transaction_id";
							   if(fld.contains("ponum"))     fields_needed+=", "+" aid.PURCHASE_ORDER_NO as po_num";
				   }
				   fields_needed=fields_needed.replaceFirst(",", "")
				   logi.logInfo "Running Input fields Query "
				   String Query = null;
				   if(fields_needed.contains("ai.total_due")){
					   Query = "SELECT DISTINCT "+fields_needed+" from ariacore.gl ai join ariacore.acct act on act.client_no=ai.client_no and ai.acct_no=act.acct_no left  join ariacore.gl_detail aid on aid.client_no=act.client_no and aid.invoice_no=ai.invoice_no JOIN ariacore.acct_statement actst ON actst.client_no   =act.client_no join ariacore.acct_transaction atn on atn.client_no=aid.client_no and atn.invoice_no=aid.invoice_no and aid.INVOICE_DETAIL_NO=atn.SOURCE_NO JOIN ariacore.transaction_types tt ON tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE join ariacore.plan_instance pi on pi.client_no=aid.client_no and pi.PLAN_INSTANCE_NO=aid.master_plan_instance_no where act.client_no = "+library.Constants.Constant.client_no+" and ai.acct_no = "+MPI+" and rownum = 1"
				   }
				   else if(fields_needed.contains("amt"))
				   {
					   Query = "select max(DEBIT) as amount from ariacore.all_invoice_details where invoice_no = (select invoice_no  from ariacore.all_invoices where acct_no  = "+MPI+" )"
				   }
				   else{
				   Query = "SELECT DISTINCT "+fields_needed+" from ariacore.gl ai join ariacore.acct act on act.client_no=ai.client_no and ai.acct_no=act.acct_no left  join ariacore.gl_detail aid on aid.client_no=act.client_no and aid.invoice_no=ai.invoice_no JOIN ariacore.acct_statement actst ON actst.client_no   =act.client_no AND actst.acct_no    =act.acct_no AND actst.INVOICE_NO = ai.INVOICE_NO join ariacore.acct_transaction atn on atn.client_no=aid.client_no and atn.invoice_no=aid.invoice_no and aid.INVOICE_DETAIL_NO=atn.SOURCE_NO JOIN ariacore.transaction_types tt ON tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE join ariacore.plan_instance pi on pi.client_no=aid.client_no and pi.PLAN_INSTANCE_NO=aid.master_plan_instance_no where act.client_no = "+library.Constants.Constant.client_no+" and ai.acct_no = "+MPI+" and rownum = 1"
				   }
				   logi.logInfo "After Running Input fields Query "
				   ResultSet resultSet = db.executePlaQuery(Query)
				   ResultSetMetaData md = resultSet.getMetaData()
				   int columns = md.getColumnCount();
				   
				   while (resultSet.next())
				   {
						 
						 for(int u=1; u<=columns; ++u)
						 {
							   output+=" and ";
							   if(resultSet.getObject(u)!=null)
									 output+=md.getColumnName(u).toLowerCase()+" = "+resultSet.getObject(u);
						 }
						 
				   }
				   output=output.replaceFirst("and", "")
				   output=output.trim();
				   logi.logInfo "query string : "+output
				   return output;
			 }
			

			
							
				/**
				 * this method is for passing the query string to the ObjQuery API
				 * @param the short form of the field name , multiple field can be passed
				 * @return all the required fields to be in the query string
				 */
				public String md_query_string_for_get_plan_instance_information_m(String param,LinkedHashMap<String,String> m)
				{
					ConnectDB db = new ConnectDB()
					logi.logInfo "string input : "+param
					String field_names = param.split("#")[0];
					String MPI=md_GET_ACCT_OR_INSTANCE_NO_m(param.split("#")[1],m);
					String []field = field_names.split(",");
					String fields_needed = "";
					String output="";
					for(String fld : field)
					{
						
							if(fld.contains("acctno"))  fields_needed+=", "+"act.acct_no";
							if(fld.contains("usrid"))   fields_needed+=", "+"act.userid as user_id";
							if(fld.contains("cactid"))  fields_needed+=", "+"act.client_acct_id";
							if(fld.contains("pin"))     fields_needed+=", "+"pi.plan_instance_no";
							if(fld.contains("cpii"))    fields_needed+=", "+"pi.plan_instance_cdid as client_plan_instance_id";
							if(fld.contains("lbd"))     fields_needed+=", "+"TO_CHAR(pi.last_bill_date, 'YYYY-MM-DD') as last_bill_date";
							if(fld.contains("nbd"))     fields_needed+=", "+"TO_CHAR(pi.next_bill_date, 'YYYY-MM-DD') as next_bill_date";
							if(fld.contains("btd"))     fields_needed+=", "+"TO_CHAR(pi.LAST_BILL_THRU_DATE, 'YYYY-MM-DD') as bill_thru_date";
							if(fld.contains("sc"))      fields_needed+=", "+"pi.status_cd";
							if(fld.contains("pfn"))     fields_needed+=", "+"cosf.FIELD_NAME as product_field_name";
							if(fld.contains("pfv"))     fields_needed+=", "+"pipf.VALUE_TEXT as product_field_value";
							if(fld.contains("pn"))      fields_needed+=", "+"pi.plan_no";
							if(fld.contains("cpid"))    fields_needed+=", "+"cp.CLIENT_PLAN_ID";
							if(fld.contains("mpib"))    fields_needed+=", "+"pim.STACK_BALANCE as master_plan_instance_balance";
						
					}
					fields_needed=fields_needed.replaceFirst(",", "")
					String Query = "";
					if(fields_needed.contains("cosf.FIELD")||fields_needed.contains("pipf.VALUE"))
					{Query = "SELECT "+fields_needed+"  from ariacore.acct act join ariacore.plan_instance pi on pi.acct_no=act.acct_no and pi.client_no=act.client_no join ariacore.client_plan cp on cp.client_no=act.client_no and cp.plan_no=pi.plan_no join ariacore.plan_instance_master pim on pim.client_no=cp.client_no and pim.PLAN_INSTANCE_NO=pi.PLAN_INSTANCE_NO join ariacore.acct_billing_group abg on abg.client_no=pi.client_no and abg.billing_group_no=pim.billing_group_no and abg.acct_no=pi.acct_no join ariacore.PLAN_INST_PRODUCT_FIELDS_VAL pipf on pipf.client_no=act.client_no and pipf.plan_instance_no=pi.plan_instance_no join ariacore.client_obj_supp_fields cosf on cosf.client_no=pipf.client_no and pipf.field_no=cosf.field_no where act.client_no = "+library.Constants.Constant.client_no+" and pi.plan_instance_no = "+MPI}
					else{Query = "SELECT "+fields_needed+"  from ariacore.acct act join ariacore.plan_instance pi on pi.acct_no=act.acct_no and pi.client_no=act.client_no join ariacore.client_plan cp on cp.client_no=act.client_no and cp.plan_no=pi.plan_no join ariacore.plan_instance_master pim on pim.client_no=cp.client_no and pim.PLAN_INSTANCE_NO=pi.PLAN_INSTANCE_NO join ariacore.acct_billing_group abg on abg.client_no=pi.client_no and abg.billing_group_no=pim.billing_group_no and abg.acct_no=pi.acct_no where act.client_no = "+library.Constants.Constant.client_no+" and pi.plan_instance_no = "+MPI}
					ResultSet resultSet = db.executePlaQuery(Query)
					ResultSetMetaData md = resultSet.getMetaData()
					int columns = md.getColumnCount();
					
					while (resultSet.next())
					{
						
						for(int u=1; u<=columns; ++u)
						{
							output+=" and ";
							if(resultSet.getObject(u)!=null)
								output+=md.getColumnName(u).toLowerCase()+" = "+resultSet.getObject(u);
						}
						
					}
					output=output.replaceFirst("and", "")
					output=output.trim();
					logi.logInfo "query string : "+output
					return output;
				}
				String md_invoice_transaction_id(String input,LinkedHashMap<String,String> m){
					ConnectDB db = new ConnectDB()
					logi.logInfo "string input 1 : "+input.split(",")[0];
					String a = input.split(",")[0];
					logi.logInfo "string input 2 : "+input.split(",")[1];
					String i = input.split(",")[1];
					String acct_no = md_GET_ACCT_OR_INSTANCE_NO_m(a,m).toString()
					String query = "SELECT PAY.INVOICE_TRANSACTION_ID FROM (SELECT AP.PAYMENT_TRANS_EVENT_NO AS INVOICE_TRANSACTION_ID, ROW_NUMBER() OVER (ORDER BY AP.PAYMENT_TRANS_EVENT_NO DESC) AS SEQ FROM ARIACORE.ALL_PAYMENTS AP JOIN ARIACORE.ACCT_TRANSACTION AT ON AP.PAYMENT_ID = AT.SOURCE_NO AND AP.CLIENT_NO = AT.CLIENT_NO WHERE AP.ACCT_NO = "+acct_no+")PAY WHERE PAY.SEQ = "+i
					String invoice_trans_no = db.executeQueryP2(query)
					if(invoice_trans_no!=null){return invoice_trans_no}
					else{
					return null}
				}
				public boolean archiveExistingMPI(String client_Number){
					boolean successFlag = false
					db = new ConnectDB();
					String archiveAccountQry = "UPDATE ARIACORE.plan_instance set status_cd = -99 where client_no = "+client_Number+" and status_cd not in (-99)"
			
					if(db.statement.executeUpdate(archiveAccountQry) > 0){
						successFlag = true
					}
					return successFlag
				}
			
				public void archiveAllExistingMPI(List clientNumber){
					int clientCount = clientNumber.size()
					boolean archivedFlag
					for(int i=0; i<clientCount; i++){
						archivedFlag = archiveExistingMPI(clientNumber.get(i))
						if(archivedFlag == true){
							logi.logInfo "Archived all the existing accounts in client : "+clientNumber.get(i)
						} else{
							logi.logInfo "Unable to Archive the existing accounts in client : "+clientNumber.get(i)
						}
					}
				}
				
				/**
				 * this method is for passing the query string to the ObjQuery API
				 * @param the short form of the field name , multiple field can be passed
				 * @return all the required fields to be in the query string
				 */
				public String md_query_string_for_get_all_invoice_information_m(String param,LinkedHashMap<String,String> m)
				{
					ConnectDB db = new ConnectDB()
					logi.logInfo "string input : "+param
					boolean OR = false;
					if(param.contains("-")) {OR = true;param=param.replaceAll("-","")}
					String field_names = param.split("#")[0];
					String MPI=md_GET_ACCT_OR_INSTANCE_NO_m(param.split("#")[1],m);
					String []field = field_names.split(",");
					String fields_needed = "";
					String output="";
					for(String fld : field)
					{
							if(fld.contains("invno"))   fields_needed+=", "+"ai.invoice_no";
							if(fld.contains("acctno"))  fields_needed+=", "+"ai.acct_no";
							if(fld.contains("usrid"))   fields_needed+=", "+"act.userid as user_id";
							if(fld.contains("cactid"))  fields_needed+=", "+"act.CLIENT_ACCT_ID";
							if(fld.contains("it"))      fields_needed+=", "+"ai.REAL_PEND_IND as invoice_type";
							if(fld.contains("bcon"))    fields_needed+=", "+"ai.BILL_COMPANY_NAME";
							if(fld.contains("pmt"))     fields_needed+=", "+"pm.METHOD_ID as pay_method_type";
							if(fld.contains("pmn"))     fields_needed+=", "+"ai.PAY_METHOD_NAME";
							if(fld.contains("csl"))     fields_needed+=", "+"ai.CUSTOM_STATUS_LABEL";
							if(fld.contains("cn"))      fields_needed+=", "+"ai.CLIENT_NOTES";
							if(fld.contains("mpid"))    fields_needed+=", "+"aid.master_plan_instance_no as master_plan_instance_id";
							if(fld.contains("clmplid")) fields_needed+=", "+"pi.PLAN_INSTANCE_CDID as client_master_plan_instance_id";
							if(fld.contains("intrid"))	fields_needed+=", "+"atn.EVENT_NO as invoice_transaction_id";
							if(fld.contains("ponum"))	fields_needed+=", "+"aid.PURCHASE_ORDER_NO as po_num";
					}
					fields_needed=fields_needed.replaceFirst(",", "")
					String Query = "SELECT "+fields_needed+" from ariacore.all_invoices ai join ariacore.acct act on act.client_no=ai.client_no and ai.acct_no=act.acct_no join ariacore.PAYMENT_METHODS pm on pm.METHOD_NAME=ai.PAY_METHOD_NAME left  join ariacore.all_invoice_details aid on aid.client_no=act.client_no and aid.invoice_no=ai.invoice_no left join ariacore.acct_transaction atn on atn.client_no=aid.client_no and atn.invoice_no=aid.invoice_no and aid.INVOICE_DETAIL_NO=atn.SOURCE_NO join ariacore.plan_instance pi on pi.client_no=aid.client_no and pi.PLAN_INSTANCE_NO=aid.master_plan_instance_no where act.client_no = "+library.Constants.Constant.client_no+" and ai.acct_no = "+MPI
					
					ResultSet resultSet = db.executePlaQuery(Query)
					ResultSetMetaData md = resultSet.getMetaData()
					int columns = md.getColumnCount();
					
					while (resultSet.next())
					{
						
						for(int u=1; u<=columns; ++u)
						{
							output+=" and ";
							if(resultSet.getObject(u)!=null)
								output+=md.getColumnName(u).toLowerCase()+" = "+resultSet.getObject(u);
						}
						break;
					}
					output=output.replaceFirst("and", "")
					output=output.trim();
					if(OR==true){output=output.replaceAll("and","or")}
					logi.logInfo "query string : "+output
					if(output.contains("pay_method_name")||output.contains("bill_company_name")){output=output.replaceAll("= ","= \"")+"\"";}
					return output;
				}
				
				/**
				 * method to get the data from previous response
				 * @param input
				 * @return
				 */
				public String md_getValFromPrevResponse_m(String input,LinkedHashMap<String,String> m)
				{
					EOMVerificationMethods vm = new EOMVerificationMethods();
							return vm.md_getValFromNodeAndIndex_m(input.replaceAll("#","\\|"));
				}
				
				/**
				 * to get the write_off number of a acct after creating writeoff/Dispute
				 * @return
				 */
				public String md_get_write_off_number_m(String input,LinkedHashMap<String,String> m)
				{
					ConnectDB db = new ConnectDB()
					return db.executeQueryP2("SELECT EVENT_NO from (SELECT EVENT_NO,row_number() over (order by event_no) as seq from ariacore.acct_transaction where client_no = "+library.Constants.Constant.client_no+" and acct_no = "+md_GET_ACCT_OR_INSTANCE_NO_m(input.split("#")[0],m).toString()+" and TRANSACTION_TYPE = 6 )where seq="+input.split("#")[1]).toString()
				}
				
				/**Getting transaction id for given account and transaction type
				* @param str (Ex : 1#a*3)
				* @return transactionId
				*/
				def md_GET_PID_NO_M(String input,LinkedHashMap<String,String> m) throws Exception
				{
												  def BGNO
												  ConnectDB db=new ConnectDB()
												  logi.logInfo("caling md_GET_PID_NO_M :: "+input)
												  try {
													  String transType=null
												  String acctString,acct_no=null
												  if(input.contains("*"))
												  {
													  
													  logi.logInfo("contains")
												  transType=input.split("\\*")[1]
												  logi.logInfo("transType"+transType)
												  acctString=input.split("\\*")[0]
												  }
												  else
												  acctString=input
												  logi.logInfo("acctString"+acctString)
												  if(acctString.contains("#"))
												  acct_no=md_GET_ACCT_OR_INSTANCE_NO2_m(acctString,null)
												  else
												  acct_no=md_GET_ACCT_OR_INSTANCE_NO_m(acctString,null)
												  logi.logInfo("Fetched acct_no"+acct_no)
												
												  String Query
												  Query ="select max(event_no) from ariacore.acct_transaction where TRANSACTION_TYPE=%s and acct_no=%s"
												  logi.logInfo Query
												  BGNO=db.executeQueryP2(String.format(Query,transType,acct_no))
												  return BGNO
												} catch (Exception e) {
													 logi.logInfo "Input method exception"+e.getMessage()
												}
				}
				
				/**
				 * it is uses to get the same DynamicData to reuse it from the above module md_unique_id_m
				 * @param input
				 * @return the UID
				 * @throws Exception
				 */
				def md_get_DynamicData_m(String input,LinkedHashMap<String,String> m) throws Exception{
					def UID
					return library.Constants.Constant.DynamicData.get(input)
				}
				
				String md_credit_Number_m(String input,LinkedHashMap<String,String> m) throws Exception
				{
					logi.logInfo("into md_credit_Number_m" + input)
					String acct_no= md_GET_ACCT_NO_m(input.split (",")[0],m).toString()
					db=new ConnectDB()
					return db.executeQueryP2("select credit_id from ariacore.all_credits where acct_no="+acct_no)
				}
			
				
				String md_get_dunning_group_no_m(String acct_hierarchy1,LinkedHashMap<String,String> m)
				{
					
					logi.logInfo("**** md_get_dunning_group_no_m *****")
					ConnectDB db = new ConnectDB()
					String acct_hierarchy= acct_hierarchy1.split("#")[0]
					String dg_name= acct_hierarchy1.split("#")[1]
					String acct_no = md_GET_ACCT_NO_m(acct_hierarchy,m).toString()
					String out= db.executeQueryP2("select DUNNING_GROUP_NO from ARIACORE.ACCT_DUNNING_GROUP where ACCT_NO="+acct_no+" and DUNNING_GROUP_NAME='"+dg_name+"'")
					return out
				}
				
				def md_endOfBillingDate_m(String clientPlanId,LinkedHashMap<String,String> m)throws Exception
				{
					  logi.logInfo("into md_endOfBillingDate_m " +clientPlanId)
					  //def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		  
		  //logi.logInfo("into md_endOfBillingDate_m " +client_no)
		  ConnectDB database = new ConnectDB();
		  logi.logInfo("i1")
		  SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd")
		  String Query = "SELECT TRUNC(ARIACORE.ARIAVIRTUALTIME(7000209)) from DUAL"
		  String currentVirtualTime = database.executeQueryP2(Query);
		  logi.logInfo("i2")
		  Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd").parse(currentVirtualTime);
		  logi.logInfo("Current VT - " +currentVTDate)
		  String Query1 = "select bi.recur_billing_interval from ariacore.NEW_RATE_SCHEDULES bi join ariacore.client_plan cp on bi.plan_no=cp.plan_no where cp.client_plan_id ='"+clientPlanId+"'"
		  String billingPeriod=database.executeQueryP2(Query1);
		  logi.logInfo("billingPeriod - " +billingPeriod)
		  Calendar cal = Calendar.getInstance();
		  cal.setTime(currentVTDate);
		  logi.logInfo("Current VT " +cal.getTime())
		  cal.add(Calendar.DATE, -1);
		  logi.logInfo("Current date -1 " +cal.getTime())
		  cal.add(Calendar.MONTH, +(billingPeriod.toInteger()))
		  logi.logInfo("Current date + Billing period " +cal.getTime())
		  String formatedDate = simpleFormat.format(cal.getTime())
		  logi.logInfo("formatedDate " + formatedDate)
		  return(formatedDate);
		  }
				
		def md_GET_PID_NO2_M(String input,LinkedHashMap<String,String> m) throws Exception
		{
			def BGNO
			ConnectDB db=new ConnectDB()
																																	 
			String account_no = md_GET_ACCT_OR_INSTANCE_NO2_m("1#a",m).toString()
			String Query
			Query ="select max(event_no) from ariacore.acct_transaction where TRANSACTION_TYPE="+input+" and acct_no="+account_no
			logi.logInfo Query
			BGNO=db.executeQueryP2(Query).toString()
			return BGNO
		}

		/** for retrieving the event no of balance transfer in parent child relation
		 * @param input 1#a
		 * @param m null
		 * @return max of event no
		 */
		String md_invoice_transaction2_id(String input,LinkedHashMap<String,String> m){
			ConnectDB db = new ConnectDB()
			logi.logInfo "string input 1 : "+input.split(",")[0];
			String a = input.split(",")[0];
			logi.logInfo "string input 2 : "+input.split(",")[1];
			String i = input.split(",")[1];
			String acct_no = null;
			if(a.contains("#")){
				 acct_no = md_GET_ACCT_OR_INSTANCE_NO2_m(a,m).toString()
			}else{
			 acct_no = md_GET_ACCT_OR_INSTANCE_NO_m(a,m).toString()
			}
			String query = "SELECT max(AT.EVENT_NO) FROM ARIACORE.ACCT_TRANSACTION AT WHERE AT.ACCT_NO = "+acct_no
			String invoice_trans_no = db.executeQueryP2(query)
			if(invoice_trans_no!=null){return invoice_trans_no}
			else{
			return null}
			
		}

	public String md_GET_PLAN_NO_Admin(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_PLAN_NO_Admin")
		String planNo = null
		String tcid= Constant.TESTCASEID
		String plan_no = vm.getValueFromResponse(type, ExPathAdminToolsApi.CREATE_PLAN_M_PLAN_NO1)

		logi.logInfo(plan_no)
		return plan_no
	}

	public String md_GET_PLAN_NO_Admin_DB_m(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_PLAN_NO_Admin_DB")
		//String planNo = null
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def planNo
		ConnectDB db=new ConnectDB()
		String Query
		Query ="select plan_no  from(select row_number() over(order by plan_no desc) as seq_no,plan_no from ARIACORE.CLIENT_PLAN WHERE CLIENT_NO = "+client_no+" )where SEQ_NO = "+type
		logi.logInfo Query
		planNo=db.executeQueryP2(Query).toString()
		return planNo
	}
	public String md_GET_PLAN_Name_Admin_DB_m(String type,LinkedHashMap m)
	{
		logi.logInfo("method md_GET_PLAN_Name_Admin_DB")
		//String planNo = null
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def planName
		ConnectDB db=new ConnectDB()
		String Query
		String plan_no = md_GET_PLAN_NO_Admin_DB_m(type,m)
		logi.logInfo"plan no is"+ plan_no
		Query ="select plan_name  from ARIACORE.CLIENT_PLAN WHERE plan_no = "+plan_no
		logi.logInfo Query
		planName=db.executeQueryP2(Query).toString()
		return planName
	}

	public String md_GET_CLIENT_RATE_SCHEDULE_ID_Admin_DB_m(String type,LinkedHashMap m)
	{
		String prefix_api=type.split("\\&")[0]
		String type_prefix=type.split("\\&")[1]
		logi.logInfo("method md_GET_CLIENT_RATE_SCHEDULE_ID_Admin_DB_m")
		//String planNo = null
		String client_no=md_GET_PLAN_NO_Admin_DB_m(prefix_api,m)
		def planNo
		ConnectDB db=new ConnectDB()
		String Query
		Query ="select Client_rate_schedule_id  from(select row_number() over(order by Client_rate_schedule_id asc) as seq_no,Client_rate_schedule_id from ARIACORE.NEW_RATE_SCHEDULES WHERE plan_no="+client_no+"  )where SEQ_NO = "+type_prefix
		logi.logInfo Query
		planNo=db.executeQueryP2(Query).toString()
		return planNo
	}

	public String md_ALT_RATE_SCHEDULE_NO_m(String type,LinkedHashMap m)
	{
		String prefix_api=type.split("\\&")[0]
		String type_prefix=type.split("\\&")[1]
		logi.logInfo("method md_ALT_RATE_SCHEDULE_NO_m")
		//String planNo = null
		String plan_no=md_GET_PLAN_NO_Admin(prefix_api,m)
		def alt_rs_no
		ConnectDB db=new ConnectDB()
		String Query
		Query ="SELECT SCHEDULE_NO FROM ARIACORE.plan_rate_schedule WHERE plan_no="+plan_no+"and Schedule_name ='"+type_prefix+"'"
		logi.logInfo Query
		alt_rs_no=db.executeQueryP2(Query).toString()
		return alt_rs_no
	}
	
	//CMRB
	/** for retrieving the credit memo no
	 * @param input 1#a
	 * @param m null
	 * @return max of event no
	 */
	String md_credit_memo_id(String input,LinkedHashMap<String,String> m){
		ConnectDB db = new ConnectDB()
		logi.logInfo "string input 1 : "+input.split(",")[0];
		logi.logInfo "string input 2 : "+input.split(",")[1];
		String acct_no = null;
		if((input.split(",")[0]).contains("#")){
			 acct_no = md_GET_ACCT_OR_INSTANCE_NO2_m(input.split(",")[0],m).toString()
		}else{
		 acct_no = md_GET_ACCT_OR_INSTANCE_NO_m(input.split(",")[0],m).toString()
		}
		String query = "SELECT CM.CM_NO FROM (SELECT CM_NO, ROW_NUMBER() OVER (ORDER BY CM_NO DESC) AS SEQ FROM ARIACORE.CREDIT_MEMO WHERE ACCT_NO = "+acct_no+")CM WHERE CM.SEQ="+input.split(",")[1]
		String cm_no = db.executeQueryP2(query)
		if(cm_no!=null){return cm_no}
		else{
		return null}
		
	}

	String md_get_inv_seq_to_rebill_m(String input,LinkedHashMap<String,String>m){
		ConnectDB db = new ConnectDB()
		logi.logInfo "String input 1 : "+input.split(",")[0];
		logi.logInfo "String input 2 : "+input.split(",")[1];
		String acct_no = null, s1 = null,query1 = null;
		if((input.split(",")[0]).contains("#")){
			acct_no = md_GET_ACCT_OR_INSTANCE_NO2_m(input.split(",")[0],m).toString()
	   }else{
		acct_no = md_GET_ACCT_OR_INSTANCE_NO_m(input.split(",")[0],m).toString()
	   }
	   if(input.split(",")[1].contains("p")){
		String seq = input.split(",")[1]
		   s1 = seq.split("p")[0]
		   query1 = "SELECT INV.INVOICE_NO FROM (SELECT INVOICE_NO,ROW_NUMBER() OVER (ORDER BY INVOICE_NO DESC)AS SEQ FROM ARIACORE.GL_PENDING WHERE ACCT_NO = "+acct_no+"  )INV WHERE INV.SEQ = "+s1
	   }else
	   {
		   s1 = input.split(",")[1]
		   query1 = "SELECT INV.INVOICE_NO FROM (SELECT INVOICE_NO,ROW_NUMBER() OVER (ORDER BY INVOICE_NO DESC)AS SEQ FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+"  )INV WHERE INV.SEQ = "+s1
	   }
	   
	 
		String inv1 = db.executeQueryP2(query1)
		return inv1
		}
	
	String md_get_usg_lineitem_seq_m(String input,LinkedHashMap<String,String>m){
		ConnectDB db = new ConnectDB()
		logi.logInfo "md_get_usg_lineitem_seq_m input 1 : "+input.split("#")[0];
		logi.logInfo "md_get_usg_lineitem_seq_m input 2 : "+input.split("#")[1];
		String str = input.split("#")[1]
		String orig_inv=md_get_inv_seq_to_rebill_m(str,null).toString()
		String query1 = "SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE SERVICE_NO = "+input.split("#")[0]+" AND INVOICE_NO = "+orig_inv
		String inv1 = db.executeQueryP2(query1)
		return inv1
	}
	
	String md_get_usg_lineitem_withplan_seq_m(String input,LinkedHashMap<String,String>m){
		ConnectDB db = new ConnectDB()
		logi.logInfo "md_get_usg_lineitem_seq_m input 1 : "+input.split("#")[0];
		logi.logInfo "md_get_usg_lineitem_seq_m input 2 : "+input.split("#")[1];
		logi.logInfo "md_get_usg_lineitem_seq_m input 3 : "+input.split("#")[2];
		String str = input.split("#")[1]
		String orig_inv=md_get_inv_seq_to_rebill_m(str,null).toString()
		String query1 = "SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE SERVICE_NO = "+input.split("#")[0]+" AND INVOICE_NO = "+orig_inv+" AND PLAN_NO = "+input.split("#")[2]
		String inv1 = db.executeQueryP2(query1)
		return inv1
	}
	
	/**to get line nos which previously has credit memos
	 * @param input service_no#acct_no,invoice_seq
	 * @param m null
	 * @return invoice line no
	 */
	String md_get_cm_line_no_m(String input,LinkedHashMap<String,String> m){
		ConnectDB db = new ConnectDB()
		logi.logInfo "md_get_usg_lineitem_seq_m input 1 : "+input.split("#")[0];
		logi.logInfo "md_get_usg_lineitem_seq_m input 2 : "+input.split("#")[1];
		String str = input.split("#")[1]
		String orig_inv=md_get_inv_seq_to_rebill_m(str,null).toString()
		String query1 = "SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL GD JOIN ARIACORE.ACCT_TRANSACTION ATN ON GD.INVOICE_DETAIL_NO = ATN.SOURCE_NO AND GD.CLIENT_NO = ATN.CLIENT_NO"+
		" WHERE INVOICE_NO = "+orig_inv+" AND SERVICE_NO = "+input.split("#")[0]
		String line_no = db.executeQueryP2(query1)
		return line_no
	}
	
	String md_get_cm_general_line_no_m(String input,LinkedHashMap<String,String> m){
		ConnectDB db = new ConnectDB()
		logi.logInfo "md_get_usg_lineitem_seq_m input 1 : "+input.split("#")[0];
		logi.logInfo "md_get_usg_lineitem_seq_m input 2 : "+input.split("#")[1];
		String str = input.split("#")[1]
		String orig_inv=md_get_inv_seq_to_rebill_m(str,null).toString()
		String query1 = "SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL "+
		" WHERE INVOICE_NO = "+orig_inv+" AND SERVICE_NO = "+input.split("#")[0]
		String line_no = db.executeQueryP2(query1)
		return line_no
	}
	
	
	String md_get_cm_pending_line_no_m(String input,LinkedHashMap<String,String> m){
		ConnectDB db = new ConnectDB()
		logi.logInfo "md_get_usg_lineitem_seq_m input 1 : "+input.split("#")[0];
		logi.logInfo "md_get_usg_lineitem_seq_m input 2 : "+input.split("#")[1];
		String str = input.split("#")[1]
		String orig_inv=md_get_inv_seq_to_rebill_m(str,null).toString()
		String query1 = "SELECT SEQ_NUM FROM ARIACORE.GL_PENDING_DETAIL GD "+
		" WHERE INVOICE_NO = "+orig_inv+" AND SERVICE_NO = "+input.split("#")[0]
		String line_no = db.executeQueryP2(query1)
		return line_no
	}
	
	/**to get line nos which does not have any previously created credit memos
	 * @param input service_no,plan_no#acct_no,invoice_seq
	 * @param m null
	 * @return invoice line no
	 */
	String md_get_cm_planline_no_m(String input,LinkedHashMap<String,String> m){
		ConnectDB db = new ConnectDB()
		logi.logInfo "md_get_usg_lineitem_seq_m input 1 : "+input.split("#")[0];
		logi.logInfo "md_get_usg_lineitem_seq_m input 2 : "+input.split("#")[1];
		String str = input.split("#")[1]
		String plan_det = input.split("#")[0]
		String service_no = plan_det.split(",")[0]
		String plan_no = plan_det.split(",")[1]
		String orig_inv=md_get_inv_seq_to_rebill_m(str,null).toString()
		String query1 = "SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL GD JOIN ARIACORE.ACCT_TRANSACTION ATN ON GD.INVOICE_DETAIL_NO = ATN.SOURCE_NO AND GD.CLIENT_NO = ATN.CLIENT_NO"+
		" WHERE INVOICE_NO = "+orig_inv+" AND SERVICE_NO = "+service_no+" AND GD.PLAN_NO = "+plan_no
		String line_no = db.executeQueryP2(query1)
		return line_no
	}

	String md_get_bill_ship_addr_seq_m(String input,LinkedHashMap<String,String> m){
		ConnectDB db = new ConnectDB()
		String val = null
		String str = input.split("#")[1]
		String orig_inv=md_get_inv_seq_to_rebill_m(str,null).toString()
		String str1 = input.split("#")[0]
		String serv_no = str1.split(",")[0]
		String ind = str1.split(",")[1]
		if(ind.equalsIgnoreCase("b")){
		val = "select BILL_TO_ADDR_SEQ from ariacore.gl_detail where invoice_no = "+orig_inv+" and service_no = "+serv_no
		}else{
		val = "select SHIP_TO_ADDR_SEQ from ariacore.gl_detail where invoice_no = "+orig_inv+" and service_no = "+serv_no
		}
		String seq_no = db.executeQueryP2(val)
		logi.logInfo "sequence no "+seq_no
		return seq_no
	}
	
	
	
	public String md_GET_SCHEDULE_NO_m(String type,LinkedHashMap m)
	{
					logi.logInfo("method md_GET_SCHEDULE_NO_m")
					String client_no, schedule_no, plan_no, Query
					client_no=Constant.mycontext.expand('${Properties#Client_No}')
					ConnectDB db=new ConnectDB()
					plan_no = vm.getValueFromResponse('create_new_plan_m',ExPathRpcEnc.CREATE_NEW_PLAN_PLANNO1)
					logi.logInfo"plan no is"+ plan_no
					Query ="select schedule_no from ARIACORE.NEW_RATE_SCHEDULES where plan_no ="+plan_no+"and client_no ="+client_no
					logi.logInfo Query
					schedule_no=db.executeQueryP2(Query).toString()
					return schedule_no
	}
	
	public String md_getProrationStartdate_m(String inp,LinkedHashMap<String,String> m)
	{
		logi.logInfo "calling md_getProrationStartdate_m "
		logi.logInfo "inp "+inp
		String numberOfDays=inp.split("\\*")[1]
		String input=inp.split("\\*")[0]
		logi.logInfo "input "+input
		String i=input.split(",")[0]
		String acct_no=md_GET_ACCT_OR_INSTANCE_NO_m(i,null)
		logi.logInfo "acct "+acct_no
		String plan_instance_no = md_GET_ACCT_OR_INSTANCE_NO_m(input,null)
		ConnectDB db=new ConnectDB();
		String q="select  to_char(hist.from_date,'yyyy-mm-dd')   from ariacore.all_acct_active_plans p join ariacore.PLAN_INSTANCE_PLAN_HIST  hist on p.plan_instance_no=hist.plan_instance_no where p.acct_no=%s  and hist.comments !='Client-defined ID Changed' and p.plan_instance_no=%s"
		logi.logInfo "into md_daysVT_m "+numberOfDays
		
		db = new ConnectDB();
		Date date = new Date();
		Calendar now = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

		
		ResultSet resultSet = db.executeQuery(String.format(q, acct_no,plan_instance_no));
		while(resultSet.next()){
			// Get only the current Date

			date = resultSet.getDate(1);
		}
		now.setTime(date);
		if(numberOfDays.contains("-")){
			if(numberOfDays.contains(".")) {
				String[] days = numberOfDays.split("\\.");
				now.add(Calendar.MONTH, Integer.parseInt(days[0]));
				now.add(Calendar.DATE, -(Integer.parseInt(days[1])));
			}
			else
				now.add(Calendar.DATE,Integer.parseInt(numberOfDays));
		}
		else{
			if(numberOfDays.contains(".")) {
				String[] days = numberOfDays.split("\\.");
				now.add(Calendar.MONTH, Integer.parseInt(days[0]));
				now.add(Calendar.DATE, Integer.parseInt(days[1]));
			}
			else
				now.add(Calendar.DATE, Integer.parseInt(numberOfDays));
		}

		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String date1 = format1.format(now.getTime());
		//db.closeConnection()
		return date1;
	}
	
	String md_get_field_no(String type,LinkedHashMap m){
		logi.logInfo("test Info")
		String fieldNo = vm.getValueFromResponse("create_supp_obj_field_m","//*/*:field_no[1]")
		return fieldNo
	}
	
	def md_plan_units_m(String input,LinkedHashMap<String,String> m) throws Exception
	{
		   def UID
		   UID=input+"_"+library.GenericLibrary.Common.getCurrentTime()
		   String str=library.Constants.Constant.UniqueID.put(input,library.GenericLibrary.Common.getCurrentTime())
		   logi.logInfo("STRR "+str+"asdasdas len "+str.length())
		   String b=str.substring(str.length()-3,str.length())
		   logi.logInfo("STRR FIANL "+b)
		   if(b.equalsIgnoreCase("0"))
		   b="1"
		   
			UID=Integer.parseInt(b)
		   
			return UID
	}

public  String md_AGREEMENT_ID_m(String input,LinkedHashMap<String,String> m)
{
InputMethods im = new InputMethods()
logi.logInfo("In md_xml_stmt_no_m ")
def acct_no= im.md_GET_ACCT_OR_INSTANCE_NO_m(input,null).toString()

//db connetion
logi.logInfo("ACCT NO "+acct_no)
ConnectDB db = new ConnectDB()

String xml_statementno="select AGREEMENT_ID from ariacore.billing_info where acct_no="+acct_no
String statement_no = db.executeQueryP2(xml_statementno)
logi.logInfo("statement_no "+statement_no)

return statement_no

}


   
   
   //dev-7039
   // billing group for multiple MPI
   public String md_get_billing_group_no_multi(String input,LinkedHashMap<String,String> m){
	   ConnectDB db = new ConnectDB()
		  logi.logInfo "in the billing group1"+input
		  String acct_no = md_GET_ACCT_OR_INSTANCE_NO_m(input.split (",")[0],m).toString()
		  logi.logInfo "mpi no" +acct_no
		  String query = "select BILLING_GROUP_NO from ariacore.ACCT_BILLING_GROUP where acct_no="+acct_no+" and PROFILE_NAME='"+ input +"'"
		  logi.logInfo query
		  String billing_group_no = db.executeQueryP2(query).toString();
		  logi.logInfo "billing group no : "+billing_group_no
		  return billing_group_no
	  }
   
   
   String md_get_entity_name_m(String type,LinkedHashMap m)
   {
	   logi.logInfo("Entering Into md_GET_ENTITY_NAME")
	   String name=vm.getValueFromHttpRequestValue("create_legal_entity_m", "//legal_entity_name").toString()
	   logi.logInfo "md_GET_ENTITY_NAME" + name
	   return name
   }
   
   String md_update_legal_entity_m(String type,LinkedHashMap m)
   {
		 logi.logInfo("method md_update_legal_entity_m")

   String legal_entity_no
   legal_entity_no = vm.getValueFromResponse("create_legal_entity_m","//*:Response[1]/*:legal_entity_no[1]")
   
   return legal_entity_no
   
   }
   
   String md_legal_entity_m(String input,LinkedHashMap m)
   {
	   logi.logInfo(" md_legal_entity_m" + input)
	   db=new ConnectDB()
	   String qry, legal_entity
	   if (input.contains("maxname"))
	   {
		   qry= "select LEGAL_ENTITY_NAME from ariacore.client_legal_entity where rownum=1 and client_no=3287679 ORDER BY LEGAL_ENTITY_NO DESC"
		   legal_entity=db.executeQueryP2(qry).toString()
	   }
	   
	   else if (input.contains("maxno"))
	   
	   {
		   qry= "select max(LEGAL_ENTITY_NO) from ariacore.client_legal_entity where client_no=3287679"
		   legal_entity=db.executeQueryP2(qry).toString()
	   }
	   
	   else if (input.contains("minname"))
	   {
		   qry= "select LEGAL_ENTITY_NAME from ariacore.client_legal_entity where rownum=1 and client_no=3287679 ORDER BY LEGAL_ENTITY_NO ASC"
		   legal_entity=db.executeQueryP2(qry).toString()
	   }
	   
	   else if (input.contains("mincid"))
	   {
		   qry= "select CLIENT_LEGAL_ENTITY_ID from ariacore.client_legal_entity where rownum=1 and client_no=3287679 ORDER BY LEGAL_ENTITY_NO ASC"
		   legal_entity=db.executeQueryP2(qry).toString()
	   }
	   else if (input.contains("maxcid"))
	   {
		   qry= "select CLIENT_LEGAL_ENTITY_ID from ariacore.client_legal_entity where rownum=1 and client_no=3287679 ORDER BY LEGAL_ENTITY_NO DESC"
		   legal_entity=db.executeQueryP2(qry).toString()
	   }
	   else if(input.contains("rnomin"))
	   
	   {
		   qry= "select REGISTRATION_NO from ariacore.client_legal_entity where rownum=1 and client_no=3287679 AND REGISTRATION_NO IS NOT NULL ORDER BY REGISTRATION_NO ASC"
		   legal_entity=db.executeQueryP2(qry).toString()
	   }
	   
	   else if (input.contains("vrnomin"))
	   {
		   qry= "select VAT_REGISTRATION_NO from ariacore.client_legal_entity where rownum=1 and client_no=3287679 AND VAT_REGISTRATION_NO IS NOT NULL ORDER BY VAT_REGISTRATION_NO ASC"
		   legal_entity=db.executeQueryP2(qry).toString()
	   }
	   
	   else if (input.contains("maxrand300"))
	   {
		   
		   
		   legal_entity = md_unique_id_m("maxrand",m)+"VANhapmljaGYXkVkkhYzH8AT4V1vOiTRbaTeAW6yI3Nqp7P1VI5NfjumHEFEOpbgvtKFeKaV9Fb02syVIluYmxpKUuybxaAhPFXRN9UvKSwJAp4cu72APao99wRGnTTvEQhTQGuaf8G9zV6ZiCCALKSEioRiBjLSOLRhncXKQynZLWeZHsAlq0h9RabACGqv3MTcFXpwN0HWDr7MvR0XaMatk4X87mn1xyoLy5GB3l12zEI3iKzCWfenSFm7UPZSXsxMJWXuUhrQwQwjepxAu";
   
	   }
	   
	   else if (input.contains("maxrand30"))
	   {
		   
		   
		   legal_entity = md_unique_id_m("maxrand",m)+"30_char";
   
	   }
	   
	   else if (input.contains("max301"))
	   {
		   
		   
		   legal_entity = md_unique_id_m("maxrand",m)+"VANhapmljaGYXkVkkhYzH8AT4V1vOiTRbaTeAW6yI3Nqp7P1VI5NfjumHEFEOpbgvtKFeKaV9Fb02syVIluYmxpKUuybxaAhPFXRN9UvKSwJAp4cu72APao99wRGnTTvEQhTQGuaf8G9zV6ZiCCALKSEioRiBjLSOLRhncXKQynZLWeZHsAlq0h9RabACGqv3MTcFXpwN0HWDr7MvR0XaMatk4X87mn1xyoLy5GB3l12zEI3iKzCWfenSFm7UPZSXsxMJWXuUhrQwQwjepxAus";
   
	   }
	   
	   else if (input.contains("acctno"))
	   {
		   qry= "select max(acct_no) from ariacore.acct where client_no=3287679"
		   legal_entity=db.executeQueryP2(qry).toString()
	   }
	   
	   return legal_entity
   }
   
   public void removeLegalEntity(List clientNumber){
	   int clientCount = clientNumber.size()
	   boolean archivedFlag
	   for(int i=0; i<clientCount; i++){
		   archivedFlag = removeUnusedLegalEntity(clientNumber.get(i))
		   if(archivedFlag == true){
			   logi.logInfo "Archived all the existing accounts in client : "+clientNumber.get(i)
		   } else{
			   logi.logInfo "Unable to Archive the existing accounts in client : "+clientNumber.get(i)
		   }
	   }
   }
   
   public boolean removeUnusedLegalEntity(String client_Number){
	   boolean successFlag = false
	   db = new ConnectDB();
	   String archiveAccountQry = "delete from ariacore.CLIENT_LEGAL_ENTITY where client_no = "+client_Number+" and USED_BY_ACCT_IND=0"

	   if(db.statement.executeUpdate(archiveAccountQry) > 0){
		   successFlag = true
	   }
	   return successFlag
   }
   
	//END OF CLASSS
}