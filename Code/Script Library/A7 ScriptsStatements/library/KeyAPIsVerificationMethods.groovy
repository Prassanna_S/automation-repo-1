package library

import java.awt.geom.Arc2D.Double;
import java.lang.reflect.Method
import java.sql.ResultSet;
import java.sql.ResultSetMetaData
import java.sql.SQLException
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import groovy.xml.StreamingMarkupBuilder

import org.apache.ivy.core.module.descriptor.ExtendsDescriptor;

import library.Constants.Constant;
import library.Constants.ExpValueConstants;
import library.Constants.ExPathRpcEnc;
import library.Constants.Constant.RESULT_TYPE;
import library.VerificationMethods
import library.ReadData

public class KeyAPIsVerificationMethods extends  UsageVerificationMethods {
	LogResult logi = new LogResult(Constant.logFilePath)
	static ConnectDB db = null;
	public DecimalFormat df=new DecimalFormat("#.##")

	public KeyAPIsVerificationMethods(ConnectDB db) {
		super(db)
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromAPI1(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromAPI(1)
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromAPI2(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromAPI(2)
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromAPI3(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromAPI(3)
	}
	HashMap md_verifyCreateOrderInvoiceDetailsFromAPI4(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromAPI(4)
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromAPI5(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromAPI(5)
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromAPI6(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromAPI(6)
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromAPI7(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromAPI(7)
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromAPI8(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromAPI(8)
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromAPI9(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromAPI(9)
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromAPI10(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromAPI(10)
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromAPI11(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromAPI(11)
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromAPI12(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromAPI(12)
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDB1(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDB(tcid,"1")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDB2(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDB(tcid,"2")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDB3(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDB(tcid,"3")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDB4(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDB(tcid,"4")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDB5(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDB(tcid,"5")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDB6(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDB(tcid,"6")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDB7(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDB(tcid,"7")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDB8(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDB(tcid,"8")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDB9(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDB(tcid,"9")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDB10(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDB(tcid,"10")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDB11(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDB(tcid,"11")
	}

	HashMap md_verifyCreateOrderInvoiceDetailsFromDB12(String tcid) {
		return  md_verifyCreateOrderInvoiceDetailsFromDB(tcid,"12")
	}

	/**
	 * Calculates Invoice detail generated from create order API
	 * @param invoice line item number
	 * @return invoice items HashMap
	 */	
	def md_verifyCreateOrderInvoiceDetailsFromAPI(int line_item_no) {
		logi.logInfo("md_verifyCreateOrderInvoiceDetailsFromAPI")
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap hm_invoice=new HashMap()
		com.eviware.soapui.support.XmlHolder outputholder
		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains('create_order') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				hm_invoice.put("LINE_NO", outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='line_no']"))
				hm_invoice.put("SERVICE_NO",outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='service_no']"))
				hm_invoice.put("SERVICE_NAME",outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='service_name']"))
				hm_invoice.put("SERVICE_IS_TAX_IND",outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='service_is_tax_ind']"))
				hm_invoice.put("LINE_UNITS",outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='line_units']"))
				hm_invoice.put("RATE_PER_UNITS",outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='rate_per_unit']"))
				hm_invoice.put("LINE_AMOUNT",outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='line_amount']"))
				hm_invoice.put("DESCRIPTION",outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='description']"))
				//hm_invoice.put("DATE_RANGE_START",outputholder.getNodeValue("//*[1]/*[local-name()='date_range_start']"))
				//hm_invoice.put("DATE_RANGE_END",outputholder.getNodeValue("//*[1]/*[local-name()='date_range_end']"))
			}
			if(hm_invoice.get("LINE_UNITS").toString() == "null")
				hm_invoice.put("LINE_UNITS","0")
			if(hm_invoice.get("RATE_PER_UNITS").toString() == "null")
				hm_invoice.put("RATE_PER_UNITS","0")
		}
		return hm_invoice.sort()
	}

	/**
	 * Calculates Total Amount generated from create order API
	 * @param testCaseId
	 * @return invoice charges HashMap
	 */
	def md_verifyCreateOrderTotalChargesFromAPI(String tcid) {
		logi.logInfo("md_verifyCreateOrderTotalChargesFromAPI")
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap cotc_api=new HashMap()
		com.eviware.soapui.support.XmlHolder outputholder

		for(int j=0; j<testCaseCmb.size(); j++) {
			if(testCaseCmb[j].toString().contains('create_order') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID) {
				outputholder = xmlValues[j]
				cotc_api.put("TOTAL_CHARGES_BEFORE_TAX", outputholder.getNodeValue("//*[1]/*[local-name()='total_charges_before_tax']"))
				cotc_api.put("TOTAL_TAX_CHARGES",outputholder.getNodeValue("//*[1]/*[local-name()='total_tax_charges']"))
				cotc_api.put("TOTAL_CHARGES_AFTER_TAX",outputholder.getNodeValue("//*[1]/*[local-name()='total_charges_after_tax']"))
				cotc_api.put("TOTAL_CREDITS",outputholder.getNodeValue("//*[1]/*[local-name()='total_credit']"))
			}
			if (cotc_api.get("TOTAL_TAX_CHARGES").toString() == "null")
				cotc_api.put("TOTAL_TAX_CHARGES","0")
			else if (cotc_api.get("TOTAL_CREDITS").toString() == "null")
				cotc_api.put("TOTAL_CREDITS","0")
		}
		logi.logInfo("md_verifyCreateOrderTotalChargesFromAPI_hashresult=> "+cotc_api)
		return cotc_api.sort();
	}

	/**
	 * Verifies Create Order Invoice Details from DB
	 * @param testCaseId,Invoice Sequence Number
	 * @return Calculated invoice charges HashMap
	 */
	def md_verifyCreateOrderInvoiceDetailsFromDB(String tcid, String seq_no)
	{
		logi.logInfo("md_verifyCreateOrderInvoiceDetailsFromDB")
		String apiname=tcid.split("-")[2]
		logi.logInfo("API Name:"+apiname)
		String acct_no=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String client_no=getValueFromRequest("create_acct_complete","//client_no[1]")
		
		def inv_no=getValueFromResponse(apiname,ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)
		String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		def creditNo = getValueFromResponse('apply_service_credit',ExPathRpcEnc.APPLY_SERVICE_CREDIT_CREDITID1)
		String units = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNITS)
		String amounts = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_AMOUNTS)
		String units_discount_amounts = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNIT_DISCOUNT_AMOUNTS)
		logi.logInfo("line units " + units.toString())
		logi.logInfo("line amounts " + amounts.toString())
		logi.logInfo("line units_discount_amounts " + units_discount_amounts.toString())

		def order_name=getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_ORDER_NAME)
		String coupon_cd
		if (apiname == "create_order_with_plans")
		{
			coupon_cd = getValueFromRequest(apiname,"//*[1]/*[local-name()='coupon_code']")
			if(coupon_cd=="NoVal")
			   coupon_cd = getValueFromRequest(apiname,"//*[1]/*[local-name()='coupon_codes']")
		}
		else
			coupon_cd = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_COUPON_CD)
		String rate
		def line_no, service_name, charges, creditAmt,couponActServiceAmt
		HashMap hm_coid= new HashMap();
		ConnectDB db = new ConnectDB();
		String unapplied_amt="SELECT NVL(debit,0) from ariacore.gl_detail where invoice_no="+inv_no+" and service_no=0"
		unapplied_amt = db.executeQueryP2(unapplied_amt)
		logi.logInfo("unapplied_amt :"+unapplied_amt)
		String coupon_ind_query="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String coupon_ind = db.executeQueryP2(coupon_ind_query)
		String coupon_type_query="SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String coupon_type = db.executeQueryP2(coupon_type_query)

		String rate_qry= "Select INVIP.price from ariacore.inventory_item_prices INVIP JOIN ariacore.INVENTORY_ITEMS INVI ON invip.item_no=invi.item_no and invip.client_no=invi.client_no where INVI.client_sku='"+order_name+"' and INVI.client_no="+client_no+" and invip.currency_cd=(SELECT CURRENCY_CD FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+ " AND CLIENT_NO="+client_no+")"
		rate = db.executeQueryP2(rate_qry);
		String coupon_DR_per_query="SELECT amount FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String coupon_DR_per= db.executeQueryP2(coupon_DR_per_query)
		logi.logInfo("coupon_DR_per" + coupon_DR_per)
		
		String coupon_DB_query="SELECT amount FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Coupon_cd = '"+coupon_cd+"' AND CLIENT_NO="+client_no+")) and FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='I'"
		String coupon_DB= db.executeQueryP2(coupon_DB_query)

		String creditreduces = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = "+client_no
		String creditreducesvalue = db.executeQueryP2(creditreduces)
		logi.logInfo("Client Credit reduces = "+creditreducesvalue)



		def couponAmt = getCouponAmtFromCreateOrder(tcid)
	logi.logInfo("Coupon Amount "+couponAmt)

		String creditAmtQuery = "select NVL(amount,0) from ariacore.all_credits where acct_no="+acct_no+" and credit_type='S' and credit_id="+creditNo
		if(creditNo !="NoVal")
		{
			creditAmt=db.executeQueryP2(creditAmtQuery)
			logi.logInfo("Credit Amount "+creditAmt)
		}

		if ((!units.equals("NoVal")) && amounts.equals("NoVal") && units_discount_amounts.equals("NoVal") && (seq_no.equals("1") || seq_no.equals("5") || seq_no.equals("9")))
		{
			logi.logInfo("In Loop IF")
			if(coupon_ind.equals("I"))
			{
				if(coupon_type.equals("P"))
				{
					def cou_rate= df.format((rate.toDouble() * (coupon_DR_per.toDouble()/100))).toString()
					logi.logInfo("cou_rate "+cou_rate.toDouble())
					rate = (rate.toDouble() -(cou_rate.toDouble()))
					logi.logInfo("cou_rate1111 "+rate)
					charges =(units.toDouble() * rate.toDouble())
					logi.logInfo("cou_rate1111_charges "+charges)
					charges= df.format(charges.toDouble()).toString()
				}
				else
					charges= ((units.toInteger() * rate.toDouble())-(couponAmt.toDouble()))
			}
			else
				charges= (units.toInteger()) * rate.toDouble()
			if(coupon_DB!=null)
			charges= ((units.toInteger() * rate.toDouble())-(coupon_DB.toDouble()))
			
			hm_coid.put("LINE_UNITS",units)
			hm_coid.put("RATE_PER_UNITS",df.format(rate.toDouble()).toString())
			hm_coid.put("LINE_AMOUNT",df.format(charges.toDouble()).toString())
		}
		else if((!units.equals("NoVal")) && amounts.equals("NoVal") && units_discount_amounts.equals("NoVal") && (!coupon_cd.equals("NoVal")) && seq_no!= "1")
		{
			logi.logInfo("In loop Else")
			if(coupon_type.equals("P"))
			{
				hm_coid.put("LINE_UNITS",units)
				hm_coid.put("RATE_PER_UNITS",df.format(-(rate.toDouble() * (coupon_DR_per.toDouble()/100))).toString())
				hm_coid.put("LINE_AMOUNT",df.format(-(couponAmt.toDouble())).toString())
			}
			else
			{
				hm_coid.put("LINE_UNITS","1")
				hm_coid.put("RATE_PER_UNITS",df.format(-(couponAmt.toDouble())).toString())
				hm_coid.put("LINE_AMOUNT",df.format(-(couponAmt.toDouble())).toString())
			}

		}
		else if ((!units.equals("NoVal")) && (!amounts.equals("NoVal")) && (seq_no.equals("1") || seq_no.equals("5") || seq_no.equals("9")))
		{
			logi.logInfo("In loop else 2")
			rate = amounts
			charges= (units.toInteger()) * rate.toDouble()

			hm_coid.put("LINE_UNITS",units)
			hm_coid.put("RATE_PER_UNITS",rate)
			hm_coid.put("LINE_AMOUNT",df.format(charges.toDouble()).toString())
		}
		else if ((!units.equals("NoVal")) && (!units_discount_amounts.equals("NoVal")) && (seq_no.equals("1") || seq_no.equals("5") || seq_no.equals("9")))
		{
			logi.logInfo("In loop else 3")
			charges= (units.toInteger()) * rate.toDouble()
			hm_coid.put("LINE_UNITS",units)
			hm_coid.put("RATE_PER_UNITS",rate)
			hm_coid.put("LINE_AMOUNT",df.format(charges.toDouble()).toString())
		}

		hm_coid.put("LINE_NO", seq_no)
		String qry = "Select service_no,comments as description from ariacore.gl_detail where invoice_no="+inv_no+" and seq_num="+seq_no
		ResultSet rs_qry = db.executePlaQuery(qry);
		ResultSetMetaData md = rs_qry.getMetaData();
		int columns = md.getColumnCount();
		while (rs_qry.next()){
			for(int i=1; i<=columns; i++){
				hm_coid.put(md.getColumnName(i),rs_qry.getObject(i));
			}
		}
		rs_qry.close();

		if ((!units.equals("NoVal")) &&  (!units_discount_amounts.equals("NoVal")) && ((!seq_no.equals("1")) && (!seq_no.equals("5")) && (!seq_no.equals("9"))))
		{
			charges= -(units.toInteger() * units_discount_amounts.toInteger())
			hm_coid.put("LINE_AMOUNT",df.format(charges.toDouble()).toString())
			hm_coid.put("LINE_UNITS","0")
			hm_coid.put("RATE_PER_UNITS","0")
		}


		if((hm_coid.get("SERVICE_NO").toString() != "0"))
		{
			String qry1 = "select service_name,taxable_ind as service_is_tax_ind from ariacore.all_service where service_no="+hm_coid.get("SERVICE_NO")+" and (client_no="+client_no+" or client_no is null)"
			ResultSet rs = db.executePlaQuery(qry1);
			ResultSetMetaData rsmd = rs.getMetaData();
			int columns1 = rsmd.getColumnCount();
			while (rs.next())
			{
				for(int j=1; j<=columns1; j++)
				{
					hm_coid.put(rsmd.getColumnName(j),rs.getObject(j));
				}
			}
			rs.close();
			String external_tax_client = db.executeQueryP2("SELECT * FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO=" + client_no)
			if((creditreducesvalue!=null) || (external_tax_client != null))
			{
				hm_coid.put("SERVICE_IS_TAX_IND","0")
			}

			if((couponAmt!= null) && (units_discount_amounts.equals("NoVal")) && (apiname == "create_order_with_plans") && (!coupon_cd.equals("NoVal")) && (!seq_no.equals("1")) && (!seq_no.equals("5"))&& (!seq_no.equals("9")))
			{
				hm_coid.put("LINE_AMOUNT",df.format(-(couponAmt.toDouble())).toString())
				hm_coid.put("SERVICE_IS_TAX_IND","0")
				hm_coid.put("LINE_UNITS","1")
				hm_coid.put("RATE_PER_UNITS",df.format(-(couponAmt.toDouble())).toString())
			}
		}

		if((unapplied_amt!= null) && (units_discount_amounts.equals("NoVal")) && (coupon_cd.equals("NoVal")) && ((!seq_no.equals("1")) && (!seq_no.equals("5")) && (!seq_no.equals("9"))))
		{

			logi.logInfo("Unapplied amount : "+unapplied_amt)
			hm_coid.put("LINE_AMOUNT",df.format((unapplied_amt.toDouble())).toString())
			hm_coid.put("SERVICE_NAME","Account Credit")
			hm_coid.put("SERVICE_IS_TAX_IND","0")
			hm_coid.put("LINE_UNITS","0")
			hm_coid.put("RATE_PER_UNITS","0")
		}
		logi.logInfo("Credit amount :: "+creditAmt)
		if((hm_coid.get("SERVICE_NO").toString() == "0") && (creditAmt!= null) && (coupon_cd.equals("NoVal")) && (!units_discount_amounts.equals("NoVal")) && ((!seq_no.equals("1")) && (!seq_no.equals("5")) && (!seq_no.equals("9"))))
		{
			logi.logInfo("Credit amount :: "+creditAmt)
			creditAmt = -(creditAmt.toInteger())
			hm_coid.put("LINE_AMOUNT",creditAmt)
			hm_coid.put("SERVICE_NAME","Account Credit")
			hm_coid.put("SERVICE_IS_TAX_IND","0")
			hm_coid.put("LINE_UNITS","0")
			hm_coid.put("RATE_PER_UNITS","0")
		}
		if((hm_coid.get("SERVICE_NO").toString() == "0") && (couponAmt!= null) && (!coupon_cd.equals("NoVal"))  && (!units_discount_amounts.equals("NoVal")) && ((!seq_no.equals("1")) && (!seq_no.equals("5")) && (!seq_no.equals("9"))))
		{
			hm_coid.put("LINE_AMOUNT",df.format(-(couponAmt.toDouble())).toString())
			hm_coid.put("SERVICE_NAME","Account Credit")
			hm_coid.put("SERVICE_IS_TAX_IND","0")
			hm_coid.put("LINE_UNITS","0")
			hm_coid.put("RATE_PER_UNITS","0")

		}
		if((hm_coid.get("SERVICE_NO").toString() == "0") && (couponAmt!= null) && (!coupon_cd.equals("NoVal")) && ((!seq_no.equals("1")) && (!seq_no.equals("5")) && (!seq_no.equals("9"))))
		{
			hm_coid.put("LINE_AMOUNT",df.format(-(couponAmt.toDouble())).toString())
			hm_coid.put("SERVICE_NAME","Account Credit")
			hm_coid.put("SERVICE_IS_TAX_IND","0")
			hm_coid.put("LINE_UNITS","0")
			hm_coid.put("RATE_PER_UNITS","0")

		}
		logi.logInfo ("expected method from DB = " +hm_coid.toString())
		return hm_coid.sort()
	}
	
	/**
	 * Verifies Create Order Invoice Charges from DB
	 * @param testCaseId
	 * @return Calculated invoice charges HashMap
	 */
	//This method is in under development, yet to add more conditions and remove log commands
	def md_verifyCreateOrderTotalChargesFromDB(String tcid)
	{
		logi.logInfo("md_verifyCreateOrderTotalChargesFromDB")
		String apiname=tcid.split("-")[2]
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		//def acct_no=Constant.mycontext.expand('${Properties#AccountNo}')
		def acct_no = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		if(acct_no =='NoVal')
		{
			   acct_no = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		def inv_no=getValueFromResponse(apiname,ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)
		String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		String units = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNITS)
		String amounts = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_AMOUNTS)
		String units_discount_amounts = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNIT_DISCOUNT_AMOUNTS)
		String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
		String coupon_cd
		if (apiname == "create_order_with_plans")
			coupon_cd = getValueFromRequest(apiname,"//*[1]/*[local-name()='coupon_code']")
		else
			coupon_cd = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_COUPON_CD)
		//Ram
		def creditNo = getValueFromResponse('apply_service_credit',ExPathRpcEnc.APPLY_SERVICE_CREDIT_CREDITID1)
		logi.logInfo("Ram Service Credit" + creditNo.toString())
		//Ram
		logi.logInfo("Coupon Code applied is " +coupon_cd)
		ConnectDB db = new ConnectDB();
		logi.logInfo("units" + units.toString())
		def order_name=getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_ORDER_NAME)
		String rate,rate1
		def seqno,charges,credits,tax_rate,tax_charges,creditAmt,coupon_amt,coupon_amt_per,coupon_dr_inline_amt,coupon_dr_offset_amt,coupon_dr_inline_amt_per,coupon_dr_offset_amt_per
		def sum=0
		def couponAmt = getCouponAmtFromCreateOrder(tcid)
		logi.logInfo("couponAmttttttt" + couponAmt)
		String rate_qry= "Select INVIP.price from ariacore.inventory_item_prices INVIP  JOIN ariacore.INVENTORY_ITEMS INVI ON invip.item_no=invi.item_no and invip.client_no=invi.client_no where INVI.client_sku='"+order_name+"' and INVI.client_no="+client_no+" and invip.currency_cd=(SELECT CURRENCY_CD FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no+"AND CLIENT_NO="+client_no+")"
		//Ram
		String coupon_ind_query="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String coupon_ind = db.executeQueryP2(coupon_ind_query)
		logi.logInfo("coupon_ind" + coupon_ind)
		String coupon_type_query="SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String coupon_type = db.executeQueryP2(coupon_type_query)
		logi.logInfo("coupon_typeeee" + coupon_type)
		
		String coupon_DB_query="SELECT amount FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Coupon_cd = '"+coupon_cd+"' AND CLIENT_NO="+client_no+")) and FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='I'"
		def coupon_DB= db.executeQueryP2(coupon_DB_query)
		
		String coupon_DB_OFF_query="SELECT amount FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Coupon_cd = '"+coupon_cd+"' AND CLIENT_NO="+client_no+")) and FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='O'"
		def coupon_DB_OFF= db.executeQueryP2(coupon_DB_OFF_query)

		String creditreduces = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = "+client_no
		String creditreducesvalue = db.executeQueryP2(creditreduces)
		logi.logInfo("Client Credit reduces = "+creditreducesvalue)

		String external_tax_client = "SELECT METHOD_CD FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO=" + client_no
		external_tax_client = db.executeQueryP2(external_tax_client)
		logi.logInfo("External Client ID = " +external_tax_client)
		HashMap seqs=new HashMap()
		HashMap row = new HashMap();




		//Returns the Credit Amount when the Apply Service Credit is used in the API
		if(creditNo =="NoVal")
		{
			creditAmt= 0
			logi.logInfo("RAM creditAmt "+creditAmt)
		}
		else
		{
			logi.logInfo("RAM SERVICE_CREDITS")
			String query1 = "select NVL(amount,0) from ariacore.all_credits where acct_no="+acct_no+" and credit_type='S' and credit_id="+creditNo
			creditAmt=db.executeQueryP2(query1)
			logi.logInfo("RAM creditAmt "+creditAmt)
			//row.put("RAM SERVICE_CREDITS",credits)
		}
		//Ram Code
		// Prints the Total Credit Amount and returns Charges when UNITES is passed and AMOUNT , UNIT DISCOUNT AMOUNT are NOT passed in create Order API
		if ((!units.equals("NoVal")) && amounts.equals("NoVal") && units_discount_amounts.equals("NoVal"))
		{
			logi.logInfo("rate_qry :"+rate_qry)
			rate = db.executeQueryP2(rate_qry)
			charges= (units.toDouble() * rate.toDouble())
			logi.logInfo("First If value  :"+charges)
			row.put("TOTAL_CREDITS","0")
			if(coupon_cd.equals("NoVal"))
			{
				String unapplied_amt="SELECT -NVL(debit,0) from ariacore.gl_detail where invoice_no="+inv_no+" and service_no NOT IN (400,401,402,403,404,405) and seq_num=2"
				unapplied_amt = db.executeQueryP2(unapplied_amt)
				logi.logInfo("unapplied_amt :"+unapplied_amt)
				if(unapplied_amt!= null)
				{
					charges = charges - unapplied_amt.toDouble()
					row.put("TOTAL_CREDITS",df.format(unapplied_amt.toDouble()).toString())
				}
				credits = 0
			}
			else
			{
				logi.logInfo("Entered into Coupon_cd method")
				//logi.logInfo("CouponAmt"+coupon_amt)
				// logi.logInfo("CouponAmtPercentage"+coupon_amt_per)
				if(couponAmt!=null)
				{
					if(coupon_type.equals("P") && coupon_ind.equals("I"))
						charges = (couponAmt.toDouble())
					else
					{
						//if(creditreducesvalue =="1")
						      charges= ((units.toInteger() * rate.toDouble()) - (couponAmt.toDouble())) 
						//else if(creditreducesvalue =="0")
						    //  charges= (units.toInteger() * rate.toDouble())
					}
						       
					if(coupon_ind.equals("I"))
						credits = 0
					else
						credits= (couponAmt.toDouble())
					row.put("TOTAL_CREDITS",df.format(credits.toDouble()).toString())
				}
				
				if(coupon_DB!=null)
				{
					charges= (((units.toInteger() * rate.toDouble()) - (coupon_DB.toDouble()))-(coupon_DB_OFF.toDouble()))
					credits= (coupon_DB.toDouble())
					row.put("TOTAL_CREDITS",df.format(credits.toDouble()).toString())
				}
				
			}
		}

		   //Prints the Total Credit Amount and returns Charges when UNITS AMD AMOUNT are passed and UNIT DISCOUNT AMOUNT is NOT passed in create Order API

		else if ((!units.equals("NoVal")) && (!amounts.equals("NoVal")) && units_discount_amounts.equals("NoVal"))
		{
			rate = amounts
			charges= (units.toDouble()) * rate.toDouble()
			row.put("TOTAL_CREDITS","0")
		}

		   // Prints the Total Credit Amount and returns Charges when UNITS , AMOUNT and UNIT DISCOUNT AMOUNT are passed in create Order API

		else if ((!units.equals("NoVal")) && (!amounts.equals("NoVal"))  && (!units_discount_amounts.equals("NoVal")))
		{
			rate = amounts
			rate1 = units_discount_amounts
			credits=(units.toInteger() * rate1.toInteger())
			logi.logInfo("Credits"+credits)
			charges= ((units.toInteger() * rate.toDouble()) - (units.toInteger() * rate1.toDouble()))
			credits= (credits + (creditAmt.toDouble()))
			logi.logInfo("Credits = "+credits)
			row.put("TOTAL_CREDITS",df.format(credits.toDouble()).toString())
		}

		//Prints the Total Credit Amount and returns Charges when UNITES and UNIT DISCOUNT AMOUNT are passed in create Order API

		else if ((!units.equals("NoVal"))  && (!units_discount_amounts.equals("NoVal")))
		{
			logi.logInfo("rate_qry :"+rate_qry)
			rate = db.executeQueryP2(rate_qry)
			rate1 = units_discount_amounts
			credits=(units.toInteger() * rate1.toInteger())
			logi.logInfo("Credits"+credits)

			if(couponAmt!=null)
			{
				charges= ((units.toInteger() * rate.toDouble()) - ((units.toInteger() * rate1.toDouble())+couponAmt.toDouble()))
				credits= (couponAmt.toDouble() +(credits + creditAmt.toDouble()))
			}
			else
			{
				charges= ((units.toInteger() * rate.toDouble()) - (units.toInteger() * rate1.toDouble()))
				credits= (credits + (creditAmt.toDouble()))
			}
			logi.logInfo("Credits = "+credits)
			row.put("TOTAL_CREDITS",df.format(credits.toDouble()).toString())
		}


		//Returns Charges based on the Credit Amount
		//Ram Code
		if(creditNo =="NoVal")
		{
			creditAmt= 0
			charges= (charges.toDouble() - creditAmt.toDouble())
			logi.logInfo("Ram Charges = "+charges)
		}
		else
		{
			if ((!units.equals("NoVal")) && amounts.equals("NoVal") && units_discount_amounts.equals("NoVal"))
			{
				logi.logInfo("Ram Charges new = "+charges)
			}
			else
			{
				charges= (charges.toDouble() - creditAmt.toDouble())
			}
			logi.logInfo("Ram Charges new 2= "+charges)

		}
		//Ram Code

		logi.logInfo("rate" + rate.toString())
		logi.logInfo("charges"+charges)

		row.put("TOTAL_CHARGES_BEFORE_TAX",df.format(charges.toDouble()).toString())


		String seq_qry= "Select seq_num from ARIACORE.gl_tax_detail where invoice_no="+inv_no+ " order by seq_num asc"
		ResultSet rs_seq_qry = db.executePlaQuery(seq_qry);
		int k=1;
		while (rs_seq_qry.next())
		{
			seqno = rs_seq_qry.getObject(1)
			seqs.put("tax"+k, seqno)
			k++
		}
		logi.logInfo("seqs array"+seqs)

		if (creditreducesvalue!="0" || creditreducesvalue==null)
		{

			logi.logInfo("External Client ID = "+ external_tax_client)
			if (external_tax_client== "10" ) // Crexendo Tax Method
			{
				String qury = "SELECT SUM(DEBIT) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (400, 401, 402, 403, 404, 405))"
				logi.logInfo("tax query : " + qury)
				String taxPercent = db.executeQueryP2(qury)
				logi.logInfo("Tax Percent is : " + taxPercent)
				tax_charges = ((taxPercent.toDouble())).toString()
				logi.logInfo("tax charges"+ tax_charges.toString())
			}
			else
			{

				//for(int i=1;i<=seqs.size();i++)
				//{
				//def s_no=seqs.get("tax"+i)
				logi.logInfo("tax_exemption_level == "+ tax_exemption_level.toString())
				String qury
				if (tax_exemption_level.toString()== "1")
					qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (400, 402, 403, 404, 405))"
				else if (tax_exemption_level.toString()== "2")
					qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (401, 402, 403, 404, 405))"
				else if (tax_exemption_level.toString()== "3")
					qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (402, 403, 404, 405))"
				else
					qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (400, 401, 402, 403, 404, 405))"
				//Ram String qury = "SELECT SUM(DEBIT) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (400, 401, 402, 403, 404, 405))"
				logi.logInfo("tax query : " + qury)
				String taxPercent = db.executeQueryP2(qury)
				logi.logInfo("Tax Percent is : " + taxPercent)
				// String tax_qry = "Select tax_rate from ARIACORE.gl_tax_detail where invoice_no="+inv_no+" and seq_num="+s_no
				//ResultSet rs_tax = db.executePlaQuery(tax_qry);
				//while(rs_tax.next())
				//{
				//tax_rate = rs_tax.getObject(1)
				//tax_charges= (charges * (tax_rate*100).toInteger())/100
				tax_charges = ((charges.toDouble())*(taxPercent.toDouble())).toString()
				logi.logInfo("tax charges"+ tax_charges.toString())
				//}
			}
			sum= sum.toDouble() + tax_charges.toDouble()
		}
		else
		{
			logi.logInfo("credit reduces = "+creditreducesvalue)
			logi.logInfo("Charges and Credit" + charges + " :: " + credits)
			def txcharges = (charges.toDouble() + credits.toDouble()).toDouble()
			logi.logInfo("calculated tax charges : " + txcharges)
			String qury
			if (tax_exemption_level.toString()== "1")
			{
				qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (400, 402, 403, 404, 405))"
				logi.logInfo("tax query : " + qury)
				String taxPercent = db.executeQueryP2(qury)
				logi.logInfo("Tax Percent is : " + taxPercent)
				tax_charges = ((charges.toDouble())*(taxPercent.toDouble())).toString()
				logi.logInfo("tax charges"+ tax_charges.toString())
				sum= sum.toDouble() + tax_charges.toDouble()
			}
			else if (tax_exemption_level.toString()== "2")
			{
				qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (401, 402, 403, 404, 405))"
				logi.logInfo("tax query : " + qury)
				String taxPercent = db.executeQueryP2(qury)
				logi.logInfo("Tax Percent is : " + taxPercent)
				tax_charges = ((charges.toDouble())*(taxPercent.toDouble())).toString()
				logi.logInfo("tax charges"+ tax_charges.toString())
				sum= sum.toDouble() + tax_charges.toDouble()
			}
			else if (tax_exemption_level.toString()== "3")
			{
				qury = "SELECT NVL(SUM(distinct(tax_rate)),0) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO=" + inv_no + " and seq_num in (SELECT seq_num FROM ARIACORE.GL_DETAIL WHERE invoice_no = " + inv_no + " AND service_no in (402, 403, 404, 405))"
				logi.logInfo("tax query : " + qury)
				String taxPercent = db.executeQueryP2(qury)
				logi.logInfo("Tax Percent is : " + taxPercent)
				tax_charges = ((charges.toDouble())*(taxPercent.toDouble())).toString()
				logi.logInfo("tax charges"+ tax_charges.toString())
				sum= sum.toDouble() + tax_charges.toDouble()
			}
			else
			{
				for(int i=1;i<=seqs.size();i++)
				{
					def s_no=seqs.get("tax"+i)
					String tax_qry = "Select tax_rate from ARIACORE.gl_tax_detail where invoice_no="+inv_no+" and seq_num="+s_no
					ResultSet rs_tax = db.executePlaQuery(tax_qry);
					while(rs_tax.next())
					{
						tax_rate = rs_tax.getObject(1)
						def tempcharges = txcharges.toDouble()
						logi.logInfo("charges = "+ tempcharges)
						tax_charges= (tempcharges * (tax_rate*100).toInteger())/100
						logi.logInfo("tax charges"+ tax_charges)
					}
					sum= sum + tax_charges
				}
			}
		}
		logi.logInfo("total tax"+ sum)
		row.put("TOTAL_TAX_CHARGES",df.format(sum.toDouble()).toString())
		def total = charges + sum
		logi.logInfo("all total "+ total)
		row.put("TOTAL_CHARGES_AFTER_TAX",df.format(total.toDouble()).toString())
		logi.logInfo("md_verifyCreateOrderTotalChargesFromDB_hashresult=> "+row)
		return row.sort();
	}
	
	/**
	 * Calculates Invoice amount by reducing applied coupon in create Order API
	 * @param testCaseId
	 * @return Invoice amount with coupon amount reduced
	 */
	def md_verifyCouponAppliedInInvoice(String testCaseId)
	{
		def expPlansInv = md_serviceInvoiceAmount(testCaseId)
		def couponAmt= getCouponAmtFromCreateOrder(testCaseId)
		def finalTotal =expPlansInv.toDouble()-couponAmt.toDouble()
		return df.format(finalTotal.toDouble()).toString()
	}

	/**
	 * Extracts the coupon amount applied in the create order API
	 * @param testCaseId
	 * @return coupon discount amount
	 */
	def getCouponAmtFromCreateOrder(String testCaseId)
	{
		logi.logInfo("Calling getCouponAmtFromCreateOrder")
		String apiname
		if(testCaseId.contains("VTV"))
		{
			apiname = testCaseId.split("\\^")[1]
		}
		else
		{
			apiname = testCaseId.split("-")[2]
		}
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def accountNo = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		double after_disc
		String units = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNITS)
		String amounts = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_AMOUNTS)
		String units_discount_amounts = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_UNIT_DISCOUNT_AMOUNTS)
		String invoiceNo =   getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
		def orderNo = getValueFromResponse(apiname,"//*[1]/*[local-name()='order_no']")
		String coupon_cd
			if (apiname == "create_order_with_plans")
			{
				coupon_cd = getValueFromRequest(apiname,"//*[1]/*[local-name()='coupon_code']")
				if(coupon_cd=="NoVal")
				   coupon_cd = getValueFromRequest(apiname,"//*[1]/*[local-name()='coupon_codes']")
			}
			else
				coupon_cd = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_COUPON_CD)
		ConnectDB database = new ConnectDB();
		def couponCTFlat_res,couponCTPercentage_res,couponDRFlatOffset_res,couponDRFlatInline_res,couponDRPerOffset_res,couponDRPerInline_res,findOverlap_res
		def couponDisBunFla_res,couponDisBunPer_res,couponDisBunFlaNo_res,couponDisBunPerNo_res,couponDisBunFla_res_No,couponDisBunPer_res_No
		def couponCTPercentage_fin_res,couponDRPercentage_off,couponDRPercentage_inline
		String couponCTFlat= "select flat_amount from ariacore.RECURRING_CREDIT_TEMPLATES where percent_amount is null and RECURRING_CREDIT_TEMPLATE_NO = (select RECURRING_CREDIT_TEMPLATE_NO from ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP where Client_no = "+client_no+" and Coupon_cd ='"+coupon_cd+"') and Client_no = "+client_no
		String couponCTPercentage = "select percent_amount from ariacore.RECURRING_CREDIT_TEMPLATES where flat_amount is null and RECURRING_CREDIT_TEMPLATE_NO = (select RECURRING_CREDIT_TEMPLATE_NO from ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP where Client_no = "+client_no+" and Coupon_cd ='"+coupon_cd+"') and Client_no = "+client_no
		String couponDRFlatOffset= "SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='O' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String couponDRFlatInline="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='I' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String couponDRPerOffset= "SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' and INLINE_OFFSET_IND='O' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String couponDRPerInline="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' and INLINE_OFFSET_IND='I' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		
		String coupon_DisBun_Flat="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select RULE_NO FROM ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+client_no+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='F'"
		String coupon_DisBun_Per="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+client_no+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='P'"		
		
		String coupon_DisBun_Flat_No="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select RULE_NO FROM ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+client_no+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='F'"
		String coupon_DisBun_Per_No="SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO in (Select rule_no from ARIACORE.client_dr_bundle_members where BUNDLE_NO = (Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO="+client_no+" and coupon_cd = '"+coupon_cd+"')) and INLINE_OFFSET_IND='O' and FLAT_PERCENT_IND='P'"
		String findOverlap="Select allow_overlap_ind from ariacore.client_discount_rule_bundles where bundle_no=(Select BUNDLE_NO from ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		couponCTFlat_res=database.executeQueryP2(couponCTFlat)
		couponCTPercentage_res=database.executeQueryP2(couponCTPercentage)
		couponDRFlatOffset_res=database.executeQueryP2(couponDRFlatOffset)
		couponDRFlatInline_res=database.executeQueryP2(couponDRFlatInline)
		couponDRPerOffset_res=database.executeQueryP2(couponDRPerOffset)
		couponDRPerInline_res=database.executeQueryP2(couponDRPerInline)
		couponDisBunFla_res=database.executeQueryP2(coupon_DisBun_Flat)
		couponDisBunPer_res=database.executeQueryP2(coupon_DisBun_Per)
		couponDisBunFla_res_No=database.executeQueryP2(coupon_DisBun_Flat_No)
		couponDisBunPer_res_No=database.executeQueryP2(coupon_DisBun_Per_No)
		findOverlap_res=database.executeQueryP2(findOverlap)
		
		if(couponCTFlat_res!=null)
			return couponCTFlat_res
		else if(couponCTPercentage_res!=null)
		{
			logi.logInfo("Entering percentage :")
			//return couponCTPercentage_res
			String query1= "select percent_eval_plan_no as plan_no,percent_eval_service_no as service_no from ariacore.RECURRING_CREDIT_TEMPLATES where flat_amount is null and RECURRING_CREDIT_TEMPLATE_NO = (select RECURRING_CREDIT_TEMPLATE_NO from ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP where Client_no = "+client_no+" and Coupon_cd ='"+coupon_cd+"')"
			logi.logInfo("percent_eval_plan_no and percent_eval_service_no :"+query1)
			ResultSet resultSet = database.executePlaQuery(query1);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();
			HashMap coupon = new HashMap();
			while (resultSet.next()){
				for(int i=1; i<=columns; i++){
					coupon.put(md.getColumnName(i),resultSet.getObject(i));
				}
			}
			logi.logInfo("coupon plan,service hash :"+coupon)
			//String query2="select DEBIT from ariacore.gl_detail where invoice_no="+invoiceNo+" and service_no="+coupon.get("SERVICE_NO")+" and plan_no="+coupon.get("PLAN_NO")
			String master_plan_units = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_MASTER_PLAN_UNITS)
			logi.logInfo("Master plan Units :"+master_plan_units)
			String query2 = "SELECT RATE_PER_UNIT * "+master_plan_units+" FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SERVICE_NO="+coupon.get("SERVICE_NO")+" AND SCHEDULE_NO = (SELECT DISTINCT(RATE_SCHEDULE_NO) FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO ="+accountNo+" AND PLAN_NO ="+coupon.get("PLAN_NO")+" AND SERVICE_NO ="+coupon.get("SERVICE_NO")+" AND RATE_SEQ_NO=1 AND CLIENT_NO ="+client_no+") AND RATE_SEQ_NO = 1 and client_no =  "+client_no
			logi.logInfo("couponActServiceAmt query :"+query2)
			couponCTPercentage_fin_res=database.executeQueryP2(query2)
			logi.logInfo("couponActServiceAmt:"+couponCTPercentage_fin_res)
			couponCTPercentage_fin_res=(couponCTPercentage_fin_res.toDouble()*(couponCTPercentage_res.toDouble()/100))
			logi.logInfo("couponActPercentageAmt:"+couponCTPercentage_fin_res)
			return df.format(couponCTPercentage_fin_res.toDouble()).toString();
			logi.logInfo("End of percentage")
		}
		else if(couponDRFlatOffset_res!=null)
			return couponDRFlatOffset_res
		else if(couponDRFlatInline_res!=null)
			return couponDRFlatInline_res
		else if(couponDRPerOffset_res!=null)
		{
			String query1= "select debit from ariacore.gl_detail where invoice_no=(select invoice_no from ariacore.gl_detail where order_no= "+orderNo+" and client_no= "+client_no+" and rownum = 1) and orig_coupon_cd is null and service_no not in('400','401')"
			couponDRPercentage_off=database.executeQueryP2(query1)
			couponDRPercentage_off=(couponDRPercentage_off.toDouble()*(couponDRPerOffset_res.toDouble()/100))
			return df.format(couponDRPercentage_off.toDouble()).toString();
		}
		else if(couponDRPerInline_res!=null)
		{
			String query1= "select debit from ariacore.gl_detail where invoice_no=(select invoice_no from ariacore.gl_detail where order_no= "+orderNo+" and client_no= "+client_no+" and rownum = 1) and orig_coupon_cd is null and service_no not in('400','401')"
			couponDRPercentage_inline=database.executeQueryP2(query1)
			//couponDRPercentage_inline=(couponDRPercentage_inline.toDouble()*(couponDRPerInline_res.toDouble()/100))
			logi.logInfo("couponDRPercentage_inline:"+couponDRPercentage_inline)
			return df.format(couponDRPercentage_inline.toDouble()).toString();
		}
		
		if ((findOverlap_res == 'Y') && (units!="NoVal") && (amounts!="NoVal") && (units_discount_amounts!="NoVal"))
		{
			logi.logInfo("Entering Yes in loop")			
			double full_amt = units.toDouble() * amounts.toDouble()			
			if(couponDisBunPer_res !=null)
			{				
				after_disc = ((full_amt.toDouble() * couponDisBunPer_res.toDouble())/100)			
				after_disc = after_disc.toDouble() + couponDisBunFla_res.toDouble()				
			}			
			def ret_amt = after_disc.toDouble()
			return df.format(ret_amt.toDouble()).toString()						
		}
		
		if ((findOverlap_res == 'N') && (units!="NoVal") && (amounts!="NoVal") && (units_discount_amounts!="NoVal"))
		{
			logi.logInfo("Entering No in loop")			
			double full_amt = units.toDouble() * amounts.toDouble()
			if(couponDisBunPer_res !=null)
			{
				after_disc = ((full_amt.toDouble() * couponDisBunPer_res.toDouble())/100)				
			}
			def ret_amt = after_disc.toDouble()
			return df.format(ret_amt.toDouble()).toString()
		}
		
	}


	HashMap md_verifyCreateOrderFederalStateChargesFromDB2F(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"2","F")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB2S(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"2","S")
	}
	HashMap md_verifyCreateOrderFederalStateChargesFromDB3S(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"3","S")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB3F(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"3","F")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB4S(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"4","S")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB4F(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"4","F")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB5S(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"5","S")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB5F(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"5","F")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB6F(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"6","F")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB7F(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"7","F")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB8F(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"8","F")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB9F(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"9","F")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB10F(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"10","F")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB11F(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"11","F")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB12F(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"12","F")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB13F(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"13","F")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB14F(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"14","F")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB6S(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"6","S")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB7S(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"7","S")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB8S(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"8","S")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB9S(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"9","S")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB10S(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"10","S")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB11S(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"11","S")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB12S(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"12","S")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB13S(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"13","S")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB14S(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"14","S")
	}


	HashMap md_verifyCreateOrderFederalStateChargesFromDB3C(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"3","C")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB7C(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"7","C")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB8C(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"8","C")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB4D(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"4","D")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB8D(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"8","D")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB9D(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"9","D")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB2CT(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"2","CT")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB3CT(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"3","CT")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB6CT(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"6","CT")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB7CT(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"7","CT")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB8CT(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"8","CT")
	}

	HashMap md_verifyCreateOrderFederalStateChargesFromDB9CT(String tcid)
	{
		return  md_verifyCreateOrderFederalStateChargesFromDB(tcid,"9","CT")
	}

	HashMap md_verifyCreateOrderInvoiceTaxDetailsFromAPI2(String tcid)
	{
		return  md_verifyCreateOrderInvoiceTaxDetailsFromAPI(2)
	}

	HashMap md_verifyCreateOrderInvoiceTaxDetailsFromAPI3(String tcid)
	{
		return  md_verifyCreateOrderInvoiceTaxDetailsFromAPI(3)
	}

	HashMap md_verifyCreateOrderInvoiceTaxDetailsFromAPI4(String tcid)
	{
		return  md_verifyCreateOrderInvoiceTaxDetailsFromAPI(4)
	}

	HashMap md_verifyCreateOrderInvoiceTaxDetailsFromAPI5(String tcid)
	{
		return  md_verifyCreateOrderInvoiceTaxDetailsFromAPI(5)
	}

	HashMap md_verifyCreateOrderInvoiceTaxDetailsFromAPI6(String tcid)
	{
		return  md_verifyCreateOrderInvoiceTaxDetailsFromAPI(6)
	}

	HashMap md_verifyCreateOrderInvoiceTaxDetailsFromAPI7(String tcid)
	{
		return  md_verifyCreateOrderInvoiceTaxDetailsFromAPI(7)
	}

	HashMap md_verifyCreateOrderInvoiceTaxDetailsFromAPI8(String tcid)
	{
		return  md_verifyCreateOrderInvoiceTaxDetailsFromAPI(8)
	}

	HashMap md_verifyCreateOrderInvoiceTaxDetailsFromAPI9(String tcid)
	{
		return  md_verifyCreateOrderInvoiceTaxDetailsFromAPI(9)
	}

	HashMap md_verifyCreateOrderInvoiceTaxDetailsFromAPI10(String tcid)
	{
		return  md_verifyCreateOrderInvoiceTaxDetailsFromAPI(10)
	}

	HashMap md_verifyCreateOrderInvoiceTaxDetailsFromAPI11(String tcid)
	{
		return  md_verifyCreateOrderInvoiceTaxDetailsFromAPI(11)
	}

	HashMap md_verifyCreateOrderInvoiceTaxDetailsFromAPI12(String tcid)
	{
		return  md_verifyCreateOrderInvoiceTaxDetailsFromAPI(12)
	}

	HashMap md_verifyCreateOrderInvoiceTaxDetailsFromAPI13(String tcid)
	{
		return  md_verifyCreateOrderInvoiceTaxDetailsFromAPI(13)
	}

	HashMap md_verifyCreateOrderInvoiceTaxDetailsFromAPI14(String tcid)
	{
		return  md_verifyCreateOrderInvoiceTaxDetailsFromAPI(14)
	}

	/**
	 * Verifies Create Order Invoice Tax Details from API
	 * @param line item number from API
	 * @return Calculated invoice charges HashMap
	 */
	def md_verifyCreateOrderInvoiceTaxDetailsFromAPI(int line_item_no)
	{
		logi.logInfo("md_verifyCreateOrderInvoiceTaxDetailsFromAPI")
		List testCaseCmb = Constant.lhmAllOutputResponseAsXML_Global.keySet().asList()
		List xmlValues = Constant.lhmAllOutputResponseAsXML_Global.values().asList()
		HashMap hm_invoice=new HashMap()
		com.eviware.soapui.support.XmlHolder outputholder
		for(int j=0; j<testCaseCmb.size(); j++)
		{
			if(testCaseCmb[j].toString().contains('create_order') && testCaseCmb[j].toString().contains(Constant.TESTCASEID) && testCaseCmb[j].toString().split("-")[1] == Constant.TESTCASEID)
			{
				outputholder = xmlValues[j]
				hm_invoice.put("LINE_NO", outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='line_no']"))
				hm_invoice.put("SERVICE_NO",outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='service_no']"))
				hm_invoice.put("SERVICE_NAME",outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='service_name']"))
				hm_invoice.put("SERVICE_IS_TAX_IND",outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='service_is_tax_ind']"))
				hm_invoice.put("LINE_AMOUNT",outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='line_amount']"))
				hm_invoice.put("DESCRIPTION",outputholder.getNodeValue("//*["+line_item_no+"]/*[local-name()='description']"))
			}
		}
		return hm_invoice.sort()
	}

	/**
	 * Calculates Create Order Federal and State taxes from DB
	 * @param testCaseId, Invoice sequence number, tax type 
	 * (F-Federal, S-State,C-Country,D-District,CT-City)
	 * @return HashMap of invoice details for the given invoice sequence number
	 */
	def md_verifyCreateOrderFederalStateChargesFromDB(String tcid, String seq_no, String taxtype)
	{
		logi.logInfo("md_verifyCreateOrderFederalStateChargesFromDB")
		String apiname=tcid.split("-")[2]
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def acct_no=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def inv_nos=getValueFromResponse(apiname,ExPathRpcEnc.CRETAE_ORDER_INVOICE_NO)
		HashMap hm_coid= new HashMap();
		// String creditreduces = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = "+client_no
		String sequenceNum
		String serviceNo
		String description
		String total_Invoice_Tax_Amt, total_Tax_Amount_Qry, tax_Sequence_Query, tax_ServiceNo_Query, taxDetail_Qry
		ConnectDB db = new ConnectDB();
		// creditreduces=db.executeQueryP2(creditreduces)
		//logi.logInfo("Client Credit reduces = "+creditreduces)
		//String external_tax_client = db.executeQueryP2("SELECT * FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO=" + client_no)
		// if(creditreduces == "1" || creditreduces == "0" || (external_tax_client != null))
		// {
		if (taxtype == "F")
		{
			logi.logInfo("entered f = ")
			total_Tax_Amount_Qry = "SELECT debit FROM ariacore.gl_detail WHERE client_no = "+client_no+" AND invoice_no  = "+inv_nos+" AND SERVICE_NO = 400"
			tax_Sequence_Query = "select seq_num from ariacore.gl_detail where invoice_no = "+inv_nos+" and SERVICE_NO = 400"
			tax_ServiceNo_Query = "select service_no from ariacore.gl_detail where invoice_no = "+inv_nos+" and SERVICE_NO = 400"
			taxDetail_Qry = "Select comments from ariacore.gl_detail where client_no="+client_no+" and invoice_no="+inv_nos+" and seq_num="+seq_no

		}

		if (taxtype == "S")
		{

			logi.logInfo("entered S = ")
			total_Tax_Amount_Qry = "SELECT debit FROM ariacore.gl_detail WHERE client_no = "+client_no+" AND invoice_no  = "+inv_nos+" AND SERVICE_NO = 401"
			tax_Sequence_Query = "select seq_num from ariacore.gl_detail where invoice_no = "+inv_nos+" and SERVICE_NO = 401"
			tax_ServiceNo_Query = "select service_no from ariacore.gl_detail where invoice_no = "+inv_nos+" and SERVICE_NO = 401"
			taxDetail_Qry = "Select comments from ariacore.gl_detail where client_no="+client_no+" and invoice_no="+inv_nos+" and seq_num="+seq_no

		}
		if (taxtype == "C")
		{
			logi.logInfo("entered C = ")
			total_Tax_Amount_Qry = "SELECT debit FROM ariacore.gl_detail WHERE client_no = "+client_no+" AND invoice_no = "+inv_nos+" AND SERVICE_NO = 402"
			tax_Sequence_Query = "select seq_num from ariacore.gl_detail where invoice_no = "+inv_nos+" and SERVICE_NO = 402"
			tax_ServiceNo_Query = "select service_no from ariacore.gl_detail where invoice_no = "+inv_nos+" and SERVICE_NO = 402"
			taxDetail_Qry = "Select comments from ariacore.gl_detail where client_no="+client_no+" and invoice_no="+inv_nos+" and seq_num="+seq_no
		}
		if (taxtype == "D")
		{
			logi.logInfo("entered D = ")
			total_Tax_Amount_Qry = "SELECT debit FROM ariacore.gl_detail WHERE client_no = "+client_no+" AND invoice_no = "+inv_nos+" AND SERVICE_NO = 405"
			tax_Sequence_Query = "select seq_num from ariacore.gl_detail where invoice_no = "+inv_nos+" and SERVICE_NO = 405"
			tax_ServiceNo_Query = "select service_no from ariacore.gl_detail where invoice_no = "+inv_nos+" and SERVICE_NO = 405"
			taxDetail_Qry = "Select comments from ariacore.gl_detail where client_no="+client_no+" and invoice_no="+inv_nos+" and seq_num="+seq_no
		}
		if (taxtype == "CT")
		{
			logi.logInfo("entered CT = ")
			total_Tax_Amount_Qry = "SELECT debit FROM ariacore.gl_detail WHERE client_no = "+client_no+" AND invoice_no  = "+inv_nos+" AND SERVICE_NO = 403"
			tax_Sequence_Query = "select seq_num from ariacore.gl_detail where invoice_no = "+inv_nos+" and SERVICE_NO = 403"
			tax_ServiceNo_Query = "select service_no from ariacore.gl_detail where invoice_no = "+inv_nos+" and SERVICE_NO = 403"
			taxDetail_Qry = "Select comments from ariacore.gl_detail where client_no="+client_no+" and invoice_no="+inv_nos+" and seq_num="+seq_no
		}

		logi.logInfo("Came Out ")
		sequenceNum = db.executeQueryP2(tax_Sequence_Query)
		logi.logInfo("Tax invoice SequenceNum : "+sequenceNum)
		serviceNo = db.executeQueryP2(tax_ServiceNo_Query)
		logi.logInfo("Tax invoice serviceNo : "+serviceNo)
		description = db.executeQueryP2(taxDetail_Qry)
		logi.logInfo("Tax description : "+description)
		total_Invoice_Tax_Amt = db.executeQueryP2(total_Tax_Amount_Qry)
		total_Invoice_Tax_Amt = df.format(total_Invoice_Tax_Amt.toDouble()).toString();
		logi.logInfo(" Tax total_Invoice_Tax_Amt : "+total_Invoice_Tax_Amt)


		hm_coid.put("LINE_NO",sequenceNum)
		hm_coid.put("SERVICE_NO",serviceNo)
		hm_coid.put("SERVICE_NAME",description)
		hm_coid.put("SERVICE_IS_TAX_IND","1")
		hm_coid.put("LINE_AMOUNT",total_Invoice_Tax_Amt)
		hm_coid.put("DESCRIPTION",description)
		//	}

		logi.logInfo ("expected method from DB = " +hm_coid.toString())
		return hm_coid.sort()

	}

	/**
	 * Calculates federal and state tax with credit reduces as Yes(CR-1)
	 * @param testCaseId, tax service number(400,401,402,403,405), invoice amount calculated
	 * @return Calculated tax for the given invoice amount
	 */
	String md_FEDERAL_STATE_TAX_CREDIT_REDUCES_1_P2(String testCaseId, String taxserno, def chargeamt) throws SQLException, ParseException
	{
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB();
		def taxRate = db.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no = "+ taxserno +"and client_no =  " + clientNo).toString()
		def taxamt = (chargeamt.toDouble() * (taxRate.toDouble()*100))/100
		logi.logInfo("tax charges"+ taxamt.toDouble())
		return df.format(taxamt.toDouble()).toString();
	}

	/**
	 * Calculates Expected Invoice amount with credit reduces as Yes(CR-1)
	 * @param testCaseId
	 * @return Calculated invoice amount with credit reduced
	 */
	def md_ExpInvoiceWithServiceCredit(String testCaseId)
	{
		logi.logInfo("calling md_ExpInvoiceWithServiceCredit")
		String apiname=testCaseId.split("-")[2]
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		def accountNo = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def orderNo = getValueFromResponse('create_order',ExPathRpcEnc.CREATE_ORDER_ORDER_NO)
		def creditNo = getValueFromResponse('apply_service_credit',ExPathRpcEnc.APPLY_SERVICE_CREDIT_CREDITID1)
		logi.logInfo("creditNo "+creditNo)
		def orderAmt, creditAmt ,expPlansInv, expInvoiceAmt
		ConnectDB database = new ConnectDB();
		expPlansInv = md_serviceInvoiceAmount(testCaseId)
		logi.logInfo("expInvoice "+expPlansInv)
		String query = "select NVL(amount,0) from ariacore.orders where acct_no="+accountNo+" and order_no="+orderNo+" and client_no="+clientNo
		orderAmt=database.executeQueryP2(query)
		logi.logInfo("orderAmt "+orderAmt)
		if(creditNo =="NoVal")
			creditAmt= 0
		else
		{
			logi.logInfo("%**************")
			String query1 = "select NVL(amount,0) from ariacore.all_credits where acct_no="+accountNo+" and credit_type='S' and credit_id="+creditNo
			creditAmt=database.executeQueryP2(query1)
			logi.logInfo("creditAmt "+creditAmt)
		}
		expInvoiceAmt = ((expPlansInv.toDouble()+orderAmt.toDouble())-creditAmt.toDouble()).toString()
		return df.format(expInvoiceAmt.toDouble()).toString()
	}

	/**
	 * Calculates Expected Invoice amount with cash credit reduced
	 * @param testCaseId
	 * @return invoice amount
	 */
	def md_ExpInvoiceWithCashCredit(String testCaseId)
	{
		logi.logInfo("calling md_ExpInvoiceWithCashCredit")
		String apiname=testCaseId.split("-")[2]
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		def accountNo = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def orderNo = getValueFromResponse('create_order',ExPathRpcEnc.CREATE_ORDER_ORDER_NO)
		def transactionNo = getValueFromResponse('apply_cash_credit',ExPathRpcEnc.APPLY_CASH_CREDIT_TRANSACTIONID1)
		def orderAmt, cashCreditAmt ,expPlansInv, expInvoiceAmt
		ConnectDB database = new ConnectDB();
		expPlansInv =md_serviceInvoiceAmount(testCaseId)
		logi.logInfo("expInvoice "+expPlansInv)
		String query = "select NVL(amount,0) from ariacore.orders where acct_no="+accountNo+" and order_no="+orderNo+" and client_no="+clientNo
		orderAmt=database.executeQueryP2(query)
		logi.logInfo("orderAmt "+orderAmt)
		String query1 = "select NVL(applied_amount,0) as credit_amount FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+accountNo+" AND TRANSACTION_TYPE=10 and client_no="+clientNo+" and event_no="+transactionNo
		cashCreditAmt=database.executeQueryP2(query1)
		logi.logInfo("creditAmt "+cashCreditAmt)
		expInvoiceAmt = ((expPlansInv.toDouble()-cashCreditAmt.toDouble())+orderAmt.toDouble()).toString()
		logi.logInfo("expInvoiceAmt "+expInvoiceAmt)
		return df.format(expInvoiceAmt.toDouble()).toString()
	}

	/**
	 * Calculates Expected Invoice amount with Usage for CR-0
	 * @param testCaseId
	 * @return invoice amount with usage charges
	 */
	public String md_ExpectedInvoiceWithUsage_CR0(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceWithUsage_CR0"
		// Calculating service, federal and state tax amount
		double serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId).toDouble()
		double federalTax = md_FEDERAL_TAX_CREDIT_REDUCES_0_P2(testCaseId).toDouble()
		double stateTax =  md_STATE_TAX_CREDIT_REDUCES_0_P2(testCaseId).toDouble()
		// Calculating deduction amount
		double creditAmount = md_CREDIT_AMT_BY_CREDITID(testCaseId).toDouble()
		// Calculating usage amount
		double usageAmount = md_ExpUsage_Amt_With_Auto_Rate(testCaseId).toDouble()
		// Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax+" Usage Amount : "+ usageAmount + " other Credit Amount : "+ creditAmount
		double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax  + usageAmount - creditAmount.toDouble()
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice).toString();
	}

	/**
	 * Calculates Expected Invoice amount with Usage for CR-1
	 * @param testCaseId
	 * @return invoice amount with usage charges
	 */
	public String md_ExpectedInvoiceWithUsage_CR1(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceWithUsage_CR1"
		// Calculating service, federal and state tax amount
		double serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId).toDouble()
		double federalTax = md_FEDERAL_TAX_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		double stateTax =  md_STATE_TAX_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		// Calculating deduction amount
		double creditAmount = md_CREDIT_AMT_BY_CREDITID(testCaseId).toDouble()
		// Calculating usage amount
		double usageAmount = md_ExpUsage_Amt_With_Auto_Rate(testCaseId).toDouble()
		// Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax+" Usage Amount : "+ usageAmount + " other Credit Amount : "+ creditAmount
		double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax  + usageAmount - creditAmount.toDouble()
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice).toString();
	}

	/**
	 * Calculates Expected Invoice amount with Usage and usage based services for CR-1
	 * @param testCaseId
	 * @return invoice amount with usage charges
	 */
	public String md_ExpectedInvoiceWithUsageAndUsageBasedServiceCredit_CR1(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceWithUsageBasedServiceCredit_CR1"
		// Calculating service and taxes(federal and state tax based on usage service credit used) amount
		double serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId).toDouble()
		double federalTax = md_FEDERAL_TAX_WITH_USAGE_SERVICE_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		double stateTax =  md_STATE_TAX_WITH_USAGE_SERVICE_CREDIT_REDUCES_1_P2(testCaseId).toDouble()
		// Calculating deduction amount
		double creditAmount = md_CREDIT_AMT_BY_CREDITID(testCaseId).toDouble()
		// Calculating usage amount
		double usageAmount = md_ExpUsage_Amt_With_Auto_Rate(testCaseId).toDouble()
		// Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federalTax + " CurrentStateTax : "+ stateTax+" Usage Amount : "+ usageAmount + " other Credit Amount : "+ creditAmount
		double totalInvoice = serviceInvoiceAmount.toDouble() + federalTax + stateTax  + usageAmount - creditAmount.toDouble()
		logi.logInfo 'Total Invoice : ' + totalInvoice
		return df.format(totalInvoice).toString();
	}

	/**
	 * Calculates create order with plans coupon amount
	 * @param testCaseId
	 * @return coupon amount aplied in create order with plans API
	 */
	//This method is in under development, yet to add more conditions and remove log commands
	def md_createOrderwithPlansCouponAmt(String testCaseId)
	{
		logi.logInfo("Calling md_createOrderwithPlansCouponAmt")

		String apiname
		if(testCaseId.contains("VTV"))
		{
			apiname = testCaseId.split("\\^")[1]
		}
		else
		{
			apiname = testCaseId.split("-")[2]
		}

		//String apiname=testCaseId.split("-")[2]
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		def orderNo = getValueFromResponse(apiname,"//*[1]/*[local-name()='order_no']")
		ConnectDB db = new ConnectDB();
		String coupon_cd
		if (apiname == "create_order_with_plans")
			coupon_cd = getValueFromRequest(apiname,"//*[1]/*[local-name()='coupon_code']")
		else
			coupon_cd = getValueFromRequest(apiname,ExPathRpcEnc.IR_CREATE_ORDER_COUPON_CD)
		logi.logInfo("coupon code = " + coupon_cd)

		String ct_flat_query= "select ((select amount from ariacore.orders where order_no="+orderNo+" and client_no="+client_no+")+ (total)-(2 * (select flat_amount from ariacore.RECURRING_CREDIT_TEMPLATES where Client_no = "+client_no+" and percent_amount is null and RECURRING_CREDIT_TEMPLATE_NO = (select RECURRING_CREDIT_TEMPLATE_NO from ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP where Client_no = "+client_no+" and Coupon_cd ='"+coupon_cd+"')))) as final_val from (SELECT sum(exp_debit) as total from (SELECT case when 0 >= a.act_debit THEN 0 else a.act_debit end as exp_debit from (select ((usage_units * usage_rate)) as act_debit from ariacore.gl_detail where invoice_no=(select invoice_no from ariacore.gl_detail where order_no="+orderNo+" and client_no="+client_no+" and orig_coupon_cd is null) and plan_no is not null and orig_coupon_cd is null and orig_client_sku is null)a))"
		String ct_flat = db.executeQueryP2(ct_flat_query)
		def ct_percentage = getCouponAmtFromCreateOrder(testCaseId)
		String ct_percentage_query="select ((select amount from ariacore.orders where order_no="+orderNo+" and client_no="+client_no+")+ (total)-(2 * ("+ct_percentage+"))) as final_val from (SELECT sum(exp_debit) as total from (SELECT case when 0 >= a.act_debit THEN 0 else a.act_debit end as exp_debit from (select ((usage_units * usage_rate)) as act_debit from ariacore.gl_detail where invoice_no=(select invoice_no from ariacore.gl_detail where order_no="+orderNo+" and client_no="+client_no+" and orig_coupon_cd is null) and plan_no is not null and orig_coupon_cd is null and orig_client_sku is null)a))"
		String ct_percentage_amt = db.executeQueryP2(ct_percentage_query)
		String coupon_ind_query="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String coupon_ind = db.executeQueryP2(coupon_ind_query)
		logi.logInfo("coupon_ind" + coupon_ind)
		String coupon_type_query="SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and Coupon_cd = '"+coupon_cd+"')"
		String coupon_type = db.executeQueryP2(coupon_type_query)
		logi.logInfo("coupon_typeeee" + coupon_type)
		String creditreduces = "SELECT CREDIT_REDUCES FROM ARIACORE.TAX_NAMES where client_no = "+client_no
		def final_amount,final_coupon_amt

		if (ct_flat != null)
			final_amount= ct_flat
		else if (ct_percentage_amt != null)
			final_amount= ct_percentage_amt
		logi.logInfo("BEFORE coupon_DR_per_amount")
		if (coupon_type == "F")
		{
			logi.logInfo("AFTER coupon_DR_per_amount")
			String coupon_DR_flat_query="select sum(exp_debit) from (SELECT case when 0 >= a.act_debit THEN 0 else a.act_debit end as exp_debit from (select ((usage_units * usage_rate)-(SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='"+coupon_ind+"' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and coupon_cd ='"+coupon_cd+"'))) as act_debit from ariacore.gl_detail where invoice_no=(select invoice_no from ariacore.gl_detail where order_no="+orderNo+" and client_no="+client_no+" and orig_coupon_cd is null) and plan_no is not null and orig_coupon_cd is null)a)"
			String coupon_DR_flat_amount = db.executeQueryP2(coupon_DR_flat_query)
			logi.logInfo("coupon_DR_per_amount" + coupon_DR_flat_amount)
			String coupon_DR_order_query="select ((amount)-(SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' and INLINE_OFFSET_IND='"+coupon_ind+"' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and coupon_cd = '"+coupon_cd+"'))) as act_debit from ariacore.orders where order_no="+orderNo+" and client_no="+client_no
			String coupon_DR_order_amount = db.executeQueryP2(coupon_DR_order_query)
			logi.logInfo("coupon_Order_amount" + coupon_DR_order_amount)
			final_amount = (coupon_DR_flat_amount.toDouble() + coupon_DR_order_amount.toDouble())
			final_amount = df.format(final_amount.toDouble()).toString()
			logi.logInfo("final_amount = "+final_amount)
		}
		else if (coupon_type == "P")
		{
			logi.logInfo("PERCENT coupon_DR_per_amount")
			if(coupon_ind == "I")
			{
				String coupon_DR_perin_query="select sum(exp_debit) from (SELECT case when 0 >= a.act_debit THEN 0 else a.act_debit end as exp_debit from (select ((usage_units * (NVL(pre_discount_unit_rate,0)))-(SELECT ((usage_units * (NVL(pre_discount_unit_rate,0))) * (AMOUNT/100)) FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' and INLINE_OFFSET_IND='I' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and coupon_cd = '"+coupon_cd+"'))) as act_debit from ariacore.gl_detail where invoice_no=(select invoice_no from ariacore.gl_detail where order_no="+orderNo+" and client_no="+client_no+" and orig_coupon_cd is null) and plan_no is not null and orig_coupon_cd is null)a)"
				String coupon_DR_perin_amount = db.executeQueryP2(coupon_DR_perin_query)
				logi.logInfo("coupon_DR_perin_amount" + coupon_DR_perin_amount)
				String coupon_DR_perin_order_query="select ((amount)-(SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' and INLINE_OFFSET_IND='I' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and coupon_cd = '"+coupon_cd+"'))) as act_debit from ariacore.orders where order_no="+orderNo+" and client_no="+client_no
				String coupon_DR_perin_order_amount = db.executeQueryP2(coupon_DR_perin_order_query)
				logi.logInfo("coupon_DR_perin_order_amount" + coupon_DR_perin_order_amount)
				final_amount = (coupon_DR_perin_amount.toDouble() + coupon_DR_perin_order_amount.toDouble())
				final_amount = df.format(final_amount.toDouble()).toString()
				logi.logInfo("final_amount = "+final_amount)
			}
			else
			{
				String coupon_DR_peroff_query="select sum(exp_debit) from (SELECT case when 0 >= a.act_debit THEN 0 else a.act_debit end as exp_debit from (select ((usage_units * usage_rate)-(SELECT ((usage_units * usage_rate) * (AMOUNT/100)) FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' and INLINE_OFFSET_IND='O' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and coupon_cd = '"+coupon_cd+"'))) as act_debit from ariacore.gl_detail where invoice_no=(select invoice_no from ariacore.gl_detail where order_no="+orderNo+" and client_no="+client_no+" and orig_coupon_cd is null) and plan_no is not null and orig_coupon_cd is null)a)"
				String coupon_DR_peroff_amount = db.executeQueryP2(coupon_DR_peroff_query)
				logi.logInfo("coupon_DR_peroff_amount" + coupon_DR_peroff_amount)
				String coupon_DR_peroff_order_query="select ((amount)-(SELECT AMOUNT FROM ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' and INLINE_OFFSET_IND='O' and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+client_no+" and coupon_cd = '"+coupon_cd+"'))) as act_debit from ariacore.orders where order_no="+orderNo+" and client_no="+client_no
				String coupon_DR_peroff_order_amount = db.executeQueryP2(coupon_DR_peroff_order_query)
				logi.logInfo("coupon_DR_perin_order_amount" + coupon_DR_peroff_order_amount)
				final_amount = (coupon_DR_peroff_amount.toDouble() + coupon_DR_peroff_order_amount.toDouble())
				final_amount = df.format(final_amount.toDouble()).toString()
				logi.logInfo("final_amount = "+final_amount)
			}
		}

		creditreduces=db.executeQueryP2(creditreduces)
		logi.logInfo("Client Credit reduces = "+creditreduces)
		if(creditreduces == "1" || creditreduces == "0")
		{
			String fedtaxamount
			fedtaxamount = "select NVL(sum(debit),0) from ariacore.gl_tax_detail where TAX_ID = (SELECT TAX_ID from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no = "+client_no+") and invoice_no= (select invoice_no from ariacore.gl_detail where order_no= "+orderNo+" and client_no= "+client_no+" and rownum = 1)"
			fedtaxamount = db.executeQueryP2(fedtaxamount)
			logi.logInfo("federalTax = "+fedtaxamount.toDouble())
			String stattaxamount
			stattaxamount = "select NVL(sum(debit),0) from ariacore.gl_tax_detail where TAX_ID = (SELECT TAX_ID from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no = "+client_no+") and invoice_no= (select invoice_no from ariacore.gl_detail where order_no= "+orderNo+" and client_no= "+client_no+" and rownum = 1)"
			stattaxamount = db.executeQueryP2(stattaxamount)
			logi.logInfo("stateTax = "+stattaxamount)
			final_coupon_amt = (final_amount.toDouble()+fedtaxamount.toDouble()+stattaxamount.toDouble()).toString()
			logi.logInfo("Amount after Tax with Coupon = "+final_coupon_amt)
		}
		return df.format(final_coupon_amt.toDouble()).toString()
	}

	/**
	 * Calculates Pending account amount from gen invoice API
	 * @param testCaseId
	 * @return invoice amount
	 */
	def md_geninvoicependingActAmt(String testCaseId)
	{
		//String apiname=testCaseId.split("-")[2]
		logi.logInfo("Calling md_geninvoicependingAmt")
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo("Client No = "+client_no)
		String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo("acct_no = "+acct_no)
		String gen_inv_no=getValueFromResponse("gen_invoice.a", "//*/*:invoice_no[1]")
		logi.logInfo("gen_inv_nonew = "+gen_inv_no)
		ConnectDB db = new ConnectDB();
		String query1 = "SELECT amount FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO= "+acct_no+" AND transaction_type=1 and source_no= "+gen_inv_no+" and CLIENT_NO="+client_no
		def geninvamount = db.executeQueryP2(query1)
		logi.logInfo("gen_inv_no Amount = "+geninvamount.toDouble())
		return df.format(geninvamount.toDouble()).toString()
	}
	
	/**
	 * Calculates Pending account amount from gen invoice API
	 * @param testCaseId
	 * @return invoice amount
	 */
	def md_geninvoicependingActAmt1(String testCaseId)
	{
		//String apiname=testcaseID.split("-")[2]
		ConnectDB db = new ConnectDB();
		logi.logInfo("Calling md_geninvoicependingAmt")
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo("Client No = "+client_no)
		String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo("acct_no = "+acct_no)
		String invs_qry = "SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO =" + acct_no
		String gen_inv_no= db.executeQueryP2(invs_qry)
		logi.logInfo("gen_inv_nonew = "+gen_inv_no)
		String query1 = "SELECT NVL((amount),0) FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO= "+acct_no+" AND transaction_type=1 and source_no= "+gen_inv_no+" and CLIENT_NO="+client_no
		def geninvamount = db.executeQueryP2(query1)
		logi.logInfo("gen_inv_no Amount = "+geninvamount.toDouble())
		return df.format(geninvamount.toDouble()).toString()
	}
	
	
	
	/**
	 * Calculates Pending account amount from gen invoice API with tax and service credit
	 * @param testCaseId
	 * @return invoice amount
	 */
	def md_geninvoicependingExpAmt(String testCaseId)
	{
		String apiname=testCaseId.split("-")[2]
		logi.logInfo("Calling md_geninvoicependingExpAmt")
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo("Client No = "+client_no)
		String acct_no=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo("acct_no = "+acct_no)
		String gen_inv_no=getValueFromResponse("gen_invoice.a", "//*/*:invoice_no[1]")
		logi.logInfo("gen_inv_nonew = "+gen_inv_no)
		String alt_bill_day = getValueFromRequest(apiname,"//alt_bill_day").toString()
		String orderAmt = "SELECT AMOUNT from ARIACORE.ORDERS where INVOICE_NO ="+gen_inv_no +"  AND CLIENT_NO ="+ client_no
		String taxAmt = "SELECT SUM(Debit) FROM ARIACORE.GL_tax_DETAIL WHERE INVOICE_NO ="+gen_inv_no
		ConnectDB dataConnect = new ConnectDB()
		orderAmt = dataConnect.executeQueryP2(orderAmt)
		logi.logInfo("Order Amount  = "+orderAmt)

		taxAmt = dataConnect.executeQueryP2(taxAmt)
		logi.logInfo("Tax Amount  = "+taxAmt)

		if(orderAmt.toString()=="null")
			orderAmt = 0

		if(taxAmt.toString()=="null")
			taxAmt = 0

		def geninvamount
		def invamtwithoutact
		def actinvamount = md_serviceInvoiceActivationAmount(testCaseId)
		logi.logInfo("Activation Amount = "+actinvamount.toDouble())
		if (!alt_bill_day.equalsIgnoreCase("NoVal"))
		{
			invamtwithoutact = md_PRORATE_INVOICE_BY_PLANS(testCaseId)
			logi.logInfo("Ram Proprate Amount without Activation  = "+invamtwithoutact.toDouble())
			geninvamount = invamtwithoutact.toDouble() + actinvamount.toDouble() + orderAmt.toDouble() + taxAmt.toDouble()
			logi.logInfo("Final Amount with Alt Bill date = "+geninvamount.toDouble())
		}
		else
		{
			invamtwithoutact = md_serviceInvoiceAmount(testCaseId)
			logi.logInfo("inv_no Amount without Activation  = "+invamtwithoutact.toDouble())
			geninvamount = invamtwithoutact.toDouble() + actinvamount.toDouble() + orderAmt.toDouble() + taxAmt.toDouble()
			logi.logInfo("Final Amount without Alt Bill date = "+geninvamount.toDouble())
		}
		return df.format(geninvamount.toDouble()).toString()
	}

	/**
	 * Calculates prorated invoice amount for plans 
	 * @param testCaseId
	 * @return prorated invoice amount
	 */
	String md_PRORATE_INVOICE_BY_PLANS(String testCaseId)
	{
		logi.logInfo "Inside md_PRORATE_INVOICE_BY_PLANS"
		def accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		//def contractNo=Constant.mycontext.expand('${Properties#ContractNo}')
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		double prorateAmount = 0
		//invoiceNo = getValueFromResponse("gen_invoice.a", "//*/*:invoice_no[1]")
		//String alt_bill_day = getValueFromRequest("gen_invoice.a","//alt_bill_day").toString()
		ConnectDB dataConnect = new ConnectDB()
		invoiceNo =  "Select Invoice_no from ariacore.gl where client_no = "+clientNo+" and Acct_no = "+accountNo+" and rownum=1 order by Invoice_no desc"
		invoiceNo = dataConnect.executeQueryP2(invoiceNo)
		logi.logInfo "invoiceNo = : "+invoiceNo
		def invoice = 0
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		def totalDays = 0
		HashMap<String, HashMap<String,String>> proPlans = getPlanServiceAndTieredPricingForProration()
		if (proPlans.size()!=0)
		{
			Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
			resultHash.putAll(proPlans)
			logi.logInfo "ResultHash Sorted(Pro) in Method: "+ resultHash.toString()
			Iterator iterator = resultHash.entrySet().iterator();
			while (iterator.hasNext())
			{
				Map.Entry item = (Map.Entry) iterator.next();
				String planKey = item.getKey()
				String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planKey + " AND CLIENT_NO= " + clientNo
				int planTypeInd = Integer.parseInt(dataConnect.executeQueryP2(planTypeQuery))
				int noOfUnits =0
				// Get Current Virtual Time
				ClientParamUtils objClient = new ClientParamUtils()
				String currentVirtualTime = objClient.getCurrentVirtualTime(clientNo)
				logi.logInfo "Current Virtual Time : "+ currentVirtualTime
				// Stores the current virtual time in the specified format
				Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
				HashMap<String,String> value = item.getValue()
				Iterator service = value.entrySet().iterator()
				Date lastBillDate
				//dataConnect.closeConnection()
				def noofUnits =0
				if(planTypeInd==0)
				{
					logi.logInfo "Master Plan : "+ planKey
					String newQuery = "SELECT PLAN_UNITS FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO = " + accountNo
					noOfUnits = Integer.parseInt(dataConnect.executeQueryP2(newQuery))
					//					noOfUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//master_plan_units"))
					logi.logInfo("RAM noOfUnits = " + noOfUnits)
				}
				else
				{
					String testId = testCaseId.substring(0, testCaseId.lastIndexOf("-")+1)

					String[] noOfUnitArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
					String[] suppPlans = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")
					if (!suppPlans.toString().equalsIgnoreCase("null")) {
						logi.logInfo("Array of supp plan")
						logi.logInfo "supp_plans :::  " + suppPlans.toString()
						logi.logInfo "noOfUnits :::  " + noOfUnitArray.toString()
						if(Arrays.asList(suppPlans).contains(planKey)){
							for( int j=0;j<suppPlans.length;j++) {
								if(suppPlans[j].toString().equalsIgnoreCase(planKey)){
									noOfUnits = Integer.parseInt(noOfUnitArray[j])
									logi.logInfo "no_of_units : "+noOfUnits+ " plan : " +planKey
									break
								}
							}
						}
					}
					else {
						logi.logInfo("single supp plan")
						String suppPlan = getValueFromRequest("create_acct_complete","//supp_plans").toString()
						logi.logInfo("single supp plan number for create acct complete : " + suppPlan)
						if ((!suppPlan.toString().equalsIgnoreCase("null"))&& suppPlan.equalsIgnoreCase(planKey)) {
							noOfUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))
							logi.logInfo("single supp plan units : " + noOfUnits)
							String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
							int noOfUnits2 = Integer.parseInt(dataConnect.executeQueryP2(supp_plan_unit_query))

							if(noOfUnits2 > noOfUnits) {
								noOfUnits = noOfUnits2
							}

							logi.logInfo "single_supp_plan :::  " + suppPlan
							logi.logInfo "single_plan_noOfUnits :::  " + noOfUnits
						}
						else {
							logi.logInfo "assign or replace supp plan"
							String supp_plan = getValueFromRequest("assign_supp_plan","//supp_plan_no")
							if(supp_plan.equalsIgnoreCase(planKey))
								noOfUnits = Integer.parseInt(getValueFromRequest("assign_supp_plan","//num_plan_units"))
							else{
								supp_plan = getValueFromRequest("replace_supp_plan","//supp_plan_no")
								if(supp_plan.equalsIgnoreCase(planKey))
									noOfUnits = Integer.parseInt(getValueFromRequest("replace_supp_plan","//num_plan_units"))
								else {
									String supp_plan_unit_query = "SELECT SUPP_PLAN_RECURRING_FACTOR FROM ARIACORE.ACCT_SUPP_PLAN_MAP WHERE ACCT_NO=" + accountNo
									noOfUnits = Integer.parseInt(db.executeQueryP2(supp_plan_unit_query))
								}
							}
						}
					}

					/*String[] noOfUnitsArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plan_units_row/supp_plan_units")
					 String[] suppPlanArray = getAllValuesFromRequest(testId+"create_acct_complete","//supp_plans_row/supp_plans")
					 if (suppPlanArray.toString().equalsIgnoreCase("null"))
					 {
					 logi.logInfo("coming to singleplan if part")
					 int suppPlan = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plans"))
					 noofUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//supp_plan_units"))
					 logi.logInfo "single_supp_plans :::  " + suppPlan
					 logi.logInfo "single_plan_noOfUnits11 :::  " + noofUnits
					 }*/
				}
				//Claculating the remaining days
				Date acctBillStartDate = gengetBillStartDate(clientNo, accountNo)
				Calendar cal2 = new GregorianCalendar()
				cal2.setTime(acctBillStartDate);
				logi.logInfo "Done cal2: "
				int createdMonth = cal2.get(Calendar.MONTH)+1
				logi.logInfo "createdMonth : "+createdMonth
				int createdYear = cal2.get(Calendar.YEAR)
				logi.logInfo "createdYear : "+createdYear
				int remainingDays = gengetNoOfDaysFromAcctBillStartDay(clientNo,accountNo)
				logi.logInfo "no_of_unitsssss : "+noOfUnits+ " plan : " +planKey
				String altFee = "0"
				//Claculating the Total days
				Calendar cal3 = new GregorianCalendar()
				Date acctpreviousBillDate = acctBillStartDate
				cal3.setTime(acctpreviousBillDate);
				logi.logInfo "Done cal3: "
				int precreatedDay = cal3.get(Calendar.DATE)
				logi.logInfo "New predate : "+precreatedDay
				int precreatedMonth = cal3.get(Calendar.MONTH)-1
				logi.logInfo "New createdMonth : "+precreatedMonth
				int precreatedYear = cal3.get(Calendar.YEAR)
				logi.logInfo "New createdYear : "+precreatedYear
				cal3.set(precreatedYear,precreatedMonth, precreatedDay );
				String prevDateString = simpleFormat.format(cal3.getTime())
				logi.logInfo "Previous Bill date : "+ prevDateString
				acctpreviousBillDate = simpleFormat.parse(prevDateString)
				totalDays = getDaysBetween(acctpreviousBillDate, acctBillStartDate)
				logi.logInfo "New Total days : "+ totalDays
				while(service.hasNext())
				{
					Map.Entry servicePairs = (Map.Entry) service.next()
					String service_no = servicePairs.getKey().toString()
					int seq_no=1;
					String tieredPricing = servicePairs.getValue().toString()
					int tieredPricingInt = Integer.parseInt(tieredPricing)
					logi.logInfo "RemainingDays : " + remainingDays
					logi.logInfo "TotalDay : " + totalDays
					double prorationFactor = remainingDays/totalDays
					logi.logInfo "prorationFactor : " + prorationFactor
					def ratePerUnit = 0
					// if (!altFee.equalsIgnoreCase("0"))
					//            ratePerUnit =altFee
					logi.logInfo "AltFee : " + altFee
					logi.logInfo "TieredPricingInt : " + tieredPricingInt
					switch(tieredPricingInt)
					{
						case 1:
							noOfUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//master_plan_units"))
							ratePerUnit = dataConnect.executeQueryP2("SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planKey+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+service_no+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seq_no+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no ")
						// database2.closeConnection()
							break;
						case 2 :
						//ConnectDB database2 = new ConnectDB()
							noOfUnits = Integer.parseInt(getValueFromRequest("create_acct_complete","//master_plan_units"))
							ratePerUnit = dataConnect.executeQueryP2("select rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+service_no+" and plan_no = "+planKey+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1")
						// database2.closeConnection()
							break;
						case 3 :
						// ConnectDB database2 = new ConnectDB()
							ratePerUnit =dataConnect.executeQueryP2("select rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+service_no+" and plan_no = "+planKey+" and ("+noOfUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1")
							noOfUnits = 1
						// database2.closeConnection()
							break;
					}

					double prorationUnits = noOfUnits.toDouble() * prorationFactor.toDouble()
					double amt = prorationUnits.toDouble() * ratePerUnit.toDouble()
					logi.logInfo "no_of_units : "+noOfUnits+" prorationFactor : "+prorationFactor+ " prorationUnits : " +prorationUnits
					logi.logInfo "prorationUnits : "+prorationUnits+" ratePerUnit : "+service_no+ " ratePerUnit : " +ratePerUnit+" Amount : :  " + amt
					logi.logInfo "Plan : "+planKey+" Service : "+service_no+ " Tiered Pricing : " +tieredPricing+" Amount : :  " + amt
					DecimalFormat df = new DecimalFormat("#.##");
					prorateAmount = prorateAmount + (df.format(amt)).toDouble()
				}

			}
		}

		DecimalFormat df = new DecimalFormat("#.##");
		double resultInvoice = prorateAmount +  (df.format(invoice)).toDouble()
		return df.format(resultInvoice).toString();
	}

	/**
	 * Predicts the accounts bill start date
	 * @param testCaseId, account number
	 * @return Bill start date
	 */
	public Date gengetBillStartDate(String client_no, String acct_no)
	{
		ClientParamUtils clientParamUtils;
		Date acctCreatedDate = new Date();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String retroactiveStartDate = getValueFromRequest("create_acct_complete","//retroactive_start_date").toString()
		String alt_bill_day = ""
		alt_bill_day = getValueFromRequest("gen_invoice.a","//alt_bill_day").toString()
		String acctActualStartDate = null
		int daysInMonth
		ConnectDB database = new ConnectDB();
		Calendar cal = new GregorianCalendar()

		logi.logInfo "In loop Alt Bill Day not empty"
		ResultSet rs = database.executeQuery("SELECT created from ariacore.acct_details where acct_no = "+ acct_no)
		if (rs.next())
		{
			acctActualStartDate = rs.getDate(1)
			acctCreatedDate = rs.getDate(1)
		}

		cal.setTime(acctCreatedDate);
		logi.logInfo "Start Date / Create date from DB: "+acctActualStartDate

		int createdDay = cal.get(Calendar.DATE)
		int createdMonth = cal.get(Calendar.MONTH)+1
		int createdYear = cal.get(Calendar.YEAR)
		int alt_bill_day_int
		if(alt_bill_day != "NoVal") {
			logi.logInfo("Alt bill loop : " + alt_bill_day)
			alt_bill_day_int = Integer.parseInt(alt_bill_day)
		}
		else
			alt_bill_day_int = 0

		logi.logInfo "Alt bill Date in getBillDate : "+alt_bill_day
		logi.logInfo "Alt bill Date in alt_bill_day_int  : "+alt_bill_day_int
		logi.logInfo "createdDay : "+createdDay
		logi.logInfo "createdMonth : "+createdMonth
		logi.logInfo "createdYear : "+createdYear

		if ((createdMonth==1) || (createdMonth==3) || (createdMonth==5) || (createdMonth==7) || (createdMonth==8) || (createdMonth==10) || (createdMonth==12))
			daysInMonth = 31
		else
			daysInMonth = 30


		logi.logInfo "daysInMonth : "+daysInMonth

		if(alt_bill_day_int>=createdDay && alt_bill_day_int<= daysInMonth )
		{
			logi.logInfo "Ram Loop1 "
			cal.set(createdYear,createdMonth-1, alt_bill_day_int );
		}
		else if(alt_bill_day_int<createdDay)
		{
			logi.logInfo "Ram Loop2 "
			if(createdMonth==11)
			{
				logi.logInfo "Ram Loo3 "
				cal.set(createdYear+1,0, alt_bill_day_int );
			}
			else
			{
				logi.logInfo "Ram Loop4 "
				cal.set(createdYear,createdMonth, alt_bill_day_int );
			}
		}

		String finalDateString = simpleFormat.format(cal.getTime())
		logi.logInfo "Bill start date : "+ finalDateString
		Date finalDate = simpleFormat.parse(finalDateString)
		return finalDate
	}

	/**
	 * Generates number of days between account bill start date to current date
	 * @param testCaseId, account number
	 * @return number of days
	 */
	int gengetNoOfDaysFromAcctBillStartDay(String clientNo, String accountNo)
	{
		logi.logInfo "Inside getNoOfDaysFromAcctBillStartDay"
		Date acctStartDate = gengetBillStartDate(clientNo, accountNo)
		logi.logInfo "Ram acctStartDate : "+acctStartDate
		logi.logInfo "got answer from getBillStartDate"

		// Get Current Virtual Time
		ClientParamUtils objClient = new ClientParamUtils()
		String currentVirtualTime = objClient.getCurrentVirtualTime(clientNo)
		logi.logInfo "got answer from currentVirtualTime : "+ currentVirtualTime
		// Stores the current virtual time in the specified format
		Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		logi.logInfo "Done currentVTDate: "
		//Date acctBillStartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(acctStartDate);

		Calendar cal1 = new GregorianCalendar()
		cal1.setTime(currentVTDate);
		logi.logInfo "Done cal1: "
		Calendar cal2 = new GregorianCalendar()
		cal2.setTime(acctStartDate);
		logi.logInfo "Done cal2: "

		// Find days between them to calculate the Days from AccountCreation
		logi.logInfo "      Ram acctStartDate " + acctStartDate.getTime()
		int daysFromAcctBillStartDay = ((acctStartDate.getTime() - currentVTDate.getTime()) / (1000 * 60 * 60 * 24))
		logi.logInfo "Ram daysFromAcctBillStartDay : "+daysFromAcctBillStartDay
		int createdMonth = cal2.get(Calendar.MONTH)
		logi.logInfo "createdMonth : "+createdMonth
		int createdYear = cal2.get(Calendar.YEAR)
		logi.logInfo "createdYear : "+createdYear

		return (daysFromAcctBillStartDay+1)
	}

	/**
	 * Calculates invoice amount after assigning supp plan to a account with credit and tax reduced
	 * @param testCaseId
	 * @return invoice amount
	 */
	public String md_ExpInvoice_assign_supp_plan(String testCaseId)
	{

		logi.logInfo("calling md_ExpInvoiceWithoutUsage_Amt")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		double usg = 0.0
		double cal_usg
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##");
		String apiname=testCaseId.split("-")[2]
		double totalInvoice=0.0
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no)
		logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		def exp = md_assign_supp_Proration_amount(testCaseId,acct_no)
		logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		def creditamt=db.executeQueryP2("SELECT NVL(SUM(AMOUNT-LEFT_TO_APPLY),0) FROM ARIACORE.CREDITS WHERE ACCT_NO ="+acct_no).toString().toDouble()
		logi.logInfo( "Credit amount " +creditamt)
		def taxamt =db.executeQueryP2("select NVL(sum(debit),0) from ariacore.gl_detail where invoice_no =(select Max(invoice_no) from ariacore.gl where acct_no ="+acct_no +") and service_no in(400,401,402,405,406) and debit > 0").toString().toDouble()
		logi.logInfo( "Tax Maount " +taxamt)
		totalInvoice =  ((exp.toDouble() + taxamt.toDouble())-(creditamt.toDouble()))
		logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		return d.format(totalInvoice.toDouble()).toString()
		logi.logInfo("totalInvoice in md_ExpInvoiceWithoutUsage_Amt method "+totalInvoice)
		return d.format(totalInvoice).toString()

	}

	/**
	 * Calculates prorated invoice amount after assigning supp plan to a account
	 * @param testCaseId, account number
	 * @return prorated invoice amount
	 */
	public String md_assign_supp_Proration_amount(String testcaseID,String accountNo)
	{
		logi.logInfo("assign_supp")
		Date assigned_date= new Date();
		Date unassigned_date= new Date();
		String planNumber
		String [] plan
		double credit = 0
		int length
		DecimalFormat d = new DecimalFormat("#.##");
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String apiname=testcaseID.split("-")[2]
		logi.logInfo("apiname name "+ apiname)
		if(apiname == 'assign_supp_plan'||apiname == 'assign_supp_plan.a' ||apiname == 'assign_supp_plan.b'|| apiname == 'modify_supp_plan')
		{
			planNumber = getValueFromRequest(apiname,"//supp_plan_no")
		}
		else if(apiname == 'replace_supp_plan')
		{
			planNumber = getValueFromRequest(apiname,"//new_supp_plan_no")
		}
		else if(apiname == 'assign_supp_plan_multi' || apiname == 'assign_supp_plan_multi.a')
		{
			plan = getAllValuesFromRequest(testcaseID,"//supp_plan_no")
			logi.logInfo("Plan Number1"+ plan)		
		}

		logi.logInfo("Plan Number"+ planNumber)
		ConnectDB database = new ConnectDB();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = database.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time : "+ currentVirtualTime
		//int useddays=database.executeQueryP2("SELECT (TRUNC(next_bill_date) -  trunc(plan_date)) AS days FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo).toInteger()
		//int useddays=database.executeQueryP2("SELECT TRUNC(LAST_BILL_THRU_DATE+1) - (select CASE WHEN  (trunc(plan_date) >= trunc(last_recur_bill_date) or last_recur_bill_date is null ) THEN trunc(plan_date) ELSE trunc(last_recur_bill_date)  END        FROM ariacore.acct WHERE acct_no = "+accountNo+")from ariacore.acct WHERE acct_no = "+accountNo+"  AND client_no ="+clientNo).toInteger()

		String assign_qry="SELECT CREATED FROM ariacore.ACCT_DETAILS WHERE ACCT_NO=" + accountNo
		ResultSet resultSet = database.executeQuery(assign_qry);

		while(resultSet.next())
		{
			assigned_date = resultSet.getDate(1);
		}
		logi.logInfo("assigned_date"+assigned_date)
		String unassign_qry="SELECT CREATE_DATE FROM ARIACORE.ACCT_SUPP_PLAN_MAP_VIEW WHERE ACCT_NO=" + accountNo +" AND SUPP_PLAN_NO= " +planNumber
		ResultSet resultSet1 = database.executeQuery(unassign_qry);
		while(resultSet1.next())
		{
			unassigned_date = resultSet1.getDate(1);
		}
		logi.logInfo("Unassigned_date"+unassigned_date)
		int useddays= getDaysBetween(assigned_date,unassigned_date)+1
		logi.logInfo("Plan useddays"+ useddays)
		Map<String, HashMap<String,String>> hash
		Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
		Iterator iterator
		if(apiname == 'assign_supp_plan_multi' || apiname == 'assign_supp_plan_multi.a')
		{
			/* for(int planitr=0;planitr< plan.size();planitr++)
			{
			 hash = getAssignsuppPlanServiceAndTieredPricing(plan[planitr])			
			}
			
			resultHash.putAll(hash)
			iterator = resultHash.entrySet().iterator() */
			
			//logi.logInfo "Before Key setting " 
			
			hash = getAssignsuppPlanServiceAndTieredPricing3(plan)
			resultHash = new TreeMap<String, TreeMap<String,String>> ()
		   resultHash.putAll(hash)
		   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
		   
					iterator = resultHash.entrySet().iterator()
				   logi.logInfo "Before Key setting "
			
		}
		else
		{
			hash = getAssignsuppPlanServiceAndTieredPricing(planNumber)
			 resultHash = new TreeMap<String, TreeMap<String,String>> ()
			resultHash.putAll(hash)
			logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
			
					 iterator = resultHash.entrySet().iterator()
					logi.logInfo "Before Key setting "
		}
		

		
		logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()

		def invoice = 0
		double proration_factor
		int cou_amount

		while (iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) iterator.next()
			String planNo = pairs.getKey().toString()
			logi.logInfo "Execution for the plan :  "+ planNo
			String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
			String planStatusQuery, planStatusDataQuery

			int planTypeInd =Integer.parseInt(database.executeQueryP2(planTypeQuery))

			logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo


			if(planTypeInd==1) {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT_SUPP_PLAN_MAP AC WHERE ACCT_NO   ="+accountNo+" AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date from ariacore.ACCT_SUPP_PLAN_MAP where acct_no = "+ accountNo +"AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo

			}
			else {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+" AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+ " AND CLIENT_NO= " + clientNo

			}

			int planStatus =Integer.parseInt(database.executeQueryP2(planStatusQuery))
			logi.logInfo "Plan Status : "+planStatus+ " plan : " +planNo
			logi.logInfo "Plan planStatusDataQuery : "+planStatusDataQuery+ " plan : " +planNo


			if(planStatus!=0){
				String statusDateDB = database.executeQueryP2(planStatusDataQuery)
				logi.logInfo "Status date from DB: "+statusDateDB
				Date statusDate = simpleFormat.parse(statusDateDB)

				// Stores the current virtual time in the specified format
				Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
				logi.logInfo "planStatus : "+planStatus+ " plan : " +planNo
				//logi.logInfo "statusDate.compareTo(currentVTDate) : "+statusDate.compareTo(currentVTDate)+ " plan : " +planNo


				//if((planStatus==1 && statusDate.compareTo(currentVTDate)<=0)|| (planTypeInd==0)){

				Map<String,String> valueMap = pairs.getValue()
				Map<String,String> value = new TreeMap<String,String> ()
				value.putAll(valueMap)

				int billingInterval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planNo+ "and client_no="+clientNo))
				Date nextbilldate
				ResultSet rs=database.executePlaQuery("SELECT TRUNC(LAST_BILL_THRU_DATE+1) FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo)
				rs.next()
				nextbilldate=rs.getDate(1)
				Date virtualstartdate = subractMonthsFromGivenDate(nextbilldate,billingInterval)

				logi.logInfo "virtualstartdate "+virtualstartdate
				logi.logInfo "nextbilldate " +nextbilldate
				int durationOfPlan = getDaysBetween(virtualstartdate,nextbilldate )
				logi.logInfo "Duration of a plan : "+planNo + " :: "+durationOfPlan
				logi.logInfo "DaysInvoice "+useddays
				logi.logInfo "daysInvoice % durationOfPlan Value : "+ (useddays % durationOfPlan )
				int tmp_days = durationOfPlan-useddays+1
				logi.logInfo "tmp_days "+tmp_days

				//proration_factor=tmp_days/durationOfPlan.toDouble()
				String Proration_qry="SELECT PRORATION_FACTOR FROM ariacore.GL_DETAIL WHERE SEQ_NUM=2 AND INVOICE_NO=(SELECT MAX(INVOICE_NO) FROM ariacore.GL WHERE ACCT_NO =" +accountNo+")"
				String proration=database.executeQueryP2(Proration_qry)
				proration_factor=proration.toDouble()
				d.format(proration_factor)
				logi.logInfo "proration_factor "+proration_factor

				//proration_factor=tmp_days/durationOfPlan.toDouble()
				//logi.logInfo "proration_factor "+proration_factor

				//if((daysInvoice % durationOfPlan)<=13){
				/*if((planEndMonth==1 && (palnEndYear%4!=0)) ||(planStartMonth==1 && (palnStartYear%4!=0))){
				 daysInvoice-=1
				 }*/
				Iterator service = value.entrySet().iterator()
				logi.logInfo "Invoice calculation for a plan : "+planNo
				String unitsquery="select recurring_factor from ariacore.all_acct_recurring_factors where ACCT_NO ="+accountNo+" and plan_no="+planNo
				int noofUnits =database.executeQueryP2(planStatusQuery).toInteger()

				logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo
				String couponqry = "SELECT COUPON_CD FROM ariacore.ACCT_COUPONS WHERE ACCT_NO="+accountNo
				String coupon_cd=database.executeQueryP2(couponqry)
				logi.logInfo "Coupon : "+ coupon_cd
				String ioflag_qry ="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
				String ioflag_indicator=database.executeQueryP2(ioflag_qry)
				logi.logInfo "ioflag_indicator : "+ ioflag_indicator
				String flat_per_qry = "SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
				String flatper_indicator=database.executeQueryP2(flat_per_qry)
				logi.logInfo "flatper_indicator : "+ flatper_indicator

				if(flatper_indicator != null  && ioflag_indicator != null )
				{
					logi.logInfo "Entering into the loop"
					String amount_qry = "SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
					cou_amount = Integer.parseInt(database.executeQueryP2(amount_qry))
					logi.logInfo "cou_amount : "+ cou_amount
				}

				while(service.hasNext())
				{
					int flag=0
					double temp = 0
					Map.Entry servicePairs = (Map.Entry) service.next()
					String serviceNo = servicePairs.getKey().toString()
					logi.logInfo "Invoice calculation for a service : "+serviceNo
					String srvqry="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO ="+serviceNo+ "AND CLIENT_NO="+clientNo
					String srvctype=database.executeQueryP2(srvqry)
					logi.logInfo "Service type: "+srvctype
					String flag_qry = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT' AND CLIENT_NO="+clientNo
					String fla_val=database.executeQueryP2(flag_qry)
					logi.logInfo "CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT : "+fla_val
					String flag_qry1 = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT' AND CLIENT_NO="+clientNo
					String fla_val1=database.executeQueryP2(flag_qry1)
					logi.logInfo "MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT : "+fla_val
					if((srvctype == 'AC' && fla_val== 'TRUE') && (srvctype == 'AC' && fla_val1 != 'FALSE'))
					{
						flag=1
					}
					else if((srvctype == 'AC' && fla_val== 'FALSE') || (srvctype == 'AC' && fla_val1 == 'FALSE'))
					{
						flag=2;
					}
					else if((srvctype == 'AC' && fla_val== 'TURE') || (srvctype == 'AC' && fla_val1 == 'FALSE'))
					{
						flag=2;
					}
					logi.logInfo "Flag "+flag

					int seqNo=1;
					String tieredPricing = servicePairs.getValue().toString()
					int tieredPricingInt = Integer.parseInt(tieredPricing)
					logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
					String altFee = "NoVal"
					double serviceInvoice
					String query
					switch(tieredPricingInt)
					{
						case 1:
							logi.logInfo "Case 1 "
							if(flag == 2 )
							{
								query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
								break;
							}
							else if(flag == 1)
							{
								String unit_diff = database.executeQueryP2("SELECT max(recurring_factor)-min(recurring_factor) from ariacore.acct_rates_history_details where acct_no = "+accountNo+"  and client_no = "+clientNo+" and plan_no = "+planNo+" and service_no = "+serviceNo+" and rate_seq_no = 1 ")
								if(unit_diff.toInteger() > 0){
									query = "SELECT NRSR.RATE_PER_UNIT * " + unit_diff.toInteger() + " as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
								}
								else
									query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
								serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								invoice
								double total =(serviceInvoice)
								logi.logInfo "total amount " + total
								invoice = invoice + total
								d.format(invoice)
								logi.logInfo "After adding with invoice " + invoice
								seqNo++;
								if(flatper_indicator == 'P' && total != 0)
								{
									temp = total * (cou_amount / 100)
									credit = credit + temp
									d.format(credit)
								}
								else if (total != 0)
								{
									temp = cou_amount
									credit= credit + temp
									d.format(credit)
								}
								else if(flatper_indicator == 'null'  && ioflag_indicator == 'null')
								{
									credit = 0
								}
								logi.logInfo "Credit " + credit
								break;
							}
							else if (srvctype != 'MN')
							{

								query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
							serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
							logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
							double total =(serviceInvoice * proration_factor)
							logi.logInfo "total amount " + total
							invoice = invoice + total
							d.format(invoice)
							logi.logInfo "After adding with invoice " + invoice
							if(flatper_indicator == 'P' && total != 0)
							{
								temp = total * (cou_amount / 100)
								credit = credit + temp
								d.format(credit)
							}
							else if (total != 0)
							{
								temp = cou_amount
								credit= credit + temp
								d.format(credit)
							}
							else if(flatper_indicator == 'null'  && ioflag_indicator == 'null')
							{
								credit = 0
							}
							}
							logi.logInfo "Credit " + credit
							seqNo++;
							break;
						case 2 :
							logi.logInfo "Case 2"
							if(!altFee.equals("NoVal"))
							{
								query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit ) and rownum = 1 order by rate_hist_seq_no desc"
							}
							else
								query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"

							String temp1=database.executeQueryP2(query)
							if(temp1==null)
								serviceInvoice = 0
							else
								serviceInvoice = Integer.parseInt(temp1)
							logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
							double total = (serviceInvoice * proration_factor)
							logi.logInfo "total amount " + total
							invoice = invoice + total
							d.format(invoice)
							logi.logInfo "After adding with invoice " + invoice
							if(flatper_indicator == 'P' && total != 0)
							{
								temp = total * (cou_amount / 100)
								credit = credit + temp
								d.format(credit)
							}
							else if (total != 0)
							{
								temp = cou_amount
								credit= credit + temp
								d.format(credit)
							}
							else if(flatper_indicator == 'null'  && ioflag_indicator == 'null')
							{
								credit = 0
							}
							logi.logInfo "Credit " + credit
							seqNo++;
							break;
							break;
						case 3 :
							logi.logInfo "Case 3"
							query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
							String temp1=database.executeQueryP2(query)
							if(temp1==null)
								serviceInvoice = 0
							else
								serviceInvoice = Integer.parseInt(temp1)
							logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
							double total = (serviceInvoice * proration_factor)
							logi.logInfo "total amount " + total
							invoice = invoice + total
							d.format(invoice)
							logi.logInfo "After adding with invoice " + invoice
							if(flatper_indicator == 'P' && total != 0)
							{
								temp = total * (cou_amount / 100)
								credit = credit + temp
								d.format(credit)
							}
							else if (total != 0)
							{
								temp = cou_amount
								credit= credit + temp
								d.format(credit)
							}
							else if(flatper_indicator == 'null'  && ioflag_indicator == 'null')
							{
								credit = 0
							}
							logi.logInfo "Credit " + credit
							seqNo++;
							break;
							break;
					}
				}
				//}
				//else
				//     logi.logInfo 'Else loop'

				//}
			}

		}

		//String proration_factor_db=db.executeQueryP2("select distinct proration_factor from ariacore.gl_detail where invoice_no="+invoiceNo+" and proration_factor is not null")
		double tinvoice=(invoice.toDouble()).round(2) - credit
		d.format(tinvoice)
		logi.logInfo("Final Invoice amount"+tinvoice)
		//double tinvoice=(invoice*proration_factor.toDouble()).round(2)
		return d.format(tinvoice).toString()
	}

	/**
	 * To find services and tiered pricing for the assigned supp plan
	 * @param plan number
	 * @return HashMap of available services for the plan
	 */
	public HashMap getAssignsuppPlanServiceAndTieredPricing(String planNo)
	{
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select supp_plan_no from ariacore.acct_supp_plan_map_view where acct_no = "+acct_no+" and supp_plan_no = "+ planNo +") and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and usage_based = 0) order by plan_no, service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select supp_plan_no from ariacore.acct_supp_plan_map_view where acct_no = "+acct_no+" and supp_plan_no = "+ planNo +") and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and usage_based = 0) order by plan_no, service_no"))
		ResultSet resultSet = database.executeQuery(query)

		HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		HashMap<String,String> serviceHash

		String[] plan = new String[count]
		String[] service = new String[count]
		String[] tieredPricing = new String[count]

		int i= 0
		while(resultSet.next()){
			plan[i]=resultSet.getString(1)
			service[i]=resultSet.getString(2)
			tieredPricing[i]=resultSet.getString(3)
			i++
		}
		String plan_no
		int serviceCount=0
		for(int planCount=0;planCount<plan.length;){
			plan_no = plan[planCount]
			serviceHash = new HashMap<String,String> ()
			for(;serviceCount<service.length;serviceCount++){
				if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					planCount ++
				}
				else
					break
			}
			resultHash.put(plan_no, serviceHash)
			logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
		}
		logi.logInfo "Total Services" + resultHash.toString()
		return resultHash
	}

	/**
	 * Calculates invoice amount after assigning supp plan to a account with Activation amount added, credit and tax reduced
	 * @param testCaseId
	 * @return invoice amount
	 */
	String md_ExpInvoice_assign_supp_plan_withActivation(String testCaseId) {
		String invoice = md_ExpInvoice_assign_supp_plan(testCaseId)
		String activation_amt = md_serviceInvoiceActivationAmount(testCaseId)
		double tot_inv = invoice.toDouble() + activation_amt.toDouble()
		return (new DecimalFormat("#.##").format(tot_inv)).toString()
	}

	/**
	 * Calculates invoice amount with tax reduced, added Actiation amount for CR-0
	 * @param testCaseId
	 * @return invoice amount
	 */
	String md_geninvoicependingExpAmt_with_Tax_CR0 (String testCaseId){
		String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		double inv_amt = md_PRORATE_INVOICE_BY_PLANS(testCaseId).toDouble()
		logi.logInfo("Invoice Amt received : " + inv_amt)
		double activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
		double inv_with_act = inv_amt + activation_amt
		logi.logInfo("Invoice with activation : " + inv_with_act)
		double stateTax = inv_amt * 0.1
		double federalTax = inv_amt * 0.2
		logi.logInfo("federal and state taxes are :: " + federalTax + " :: " + stateTax)
		double creditApplied = md_ACCOUNT_MULTIPLE_CREDIT_APPLIED(testCaseId).toDouble()
		double total_inv = inv_with_act + stateTax + federalTax - creditApplied
		logi.logInfo("Total Invoice :: " + total_inv)
		return (new DecimalFormat("#").format(total_inv)).toString()
	}

	/**
	 * Calculates invoice amount with tax reduced, added Actiation amount for CR-1
	 * @param testCaseId
	 * @return invoice amount
	 */
	String md_geninvoicependingExpAmt_with_Tax_CR1 (String testCaseId){
		String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		double inv_amt = md_PRORATE_INVOICE_BY_PLANS(testCaseId).toDouble()
		logi.logInfo("Invoice Amt received : " + inv_amt)
		double activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
		double inv_with_act = inv_amt + activation_amt
		logi.logInfo("Invoice with activation : " + inv_with_act)

		double creditApplied = md_ACCOUNT_MULTIPLE_CREDIT_APPLIED(testCaseId).toDouble()

		double stateTax = (inv_amt-creditApplied) * 0.1
		double federalTax = (inv_amt-creditApplied) * 0.2
		logi.logInfo("federal and state taxes are :: " + federalTax + " :: " + stateTax)
		double total_inv = inv_with_act + stateTax + federalTax - creditApplied
		logi.logInfo("Total Invoice :: " + total_inv)
		return (new DecimalFormat("#").format(total_inv)).toString()
	}

	/**
	 * Calculates invoice amount after updating master plan to a account with credit and tax reduced
	 * @param testCaseId
	 * @return invoice amount
	 */
	public String md_ExpInvoice_update_master_plan(String testCaseId)
	{
		logi.logInfo("calling md_ExpInvoiceWithoutUsage_Amt")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		def tax
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##");
		String apiname=testCaseId.split("-")[2]
		def totalInvoice
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no)
		logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		def exp = md_update_master_Proration_amount(testCaseId,acct_no)
		logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		def creditamt=db.executeQueryP2("SELECT NVL(SUM(AMOUNT-LEFT_TO_APPLY),0) FROM ARIACORE.CREDITS WHERE ACCT_NO ="+acct_no + " and (alt_service_no_2_apply not in (400,401,402,405,406) or reason_cd =1)").toString().toDouble()
		logi.logInfo( "Credit amount " +creditamt)
		def taxamt =db.executeQueryP2("select NVL(sum(debit),0) from ariacore.gl_detail where invoice_no =(select Max(invoice_no) from ariacore.gl where acct_no ="+acct_no +") and service_no in(400,401,402,405,406)").toString().toDouble()
		logi.logInfo( "Tax Maount " +taxamt)
		totalInvoice =  ((exp.toDouble() + taxamt.toDouble())-(creditamt.toDouble()))
		logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		return d.format(totalInvoice.toDouble()).toString()
	}


	/**
	 * Calculates prorated invoice amount after updatng master plan to a account
	 * @param testCaseId, account number
	 * @return prorated invoice amount
	 */
	public String md_update_master_Proration_amount(String testcaseID,String accountNo)
	{
		logi.logInfo("Update Master plan")
		Date assigned_date= new Date();
		Date unassigned_date= new Date();
		DecimalFormat d = new DecimalFormat("#.##");
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String apiname=testcaseID.split("-")[2]
		String planNumber = getValueFromRequest(apiname,"//master_plan_no")
		if(planNumber == 'NoVal')
		{
			planNumber = getValueFromRequest("update_master_plan","//master_plan_no")
		}
		String CAC_planNumber = getValueFromRequest("create_acct_complete","//master_plan_no")
		logi.logInfo("Plan Number"+ planNumber)
		logi.logInfo("CAC_Plan Number"+ CAC_planNumber)
		double credit = 0
		ConnectDB database = new ConnectDB();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = database.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time : "+ currentVirtualTime
		//int useddays=database.executeQueryP2("SELECT (TRUNC(next_bill_date) -  trunc(plan_date)) AS days FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo).toInteger()
		//int useddays=database.executeQueryP2("SELECT TRUNC(LAST_BILL_THRU_DATE+1) - (select CASE WHEN  (trunc(plan_date) >= trunc(last_recur_bill_date) or last_recur_bill_date is null ) THEN trunc(plan_date) ELSE trunc(last_recur_bill_date)  END 	FROM ariacore.acct WHERE acct_no = "+accountNo+")from ariacore.acct WHERE acct_no = "+accountNo+"  AND client_no ="+clientNo).toInteger()

		String assign_qry="SELECT CREATED FROM ariacore.ACCT_DETAILS WHERE ACCT_NO=" + accountNo
		ResultSet resultSet = database.executeQuery(assign_qry);

		while(resultSet.next())
		{
			assigned_date = resultSet.getDate(1);
		}
		logi.logInfo("assigned_date"+assigned_date)
		String unassign_qry="SELECT FROM_DATE FROM ARIACORE.ACCT_PLAN_HIST WHERE ACCT_NO=" + accountNo +" AND NEW_PLAN= " +planNumber
		ResultSet resultSet1 = database.executeQuery(unassign_qry);
		while(resultSet1.next())
		{
			unassigned_date = resultSet1.getDate(1);
		}
		logi.logInfo("Unassigned_date"+unassigned_date)
		int useddays= getDaysBetween(assigned_date,unassigned_date)+1
		logi.logInfo("Plan useddays"+ useddays)

		Map<String, HashMap<String,String>> hash = getUpdateMasterPlanServiceAndTieredPricing(planNumber)

		Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
		resultHash.putAll(hash)

		logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()

		Iterator iterator = resultHash.entrySet().iterator()
		logi.logInfo "Before Key setting "


		double invoice = 0
		double proration_factor

		while (iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) iterator.next()
			String planNo = pairs.getKey().toString()
			logi.logInfo "Execution for the plan :  "+ planNo
			String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
			String planStatusQuery, planStatusDataQuery

			int planTypeInd = Integer.parseInt(database.executeQueryP2(planTypeQuery))

			logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo


			if(planTypeInd==1) {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT_SUPP_PLAN_MAP AC WHERE ACCT_NO   ="+accountNo+" AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date from ariacore.ACCT_SUPP_PLAN_MAP where acct_no = "+ accountNo +"AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo

			}
			else {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+" AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+ " AND CLIENT_NO= " + clientNo

			}

			int planStatus = Integer.parseInt(database.executeQueryP2(planStatusQuery))
			logi.logInfo "Plan Status : "+planStatus+ " plan : " +planNo
			logi.logInfo "Plan planStatusDataQuery : "+planStatusDataQuery+ " plan : " +planNo


			if(planStatus!=0){
				String statusDateDB = database.executeQueryP2(planStatusDataQuery)
				logi.logInfo "Status date from DB: "+statusDateDB
				Date statusDate = simpleFormat.parse(statusDateDB)

				// Stores the current virtual time in the specified format
				Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
				logi.logInfo "planStatus : "+planStatus+ " plan : " +planNo
				logi.logInfo "statusDate.compareTo(currentVTDate) : "+statusDate.compareTo(currentVTDate)+ " plan : " +planNo


				if((planStatus==1 && statusDate.compareTo(currentVTDate)<=0)|| (planTypeInd==0)){

					Map<String,String> valueMap = pairs.getValue()
					Map<String,String> value = new TreeMap<String,String> ()
					value.putAll(valueMap)

					int billingInterval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planNo+ "and client_no="+clientNo))
					Date nextbilldate
					ResultSet rs=database.executePlaQuery("SELECT TRUNC(LAST_BILL_THRU_DATE+1) FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo)
					rs.next()
					nextbilldate=rs.getDate(1)
					Date virtualstartdate = subractMonthsFromGivenDate(nextbilldate,billingInterval)

					logi.logInfo "virtualstartdate "+virtualstartdate
					logi.logInfo "nextbilldate " +nextbilldate
					int durationOfPlan = getDaysBetween(virtualstartdate,nextbilldate )
					logi.logInfo "Duration of a plan : "+planNo + " :: "+durationOfPlan
					logi.logInfo "DaysInvoice "+useddays
					logi.logInfo "daysInvoice % durationOfPlan Value : "+ (useddays % durationOfPlan )
					int tmp_days = durationOfPlan-useddays+1
					logi.logInfo "tmp_days "+tmp_days
					//proration_factor=tmp_days/durationOfPlan.toDouble()
					String Proration_qry="SELECT PRORATION_FACTOR FROM ariacore.GL_DETAIL WHERE SEQ_NUM=2 AND INVOICE_NO=(SELECT MAX(INVOICE_NO) FROM ariacore.GL WHERE ACCT_NO =" +accountNo+")"
					String proration=database.executeQueryP2(Proration_qry)
					proration_factor=proration.toDouble()
					d.format(proration_factor)
					logi.logInfo "proration_factor "+proration_factor

					//if((daysInvoice % durationOfPlan)<=13){
					/*if((planEndMonth==1 && (palnEndYear%4!=0)) ||(planStartMonth==1 && (palnStartYear%4!=0))){
					 daysInvoice-=1
					 }*/
					Iterator service = value.entrySet().iterator()
					logi.logInfo "Invoice calculation for a plan : "+planNo
					String unitsquery="select recurring_factor from ariacore.all_acct_recurring_factors where ACCT_NO ="+accountNo+" and plan_no="+planNo
					int noofUnits =database.executeQueryP2(planStatusQuery).toInteger()

					logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo

					//Coupon query
					String couponqry = "SELECT COUPON_CD FROM ariacore.ACCT_COUPONS WHERE ACCT_NO="+accountNo
					String coupon_cd=database.executeQueryP2(couponqry)
					logi.logInfo "Coupon : "+ coupon_cd
					String ioflag_qry ="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
					String ioflag_indicator=database.executeQueryP2(ioflag_qry)
					logi.logInfo "ioflag_indicator : "+ ioflag_indicator
					String flat_per_qry = "SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
					String flatper_indicator=database.executeQueryP2(flat_per_qry)
					logi.logInfo "flatper_indicator : "+ flatper_indicator
					String amount_qry = "SELECT NVL(SUM(AMOUNT),0) FROM ariacore.client_discount_rules WHERE client_no = "+clientNo+" and RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
					int cou_amount = Integer.parseInt(database.executeQueryP2(amount_qry))
					logi.logInfo "cou_amount : "+ cou_amount

					while(service.hasNext())
					{
						int flag=0
						double temp = 0
						Map.Entry servicePairs = (Map.Entry) service.next()
						String serviceNo = servicePairs.getKey().toString()
						logi.logInfo "Invoice calculation for a service : "+serviceNo
						String srvqry="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO ="+serviceNo+ "AND CLIENT_NO="+clientNo
						String srvctype=database.executeQueryP2(srvqry)
						logi.logInfo "Service type: "+srvctype
						String flag_qry = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT' AND CLIENT_NO="+clientNo
						String fla_val=database.executeQueryP2(flag_qry)
						logi.logInfo "fla_val : "+fla_val
						if(srvctype == 'AC' && fla_val== 'TRUE')
						{
							flag=1
						}
						else if(srvctype == 'AC' && fla_val== 'FALSE')
						{
							flag=2;
						}
						logi.logInfo "Flag "+flag
						int seqNo=1;
						String tieredPricing = servicePairs.getValue().toString()
						int tieredPricingInt = Integer.parseInt(tieredPricing)
						logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
						String altFee = "NoVal"
						double serviceInvoice
						String query
						switch(tieredPricingInt)
						{
							case 1:
								logi.logInfo "Case 1 "
								if(flag == 2)
								{
									query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
									break;
								}
								else if(flag == 1)
								{
								  if(planNumber != CAC_planNumber)
								  {
									query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
									serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
									logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
									invoice
									double total =(serviceInvoice)
									d.format(total)
									logi.logInfo "total amount " + total
									invoice = invoice + total
									d.format(invoice)
									logi.logInfo "After adding with invoice " + invoice
									seqNo++;
									if(flatper_indicator == 'P')
									{
										temp = total * (cou_amount / 100)
										credit = credit + temp
										d.format(credit)
									}
									else
									{
										temp = cou_amount
										credit= credit + temp
										d.format(credit)
									}
									logi.logInfo "Credit " + credit
									break;
								  }
								  else
								  {
									  logi.logInfo "Both Plans are equal"
									  String old_unit = getValueFromRequest("create_acct_complete","//master_plan_units")
									  logi.logInfo "old_unit  " + old_unit
									  String new_unit = getValueFromRequest(apiname,"//num_plan_units")
									  if(new_unit == 'NoVal')
									  {
										  new_unit = getValueFromRequest("update_master_plan","//num_plan_units")
									  }
									  logi.logInfo "new_unit  " + new_unit
									  String unit_diff = (new_unit).toInteger() - old_unit.toInteger()
									  logi.logInfo "unit_diff  " + unit_diff
									  query = "SELECT NRSR.RATE_PER_UNIT * " + unit_diff.toInteger() + " as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
									  serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
									  logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
									  invoice
									  double total =(serviceInvoice)
									  d.format(total)
									  logi.logInfo "total amount " + total
									  invoice = invoice + total
									  d.format(invoice)
									  logi.logInfo "After adding with invoice " + invoice
									  seqNo++;
									  if(flatper_indicator == 'P')
									  {
										  temp = total * (cou_amount / 100)
										  credit = credit + temp
										  d.format(credit)
									  }
									  else
									  {
										  temp = cou_amount
										  credit= credit + temp
										  d.format(credit)
									  }
									  logi.logInfo "Credit " + credit
									  break;
								  }
								}
								else

									query = "SELECT NRSR.RATE_PER_UNIT * ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no desc"
								serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								double total =(serviceInvoice * proration_factor).round(2)
								d.format(total)
								logi.logInfo "total amount " + total
								invoice = invoice + total
								d.format(invoice)
								logi.logInfo "After adding with invoice " + invoice
								if(flatper_indicator == 'P')
								{
									temp = total * (cou_amount / 100)
									credit = credit + temp
									d.format(credit)
								}
								else
								{
									temp = cou_amount
									credit= credit + temp
									d.format(credit)
								}
								logi.logInfo "Credit " + credit
								seqNo++;
								break;
							case 2 :
								logi.logInfo "Case 2"
								if(!altFee.equals("NoVal"))
								{
									query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
								}
								else
									query ="select rate_per_unit*recurring_factor from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"

								serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								double total = (serviceInvoice * proration_factor).round(2)
								d.format(total)
								logi.logInfo "total amount " + total
								invoice = invoice + total
								d.format(invoice)
								logi.logInfo "After adding with invoice " + invoice
								if(flatper_indicator == 'P')
								{
									temp = total * (cou_amount / 100)
									credit = credit + temp
									d.format(credit)
								}
								else
								{
									temp = cou_amount
									credit= credit + temp
									d.format(credit)
								}
								logi.logInfo "Credit " + credit
								seqNo++;
								break;
								break;
							case 3 :
								logi.logInfo "Case 3"
								query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit) and rownum = 1 order by rate_hist_seq_no desc"
								serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								double total = (serviceInvoice * proration_factor).round(2)
								d.format(total)
								logi.logInfo "total amount " + total
								invoice = invoice + total
								d.format(invoice)
								logi.logInfo "After adding with invoice " + invoice
								if(flatper_indicator == 'P')
								{
									temp = total * (cou_amount / 100)
									credit = credit + temp
									d.format(credit)
								}
								else
								{
									temp = cou_amount
									credit= credit + temp
									d.format(credit)
								}
								logi.logInfo "Credit " + credit
								seqNo++;
								break;
								break;


						}
					}
					//}
					//else
					//	logi.logInfo 'Else loop'

				}
			}

		}

		//String proration_factor_db=db.executeQueryP2("select distinct proration_factor from ariacore.gl_detail where invoice_no="+invoiceNo+" and proration_factor is not null")
		double tinvoice=(invoice.toDouble()).round(2) - credit
		d.format(tinvoice)
		logi.logInfo("Final Invoice amount"+tinvoice)
		//double tinvoice=(invoice*proration_factor.toDouble()).round(2)
		return d.format(tinvoice).toString()
	}

	/**
	 * To find the services and tiered pricing for the master plan
	 * @param testCaseId, account number
	 * @return HashMap of services
	 */
	public HashMap getUpdateMasterPlanServiceAndTieredPricing(String planNo)
	{
		def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select new_plan from ariacore.acct_plan_hist where acct_no = "+acct_no+" and new_plan = "+ planNo +") and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and usage_based = 0) order by plan_no, service_no"
		int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select new_plan from ariacore.acct_plan_hist where acct_no = "+acct_no+" and new_plan = "+ planNo +") and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and usage_based = 0) order by plan_no, service_no"))
		ResultSet resultSet = database.executeQuery(query)

		HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		HashMap<String,String> serviceHash

		String[] plan = new String[count]
		String[] service = new String[count]
		String[] tieredPricing = new String[count]

		int i= 0
		while(resultSet.next()){
			plan[i]=resultSet.getString(1)
			service[i]=resultSet.getString(2)
			tieredPricing[i]=resultSet.getString(3)
			i++
		}
		String plan_no
		int serviceCount=0
		for(int planCount=0;planCount<plan.length;){
			plan_no = plan[planCount]
			serviceHash = new HashMap<String,String> ()
			for(;serviceCount<service.length;serviceCount++){
				if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					planCount ++
				}
				else
					break
			}
			resultHash.put(plan_no, serviceHash)
			logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
		}
		logi.logInfo "Total Services" + resultHash.toString()
		return resultHash
	}

	/**
	 * Calculates invoice amount after assigning supp plan to a account without usage
	 * @param testCaseId
	 * @return invoice amount
	 */
	public String md_ExpInvoice_assign_supp_plan_inclusive(String testCaseId)
	{
		logi.logInfo("calling md_ExpInvoiceWithoutUsage_Amt")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		double usg = 0.0
		double cal_usg
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##");
		String apiname=testCaseId.split("-")[2]
		double totalInvoice=0.0
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no)
		logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		def exp = md_assign_supp_Proration_amount(testCaseId,acct_no)
		logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		def creditamt=db.executeQueryP2("SELECT NVL(SUM(AMOUNT-LEFT_TO_APPLY),0) FROM ARIACORE.CREDITS WHERE ACCT_NO ="+acct_no).toString().toDouble()
		logi.logInfo( "Credit amount " +creditamt)
		def taxamt =db.executeQueryP2("select NVL(sum(debit),0) from ariacore.gl_detail where invoice_no =(select Max(invoice_no) from ariacore.gl where acct_no ="+acct_no +") and service_no in(400,401,402,405,406)").toString().toDouble()
		logi.logInfo( "Tax Amount " +taxamt)
		totalInvoice =  ((exp.toDouble())-(creditamt.toDouble()))
		logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		return d.format(totalInvoice.toDouble()).toString()
	}

	/**
	 * Calculates invoice amount after modifying supp plan to a account with credit and tax reduced
	 * @param testCaseId
	 * @return invoice amount
	 */
	public String md_ExpInvoice_modify_supp_plan(String testCaseId)
	{
		logi.logInfo("calling md_ExpInvoiceWithoutUsage_Amt")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		double usg = 0.0
		double cal_usg
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##");
		String apiname=testCaseId.split("-")[2]
		double totalInvoice=0.0
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no)
		logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		def exp = md_modify_supp_Proration_amount(testCaseId,acct_no)
		logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		def creditamt=db.executeQueryP2("SELECT NVL(SUM(AMOUNT-LEFT_TO_APPLY),0) FROM ARIACORE.CREDITS WHERE ACCT_NO ="+acct_no + "and alt_service_no_2_apply not in (400,401,402,405,406)").toString().toDouble()
		logi.logInfo( "Credit amount " +creditamt)
		def taxamt =db.executeQueryP2("select NVL(sum(debit),0) from ariacore.gl_detail where invoice_no =(select Max(invoice_no) from ariacore.gl where acct_no ="+acct_no +") and service_no in(400,401)").toString().toDouble()
		logi.logInfo( "Tax Maount " +taxamt)
		totalInvoice =  ((exp.toDouble() + taxamt.toDouble())-(creditamt.toDouble()))
		logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		return d.format(totalInvoice.toDouble()).toString()
	}

	/**
	 * Calculates invoice amount after modifying supp plan to a account with credit and tax reduced
	 * @param testCaseId
	 * @return invoice amount
	 */
	public String md_ExpInvoice_modify_supp_plan2(String testCaseId)
	{
		logi.logInfo("calling md_ExpInvoiceWithoutUsage_Amt")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String acct_no= getValueFromResponse('create_acct_complete',"//acct_no")
		double usg = 0.0
		double cal_usg
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##");
		String apiname=testCaseId.split("-")[2]
		double totalInvoice=0.0
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no)
		logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		def exp = md_modify_supp_Proration_amount(testCaseId,acct_no)
		logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		def creditamt=db.executeQueryP2("SELECT NVL(SUM(AMOUNT-LEFT_TO_APPLY),0) FROM ARIACORE.CREDITS WHERE ACCT_NO ="+acct_no + "AND ORIG_COUPON_CD IS NULL and alt_service_no_2_apply not in (400,401,402,405,406)").toString().toDouble()
		logi.logInfo( "Credit amount " +creditamt)
		def taxamt =db.executeQueryP2("select NVL(sum(debit),0) from ariacore.gl_detail where invoice_no =(select Max(invoice_no) from ariacore.gl where acct_no ="+acct_no +") and service_no in(400,401)").toString().toDouble()
		logi.logInfo( "Tax Maount " +taxamt)
		totalInvoice =  ((exp.toDouble() + taxamt.toDouble())-(creditamt.toDouble()))
		logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		return d.format(totalInvoice.toDouble()).toString()
	}

	/**
	 * Calculates invoice amount after modifying supp plan to a account with credit and tax reduced
	 * @param testCaseId
	 * @return invoice amount
	 */
	public String md_ExpInvoice_modify_supp_plan1(String testCaseId)
	{
		logi.logInfo("calling md_ExpInvoiceWithoutUsage_Amt")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String acct_no=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		double usg = 0.0
		double cal_usg
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##");
		String apiname=testCaseId.split("-")[2]
		double totalInvoice=0.0
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no)
		logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		def exp = md_modify_supp_Proration_amount(testCaseId,acct_no)
		logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		def creditamt=db.executeQueryP2("SELECT NVL(SUM(AMOUNT-LEFT_TO_APPLY),0) FROM ARIACORE.CREDITS WHERE ACCT_NO ="+acct_no + "AND ORIG_COUPON_CD IS NULL").toString().toDouble()
		logi.logInfo( "Credit amount " +creditamt)
		def taxamt =db.executeQueryP2("select NVL(sum(debit),0) from ariacore.gl_detail where invoice_no =(select Max(invoice_no) from ariacore.gl where acct_no ="+acct_no +") and service_no in(400,401)").toString().toDouble()
		logi.logInfo( "Tax Maount " +taxamt)
		totalInvoice =  ((exp.toDouble() + taxamt.toDouble())-(creditamt.toDouble()))
		logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		return d.format(totalInvoice.toDouble()).toString()
	}

	/**
	 * Calculates prorated invoice amount after modifying supp plan to a account
	 * @param testCaseId, account number
	 * @return prorated invoice amount
	 */
	public String md_modify_supp_Proration_amount(String testcaseID,String accountNo)
	{
		logi.logInfo("assign_supp")
		Date assigned_date= new Date();
		Date unassigned_date= new Date();
		String planNumber
		double credit = 0
		DecimalFormat d = new DecimalFormat("#.##");
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String apiname=testcaseID.split("-")[2]
		logi.logInfo("apiname name "+ apiname)
		if(apiname == 'assign_supp_plan' || apiname == 'modify_supp_plan')
		{
			planNumber = getValueFromRequest(apiname,"//supp_plan_no")
		}
		logi.logInfo("Plan Number"+ planNumber)
		ConnectDB database = new ConnectDB();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = database.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time : "+ currentVirtualTime
		//int useddays=database.executeQueryP2("SELECT (TRUNC(next_bill_date) -  trunc(plan_date)) AS days FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo).toInteger()
		//int useddays=database.executeQueryP2("SELECT TRUNC(LAST_BILL_THRU_DATE+1) - (select CASE WHEN  (trunc(plan_date) >= trunc(last_recur_bill_date) or last_recur_bill_date is null ) THEN trunc(plan_date) ELSE trunc(last_recur_bill_date)  END 	FROM ariacore.acct WHERE acct_no = "+accountNo+")from ariacore.acct WHERE acct_no = "+accountNo+"  AND client_no ="+clientNo).toInteger()

		String assign_qry="SELECT CREATED FROM ariacore.ACCT_DETAILS WHERE ACCT_NO=" + accountNo
		ResultSet resultSet = database.executeQuery(assign_qry);

		while(resultSet.next())
		{
			assigned_date = resultSet.getDate(1);
		}
		logi.logInfo("assigned_date"+assigned_date)
		String unassign_qry="SELECT CREATE_DATE FROM ARIACORE.ACCT_SUPP_PLAN_MAP_VIEW WHERE ACCT_NO=" + accountNo +" AND SUPP_PLAN_NO= " +planNumber
		ResultSet resultSet1 = database.executeQuery(unassign_qry);
		while(resultSet1.next())
		{
			unassigned_date = resultSet1.getDate(1);
		}
		logi.logInfo("Unassigned_date"+unassigned_date)
		int useddays= getDaysBetween(assigned_date,unassigned_date)+1
		logi.logInfo("Plan useddays"+ useddays)

		Map<String, HashMap<String,String>> hash = getAssignsuppPlanServiceAndTieredPricing(planNumber)

		Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
		resultHash.putAll(hash)

		logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()

		Iterator iterator = resultHash.entrySet().iterator()
		logi.logInfo "Before Key setting "


		def invoice = 0
		double proration_factor
		int cou_amount

		while (iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) iterator.next()
			String planNo = pairs.getKey().toString()
			logi.logInfo "Execution for the plan :  "+ planNo
			String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
			String planStatusQuery, planStatusDataQuery

			int planTypeInd =(database.executeQueryP2(planTypeQuery)).toInteger()

			logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo


			if(planTypeInd==1) {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT_SUPP_PLAN_MAP AC WHERE ACCT_NO   ="+accountNo+" AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date from ariacore.ACCT_SUPP_PLAN_MAP where acct_no = "+ accountNo +"AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo

			}
			else {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+" AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+ " AND CLIENT_NO= " + clientNo

			}

			int planStatus =(database.executeQueryP2(planStatusQuery)).toInteger()
			logi.logInfo "Plan Status : "+planStatus+ " plan : " +planNo
			logi.logInfo "Plan planStatusDataQuery : "+planStatusDataQuery+ " plan : " +planNo


			if(planStatus!=0)
			{
				String statusDateDB = database.executeQueryP2(planStatusDataQuery)
				logi.logInfo "Status date from DB: "+statusDateDB
				Date statusDate = simpleFormat.parse(statusDateDB)

				// Stores the current virtual time in the specified format
				Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
				logi.logInfo "planStatus : "+planStatus+ " plan : " +planNo
				logi.logInfo "statusDate.compareTo(currentVTDate) : "+statusDate.compareTo(currentVTDate)+ " plan : " +planNo


				if((planStatus==1 && statusDate.compareTo(currentVTDate)<=0) || (planTypeInd==0))
				{

					Map<String,String> valueMap = pairs.getValue()
					Map<String,String> value = new TreeMap<String,String> ()
					value.putAll(valueMap)

					int billingInterval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planNo+ "and client_no="+clientNo))
					Date nextbilldate
					ResultSet rs=database.executePlaQuery("SELECT TRUNC(LAST_BILL_THRU_DATE+1) FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo)
					rs.next()
					nextbilldate=rs.getDate(1)
					Date virtualstartdate = subractMonthsFromGivenDate(nextbilldate,billingInterval)

					logi.logInfo "virtualstartdate "+virtualstartdate
					logi.logInfo "nextbilldate " +nextbilldate
					int durationOfPlan = getDaysBetween(virtualstartdate,nextbilldate )
					logi.logInfo "Duration of a plan : "+planNo + " :: "+durationOfPlan
					logi.logInfo "DaysInvoice "+useddays
					logi.logInfo "daysInvoice % durationOfPlan Value : "+ (useddays % durationOfPlan )
					int tmp_days = durationOfPlan-useddays+1
					logi.logInfo "tmp_days "+tmp_days

					//proration_factor=tmp_days/durationOfPlan.toDouble()
					String Proration_qry="SELECT PRORATION_FACTOR FROM ariacore.GL_DETAIL WHERE SEQ_NUM=2 AND INVOICE_NO=(SELECT MAX(INVOICE_NO) FROM ariacore.GL WHERE ACCT_NO =" +accountNo+")"
					String proration=database.executeQueryP2(Proration_qry)
					proration_factor=proration.toDouble()
					d.format(proration_factor)
					logi.logInfo "proration_factor "+proration_factor

					//proration_factor=tmp_days/durationOfPlan.toDouble()
					//logi.logInfo "proration_factor "+proration_factor

					//if((daysInvoice % durationOfPlan)<=13){
					/*if((planEndMonth==1 && (palnEndYear%4!=0)) ||(planStartMonth==1 && (palnStartYear%4!=0))){
					 daysInvoice-=1
					 }*/
					Iterator service = value.entrySet().iterator()
					logi.logInfo "Invoice calculation for a plan : "+planNo
					String unitsquery="select recurring_factor from ariacore.all_acct_recurring_factors where ACCT_NO ="+accountNo+" and plan_no="+planNo
					int noofUnits =database.executeQueryP2(planStatusQuery).toInteger()

					logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo
					String couponqry = "SELECT COUPON_CD FROM ariacore.ACCT_COUPONS WHERE ACCT_NO="+accountNo
					String coupon_cd=database.executeQueryP2(couponqry)
					logi.logInfo "Coupon : "+ coupon_cd
					String ioflag_qry ="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
					String ioflag_indicator=database.executeQueryP2(ioflag_qry)
					logi.logInfo "ioflag_indicator : "+ ioflag_indicator
					String flat_per_qry = "SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
					String flatper_indicator=database.executeQueryP2(flat_per_qry)
					logi.logInfo "flatper_indicator : "+ flatper_indicator
					if(flatper_indicator != null  && ioflag_indicator != null )
					{
						logi.logInfo "Entering into the loop"
						String amount_qry = "SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
						cou_amount = Integer.parseInt(database.executeQueryP2(amount_qry))
						logi.logInfo "cou_amount : "+ cou_amount
					}

					while(service.hasNext())
					{

						int flag=0
						double temp = 0
						int seqNo=1;
						Map.Entry servicePairs = (Map.Entry) service.next()
						String serviceNo = servicePairs.getKey().toString()
						//unit calculation
						String query_rate = "SELECT ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
						def old_unit=(database.executeQueryP2(query_rate)).toInteger()
						logi.logInfo "Old Unit" + old_unit
						def new_unit = getValueFromRequest(apiname,"//num_plan_units")
						logi.logInfo "New Unit" + new_unit
						def act_unit = new_unit.toInteger() - (old_unit)
						logi.logInfo "Activation Unit" + act_unit

						logi.logInfo "Invoice calculation for a service : "+serviceNo
						String srvqry="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO ="+serviceNo+ "AND CLIENT_NO="+clientNo
						String srvctype=database.executeQueryP2(srvqry)
						logi.logInfo "Service type: "+srvctype
						String flag_qry = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT' AND CLIENT_NO="+clientNo
						String fla_val=database.executeQueryP2(flag_qry)
						logi.logInfo "CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT : "+fla_val
						String flag_qry1 = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT' AND CLIENT_NO="+clientNo
						String fla_val1=database.executeQueryP2(flag_qry1)
						logi.logInfo "MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT : "+fla_val
						if((srvctype == 'AC' && fla_val== 'TRUE') && (srvctype == 'AC' && fla_val1 != 'FALSE'))
						{
							flag=1
						}
						else if((srvctype == 'AC' && fla_val== 'FALSE') || (srvctype == 'AC' && fla_val1 == 'FALSE'))
						{
							flag=2;
						}
						else if((srvctype == 'AC' && fla_val== 'TURE') || (srvctype == 'AC' && fla_val1 == 'FALSE'))
						{
							flag=2;
						}
						logi.logInfo "Flag "+flag

						String tieredPricing = servicePairs.getValue().toString()
						int tieredPricingInt = Integer.parseInt(tieredPricing)
						logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
						String altFee = "NoVal"
						def serviceInvoice
						def rate
						String query
						switch(tieredPricingInt)
						{
							case 1:
								logi.logInfo "Case 1 "
								if(flag == 2)
								{
									query = "SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
									break;
								}
								else if(flag == 1)
								{
									query = "SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
									rate = (database.executeQueryP2(query)).toDouble()
									d.format(rate)
									serviceInvoice= rate * act_unit
									d.format(serviceInvoice)
									logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice

									def total =(serviceInvoice)
									d.format(total)
									logi.logInfo "total amount " + total
									invoice = invoice + total
									d.format(invoice)
									logi.logInfo "After adding with invoice " + invoice
									seqNo++;
									if(flatper_indicator == 'P' && total != 0)
									{
										temp = total * (cou_amount / 100)
										d.format(temp)

										credit = credit + temp
										d.format(credit)
									}
									else if (total != 0)
									{
										temp = cou_amount
										d.format(temp)
										credit= credit + temp
										d.format(credit)
									}
									else if(flatper_indicator == 'null'  && ioflag_indicator == 'null')
									{
										credit = 0
									}
									logi.logInfo "Credit " + credit
									break;
								}
								else if(srvctype != 'MN') 
								{

									query = "SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
								rate =(database.executeQueryP2(query)).toDouble()
								serviceInvoice = rate * new_unit.toDouble()
								d.format(serviceInvoice)
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								def total =(serviceInvoice * proration_factor)
								d.format(total)
								logi.logInfo "total amount " + total
								invoice = invoice + total
								d.format(invoice)
								logi.logInfo "After adding with invoice " + invoice
								if(flatper_indicator == 'P' && total != 0)
								{
									temp = total * (cou_amount / 100)
									d.format(temp)
									credit = credit + temp
									d.format(credit)
								}
								else if (total != 0)
								{
									temp = cou_amount
									d.format(temp)
									credit= credit + temp
									d.format(credit)
								}
								else if(flatper_indicator == 'null'  && ioflag_indicator == 'null')
								{
									credit = 0
								}
								}
								logi.logInfo "Credit " + credit
								seqNo++;
								break;
							case 2 :
								logi.logInfo "Case 2"
								if(!altFee.equals("NoVal"))
								{
									query ="select rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
								}
								else
									query ="select rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"

								rate = (database.executeQueryP2(query)).toDouble()
								serviceInvoice = rate * new_unit.toDouble()
								d.format(serviceInvoice)
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								def total = (serviceInvoice * proration_factor)
								d.format(total)
								logi.logInfo "total amount " + total
								invoice = invoice + total
								d.format(invoice)
								logi.logInfo "After adding with invoice " + invoice
								if(flatper_indicator == 'P' && total != 0)
								{
									temp = total * (cou_amount / 100)
									d.format(temp)
									credit = credit + temp
									d.format(credit)
								}
								else if (total != 0)
								{
									temp = cou_amount
									d.format(temp)
									credit= credit + temp
									d.format(credit)
								}
								else if(flatper_indicator == 'null'  && ioflag_indicator == 'null')
								{
									credit = 0
								}
								logi.logInfo "Credit " + credit
								seqNo++;
								break;
								break;
							case 3 :
								logi.logInfo "Case 3"
								query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
								serviceInvoice = (database.executeQueryP2(query)).toDouble()
								d.format(serviceInvoice)
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								def total = (serviceInvoice * proration_factor)
								d.format(total)
								logi.logInfo "total amount " + total
								invoice = invoice + total
								d.format(invoice)
								logi.logInfo "After adding with invoice " + invoice
								if(flatper_indicator == 'P' && total != 0)
								{
									temp = total * (cou_amount / 100)
									d.format(temp)
									credit = credit + temp
									d.format(credit)
								}
								else if (total != 0)
								{
									temp = cou_amount
									d.format(temp)
									credit= credit + temp
									d.format(credit)
								}
								else if(flatper_indicator == 'null'  && ioflag_indicator == 'null')
								{
									credit = 0
								}
								logi.logInfo "Credit " + credit
								seqNo++;
								break;
								break;
						}
					}
					//}
					//else
					//	logi.logInfo 'Else loop'

				}
			}

		}

		//String proration_factor_db=db.executeQueryP2("select distinct proration_factor from ariacore.gl_detail where invoice_no="+invoiceNo+" and proration_factor is not null")
		double tinvoice=(invoice.toDouble()).round(2) - credit
		d.format(tinvoice)
		logi.logInfo("Final Invoice amount"+tinvoice)
		//double tinvoice=(invoice*proration_factor.toDouble()).round(2)
		return d.format(tinvoice).toString()
	}

	/**
	 * Calculates prorated invoice amount after updating an account
	 * @param testCaseId, account number
	 * @return prorated invoice amount
	 */
	public String md_update_acct_complete_Proration_amount(String testcaseID,String accountNo)
	{
		logi.logInfo("assign_supp")
		Date assigned_date= new Date();
		Date unassigned_date= new Date();
		String planNumber
		double credit = 0
		DecimalFormat d = new DecimalFormat("#.##########");
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String apiname=testcaseID.split("-")[2]
		logi.logInfo("apiname name "+ apiname)

		planNumber = getValueFromRequest(apiname,"//master_plan_no")

		logi.logInfo("Plan Number"+ planNumber)
		ConnectDB database = new ConnectDB();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
		String currentVirtualTime = database.executeQueryP2(Query);
		logi.logInfo "Current Virtual Time : "+ currentVirtualTime
		//int useddays=database.executeQueryP2("SELECT (TRUNC(next_bill_date) -  trunc(plan_date)) AS days FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo).toInteger()
		//int useddays=database.executeQueryP2("SELECT TRUNC(LAST_BILL_THRU_DATE+1) - (select CASE WHEN  (trunc(plan_date) >= trunc(last_recur_bill_date) or last_recur_bill_date is null ) THEN trunc(plan_date) ELSE trunc(last_recur_bill_date)  END 	FROM ariacore.acct WHERE acct_no = "+accountNo+")from ariacore.acct WHERE acct_no = "+accountNo+"  AND client_no ="+clientNo).toInteger()

		String assign_qry="SELECT CREATED FROM ariacore.ACCT_DETAILS WHERE ACCT_NO=" + accountNo
		ResultSet resultSet = database.executeQuery(assign_qry);

		while(resultSet.next())
		{
			assigned_date = resultSet.getDate(1);
		}
		logi.logInfo("assigned_date"+assigned_date)
		String unassign_qry="SELECT CREATE_DATE FROM ARIACORE.ACCT_SUPP_PLAN_MAP_VIEW WHERE ACCT_NO=" + accountNo +" AND SUPP_PLAN_NO= " +planNumber
		ResultSet resultSet1 = database.executeQuery(unassign_qry);
		while(resultSet1.next())
		{
			unassigned_date = resultSet1.getDate(1);
		}
		logi.logInfo("Unassigned_date"+unassigned_date)
		int useddays= getDaysBetween(assigned_date,unassigned_date)+1
		logi.logInfo("Plan useddays"+ useddays)

		Map<String, HashMap<String,String>> hash = getUpdateMasterPlanServiceAndTieredPricing(planNumber)

		Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
		resultHash.putAll(hash)

		logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()

		Iterator iterator = resultHash.entrySet().iterator()
		logi.logInfo "Before Key setting "


		def invoice = 0
		double proration_factor
		int cou_amount

		while (iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) iterator.next()
			String planNo = pairs.getKey().toString()
			logi.logInfo "Execution for the plan :  "+ planNo
			String planTypeQuery = "SELECT SUPP_PLAN_IND FROM ARIACORE.client_plan where plan_no= " + planNo + " AND CLIENT_NO= " + clientNo
			String planStatusQuery, planStatusDataQuery

			int planTypeInd =(database.executeQueryP2(planTypeQuery)).toInteger()

			logi.logInfo "Plan planTypeInd : "+planTypeInd+ " plan : " +planNo


			if(planTypeInd==1) {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT_SUPP_PLAN_MAP AC WHERE ACCT_NO   ="+accountNo+" AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date from ariacore.ACCT_SUPP_PLAN_MAP where acct_no = "+ accountNo +"AND SUPP_PLAN_NO= "+ planNo+ " AND CLIENT_NO= " + clientNo

			}
			else {
				planStatusQuery = "SELECT status_cd FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+" AND CLIENT_NO= " + clientNo
				planStatusDataQuery = "SELECT status_date FROM ARIACORE.ACCT WHERE ACCT_NO   ="+accountNo+ " AND CLIENT_NO= " + clientNo

			}

			int planStatus =(database.executeQueryP2(planStatusQuery)).toInteger()
			logi.logInfo "Plan Status : "+planStatus+ " plan : " +planNo
			logi.logInfo "Plan planStatusDataQuery : "+planStatusDataQuery+ " plan : " +planNo


			if(planStatus!=0){
				String statusDateDB = database.executeQueryP2(planStatusDataQuery)
				logi.logInfo "Status date from DB: "+statusDateDB
				Date statusDate = simpleFormat.parse(statusDateDB)

				// Stores the current virtual time in the specified format
				Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
				logi.logInfo "planStatus : "+planStatus+ " plan : " +planNo
				logi.logInfo "statusDate.compareTo(currentVTDate) : "+statusDate.compareTo(currentVTDate)+ " plan : " +planNo


				if((planStatus==1 && statusDate.compareTo(currentVTDate)<=0)|| (planTypeInd==0)){

					Map<String,String> valueMap = pairs.getValue()
					Map<String,String> value = new TreeMap<String,String> ()
					value.putAll(valueMap)

					int billingInterval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ planNo+ "and client_no="+clientNo))
					Date nextbilldate
					ResultSet rs=database.executePlaQuery("SELECT TRUNC(LAST_BILL_THRU_DATE+1) FROM ariacore.acct WHERE acct_no = "+accountNo+" AND client_no ="+clientNo)
					rs.next()
					nextbilldate=rs.getDate(1)
					Date virtualstartdate = subractMonthsFromGivenDate(nextbilldate,billingInterval)

					logi.logInfo "virtualstartdate "+virtualstartdate
					logi.logInfo "nextbilldate " +nextbilldate
					int durationOfPlan = getDaysBetween(virtualstartdate,nextbilldate )
					logi.logInfo "Duration of a plan : "+planNo + " :: "+durationOfPlan
					logi.logInfo "DaysInvoice "+useddays
					logi.logInfo "daysInvoice % durationOfPlan Value : "+ (useddays % durationOfPlan )
					int tmp_days = durationOfPlan-useddays+1
					logi.logInfo "tmp_days "+tmp_days

					//proration_factor=tmp_days/durationOfPlan.toDouble()
					String Proration_qry="SELECT PRORATION_FACTOR FROM ariacore.GL_DETAIL WHERE SEQ_NUM=2 AND INVOICE_NO=(SELECT MAX(INVOICE_NO) FROM ariacore.GL WHERE ACCT_NO =" +accountNo+")"
					String proration=database.executeQueryP2(Proration_qry)
					proration_factor=proration.toDouble()
					logi.logInfo "proration_factor "+proration_factor

					//proration_factor=tmp_days/durationOfPlan.toDouble()
					//logi.logInfo "proration_factor "+proration_factor

					//if((daysInvoice % durationOfPlan)<=13){
					/*if((planEndMonth==1 && (palnEndYear%4!=0)) ||(planStartMonth==1 && (palnStartYear%4!=0))){
					 daysInvoice-=1
					 }*/
					Iterator service = value.entrySet().iterator()
					logi.logInfo "Invoice calculation for a plan : "+planNo
					String unitsquery="select recurring_factor from ariacore.all_acct_recurring_factors where ACCT_NO ="+accountNo+" and plan_no="+planNo
					int noofUnits =database.executeQueryP2(planStatusQuery).toInteger()

					logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo
					String couponqry = "SELECT COUPON_CD FROM ariacore.ACCT_COUPONS WHERE ACCT_NO="+accountNo
					String coupon_cd=database.executeQueryP2(couponqry)
					logi.logInfo "Coupon : "+ coupon_cd
					String ioflag_qry ="SELECT INLINE_OFFSET_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
					String ioflag_indicator=database.executeQueryP2(ioflag_qry)
					logi.logInfo "ioflag_indicator : "+ ioflag_indicator
					String flat_per_qry = "SELECT FLAT_PERCENT_IND FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
					String flatper_indicator=database.executeQueryP2(flat_per_qry)
					logi.logInfo "flatper_indicator : "+ flatper_indicator
					if(flatper_indicator != null  && ioflag_indicator != null )
					{
						logi.logInfo "Entering into the loop"
						String amount_qry = "SELECT AMOUNT FROM ariacore.client_discount_rules WHERE RULE_NO = (select RULE_NO from ariacore.coupon_discount_rule_map where Client_no = "+clientNo+" and Coupon_cd = '"+coupon_cd+"')"
						cou_amount = Integer.parseInt(database.executeQueryP2(amount_qry))
						logi.logInfo "cou_amount : "+ cou_amount
					}

					while(service.hasNext())
					{

						int flag=0
						double temp = 0
						int seqNo=1;
						Map.Entry servicePairs = (Map.Entry) service.next()
						String serviceNo = servicePairs.getKey().toString()
						//unit calculation
						String query_rate = "SELECT ARHD.RECURRING_FACTOR as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
						def old_unit=(database.executeQueryP2(query_rate)).toInteger()
						logi.logInfo "Old Unit" + old_unit
						def new_unit = getValueFromRequest(apiname,"//master_plan_units")
						logi.logInfo "New Unit" + new_unit
						def act_unit = new_unit.toInteger() - (old_unit)
						logi.logInfo "Activation Unit" + act_unit

						logi.logInfo "Invoice calculation for a service : "+serviceNo
						String srvqry="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO ="+serviceNo+ "AND CLIENT_NO="+clientNo
						String srvctype=database.executeQueryP2(srvqry)
						logi.logInfo "Service type: "+srvctype
						String flag_qry = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT' AND CLIENT_NO="+clientNo
						String fla_val=database.executeQueryP2(flag_qry)
						logi.logInfo "fla_val : "+fla_val
						if(srvctype == 'AC' && fla_val== 'TRUE')
						{
							flag=1
						}
						else if(srvctype == 'AC' && fla_val== 'FALSE')
						{
							flag=2;
						}
						logi.logInfo "Flag "+flag

						String tieredPricing = servicePairs.getValue().toString()
						int tieredPricingInt = Integer.parseInt(tieredPricing)
						logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
						String altFee = "NoVal"
						def serviceInvoice
						def rate
						String query
						switch(tieredPricingInt)
						{
							case 1:
								logi.logInfo "Case 1 "
								if(flag == 2)
								{
									query = "SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
									break;
								}
								else if(flag == 1)
								{
									query = "SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
									rate = (database.executeQueryP2(query)).toDouble()
									serviceInvoice= rate * act_unit
									logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice

									def total =(serviceInvoice)
									logi.logInfo "total amount " + total
									invoice = invoice + total
									logi.logInfo "After adding with invoice " + invoice
									seqNo++;
									if(flatper_indicator == 'P' && total != 0)
									{
										temp = total * (cou_amount / 100)
										credit = credit + temp
									}
									else if (total != 0)
									{
										temp = cou_amount
										credit= credit + temp
									}
									else if(flatper_indicator == 'null'  && ioflag_indicator == 'null')
									{
										credit = 0
									}
									logi.logInfo "Credit " + credit
									break;
								}
								else

									query = "SELECT NRSR.RATE_PER_UNIT as Rate_IN_Recurring_Factor FROM ARIACORE.NEW_RATE_SCHED_RATES NRSR JOIN ARIACORE.ACCT_RATES_HISTORY_DETAILS ARHD ON NRSR.SERVICE_NO = ARHD.SERVICE_NO AND ARHD.RATE_SCHEDULE_NO = NRSR.SCHEDULE_NO where ARHD.ACCT_NO  = "+accountNo+"  AND ARHD.PLAN_NO ="+planNo+"  AND ARHD.RATE_SEQ_NO=1 AND ARHD.SERVICE_NO = "+serviceNo+" AND ARHD.CLIENT_NO  ="+clientNo+" AND NRSR.RATE_SEQ_NO = "+seqNo+" AND NRSR.CLIENT_NO   ="+clientNo+"  AND rownum = 1  ORDER BY ARHD.rate_hist_seq_no "
								rate =(database.executeQueryP2(query)).toDouble()
								serviceInvoice = rate * new_unit.toDouble()
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								def total =(serviceInvoice * proration_factor)
								logi.logInfo "total amount " + total
								invoice = invoice + total
								logi.logInfo "After adding with invoice " + invoice
								if(flatper_indicator == 'P' && total != 0)
								{
									temp = total * (cou_amount / 100)
									credit = credit + temp
								}
								else if (total != 0)
								{
									temp = cou_amount
									credit= credit + temp
								}
								else if(flatper_indicator == 'null'  && ioflag_indicator == 'null')
								{
									credit = 0
								}
								logi.logInfo "Credit " + credit
								seqNo++;
								break;
							case 2 :
								logi.logInfo "Case 2"
								if(!altFee.equals("NoVal"))
								{
									query ="select rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
								}
								else
									query ="select rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"

								rate = (database.executeQueryP2(query)).toDouble()
								serviceInvoice = rate * new_unit.toDouble()
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								def total = (serviceInvoice * proration_factor)
								logi.logInfo "total amount " + total
								invoice = invoice + total
								logi.logInfo "After adding with invoice " + invoice
								if(flatper_indicator == 'P' && total != 0)
								{
									temp = total * (cou_amount / 100)
									credit = credit + temp
								}
								else if (total != 0)
								{
									temp = cou_amount
									credit= credit + temp
								}
								else if(flatper_indicator == 'null'  && ioflag_indicator == 'null')
								{
									credit = 0
								}
								logi.logInfo "Credit " + credit
								seqNo++;
								break;
								break;
							case 3 :
								logi.logInfo "Case 3"
								query = "select 1*rate_per_unit from ariacore.acct_rates_history_details where acct_no = "+accountNo+" and client_no = "+clientNo+" and service_no = "+serviceNo+" and plan_no = "+planNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
								serviceInvoice = (database.executeQueryP2(query)).toDouble()
								logi.logInfo "Invoice for Plan : "+planNo +" and Service : "+ serviceNo +" is "+ serviceInvoice
								def total = (serviceInvoice * proration_factor)
								logi.logInfo "total amount " + total
								invoice = invoice + total
								logi.logInfo "After adding with invoice " + invoice
								if(flatper_indicator == 'P' && total != 0)
								{
									temp = total * (cou_amount / 100)
									credit = credit + temp
								}
								else if (total != 0)
								{
									temp = cou_amount
									credit= credit + temp
								}
								else if(flatper_indicator == 'null'  && ioflag_indicator == 'null')
								{
									credit = 0
								}
								logi.logInfo "Credit " + credit
								seqNo++;
								break;
								break;
						}
					}
					//}
					//else
					//	logi.logInfo 'Else loop'

				}
			}

		}

		//String proration_factor_db=db.executeQueryP2("select distinct proration_factor from ariacore.gl_detail where invoice_no="+invoiceNo+" and proration_factor is not null")
		double tinvoice=(invoice.toDouble()).round(2) - credit
		d.format(tinvoice)
		logi.logInfo("Final Invoice amount"+tinvoice)
		//double tinvoice=(invoice*proration_factor.toDouble()).round(2)
		return d.format(tinvoice).toString()
	}

	/**
	 * To find the coupon is applied to the invoice
	 * @param testCaseId
	 * @return true or false
	 */
	public String md_coupon_applied_status(String testCaseId)
	{
		String flag ='FALSE'
		ConnectDB db = new ConnectDB()
		String apiname=testCaseId.split("-")[2]
		String acct_no=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		logi.logInfo("Enter into the coupon applied loop")
		String coupon_cd = getValueFromRequest(apiname,"//coupon_code")
		logi.logInfo("coupon_cd" + coupon_cd)
		String query = "select orig_coupon_cd from ariacore.gl_detail where invoice_no = (select max(invoice_no) from ariacore.gl where acct_no =" + acct_no + ")"
		ResultSet resultSet=db.executePlaQuery(query)
		logi.logInfo "Executed service_no query..."
		while(resultSet.next())
		{
			logi.logInfo ""
			String code= resultSet.getString(1)
			logi.logInfo(" code " + code)
			if(coupon_cd == code)
			{
				flag ='TRUE'
				logi.logInfo("coupon was applied to in the invoice")
				break
			}

		}
		return (flag)
	}

	/**
	 * Calculates invoice amount after updating an account details
	 * @param testCaseId
	 * @return invoice amount
	 */
	public String md_ExpInvoice_update_acct_complete(String testCaseId)
	{
		logi.logInfo("calling md_ExpInvoiceWithoutUsage_Amt")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String acct_no=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		double usg = 0.0
		double cal_usg
		ConnectDB db = new ConnectDB()
		DecimalFormat d = new DecimalFormat("#.##");
		String apiname=testCaseId.split("-")[2]
		double totalInvoice=0.0
		String invoiceNo = getInvoiceNoVT(clientNo, acct_no)
		logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		def exp = md_update_acct_complete_Proration_amount(testCaseId,acct_no)
		logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		def creditamt=db.executeQueryP2("SELECT NVL(SUM(AMOUNT-LEFT_TO_APPLY),0) FROM ARIACORE.CREDITS WHERE ACCT_NO ="+acct_no+ " and alt_service_no_2_apply not in (400,401,402,405,406)").toString().toDouble()
		logi.logInfo( "Credit amount " +creditamt)
		def taxamt =db.executeQueryP2("select NVL(sum(debit),0) from ariacore.gl_detail where invoice_no =(select Max(invoice_no) from ariacore.gl where acct_no ="+acct_no +" and service_no in(400,401))").toString().toDouble()
		logi.logInfo( "Tax Maount " +taxamt)
		totalInvoice =  ((exp.toDouble() + taxamt.toDouble())-(creditamt.toDouble()))
		logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		return d.format(totalInvoice.toDouble()).toString()
		logi.logInfo("totalInvoice in md_ExpInvoiceWithoutUsage_Amt method "+totalInvoice)
		return d.format(totalInvoice).toString()
	}

	/**
	 * Calculates the state tax for the invoice amount
	 * @param testCaseId
	 * @return state tax
	 */
	String md_stateTax_exclusive(String testCaseId)
	{
		logi.logInfo "Inside md_stateTax_exclusive"
		String accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		List plan=[],service=[]
		double actualAmount=0.0
		double ans,stateTax
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		String plan_query="SELECT DISTINCT(PLAN_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND PLAN_NO IS  NOT NULL"
		ResultSet rs=db.executePlaQuery(plan_query)
		logi.logInfo "Executed result set...."
		while(rs.next())
		{
			logi.logInfo" Inside result set of plan....."
			String plan_no=rs.getString(1)
			plan.add(plan_no)
		}
		logi.logInfo "Plan list : "+plan.toString()

		for(int i=0;i<plan.size();i++)
		{
			service.clear()
			String service_query="SELECT service_no from ariacore.gl_detail where invoice_no= "+invoice_no+ "and service_no not in (0,400,401,402,403,405) and plan_no="+plan.get(i).toString()+ " AND DEBIT >0"
			rs=db.executePlaQuery(service_query)
			while(rs.next())
			{
				logi.logInfo "Inside result set..."
				String service_no=rs.getString(1)
				service.add(service_no)
			}
			logi.logInfo "Service list : " +service.toString()

		    for(int j=0;j<service.size();j++)
			{
				String query1="SELECT NVL((PRORATION_FACTOR*BASE_PLAN_UNITS),0) FROM ARIACORE.GL_DETAIL WHERE SERVICE_NO=" +service.get(j).toString()+"AND PLAN_NO= "+plan.get(i).toString()+ "AND INVOICE_NO= "+invoice_no
				ans=(db.executeQueryP2(query1)).toDouble()
				logi.logInfo "Ans : "+ans
				String rate_query="SELECT NVL((rate_per_unit),0) from ARIACORE.NEW_RATE_SCHED_RATES where service_no= " +service.get(j).toString()+ " and SCHEDULE_NO=(SELECT max(RATE_SCHEDULE_NO) FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = " +accountNo+ " AND PLAN_NO= "+plan.get(i).toString()+ " AND SERVICE_NO= " +service.get(i).toString()+ " AND RATE_SEQ_NO=1) AND RATE_SEQ_NO = 1 and client_no= "+clientNo
				double rate=(db.executeQueryP2(rate_query)).toDouble()
				logi.logInfo "Rate : "+rate
				String credit_service_q="select NVL((sum(debit)),0) from ariacore.gl_detail where invoice_no="+invoice_no+ " and service_no in(" +service.get(j).toString()+ ",401) and orig_credit_id is not null"
				String rate1=db.executeQueryP2(credit_service_q)
				if(rate1!="0")
				{
					actualAmount=actualAmount+((ans*rate)+rate1.toDouble())
					logi.logInfo "rate1_credit :" +rate1
					logi.logInfo "Actual amount" +actualAmount
				}
				else
				{
				actualAmount=actualAmount+(ans*rate)
				logi.logInfo "Actual amount" +actualAmount
				}
			}

		}		
		int external_client_ind=db.executeQueryP2("SELECT count(*) FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO="+clientNo)
		String credit_query="SELECT (DEBIT)*-1 FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO=0"
		String credit=db.executeQueryP2(credit_query)
		if(credit==null)
		{
			credit=0
			logi.logInfo "Credit :" +credit
		}
		logi.logInfo "Credit :" +credit
		double totalAmount
		if(external_client_ind!=0)
		{
			totalAmount=actualAmount
			logi.logInfo "External Total amount :" +totalAmount
		}
		else
		{
			totalAmount=actualAmount-credit.toDouble()
			logi.logInfo "Total amount :" +totalAmount
		}
		String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO = 401 order by seq_num desc"
		String seq_num=db.executeQueryP2(seq_query)
		String tax_query="SELECT NVL(SUM(DISTINCT(TAX_RATE)),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SEQ_NUM= "+seq_num
		String tax_rate=db.executeQueryP2(tax_query)
		logi.logInfo "Tax rate :" +tax_rate
		stateTax=(totalAmount*(tax_rate.toDouble()))
		logi.logInfo "stateTax :" +stateTax
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(stateTax).toString()

	}

	/**
	 * Calculates the federal tax for the invoice amount
	 * @param testCaseId
	 * @return federal tax
	 */
	String md_federalTax_exclusive(String testCaseId)
	{
		logi.logInfo "Inside md_stateTax_exclusive"
		String accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		List plan=[],service=[]
		double actualAmount=0.0
		double ans,stateTax
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		String plan_query="SELECT DISTINCT(PLAN_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND PLAN_NO IS  NOT NULL"
		ResultSet rs=db.executePlaQuery(plan_query)
		logi.logInfo "Executed result set...."
		while(rs.next())
		{
			logi.logInfo" Inside result set of plan....."
			String plan_no=rs.getString(1)
			plan.add(plan_no)
		}
		logi.logInfo "Plan list : "+plan.toString()

		for(int i=0;i<plan.size();i++)
		{
			service.clear()
			String service_query="SELECT service_no from ariacore.gl_detail where invoice_no= "+invoice_no+ "and service_no not in (0,400,401,402,403,405) and plan_no="+plan.get(i).toString()
			rs=db.executePlaQuery(service_query)
			while(rs.next())
			{
				logi.logInfo "Inside result set..."
				String service_no=rs.getString(1)
				service.add(service_no)
			}
			logi.logInfo "Service list : " +service.toString()

			for(int j=0;j<service.size();j++)
			{
				String query1="SELECT NVL((PRORATION_FACTOR*BASE_PLAN_UNITS),0) FROM ARIACORE.GL_DETAIL WHERE SERVICE_NO=" +service.get(j).toString()+"AND PLAN_NO= "+plan.get(i).toString()+ "AND INVOICE_NO= "+invoice_no
				ans=(db.executeQueryP2(query1)).toDouble()
				logi.logInfo "Ans : "+ans
				String rate_query="SELECT NVL((rate_per_unit),0) from ARIACORE.NEW_RATE_SCHED_RATES where service_no= " +service.get(j).toString()+ " and SCHEDULE_NO=(SELECT max(RATE_SCHEDULE_NO) FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = " +accountNo+ " AND PLAN_NO= "+plan.get(i).toString()+ " AND SERVICE_NO= " +service.get(i).toString()+ " AND RATE_SEQ_NO=1) AND RATE_SEQ_NO = 1 and client_no= "+clientNo
				double rate=(db.executeQueryP2(rate_query)).toDouble()
				logi.logInfo "Rate : "+rate
				actualAmount=actualAmount+(ans*rate)
				logi.logInfo "Actual amount" +actualAmount
			}

		}
		String credit_query="SELECT (DEBIT)*-1 FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO=0"
		String credit=db.executeQueryP2(credit_query)
		if(credit==null)
		{
			credit=0
			logi.logInfo "Credit :" +credit
		}
		logi.logInfo "Credit :" +credit
		double totalAmount=actualAmount-credit.toDouble()
		logi.logInfo "Total amount :" +totalAmount

		String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO = 400 order by seq_num desc"
		String seq_num=db.executeQueryP2(seq_query)
		String tax_query="SELECT NVL(SUM(DISTINCT(TAX_RATE)),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SEQ_NUM= "+seq_num
		String tax_rate=db.executeQueryP2(tax_query)
		stateTax=actualAmount*(tax_rate.toDouble())
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(stateTax).toString()

	}

	/**
	 * Calculates the City tax for the invoice amount
	 * @param testCaseId
	 * @return City tax
	 */
	String md_cityTax_exclusive(String testCaseId)
	{
		logi.logInfo "Inside md_stateTax_exclusive"
		String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		List plan=[],service=[]
		double actualAmount=0.0
		double ans,stateTax
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		String plan_query="SELECT DISTINCT(PLAN_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND PLAN_NO IS  NOT NULL"
		ResultSet rs=db.executePlaQuery(plan_query)
		logi.logInfo "Executed result set...."
		while(rs.next())
		{
			logi.logInfo" Inside result set of plan....."
			String plan_no=rs.getString(1)
			plan.add(plan_no)
		}
		logi.logInfo "Plan list : "+plan.toString()

		for(int i=0;i<plan.size();i++)
		{
			service.clear()
			String service_query="SELECT service_no from ariacore.gl_detail where invoice_no= "+invoice_no+ "and service_no not in (0,400,401,402,403,405) and plan_no="+plan.get(i).toString()
			rs=db.executePlaQuery(service_query)
			while(rs.next())
			{
				logi.logInfo "Inside result set..."
				String service_no=rs.getString(1)
				service.add(service_no)
			}
			logi.logInfo "Service list : " +service.toString()

			for(int j=0;j<service.size();j++)
			{
				String query1="SELECT NVL((PRORATION_FACTOR*BASE_PLAN_UNITS),0) FROM ARIACORE.GL_DETAIL WHERE SERVICE_NO=" +service.get(j).toString()+"AND PLAN_NO= "+plan.get(i).toString()+ "AND INVOICE_NO= "+invoice_no
				ans=(db.executeQueryP2(query1)).toDouble()
				logi.logInfo "Ans : "+ans
				String rate_query="SELECT NVL((rate_per_unit),0) from ARIACORE.NEW_RATE_SCHED_RATES where service_no= " +service.get(j).toString()+ " and SCHEDULE_NO=(SELECT max(RATE_SCHEDULE_NO) FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = " +accountNo+ " AND PLAN_NO= "+plan.get(i).toString()+ " AND SERVICE_NO= " +service.get(i).toString()+ " AND RATE_SEQ_NO=1) AND RATE_SEQ_NO = 1 and client_no= "+clientNo
				double rate=(db.executeQueryP2(rate_query)).toDouble()
				logi.logInfo "Rate : "+rate
				String credit_service_q="select NVL((debit),0) from ariacore.gl_detail where invoice_no="+invoice_no+ " and service_no in(" +service.get(j).toString()+ ",403) and orig_credit_id is not null"
				String rate1=db.executeQueryP2(credit_service_q)
				if(rate1!=null)
				{
					actualAmount=actualAmount+((ans*rate)+rate1.toDouble())
					logi.logInfo "rate1_credit :" +rate1
					logi.logInfo "Actual amount" +actualAmount
				}
				else
				{
				actualAmount=actualAmount+(ans*rate)
				logi.logInfo "Actual amount" +actualAmount
				}
			}

		}		
		int external_client_ind=db.executeQueryP2("SELECT count(*) FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO="+clientNo)
		String credit_query="SELECT (DEBIT)*-1 FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO=0"
		String credit=db.executeQueryP2(credit_query)
		if(credit==null)
		{
			credit=0
			logi.logInfo "Credit :" +credit
		}
		logi.logInfo "Credit :" +credit
		double totalAmount
		if(external_client_ind!=0)
		{
			totalAmount=actualAmount
			logi.logInfo "External Total amount :" +totalAmount
		}
		else
		{
			totalAmount=actualAmount-credit.toDouble()
			logi.logInfo "Total amount :" +totalAmount
		}
		String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO = 403 order by seq_num desc"
		String seq_num=db.executeQueryP2(seq_query)
		String tax_query="SELECT NVL(SUM(DISTINCT(TAX_RATE)),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SEQ_NUM= "+seq_num
		String tax_rate=db.executeQueryP2(tax_query)
		stateTax=totalAmount*(tax_rate.toDouble())
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(stateTax).toString()

	}

	/**
	 * Calculates the Country tax for the invoice amount
	 * @param testCaseId
	 * @return Country tax
	 */
	String md_countyTax_exclusive(String testCaseId)
	{
		logi.logInfo "Inside md_stateTax_exclusive"
		String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		List plan=[],service=[]
		double actualAmount=0.0
		double ans,stateTax
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		String plan_query="SELECT DISTINCT(PLAN_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND PLAN_NO IS  NOT NULL"
		ResultSet rs=db.executePlaQuery(plan_query)
		logi.logInfo "Executed result set...."
		while(rs.next())
		{
			logi.logInfo" Inside result set of plan....."
			String plan_no=rs.getString(1)
			plan.add(plan_no)
		}
		logi.logInfo "Plan list : "+plan.toString()

		for(int i=0;i<plan.size();i++)
		{
			service.clear()
			String service_query="SELECT service_no from ariacore.gl_detail where invoice_no= "+invoice_no+ "and service_no not in (0,400,401,402,403,405) and plan_no="+plan.get(i).toString()+"AND DEBIT > 0"
			rs=db.executePlaQuery(service_query)
			while(rs.next())
			{
				logi.logInfo "Inside result set..."
				String service_no=rs.getString(1)
				service.add(service_no)
			}
			logi.logInfo "Service list : " +service.toString()

			for(int j=0;j<service.size();j++)
			{
				String query1="SELECT NVL((PRORATION_FACTOR*BASE_PLAN_UNITS),0) FROM ARIACORE.GL_DETAIL WHERE SERVICE_NO=" +service.get(j).toString()+"AND PLAN_NO= "+plan.get(i).toString()+ "AND INVOICE_NO= "+invoice_no
				ans=(db.executeQueryP2(query1)).toDouble()
				logi.logInfo "Ans : "+ans
				String rate_query="SELECT NVL((rate_per_unit),0) from ARIACORE.NEW_RATE_SCHED_RATES where service_no= " +service.get(j).toString()+ " and SCHEDULE_NO=(SELECT max(RATE_SCHEDULE_NO) FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = " +accountNo+ " AND PLAN_NO= "+plan.get(i).toString()+ " AND SERVICE_NO= " +service.get(i).toString()+ " AND RATE_SEQ_NO=1) AND RATE_SEQ_NO = 1 and client_no= "+clientNo
				double rate=(db.executeQueryP2(rate_query)).toDouble()
				logi.logInfo "Rate : "+rate
				String credit_service_q="select NVL((debit),0) from ariacore.gl_detail where invoice_no="+invoice_no+ " and service_no in(" +service.get(j).toString()+ ",402) and orig_credit_id is not null"
				String rate1=db.executeQueryP2(credit_service_q)
				if(rate1!=null)
				{
					actualAmount=actualAmount+((ans*rate)+rate1.toDouble())
					logi.logInfo "rate1_credit :" +rate1
					logi.logInfo "Actual amount" +actualAmount
				}
				else
				{
				actualAmount=actualAmount+(ans*rate)
				logi.logInfo "Actual amount" +actualAmount
				}
			}

		}		
		int external_client_ind=db.executeQueryP2("SELECT count(*) FROM ARIACORE.CLIENT_TAX_CONFIG_SET WHERE CLIENT_NO="+clientNo)
		String credit_query="SELECT (DEBIT)*-1 FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO=0"
		String credit=db.executeQueryP2(credit_query)
		if(credit==null)
		{
			credit=0
			logi.logInfo "Credit :" +credit
		}
		logi.logInfo "Credit :" +credit
		double totalAmount
		if(external_client_ind!=0)
		{
			totalAmount=actualAmount
			logi.logInfo "External Total amount :" +totalAmount
		}
		else
		{
			totalAmount=actualAmount-credit.toDouble()
			logi.logInfo "Total amount :" +totalAmount
		}
		String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO = 402 order by seq_num desc"
		String seq_num=db.executeQueryP2(seq_query)
		String tax_query="SELECT NVL(SUM(DISTINCT(TAX_RATE)),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SEQ_NUM= "+seq_num
		String tax_rate=db.executeQueryP2(tax_query)
		stateTax=totalAmount*(tax_rate.toDouble())
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(stateTax).toString()

	}

	/**
	 * Calculates the state tax for the invoice amount
	 * @param testCaseId
	 * @return state tax
	 */
	String md_stateTax_ex(String testCaseId)
	{
		logi.logInfo "Inside md_stateTax_exclusive"
		String accountNo= getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO = 401 order by seq_num desc"
		String seq_num=db.executeQueryP2(seq_query)
		String debit_query="SELECT SUM(DEBIT) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SEQ_NUM= " +seq_num
		String debit = db.executeQueryP2(debit_query)
		debit== null ? "No Data":debit
	}

	/**
	 * Calculates the Federal tax for the invoice amount
	 * @param testCaseId
	 * @return Federal tax
	 */
	String md_federalTax_ex(String testCaseId)
	{
		logi.logInfo "Inside md_stateTax_exclusive"
		String accountNo= getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO = 400 order by seq_num desc"
		String seq_num=db.executeQueryP2(seq_query)
		String debit_query="SELECT SUM(DEBIT) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SEQ_NUM= " +seq_num
		String debit = db.executeQueryP2(debit_query)
		debit== null ? "No Data":debit
	}

	/**
	 * Calculates the Country tax for the invoice amount
	 * @param testCaseId
	 * @return Country tax
	 */
	String md_countyTax_ex(String testCaseId)
	{
		logi.logInfo "Inside md_stateTax_exclusive"
		String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO = 402 order by seq_num desc"
		String seq_num=db.executeQueryP2(seq_query)
		String debit_query="SELECT SUM(DEBIT) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SEQ_NUM= " +seq_num
		String debit = db.executeQueryP2(debit_query)
		debit== null ? "No Data":debit
	}

	/**
	 * Verifies the account's bill thru date 
	 * @param testCaseId
	 * @return true of false
	 */	 
	//To verify the acct bill thru date depends on the flag "SYNC_MSTR_BILL_DATES_ON_1ST_SUPP"
	def md_verifySyncMasterBillThruDateOn1stSuppPlan(String testCaseId)
	{
		logi.logInfo("Calling md_verifySyncMasterBillThruDateOn1stSuppPlan")
		String splanNo,syncFlag
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def mplanNo = getValueFromRequest('create_acct_complete',"//master_plan_no")
		 splanNo = getValueFromRequest('create_order_with_plans',"//supp_plan_no")
		 syncFlag = getValueFromRequest('create_order_with_plans',"//sync_mstr_bill_dates_override")
		if(splanNo=="NoVal")
		{
			splanNo=getValueFromRequest('assign_supp_plan',"//supp_plan_no")
			syncFlag = getValueFromRequest('assign_supp_plan',"//sync_mstr_bill_dates_override")
		}
		ConnectDB db = new ConnectDB()
		String created_query="SELECT CREATED FROM ARIACORE.ACCT WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		// String created_query="SELECT TO_CHAR (ARIACORE.ARIAVIRTUALTIME("+clientNo+"), 'YYYY-MM-DD')  FROM DUAL"
		//String created_query="SELECT SUBSTR(comments,19,10) FROM ARIACORE.ACCT WHERE ACCT_NO="+accountNo+" and client_no="+clientNo
		def acctCreatedDate=db.executeQueryP2(created_query)
		logi.logInfo "acctCreatedDate is " + acctCreatedDate
		String billingInterval_query="select billing_interval from ariacore.client_plan where plan_no="+mplanNo+" and client_no="+clientNo
		def billingInterval=db.executeQueryP2(billingInterval_query)
		logi.logInfo " billingInterval is " + billingInterval
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
		acctCreatedDate=formattedDate(acctCreatedDate)
		Date date = dateFormat.parse(acctCreatedDate);
		Calendar now = Calendar.getInstance();
		Calendar now1 = Calendar.getInstance();
		Calendar now2 = Calendar.getInstance();
		now.setTime(date)
		now.add(Calendar.MONTH,billingInterval.toInteger())
		now.add(Calendar.DATE,-1)
		//SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-YYYY");
		String acctBillThruDate = dateFormat.format(now.getTime())
		logi.logInfo "acctBillThruDate formatting " +acctBillThruDate

		String vt_qry="SELECT TO_CHAR(ARIACORE.ARIAVIRTUALTIME("+clientNo+"), 'YYYY-MM-DD')  FROM DUAL"
		String currentVt=db.executeQueryP2(vt_qry)
		def currentVt1=formattedDate(currentVt)
		logi.logInfo "currentVt1 formatting " +currentVt1
		String sbillingInterval_query="select billing_interval from ariacore.client_plan where plan_no="+splanNo+" and client_no="+clientNo
		def sbillingInterval=db.executeQueryP2(sbillingInterval_query)
		Date date1 = dateFormat.parse(currentVt1);
		now1.setTime(date1)
		now1.add(Calendar.MONTH,sbillingInterval.toInteger())
		now1.add(Calendar.DATE,-1)
		String suppPlanBillThruDate = dateFormat.format(now1.getTime())
		logi.logInfo "suppPlanBillThruDate formatting " +suppPlanBillThruDate

		int updatedVTDays= getDaysBetween(date,date1)
		logi.logInfo("updatedVTDays days:"+updatedVTDays)

		String verifyFlagQuery="select param_val from ariacore.aria_client_params where param_name = 'SYNC_MSTR_BILL_DATES_ON_1ST_SUPP' and client_no="+clientNo
		String verifyFlag=db.executeQueryP2(verifyFlagQuery)
		String finalBillThruDate
		if (verifyFlag=="1" && syncFlag=="1")
			finalBillThruDate=acctBillThruDate
		else
		{
			logi.logInfo("Entering into the else loop")
			Date date2 = dateFormat.parse(acctBillThruDate);
			now2.setTime(date2)
			int days = now2.getActualMaximum(Calendar.DAY_OF_MONTH)
			int check = now2.get(Calendar.DATE)
			logi.logInfo("Total Number days:"+days) //30
			logi.logInfo("Total Number date:"+check)  //6
			if(days == 31 )
			{
				now2.add(Calendar.DATE,updatedVTDays)
			}
			else if((days == 31 && check > 10) || (days == 30))
			{
				//now2.add(Calendar.DATE,updatedVTDays,-1)
				now2.add(Calendar.DATE,-1)
			}
			finalBillThruDate = dateFormat.format(now2.getTime())
		}
		logi.logInfo("finalBillThruDate days:"+finalBillThruDate)
		logi.logInfo("suppPlanBillThruDate days:"+suppPlanBillThruDate)


		if (finalBillThruDate==suppPlanBillThruDate)
			return "TRUE"
		else
			return "FALSE"

	}

	/**
	 * Calculates invoice amount
	 * @param testCaseId
	 * @return Invoice amount
	 */
	String md_invoice_total_amount(String testCaseId)
	{
		logi.logInfo "Inside md_invoice_total_amount"
		String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		List plan=[],service=[],seqNum=[]
		double actualAmount=0.0,taxAmount=0.0,invoiceAmount=0.0
		double ans,stateTax
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		String plan_query="SELECT DISTINCT(PLAN_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND PLAN_NO IS  NOT NULL"
		ResultSet rs=db.executePlaQuery(plan_query)
		logi.logInfo "Executed result set...."
		while(rs.next())
		{
			logi.logInfo" Inside result set of plan....."
			String plan_no=rs.getString(1)
			plan.add(plan_no)
		}
		logi.logInfo "Plan list : "+plan.toString()

		for(int i=0;i<plan.size();i++)
		{
			service.clear()
			String service_query="SELECT service_no from ariacore.gl_detail where invoice_no= "+invoice_no+ "and service_no not in (0,400,401,402,403,405) and plan_no="+plan.get(i).toString()+ " AND DEBIT >0"
			rs=db.executePlaQuery(service_query)
			while(rs.next())
			{
				logi.logInfo "Inside result set..."
				String service_no=rs.getString(1)
				service.add(service_no)
			}
			logi.logInfo "Service list : " +service.toString()
			logi.logInfo "Service listi :"+i+"service" +service.toString()

			for(int j=0;j<service.size();j++)
			{
				String query1="SELECT NVL((PRORATION_FACTOR*BASE_PLAN_UNITS),0) FROM ARIACORE.GL_DETAIL WHERE SERVICE_NO=" +service.get(j).toString()+"AND PLAN_NO= "+plan.get(i).toString()+ "AND INVOICE_NO= "+invoice_no
				ans=(db.executeQueryP2(query1)).toDouble()
				logi.logInfo "Ans : "+ans
				logi.logInfo "In.........."+service.get(j).toString() +" " +plan.get(i).toString()
				String ser=service.get(j).toString()
				logi.logInfo "Service_no :" +ser
				String plannn=plan.get(i).toString()
				logi.logInfo"Plan_no : " +plannn
				String rate_query="SELECT NVL(sum(rate_per_unit),0) from ARIACORE.NEW_RATE_SCHED_RATES where service_no= " +ser+ " and SCHEDULE_NO=(SELECT max(RATE_SCHEDULE_NO) FROM ARIACORE.ACCT_RATES_HISTORY_DETAILS WHERE ACCT_NO = " +accountNo+ " AND PLAN_NO= "+plannn+ " AND SERVICE_NO= " +ser+ " AND RATE_SEQ_NO=1) AND RATE_SEQ_NO = 1 and client_no= "+clientNo
				logi.logInfo "The query is : "+rate_query
				logi.logInfo "Query......."
				double rate=(db.executeQueryP2(rate_query)).toDouble()
				logi.logInfo "Done..........."
				logi.logInfo "Rate : "+rate
				actualAmount=actualAmount+(ans*rate)
				logi.logInfo "Actual amount" +actualAmount
			}

		}
		String credit_query="SELECT (DEBIT)*-1 FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ "AND SERVICE_NO=0"
		String credit=db.executeQueryP2(credit_query)
		if(credit==null)
		{
			credit=0
			logi.logInfo "Credit :" +credit
		}
		logi.logInfo "Credit :" +credit
		double totalAmount=actualAmount-credit.toDouble()
		logi.logInfo "Total amount :" +totalAmount
		String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO IN (400,401,402,403,405) ORDER BY SEQ_NUM DESC"
		rs=db.executePlaQuery(seq_query)
		while(rs.next())
		{
			logi.logInfo" Inside result set of SEQ_NUM....."
			String seq_no=rs.getString(1)
			seqNum.add(seq_no)
		}

		for(int k=0;k<seqNum.size();k++)
		{
			String tax_query="SELECT SUM(DEBIT) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND SEQ_NUM= "+seqNum.get(k).toString()
			taxAmount=taxAmount+(db.executeQueryP2(tax_query)).toDouble()
			logi.logInfo "Tax amount :" +taxAmount
		}
		invoiceAmount=totalAmount+taxAmount
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(invoiceAmount).toString()

	}

	String md_stateTax_Actual(String testCaseId)
	{
		String stateTax=md_formatTax(401)
		return stateTax
	}

	String md_federalTax_Actual(String testCaseId)
	{
		String stateTax=md_formatTax(400)
		return stateTax
	}

	String md_countyTax_Actual(String testCaseId)
	{
		String stateTax=md_formatTax(402)
		return stateTax
	}

	/**
	 * Format the tax amount to a round off value 
	 * @param service number
	 * @return formatted debit
	 */
	String md_formatTax(String service_no)
	{
		String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String taxQuery="SELECT max(DEBIT) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = (SELECT max(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+accountNo+ ") and SERVICE_NO= "+service_no
		Double tax=db.executeQueryP2(taxQuery).toDouble()
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(tax).toString
	}

	/**
	 * Calculates invoice amount with multiple credits applied 
	 * @param testCaseId
	 * @return invoice amount
	 */
	String md_ExpectedInvoiceWithMultipleInvoiceServiceCredit_CR0(String testCaseId){
		logi.logInfo "Inside md_ExpectedInvoiceWithMultipleInvoiceServiceCredit_CR0"
		String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		// Calculating service, federal and state tax amount
		double serviceInvoiceAmount = md_serviceInvoiceAmount(testCaseId).toDouble()
		logi.logInfo("Service Invoice is : " + serviceInvoiceAmount)
		ConnectDB dataConnect = new ConnectDB()
		String federalTaxRate = dataConnect.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =400 and client_no =  " + clientNo).toString()
		logi.logInfo("Federal tax rate : " + federalTaxRate)
		String stateTaxRate = dataConnect.executeQueryP2("SELECT rate from ARIACORE.TAX_CALC_VALUE where tax_service_no =401 and client_no =  " + clientNo).toString()
		logi.logInfo("State tax rate : " + stateTaxRate)
		double federal_Tax = (serviceInvoiceAmount.toDouble()) * (federalTaxRate.toDouble())
		logi.logInfo("federal tax : " + federal_Tax)
		double state_Tax =  (serviceInvoiceAmount.toDouble()) * (stateTaxRate.toDouble())
		logi.logInfo("state tax : " + state_Tax)
		// Calculating deduction amount
		String credit_id = dataConnect.executeQueryP2("SELECT MAX(CREDIT_ID) from ariacore.all_credits where acct_no=" + accountNo + " and orig_recurring_credit_no=(select orig_recurring_credit_no from ariacore.all_recur_credit WHERE ACCT_NO=" + accountNo + ")")
		logi.logInfo("Credit ID : " + credit_id)
		String creditAmount = dataConnect.executeQueryP2("SELECT AMOUNT from ariacore.all_credits where credit_id = " + credit_id)
		logi.logInfo("credit amount : " + creditAmount)
		String activation_flg = dataConnect.executeQueryP2("select count(service_no) from ariacore.gl_detail where invoice_no=(SELECT max(invoice_no) from ariacore.gl where acct_no = " + accountNo + ") and comments like '%ACTIVATION%'")
		logi.logInfo("Activation flag set : " + activation_flg)
		double activation_amt = 0
		if(activation_flg == "1") {
			activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
			logi.logInfo "Activation amount : " + (activation_amt)/2
			activation_amt = activation_amt/2
		}
		else if(activation_flg != "0" && activation_flg != "1") {
			activation_amt = md_serviceInvoiceActivationAmount(testCaseId).toDouble()
			logi.logInfo "Activation amount : " + activation_amt
		}
		// Subtracting credit amount from the amount calculated(service+federal+state+usage amount) to get current invoice amount
		logi.logInfo 'Total active plans invoice amount : ' + serviceInvoiceAmount + " CurrentFederalTax : "+federal_Tax + " CurrentStateTax : "+ state_Tax + " CreditAmount : "+ creditAmount + "Activation Amount : " + activation_amt
		double totalInvoice = serviceInvoiceAmount.toDouble() + federal_Tax + state_Tax  - creditAmount.toDouble() + activation_amt
		logi.logInfo 'Total Invoice : ' + totalInvoice
		String tot_inv = df.format(totalInvoice).toString()
		if(tot_inv.contains(",")){
			tot_inv = tot_inv.replaceAll(",", "")
		}
		return tot_inv
	}
	
   def md_invc_end_date(String testCaseId)
   {
	   logi.logInfo "Entering into the method md_invc_end_date "
	   String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	   ConnectDB database = new ConnectDB();
	   String plan_no
	   SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
	   String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	   logi.logInfo "account_no" +accountNo
	   plan_no = getValueFromRequest('create_acct_complete',"//master_plan_no")
	   logi.logInfo "plan_no : "+plan_no
	   int billingInterval = Integer.parseInt(database.executeQueryP2("select billing_interval from ariacore.client_plan where plan_no = "+ plan_no+ "and client_no="+clientNo))
	   logi.logInfo "billingInterval : "+billingInterval
	   Date nextbilldate
	   String query=database.executeQueryP2("select to_char(last_bill_date,'yyyy-MM-dd') from ariacore.acct where acct_no=" + accountNo)
	  
	   nextbilldate=simpleFormat.parse(query)
	   logi.logInfo "nextbilldate : "+nextbilldate
	   Date virtualstartdate = addMonthsFromGivenDate(nextbilldate,billingInterval)
	
	   logi.logInfo "virtualstartdate : "+virtualstartdate
	   
	   String next_bill = simpleFormat.format(virtualstartdate-1)
	   return next_bill
	   
   }
   
   /**
	* To verify the error msg if we pass the invalid date format in record_standing_order first bill date field
	* @param testCaseId
	* @return error msg with invalid first bill date format
	*/
   String md_verifyBug3495(String testCaseId)
   {
	  String firstBillDate = getValueFromRequest('record_standing_order.a',"//*[1]/*[local-name()='first_bill_date']")
	  String result = "Malformed first_bill_date \""+firstBillDate+"\" supplied"
	  return result
   } 
   
   def md_verifyCreateOrderInvoiceDetailsmultiinlineFromDB(String tcid)
   {
	   logi.logInfo("md_verifyCreateOrderInvoiceDetailsFromDB")
	   String apiname=tcid.split("-")[2]
	   logi.logInfo("API Name:"+apiname)
	   DecimalFormat df = new DecimalFormat("#.##");
	   String client_no=Constant.mycontext.expand('${Properties#Client_No}')
	   logi.logInfo "client QUERY"+client_no
	   def acct_no=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	   logi.logInfo "acct QUERY"+acct_no
	   logi.logInfo "BEFORE QUERY"
	   ConnectDB db = new ConnectDB()
	   def order_no =getValueFromResponse('create_order_with_plans',ExPathRpcEnc.CREATE_ORDER_WITH_PLANS_ORDERNO1)
	   String order_amt = " select amount FROM ariacore.orders WHERE order_no= " +order_no + "AND client_no =" +client_no
	   String queryamt = db.executeQueryP2(order_amt).toDouble()
	   logi.logInfo "Orderno"+queryamt
	   List coupon_list = []
	  
	   String Coupon_Query =  "SELECT coupon_cd FROM ariacore.ACCT_COUPONS WHERE ACCT_NO = "+ acct_no
	   ResultSet rs1=db.executePlaQuery(Coupon_Query)
	   logi.logInfo "Executed result set"
	  
	   while(rs1.next())
	   {
			  logi.logInfo" Inside result set of Service number"
			  String coupon_name=rs1.getString(1)
			  coupon_list.add(coupon_name)
	   }
	   logi.logInfo "CouponList"+coupon_list.toString()
	   
	   def total_amt = queryamt
	   def total_amt1
	   String amt
	   int i = coupon_list.size ;
	   logi.logInfo "Size" + i
	   for(int k=0;k<coupon_list.size;k++)  
	   {
		logi.logInfo "count"+k
		if ( i == 2)
		{
			String InlineFlat  = " SELECT amount from ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='F' AND INLINE_OFFSET_IND ='I'  AND RULE_NO = (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no = "+client_no +"	AND coupon_cd   ='"+coupon_list.get(k).toString()+ "')"  
			amt = db.executeQueryP2(InlineFlat).toDouble()
			logi.logInfo "List flat"+amt
			total_amt1 = total_amt.toDouble() - amt.toDouble()
			logi.logInfo "flat loop"+total_amt1
			i--;
		}
		else if(i==1)
		{
			String InlinePer  = "select (AMOUNT/100) from ariacore.client_discount_rules WHERE FLAT_PERCENT_IND='P' AND INLINE_OFFSET_IND ='I' AND RULE_NO = (SELECT RULE_NO FROM ariacore.coupon_discount_rule_map WHERE Client_no ="+client_no +" AND coupon_cd ='"+coupon_list.get(k).toString()+ "' )" 
			amt = db.executeQueryP2(InlinePer).toDouble()
			logi.logInfo "List per"+amt
			def percal = total_amt1.toDouble() * amt.toDouble()
			total_amt1 = total_amt1.toDouble() - percal.toDouble()
			logi.logInfo "per loop"+total_amt1
		}
		logi.logInfo "before cal List"+total_amt1
		//total_amt1 = (total_amt) - (amt)
		logi.logInfo "after cal List"+df.format(total_amt1).toString()
		
	   }
	   logi.logInfo "order amount" +df.format(total_amt1).toString()
	   return df.format(total_amt1).toString()
 
   }
   public HashMap getAssignsuppPlanServiceAndTieredPricing3(String[] planNo)
   {
	   def acct_no = getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	   String client_no=Constant.mycontext.expand('${Properties#Client_No}')
	   logi.logInfo "getAssignsuppPlanServiceAndTieredPricing3" + planNo.size()
	   ConnectDB database = new ConnectDB();
	   HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
	   for(int planitr=0;planitr< planNo.size();planitr++)
	   {
	   String query = "select plan_no, service_no, TIERED_PRICING_RULE_NO from ariacore.plan_services where plan_no in (select supp_plan_no from ariacore.acct_supp_plan_map_view where acct_no = "+acct_no+" and supp_plan_no = "+ planNo[planitr] +") and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and usage_based = 0) order by plan_no, service_no"
	   int count = Integer.parseInt(database.executeQueryP2("select count(plan_no) from ariacore.plan_services where plan_no in (select supp_plan_no from ariacore.acct_supp_plan_map_view where acct_no = "+acct_no+" and supp_plan_no = "+ planNo[planitr] +") and client_no = "+client_no+" and service_no in (select service_no from ariacore.services where custom_to_client_no = "+client_no+" and usage_based = 0) order by plan_no, service_no"))
	   ResultSet resultSet = database.executeQuery(query)

	  
	   HashMap<String,String> serviceHash

	   String[] plan = new String[count]
	   String[] service = new String[count]
	   String[] tieredPricing = new String[count]

	   int i= 0
	   while(resultSet.next()){
		   plan[i]=resultSet.getString(1)
		   service[i]=resultSet.getString(2)
		   tieredPricing[i]=resultSet.getString(3)
		   i++
	   }
	   String plan_no
	   int serviceCount=0
	   for(int planCount=0;planCount<plan.length;){
		   plan_no = plan[planCount]
		   serviceHash = new HashMap<String,String> ()
		   for(;serviceCount<service.length;serviceCount++){
			   if(plan_no.equalsIgnoreCase(plan[serviceCount])){
				   serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
				   planCount ++
			   }
			   else
				   break
		   }
		   resultHash.put(plan_no, serviceHash)
		   logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
	   }
	   logi.logInfo "Total Services" + resultHash.toString()
	   }
	   return resultHash
	   
	   
   }
   
   String md_custom_label(String testCaseId)
   {
	   logi.logInfo("md_custom_label")
	   String flag='Not null'
	   def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	   String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
	   //String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
	   ConnectDB db = new ConnectDB()
	   //String invoice_no=db.executeQueryP2(invoiceQuery)
	   String cust_query="SELECT CUSTOM_STATUS_LABEL FROM ARIACORE.GL WHERE ACCT_NO= "+accountNo
	   String cust_label=db.executeQueryP2(cust_query)
	   if(cust_label==null)
	   flag='No Entry in Table'
	   return flag
   }
   
   String md_client_notes(String testCaseId)
   {
	   logi.logInfo("md_client_notes")
	   String flag='Not null'
	   def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
	   String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
	   //String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
	   ConnectDB db = new ConnectDB()
	   //String invoice_no=db.executeQueryP2(invoiceQuery)
	   String cust_query="SELECT  CLIENT_NOTES FROM ARIACORE.GL WHERE ACCT_NO= "+accountNo
	   String cust_label=db.executeQueryP2(cust_query)
	   if(cust_label==null)
	   flag='No Entry in Table'
	   return flag
   }
   
   def md_get_invoice_details_api(String testcaseId)
	{
		
			logi.logInfo("Inside method md_get_invoice_details_api")
			// Getting the account number and client id
			HashMap row =new HashMap()
			def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			
			if(accountNo =='NoVal')
			{
				   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
			}
			
			DecimalFormat df = new DecimalFormat("#.##");
			
			String planNo  = getValueFromResponse('get_invoice_details',ExPathRpcEnc.GET_INVOICE_DETAILS_PLAN_NO3)
			logi.logInfo("Plan No => "+planNo)
			row.put("Plan No",planNo.toString())
			
			String planName  = getValueFromResponse('get_invoice_details',ExPathRpcEnc.GET_INVOICE_DETAILS_PLAN_NAME1)
			logi.logInfo("Plan Name => "+planName)
			row.put("Plan Name",planName.toString())
				
			String serviceNo  = getValueFromResponse('get_invoice_details',ExPathRpcEnc.GET_INVOICE_DETAILS_SERVICENO1)
			logi.logInfo("Service No => "+serviceNo)
			row.put("Service No",serviceNo.toString())
			
			String serviceName  = getValueFromResponse('get_invoice_details',ExPathRpcEnc.GET_INVOICE_DETAILS_SERVICENAME1)
			logi.logInfo("Service Name => "+serviceName)
			row.put("Service Name",serviceName.toString())
	
			String RPU  = getValueFromResponse('get_invoice_details',ExPathRpcEnc.GET_INVOICE_DETAILS_RATE_PER_UNIT3)
			logi.logInfo("Last Name => "+RPU.toString())
			row.put("Rate Per Unit",RPU.toString())
			
			String Unit  = getValueFromResponse('get_invoice_details',ExPathRpcEnc.GET_INVOICE_DETAILS_UNITS1)
			logi.logInfo("Unit => "+Unit.toString())
			row.put("Unit",Unit.toString())
			
			String Amount  = getValueFromResponse('get_invoice_details',ExPathRpcEnc.GET_INVOICE_DETAILS_AMOUNT1)
			logi.logInfo("Amount => "+Amount.toString())
			row.put("Amount",Amount.toString())
			
			String custStatusLabel  = getValueFromResponse('get_invoice_details',ExPathRpcEnc.GET_INVOICE_DETAILS_CUSTOM_STATUS_LABEL)
			logi.logInfo("Custom Status Label => "+custStatusLabel.toString())
			row.put("Custom Status Label",custStatusLabel.toString())
			
			String custStatusDesc  = getValueFromResponse('get_invoice_details',ExPathRpcEnc.GET_INVOICE_DETAILS_CUSTOM_STATUS_DESC)
			logi.logInfo("Custom Status Desc => "+custStatusDesc.toString())
			row.put("Custom Status Desc",custStatusDesc.toString())
			
			String clientNotes  = getValueFromResponse('get_invoice_details',ExPathRpcEnc.GET_INVOICE_DETAILS_CLIENT_NOTES)
			logi.logInfo("Client Notes => "+clientNotes.toString())
			row.put("Client Notes",clientNotes.toString())
			
			String isPendingIndicator  = getValueFromResponse('get_invoice_details',ExPathRpcEnc.GET_INVOICE_DETAILS_IS_PENDING_INDICATOR)
			logi.logInfo("Is Pending Indicator => "+isPendingIndicator.toString())
			row.put("Is Pending Indicator",isPendingIndicator.toString())
			
			logi.logInfo("md_get_invoice_details_api => "+row)
			return row.sort()
		
	}
	
	def md_get_invoice_details_DB(String testcaseId)
	{
		
			logi.logInfo("Inside method md_get_invoice_details_DB")
			// Getting the account number and client id
			HashMap row =new HashMap()
			def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			
			if(accountNo =='NoVal')
			{
				   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
			}
			String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
			ConnectDB db = new ConnectDB()
			String invoice_no=db.executeQueryP2(invoiceQuery)
			
			DecimalFormat df = new DecimalFormat("#.##");
			
			String planNo_Q =  "SELECT PLAN_NO FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=  "+invoice_no+ "and client_no = "+clientNo
			String planNo = db.executeQueryP2(planNo_Q)
			logi.logInfo("Plan No  => "+planNo.toString())
			row.put("Plan No",planNo.toString())
			
			String planName_Q =  "SELECT PLAN_NAME FROM ARIACORE.CLIENT_PLAN WHERE PLAN_NO=  "+planNo+ "and client_no = "+clientNo
			String planName = db.executeQueryP2(planName_Q)
			logi.logInfo("Plan Name  => "+planName.toString())
			row.put("Plan Name",planName.toString())
			
			String serviceNo_Q =  "SELECT SERVICE_NO FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=  "+invoice_no+ "and client_no = "+clientNo
			String serviceNo = db.executeQueryP2(serviceNo_Q)
			logi.logInfo("Service No  => "+serviceNo.toString())
			row.put("Service No",serviceNo.toString())
			
			String serviceName_Q =  "SELECT comments FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=  "+invoice_no+ "and client_no = "+clientNo
			String serviceName = db.executeQueryP2(serviceName_Q)
			logi.logInfo("Service Name  => "+serviceName.toString())
			row.put("Service Name",serviceName.toString())
			
			String RPU_Q =  "SELECT usage_rate FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=  "+invoice_no+ "and client_no = "+clientNo
			String RPU = db.executeQueryP2(RPU_Q)
			logi.logInfo("Rate Per Unit  => "+RPU.toString())
			row.put("Rate Per Unit",RPU.toString())
			
			String Unit_Q =  "SELECT usage_rate FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=  "+invoice_no+ "and client_no = "+clientNo
			String Unit = db.executeQueryP2(RPU_Q)
			logi.logInfo("Unit  => "+Unit.toString())
			row.put("Unit",Unit.toString())
			
			String Amount_Q =  "SELECT debit FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO=  "+invoice_no+ "and client_no = "+clientNo
			String Amount = db.executeQueryP2(Amount_Q)
			logi.logInfo("Amount => "+Amount.toString())
			row.put("Amount",Amount.toString())
			
			String CustLabel_Q =  "SELECT CUSTOM_STATUS_LABEL FROM ARIACORE.GL WHERE ACCT_NO= "+accountNo+ "and client_no = "+clientNo
			String CustLabel = db.executeQueryP2(CustLabel_Q)
			logi.logInfo("Custom Status Label => "+CustLabel.toString())
			row.put("Custom Status Label",CustLabel.toString())
			
			String CustDesc_Q =  "SELECT CUSTOM_STATUS_DESC FROM ARIACORE.GL WHERE ACCT_NO= "+accountNo+ "and client_no = "+clientNo
			String CustDesc = db.executeQueryP2(CustDesc_Q)
			logi.logInfo("Custom Status Desc => "+CustDesc.toString())
			row.put("Custom Status Desc",CustDesc.toString())
			
			String ClientNotes_Q =  "SELECT CLIENT_NOTES FROM ARIACORE.GL WHERE ACCT_NO= "+accountNo+ "and client_no = "+clientNo
			String ClientNotes = db.executeQueryP2(ClientNotes_Q)
			logi.logInfo("Client Notes => "+ClientNotes.toString())
			row.put("Client Notes",ClientNotes.toString())
			
			String indicator=1
			String IsPending_Q =  "SELECT * FROM ARIACORE.GL_PENDING WHERE ACCT_NO= "+accountNo+ "and client_no = "+clientNo
			String IsPending = db.executeQueryP2(IsPending_Q)
			if(IsPending==null)
			{
			indicator=0
			}
			logi.logInfo("Is Pending Indicator => "+indicator.toString())
			row.put("Is Pending Indicator",indicator.toString())

			logi.logInfo("md_get_acct_details_all_DB => "+row)
			return row.sort()
								
	}
	
	String md_billthrudays(String testCaseId)
	{
		logi.logInfo("Inside method md_billthrudays")
		// Getting the account number and client id
		String start_Query,start_date,end_Query,end_date;
		HashMap row =new HashMap()
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB()
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		logi.logInfo("Starting query executionnnnnnnnnnnnnnnnnnnnnnnnnnnnn")
		 start_Query="SELECT to_char(trunc(BILL_FROM_DATE),'dd-MM-yyyy') FROM ARIACORE.GL WHERE ACCT_NO="+accountNo+ " AND CLIENT_NO="+clientNo
		 start_date=database.executeQueryP2(start_Query)
		 end_Query="SELECT to_char(trunc(BILL_THRU_DATE+1),'dd-MM-yyyy') FROM ARIACORE.GL WHERE ACCT_NO="+accountNo+ " AND CLIENT_NO="+clientNo
		 end_date=database.executeQueryP2(end_Query)
		logi.logInfo("Query executedddddddddddddddddddddddddddddddd")
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
		Date d1=formatter.parse(start_date)
		logi.logInfo "Date1 "+d1
		Date d2=formatter.parse(end_date)
		logi.logInfo "Date2 "+d2
		int daysInBetween=getDaysBetween(d1,d2);
		logi.logInfo "Days in between "+daysInBetween.toString()
		return daysInBetween.toString()
	
	}
	
	String md_billingIntevalDays(String testCaseId)
	{
		logi.logInfo("Inside method md_billingIntevalDays")
		// Getting the account number and client id
		String start_Query,start_date,end_Query,end_date;
		HashMap row =new HashMap()
		
		def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB()
		if(accountNo =='NoVal')
		{
			   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		ConnectDB db = new ConnectDB()
		String planNo_Query="SELECT PLAN_NO FROM ARIACORE.GL WHERE ACCT_NO= "+accountNo+ " AND CLIENT_NO= "+clientNo
		String planNo=db.executeQueryP2(planNo_Query)
		logi.logInfo "Master Plan no "+planNo
		String billingInterval_Query="SELECT BILLING_INTERVAL FROM ARIACORE.CLIENT_PLAN WHERE PLAN_NO= "+planNo+ " AND CLIENT_NO= "+clientNo
		String billingInterval=db.executeQueryP2(billingInterval_Query)
		logi.logInfo "Billing interval of Master Plan "+billingInterval
		List list1,interval
		String supp_plan_no,billInt
		/*String supp_plan="SELECT SUPP_PLAN_NO from ariacore.acct_supp_plan_map_view where acct_no= "+accountNo
		ResultSet rs=db.executePlaQuery(supp_plan)
		logi.logInfo "Executed result set...."
		while(rs.next())
		{
			logi.logInfo" Inside result set of plan....."
			supp_plan_no=rs.getString(1)
			logi.logInfo "Noooooooooooooooooooooooo "+supp_plan_no.toString()
			list1.add(supp_plan_no);
			logi.logInfo "Plan Addedddddddddddddddddddd "+supp_plan_no.toString()
			
		}
		logi.logInfo "Plan list : "+list1.toString()
		for(int i=0;i<list1.size();i++)
		{
			//plan.clear()
			String service_query="SELECT BILLING_INTERVAL FROM ARIACORE.CLIENT_PLANS WHERE PLAN_NO= "+plan.get(i).toString()+ " AND CLIENT_NO= "+clientNo
			rs=db.executePlaQuery(service_query)
			while(rs.next())
			{
				logi.logInfo "Inside result set..."
				billInt=rs.getString(1)
				interval.add(billInt)
			}
		}
		interval.add(billingInterval)
		logi.logInfo "Interval list : "+interval.toString()
		int smallInterval=Collections.min(interval)
		logi.logInfo "Smallest Intervalllllllllllll : "+smallInterval.toString()*/
		
		logi.logInfo("Starting query executionnnnnnnnnnnnnnnnnnnnnnnnnnnnn")
		start_Query="SELECT to_char(trunc(BILL_FROM_DATE+1),'dd-MM-yyyy') FROM ARIACORE.GL WHERE ACCT_NO="+accountNo+ " AND CLIENT_NO="+clientNo
		start_date=database.executeQueryP2(start_Query)
		logi.logInfo "Start date: "+start_date.toString()
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
		Date d1=formatter.parse(start_date)
		logi.logInfo "Parsed dateeeeeeeee "+d1
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		cal.setTime(d1)
		logi.logInfo  "Set in callllllllllllllllll"
		//cal.add(Calendar.DAY_OF_MONTH, 1);
		//int billInt=billingInterval.toInteger()
		cal1.setTime(cal.getTime())
		logi.logInfo  "Set in cal1"
		cal1.add(Calendar.MONTH, 1);
		//Date d2=formatter.parse(date2)
		logi.logInfo "End dateeeeeeeeeeeeee "
		//int daysInBetween=getDaysBetween(cal,cal1);
		long diff = cal.getTimeInMillis() - cal1.getTimeInMillis();
		// Calculate difference in hours
		long diffHours = (diff / (60 * 60 * 1000))*-1;
		logi.logInfo "diffHoursssssssssssssss "+diffHours.toString()
		long diffInDays=diffHours/24
		logi.logInfo "diffDaysssssssssssssss "+diffInDays.toString()
		return diffInDays.toString();
	
	}
   
	
	public md_Tax_Recurring_Service1(String testCaseId)
	   {
		   logi.logInfo("method md_Tax_Usage_Service1")
		   String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		   double usg = 0.0
		   double cal_usg
		   ConnectDB db = new ConnectDB()
		   DecimalFormat d = new DecimalFormat("#.##");
		   String apiname=testCaseId.split("-")[2]
		   double totalInvoice=0.0
		   String invoice_no = getInvoiceNoVT(client_no, acct_no)
		   logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		///   def exp = md_assign_supp_Proration_amount(testCaseId,acct_no)
		 //  logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		   def amt=db.executeQueryP2("SELECT MIN(RATE_PER_UNIT) FROM ariacore.ACCT_RATES WHERE SERVICE_NO IN (10136170,10136171) AND CLIENT_NO="+client_no+" AND RATE_SEQ_NO=1 AND PLAN_NO=(SELECT PLAN_NO FROM ARIACORE.ACCT where acct_no ="+acct_no+" and client_no ="+client_no+")").toString().toDouble()
		   logi.logInfo( "Credit amount " +amt)
		   def units =db.executeQueryP2("SELECT MAX(BASE_PLAN_UNITS) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = "+invoice_no+"").toString().toDouble()
		   logi.logInfo( "Tax Amount " +units)
		   def tax =db.executeQueryP2("SELECT MIN(RATE) from ARIACORE.TAX_CALC_VALUE WHERE CLIENT_NO="+client_no+"").toString().toDouble()
		   logi.logInfo( "Tax Amount " +tax)
		   totalInvoice =  (amt.toDouble()*units.toDouble()*tax.toDouble())
		   logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		   return d.format(totalInvoice.toDouble()).toString()
	   }
	   
	   
	  
	   public md_Tax_Recurring_Service2(String testCaseId)
	   {
		   logi.logInfo("method md_Tax_Usage_Service2")
		   String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		   double usg = 0.0
		   double cal_usg
		   ConnectDB db = new ConnectDB()
		   DecimalFormat d = new DecimalFormat("#.##");
		   String apiname=testCaseId.split("-")[2]
		   double totalInvoice=0.0
		   String invoice_no = getInvoiceNoVT(client_no, acct_no)
		   logi.logInfo( "invoiceNo in md_ExpInvoiceWithoutUsage_Amt method " +invoiceNo)
		///   def exp = md_assign_supp_Proration_amount(testCaseId,acct_no)
		 //  logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+exp.toString())
		   def amt=db.executeQueryP2("SELECT MAX(RATE_PER_UNIT) FROM ariacore.ACCT_RATES WHERE SERVICE_NO IN (10136170,10136171) AND CLIENT_NO="+client_no+" AND RATE_SEQ_NO=1 AND PLAN_NO=(SELECT PLAN_NO FROM ARIACORE.ACCT where acct_no ="+acct_no+" and client_no ="+client_no+")").toString().toDouble()
		   logi.logInfo( "Credit amount " +amt)
		   def units =db.executeQueryP2("SELECT MAX(BASE_PLAN_UNITS) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = "+invoice_no+"").toString().toDouble()
		   logi.logInfo( "Tax Amount " +units)
		   def tax =db.executeQueryP2("SELECT MIN(RATE) from ARIACORE.TAX_CALC_VALUE WHERE CLIENT_NO="+client_no+"").toString().toDouble()
		   logi.logInfo( "Tax Amount " +tax)
		   totalInvoice =  (amt.toDouble()*units.toDouble()*tax.toDouble())
		   logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		   return d.format(totalInvoice.toDouble()).toString()
	   }
	   
	   
	   
	   
	   public md_Total_Reccuring_Tax(String testCaseId)
	   {
		   logi.logInfo("method md_Total_Recurring_Tax")
		   DecimalFormat d = new DecimalFormat("#.##");
		   String apiname=testCaseId.split("-")[2]
		   double totalInvoice=0.0
		   double rec1=0.0
		   double rec2=0.0
		   rec1 = md_Tax_Recurring_Service1(testCaseId).toString().toDouble()
		   logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+rec1.toString())
		   rec2 = md_Tax_Recurring_Service2(testCaseId).toString().toDouble()
		   logi.logInfo( "Credit amount " +rec2.toString())
	       totalInvoice =  (rec1.toDouble())+(rec2.toDouble())
		   logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		   return d.format(totalInvoice.toDouble()).toString()
	   }
	   
	   
	   
	   public md_Total_Invoice_Before_Tax(String testCaseId)
	   {
		   logi.logInfo("method md_Invoice_Before_Tax")
		   String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		   DecimalFormat d = new DecimalFormat("#.##");
		   String apiname=testCaseId.split("-")[2]
		   String invoice_no = getInvoiceNoVT(client_no, acct_no)
		   logi.logInfo( "Invoice_No " +invoice_no.toString())
		   double totalInvoice=0.0
		   double tax=0.0
		   ConnectDB db = new ConnectDB()
		   def amt=db.executeQueryP2("SELECT NVL(SUM(DEBIT),0) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = "+invoice_no+"").toString().toDouble()
		   logi.logInfo( "Invoice Amount " +amt.toString())
		   tax = md_Total_Reccuring_Tax(testCaseId).toString().toDouble()
		   logi.logInfo("expected Invoice in md_Total_Reccuring_Tax  "+tax.toString())
		   totalInvoice =  (amt.toDouble())-(tax.toDouble())
		   logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
		   return d.format(totalInvoice.toDouble()).toString()
	   }
	     
	   
	   
	   public md_Total_Recurring_Amt_Before_Tax(String testCaseId)
	   {
		   logi.logInfo("method md_Total_Recurring_Amt_Before_Tax")
		   DecimalFormat d = new DecimalFormat("#.##");
		   String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		   String apiname=testCaseId.split("-")[2]
		   double totalInvoice=0.0
		   double rec1=0.0
		   ConnectDB db = new ConnectDB()
		   def invoice_no=db.executeQueryP2("SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+"  AND CLIENT_NO="+client_no+"").toString().toDouble()
		   logi.logInfo( "invoice no is " +invoice_no)
		   def units =db.executeQueryP2("SELECT MAX(BASE_PLAN_UNITS) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = "+invoice_no+"").toString().toDouble()
		   logi.logInfo( "Reccuring Units " +units)
//		   if(Service.equals("MIN"))
//		   {
//		   rec1 = md_Tax_Recurring_Service1(testCaseId).toString().toDouble()
//		   logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+rec1.toString())
//		   def amt=db.executeQueryP2("SELECT MIN(USAGE_RATE) from ARIACORE.GL_DETAIL WHERE INVOICE_NO ="+invoice_no+" AND USAGE_UNITS=(SELECT MAX(BASE_PLAN_UNITS) from ARIACORE.GL_DETAIL WHERE INVOICE_NO ="+invoice_no+")").toString().toDouble()
//		   logi.logInfo( "Max Reccuring amount " +amt)
//		   totalInvoice =  ((amt.toDouble()*units.toDouble())-(rec1.toDouble()))
//		   logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
//		   }
//		   else if(Service.equals("MAX"))
//		   {
		   rec1 = md_Tax_Recurring_Service1(testCaseId).toString().toDouble()
		   logi.logInfo( "Min Reccuring amount  " +rec1.toString())
		   def amt=db.executeQueryP2("SELECT MIN(RATE_PER_UNIT) FROM ariacore.ACCT_RATES WHERE SERVICE_NO IN (10136170,10136171) AND CLIENT_NO="+client_no+" AND RATE_SEQ_NO=1 AND PLAN_NO=(SELECT PLAN_NO FROM ARIACORE.ACCT where acct_no ="+acct_no+" and client_no ="+client_no+")").toString().toDouble()
		   logi.logInfo( "aMOUNT CREDITED " +amt)
		   totalInvoice =  ((amt.toDouble()*units.toDouble())-(rec1.toDouble()))
		   logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
//		   }
		   
		   return d.format(totalInvoice.toDouble()).toString()
	   }
	   
	   public md_Total_Recurring_Amt_Before_Tax2(String testCaseId)
	   {
		   logi.logInfo("method md_Total_Recurring_Amt_Before_Tax")
		   DecimalFormat d = new DecimalFormat("#.##");
		   String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   String acct_no=getValueFromResponse('create_acct_complete',"//acct_no")
		   String apiname=testCaseId.split("-")[2]
		   double totalInvoice=0.0
		   double rec1=0.0
		   ConnectDB db = new ConnectDB()
		   def invoice_no=db.executeQueryP2("SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO ="+acct_no+"  AND CLIENT_NO="+client_no+"").toString().toDouble()
		   logi.logInfo( "invoice no is " +invoice_no)
		   def units =db.executeQueryP2("SELECT MAX(BASE_PLAN_UNITS) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = "+invoice_no+"").toString().toDouble()
		   logi.logInfo( "Reccuring Units " +units)
//		   if(Service.equals("MIN"))
//		   {
		   rec1 = md_Tax_Recurring_Service2(testCaseId).toString().toDouble()
		   logi.logInfo("expected Invoice in md_ExpInvoiceWithoutUsage_Amt method  "+rec1.toString())
		   def amt=db.executeQueryP2("SELECT MAX(RATE_PER_UNIT) FROM ariacore.ACCT_RATES WHERE SERVICE_NO IN (10136170,10136171) AND CLIENT_NO="+client_no+" AND RATE_SEQ_NO=1 AND PLAN_NO=(SELECT PLAN_NO FROM ARIACORE.ACCT where acct_no ="+acct_no+" and client_no ="+client_no+")").toString().toDouble()
		   logi.logInfo( "Max Reccuring amount " +amt)
		   totalInvoice =  ((amt.toDouble()*units.toDouble())-(rec1.toDouble()))
		   logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
//		   }
//		   else if(Service.equals("MAX"))
//		   {
//		   rec1 = md_Tax_Recurring_Service1(testCaseId).toString().toDouble()
//		   logi.logInfo( "Min Reccuring amount  " +rec1.toString())
//		   def amt=db.executeQueryP2("SELECT MAX(USAGE_RATE) from ARIACORE.GL_DETAIL WHERE INVOICE_NO ="+invoice_no+" AND USAGE_UNITS=(SELECT MAX(BASE_PLAN_UNITS) from ARIACORE.GL_DETAIL WHERE INVOICE_NO ="+invoice_no+")").toString().toDouble()
//		   logi.logInfo( "aMOUNT CREDITED " +amt)
//		   totalInvoice =  ((amt.toDouble()*units.toDouble())-(rec1.toDouble()))
//		   logi.logInfo("totalInvoice in md_ExpInvoice "+ totalInvoice)
//		   }
		   
		   return d.format(totalInvoice.toDouble()).toString()
	   }
	   
	
   
   
}