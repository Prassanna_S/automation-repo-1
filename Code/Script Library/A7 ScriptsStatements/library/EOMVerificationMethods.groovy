package library
import java.awt.geom.Arc2D.Double;
import java.lang.reflect.Method
import java.sql.ResultSet;
import java.sql.ResultSetMetaData
import java.sql.SQLException
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Scanner;
import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.text.NumberFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;





import groovy.xml.StreamingMarkupBuilder

import java.lang.Object

import org.apache.ivy.core.module.descriptor.ExtendsDescriptor;

import com.lowagie.text.Row;

import org.apache.poi.hssf.record.aggregates.WorksheetProtectionBlock;
import org.apache.poi.hssf.usermodel.*;

import java.util.List;
import java.util.Iterator;
import java.util.HashMap;
import java.awt.geom.Arc2D.Float;
import java.lang.reflect.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import library.Constants.*;
import library.GenericLibrary.*;

import java.sql.ResultSet;

import org.apache.commons.lang.StringUtils;

import library.Constants.Constant;
import library.Constants.ExpValueConstants;
import library.Constants.ExPathRpc;
import library.Constants.ExPathRpcEnc;
import library.Constants.ExPathAdminToolsApi;
import library.Constants.Constant.RESULT_TYPE;

/**
 * @author prassanna.senthilkum
 *
 */
/**
 * @author prassanna.senthilkum
 *
 */
public class EOMVerificationMethods  extends DunningVerificationMethods {
	String pnode="SOAP-ENV:Envelope"
	String pname=null
	static String childnodename = null
	
	int Sindex=0;

	LogResult logi = new LogResult(Constant.logFilePath)
	static ConnectDB db = null;
	public DecimalFormat df=new DecimalFormat("#.##")

	InputMethods im = new InputMethods()
	public EOMVerificationMethods(ConnectDB db) {
		super(db)
	}

	
	/**
	 * @param parentnode
	 * @return the entire hashmap
	 */
	public  LinkedHashMap md_getspecificchildnodes_m(String parentnode) {
		try{
								logi.logInfo(parentnode);
								logi.logInfo("inside md_getspecificchildnodes_m");
								pnode=parentnode.split("\\,")[0]
								logi.logInfo ""+pnode;
								int index=Integer.parseInt(parentnode.split("\\,")[1]);
								index-=1;
								logi.logInfo ""+index;
								ArrayList<String> childnodes = new ArrayList<String>();
								ArrayList<ArrayList> all = new ArrayList<ArrayList>();
								LinkedHashMap<String, String> inner = new LinkedHashMap<String, String>();

								String xmlRecords = library.Constants.Constant.RESPONSE


								//for getting the values of child nodes

								childnodes=getnode(xmlRecords,pnode,index);
								logi.logInfo("CHILDDD :"+childnodes)
								for (String number : childnodes)

								{
												String a=library.Constants.Constant.service.toString()

												logi.logInfo("values :"+getval2(xmlRecords,number,index))

												if(getval2(xmlRecords,number,index)!=null) { inner.put(number.replace("ns1:",""),getval2(xmlRecords,number,index))}
								}
								logi.logInfo ("hashmap"+inner)
								return inner.sort()
		}
		catch(Exception e)
		{
			return "NoVal"
		}
								}
	
	
	

	
	/**
	 * @param parentnode
	 * @return entire hashmap without sorting
	 */
	public  LinkedHashMap md_getspecificchildnodes2_m(String parentnode) {
		try{
								logi.logInfo(parentnode);
								logi.logInfo("inside md_getspecificchildnodes_m");
								pnode=parentnode.split("\\,")[0]
								logi.logInfo ""+pnode;
								int index=Integer.parseInt(parentnode.split("\\,")[1]);
								index-=1;
								logi.logInfo ""+index;
								ArrayList<String> childnodes = new ArrayList<String>();
								ArrayList<ArrayList> all = new ArrayList<ArrayList>();
								LinkedHashMap<String, String> inner = new LinkedHashMap<String, String>();

								String xmlRecords = library.Constants.Constant.RESPONSE


								//for getting the values of child nodes

								childnodes=getnode(xmlRecords,pnode,index);
								logi.logInfo("CHILDDD :"+childnodes)
								for (String number : childnodes)

								{
												String a=library.Constants.Constant.service.toString()

												logi.logInfo("values :"+getval2(xmlRecords,number,index))

												if(getval2(xmlRecords,number,index)!=null) { inner.put(number.replace("ns1:",""),getval2(xmlRecords,number,index))}
								}
								logi.logInfo ("hashmap"+inner)
								return inner
								}
								catch(Exception e)
										{
											return "NoVal"
										}
								}
	
	public  LinkedHashMap md_getspecificchildnodesRname_m(String parentnode,int index) {
		try{
								logi.logInfo(parentnode);
								logi.logInfo("inside md_getspecificchildnodesRname_m");
								pnode=parentnode.split("\\,")[0]
								String [] rname=parentnode.split(",");
								parentnode=parentnode.split(",")[0];
								logi.logInfo ""+pnode;
							//	int index=Integer.parseInt(parentnode.split("\\,")[1]);
								index-=1;
								logi.logInfo ""+index;
								ArrayList<String> childnodes = new ArrayList<String>();
								ArrayList<ArrayList> all = new ArrayList<ArrayList>();
								LinkedHashMap<String, String> inner = new LinkedHashMap<String, String>();

								String xmlRecords = library.Constants.Constant.RESPONSE


								//for getting the values of child nodes

								childnodes=getnode(xmlRecords,pnode,index);
								logi.logInfo("CHILDDD :"+childnodes)
								for (String number : childnodes)

								{
												String a=library.Constants.Constant.service.toString()

												logi.logInfo("values :"+getval2(xmlRecords,number,index))

												if(getval2(xmlRecords,number,index)!=null)
												
													{
														if(!Arrays.asList(rname).contains(number.replace("ns1:","")))
													inner.put(number.replace("ns1:",""),getval2(xmlRecords,number,index))
													
													}
								}
								logi.logInfo ("hashmap"+inner)
								return inner
								}
								catch(Exception e)
										{
											return "NoVal"
										}
								}

	/**
	 *used to get all the child node's child nodes and its values
	 *param can be sapped if u feel there are more than one nodes with the same name
	 * @param parentnode
	 * @return outer
	 * @throws Exception
	 */
		
		 LinkedHashMap md_GetAllChildNodes_m(String parentnode) throws Exception{
			  logi.logInfo(parentnode);
			  pnode=parentnode
			  ArrayList<String> childnodes = new ArrayList<String>();
			  ArrayList<ArrayList> all = new ArrayList<ArrayList>();
			  LinkedHashMap<String, String> inner = new LinkedHashMap<String, String>();
			  LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
			  String xmlRecords = library.Constants.Constant.RESPONSE
			  //Getting the first level of child nodes
			  childnodes=getnode(xmlRecords,parentnode,0);
			  int i=0;
			  //for getting the second level of child nodes
			  for (String number : childnodes)
			  {
					 all.add(getnode(xmlRecords,number,i));
					 i++;

			  }
			  int j=1;
			  for(ArrayList<String> a1 : all)
			  {
					 pname=childnodes.get(j-1);
					 for(String a2 : a1)
					 {
						   if(getval(xmlRecords,a2,(j-1))!=null)
								  inner.put(a2.replace("ns1:", ""),getval(xmlRecords,a2,(j-1)));

					 }
					 outer.put(childnodes.get(j-1).replace("ns1:", "")+"["+(j-1)+"]", inner.sort());
					 inner = new LinkedHashMap<String, String>();
					 j++;
			  }

			  return outer
	   }




	public  ArrayList<String> getnode(String xmlRecords, String number, Integer index) throws Exception{
		ArrayList<String> nn = new ArrayList<String>();
		try {
			//logi.logInfo "into getnode "+number +" : "+index
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords));
			DocumentBuilderFactory     dbFactory=DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder=dbFactory.newDocumentBuilder();
			Document document=dBuilder.parse(is);
			document.getDocumentElement().normalize();
			Node rootNode=document.getDocumentElement(); //saving root node in a variable.
			NodeList r= document.getElementsByTagName(number);

			rootNode=r.item(index);
			NodeList nList=rootNode.getChildNodes(); //to store the child nodes as node list.
		
			for (int i = 0;i < nList.getLength(); i++) {
				Node node = nList.item(i);
			
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					 
					nn.add(node.getNodeName());
				}
				
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		//logi.logInfo "getnode giving "+nn
		return nn;

	}
	public  String getval(String xmlRecords,String name1,int index) throws Exception{
								HashMap<String, String> hm = new HashMap<String, String>();
								String temp = null;
								DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
								InputSource is = new InputSource();
								is.setCharacterStream(new StringReader(xmlRecords));
								Document doc = db.parse(is);
								NodeList nodes = doc.getElementsByTagName(pname);
								Element element = (Element) nodes.item(index);
								NodeList name = element.getElementsByTagName(name1);
								Element line = (Element) name.item(0);
								temp=getCharacterDataFromElement(line);
								if(temp!=null)
								return temp;else return null
				}



					public  String getval2(String xmlRecords,String name1,int index) throws Exception{

								HashMap<String, String> hm = new HashMap<String, String>();
								String temp = null;
								DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
								InputSource is = new InputSource();
								is.setCharacterStream(new StringReader(xmlRecords));
								Document doc = db.parse(is);
								NodeList nodes = doc.getElementsByTagName(pnode);
								Element element = (Element) nodes.item(index);
								NodeList name = element.getElementsByTagName(name1);
								Element line = (Element) name.item(0);
								temp=getCharacterDataFromElement(line);
								if(temp!=null)
								return temp;else return null
				}



	public String getCharacterDataFromElement(Element e) {
	
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return null;
	}
	public String getCharacterDataFromElement2(Element e) {
		
			Node child = e.getFirstChild();
			if (child instanceof CharacterData) {
				CharacterData cd = (CharacterData) child;
				return cd.getData();
			}
			return 'NO VALUE';
		}
	
	def md_GET_MPI_NO2(String mode) throws Exception
	{
		//if the account has only one acct, dont mention the acct index and only mention the MPI index
		def MPINO
		try{
		logi.logInfo("calling md_MPI_NO ")
		
		logi.logInfo("input string :"+mode)
		String Acct,MPI
		String Xpath;
		
		if (mode.contains(","))
		{

			Acct=mode.split("\\,")[0]
			MPI=mode.split("\\,")[1]
			Xpath=ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M+"["+Acct+"]"+ExPathRpcEnc.CREATE_ACCT_COMPLETE_MPINO_M+"_row["+MPI+"]/*:plan_instance_no[1]";
			MPINO = getValueFromResponse("create_acct_complete_m",Xpath)
			if(MPINO==null||MPI.toString().equalsIgnoreCase("noval"))
			{
				Xpath=ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M+"[1]/*:out_acct_row["+Acct+"]"+ExPathRpcEnc.CREATE_ACCT_COMPLETE_MPINO_M+"[1]/*:master_plans_assigned_row["+MPI+"]/*:plan_instance_no[1]";
				MPINO = getValueFromResponse("create_acct_complete_m",Xpath)
			}
			
		}
		else
		{
			MPI=mode
			Xpath=ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M+"["+MPI+"]/ns1:plan_instance_no[1]"
			
		}
		
		logi.logInfo("Xpath : "+Xpath)
		
		
		if(MPINO==null||MPI.toString().equalsIgnoreCase("noval")){throw NullPointerException}
		
		}catch (Exception e)
		{    	logi.logInfo(" There is no such MPI present ")	}
		return MPINO
	}
	
	def md_GET_VT(String mode)
	{
		logi.logInfo(" md_get_VT ")
		String Query="select to_char(ariacore.ariavirtualtime("+mode+"),'YYYY-MM-DD') from dual"
		ConnectDB db = new ConnectDB()
		logi.logInfo(Query)
		def VT=db.executeQueryP2(Query)
		return VT.toString()
		
	}
	
	public  String md_getspecificnode_m(String parentnode) throws Exception{
		logi.logInfo("into md_getspecificnode_m")
		logi.logInfo("param got "+parentnode)
		LinkedHashMap<String, String> inner = new LinkedHashMap<String, String>();
		String name = parentnode.split("\\,")[2]
		inner=md_getspecificchildnodes_m(parentnode)
		String result=inner.get(name).toString();
		logi.logInfo("node to be got "+name)
		logi.logInfo("getting node over "+result)
		return result
	}
	
	public String md_get_acct_count_m(String empty){
		logi.logInfo("into md_get_acct_count_m")
		LinkedHashMap<String,String> inner = new LinkedHashMap<String,String>()
		int acct_count=im.md_GET_ALL_ACCT_NO_m(empty,inner).size()
		logi.logInfo("method caught accts "+acct_count)
		return acct_count.toString()
		
	}

	public String md_GET_MPI_NO_M_FROM_DB (String str)
	{
					VerificationMethods vm =new VerificationMethods()
					String acct_no= getValueFromResponse('create_acct_complete_m',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M)
					ConnectDB db = new ConnectDB()
					String Query="SELECT TEMP.MASTER_PLAN_INSTANCE_NO FROM ((SELECT MASTER_PLAN_INSTANCE_NO,ROW_NUMBER() OVER(ORDER BY MASTER_PLAN_INSTANCE_NO DESC)AS MPI  FROM ARIACORE.ACCT_BILLABILITY_BY_BILLDATE_M WHERE ACCT_NO=�+acct_no+�)TEMP) WHERE TEMP.MPI=1"
					String mpi_no=db.executeQuery(Query);
					return mpi_no
	
	}

		public  LinkedHashMap md_getspecificchildnodesRK_m(String parentnode) {
			try{
			logi.logInfo("into md_getspecificchildnodesRK_m")
			String parent=parentnode.split("#")[0]
			LinkedHashMap<String, String> inner = new LinkedHashMap<String, String>();
			LinkedHashMap<String, String> check = new LinkedHashMap<String, String>();
			LinkedHashMap<String, String> check2 = new LinkedHashMap<String, String>();
			LinkedHashMap<String, String> temp = new LinkedHashMap<String, String>();
			String rk=parentnode.split("#")[1]
			List<String> list = new ArrayList<String>();
					
			inner=md_getspecificchildnodes2_m(parent)
			int z=0
			for( String key : inner.keySet())
			{
				z++
				check.put(z.toString(),key)
				
			}
			logi.logInfo("Check : "+check)
			
			
			
			String[] format=rk.split (",")
			int count=0;
			for (int i = 0; i < rk.length(); i++)
			{
				  if (rk.charAt(i) == ',')
				  {
						 count++;
				  }
			}
			int length=count+1
			
			if(format[0].equalsIgnoreCase("r"))
			{
				for(int start=1;start<length;start++)
				{
					  String unformatted=format[start]
					  list.add(unformatted);
					  
				}
				
				for(String key : check.keySet())
				{
					if(!(list.contains(key)))
					{
						logi.logInfo("into r case : "+key)
					   check2.put(key,check.get(key))
					}
				}
				logi.logInfo("Check2 : "+check2)
				//removing the keys are done
				//now moving the value to a result hash called temp
				
				for( String key : check2.keySet())
				{
					temp.put(check2.get(key),inner.get(check2.get(key)))
					
				}
				
			}else if(format[0].equalsIgnoreCase("k"))
			{
				for(int start=1;start<length;start++)
				{
					  String unformatted=format[start]
					  for(String key : check.keySet())
					  {
						  if(key.equalsIgnoreCase(unformatted))
						  {
							  logi.logInfo("into k case : "+unformatted)
							 check2.put(key,check.get(key))
						  }
					  }
					  
				}
				
				for( String key : check2.keySet())
				{
					temp.put(check2.get(key),inner.get(check2.get(key)))
					
				}
			}
		
			temp=temp.sort()
			return temp
			}
			catch(Exception e)
			{
				return "NoVal"
			}
		}
		
		
		
		
		public String md_getValFromSpecificnodeK_m(String nodesandval)
		{
			List<List<String>> l = new ArrayList<List<String>>(md_getspecificchildnodesRK_m(nodesandval).values())
			return l.get(0).toString()
		}
			 
			 
			 String getExpValue(String constNameReference) {
				 def objxPath
				 String query
		 
				 if(constNameReference.contains('DB') || constNameReference.contains('md') || constNameReference.contains('uv') || constNameReference.contains('auth_')) {
					 objxPath = new ExpValueConstants()
				 } else {
					 query = 'Noqueryfound'
				 }
		 
				 for(Field field : objxPath.getClass().getFields()) {
					 if(field.getName().toString().equalsIgnoreCase(constNameReference)) {
						 Field fieldName = objxPath.getClass().getDeclaredField(field.getName().toString());
						 query = fieldName.get(objxPath)
						 break
					 }
				 }
				 return query
			 }
			 
			 
			 
			public def md_Exe_db_m (String query_name){
				
				
				logi.logInfo "into md_Exe_db_m"+query_name
				String Query=getExpValue(query_name.split("#")[0]);
				Class[] paramTypes = new Class[2]
				paramTypes[0] = String.class;
				paramTypes[1] = LinkedHashMap.class;
				LinkedHashMap<String,String> dummy = new LinkedHashMap<String,String>()
				Object[] param = new Object[2];
				param[0] = query_name.split("#")[1]
				param[1] = dummy
				String module=Query.substring(Query.indexOf("md_"),(Query.indexOf("_m")+2));
				InputMethods im = new InputMethods()
				Method method = im.class.getMethod(module, paramTypes)
				def methodvar = method.invoke(im, param)
				Query = Query.substring(0,Query.indexOf("<md_")) + methodvar + Query.substring((Query.indexOf("_m>")+3),Query.length());
				Query=Query+query_name.split("#")[2]
				
				logi.logInfo "Query :"+Query
				ConnectDB db=new ConnectDB();
				ResultSet resultSet = db.executePlaQuery(Query)
				
				ResultSetMetaData md = resultSet.getMetaData();
			
				int columns = md.getColumnCount();
				if(columns>1)
				{
				LinkedHashMap row = new LinkedHashMap(columns);
				  while (resultSet.next()){
					
					 for(int u=1; u<=columns; ++u){
						
					  row.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
					 }
					
				  }
				 
				 return  row.sort()
				}
				else {if(resultSet.next()){return resultSet.getObject(1).toString()}									}
				
			}
			
			public String md_get_acct_inst_no2_m (String input)
			{
				return im.md_GET_ACCT_OR_INSTANCE_NO2_m(input,null);
			}
			public String md_get_acct_inst_no_m (String input)
			{
				return im.md_GET_ACCT_OR_INSTANCE_NO_m(input,null);
			}
			
			
			/**
						 *To get the Payment method for the account number
						 * @param seq_no
						 * @return all the payment details for the given payment_method number
						 */
	//public LinkedHashMap md_get_acct_pay_methods_m(String seq_no,String disabled_ind)
	public LinkedHashMap md_get_acct_pay_methods_m(String disabled_ind)
	{
		if(disabled_ind=="2")
			disabled_ind=null
		int q=0
		//int q=1


		//int count = Integer.parseInt(seq_no)
		//     logi.logInfo("COUNT "+count)
		logi.logInfo("into md_GET_ACCT_PAY_METHODS")


		logi.logInfo("disabled_ind"+disabled_ind)
		ConnectDB dbb = new ConnectDB()


		InputMethods im=new InputMethods()
		// TO GET TJE ACCT NUMBER
		String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
		logi.logInfo("acct_no"+acct_no)

		//{

		LinkedHashMap h1 = new LinkedHashMap ()
		LinkedHashMap all_pay_methods_lhm1 = new LinkedHashMap()


		// TO find out the pay method as fields differ for CC and ACH
		String pay_type_finding_query=null
		String pay_type_finding=null
		String pay_method_type_query=null
		String pay_method_type=null
		String pay_method_type1=null
		/*if(disabled_ind!=null)
		 {
		 pay_type_finding_query="SELECT count(PAY_METHOD) FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no+" AND DISABLED_IND="+disabled_ind
		 pay_type_finding=dbb.executeQueryP2(pay_type_finding_query)
		 }
		 else if(disabled_ind==null)
		 {
		 pay_type_finding_query="SELECT count(PAY_METHOD) FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no
		 pay_type_finding=dbb.executeQueryP2(pay_type_finding_query)
		 } */

		pay_type_finding_query="SELECT count(PAY_METHOD) FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no
		pay_type_finding=dbb.executeQueryP2(pay_type_finding_query)

		if(pay_type_finding.toInteger() >0)
		{
			for(int j=1;j<=pay_type_finding.toInteger();j++)
			{
				LinkedHashMap all_pay_methods_lhm= new LinkedHashMap()

				if(!(disabled_ind==null))
				{
					String pay_method_type1_query="SELECT count(PAY_METHOD) FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no+" AND DISABLED_IND="+disabled_ind+"and seq_num="+j
					pay_method_type1=dbb.executeQueryP2(pay_method_type1_query);

					if(pay_method_type1.toInteger() >0)
					{
						pay_method_type_query="SELECT PAY_METHOD FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no+" AND DISABLED_IND="+disabled_ind+"and seq_num="+j
						pay_method_type=dbb.executeQueryP2(pay_method_type_query);
					}
				}
				else if (disabled_ind==null)
				{
					String pay_method_type1_query="SELECT count(PAY_METHOD) FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no+" and seq_num="+j
					pay_method_type1=dbb.executeQueryP2(pay_method_type1_query);

					if(pay_method_type1.toInteger()>0)

					{
						logi.logInfo("pay_method_type"+pay_method_type1)
						// FOR CC
						pay_method_type_query="SELECT PAY_METHOD FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no+"and seq_num="+j
						pay_method_type=dbb.executeQueryP2(pay_method_type_query);
					}



				}
				//String pay_type_finding="SELECT PAY_METHOD FROM  ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no+" AND DISABLED_IND="+disabled_ind+"and seq_num="+count


				if(pay_method_type1.toInteger()>0)
				{


					String all_pay_methods_pay_details_query=null
					/*
					 // FOR ACH
					 if(pay_method_type.equalsIgnoreCase("2"))
					 {
					 logi.logInfo("PAY method is ACH ")
					 //all_pay_methods_pay_details_query="SELECT PAYMENT_METHOD_NAME AS PAY_METHOD_NAME,PAY_METHOD AS PAY_METHOD_TYPE,SUFFIX AS SUFFIX,BANK_ROUTING_NUM AS BANK_ROUTING_NUM , PAY_METHOD AS PAYMENT_METHOD_NO,PERSISTENT_IND AS PERSISTENT_IND,TO_CHAR(EFFECTIVE_DATE, 'YYYY-MM-DD HH:MI:SSxFF') AS FROM_DATE , 1 AS STATUS FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no+" AND SEQ_NUM="+j//count
					 all_pay_methods_pay_details_query="SELECT PAYMENT_METHOD_NAME AS PAY_METHOD_NAME,PAY_METHOD AS PAY_METHOD_TYPE,SUFFIX AS SUFFIX,BANK_ROUTING_NUM AS BANK_ROUTING_NUM , PAY_METHOD AS PAYMENT_METHOD_NO,PERSISTENT_IND AS PERSISTENT_IND,TO_CHAR(EFFECTIVE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS FROM_DATE  FROM ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no+" AND SEQ_NUM="+j//count
					 }
					 */
					//all_pay_methods_pay_details_query="SELECT distinct BI.PAYMENT_METHOD_NAME AS PAY_METHOD_NAME,BI.PAYMENT_METHOD_DESCRIPTION AS PAY_METHOD_DESCRIPTION,BI.PAY_METHOD AS PAY_METHOD_TYPE,BI.SUFFIX, BI.bank_routing_num  AS BANK_ROUTING_Num,BI.CC_EXPIRE_MM,BI.CC_EXPIRE_YYYY,BI.AGREEMENT_ID AS BILL_AGREEMENT_ID,BI.PERSISTENT_IND,TO_CHAR(BI.EFFECTIVE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS FROM_DATE,BI.SEQ_NUM AS  payment_method_no ,ADS.FIRST_NAME AS BILL_FIRST_NAME,ADS.LAST_NAME AS BILL_LAST_NAME,ADS.COMPANY_NAME AS BILL_COMPANY_NAME,ADS.ADDRESS1 AS BILL_ADDRESS1,ADS.ADDRESS2 AS BILL_ADDRESS2,ADS.CITY AS BILL_CITY,ADS.STATE AS BILL_STATE_PROV,ADS.COUNTRY AS BILL_COUNTRY,ADS.ZIP AS BILL_POSTAL_CD,ADS.EMAIL AS BILL_EMAIL,ADS.INTL_PHONE AS BILL_PHONE FROM ARIACORE.ACCT_ADDRESS  ADS join ARIACORE.BILLING_INFO  BI on BI.ACCT_NO=ADS.ACCT_NO  AND ADS.ADDRESS_SEQ=BI.BILL_ADDRESS_SEQ  WHERE BI.ACCT_NO="+acct_no+" AND BI.SEQ_NUM="+j
					//all_pay_methods_pay_details_query="SELECT PAYMENT_METHOD_NAME AS PAY_METHOD_NAME,PAY_METHOD AS PAY_METHOD_TYPE,SUFFIX, BANK_ROUTING_NUM  AS BANK_ROUTING_NO,CC_EXPIRE_MM,CC_EXPIRE_YYYY,AGREEMENT_ID AS BILL_AGREEMENT_ID,PERSISTENT_IND,TO_CHAR(EFFECTIVE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF'),SEQ_NUM AS  payment_method_no    FROM  ARIACORE.BILLING_INFO WHERE ACCT_NO="+acct_no+" AND SEQ_NUM="+j


					//WITH ADDRESSS
					//all_pay_methods_pay_details_query="SELECT distinct BI.PAYMENT_METHOD_NAME AS PAY_METHOD_NAME,BI.PAYMENT_METHOD_DESCRIPTION AS PAY_METHOD_DESCRIPTION,BI.PAY_METHOD AS PAY_METHOD_TYPE,BI.SUFFIX, BI.bank_routing_num  AS BANK_ROUTING_Num,BI.CC_EXPIRE_MM,BI.CC_EXPIRE_YYYY,BI.AGREEMENT_ID AS BILL_AGREEMENT_ID,BI.PERSISTENT_IND,TO_CHAR(BI.EFFECTIVE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS FROM_DATE,BI.SEQ_NUM AS  payment_method_no ,ADS.FIRST_NAME AS BILL_FIRST_NAME,ADS.LAST_NAME AS BILL_LAST_NAME,ADS.COMPANY_NAME AS BILL_COMPANY_NAME,ADS.ADDRESS1 AS BILL_ADDRESS1,ADS.ADDRESS2 AS BILL_ADDRESS2,ADS.CITY AS BILL_CITY,ADS.STATE AS BILL_STATE_PROV,ADS.COUNTRY AS BILL_COUNTRY,ADS.ZIP AS BILL_POSTAL_CD,ADS.EMAIL AS BILL_EMAIL,ADS.INTL_PHONE AS BILL_PHONE FROM ARIACORE.ACCT_ADDRESS  ADS join ARIACORE.BILLING_INFO  BI on BI.ACCT_NO=ADS.ACCT_NO  AND ADS.ADDRESS_SEQ=BI.BILL_ADDRESS_SEQ  WHERE BI.ACCT_NO="+acct_no+" AND BI.SEQ_NUM="+j
					//all_pay_methods_pay_details_query="SELECT DISTINCT BI.PAYMENT_METHOD_NAME AS PAY_METHOD_NAME,BI.PAYMENT_METHOD_DESCRIPTION  AS PAY_METHOD_DESCRIPTION,BI.PAY_METHOD AS payment_method_no,  BI.PAY_METHOD                        AS PAY_METHOD_TYPE,  BI.SUFFIX,  BI.bank_routing_num AS BANK_ROUTING_Num,  BI.CC_EXPIRE_MM,  BI.CC_EXPIRE_YYYY,  BI.AGREEMENT_ID AS BILL_AGREEMENT_ID,  BI.PERSISTENT_IND,  TO_CHAR(BI.EFFECTIVE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS FROM_DATE   FROM  ARIACORE.BILLING_INFO BI WHERE ACCT_NO="+acct_no+" AND BI.SEQ_NUM="+j

					//all_pay_methods_pay_details_query = "SELECT BI.PAYMENT_METHOD_NAME AS PAY_METHOD_NAME,BI.PAYMENT_METHOD_DESCRIPTION AS PAY_METHOD_DESCRIPTION,BI.PAY_METHOD  AS PAY_METHOD_TYPE,BI.SEQ_NUM   AS payment_method_no,BI.SUFFIX, BI.bank_routing_num AS BANK_ROUTING_Num, BI.CC_EXPIRE_MM,BI.CC_EXPIRE_YYYY, BI.AGREEMENT_ID AS BILL_AGREEMENT_ID, BI.PERSISTENT_IND,TO_CHAR(BI.EFFECTIVE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS FROM_DATE,ACD.FIRST_NAME   AS bill_first_name ,ACD.LAST_NAME  AS bill_last_name ,ACD.ADDRESS1     AS bill_address1 ,ACD.ADDRESS2     AS bill_address2 ,ACD.CITY  AS bill_city ,ACD.COUNTRY      AS bill_country , ACD.COMPANY_NAME AS bill_company_name ,ACD.INTL_PHONE   AS bill_phone ,ACD.ZIP AS bill_postal_cd ,ACD.STATE  AS bill_state_prov FROM ARIACORE.BILLING_INFO BI JOIN ARIACORE.acct_address ACD ON ACD.ACCT_NO=BI.ACCT_NO WHERE BI.ACCT_NO ="+acct_no+" AND ACD.SOURCE_NO=1 AND BI.SEQ_NUM= "+j
					all_pay_methods_pay_details_query = "SELECT PAYMENT_METHOD_NAME   AS PAY_METHOD_NAME,PAYMENT_METHOD_DESCRIPTION AS PAY_METHOD_DESCRIPTION,PAY_METHOD  AS PAY_METHOD_TYPE,SEQ_NUM AS payment_method_no,SUFFIX, bank_routing_num AS BANK_ROUTING_Num, CC_EXPIRE_MM, CC_EXPIRE_YYYY, AGREEMENT_ID AS BILL_AGREEMENT_ID,PERSISTENT_IND, TO_CHAR(EFFECTIVE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS FROM_DATE FROM ARIACORE.BILLING_INFO WHERE ACCT_NO ="+acct_no+" AND SEQ_NUM = "+j

					ResultSet rs1=dbb.executePlaQuery(all_pay_methods_pay_details_query);

					ResultSetMetaData md1 = rs1.getMetaData()
					int columns1 = md1.getColumnCount()

					logi.logInfo("COLUMS"+columns1)

					while (rs1.next())
					{

						for(int i=1; i<=columns1; ++i)
						{
							if(!(rs1.getObject(i).toString().equals("null")))
							{
								logi.logInfo("putting ;;;;; +"+i+" values in if ")
								all_pay_methods_lhm.put(md1.getColumnName(i).toLowerCase(),rs1.getObject(i))

							}
							else
							{
								logi.logInfo("ELSE OF GET ACCT PAYMENT METHOD value :"+md1.getColumnName(i).equalsIgnoreCase("null"))

							}
						}
					}

					String all_pay_methods_contacts_query=null
					all_pay_methods_contacts_query = "SELECT aa.first_name as bill_first_name,aa.middle_initial as bill_middle_initial,aa.last_name as bill_last_name, aa.company_name as bill_company_name,aa.address1 as bill_address1,aa.address2 as bill_address2,aa.address3 as bill_address3,aa.city as bill_city,aa.locality as bill_locality,aa.state as bill_state_prov,aa.country as bill_country,aa.zip as bill_postal_cd,aa.intl_phone as bill_phone,aa.phone_extension as bill_phone_ext,aa.cell_phone as bill_cell_phone ,aa.work_phone as bill_work_phone,aa.work_phone_extension as bill_work_phone_ext, aa.fax_phone as bill_fax,aa.email as bill_email,TO_CHAR(aa.birthdate,'YYYY-MM-DD') as bill_birthdate FROM  ARIACORE.ACCT_DETAILS ad join  ariacore.acct_address aa on ad.acct_no=aa.acct_no and aa.address_type_cd=1 join ariacore.billing_info bi on bi.bill_address_seq=aa.address_seq join ariacore.acct_billing_group abg on abg.PRIMARY_PMT_METHOD_SEQ=bi.SEQ_NUM and bi.acct_no=abg.acct_no and bi.client_no=abg.client_no and source_no!=0 and aa.acct_no="+acct_no+"  order by 1"

					ResultSet rs2=dbb.executePlaQuery(all_pay_methods_contacts_query);

					ResultSetMetaData md2 = rs2.getMetaData()
					int columns2 = md2.getColumnCount()

					logi.logInfo("COLUMNS"+columns2)

					while (rs2.next())
					{

						for(int i=1; i<=columns2; ++i)
						{
							if(!(rs2.getObject(i).toString().equals("null")))
							{
								logi.logInfo("putting ;;;;; +"+i+" values in if ")
								all_pay_methods_lhm.put(md2.getColumnName(i).toLowerCase(),rs2.getObject(i))

							}
							else
							{
								logi.logInfo("ELSE OF GET ACCT PAYMENT METHOD value :"+md2.getColumnName(i).equalsIgnoreCase("null"))

							}
						}
					}

					logi.logInfo(" hashhhhhhh ***  :"+all_pay_methods_lhm)
					all_pay_methods_lhm=all_pay_methods_lhm.sort()
					h1.put("account_payment_methods_row["+ q +"]",all_pay_methods_lhm)
					++q
				}
	
			}
		}
		logi.logInfo(" GET ALL PAY METHODS ******* "+h1)

		//return all_pay_methods_lhm.sort()
		h1.sort()

		return h1

	}



						/**
						 * TO get the payment history for the given account number
						 * @param acct_hierarchy
						 * @return all the fields in the response of md_get_acct_payment_history_from_db_m
						 */
						public LinkedHashMap md_get_acct_payment_history_from_db_m (String acct_hierarchy1)
						{
							def client_no=Constant.mycontext.expand('${Properties#Client_No}')
							logi.logInfo("into md_get_acct_payment_history_from_db_m")
						String acct_hierarchy=acct_hierarchy1.split("#")[0]
						String seq=acct_hierarchy1.split("#")[1]
							logi.logInfo("AAAAAAAAAAAAAAAAAAAAAAAAA"+acct_hierarchy)
							
								ConnectDB dbb = new ConnectDB()
					
							InputMethods im=new InputMethods()
					
							String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
							logi.logInfo("acct_no"+acct_no)
					
							//String Query="SELECT PAYMENT_TRANS_EVENT_NO  AS TRANSACTION_ID,STATUS_TEXT AS PAYMENT_STATUS,TO_CHAR(PAYMENT_DATE, 'YYYY-MM-DD') AS PAYMENT_DATE,PAYMENT_TRANS_TYPE,CURRENCY_CD AS PAYMENT_CURRENCY,AMOUNT AS PAYMENT_AMOUNT,LEFT_TO_APPLY AS PAYMENT_AMOUNT_LEFT_TO_APPLY,PAY_SOURCE AS CC_SUFFIX,PAY_METHOD AS PAY_METHOD_TYPE,PROC_STATUS_CODE,PROC_PYMNT_ID AS PROC_PAYMENT_ID,AUTH_CODE AS PROC_AUTH_CODE,TO_CHAR(UPDATE_DATE, 'YYYY-MM-DD HH:MI:SSxFF') AS PAYMENT_TIMESTAMP,PROC_STATUS_TEXT,PAY_METHOD  AS PAYMENT_METHOD_NO,TO_CHAR(PAYMENT_DATE, 'YYYY-MM-DD HH:MI:SSxFF') AS PAYMENT_RECIEVED_DATE FROM ARIACORE.ALL_PAYMENTS WHERE ACCT_NO="+acct_no+" and rownum=1 order by transaction_id desc"
							  //String Query="SELECT * FROM (SELECT PAYMENT_TRANS_EVENT_NO  AS TRANSACTION_ID,STATUS_TEXT AS PAYMENT_STATUS,TO_CHAR(PAYMENT_DATE, 'YYYY-MM-DD') AS PAYMENT_DATE,PAYMENT_TRANS_TYPE,CURRENCY_CD AS PAYMENT_CURRENCY,AMOUNT AS PAYMENT_AMOUNT,LEFT_TO_APPLY AS PAYMENT_AMOUNT_LEFT_TO_APPLY,PAY_SOURCE AS CC_SUFFIX,PAY_METHOD AS PAY_METHOD_TYPE,PROC_STATUS_CODE,PROC_PYMNT_ID AS PROC_PAYMENT_ID,AUTH_CODE AS PROC_AUTH_CODE,TO_CHAR(UPDATE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS PAYMENT_TIMESTAMP,PROC_STATUS_TEXT,PAY_METHOD  AS PAYMENT_METHOD_NO,TO_CHAR(PAYMENT_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS PAYMENT_RECEIVED_DATE FROM ARIACORE.ALL_PAYMENTS WHERE ACCT_NO="+acct_no+" order by transaction_id desc) WHERE ROWNUM=1"
							// todayString Query="SELECT B.TRANSACTION_ID,B.PAYMENT_STATUS,B.PAYMENT_DATE,B.PAYMENT_TRANS_TYPE,B.PROC_STATUS_TEXT,B.PAYMENT_CURRENCY,B.PAYMENT_AMOUNT,B.PAYMENT_AMOUNT_LEFT_TO_APPLY,B.CC_SUFFIX,B.PAY_METHOD_TYPE, B.PROC_PAYMENT_ID,B.PROC_AUTH_CODE,B.PAYMENT_TIMESTAMP,B.PAYMENT_METHOD_NO FROM (SELECT ROW_NUMBER() OVER(ORDER BY  A.LEFT_TO_APPLY) as seq ,A.TRANSACTION_ID,A.PAYMENT_STATUS,A.PROC_STATUS_TEXT,A.PAYMENT_DATE,A.PAYMENT_TRANS_TYPE,A.PAYMENT_CURRENCY,A.PAYMENT_AMOUNT,A.PAYMENT_AMOUNT_LEFT_TO_APPLY,A.CC_SUFFIX,A.PAY_METHOD_TYPE, A.PROC_PAYMENT_ID,A.PROC_AUTH_CODE,A.PAYMENT_TIMESTAMP,A.PAYMENT_METHOD_NO  FROM (SELECT PAYMENT_TRANS_EVENT_NO  AS TRANSACTION_ID,STATUS_TEXT AS PAYMENT_STATUS,TO_CHAR(PAYMENT_DATE, 'YYYY-MM-DD') AS PAYMENT_DATE,PAYMENT_TRANS_TYPE,CURRENCY_CD   AS PAYMENT_CURRENCY,AMOUNT AS PAYMENT_AMOUNT,LEFT_TO_APPLY AS PAYMENT_AMOUNT_LEFT_TO_APPLY,PAY_SOURCE    AS CC_SUFFIX,PAY_METHOD AS PAY_METHOD_TYPE,PROC_STATUS_CODE,PROC_PYMNT_ID AS PROC_PAYMENT_ID,AUTH_CODE AS PROC_AUTH_CODE,TO_CHAR(UPDATE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS PAYMENT_TIMESTAMP,PROC_STATUS_TEXT,CASE WHEN PAY_METHOD='-2' THEN NULL ELSE PAY_METHOD END AS PAYMENT_METHOD_NO , LEFT_TO_APPLY,TO_CHAR(PAYMENT_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS PAYMENT_RECEIVED_DATE FROM ARIACORE.ALL_PAYMENTS WHERE ACCT_NO="+acct_no+" ORDER BY transaction_id DESC )A)B WHERE B.SEQ="+seq
												String Query="SELECT B.TRANSACTION_ID,B.PAYMENT_TIMESTAMP as PAYMENT_RECEIVED_DATE,B.PROC_STATUS_CODE,B.PAYMENT_STATUS,B.PAYMENT_DATE,B.PAYMENT_TRANS_TYPE,B.PROC_STATUS_TEXT,B.PAYMENT_CURRENCY,B.PAYMENT_AMOUNT,B.PAYMENT_AMOUNT_LEFT_TO_APPLY,B.CC_SUFFIX,B.PAY_METHOD_TYPE, B.PROC_PAYMENT_ID,B.PROC_AUTH_CODE,B.PAYMENT_TIMESTAMP,B.PAYMENT_METHOD_NO,B.VOIDING_EVENT_NO FROM (SELECT ROW_NUMBER() OVER(ORDER BY  A.LEFT_TO_APPLY) as seq ,A.TRANSACTION_ID,A.PAYMENT_STATUS,A.PROC_STATUS_TEXT,A.PROC_STATUS_CODE,A.PAYMENT_DATE,A.PAYMENT_TRANS_TYPE,A.PAYMENT_CURRENCY,A.PAYMENT_AMOUNT,A.PAYMENT_AMOUNT_LEFT_TO_APPLY,A.CC_SUFFIX,A.PAY_METHOD_TYPE,A.PAYMENT_RECEIVED_DATE, A.PROC_PAYMENT_ID,A.PROC_AUTH_CODE,A.PAYMENT_TIMESTAMP,A.PAYMENT_METHOD_NO,A.VOIDING_EVENT_NO  FROM (SELECT PAYMENT_TRANS_EVENT_NO  AS TRANSACTION_ID,STATUS_TEXT AS PAYMENT_STATUS,TO_CHAR(PAYMENT_DATE, 'YYYY-MM-DD') AS PAYMENT_DATE,PAYMENT_TRANS_TYPE,CURRENCY_CD   AS PAYMENT_CURRENCY,AMOUNT AS PAYMENT_AMOUNT,LEFT_TO_APPLY AS PAYMENT_AMOUNT_LEFT_TO_APPLY,VOIDING_EVENT_NO, PAY_SOURCE    AS CC_SUFFIX,PAY_METHOD AS PAY_METHOD_TYPE,PROC_STATUS_CODE,PROC_PYMNT_ID AS PROC_PAYMENT_ID,AUTH_CODE AS PROC_AUTH_CODE,TO_CHAR(UPDATE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS PAYMENT_TIMESTAMP,PROC_STATUS_TEXT,CASE WHEN PAY_METHOD='-2' THEN NULL ELSE BILL_SEQ_NUM END AS PAYMENT_METHOD_NO , LEFT_TO_APPLY,TO_CHAR(PAYMENT_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS PAYMENT_RECEIVED_DATE FROM ARIACORE.ALL_PAYMENTS WHERE ACCT_NO="+acct_no+" ORDER BY transaction_id DESC )A)B WHERE B.SEQ="+seq
									ResultSet rs = dbb.executePlaQuery(Query)
							ResultSetMetaData md = rs.getMetaData()
							int columns = md.getColumnCount()
					
							LinkedHashMap pay_history_hm = new LinkedHashMap();
							while (rs.next())
							{
					
								for(int i=1; i<=columns; ++i)
								{
									//pay_history_hm.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
									if(!(rs.getObject(i).toString().equalsIgnoreCase("null")))
									{
										pay_history_hm.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
									}
								}
								//
							}
					
					
							logi.logInfo("PAY_HISTORY_HASHMAP  ******************** "+pay_history_hm)
							//Collections.sort(list)
							pay_history_hm=pay_history_hm.sort()
							return pay_history_hm.sort()
						}
					
						String md_get_coupon_count_m(String acct_hierarchy)
						{
							def client_no=Constant.mycontext.expand('${Properties#Client_No}')
							logi.logInfo("into md_get_coupon_count_m")
							ConnectDB dbb = new ConnectDB()
					
							InputMethods im=new InputMethods()
					
							String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
							logi.logInfo("acct_no"+acct_no)
							String Query="SELECT COUNT(COUPON_CD) FROM ARIACORE.ACCT_COUPONS WHERE ACCT_NO="+acct_no+" and client_no"+client_no
							String count = dbb.executeQueryP2(Query)
					
							return count
					
						}
			
				   
		   



			
				   
				   /*************GET_ACCT_DETAILS_ALL_M**************/
				   
				   /**To get overall acct details through hash value
					*
					* @return acct_details
					*/
				   public LinkedHashMap md_GET_ACCT_DETAILS_ALL_m (String str)
	{
		InputMethods im = new InputMethods()
		
		String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
		  logi.logInfo("acct_no : "+acct_no)
		 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		String ss = "test"
		LinkedHashMap row = new LinkedHashMap();
		String acct_count = "SELECT count(*) as counts FROM ariacore.acct a JOIN ariacore.acct_address ad ON a.acct_address_seq = ad.address_seq WHERE a.client_no = "+client_no+" AND a.acct_no = "+acct_no
		String c = db.executeQueryP2(acct_count).toString()
		
		if(c.equals("0")){
			
				String query_acct = 	"SELECT a.acct_no AS acct_no,"+
										"  a.userid       AS userid,"+
										 " a.CLIENT_ACCT_ID,"+
										 " a.consumer_acct_ind,"+
										" a.status_cd,"+
										"  a.notify_method,"+
										"  a.senior_acct_no,"+
										"  a.TEST_ACCT_IND,a.taxpayer_id,"+
										 " a.SEQ_FUNC_GROUP_NO,"+
										"  a.INV_APPROVAL_REQ_IND                  AS invoice_approval_required,"+
										"  TO_CHAR(a.ACCT_START_DATE,'YYYY-MM-DD') AS acct_start_date,"+
										"  a.CURRENCY_CD                           AS acct_currency,"+
										"  a.acct_balance,a.LOCALE_NO as acct_locale_no,"+
										"  a.ACCT_CREATE_API_RECEIPT_ID AS acct_create_client_receipt_id "+
										 " FROM ariacore.acct a "+
										" WHERE a.client_no =  "+client_no+
										" AND a.acct_no     = "+acct_no
									ResultSet rs1 = db.executePlaQuery(query_acct);
						ResultSetMetaData md1 = rs1.getMetaData();
						 int columns = md1.getColumnCount();
						 
						 logi.logInfo("executed query : "+query_acct )
						 while (rs1.next()){
						
							 for(int j=1; j<=columns; ++j){
								if(rs1.getObject(j).toString().equals("null")){
								//	row.put(md1.getColumnName(j).toLowerCase(),"");
								}else{
									 
									 logi.logInfo("pop up : "+md1.getColumnName(j) )
							  row.put(md1.getColumnName(j).toLowerCase(),rs1.getObject(j));
								 }
							  logi.logInfo("log getobject : "+rs1.getObject(j) )
							 }
							 
							 
						 }
						 
						 
						 String query_addr = "select a.first_name ,a.middle_initial,a.last_name,a.company_name,a.address1,a.address2,a.address3,a.city,a.locality,a.STATE as state_prov,"+
							"a.country as country_cd,a.zip as postal_cd,a.intl_phone as phone,a.phone_extension as phone_ext,a.cell_phone,a.work_phone,a.work_phone_extension as work_phone_ext,"+
							"a.fax_phone as fax,a.email,"+
							"TO_CHAR(a.birthdate,'YYYY-MM-DD') AS BIRTHDATE from ariacore.acct_address a where acct_no = "+acct_no
							logi.logInfo("executing primodial : "+query_addr )
							ResultSet rs2 = db.executePlaQuery(query_addr);
							logi.logInfo("executing result set value : "+rs2.toString() )
							ResultSetMetaData md2 = rs2.getMetaData();
							 int columns2 = md2.getColumnCount();
							 logi.logInfo("column counts : "+columns2 )
							
								 
									  for(int j=1; j<=columns2; ++j){
										//  row.put(md1.getColumnName(j).toLowerCase(),"");
										  logi.logInfo("getting column name : "+md1.getColumnName(j).toLowerCase() )
									  }
									  
									 
						
						 logi.logInfo("before returning hash : "+row )
						
				
		
			  }
		else{
			 
		
		
		String Query = "SELECT a.acct_no AS acct_no,"+
						"  a.userid       AS userid,"+
						"  a.CLIENT_ACCT_ID,"+
						"  a.consumer_acct_ind,"+
						"  ad.first_name,"+
						"  ad.middle_initial,"+
						"  ad.last_name,"+
						"  ad.company_name,"+
						"  ad.address1,"+
						"  ad.address2,"+
						"  ad.address3,"+
						"  ad.CITY,"+
						"  ad.locality,"+
						"  ad.STATE AS state_prov,"+
						"  ad.zip as postal_cd,"+
						"  ad.country         AS country_cd,"+
						"  ad.PHONE_EXTENSION AS phone_ext,"+
						"  ad.INTL_PHONE      AS phone,"+
						"  ad.EMAIL,"+
						"  ad.WORK_PHONE_EXTENSION            AS work_phone_ext,"+
						"  TO_CHAR(AD.BIRTHDATE,'YYYY-MM-DD') AS BIRTHDATE,"+
						"  ad.FAX_PHONE                       AS fax,a.taxpayer_id,"+
						"  ad.CELL_PHONE,"+
						"  ad.WORK_PHONE,"+
						"  a.status_cd,"+
						"  a.notify_method,"+
						"  a.senior_acct_no,"+
						"  a.TEST_ACCT_IND,"+
						"  a.SEQ_FUNC_GROUP_NO,"+
						"  a.INV_APPROVAL_REQ_IND                  AS invoice_approval_required,"+
						"  TO_CHAR(a.ACCT_START_DATE,'YYYY-MM-DD') AS acct_start_date,"+
						"  a.CURRENCY_CD                           AS acct_currency,"+
						"  a.acct_balance,a.LOCALE_NO as acct_locale_no,"+
						"  a.ACCT_CREATE_API_RECEIPT_ID AS acct_create_client_receipt_id"+
						" FROM ariacore.acct a"+
						" JOIN ariacore.acct_address ad "+
						" ON a.acct_address_seq = ad.address_seq "+
						" WHERE a.client_no =  "+ client_no +
						" AND a.acct_no     = "+ acct_no
							
							
			ResultSet rs = db.executePlaQuery(Query)
			ResultSetMetaData md = rs.getMetaData();
			 int columns = md.getColumnCount();
			
			
			 while (rs.next()){
				 
				 for(int j=1; j<=columns; ++j){
								if(rs.getObject(j).toString().equals("null")){
								//	row.put(md.getColumnName(j).toLowerCase(),"");
								}else{
									 
									 logi.logInfo("pop up : "+md.getColumnName(j) )
									 
									 
							  row.put(md.getColumnName(j).toLowerCase(),rs.getObject(j));
								 }
							  
							  
							  logi.logInfo("log getobject : "+rs.getObject(j) )
							 }
		
			
			 }
			 }
		logi.logInfo("exited loop ")
		String senior_acct = (String)row.get("senior_acct_no")
		logi.logInfo("fetched senior acct : "+senior_acct)
		if(senior_acct.equals("")){
	//		row.put("senior_acct_user_id","");
	//		row.put("senior_client_acct_id","");
		   
		}else{
		logi.logInfo("senior acct present ")
		String senior_query = "select USERID as senior_acct_user_id,CLIENT_ACCT_ID as senior_client_acct_id from ariacore.acct_details where acct_no = "+ senior_acct
		ResultSet rs3 = db.executePlaQuery(senior_query)
		ResultSetMetaData md3 = rs3.getMetaData()
		logi.logInfo("result set values " +rs3)
		int senior_col = md3.getColumnCount()
		logi.logInfo("senior acct detilas columns : " +senior_col)
		while(rs3.next()){
			logi.logInfo("entered loop ")
			for(int i = 1;i<=senior_col;i++){
				if(rs3.getObject(i).toString().equals("null")){
					logi.logInfo("null value in column : "+rs3.getObject(i).toString() )
				//	row.put(md3.getColumnName(i).toLowerCase(),"");
				}
				else{
					logi.logInfo("insert to : "+md3.getColumnName(i) )
					row.put(md3.getColumnName(i).toLowerCase(),rs3.getObject(i));
				}
			}
		   
		}
		logi.logInfo("exited loop and proceeding to return ")
		}
		
		String alt_template_id = "SELECT aot.alt_msg_template_no,et.CLIENT_EMAIL_TEMPLATE_ID AS client_alt_msg_template_id from ariacore.acct_override_template aot LEFT JOIN ariacore.email_templates et on aot.alt_msg_template_no = et.template_no  where  aot.acct_no=  "+acct_no+" and aot.template_class = 'I' and aot.client_no = "+client_no+""
		/*String alt_template_id = "SELECT aot.alt_msg_template_no, et.CLIENT_EMAIL_TEMPLATE_ID as client_alt_msg_template_id FROM ariacore.acct a JOIN ariacore.acct_address ad ON a.acct_address_seq = ad.address_seq "+
		" join ariacore.acct_override_template aot on aot.acct_no = ad.acct_no "+
		" join ariacore.email_templates et on et.template_no = aot.alt_msg_template_no and et.CUSTOM_TO_CLIENT_NO = a.client_no and et.template_class = 'I' "+
		" WHERE a.client_no = "+client_no+" AND a.acct_no = " +acct_no*/
		ResultSet rs5 = db.executeQuery(alt_template_id)
		ResultSetMetaData md4 = rs5.getMetaData()
		logi.logInfo("meta data value" + md4)
		int columns5 = md4.getColumnCount()
		logi.logInfo("meta data value" + columns5)
		if(!rs5.isBeforeFirst()){
			logi.logInfo "no rows"
	//		row.put("client_alt_msg_template_id","")
	//		row.put("alt_msg_template_no","")
			
		}else{
		
		while(rs5.next()){
			logi.logInfo("inside while loop of statement")
			for(int i = 1;i<=columns5;i++){
				if(rs5.getObject(i).toString().equals("null")||rs5.getObject(i).toString()==null){
					logi.logInfo("null value in column : "+rs5.getObject(i).toString() )
	//				row.put(md4.getColumnName(i).toLowerCase(),"");
				}
				else{
					logi.logInfo("insert to : "+md4.getColumnName(i) )
					row.put(md4.getColumnName(i).toLowerCase(),rs5.getObject(i));
				}
			}
		}
		}
		
		String revrec = "SELECT A.PROFILE_ID AS revrec_profile_no,B.CLIENT_PROFILE_ID AS client_revrec_id FROM ARIACORE.ACCT_REVREC_PROFILE A JOIN REVREC.CLIENT_REVREC_PROFILE B ON A.PROFILE_ID = B.PROFILE_ID AND A.CLIENT_NO = B.CLIENT_NO WHERE A.ACCT_NO =" +acct_no+" and a.status = '1'"
		   ResultSet rs6 = db.executeQuery(revrec)
		   ResultSetMetaData md5 = rs6.getMetaData()
		   logi.logInfo("meta data value" + md5)
		   int columns6 = md5.getColumnCount()
		   logi.logInfo("meta data value" + columns6)
		   if(!rs6.isBeforeFirst()){
			   logi.logInfo "no rows"
		   }else{
		   
		   while(rs6.next()){
			   logi.logInfo("inside while loop of statement")
			   for(int i = 1;i<=columns6;i++){
				   if(rs6.getObject(i).toString().equals("null")||rs6.getObject(i).toString()==null){
					   logi.logInfo("null value in column : "+rs6.getObject(i).toString() )
				   }
				   else{
					   logi.logInfo("insert to : "+md5.getColumnName(i) )
					   row.put(md5.getColumnName(i).toLowerCase(),rs6.getObject(i));
				   }
			   }
		   }
		   }
		String cn_template_id = "SELECT et.CLIENT_EMAIL_TEMPLATE_ID FROM ariacore.acct a JOIN ariacore.acct_address ad ON a.acct_address_seq = ad.address_seq "+
								" join ariacore.acct_override_template aot on aot.acct_no = ad.acct_no "+
								" join ariacore.email_templates et on et.template_no = aot.alt_msg_template_no and et.CUSTOM_TO_CLIENT_NO = a.client_no and et.template_class = 'CN' "+
								" WHERE a.client_no = "+client_no+" AND a.acct_no = " +acct_no
			 String cn_alt_msg = db.executeQueryP2(cn_template_id).toString()
			 if(cn_alt_msg.equals("null")||cn_alt_msg == null){
				logi.logInfo ("cn_alt_msg id is null")
	//			  row.put("client_cn_alt_msg_template_id","")
				  logi.logInfo "null value replacement has been done in 2 check"
			 }else{
			 logi.logInfo "cn_alt_msg id has values"
				 row.put("client_cn_alt_msg_template_id",cn_alt_msg)
				 logi.logInfo "value has been placed at client cn alt msg"
			 }
			 
			String mp_count = "SELECT COUNT(*) FROM ariacore.plan_instance pi, ariacore.client_plan cp WHERE pi.client_no = "+client_no+" AND pi.acct_no = "+acct_no+"  AND cp.client_no = pi.client_no AND cp.plan_no = pi.plan_no AND cp.status_cd = 1 AND cp.supp_plan_ind = 0 AND pi.parent_plan_instance_no IS NULL";
			String master_plan_count1 = db.executeQueryP2(mp_count).toString()
			if(master_plan_count1.equals("null")||master_plan_count1 == null){
			 logi.logInfo ("master_plan_count1 is null")
			logi.logInfo "null value replacement has been done in 2 check"
			}else{
			logi.logInfo "master_plan_count1 has values"
			row.put("master_plan_count",master_plan_count1)
			logi.logInfo "value has been placed at master_plan_count"
			}
			
			String sp_count = "SELECT COUNT(*) FROM ariacore.plan_instance pi, ariacore.client_plan cp WHERE pi.client_no = "+client_no+" AND pi.acct_no = "+acct_no+"  AND cp.client_no = pi.client_no AND cp.plan_no = pi.plan_no AND cp.status_cd = 1 AND cp.supp_plan_ind = 1 AND pi.parent_plan_instance_no IS NOT NULL";
			String supp_plan_count1 = db.executeQueryP2(sp_count).toString()
			if(supp_plan_count1.equals("null")||supp_plan_count1 == null){
			 logi.logInfo ("supp_plan_count1 is null")
			logi.logInfo "null value replacement has been done in 2 check"
			}else{
			logi.logInfo "supp_plan_count1 has values"
			row.put("supp_plan_count",supp_plan_count1)
			logi.logInfo "value has been placed at supp_plan_count"
			}
						 
			 String tax_acct_exempt = "SELECT EXEMPT_FROM FROM ARIACORE.TAX_ACCT_EXEMPT WHERE ACCT_NO = "+acct_no+" AND CLIENT_NO = " +client_no
			 String exempt_from = db.executeQueryP2(tax_acct_exempt).toString()
			 if(exempt_from.equals("null")||exempt_from == null){
			 logi.logInfo ("exempt_from id is null")
			 //			  row.put("client_cn_alt_msg_template_id","")
			 logi.logInfo "null value"
			 }else{
			 logi.logInfo "exempt_from id has values"
			 row.put("tax_exemption_level",exempt_from)
			 logi.logInfo "value has been placed at tax exempt_level"
			 }
			 
			 
			 
			 logi.logInfo("in get_acct_details_all_m "+row)
			 row=row.sort()
			return row
		
	}

				   
				  
				   
				   
				   
				   /**
					* @param Payment method row information
					* @param acct_no and row_sequence number(i)
					* @return payment method row
					*/
	
	/**
	 * @param Payment method row information
	 * @param acct_no and row_sequence number(i)
	 * @return payment method row
	 */
	public LinkedHashMap md_Get_payment_method_row (String acct_no,int i){
	
			logi.logInfo("Entering payment method row info")
		ConnectDB db = new ConnectDB()
		InputMethods im = new InputMethods()
		
		  logi.logInfo("acct_no for pay method group "+acct_no)
		LinkedHashMap<String,String> pay_row = new LinkedHashMap<String,String>()
		
		String query = "select pa.billing_group_no,pa.payment_method_no,pa.bill_first_name,pa.bill_middle_initial,pa.bill_last_name,pa.bill_company_name,pa.bill_address1,"+
						" pa.bill_address2,pa.bill_address3,pa.bill_city,pa.bill_agreement_id,pa.bill_locality,pa.bill_state_prov,pa.bill_country,pa.bill_postal_cd,pa.bill_phone,pa.bill_phone_ext,"+
						" pa.bill_cell_phone,pa.bill_work_phone,pa.bill_work_phone_ext,pa.BANK_ROUTING_NUM,pa.client_payment_method_id,"+
						" pa.bill_fax,pa.bill_email,pa.bill_birthdate,pa.pay_method_name,pa.pay_method_description,pa.pay_method_type,pa.suffix,pa.cc_expire_mm,pa.cc_expire_yyyy,pa.cc_id,pa.bill_contact_no "+
						" from(select aa.billing_group_no,b.seq_num as payment_method_no,ad.first_name as bill_first_name,ad.middle_initial as bill_middle_initial,ad.last_name as bill_last_name,"+
						" ad.company_name as bill_company_name,ad.address1 as bill_address1,ad.address2 as bill_address2,ad.address3 as bill_address3,ad.city as bill_city,"+
						" ad.locality as bill_locality,ad.state as bill_state_prov,ad.address_seq as bill_contact_no,ad.country as bill_country,ad.zip as bill_postal_cd,ad.INTL_PHONE as bill_phone,"+
						" ad.phone_extension as bill_phone_ext,ad.cell_phone as bill_cell_phone,b.BANK_ROUTING_NUM,b.agreement_id as bill_agreement_id,"+
						" ad.work_phone as bill_work_phone,ad.WORK_PHONE_EXTENSION as bill_work_phone_ext,ad.fax_phone as bill_fax,ad.email as bill_email,TO_char(ad.birthdate,'YYYY-MM-DD') as bill_birthdate,"+
						" b.PAYMENT_METHOD_NAME as pay_method_name,b.payment_method_description as pay_method_description,b.pay_method as pay_method_type,b.suffix,b.CC_EXPIRE_MM,b.CC_EXPIRE_YYYY,b.cc_id,b.PAYMENT_METHOD_CDID   AS client_payment_method_id,row_number() over(order by b.seq_num) as seq "+
						" from ariacore.acct_billing_group aa join ariacore.billing_info b on ( aa.PRIMARY_PMT_METHOD_SEQ = b.seq_num or aa.SECONDARY_PMT_METHOD_SEQ  = b.seq_num ) and aa.acct_no = b.acct_no "+
						" left join ariacore.acct_address ad on b.BILL_ADDRESS_SEQ = ad.ADDRESS_SEQ and b.acct_no = ad.acct_no "+
						" where aa.acct_no = "+acct_no+")pa where pa.seq = "+i
						ResultSet rs1 = db.executePlaQuery(query);
						ResultSetMetaData md1 = rs1.getMetaData();
						int columns1 = md1.getColumnCount();
						while (rs1.next()){
							
								 for(int s=1; s<=columns1; ++s){
									 if(rs1.getObject(s).toString().equals("null")){
										 logi.logInfo("No value in "+rs1.getObject(s))
									 }else{logi.logInfo("Value inserting in hash map")
									 pay_row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
									 logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
								 }
						}
						
					
						
						String cc_id = (String)pay_row.get("cc_id")
						if(!cc_id.equals("null")){
							String cc_type_db = "select cc_type from ariacore.credit_cards where cc_id = "+cc_id
							String cc_type = db.executeQueryP2(cc_type_db).toString()
							pay_row.put("cc_type",cc_type)
							pay_row.remove("cc_id")
							String cc_type_check = (String)pay_row.get("cc_type")
							if(cc_type_check.equals("null")){
								pay_row.remove("cc_type")
							}
							return pay_row.sort()
							
						 }else{
						 pay_row.remove("cc_id")
						 String cc_type = (String)pay_row.get("cc_type")
						 if(cc_type.equals("null")){
							 pay_row.remove("cc_type")
						 }
						 return pay_row.sort()
						 }
						
		}
	/** main method to get payment method (inclue billing group and payment method number must be 1 in input)
	 * @param account number param value can be given from response sheet
	 * @return returns all the row of payment method
	 */
	public LinkedHashMap md_GET_PAYMENT_METHOD_m (String str){
	
		ConnectDB db = new ConnectDB()
		InputMethods im = new InputMethods()
		String payment_info = getValueFromRequest("get_acct_details_all_m",ExPathRpcEnc.payment_methods);logi.logInfo("Supp Plans : "+payment_info)
		String billing_info = getValueFromRequest("get_acct_details_all_m",ExPathRpcEnc.Billing_info);logi.logInfo("Supp Plans : "+billing_info)
		LinkedHashMap row = new LinkedHashMap<String , HashMap<String,String>>()
		LinkedHashMap row1 = new LinkedHashMap<String , HashMap<String,String>>()
		String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
		if(billing_info.equals("1")&&payment_info.equals("1")){
			String query = "select count(*) as counts from ariacore.acct_billing_group aa join ariacore.billing_info b on ( aa.PRIMARY_PMT_METHOD_SEQ = b.seq_num or aa.SECONDARY_PMT_METHOD_SEQ  = b.seq_num ) and aa.acct_no = b.acct_no where aa.acct_no = " +acct_no
			String pcount = db.executeQueryP2(query).toString()
			int pay_count = Integer.parseInt(pcount)
			if(pay_count>0){
				for(int i=1;i<=pay_count;i++){int s =i-1;
					logi.logInfo("counting"+s)
				row.put("payment_methods_info_row["+s+"]",md_Get_payment_method_row(acct_no,i))
				}
			return row.sort()
			}
			else{logi.logInfo("no payment method values")
				return row1}
		}
		else{
			logi.logInfo("If billing group is 0, error:include billing group to display payment method")
			return row1
		}
	}
		public LinkedHashMap md_Master_Plan_instance_m(String str){
			ConnectDB db = new ConnectDB()
			//String supp_plans = getValueFromRequest("get_acct_details_all_m",ExPathRpcEnc.Supp_plans);logi.logInfo("Supp Plans : "+supp_plans)
			String client_no = Constant.client_no
			InputMethods im =new InputMethods()
			LinkedHashMap row = new LinkedHashMap<String , HashMap<String,String>>()
			String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
			String resp_level_cd = "Select resp_level_cd from ariacore.acct where acct_no = "+acct_no+"";
			int resp_lev_cd = db.executeQueryP2(resp_level_cd)
			logi.logInfo("Resp level code value"+resp_lev_cd)
			String query_count = null;
			if (resp_lev_cd==1){
			query_count = "Select count(*) as counts from ariacore.plan_instance  PI join ariacore.client_plan CP ON PI.plan_no=CP.plan_no and pi.client_no=cp.client_no join "+
										  " ariacore.plan_rate_schedule prs on pi.plan_no=prs.plan_no and pi.schedule_no = prs.SCHEDULE_NO AND pi.client_no   =prs.client_no "+
										  " join ariacore.plan_instance_master pim on pim.client_no=pi.client_no and pim.plan_instance_no=pi.plan_instance_no "+
										  " join ariacore.acct_dunning_group  adg on adg.acct_no=pi.acct_no and adg.dunning_group_no = pim.dunning_group_no and adg.client_no = pim.client_no "+
										  " where pi.acct_no="+acct_no+" and pi.client_no="+client_no
			}else{
			query_count = "Select count(*) as counts from ariacore.plan_instance  PI join ariacore.client_plan CP ON PI.plan_no=CP.plan_no and pi.client_no=cp.client_no join "+
			" ariacore.plan_rate_schedule prs on pi.plan_no=prs.plan_no and pi.schedule_no = prs.SCHEDULE_NO AND pi.client_no   =prs.client_no "+
			" join ariacore.plan_instance_master pim on pim.client_no=pi.client_no and pim.plan_instance_no=pi.plan_instance_no "+
			" join ariacore.acct_dunning_group  adg on adg.acct_no=pi.acct_no and adg.client_no = pim.client_no "+
			" where pi.acct_no="+acct_no+" and pi.client_no="+client_no
			}
										  String plancount = db.executeQueryP2(query_count).toString()
										  int plan_count = Integer.parseInt(plancount)
										  logi.logInfo("value converted to integer : "+plan_count)
			for(int i=1;i<=plan_count;i++){
				  logi.logInfo("entered into the loop")
				  int s =i-1;
				  logi.logInfo("counting"+s)
			row.put("master_plans_info_row["+s+"]",md_get_mpi_row(acct_no,i))
			}
			return row.sort()
	  }
						  
					/** to get mpi row values
					* @param acct_no
					* @param i
					* @return returns the mpi instance values
					*/
					public LinkedHashMap md_get_mpi_row(String acct_no, int i){
			ConnectDB db = new ConnectDB()
			LinkedHashMap<String,String> plan_row = new LinkedHashMap<String,String>()
			String client_no = Constant.client_no
			String resp_level_cd = "Select resp_level_cd from ariacore.acct where acct_no = "+acct_no+"";
			String resp_lev_cd = db.executeQueryP2(resp_level_cd)
			logi.logInfo("Resp level code value"+resp_lev_cd)
			String query = null;
		//	if (resp_lev_cd==1){
		   query = "select s.plan_deprovisioned_date,s.dunning_degrade_date,s.master_plan_instance_no,s.Client_Master_Plan_Instance_id,s.client_master_plan_id,s.master_plan_no,s.plan_instance_description,s.Dunning_group_no,"+
										  " s.Client_dunning_group_id,s.dunning_step,s.Dunning_group_name,s.dunning_process_no,s.Billing_group_no,s.client_billing_group_id,"+
										  " s.Master_Plan_instance_Status,s.master_plan_units,s.resp_level_cd,s.alt_rate_schedule_no,"+
										  " s.client_alt_rate_schedule_id,s.promo_cd,s.bill_day,s.last_arrears_bill_thru_date,s.dunning_group_description,s.last_bill_date,s.last_bill_thru_date,s.next_bill_date,s.plan_date,s.status_date,"+
										  " s.master_plan_instance_balance,s.Recurring_billing_interval,s.Usage_billing_interval,s.initial_plan_status,s.Rollover_plan_status ,s.Rollover_plan_status_uom_cd,"+
										  " s.init_free_period_uom_cd,s.Dunning_state,s.parent_master_plan_inst_no from "+
										  " (Select pi.PLAN_INSTANCE_DESCRIPTION as plan_instance_description,adg.DUNNING_GROUP_DESC as dunning_group_description,cp.CLIENT_PLAN_ID as client_master_plan_id,pi.PLAN_INSTANCE_NO as master_plan_instance_no,pi.PLAN_INSTANCE_CDID as Client_Master_Plan_Instance_id,pi.PLAN_NO as master_plan_no,adg.DUNNING_GROUP_NO as Dunning_group_no,"+
										  " adg.DUNNING_PROC_NO as dunning_process_no,adg.DUNNING_GROUP_CDID as Client_dunning_group_id,adg.DUNNING_GROUP_NAME as Dunning_group_name,pim.BILLING_GROUP_NO as Billing_group_no,pi.STATUS_CD as Master_Plan_instance_Status,"+
										  " TO_CHAR(pi.DEPROVISION_DATE,'YYYY-MM-DD') AS plan_deprovisioned_date,pi.PLAN_UNITS as master_plan_units,pim.RESPONSIBILITY_LEVEL_CD as resp_level_cd,pi.SCHEDULE_NO as alt_rate_schedule_no,prs.CLIENT_RATE_SCHEDULE_ID as client_alt_rate_schedule_id, "+
										  " prs.RECUR_BILLING_INTERVAL as Recurring_billing_interval,pim.DUNNING_STEP as dunning_step,prs.USAGE_BILLING_INTERVAL as Usage_billing_interval,cp.ROLLOVER_ACCT_STATUS_CD as Rollover_plan_status ,TO_CHAR(pim.DUNNING_DEGRADE_DATE,'YYYY-MM-DD') AS dunning_degrade_date,  cp.ROLLOVER_INST_STATUS_CD as Rollover_plan_status_uom_cd,"+
										  " cp.INITIAL_FREE_PTYPE as init_free_period_uom_cd,pim.DUNNING_STATE as Dunning_state, pim.promo_cd,pim.bill_day,"+
										  " TO_CHAR(pi.PLAN_DATE,'YYYY-MM-DD') AS PLAN_DATE,"+
										  "  TO_CHAR(pi.LAST_BILL_DATE,'YYYY-MM-DD')                               AS last_bill_date ,"+
										  "  TO_CHAR(pi.NEXT_BILL_DATE,'YYYY-MM-DD')                               AS next_bill_date,"+
										  "  TO_CHAR(pi.LAST_BILL_THRU_DATE,'YYYY-MM-DD')                          AS last_bill_thru_date,abg.profile_cdid as client_billing_group_id,"+
										  "   TO_CHAR(pi.STATUS_DATE,'YYYY-MM-DD')                          AS status_date,cp.DEFAULT_PLAN_INST_STATUS_CD as initial_plan_status,pim.RESPONSIBLE_PLAN_INSTANCE_NO as parent_master_plan_inst_no,"+
										  "   TO_CHAR(pi.LAST_ARREARS_BILL_THRU_DATE,'YYYY-MM-DD')                          AS last_arrears_bill_thru_date,pim.stack_balance as master_plan_instance_balance,"+
										  " row_number() over(order by pi.PLAN_INSTANCE_NO)as seq from ariacore.plan_instance  PI join ariacore.client_plan CP ON PI.plan_no=CP.plan_no and pi.client_no=cp.client_no "+
										  " join ariacore.plan_rate_schedule prs on pi.plan_no=prs.plan_no and pi.schedule_no = prs.SCHEDULE_NO and pi.client_no=prs.client_no "+
										  " join ariacore.plan_instance_master pim on pim.client_no=pi.client_no and pim.plan_instance_no=pi.plan_instance_no "+
										  " left join ariacore.acct_dunning_group  adg on adg.acct_no=pi.acct_no and adg.dunning_group_no = pim.dunning_group_no and adg.client_no = pim.client_no "+
										  " left join ariacore.acct_billing_group abg on abg.acct_no = pi.acct_no and abg.client_no = pi.client_no and abg.billing_group_no = pim.billing_group_no "+
										  " where pi.acct_no="+acct_no+" and pi.client_no="+client_no+")s where seq = "+i
			/*}else
		{	query = "select s.plan_deprovisioned_date,s.dunning_degrade_date,s.master_plan_instance_no,s.Client_Master_Plan_Instance_id,s.client_master_plan_id,s.master_plan_no,s.plan_instance_description,s.Dunning_group_no,"+
			" s.Client_dunning_group_id,s.dunning_step,s.Dunning_group_name,s.dunning_process_no,s.Billing_group_no,s.client_billing_group_id,"+
			" s.Master_Plan_instance_Status,s.master_plan_units,s.resp_level_cd,s.alt_rate_schedule_no,"+
			" s.client_alt_rate_schedule_id,s.promo_cd,s.bill_day,s.last_arrears_bill_thru_date,s.dunning_group_description,s.last_bill_date,s.last_bill_thru_date,s.next_bill_date,s.plan_date,s.status_date,"+
			" s.master_plan_instance_balance,s.Recurring_billing_interval,s.Usage_billing_interval,s.initial_plan_status,s.Rollover_plan_status ,s.Rollover_plan_status_uom_cd,"+
			" s.init_free_period_uom_cd,s.Dunning_state,s.parent_master_plan_inst_no from "+
			" (Select pi.PLAN_INSTANCE_DESCRIPTION as plan_instance_description,adg.DUNNING_GROUP_DESC as dunning_group_description,cp.CLIENT_PLAN_ID as client_master_plan_id,pi.PLAN_INSTANCE_NO as master_plan_instance_no,pi.PLAN_INSTANCE_CDID as Client_Master_Plan_Instance_id,pi.PLAN_NO as master_plan_no,adg.DUNNING_GROUP_NO as Dunning_group_no,"+
			" adg.DUNNING_PROC_NO as dunning_process_no,adg.DUNNING_GROUP_CDID as Client_dunning_group_id,adg.DUNNING_GROUP_NAME as Dunning_group_name,pim.BILLING_GROUP_NO as Billing_group_no,pi.STATUS_CD as Master_Plan_instance_Status,"+
			" TO_CHAR(pi.DEPROVISION_DATE,'YYYY-MM-DD') AS plan_deprovisioned_date,pi.PLAN_UNITS as master_plan_units,pim.RESPONSIBILITY_LEVEL_CD as resp_level_cd,pi.SCHEDULE_NO as alt_rate_schedule_no,prs.CLIENT_RATE_SCHEDULE_ID as client_alt_rate_schedule_id, "+
			" prs.RECUR_BILLING_INTERVAL as Recurring_billing_interval,pim.DUNNING_STEP as dunning_step,prs.USAGE_BILLING_INTERVAL as Usage_billing_interval,cp.ROLLOVER_ACCT_STATUS_CD as Rollover_plan_status ,TO_CHAR(pim.DUNNING_DEGRADE_DATE,'YYYY-MM-DD') AS dunning_degrade_date,  cp.ROLLOVER_INST_STATUS_CD as Rollover_plan_status_uom_cd,"+
			" cp.INITIAL_FREE_PTYPE as init_free_period_uom_cd,pim.DUNNING_STATE as Dunning_state, pim.promo_cd,pim.bill_day,"+
			" TO_CHAR(pi.PLAN_DATE,'YYYY-MM-DD') AS PLAN_DATE,"+
			"  TO_CHAR(pi.LAST_BILL_DATE,'YYYY-MM-DD')                               AS last_bill_date ,"+
			"  TO_CHAR(pi.NEXT_BILL_DATE,'YYYY-MM-DD')                               AS next_bill_date,"+
			"  TO_CHAR(pi.LAST_BILL_THRU_DATE,'YYYY-MM-DD')                          AS last_bill_thru_date,abg.profile_cdid as client_billing_group_id,"+
			"   TO_CHAR(pi.STATUS_DATE,'YYYY-MM-DD')                          AS status_date,cp.DEFAULT_PLAN_INST_STATUS_CD as initial_plan_status,pim.RESPONSIBLE_PLAN_INSTANCE_NO as parent_master_plan_inst_no,"+
			"   TO_CHAR(pi.LAST_ARREARS_BILL_THRU_DATE,'YYYY-MM-DD')                          AS last_arrears_bill_thru_date,pim.stack_balance as master_plan_instance_balance,"+
			" row_number() over(order by pi.PLAN_INSTANCE_NO)as seq from ariacore.plan_instance  PI join ariacore.client_plan CP ON PI.plan_no=CP.plan_no and pi.client_no=cp.client_no "+
			" join ariacore.plan_rate_schedule prs on pi.plan_no=prs.plan_no and pi.schedule_no = prs.SCHEDULE_NO and pi.client_no=prs.client_no "+
			" join ariacore.plan_instance_master pim on pim.client_no=pi.client_no and pim.plan_instance_no=pi.plan_instance_no "+
			" join ariacore.acct_dunning_group  adg on adg.acct_no=pi.acct_no and adg.client_no = pim.client_no "+
			" join ariacore.acct_billing_group abg on abg.acct_no = pi.acct_no and abg.client_no = pi.client_no "+
			" where pi.acct_no="+acct_no+" and pi.client_no="+client_no+")s where seq = "+i
		}												  */
										  ResultSet rs1 = db.executePlaQuery(query);
										  ResultSetMetaData md1 = rs1.getMetaData();
										  int columns1 = md1.getColumnCount();
										  while (rs1.next()){
												
													  for(int s=1; s<=columns1; ++s){
														   /* if(md1.getColumnName(s).toLowerCase().toString().equals("plan_deprovisioned_date")){
																  if(rs1.getObject(s).toString().equals("null")){
																		plan_row.put("plan_deprovisioned_date","")
																		}
															}*/
															if(rs1.getObject(s).toString().equals("null")){
																  logi.logInfo("No value in "+rs1.getObject(s))
															}else{logi.logInfo("Value inserting in hash map")
															plan_row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
															logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
															
													   }
													  // plan_row.put("master_plan_plan_inst_fields","  ")
													  
													   String master_plan_instance_description = (String)plan_row.get("plan_instance_description")
													  if(master_plan_instance_description.equals("null")||master_plan_instance_description==null){
															plan_row.remove("plan_instance_description")
															  
													   }else{
													  plan_row.put("master_plan_instance_description",master_plan_instance_description)
													  plan_row.remove("plan_instance_description")
													  }
													  String dunning_process_no = (String)plan_row.get("dunning_process_no")
													  if(!dunning_process_no.equals("null")){
															String client_dunning_query = "select DUNNING_PROC_CDID as client_dunning_process_id from ariacore.client_dunning_proc where dunning_proc_no = "+dunning_process_no
															String client_dunning_process_id = db.executeQueryP2(client_dunning_query).toString()
															plan_row.put("client_dunning_process_id",client_dunning_process_id)
													  }
													  
													   String plan_instance_status = (String)plan_row.get("master_plan_instance_status")
													  if(plan_instance_status.equals("1")){
															plan_row.put("mp_instance_status_label","Active")
													  }
													  if(plan_instance_status.equals("0")){
														  plan_row.put("mp_instance_status_label","Inactive")
													}
													  if(plan_instance_status.equals("41")){
															plan_row.put("mp_instance_status_label","Trial")
													  }
													  if(plan_instance_status.equals("61")){
															plan_row.put("mp_instance_status_label","Active Non-Billable")
													  }
													  if(plan_instance_status.equals("-1")){
														  plan_row.put("mp_instance_status_label","Suspended")
													}
													  if(plan_instance_status.equals("-2")){
														  plan_row.put("mp_instance_status_label","Cancelled")
													}
													  if(plan_instance_status.equals("32")){
															plan_row.put("mp_instance_status_label","Pending Activation")
													  }
													  if(plan_instance_status.equals("-3")){
														  plan_row.put("mp_instance_status_label","Terminated")
													}
													  if(plan_instance_status.equals("31")){
														  plan_row.put("mp_instance_status_label","Pending Installation")
													}
													  
													   String plan_deprovisioned_date = (String)plan_row.get("plan_deprovisioned_date")
													  logi.logInfo("checking for deprovision date value "+plan_deprovisioned_date)
													  
													  
													   
													   String client_dunning_id_check = (String)plan_row.get("client_dunning_process_id")
													  if(client_dunning_id_check.equals("null")){plan_row.remove("client_dunning_process_id")}
													  
													   
									 }
										  return plan_row.sort()
	  }
				   
				   
				   
				   
				   /** to get supp plans mapped to a mpi
					* @param str
					* @return returns hash of supp plans
					*/
					LinkedHashMap md_supp_plan_info_m(String str){
						logi.logInfo("/************supp***********/")
						String supp_plans = getValueFromRequest("get_acct_details_all_m",ExPathRpcEnc.Supp_plans);logi.logInfo("Supp Plans : "+supp_plans)
						LinkedHashMap row1 = new LinkedHashMap<String , HashMap<String,String>>()
						if(supp_plans.equals("1")){
						ConnectDB db = new ConnectDB()
						InputMethods im =new InputMethods()
						LinkedHashMap row = new LinkedHashMap<String , HashMap<String,String>>()
						String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
						String query = "select count(*) from ariacore.plan_instance pi join ariacore.client_plan cp on pi.client_no = cp.client_no and pi.plan_no = cp.plan_no "+
										" join ariacore.plan_rate_schedule prs on prs.plan_no = pi.plan_no and PI.SCHEDULE_NO = prs.SCHEDULE_NO and pi.client_no=prs.client_no "+
										" where pi.parent_plan_instance_no is not null and pi.acct_no = "+acct_no
										String plancount = db.executeQueryP2(query).toString()
										int plan_count = Integer.parseInt(plancount)
										logi.logInfo("value converted to integer : "+plan_count)
										for(int i=1;i<=plan_count;i++){
											logi.logInfo("entered into the loop")
											int s =i-1;
											logi.logInfo("counting"+s)
										row.put("supp_plans_info_row["+s+"]",md_Supp_instance_row(acct_no,i))
										}
										return row.sort()
						}else{
						return row1
						}
					}
					
					
					/** to get supp row values
					 * @param acct_no
					 * @param i
					 * @return spi instance values are returned
					 */
					public LinkedHashMap md_Supp_instance_row(String acct_no, int i){
								
						ConnectDB db = new ConnectDB()
						LinkedHashMap<String,String> supp_plan_row = new LinkedHashMap<String,String>()
						String query = "select bb.supp_plan_instance_no,bb.client_supp_plan_instance_id,bb.supp_plan_no,bb.supp_plan_instance_description,bb.supp_plan_units,"+
										" bb.last_arrears_bill_thru_date,bb.last_bill_date,bb.last_bill_thru_date,bb.next_bill_date,bb.plan_date,bb.status_date,bb.supp_plan_instance_status_cd,bb.parent_plan_instance_no,bb.recurring_billing_interval,bb.usage_billing_interval,"+
						 " bb.rollover_plan_status,bb.rollover_plan_status_uom_cd,bb.client_supp_plan_id,bb.alt_rate_schedule_no,bb.plan_deprovisioned_date,bb.client_alt_rate_schedule_id "+
						" from "+
						" (select pi.plan_instance_no as supp_plan_instance_no,pi.plan_instance_cdid as client_supp_plan_instance_id,pi.PLAN_NO as supp_plan_no,cp.CLIENT_PLAN_ID as client_supp_plan_id "+
									"	,pi.PLAN_INSTANCE_DESCRIPTION as supp_plan_instance_description,pi.plan_units as supp_plan_units,pi.SCHEDULE_NO as alt_rate_schedule_no,"+
					   "   prs.CLIENT_RATE_SCHEDULE_ID as client_alt_rate_schedule_id,"+
									"	TO_CHAR(pi.LAST_ARREARS_BILL_THRU_DATE,'YYYY-MM-DD') as last_arrears_bill_thru_date,"+
								"	TO_CHAR(pi.LAST_BILL_DATE,'YYYY-MM-DD') as last_bill_date, "+
								"		TO_CHAR(pi.LAST_BILL_THRU_DATE,'YYYY-MM-DD') as last_bill_thru_date, "+
									"	TO_CHAR(pi.NEXT_BILL_DATE,'YYYY-MM-DD') as next_bill_date, "+
									"	TO_CHAR(pi.PLAN_DATE,'YYYY-MM-DD') as plan_date,"+
									"	TO_CHAR(pi.STATUS_DATE,'YYYY-MM-DD') as status_date,"+
									"	pi.STATUS_CD as supp_plan_instance_status_cd,pi.PARENT_PLAN_INSTANCE_NO as parent_plan_instance_no,"+
									"   prs.RECUR_BILLING_INTERVAL as recurring_billing_interval,"+
									"	prs.USAGE_BILLING_INTERVAL as usage_billing_interval,cp.ROLLOVER_INST_STATUS_CD as rollover_plan_status,"+
									"	cp.ROLLOVER_INST_STATUS_PTYPE as rollover_plan_status_uom_cd,TO_CHAR(pi.DEPROVISION_DATE,'YYYY-MM-DD') AS plan_deprovisioned_date,row_number() over(order by pi.PLAN_INSTANCE_NO)as seq "+
									"	from ariacore.plan_instance pi "+
									"	 join ariacore.client_plan cp on pi.client_no = cp.client_no and pi.plan_no = cp.plan_no "+
									"	 join ariacore.plan_rate_schedule prs on prs.plan_no = pi.plan_no and PI.SCHEDULE_NO = prs.SCHEDULE_NO and pi.client_no=prs.client_no "+
									"	 where pi.parent_plan_instance_no is not null and prs.client_no = cp.client_no and pi.acct_no = "+acct_no+")bb where bb.seq ="+i
										ResultSet rs1 = db.executePlaQuery(query);
										ResultSetMetaData md1 = rs1.getMetaData();
										int columns1 = md1.getColumnCount();
										while (rs1.next()){
											
												 for(int s=1; s<=columns1; ++s){
													 if(rs1.getObject(s).toString().equals("null")){
														 logi.logInfo("No value in "+rs1.getObject(s))
													 }else{logi.logInfo("Value inserting in hash map")
													 supp_plan_row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
													 logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
													 
												 }
												// plan_row.put("master_plan_plan_inst_fields","  ")
												String instance = (String)supp_plan_row.get("parent_plan_instance_no")
										String cdid = "select PLAN_INSTANCE_CDID from ariacore.plan_instance where plan_instance_no = "+instance
										String client_parent_plan_instance_id=db.executeQueryP2(cdid).toString()
										supp_plan_row.put("client_parent_plan_instance_id",client_parent_plan_instance_id)
												 String plan_instance_status = (String)supp_plan_row.get("supp_plan_instance_status_cd")
												 if(plan_instance_status.equals("1")){
													 supp_plan_row.put("supp_plan_instance_status","Active")
												 }
												 if(plan_instance_status.equals("41")){
													 supp_plan_row.put("supp_plan_instance_status","Trial")
												 }
												 if(plan_instance_status.equals("61")){
													 supp_plan_row.put("supp_plan_instance_status","Active Non-Billable")
												 }
												 if(plan_instance_status.equals("-3")){
													 supp_plan_row.put("supp_plan_instance_status","Terminated")
												 }
												 if(plan_instance_status.equals("-2")){
													 supp_plan_row.put("supp_plan_instance_status","Cancelled")
												 }
												
									 }
						return supp_plan_row.sort()
					}
				   
				   
				   
				   
				   
				   LinkedHashMap md_GetAllChildNodesRname_m(String parentnode) throws Exception{
								 logi.logInfo("into md_GetAllChildNodesRname_m :"+parentnode);
								 
								 String [] rname=parentnode.split(",");
								 parentnode=parentnode.split(",")[0];
								 //rname.remove(0);
								 logi.logInfo "rname :"+rname
								 logi.logInfo "parentnode :"+parentnode
								 pnode=parentnode
								 logi.logInfo "pnode :"+pnode
								 ArrayList<String> childnodes = new ArrayList<String>();
								 ArrayList<ArrayList> all = new ArrayList<ArrayList>();
								 LinkedHashMap<String, String> inner = new LinkedHashMap<String, String>();
								 LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
								 String xmlRecords = library.Constants.Constant.RESPONSE
								 //Getting the first level of child nodes
								 int i=0;
								 childnodes=getnode(xmlRecords,parentnode,Sindex);
								 //for getting the second level of child nodes
								 for (String number : childnodes)
								 {
									   all.add(getnode(xmlRecords,number,i));
									   i++;
					 
								 }
								 logi.logInfo "all :"+all
								 int j=1;
								 for(ArrayList<String> a1 : all)
								 {
									   pname=childnodes.get(j-1);
									   for(String a2 : a1)
									   {
											 if(getval(xmlRecords,a2,(j-1))!=null)
											 if(!Arrays.asList(rname).contains(a2.replace("ns1:","")))
												   inner.put(a2.replace("ns1:", ""),getval(xmlRecords,a2,(j-1)));
					 
									   }
									   inner=inner.sort()
									   outer.put(childnodes.get(j-1).replace("ns1:", "")+"["+(j-1)+"]", inner);
									   inner = new LinkedHashMap<String, String>();
									   j++;
								 }
								 
								 outer=outer.sort()
								 logi.logInfo "outer :"+outer
								 return outer
						   }

				   
				   public   HashMap md_getSpecificnodeAsList_m(String name1) throws Exception{
								 logi.logInfo "into md_getSpecificnodeAsList_m"
								 logi.logInfo "param given "+name1+Sindex
								 String xmlRecords = library.Constants.Constant.RESPONSE
									LinkedList<String> temp2 = new LinkedList<String>();
									HashMap<String, String> hm = new HashMap<String, String>();
									String temp = null;
									DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
									InputSource is = new InputSource();
									is.setCharacterStream(new StringReader(xmlRecords));
									Document doc = db.parse(is);
									NodeList nodes = doc.getElementsByTagName(pnode);
									   Element element = (Element) nodes.item(Sindex);
									   
									NodeList name = element.getElementsByTagName(name1);
									int index=name.getLength();
									for(int k=index;k>0;k--)
									{ Element line = (Element) name.item(k-1);
									temp=getCharacterDataFromElement(line);
									
									   if(!temp.equals(null)||temp!=null)
									   {
											 temp2.add(temp);
											 if(k>1)
											 hm.put(name1.replace("ns1:","")+"["+(k)+"]",temp);
											 else
												   hm.put(name1.replace("ns1:",""),temp);
									   }
									}
									   return hm.sort();
					 }
				   
				   public String md_getValFromNodeAndIndex_m(String input)
				   {   try{
					   logi.logInfo "into the md_getValFromNodeAndIndex_m"+input
					   String xmlRecords = library.Constants.Constant.RESPONSE
					   LinkedList<String> temp2 = new LinkedList<String>();
					   HashMap<String, String> hm = new HashMap<String, String>();
					   String temp = null;
					   DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					   InputSource is = new InputSource();
					   is.setCharacterStream(new StringReader(xmlRecords));
					   Document doc = db.parse(is);
					   String true_input=input;
					   String  [] inp=input.split("\\|")
					   int count=0;
					   for (int i = 0; i < input.length(); i++)
					   {
							 if (input.charAt(i) == '|')
							 {
									count++;
							 }
					   }
					   logi.logInfo 'got the count '+count
					   //store the target node as input value
					   input=	inp[count]
					   NodeList nodes = doc.getElementsByTagName(doc.getDocumentElement().getNodeName());
					   Element element = (Element) nodes.item((0));
					   if(count>0)
					   {
						   for(int t=0;t<count;t++)
						   {
							   logi.logInfo "Setting boundary "+ true_input.split('\\|')[t]
							   nodes = element.getElementsByTagName(true_input.split('\\|')[t].split(',')[0]);
							   if(nodes.getLength()<Integer.parseInt(true_input.split('\\|')[t].split(',')[1])) return 'NO NODE'
							   else element = (Element) nodes.item(((Integer.parseInt(true_input.split('\\|')[t].split(',')[1]))-1));
						   }
					   }
					   NodeList name = element.getElementsByTagName(input.split(",")[0]);
					   int index=Integer.parseInt(input.split(",")[1])
					   index-=1;
					   if(name.getLength()<=index)return 'NO NODE'
					   else {Element line = (Element) name.item(index);
					   temp=getCharacterDataFromElement2(line);
					   logi.logInfo 'value got '+temp;
					   return temp;}
					   }catch(Exception e){return 'Exception'}
																 
				   }

				   
				   
				   public  LinkedHashMap md_getAllnodesAsList_m(String parentnode) throws Exception
						   {
								 logi.logInfo "into md_getAllnodesAsList_m"
								 String xmlRecords = library.Constants.Constant.RESPONSE
								 pnode=parentnode.split("\\,")[0];
								 Sindex=Integer.parseInt(parentnode.split("\\,")[1]);
								 Sindex-=1;
								 logi.logInfo "param given "+parentnode
								 LinkedList<String> childnodes = new LinkedList<String>();
								 LinkedList<String> childnodes2 = new LinkedList<String>();
								 LinkedHashMap<String, String> inner = new LinkedHashMap<String, String>();
								 //for getting the values of child nodes
					 
								 childnodes=getnode(xmlRecords,pnode,Sindex);
								 logi.logInfo "param given "+parentnode
								 int sizze=childnodes.size();
								 int innerindex=0;
								 for (int i=0;i<sizze;i++)
								 {
								
									   innerindex=Collections.frequency(childnodes2, childnodes.get(i));
						   
									   
									   if(getnode(xmlRecords,childnodes.get(i),0).size()<1)
								 {
											 
												   if(Collections.frequency(childnodes2, childnodes.get(i))==0)
												   childnodes2.add(childnodes.get(i));
								   
											 
								 }
									   
								 else {
									   childnodes2.add(childnodes.get(i));
									   childnodes.addAll(getnode(xmlRecords,childnodes.get(i),innerindex));
									   
										  }
								 
								 sizze=childnodes.size();
								 }
						   
							   
								 for(String a : childnodes2)
								 {
								 if(getnode(xmlRecords,a,0).size()<1)
									   inner.putAll(md_getSpecificnodeAsList_m(a));
								 
								 }
								 System.out.println(inner.size());
								 return inner.sort();
					 
						   }

	def md_GetAllNestedChildNodes_m(String parentnode) throws Exception{
		LinkedHashMap<String, String> listing = new LinkedHashMap<String, String>();
		LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
		LinkedHashMap<String, HashMap> outer1 = new LinkedHashMap<String, HashMap>();

		listing=md_getSpecificnodeAsList_m(parentnode)

		int counts=listing.size()
		logi.logInfo "the count is here :"+counts
		for(int i=0;i<counts;i++)
		{

			outer=(md_getspecificchildnodes_m(parentnode+","+(i+1).toString()))
			outer1.put(parentnode.replace("ns1:","")+"["+(i)+"]",outer)
			outer = new LinkedHashMap<String, String>();
		}
		outer1=outer1.sort()
		return outer1
	}
						   
	def md_GetAllNestedChildNodesRname_m(String parentnode) throws Exception{
		LinkedHashMap<String, String> listing = new LinkedHashMap<String, String>();
		LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
		LinkedHashMap<String, HashMap> outer1 = new LinkedHashMap<String, HashMap>();
		
		listing=md_getSpecificnodeAsList_m(parentnode.split(",")[0])

		int counts=listing.size()
		logi.logInfo "the count is here :"+counts
		for(int i=0;i<counts;i++)
		{

			outer=(md_getspecificchildnodesRname_m(parentnode,(i+1)))
			outer=outer.sort()
			outer1.put(parentnode.split(",")[0].replace("ns1:","")+"["+(i)+"]",outer)
			outer = new LinkedHashMap<String, String>();
		}
		outer1=outer1.sort()
		return outer1
	}

				   
				   
				   LinkedHashMap md_GetNestedChildNodes_m(String parentnode) throws Exception{
								 logi.logInfo(parentnode);
								 pnode=parentnode
								 ArrayList<String> childnodes = new ArrayList<String>();
								 ArrayList<ArrayList> all = new ArrayList<ArrayList>();
								 LinkedHashMap<String, String> inner = new LinkedHashMap<String, String>();
								 LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
								 String xmlRecords = library.Constants.Constant.RESPONSE
								 //Getting the first level of child nodes
								 childnodes=getnode(xmlRecords,parentnode,Sindex);
								 logi.logInfo ""+childnodes
								 int i=0;
								 //for getting the second level of child nodes
								 for (String number : childnodes)
								 {
											//pname=childnodes.get(i);
										  // all.add(getnode(xmlRecords,number,i));
									   //Sindex=(i+1)
											inner=md_getAllnodesAsList_m(number+","+(i+1))
										  
									outer.put(number.replace("ns1:","")+"["+(i+1)+"]",inner)
									   inner = new LinkedHashMap<String, String>();
									   i++;
								 }
								
			 
								 return outer
						}

		 
				   
				   
				   
				   def md_GET_ACCT_SURCHARGES_m_Plan_Details(String s)
				   {
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m Start**************")
						 InputMethods im = new InputMethods()
						 LinkedHashMap<String, String> hm = new LinkedHashMap<String, String>()
						 ArrayList<String> list = new ArrayList<String>()
						 int i = 1
						 String acctNo = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
						 ConnectDB db = new ConnectDB()
						 String querySurchargeNo = "SELECT surcharge_no FROM ARIACORE.acct_surcharge WHERE ACCT_NO = "+acctNo+" AND CLIENT_NO = "+Constant.Client_No+""
						 ResultSet rs_SurchargeNo = db.executePlaQuery(querySurchargeNo)
						 while(rs_SurchargeNo.next())
						 {
								  
								  list[i]=rs_SurchargeNo.getObject(i)
									 i++
						 }
						 for(i=1;i<=list.size();i++) {
							   String DB_Surcharge_Rate_query = "SELECT * FROM(SELECT csp.RATE_SEQ_NO,csp.FROM_UNIT,csp.TO_UNIT,csp.RATE_PER_UNIT,srs.include_zero_ind as include_zero,srs.def_rate_schedule_ind as rate_sched_is_assigned_ind,srs.surcharge_no FROM ARIACORE.CLIENT_SURCHARGE_PRICE csp,ariacore.surcharge_rate_schedule srs WHERE csp.client_no ="+Constant.Client_No+" AND csp.surcharge_no= srs.surcharge_no) WHERE surcharge_no ="+list.get(i)+""
							   ResultSet resultSet = db.executePlaQuery(DB_Surcharge_Rate_query)
							   ResultSetMetaData md = resultSet.getMetaData()
							   int columns = md.getColumnCount()
							   logi.logInfo("Print column value"+columns)
							   while (resultSet.next()){
									 for(int j = 1;j<=columns;j++){
										   if(resultSet.getObject(j).toString().equals("null"))
										   {
												 logi.logInfo("No value in "+resultSet.getObject(j).toString())
										   }
										   else
										   {
												 logi.logInfo("Value inserting in hash map")
												 
												 logi.logInfo("Surcharge Plan details" + md.getColumnName(j).toString())
												 hm.put(md.getColumnName(j).toLowerCase(),resultSet.getObject(j));
										   }
									 }
							   }
						 
						 }
						 return hm
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m Ends**************")
				   }
			 
				   def md_invoice_itemsDB_m (String s)
				   {
												
												 HashMap hh=md_getSpecificnodeAsList_m("ns1:invoice_no")
												ArrayList<String> invoice_no = new ArrayList<String>(hh.values());
																		Collections.sort(invoice_no)
												LinkedHashMap<String,LinkedHashMap> invoice_info_row = new LinkedHashMap<String,LinkedHashMap>();
												LinkedHashMap<String,LinkedHashMap> invoice = new LinkedHashMap<String,LinkedHashMap>();
												LinkedHashMap line_item = new LinkedHashMap();
																		int loops=1;
												for(int i=0;i<invoice_no.size();i++)
													  {
															  String loopval= invoice_no.get(i);
															  ConnectDB db = new ConnectDB()
														int counts= Integer.parseInt(db.executeQueryP2("SELECT count(*) from ariacore.gl_detail where invoice_no="+loopval).toString())
																				for(int loop2=1;loop2<=counts;loop2++)
														{
																							String  DB_INVOICE_ITEMS_ROW_m=""
																							ResultSet resultSet =null;
																							ResultSetMetaData md ;
																							
																							String check_surcharge=db.executeQueryP2("select surcharge_rate_seq_no from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+loopval).toString()
																							String check_coupon=db.executeQueryP2("select orig_coupon_cd from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+loopval).toString()
																							String Check_NSO=db.executeQueryP2("select orig_client_sku from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+loopval).toString()
																																		  logi.logInfo ""+check_surcharge
																							logi.logInfo ""+check_coupon
																							if((check_surcharge.contains("null")&&check_coupon.contains("null")&&Check_NSO.contains("null"))||(check_surcharge==null&&check_coupon==null&&Check_NSO==null))
																							{
																											DB_INVOICE_ITEMS_ROW_m="SELECT a.plan_no,a.client_plan_id ,a.service_no, a.service_name, a.service_coa_id, a.units, a.rate_per_unit,a.client_service_id,a.RATE_SCHEDULE_NO,a.BASE_PLAN_UNITS,a.proration_factor,a.line_description,a.plan_instance_no,a.client_plan_instance_id,a.ship_to_address_no,a.bill_to_address_no FROM(SELECT  gld.RATE_SCHEDULE_NO,gld.plan_instance_no,pi.PLAN_INSTANCE_CDID as client_plan_instance_id,gld.BASE_PLAN_UNITS,gld.proration_factor,gld.seq_num AS seq,gld.plan_no AS plan_no,gld.service_no  AS service_no,gld.usage_units AS units, gld.usage_rate  AS rate_per_unit,CS.SERVICE_NAME AS service_name,cs.client_service_id AS client_service_id,cs.coa_id as service_coa_id,CP.CLIENT_PLAN_ID AS client_plan_id, gld.comments as line_description,gld.bill_to_addr_seq  AS bill_to_address_no, gld.ship_to_addr_seq  AS ship_to_address_no FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL  ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.ALL_SERVICE ALS ON ALS.SERVICE_NO=GLD.SERVICE_NO AND ALS.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.PLAN_SERVICES PS ON PS.CLIENT_NO=ALS.CLIENT_NO AND PS.SERVICE_NO=ALS.SERVICE_NO AND PS.PLAN_NO=GLD.PLAN_NO AND PS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE  CS ON CS.SERVICE_NO= gld.SERVICE_NO and cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP ON CP.PLAN_NO=PS.PLAN_NO and cp.client_no=ps.client_no join ariacore.plan_instance pi on pi.PLAN_INSTANCE_NO=gld.plan_instance_no and pi.client_no=gld.client_no WHERE gld.invoice_no= "+loopval+") a where seq="+loop2
																										   if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_INVOICE_ITEMS_ROW_m+")"))!=0)
																											{ resultSet = db.executePlaQuery(DB_INVOICE_ITEMS_ROW_m)
																											md = resultSet.getMetaData()}
																							}
																							if(resultSet==null)
																							{
																								 check_surcharge=db.executeQueryP2("select surcharge_rate_seq_no from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+loopval).toString()
																								if(!check_surcharge.contains("null"))
																								{
																									DB_INVOICE_ITEMS_ROW_m=" select gld.plan_instance_no,pi.PLAN_INSTANCE_CDID as client_plan_instance_id, gld.proration_factor,gld.base_plan_units,gld.RATE_SCHEDULE_NO,gld.comments as line_description,gld.usage_units as units,gld.usage_rate as rate_per_unit,gld.service_no, ser.coa_id as service_coa_id, als.service_name,ser.client_service_id from ariacore.gl_detail gld join  ariacore.services  ser on ser.service_no=gld.service_no join ariacore.all_service als on als.service_no=ser.service_no join ariacore.client_surcharge cs on cs.alt_service_no_2_apply=ser.service_no join ariacore.CLIENT_SURCHARGE_PRICE csp on csp.surcharge_no=cs.surcharge_no join ariacore.plan_instance pi on gld.plan_instance_no = pi.plan_instance_no where ser.is_surcharge=1 and gld.invoice_no= "+loopval+" and seq_num="+loop2
																									if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_INVOICE_ITEMS_ROW_m+")"))!=0)
																									{resultSet = db.executePlaQuery(DB_INVOICE_ITEMS_ROW_m)
																									md = resultSet.getMetaData()}
																								}
																							}
																								  if(resultSet==null)
																								  {
																											  check_surcharge=db.executeQueryP2("select orig_client_sku from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+loopval).toString()
																											  if(!check_surcharge.contains("null"))
																											  {
																															  DB_INVOICE_ITEMS_ROW_m="SELECT  a.service_no, a.service_name, a.service_coa_id, a.units, a.rate_per_unit, a.client_service_id,a.ORIG_CLIENT_SKU as client_sku,a.ITEM_NO, a.CLIENT_ITEM_ID ,a.ORDER_NO,a.line_description FROM(SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS seq, it.CLIENT_ITEM_ID , gld.service_no AS service_no, gld.usage_units AS units, gld.usage_rate  AS rate_per_unit,CS.SERVICE_NAME AS service_name, cs.client_service_id AS client_service_id, cs.coa_id AS service_coa_id, gld.comments AS line_description,gld.ORIG_CLIENT_SKU,gld.ITEM_NO,gld.ORDER_NO  FROM ARIACORE.GL_DETAIL GLD  JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.CLIENT_SERVICE CS ON CS.SERVICE_NO= gld.SERVICE_NO and gld.client_no=cs.client_no join ariacore.INVENTORY_ITEMS it   ON gld.ORIG_CLIENT_SKU = it.CLIENT_SKU   AND it.CLIENT_NO=gld.CLIENT_NO WHERE gld.invoice_no= "+loopval+"and  gld.seq_num="+loop2+")a"
																															  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_INVOICE_ITEMS_ROW_m+")"))!=0)
																															 { resultSet = db.executePlaQuery(DB_INVOICE_ITEMS_ROW_m)
																															  md = resultSet.getMetaData()}
																											  }
																								  }
																  if(resultSet==null)
																 {
																		  check_coupon=db.executeQueryP2("select orig_coupon_cd from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+loopval).toString()
																		  if(!check_coupon.contains("null"))
																		  {
																					//DB_INVOICE_ITEMS_ROW_m="select gld.usage_rate as rate_per_unit,gld.plan_no,gld.comments as line_description,ac.COUPON_CD as credit_coupon_code,cp.client_plan_id,gld.service_no, cs.coa_id as service_coa_id, als.service_name,cs.client_service_id,gld.usage_units AS units from ariacore.gl_pending_detail gld join  ariacore.client_service  cs on cs.service_no=gld.service_no and cs.client_no=gld.client_no join ariacore.all_service als on als.service_no=cs.service_no and als.client_no=gld.client_no JOIN ARIACORE.PLAN_SERVICES PS ON PS.CLIENT_NO=ALS.CLIENT_NO AND PS.SERVICE_NO=ALS.SERVICE_NO AND PS.PLAN_NO=GLD.PLAN_NO AND PS.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.CLIENT_PLAN CP ON CP.PLAN_NO=PS.PLAN_NO and cp.client_no=ps.client_no join ariacore.all_invoices ai on ai.client_no=gld.client_no and ai.invoice_no = gld.invoice_no   join ariacore.ACCT_COUPONS ac on ac.client_no=gld.client_no and ac.acct_no = ai.acct_no  where gld.invoice_no= "+loopval+"and gld.seq_num="+loop2
																					DB_INVOICE_ITEMS_ROW_m = "SELECT distinct a.invoice_line_no,a.service_no,a.client_service_id,a.service_name,a.rate_per_unit, a.start_date_range,a.end_date_range,a.description,a.line_amount,a.line_units,a.credit_coupon_code, a.plan_no, a.plan_name, a.client_plan_id FROM(SELECT gld.seq_num AS invoice_line_no, GLD.service_no, gld.plan_no AS plan_no, GLD.debit AS line_amount,GLD.comments AS description,    GLD.usage_rate  AS rate_per_unit,  GLD.usage_units AS line_units,  TO_CHAR(GLD.start_date, 'YYYY-MM-DD') AS start_date_range, TO_CHAR(GLD.end_date, 'YYYY-MM-DD')   AS end_date_range, SER.client_service_id AS client_service_id,ALS.service_name  AS service_name,GLD.orig_coupon_cd  AS credit_coupon_code,CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID AS client_plan_id FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.SERVICES SER ON SER.SERVICE_NO = gld.SERVICE_NO JOIN ARIACORE.ALL_SERVICE ALS ON SER.SERVICE_NO = ALS.SERVICE_NO JOIN ARIACORE.CLIENT_PLAN ALS  ON SER.SERVICE_NO = ALS.SERVICE_NO  JOIN ARIACORE.CLIENT_PLAN CP ON CP.PLAN_NO        =GLD.PLAN_NO  AND CP.CLIENT_NO =GLD.CLIENT_NO  WHERE invoice_no = "+loopval+")a where invoice_line_no="+loop2
																					if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_INVOICE_ITEMS_ROW_m+")"))!=0)
																					{resultSet = db.executePlaQuery(DB_INVOICE_ITEMS_ROW_m)
																					md = resultSet.getMetaData()}
																		  }
																  }
																  
																   if(resultSet==null)
																  {
																		  logi.logInfo "into the tax part"
																		   DB_INVOICE_ITEMS_ROW_m="SELECT gld.service_no AS service_no, NVL(AlS.client_service_id,als.service_no) as client_service_id,AlS.SERVICE_NAME AS SERVICE_NAME,AlS.COA_ID  AS SERVICE_COA_ID,gld.comments AS line_description,gld.ORIG_COUPON_CD AS credit_coupon_code,  gld.base_plan_units, cp.client_plan_id,gld.plan_no,gld.PRORATION_FACTOR,gld.usage_rate  AS rate_per_unit,gld.rate_schedule_no,gld.usage_units AS units FROM ARIACORE.GL_DETAIL GLD LEFT JOIN ariacore.all_client_service AlS ON gld.client_no  =NVL(als.client_no,gld.client_no)AND AlS.SERVICE_NO=GLD.SERVICE_NO LEFT JOIN ARIACORE.PLAN_SERVICES PS ON PS.PLAN_NO   =GLD.PLAN_NO AND PS.CLIENT_NO =gld.client_no and gld.service_no = ps.service_no LEFT JOIN ARIACORE.CLIENT_PLAN CP ON CP.PLAN_NO =gld.PLAN_NO and cp.client_no = gld.client_no WHERE gld.invoice_no="+loopval+" and gld.seq_num="+loop2
																		   if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_INVOICE_ITEMS_ROW_m+")"))!=0)
																		   {resultSet = db.executePlaQuery(DB_INVOICE_ITEMS_ROW_m)
																		md = resultSet.getMetaData()}
																  }
																												  
																		int columns = md.getColumnCount();
																		while (resultSet.next())
																		{
																		
																		   for(int u=1; u<=columns; ++u)
																		   {
																			   if((resultSet.getObject(u))!=null)
																				line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
																		   }
																		   
																		}
																  
																		line_item.sort();
																		invoice_info_row.put("invoice_items_row["+(loops-1)+"]",line_item.sort())
																		line_item = new LinkedHashMap();
																		loops++;
														}
																					if(counts==0){
																							counts= Integer.parseInt(db.executeQueryP2("SELECT count(*) from ariacore.GL_PENDING_DETAIL where invoice_no="+loopval).toString())
																					
																							for(int loop2=1;loop2<=counts;loop2++)
																							{
																																				  String  DB_INVOICE_ITEMS_ROW_m=""
																																				  ResultSet resultSet =null;
																																				  ResultSetMetaData md ;
																																				  
																																				  String check_surcharge=db.executeQueryP2("select surcharge_rate_seq_no from ariacore.GL_PENDING_DETAIL where seq_num="+loop2+" and invoice_no="+loopval).toString()
																																				  String check_coupon=db.executeQueryP2("select orig_coupon_cd from ariacore.GL_PENDING_DETAIL where seq_num="+loop2+" and invoice_no="+loopval).toString()
																																				  String Check_NSO=db.executeQueryP2("select orig_client_sku from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+loopval).toString()
																																		  logi.logInfo ""+check_surcharge
																							logi.logInfo ""+check_coupon
																							if((check_surcharge.contains("null")&&check_coupon.contains("null")&&Check_NSO.contains("null"))||(check_surcharge==null&&check_coupon==null&&Check_NSO==null))
																											{
																																										  DB_INVOICE_ITEMS_ROW_m="SELECT a.plan_no,a.client_plan_id ,a.service_no, a.service_name, a.service_coa_id, a.units, a.rate_per_unit,a.client_service_id,a.RATE_SCHEDULE_NO,a.BASE_PLAN_UNITS,a.proration_factor,a.line_description,a.plan_instance_no,a.client_plan_instance_id FROM(SELECT  gld.RATE_SCHEDULE_NO,gld.BASE_PLAN_UNITS,gld.proration_factor, gld.seq_num AS seq,gld.plan_no AS plan_no,gld.service_no  AS service_no,gld.usage_units AS units, gld.usage_rate  AS rate_per_unit,CS.SERVICE_NAME AS service_name,cs.client_service_id AS client_service_id,cs.coa_id as service_coa_id,gld.plan_instance_no,pi.PLAN_INSTANCE_CDID as client_plan_instance_id,CP.CLIENT_PLAN_ID AS client_plan_id, gld.comments as line_description FROM ARIACORE.GL_PENDING_DETAIL GLD JOIN ariacore.GL_PENDING GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.ALL_SERVICE ALS ON ALS.SERVICE_NO=GLD.SERVICE_NO AND ALS.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.PLAN_SERVICES PS ON PS.CLIENT_NO=ALS.CLIENT_NO AND PS.SERVICE_NO=ALS.SERVICE_NO AND PS.PLAN_NO=GLD.PLAN_NO AND PS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE  CS ON CS.SERVICE_NO= gld.SERVICE_NO and cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP ON CP.PLAN_NO=PS.PLAN_NO and cp.client_no=ps.client_no join ariacore.plan_instance pi on pi.PLAN_INSTANCE_NO=gld.plan_instance_no and pi.client_no=gld.client_no WHERE gld.invoice_no= "+loopval+") a where seq="+loop2
																																										  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_INVOICE_ITEMS_ROW_m+")"))!=0)
																																										  {resultSet = db.executePlaQuery(DB_INVOICE_ITEMS_ROW_m)
																																										  md = resultSet.getMetaData()}
																											}
																											if(resultSet==null)
																											{
																														check_surcharge=db.executeQueryP2("select surcharge_rate_seq_no from ariacore.GL_PENDING_DETAIL where seq_num="+loop2+" and invoice_no="+loopval).toString()
																														if(!check_surcharge.contains("null"))
																														{
																																	  DB_INVOICE_ITEMS_ROW_m=" select gld.proration_factor,gld.base_plan_units,gld.RATE_SCHEDULE_NO,gld.comments as line_description,gld.usage_units as units,gld.usage_rate as rate_per_unit,gld.service_no, ser.coa_id as service_coa_id, als.service_name,ser.client_service_id from ariacore.GL_PENDING_DETAIL gld join  ariacore.services  ser on ser.service_no=gld.service_no join ariacore.all_service als on als.service_no=ser.service_no join ariacore.client_surcharge cs on cs.alt_service_no_2_apply=ser.service_no join ariacore.CLIENT_SURCHARGE_PRICE csp on csp.surcharge_no=cs.surcharge_no where ser.is_surcharge=1 and gld.invoice_no= "+loopval+" and seq_num="+loop2
																																	  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_INVOICE_ITEMS_ROW_m+")"))!=0)
																																	{  resultSet = db.executePlaQuery(DB_INVOICE_ITEMS_ROW_m)
																																	  md = resultSet.getMetaData()}
																														}
																											}
																											if(resultSet==null)
																											{
																														check_coupon=db.executeQueryP2("select orig_client_sku from ariacore.GL_PENDING_DETAIL where seq_num="+loop2+" and invoice_no="+loopval).toString()
																														if(!check_coupon.contains("null"))
																														{
																																	  DB_INVOICE_ITEMS_ROW_m="SELECT  a.service_no, a.service_name, a.service_coa_id, a.units, a.rate_per_unit, a.client_service_id,a.ORIG_CLIENT_SKU as client_sku,a.ITEM_NO, a.CLIENT_ITEM_ID ,a.ORDER_NO,a.line_description FROM(SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS seq, it.CLIENT_ITEM_ID , gld.service_no AS service_no, gld.usage_units AS units, gld.usage_rate  AS rate_per_unit,CS.SERVICE_NAME AS service_name, cs.client_service_id AS client_service_id, cs.coa_id AS service_coa_id, gld.comments AS line_description,gld.ORIG_CLIENT_SKU,gld.ITEM_NO,gld.ORDER_NO  FROM ARIACORE.GL_PENDING_DETAIL GLD  JOIN ARIACORE.GL_PENDING gl ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.CLIENT_SERVICE CS ON CS.SERVICE_NO= gld.SERVICE_NO and gld.client_no=cs.client_no join ariacore.INVENTORY_ITEMS it   ON gld.ORIG_CLIENT_SKU = it.CLIENT_SKU   AND it.CLIENT_NO=gld.CLIENT_NO   WHERE gld.invoice_no= "+loopval+" and gld.seq_num="+loop2+")a"
																																	  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_INVOICE_ITEMS_ROW_m+")"))!=0)
																																	  { resultSet = db.executePlaQuery(DB_INVOICE_ITEMS_ROW_m)
																																	  md = resultSet.getMetaData()}
																														}
																											}
																											if(resultSet==null)
																											{
																														check_coupon=db.executeQueryP2("select orig_coupon_cd from ariacore.GL_PENDING_DETAIL where seq_num="+loop2+" and invoice_no="+loopval).toString()
																														if(!check_coupon.contains("null"))
																														{
																																	  DB_INVOICE_ITEMS_ROW_m="select gld.usage_rate as rate_per_unit,gld.plan_no,gld.comments as line_description,ac.COUPON_CD as credit_coupon_code,cp.client_plan_id,gld.service_no, cs.coa_id as service_coa_id, als.service_name,cs.client_service_id,gld.usage_units AS units from ariacore.gl_pending_detail gld join  ariacore.client_service  cs on cs.service_no=gld.service_no and cs.client_no=gld.client_no join ariacore.all_service als on als.service_no=cs.service_no and als.client_no=gld.client_no JOIN ARIACORE.PLAN_SERVICES PS ON PS.CLIENT_NO=ALS.CLIENT_NO AND PS.SERVICE_NO=ALS.SERVICE_NO AND PS.PLAN_NO=GLD.PLAN_NO AND PS.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.CLIENT_PLAN CP ON CP.PLAN_NO=PS.PLAN_NO and cp.client_no=ps.client_no join ariacore.all_invoices ai on ai.client_no=gld.client_no and ai.invoice_no = gld.invoice_no   join ariacore.ACCT_COUPONS ac on ac.client_no=gld.client_no and ac.acct_no = ai.acct_no  where gld.invoice_no= "+loopval+"and gld.seq_num="+loop2
																																	  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_INVOICE_ITEMS_ROW_m+")"))!=0)
																																	 { resultSet = db.executePlaQuery(DB_INVOICE_ITEMS_ROW_m)
																																	  md = resultSet.getMetaData()}
																														}
																											}
																											if(resultSet==null)
																											{
																														logi.logInfo "into the tax part"
																														
																														DB_INVOICE_ITEMS_ROW_m="SELECT gld.service_no AS service_no, NVL(AlS.client_service_id,als.service_no) as client_service_id,AlS.SERVICE_NAME AS SERVICE_NAME,AlS.COA_ID  AS SERVICE_COA_ID,gld.comments AS line_description,gld.ORIG_COUPON_CD AS credit_coupon_code,  gld.base_plan_units, cp.client_plan_id,gld.plan_no,gld.PRORATION_FACTOR,gld.usage_rate  AS rate_per_unit,gld.rate_schedule_no,gld.usage_units AS units FROM ARIACORE.GL_PENDING_DETAIL GLD LEFT JOIN ariacore.all_client_service AlS ON gld.client_no  =NVL(als.client_no,gld.client_no)AND AlS.SERVICE_NO=GLD.SERVICE_NO LEFT JOIN ARIACORE.PLAN_SERVICES PS ON PS.PLAN_NO   =GLD.PLAN_NO AND PS.CLIENT_NO =gld.client_no and gld.service_no = ps.service_no LEFT JOIN ARIACORE.CLIENT_PLAN CP ON CP.PLAN_NO =gld.PLAN_NO and cp.client_no = gld.client_no WHERE gld.invoice_no="+loopval+" and gld.seq_num="+loop2
																														if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_INVOICE_ITEMS_ROW_m+")"))!=0)
																														{resultSet = db.executePlaQuery(DB_INVOICE_ITEMS_ROW_m)
																													md = resultSet.getMetaData()}
																											}
																																													
																													int columns = md.getColumnCount();
																													while (resultSet.next())
																													{
																													
																														 for(int u=1; u<=columns; ++u)
																														{
																															if((resultSet.getObject(u))!=null)
																																line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
																														}
																														
																													}
																											
																													line_item.sort();
																													invoice_info_row.put("invoice_items_row["+(loops-1)+"]",line_item.sort())
																													line_item = new LinkedHashMap();
																													loops++;
																							}
													  // invoice.put("invoice_no["+loopval+"]",invoice_info_row.sort())
													  // invoice_info_row = new LinkedHashMap<String,LinkedHashMap>();
												   }}
																				invoice_info_row=invoice_info_row.sort()
												return invoice_info_row
											}




				  
				   def md_invoice_items_m(String s)
						   {
								 HashMap hh=md_getSpecificnodeAsList_m("ns1:invoice_no")
								 ArrayList<String> invoice_no = new ArrayList<String>(hh.values());
								 LinkedHashMap<String,LinkedHashMap> invoice_info_row = new LinkedHashMap<String,LinkedHashMap>();
								 LinkedHashMap<String,LinkedHashMap> invoice = new LinkedHashMap<String,LinkedHashMap>();
								 LinkedHashMap line_item = new LinkedHashMap();
								 for(int i=0;i<invoice_no.size();i++)
								 {
									   String loopval= invoice_no.get(i);
									   Sindex=i;
									   invoice_info_row=md_GetAllChildNodesRname_m("ns1:invoice_items,invoice_line_no,plan_name,ns1:line_amount,line_description,start_date_range,end_date_range,line_amount");
									   invoice.put("invoice_no["+loopval+"]",invoice_info_row.sort())
									   invoice_info_row = new LinkedHashMap<String,LinkedHashMap>();
								 }
								 invoice=invoice.sort()
								 return invoice
						   }


				   
	def md_billing_errorsDB_m(String s)
	  {
			  ConnectDB db = new ConnectDB()
			  LinkedHashMap row_item = new LinkedHashMap();
			  ArrayList<String> acct_nos=im.md_GET_ALL_ACCT_NO_m("empty",null)
			  logi.logInfo "acct no got "+acct_nos
			  LinkedHashMap<String,LinkedHashMap> row = new LinkedHashMap<String,LinkedHashMap>();
			  LinkedHashMap<String,LinkedHashMap> billing = new LinkedHashMap<String,LinkedHashMap>();
					int k=0;
			  for(int i=0;i<acct_nos.size();i++)
			  {
						String loopval=acct_nos.get(i)
						logi.logInfo ""+loopval
						int counts= db.executeQueryP2("Select count(*) from (SELECT distinct(pim.billing_group_no) as b from ariacore.acct_billing_group abg JOIN ariacore.plan_instance PI ON abg.acct_no   = pi.acct_no AND abg.client_no = pi.client_no JOIN ariacore.plan_instance_master pim ON abg.billing_group_no  = pim.billing_group_no AND pim.client_no = abg.client_no AND pim.plan_instance_no = pi.plan_instance_no where abg.acct_no="+loopval+")").toInteger()
						for(int j=1;j<=counts;j++)
						{
								logi.logInfo "into the for loop for count";
							//String Query = "select a.billing_group_no,a.client_billing_group_id from(select ROW_NUMBER()OVER(ORDER BY billing_group_no ) as seq,billing_group_no , profile_cdid as client_billing_group_id from ariacore.acct_billing_group where acct_no="+acct_nos.get(i).toString()+"  )a where a.seq="+j;
								String Query = "Select a.billing_group_no,a.client_billing_group_id from(select ROW_NUMBER()OVER(ORDER BY pim.billing_group_no ) as seq, pim.billing_group_no, abg.profile_cdid as client_billing_group_id FROM ariacore.acct_billing_group abg JOIN ariacore.plan_instance PI ON abg.acct_no = pi.acct_no AND abg.client_no = pi.client_no JOIN ariacore.plan_instance_master pim ON abg.billing_group_no = pim.billing_group_no AND pim.client_no =abg.client_no AND pim.plan_instance_no =pi.plan_instance_no WHERE abg.acct_no="+acct_nos.get(i).toString()+"  )a where a.seq="+j;
								
						ResultSet resultSet = db.executePlaQuery(Query)
						ResultSetMetaData md = resultSet.getMetaData()
				  
						int columns = md.getColumnCount();
									while (resultSet.next())
										  {
												  logi.logInfo "into the while loop for putting the values"
												/* row_item.put("invoicing_error_code","0")
												  row_item.put("invoicing_error_msg","OK")
												  row_item.put("collection_error_code","0")
												  row_item.put("collection_error_msg","OK")
												  row_item.put("statement_error_code","0")
												  row_item.put("statement_error_msg","OK")*/
												  for(int u=1; u<=columns; ++u)
													  {
																	row_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
													  }
										  }
						
						row.put("billing_errors_row["+(k)+"]",row_item.sort())
									k++;
						row_item = new LinkedHashMap();
						}
			  }
			  row=row.sort()
			  return row
	  
	  }


				   
				   def md_billing_errors_m(String s)
				   {
					   db = new ConnectDB()
					   LinkedHashMap<String,LinkedHashMap> row = new LinkedHashMap<String,LinkedHashMap>();
					   LinkedHashMap<String,LinkedHashMap> billing = new LinkedHashMap<String,LinkedHashMap>();
					   ArrayList<String> acct_nos=im.md_GET_ALL_ACCT_NO_m("empty",null)
					   Collections.sort(acct_nos)
					   for(int i=0;i<acct_nos.size();i++)
					   {
						   Sindex=i;
						   row=md_GetAllChildNodesRname_m("ns1:billing_errors,collection_error_code,collection_error_msg,invoicing_error_code,invoicing_error_msg,statement_error_code,statement_error_msg")
						   row=row.sort()
						   billing.put("billing group  for account : "+acct_nos.get(i).toString(),row)
						   row = new LinkedHashMap<String,LinkedHashMap>();
					   }
					   billing=billing.sort()
					   return billing;
				   }
				   
				   def md_get_node_count_m(String input)
				   {
					  logi.logInfo("into md_get_node_count_m")
					  LinkedHashMap<String,String> inner = new LinkedHashMap<String,String>()
					  int acct_count=md_getSpecificnodeAsList_m(input).size()
					  logi.logInfo("method caught accts "+acct_count)
					  return acct_count.toString()
				   }
				   
				   public LinkedHashMap md_get_acct_supp_fields_m(String str){
					   InputMethods im = new InputMethods()
					   ConnectDB db = new ConnectDB()
					   String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
					   LinkedHashMap<String,HashMap<String,String>> row = new LinkedHashMap<String,HashMap<String,String>>()
					   String query = "select count(*) as counts from ariacore.ACCT_SUPP_FIELD_VALS where acct_no = "+acct_no
					   String counting = db.executeQueryP2(query).toString()
					   int counts = Integer.parseInt(counting)
					   for(int i=1;i<=counts;i++){
							 int s =i-1;
							 logi.logInfo("counting"+s)
					   
								   row.put("supp_fields_row["+s+"]",md_get_supp_fields_row(acct_no,i))
					   }
					   logi.logInfo("returning main hash : "+row)
					   return row.sort()
								   
				 }
				 public LinkedHashMap md_get_supp_fields_row(String acct_no,int i){
					   
					   ConnectDB db = new ConnectDB()
					   LinkedHashMap<String,String> row = new LinkedHashMap<String,String>()
					   String query = "select field_name,field_value from(select field_name,value_text as field_value, row_number() over (order by field_name) as seq from ariacore.ACCT_SUPP_FIELD_VALS where acct_no = "+acct_no+") where seq = "+i
					   ResultSet rs1 = db.executePlaQuery(query)
					   ResultSetMetaData md1 = rs1.getMetaData();
					   int columns1 = md1.getColumnCount();
					   while (rs1.next()){
					   
							 for(int s=1; s<=columns1; ++s){
								   if(rs1.getObject(s).toString().equals("null")){
										 logi.logInfo("No value in "+rs1.getObject(s))
								   }else{logi.logInfo("Value inserting in hash map")
								   row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
								   logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s).toLowerCase())
								   
							  }
					   }
					   logi.logInfo("returning the row hash : "+row)
					   
										 return row.sort()
				 }
				 
				 
				 
				 
				 
				 
				 public LinkedHashMap md_GetCollectionalGroup_m(String str){
					 InputMethods im = new InputMethods()
					 LinkedHashMap<String,String> row = new LinkedHashMap<String,String>()
					 LinkedHashMap<String,String> row1 = new LinkedHashMap<String,String>()
					 String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
					 ConnectDB db = new ConnectDB()
					 String func_count = "SELECT count(*) as counts FROM ariacore.acct_group_map agm JOIN ariacore.client_acct_groups cag ON agm.group_no = cag.group_no AND agm.client_no = cag.client_no AND collections_based_ind = 1 WHERE acct_no = " +acct_no
					 String fc = db.executeQueryP2(func_count).toString()
					 if(fc.equals("0")){
						   return row1
					 }else{
					 int cc = Integer.parseInt(fc)
					 for(int i=1;i<=cc;i++){
						   int s =i-1;
						   logi.logInfo("counting"+s)
					 
					 row.put("collection_acct_group_row["+s+"]",get_collectional_row(acct_no,i))
					 
					 }
					 }
					 row=row.sort()
					 return row
					 }
				 
				 public LinkedHashMap get_collectional_row(String acct_no,int i){
					 ConnectDB db = new ConnectDB()
					 LinkedHashMap<String,String> row = new LinkedHashMap<String,String>()
					 String query = "select s.collections_acct_group_no,s.client_collections_group_id from "+
											 "(SELECT agm.group_no as collections_acct_group_no,"+
											 " cag.CLIENT_GROUP_ID as client_collections_group_id,"+
											 " row_number() over (order by agm.group_no) AS seq "+
											 " FROM ariacore.acct_group_map agm "+
											 " JOIN ariacore.client_acct_groups cag "+
											 " ON agm.group_no           = cag.group_no "+
											 " AND agm.client_no         = cag.client_no "+
											 " AND collections_based_ind = 1 "+
											 " WHERE acct_no             = "+acct_no+")s where s.seq = "+i
					 ResultSet rs1 = db.executePlaQuery(query)
					 ResultSetMetaData md1 = rs1.getMetaData();
					 int columns1 = md1.getColumnCount();
					 while (rs1.next()){
					 
						   for(int s=1; s<=columns1; ++s){
								 if(rs1.getObject(s).toString().equals("null")){
									   logi.logInfo("No value in "+rs1.getObject(s))
								 }else{logi.logInfo("Value inserting in hash map")
								 row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
								 logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
								 
							}
					 }
					 String client_collections_acct_group_id = (String)row.get("client_collections_group_id")
					 row.put("client_collections_acct_group_id",client_collections_acct_group_id)
					 row.remove("client_collections_group_id")
					 row=row.sort()
									   return row
			   }
   
   
				 public LinkedHashMap md_GetFunctionalGroup_m(String str){
					 InputMethods im = new InputMethods()
					 LinkedHashMap<String,String> row = new LinkedHashMap<String,String>()
					 LinkedHashMap<String,String> row1 = new LinkedHashMap<String,String>()
					 String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
					 ConnectDB db = new ConnectDB()
					 String func_count = "SELECT count(*) as counts FROM ariacore.acct_group_map agm JOIN ariacore.client_acct_groups cag ON agm.group_no = cag.group_no AND agm.client_no = cag.client_no AND collections_based_ind = 0 WHERE acct_no = " +acct_no
					 String fc = db.executeQueryP2(func_count).toString()
					 if(fc.equals("0")){
						   return row1
					 }else{
					 int cc = Integer.parseInt(fc)
					 for(int i=1;i<=cc;i++){
						   int s =i-1;
						   logi.logInfo("counting"+s)
					 
					 row.put("functional_acct_group_row["+s+"]",get_functional_row(acct_no,i))
					 
					 }
					 }
					 row=row.sort()
					 return row
					 }
	   
				 public LinkedHashMap get_functional_row(String acct_no,int i){
					 ConnectDB db = new ConnectDB()
					 LinkedHashMap<String,String> row = new LinkedHashMap<String,String>()
					 String query = "select s.functional_acct_group_no,s.client_functional_group_id from "+
											 "(SELECT agm.group_no as functional_acct_group_no,"+
											 " cag.CLIENT_GROUP_ID as client_functional_group_id,"+
											 " row_number() over (order by agm.group_no) AS seq "+
											 " FROM ariacore.acct_group_map agm "+
											 " JOIN ariacore.client_acct_groups cag "+
											 " ON agm.group_no           = cag.group_no "+
											 " AND agm.client_no         = cag.client_no "+
											 " AND collections_based_ind = 0 "+
											 " WHERE acct_no             = "+acct_no+")s where s.seq = "+i
					 ResultSet rs1 = db.executePlaQuery(query)
					 ResultSetMetaData md1 = rs1.getMetaData();
					 int columns1 = md1.getColumnCount();
					 while (rs1.next()){
					 
						   for(int s=1; s<=columns1; ++s){
								 if(rs1.getObject(s).toString().equals("null")){
									   logi.logInfo("No value in "+rs1.getObject(s))
								 }else{logi.logInfo("Value inserting in hash map")
								 row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
								 logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
								 
							}
					 }
					 String client_functional_acct_group_id = (String)row.get("client_functional_group_id")
					 row.put("client_functional_acct_group_id",client_functional_acct_group_id)
					 row.remove("client_functional_group_id")
									   return row.sort()
			   }
   
					 
				 /**
				  * TO get all the coupon details
				  * @param acct_hierarchy
				  * @return all the fields in the response of get_acct_coupon_details_m
				  */
				 LinkedHashMap md_get_coupon_details_all_m(String input)
				 {
					   ConnectDB dbb = new ConnectDB()
					   VerificationMethods vm = new VerificationMethods()
					   LinkedHashMap h1= new LinkedHashMap()
					   def acct_no,mpi,coupon_cd
					   if(input.contains("a"))
					   {
						   acct_no = vm.getValueFromRequest('get_acct_coupon_details_m+'+input,"//acct_no[1]")
						   mpi =vm.getValueFromRequest('get_acct_coupon_details_m+'+input,"//*/*:master_plan_instance_no[1]")
						   coupon_cd=vm.getValueFromRequest("get_acct_coupon_details_m+"+input,"//coupon_cd")
						
					   }
					   else
					   {
						   acct_no = vm.getValueFromRequest('get_acct_coupon_details_m',"//acct_no[1]")
						   mpi =vm.getValueFromRequest('get_acct_coupon_details_m',"//*/*:master_plan_instance_no[1]")
						   coupon_cd=vm.getValueFromRequest("get_acct_coupon_details_m","//coupon_cd")
					   }
					   
					   logi.logInfo "mpi: " + mpi
					   logi.logInfo "mpi: " + coupon_cd
					   if(!( mpi.toString().equalsIgnoreCase("NoVal")) && !( acct_no.toString().equalsIgnoreCase("NoVal"))&& ( coupon_cd.toString().equalsIgnoreCase("NoVal")))
					   {
		   
							 logi.logInfo(" CALLINGINGINGNIGNNG  MPIIIIII VERIFICATION ")
							 h1=get_coupon_from_mpi(mpi,acct_no)
							 //h1=get_coupon_from_mpi(acct_no)
							 logi.logInfo("HASHMAP final :"+h1)
		   
					   }
		   
					   if(!( coupon_cd.toString().equalsIgnoreCase("NoVal")) && !( acct_no.toString().equalsIgnoreCase("NoVal")) && ( mpi.toString().equalsIgnoreCase("NoVal")))
					   {
		   
							 logi.logInfo(" CALLINGINGINGNIGNNG  COUPON  VERIFICATION with ACCT NUMBER")
							 //h1=get_coupon_from_coupon(coupon_cd,acct_no)
							 logi.logInfo("HASHMAP final :"+h1)
		   
					   }
		   
		   
					   if(!( coupon_cd.toString().equalsIgnoreCase("NoVal")) && !( acct_no.toString().equalsIgnoreCase("NoVal")) && !( mpi.toString().equalsIgnoreCase("NoVal")))
					   {
		   
							 logi.logInfo(" CALLINGINGINGNIGNNG  COUPON WITH ACCT NUM +  MPI  VERIFICATION ")
							 //h1=get_coupon_from_coupon(coupon_cd,acct_no)
							 logi.logInfo("HASHMAP final :"+h1)
						  }
		   
		   
					   if((mpi.toString().equalsIgnoreCase("NoVal")) && (coupon_cd.toString().equalsIgnoreCase("NoVal")) && !( acct_no.toString().equalsIgnoreCase("NoVal")))
					   {
							 logi.logInfo("INTOOOOOO ACCT NU")
		   
							 h1=get_coupon_from_acct(acct_no)
		   
							 logi.logInfo("got here"+h1)
							 //logi.logInfo("HASHMAP before :"+h1)
							  }
		   
					   h1.remove("max_uses")
					   h1.remove("max_applications_per_acct")
					   logi.logInfo("HASHMAP beofre sort")
					   //h1=h1.sort()
					   logi.logInfo("HASHMAP after sort")
					   logi.logInfo("HASHMAP after :"+h1)
					   
					   return h1
				 }
			  
					 def md_GET_ACCT_SURCHARGES_m_rate_details_m(String s){
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m_rate_details_m Start**************")
						def client_no=Constant.mycontext.expand('${Properties#Client_No}')
						 InputMethods im = new InputMethods()
						 LinkedHashMap<String, String> hm = new LinkedHashMap<String, String>()
						 ArrayList<String> list = new ArrayList<String>()
						 int i = 1
						 String acctNo = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null);
						 ConnectDB db = new ConnectDB()
						 String querySurchargeNo = "SELECT surcharge_no FROM ARIACORE.acct_surcharge WHERE ACCT_NO = "+acctNo+" AND CLIENT_NO = "+client_no+""
						 ResultSet rs_SurchargeNo = db.executePlaQuery(querySurchargeNo)
						 while(rs_SurchargeNo.next())
						 {
								 int c =i-1;
								 logi.logInfo("counting"+c)
									 list[i]=rs_SurchargeNo.getObject(1)
									 logi.logInfo "Showing Surcharge number"+list[i]
									 // hm.put("all_surcharges_row["+c+"]",md_GET_ACCT_SURCHARGES_m_Surcharge_Details_row(acctNo,list[i].toString()))
										   i++
						 }
						 logi.logInfo("I value BEFORE"+i)
						 String querySurchargeNo1 ="SELECT SURCHARGE_NO FROM ARIACORE.PLAN_INSTANCE_SURCHARGE WHERE PLAN_INSTANCE_NO IN (SELECT PIM.PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_INSTANCE_MASTER PIM ON PI.PLAN_INSTANCE_NO = PIM.PLAN_INSTANCE_NO WHERE PI.ACCT_NO = "+acctNo+" AND PI.CLIENT_NO = "+client_no+")"
						 ResultSet rs_SurchargeNo1 = db.executePlaQuery(querySurchargeNo1)
						 while(rs_SurchargeNo1.next())
						 {
								 int c =i-1;
								 logi.logInfo("counting1"+c)
									 list[i]=rs_SurchargeNo1.getObject(1)
									 logi.logInfo "Showing Surcharge number1"+list[i]
									 // hm.put("all_surcharges_row["+c+"]",md_GET_ACCT_SURCHARGES_m_Surcharge_Details_row(acctNo,list[i].toString()))
										   i++
						 }
						 logi.logInfo("I value"+i)
						 list.sort()
						 for (int j=1;j<=i-1;j++)
						 {
							   hm.put("surcharge_rate_row["+ (j-1) +"]",md_GET_ACCT_SURCHARGES_m_rate_details_row(acctNo,list[j].toString()))
						 }
						 hm=hm.sort()
						 return hm
						 
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m_rate_details_m Ends**************")
				   }
				   public LinkedHashMap md_GET_ACCT_SURCHARGES_m_rate_details_row(String acct_no, String surcharge_no){
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m_rate_details_row Starts**************")
						 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
						 ConnectDB db = new ConnectDB()
						 LinkedHashMap <String,String> hm = new LinkedHashMap<String,String>()
						 String query = "SELECT a.RATE_SEQ_NO,a.FROM_UNIT,a.RATE_PER_UNIT,a.include_zero,a.rate_sched_is_assigned_ind,a.rate_tier_description FROM(SELECT csp.RATE_SEQ_NO,csp.FROM_UNIT,csp.TO_UNIT,csp.RATE_PER_UNIT,csp.description as rate_tier_description,srs.include_zero_ind as include_zero,srs.def_rate_schedule_ind as rate_sched_is_assigned_ind,srs.surcharge_no FROM ARIACORE.CLIENT_SURCHARGE_PRICE csp,ariacore.surcharge_rate_schedule srs WHERE csp.client_no ="+client_no+" AND csp.surcharge_no= srs.surcharge_no)a WHERE surcharge_no ="+surcharge_no+""
						 ResultSet rs1 = db.executePlaQuery(query)
						 ResultSetMetaData md1 = rs1.getMetaData();
						 int columns1 = md1.getColumnCount();
						 while (rs1.next()){
							   for(int j = 1;j<=columns1;j++){
									 if(rs1.getObject(j).toString().equals("null"))
									 {
										   logi.logInfo("No value in "+rs1.getObject(j).toString())
									 }
									 else
									 {
										   logi.logInfo("Value inserting in hash map")
										   logi.logInfo("Surcharge Rate details" + md1.getColumnName(j).toString())
										   hm.put(md1.getColumnName(j).toLowerCase(),rs1.getObject(j));
									 }
							   }
						 }
			 
						 logi.logInfo("returning the row hash : "+hm)
						 
						 return hm.sort()
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m_rate_details_row Ends**************")
			   }
				   
				   public LinkedHashMap md_GET_ACCT_SURCHARGES_m_Surcharge_Details_m(String s){
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m_rate_details Start**************")
						 InputMethods im = new InputMethods()
						 LinkedHashMap<String, String> hm = new LinkedHashMap<String, String>()
						 ArrayList<String> list = new ArrayList<String>()
						 int i = 1
						 String acctNo = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
						 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
						 ConnectDB db = new ConnectDB()
						 String querySurchargeNo = "SELECT surcharge_no FROM ARIACORE.acct_surcharge WHERE ACCT_NO = "+acctNo+" AND CLIENT_NO = "+client_no+""
						 ResultSet rs_SurchargeNo = db.executePlaQuery(querySurchargeNo)
						 while(rs_SurchargeNo.next())
						 {
								 int c =i-1;
								 logi.logInfo("counting"+c)
									 list[i]=rs_SurchargeNo.getObject(1)
									 logi.logInfo "Showing Surcharge number"+list[i]
									 // hm.put("all_surcharges_row["+c+"]",md_GET_ACCT_SURCHARGES_m_Surcharge_Details_row(acctNo,list[i].toString()))
										   i++
						 }
						 logi.logInfo("I value BEFORE"+i)
						 String querySurchargeNo1 ="SELECT SURCHARGE_NO FROM ARIACORE.PLAN_INSTANCE_SURCHARGE WHERE PLAN_INSTANCE_NO IN (SELECT PIM.PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_INSTANCE_MASTER PIM ON PI.PLAN_INSTANCE_NO = PIM.PLAN_INSTANCE_NO WHERE PI.ACCT_NO = "+acctNo+" AND PI.CLIENT_NO = "+client_no+")"
						 ResultSet rs_SurchargeNo1 = db.executePlaQuery(querySurchargeNo1)
						 while(rs_SurchargeNo1.next())
						 {
								 int c =i-1;
								 logi.logInfo("counting1"+c)
									 list[i]=rs_SurchargeNo1.getObject(1)
									 logi.logInfo "Showing Surcharge number1"+list[i]
									 // hm.put("all_surcharges_row["+c+"]",md_GET_ACCT_SURCHARGES_m_Surcharge_Details_row(acctNo,list[i].toString()))
										   i++
						 }
						 logi.logInfo("I value"+i)
						 list.sort()
						 for (int j=1;j<=i-1;j++)
						 {
							   hm.put("all_surcharges_row["+ (j-1) +"]",md_GET_ACCT_SURCHARGES_m_Surcharge_Details_row(acctNo,list[j].toString()))
						 }
						 return hm.sort()
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m_rate_details Ends**************")
			   }
				   
				   public LinkedHashMap md_GET_ACCT_SURCHARGES_m_Surcharge_Details_row(String acct_no, String surcharge_no){
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m_details_row Starts**************")
						 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
						 ConnectDB db = new ConnectDB()
						 LinkedHashMap <String,String> hm = new LinkedHashMap<String,String>()
						 String surcharges_Query = "SELECT DISTINCT a.surcharge_no, a.surcharge_name, a.description, a.ext_description,a.surcharge_type,a.currency, a.invoice_app_method,a.client_surcharge_id FROM  (SELECT cs.SURCHARGE_NO AS surcharge_no,    cs.surcharge_name     AS surcharge_name,    cs.description        AS description,    cs.ext_desc           AS ext_description,    cs.surcharge_type_cd  AS surcharge_type,    cscm.currency_cd      AS currency,    s2tg.tax_group_no     AS tax_group,    cs.inline_offset_ind  AS invoice_app_method,cs.CLIENT_SURCHARGE_ID as client_surcharge_id  FROM ariacore.client_surcharge cs  JOIN ariacore.client_surcharge_currency_map cscm  ON cscm.surcharge_no = cs.surcharge_no  AND cscm.client_no   = cs.client_no  JOIN ariacore.service_2_tax_group s2tg  ON s2tg.client_no=cs.client_no )a WHERE a.SURCHARGE_NO = "+surcharge_no+""
						 ResultSet rs1 = db.executePlaQuery(surcharges_Query)
						 ResultSetMetaData md1 = rs1.getMetaData();
						 int columns1 = md1.getColumnCount();
						 while (rs1.next()){
							   for(int j = 1;j<=columns1;j++){
									 if(rs1.getObject(j).toString().equals("null"))
									 {
										   logi.logInfo("No value in "+rs1.getObject(j).toString())
									 }
									 else
									 {
										   logi.logInfo("Value inserting in hash map")
										   logi.logInfo("Surcharge Rate details" + md1.getColumnName(j).toString())
										   hm.put(md1.getColumnName(j).toLowerCase(),rs1.getObject(j));
									 }
							   }
						 }
			 
						 logi.logInfo("returning the row hash : "+hm)
						 
						 return hm.sort()
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m_details_row Ends**************")
				 }
				   
				   def md_GET_ACCT_SURCHARGES_m_Surcharge_Plan_Details_m(String s){
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m_Plan_Details Start**************")
						 InputMethods im = new InputMethods()
						 LinkedHashMap<String, String> hm = new LinkedHashMap<String, String>()
						 ArrayList<String> list = new ArrayList<String>()
						 int i = 1
						 String acctNo = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
						 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
						 ConnectDB db = new ConnectDB()
						 String querySurchargeNo = "SELECT surcharge_no FROM ARIACORE.acct_surcharge WHERE ACCT_NO = "+acctNo+" AND CLIENT_NO = "+client_no+""
						 ResultSet rs_SurchargeNo = db.executePlaQuery(querySurchargeNo)
						 while(rs_SurchargeNo.next())
						 {
								 int c =i-1;
								 logi.logInfo("counting"+c)
									 list[i]=rs_SurchargeNo.getObject(1)
									 logi.logInfo "Showing Surcharge number"+list[i]
									 // hm.put("all_surcharges_row["+c+"]",md_GET_ACCT_SURCHARGES_m_Surcharge_Details_row(acctNo,list[i].toString()))
										   i++
						 }
						 logi.logInfo("I value BEFORE"+i)
						 String querySurchargeNo1 ="SELECT SURCHARGE_NO FROM ARIACORE.PLAN_INSTANCE_SURCHARGE WHERE PLAN_INSTANCE_NO IN (SELECT PIM.PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_INSTANCE_MASTER PIM ON PI.PLAN_INSTANCE_NO = PIM.PLAN_INSTANCE_NO WHERE PI.ACCT_NO = "+acctNo+" AND PI.CLIENT_NO = "+client_no+")"
						 ResultSet rs_SurchargeNo1 = db.executePlaQuery(querySurchargeNo1)
						 while(rs_SurchargeNo1.next())
						 {
								 int c =i-1;
								 logi.logInfo("counting1"+c)
									 list[i]=rs_SurchargeNo1.getObject(1)
									 logi.logInfo "Showing Surcharge number1"+list[i]
									 // hm.put("all_surcharges_row["+c+"]",md_GET_ACCT_SURCHARGES_m_Surcharge_Details_row(acctNo,list[i].toString()))
										   i++
						 }
						 logi.logInfo("I value"+i)
						 list.sort()
						 for (int j=1;j<=i-1;j++)
						 {
							   hm.put("surcharge_plan["+ (j-1) +"]",md_GET_ACCT_SURCHARGES_m_Surcharge_Plan_Details_row(acctNo,list[j].toString()))
						 }
						 return hm.sort()
						 
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m_Plan_Details Ends**************")
				   }
				   
				   public LinkedHashMap md_GET_ACCT_SURCHARGES_m_Surcharge_Plan_Details_row(String acct_no, String surcharge_no){
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m_Plan_Details_row Starts**************")
						 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
						 ConnectDB db = new ConnectDB()
						 LinkedHashMap <String,String> hm = new LinkedHashMap<String,String>()
						 String surcharges_Plan_Query = "SELECT ps.plan_no, cp.plan_name FROM ariacore.plan_surcharge ps, ariacore.client_plan cp WHERE ps.client_no = "+client_no+" AND ps.surcharge_no = "+surcharge_no+""
						 ResultSet rs1 = db.executePlaQuery(surcharges_Plan_Query)
						 ResultSetMetaData md1 = rs1.getMetaData();
						 int columns1 = md1.getColumnCount();
						 while (rs1.next()){
							   for(int j = 1;j<=columns1;j++){
									 if(rs1.getObject(j).toString().equals("null"))
									 {
										   logi.logInfo("No value in "+rs1.getObject(j).toString())
									 }
									 else
									 {
										   logi.logInfo("Value inserting in hash map")
										   logi.logInfo("Surcharge Rate details" + md1.getColumnName(j).toString())
										   hm.put(md1.getColumnName(j).toLowerCase(),rs1.getObject(j));
									 }
							   }
						 }
			 
						 logi.logInfo("returning the row hash : "+hm)
						 
						 return hm.sort()
						 logi.logInfo("********md_GET_ACCT_SURCHARGES_m_Plan_Details_row Ends**************")
				   }
				   public LinkedHashMap md_GET_ACCT_BALANCE_m(String str){
					   
										 logi.logInfo("********md_GET_ACCT_BALANCE_m Start**************")
										 LinkedHashMap<String, HashMap<String,String>> row = new LinkedHashMap<String, HashMap<String,String>>()
										 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
										 logi.logInfo("Given Client no" + client_no)
										 ConnectDB db = new ConnectDB()
										 InputMethods im = new InputMethods()
										 String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
										 String q_count = "SELECT COUNT(*) FROM ARIACORE.PLAN_INSTANCE WHERE ACCT_NO = "+acct_no+" AND CLIENT_NO = "+client_no+" AND PARENT_PLAN_INSTANCE_NO IS NULL"
										 String counter = db.executeQueryP2(q_count).toString()
										 int counts = Integer.parseInt(counter)
										 for(int i=1;i<=counts;i++){
											   int s =i-1;
											   logi.logInfo("counting"+s)
										 
													 row.put("master_plan_instances_row["+s+"]",md_GET_ACCT_BALANCE_row(acct_no,i))
										 }
										 logi.logInfo("returning main hash : "+row)
										 return row.sort()
							   }
				 
				 
				 public LinkedHashMap md_GET_ACCT_BALANCE_row(String acct_no, int i){
					   ConnectDB db = new ConnectDB()
					   LinkedHashMap <String,String> row = new LinkedHashMap<String,String>()
					   String query = " select mp.master_plan_instance_no,mp.PLAN_INSTANCE_CDID,mp.master_plan_name,mp.master_plan_instance_desc from(SELECT pi.plan_instance_no AS master_plan_instance_no,"+
											   "  pi.PLAN_INSTANCE_CDID,"+
											   "  cp.plan_name                 AS master_plan_name,"+
											   "  pi.plan_instance_description AS master_plan_instance_desc,"+
											   "  row_number() over (order by pi.plan_instance_no) as seq"+
											   " FROM ariacore.plan_instance pi "+
											   " JOIN ariacore.client_plan cp "+
											   " ON pi.plan_no=cp.plan_no and pi.client_no=cp.client_no"+
											   " JOIN ariacore.plan_instance_master pim"+
											   " ON pi.plan_instance_no=pim.plan_instance_no and pi.client_no=pim.client_no"+
											   " WHERE pi.acct_no      ="+acct_no+")mp where mp.seq = "+i
					   ResultSet rs1 = db.executePlaQuery(query)
					   ResultSetMetaData md1 = rs1.getMetaData();
					   int columns1 = md1.getColumnCount();
					   while (rs1.next()){
					   
							 for(int s=1; s<=columns1; ++s){
								   if(rs1.getObject(s).toString().equals("null")||rs1.getObject(s).toString()==null){
										 logi.logInfo("No value in "+rs1.getObject(s))
								   }else{logi.logInfo("Value inserting in hash map")
								   row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
								   logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s).toLowerCase())
								   
							  }
					   }
					   String client_def = (String)row.get("plan_instance_cdid")
					   if(client_def==null){
							 
							 row.remove("PLAN_INSTANCE_CDID")
					   }
					   else{
							 row.put("client_def_master_plan_instance_id",client_def)
							 row.remove("plan_instance_cdid")
					   }
					   logi.logInfo("returning the row hash : "+row)
					   
										 return row.sort()
				 }
		   
		   
		   
				 /*****************************GEN_INVOICE_M********************************/
				 
					 public String md_gen_invoice_m(String str){
							 InputMethods im = new InputMethods()
							 ConnectDB db = new ConnectDB()
							 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
							 String plan_instance
							 if(str.contains("#")){
								 plan_instance = im.md_GET_ACCT_OR_INSTANCE_NO2_m(str,null)
							}else{
							 plan_instance = im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null)
							}
							   
							 String query = "select max(invoice_no) as invoice_no from ariacore.gl_detail where master_plan_instance_no = " + plan_instance +" and client_no = "+client_no
							 String invoice_no = db.executeQueryP2(query)
							 return invoice_no
					   }
		  
				
					  

					 /**
					  * Used to verify the response of get_acct_plans_m API
					  * @param TESTCASEID
					  * @return LinkedHashMap
					  */
					  public  LinkedHashMap md_verifyGetAcctPlan_m(String TESTCASEID)
					 {
							logi.logInfo("md_verifyGetAcctPlanMApi")
							ConnectDB db = new ConnectDB()
							ArrayList Plan_inst_no = new ArrayList();
							//ConnectDB db1 = new ConnectDB()
							int j =0;
							
							HashMap <String, String> resultHash = new HashMap<String, String>()
							LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
							LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
							com.eviware.soapui.support.XmlHolder outputholder
							def client_no=Constant.mycontext.expand('${Properties#Client_No}')
							logi.logInfo("Client_No" +client_no )
							String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(TESTCASEID,null)
							//def acct_no = getValueFromResponse('create_acct_complete_m',ExPathRpc.CREATE_ACCT_COMPLETE_M_ACCTNO1)
							String qry = "SELECT plan_instance_no from ariacore.plan_instance where acct_no = "+acct_no+" and client_no = "+client_no+" ORDER BY master_plan_instance_no asc"
							ResultSet rl1 = db.executePlaQuery(qry)
						 ResultSetMetaData meta1 = rl1.getMetaData()
						 int columns1 = meta1.getColumnCount();
						 while (rl1.next())
						 {
							 for(int u1=1; u1<=columns1; ++u1)
							 {
								 if(rl1.getObject(u1)!=null)
									 Plan_inst_no.add(rl1.getObject(u1));
							 }
						 }
						 
						 Collections.sort(Plan_inst_no)
					
						 logi.logInfo  "Plan_inst_no in the list "+Plan_inst_no
				
						
							 
									 logi.logInfo("acct_no" +acct_no )
									 //String query = "SELECT TO_CHAR(rs.EFFECTIVE_FROM_DATE,'YYYY-MM-DD') as available_from_dt,TO_CHAR(rs.EFFECTIVE_TO_DATE,'YYYY-MM-DD') as available_from_dt, TO_CHAR(pi.DEPROVISION_DATE,'YYYY-MM-DD') AS plan_deprovisioned_date, cp.plan_no,cp.plan_name,cp.eu_html_long AS PLAN_DESC,pi.plan_instance_no,pi.PLAN_INSTANCE_CDID as client_plan_instance_id,TO_CHAR(pi.PLAN_DATE,'YYYY-MM-DD') AS PLAN_DATE,pi.plan_units,TO_CHAR(pi.LAST_BILL_DATE,'YYYY-MM-DD') AS last_bill_date ,TO_CHAR(pi.NEXT_BILL_DATE,'YYYY-MM-DD') AS next_bill_date,TO_CHAR(pi.LAST_BILL_THRU_DATE,'YYYY-MM-DD') AS bill_thru_date,rs.RECUR_BILLING_INTERVAL as recurring_billing_interval,rs.USAGE_BILLING_INTERVAL as usage_billing_interval,(SELECT PLAN_INSTANCE_CDID FROM plan_instance b WHERE pi.client_no = b.client_no AND pi.PARENT_PLAN_INSTANCE_NO = b.plan_instance_no) as client_parent_plan_instance_id,(SELECT PLAN_INSTANCE_CDID FROM plan_instance b WHERE pi.client_no = b.client_no AND pi.master_PLAN_INSTANCE_NO = b.plan_instance_no) as client_master_plan_instance_id,cp.BILLING_IND,cp.CURRENCY_CD,cp.CLIENT_PLAN_ID,cp.SUPP_PLAN_IND,cp.NO_DISPLAY_IND as display_ind,pi.schedule_no as rate_schedule_no,rs.SCHEDULE_NAME as rate_schedule_name,rs.CLIENT_RATE_SCHEDULE_ID as client_rate_schedule_id,cp.PRORATION_INVOICE_TIMING_CD as proration_invoice_timing_cd,pi.MASTER_PLAN_INSTANCE_NO as master_plan_instance_no,cp.ROLLOVER_MONTHS,cp.rollover_inst_status_cd as rollover_plan_status_cd,cp.ROLLOVER_INST_STATUS_PTYPE as rollover_plan_status_uom_cd,cp.ROLLOVER_PLAN_PTYPE as rollover_plan_uom_cd,cp.ROLLOVER_PLAN_NO,pi.PARENT_PLAN_INSTANCE_NO,pi.status_cd as plan_instance_status_cd,cp.default_plan_inst_status_cd as initial_plan_status_cd,g.label as plan_instance_status_label,TO_CHAR(hist.last_status_change_date,'YYYY-MM-DD') AS plan_instance_status_date,pd.prepaid_plan_ind as prepaid_ind,TO_CHAR(pi.provision_date,'YYYY-MM-DD') AS plan_assignment_date,cp.status_cd as initial_plan_status, cp.rollover_inst_status_cd    AS rollover_plan_status FROM ariacore.plan_instance pi JOIN ariacore.client_plan cp ON pi.plan_no = cp.plan_no and pi.client_no = cp.client_no JOIN ARIACORE.NEW_RATE_SCHEDULES rs ON pi.schedule_no  =rs.schedule_no and pi.client_no=rs.client_no JOIN ariacore.global_plan_status_code g on g.plan_status_cd = pi.status_cd  JOIN (SELECT (MAX (h.create_date)) last_status_change_date,h.client_no,h.plan_instance_no FROM ariacore.plan_instance_plan_hist h,ariacore.plan_instance pi2 WHERE h.client_no                = pi2.client_no AND h.plan_instance_no           = pi2.plan_instance_no AND pi2.acct_no= "+acct_no+" AND NVL (h.old_status_cd, -123) <> NVL (h.new_status_cd, -123) GROUP BY h.client_no,h.plan_instance_no) hist ON hist.client_no = pi.client_no AND hist.plan_instance_no = pi.plan_instance_no join ariacore.plan_defaults pd on pi.plan_no=pd.plan_no and pi.client_no = pd.client_no where pi.client_no =" +client_no +" and pi.acct_no= " +acct_no+" order by pi.master_plan_instance_no asc"
									 //String query = "SELECT TO_CHAR(PIM.DUNNING_DEGRADE_DATE,'YYYY-MM-DD') AS dunning_degrade_date,pim.dunning_step,pim.dunning_state, pim.bill_day,TO_CHAR(rs.EFFECTIVE_FROM_DATE,'YYYY-MM-DD') AS available_from_dt,TO_CHAR(rs.EFFECTIVE_FROM_DATE,'YYYY-MM-DD') as available_from_dt,TO_CHAR(rs.EFFECTIVE_TO_DATE,'YYYY-MM-DD') as available_from_dt, TO_CHAR(pi.DEPROVISION_DATE,'YYYY-MM-DD') AS plan_deprovisioned_date, cp.plan_no,cp.plan_name,cp.eu_html_long AS PLAN_DESC,pi.plan_instance_no,pi.PLAN_INSTANCE_CDID as client_plan_instance_id,TO_CHAR(pi.PLAN_DATE,'YYYY-MM-DD') AS PLAN_DATE,pi.plan_units,TO_CHAR(pi.LAST_BILL_DATE,'YYYY-MM-DD') AS last_bill_date ,TO_CHAR(pi.NEXT_BILL_DATE,'YYYY-MM-DD') AS next_bill_date,TO_CHAR(pi.LAST_BILL_THRU_DATE,'YYYY-MM-DD') AS bill_thru_date,rs.RECUR_BILLING_INTERVAL as recurring_billing_interval,rs.USAGE_BILLING_INTERVAL as usage_billing_interval,(SELECT PLAN_INSTANCE_CDID FROM plan_instance b WHERE pi.client_no = b.client_no AND pi.PARENT_PLAN_INSTANCE_NO = b.plan_instance_no) as client_parent_plan_instance_id,(SELECT PLAN_INSTANCE_CDID FROM plan_instance b WHERE pi.client_no = b.client_no AND pi.master_PLAN_INSTANCE_NO = b.plan_instance_no) as client_master_plan_instance_id,cp.BILLING_IND,cp.CURRENCY_CD,cp.CLIENT_PLAN_ID,cp.SUPP_PLAN_IND,cp.NO_DISPLAY_IND as display_ind,pi.schedule_no as rate_schedule_no,rs.SCHEDULE_NAME as rate_schedule_name,rs.CLIENT_RATE_SCHEDULE_ID as client_rate_schedule_id,cp.PRORATION_INVOICE_TIMING_CD as proration_invoice_timing_cd,pi.MASTER_PLAN_INSTANCE_NO as master_plan_instance_no,cp.ROLLOVER_MONTHS,cp.rollover_inst_status_cd as rollover_plan_status_cd,cp.ROLLOVER_INST_STATUS_PTYPE as rollover_plan_status_uom_cd,cp.ROLLOVER_PLAN_PTYPE as rollover_plan_uom_cd,cp.ROLLOVER_PLAN_NO,pi.PARENT_PLAN_INSTANCE_NO,pi.status_cd as plan_instance_status_cd,cp.default_plan_inst_status_cd as initial_plan_status_cd,g.label as plan_instance_status_label,TO_CHAR(hist.last_status_change_date,'YYYY-MM-DD') AS plan_instance_status_date,pd.prepaid_plan_ind as prepaid_ind,TO_CHAR(pi.provision_date,'YYYY-MM-DD') AS plan_assignment_date,cp.status_cd as initial_plan_status, cp.rollover_inst_status_cd    AS rollover_plan_status FROM ariacore.plan_instance pi JOIN ariacore.plan_instance_master pim on pi.plan_instance_no=pim.plan_instance_no JOIN ariacore.client_plan cp ON pi.plan_no = cp.plan_no and pi.client_no = cp.client_no JOIN ARIACORE.NEW_RATE_SCHEDULES rs ON pi.schedule_no  =rs.schedule_no and pi.client_no=rs.client_no JOIN ariacore.global_plan_status_code g on g.plan_status_cd = pi.status_cd  JOIN (SELECT (MAX (h.create_date)) last_status_change_date,h.client_no,h.plan_instance_no FROM ariacore.plan_instance_plan_hist h,ariacore.plan_instance pi2 WHERE h.client_no          = pi2.client_no AND h.plan_instance_no = pi2.plan_instance_no AND pi2.acct_no= "+acct_no+" AND NVL (h.old_status_cd, -123) <> NVL (h.new_status_cd, -123) GROUP BY h.client_no,h.plan_instance_no) hist ON hist.client_no = pi.client_no AND hist.plan_instance_no = pi.plan_instance_no join ariacore.plan_defaults pd on pi.plan_no=pd.plan_no and pi.client_no = pd.client_no where pi.client_no =" +client_no +" and pi.acct_no= " +acct_no+" order by pi.MASTER_PLAN_INSTANCE_NO asc"
									 //String query = "SELECT PIM.bill_day,TO_CHAR(PIM.DUNNING_DEGRADE_DATE,'YYYY-MM-DD') AS dunning_degrade_date,pim.dunning_step,pim.dunning_state, TO_CHAR(rs.EFFECTIVE_FROM_DATE,'YYYY-MM-DD') as available_from_dt,TO_CHAR(rs.EFFECTIVE_TO_DATE,'YYYY-MM-DD') as available_from_dt, TO_CHAR(pi.DEPROVISION_DATE,'YYYY-MM-DD') AS plan_deprovisioned_date, cp.plan_no,cp.plan_name,cp.eu_html_long AS PLAN_DESC,pi.plan_instance_no,pi.PLAN_INSTANCE_CDID as client_plan_instance_id,TO_CHAR(pi.PLAN_DATE,'YYYY-MM-DD') AS PLAN_DATE,pi.plan_units,TO_CHAR(pi.LAST_BILL_DATE,'YYYY-MM-DD') AS last_bill_date ,TO_CHAR(pi.NEXT_BILL_DATE,'YYYY-MM-DD') AS next_bill_date,TO_CHAR(pi.LAST_BILL_THRU_DATE,'YYYY-MM-DD') AS bill_thru_date,rs.RECUR_BILLING_INTERVAL as recurring_billing_interval,rs.USAGE_BILLING_INTERVAL as usage_billing_interval,(SELECT PLAN_INSTANCE_CDID FROM plan_instance b WHERE pi.client_no = b.client_no AND pi.PARENT_PLAN_INSTANCE_NO = b.plan_instance_no) as client_parent_plan_instance_id,(SELECT PLAN_INSTANCE_CDID FROM plan_instance b WHERE pi.client_no = b.client_no AND pi.master_PLAN_INSTANCE_NO = b.plan_instance_no) as client_master_plan_instance_id,cp.BILLING_IND,cp.CURRENCY_CD,cp.CLIENT_PLAN_ID,cp.SUPP_PLAN_IND,cp.NO_DISPLAY_IND as display_ind,pi.schedule_no as rate_schedule_no,rs.SCHEDULE_NAME as rate_schedule_name,rs.CLIENT_RATE_SCHEDULE_ID as client_rate_schedule_id,cp.PRORATION_INVOICE_TIMING_CD as proration_invoice_timing_cd,pi.MASTER_PLAN_INSTANCE_NO as master_plan_instance_no,cp.ROLLOVER_MONTHS,cp.rollover_inst_status_cd as rollover_plan_status_cd,cp.ROLLOVER_INST_STATUS_PTYPE as rollover_plan_status_uom_cd,cp.ROLLOVER_PLAN_PTYPE as rollover_plan_uom_cd,cp.ROLLOVER_PLAN_NO,pi.PARENT_PLAN_INSTANCE_NO,pi.status_cd as plan_instance_status_cd,cp.default_plan_inst_status_cd as initial_plan_status_cd,g.label as plan_instance_status_label,TO_CHAR(hist.last_status_change_date,'YYYY-MM-DD') AS plan_instance_status_date,pd.prepaid_plan_ind as prepaid_ind,TO_CHAR(pi.provision_date,'YYYY-MM-DD') AS plan_assignment_date,cp.status_cd as initial_plan_status, cp.rollover_inst_status_cd    AS rollover_plan_status FROM ariacore.plan_instance pi LEFT JOIN ariacore.plan_instance_master pim on pi.plan_instance_no=pim.plan_instance_no JOIN ariacore.client_plan cp ON pi.plan_no = cp.plan_no and pi.client_no = cp.client_no JOIN ARIACORE.NEW_RATE_SCHEDULES rs ON pi.schedule_no  =rs.schedule_no and pi.client_no=rs.client_no JOIN ariacore.global_plan_status_code g on g.plan_status_cd = pi.status_cd  JOIN (SELECT (MAX (h.create_date)) last_status_change_date,h.client_no,h.plan_instance_no FROM ariacore.plan_instance_plan_hist h,ariacore.plan_instance pi2 WHERE h.client_no                = pi2.client_no AND h.plan_instance_no           = pi2.plan_instance_no AND pi2.acct_no= "+acct_no+" AND NVL (h.old_status_cd, -123) <> NVL (h.new_status_cd, -123) GROUP BY h.client_no,h.plan_instance_no) hist ON hist.client_no = pi.client_no AND hist.plan_instance_no = pi.plan_instance_no join ariacore.plan_defaults pd on pi.plan_no=pd.plan_no and pi.client_no = pd.client_no where pi.client_no ="+client_no +" and pi.acct_no= "+acct_no+" ORDER BY pi.master_plan_instance_no asc";
									 
									String query = "SELECT pim.bill_day,CASE WHEN pi.PARENT_PLAN_INSTANCE_NO is null THEN TO_CHAR(PIM.DUNNING_DEGRADE_DATE,'YYYY-MM-DD') end AS dunning_degrade_date,CASE WHEN pi.PARENT_PLAN_INSTANCE_NO is null THEN pim.dunning_step END as dunning_step,CASE WHEN pi.PARENT_PLAN_INSTANCE_NO is null THEN pim.dunning_state END as dunning_state, TO_CHAR(rs.EFFECTIVE_FROM_DATE,'YYYY-MM-DD') as available_from_dt,TO_CHAR(rs.EFFECTIVE_TO_DATE,'YYYY-MM-DD') as available_from_dt, TO_CHAR(pi.DEPROVISION_DATE,'YYYY-MM-DD') AS plan_deprovisioned_date, cp.plan_no,cp.plan_name,cp.eu_html_long AS PLAN_DESC,pi.plan_instance_no,pi.PLAN_INSTANCE_CDID as client_plan_instance_id,TO_CHAR(pi.PLAN_DATE,'YYYY-MM-DD') AS PLAN_DATE,pi.plan_units,TO_CHAR(pi.LAST_BILL_DATE,'YYYY-MM-DD') AS last_bill_date ,TO_CHAR(pi.NEXT_BILL_DATE,'YYYY-MM-DD') AS next_bill_date,TO_CHAR(pi.LAST_BILL_THRU_DATE,'YYYY-MM-DD') AS bill_thru_date,rs.RECUR_BILLING_INTERVAL as recurring_billing_interval,rs.USAGE_BILLING_INTERVAL as usage_billing_interval,(SELECT PLAN_INSTANCE_CDID FROM plan_instance b WHERE pi.client_no = b.client_no AND pi.PARENT_PLAN_INSTANCE_NO = b.plan_instance_no) as client_parent_plan_instance_id,(SELECT PLAN_INSTANCE_CDID FROM plan_instance b WHERE pi.client_no = b.client_no AND pi.master_PLAN_INSTANCE_NO = b.plan_instance_no) as client_master_plan_instance_id,cp.BILLING_IND,cp.CURRENCY_CD,cp.CLIENT_PLAN_ID,cp.SUPP_PLAN_IND,cp.NO_DISPLAY_IND as display_ind,pi.schedule_no as rate_schedule_no,rs.SCHEDULE_NAME as rate_schedule_name,rs.CLIENT_RATE_SCHEDULE_ID as client_rate_schedule_id,cp.PRORATION_INVOICE_TIMING_CD as proration_invoice_timing_cd,pi.MASTER_PLAN_INSTANCE_NO as master_plan_instance_no,cp.ROLLOVER_MONTHS,cp.rollover_inst_status_cd as rollover_plan_status_cd,cp.ROLLOVER_INST_STATUS_PTYPE as rollover_plan_status_uom_cd,cp.ROLLOVER_PLAN_PTYPE as rollover_plan_uom_cd,cp.ROLLOVER_PLAN_NO,pi.PARENT_PLAN_INSTANCE_NO,pi.status_cd as plan_instance_status_cd,cp.default_plan_inst_status_cd as initial_plan_status_cd,g.label as plan_instance_status_label,TO_CHAR(hist.last_status_change_date,'YYYY-MM-DD') AS plan_instance_status_date,pd.prepaid_plan_ind as prepaid_ind,TO_CHAR(pi.provision_date,'YYYY-MM-DD') AS plan_assignment_date,cp.status_cd as initial_plan_status, cp.rollover_inst_status_cd    AS rollover_plan_status FROM ariacore.plan_instance pi JOIN ariacore.plan_instance_master pim ON pim.plan_instance_no=nvl(pi.master_plan_instance_no,pi.plan_instance_no) JOIN ariacore.client_plan cp ON pi.plan_no = cp.plan_no and pi.client_no = cp.client_no JOIN ARIACORE.NEW_RATE_SCHEDULES rs ON pi.schedule_no  =rs.schedule_no and pi.client_no=rs.client_no JOIN ariacore.global_plan_status_code g on g.plan_status_cd = pi.status_cd  JOIN (SELECT (MAX (h.create_date)) last_status_change_date,h.client_no,h.plan_instance_no FROM ariacore.plan_instance_plan_hist h,ariacore.plan_instance pi2 WHERE h.client_no                = pi2.client_no AND h.plan_instance_no           = pi2.plan_instance_no AND pi2.acct_no= "+acct_no+" AND NVL (h.old_status_cd, -123) <> NVL (h.new_status_cd, -123) GROUP BY h.client_no,h.plan_instance_no) hist ON hist.client_no = pi.client_no AND hist.plan_instance_no = pi.plan_instance_no join ariacore.plan_defaults pd on pi.plan_no=pd.plan_no and pi.client_no = pd.client_no where pi.client_no ="+client_no +" and pi.acct_no= "+acct_no+" ORDER BY pi.plan_instance_no asc";
									 
									 
									 //String query1="select bill_day from ariacore.plan_instance pi join ariacore.plan_instance_master pim on pi.plan_instance_no=pim.plan_instance_no where acct_no="+acct_no+" and pi.plan_instance_no = "+plan_inst+""
									 //String query1 = "select bill_day from ariacore.plan_instance pi join ariacore.plan_instance_master pim on pi.plan_instance_no=pim.plan_instance_no where acct_no="+acct_no+" and pi.plan_instance_no = (select master_plan_instance_no from ariacore.plan_instance where plan_instance_no = "+plan_inst+")"
									 /* String bill_day=db.executeQueryP2(query1)
									 String query2="select pim.dunning_state from ariacore.plan_instance pi join ariacore.plan_instance_master pim on pi.plan_instance_no=pim.plan_instance_no where acct_no ="+acct_no+" and pi.plan_instance_no = "+plan_inst+""
									 String dunning_state=db.executeQueryP2(query2)
									 String query3="select pim.dunning_step from ariacore.plan_instance pi join ariacore.plan_instance_master pim on pi.plan_instance_no=pim.plan_instance_no where acct_no ="+acct_no+" and pi.plan_instance_no = "+plan_inst+""
									 String dunning_step=db.executeQueryP2(query3)
									 String query4="select TO_CHAR(PIM.DUNNING_DEGRADE_DATE,'YYYY-MM-DD') AS dunning_degrade_date from ariacore.plan_instance pi join ariacore.plan_instance_master pim on pi.plan_instance_no=pim.plan_instance_no where acct_no ="+acct_no+" and pi.plan_instance_no = "+plan_inst+""
									 String dunning_date=db.executeQueryP2(query4)*/
									 ResultSet rs=db.executePlaQuery(query)
									 ResultSetMetaData md = rs.getMetaData();
									 int columns = md.getColumnCount();
									 logi.logInfo("Columns value after resultset")
									 ArrayList list = new ArrayList(150);
									 ResultSet rs1 = rs
									 
									 while (rs.next())
									 {
										 
										 logi.logInfo("Count"+j)
										 logi.logInfo("SUPP_PLAN_IND"+rs.getObject('SUPP_PLAN_IND'))
										 
										 
										 for(int i=1; i<=columns; i++)
										 {
											 
											 if((rs.getObject(i))!=null)
											 {
												 
													 row.put((md.getColumnName(i)).toLowerCase(),(rs.getObject(i)));
													 logi.logInfo("rate_schedule_no column"+md.getColumnName(i))
													 logi.logInfo("rate_schedule_no"+rs.getObject('rate_schedule_no'))
													 (rs.getObject('rate_schedule_no')!=null)? row.put("rate_sched_is_default_ind",1):row.put("rate_sched_is_default_ind",0);
												 
												 
											 }
											 
										 }
										 if(rs.getObject('SUPP_PLAN_IND')== 1)
										 {
											 row.remove("rollover_plan_status")
											 row.remove("rollover_plan_status_uom_cd")
											 row.remove("initial_plan_status")
											 row.remove("initial_plan_status_cd")
											 row.remove("rollover_plan_status_cd")
											 
										 }
										 
										/* row.put("bill_day",bill_day);
										 logi.logInfo("bill_day"+bill_day)*/
										 logi.logInfo("Display"+(row.get("display_ind")))
										 if((row.get("display_ind").toString().trim())=="0")
										 {
											 logi.logInfo("Display Indicator conversion")
											 row.put("display_ind","1")
										 }
										 /*if((rs.getObject('plan_instance_no'))==(rs.getObject('master_plan_instance_no')))
										 {
											 logi.logInfo("Resultset"+rs)
											 String plan_instance = (rs.getObject('master_plan_instance_no')).toString()
											 
											 row.put("dunning_state",dunning_state)
											 logi.logInfo("Query1Completed")
											 
											 logi.logInfo("dunning_step"+dunning_step)
											 if(dunning_step!=null)
											 {
												 row.put("dunning_step",dunning_step)
											 }
											 logi.logInfo("Query2Completed")
											 
											 if(dunning_date!=null)
											 {
												 row.put("dunning_degrade_date",dunning_date)
											 }
											 logi.logInfo("Query3Completed")
										 }*/
										 outer.put("acct_plans_m_row["+j+"]",row.sort())
										 logi.logInfo("bill_day_uk")
										 row = new LinkedHashMap<String, String>();
										 j++
									 }
						 
									 return outer.sort()
						}
		 
		 
		 /**
		* To verify the get_acct_hierarchy_details API response
		* @param TESTCASEID
		* @return LinkedHashMap
		*/
	   public  LinkedHashMap md_verifyGetAcctHie_m(String TESTCASEID)
	   {
		   logi.logInfo("md_verifyGetAcctHie_m")
		   ConnectDB db = new ConnectDB()
		   ArrayList<String> list = new ArrayList<String>()
		   
		   int i =1
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   logi.logInfo("Client_No" +client_no )
		   String acct_no = getValueFromRequest("get_acct_hierarchy_details_m","//acct_no")
		   logi.logInfo("acct_no" +acct_no )
		   String hie_filter = getValueFromRequest("get_acct_hierarchy_details_m","//hierarchy_filter")
		   logi.logInfo("hie_filter" +hie_filter )
		   String include_current_acct = getValueFromRequest("get_acct_hierarchy_details_m","//include_current_acct")
		   logi.logInfo("include_current_acct" +include_current_acct )
		   String include_supp_plans = getValueFromRequest("get_acct_hierarchy_details_m","//include_supp_plans")
		   logi.logInfo("include_supp_plans" +include_supp_plans )
		   String include_billing_groups = getValueFromRequest("get_acct_hierarchy_details_m","//include_billing_groups")
		   logi.logInfo("include_billing_groups" +include_billing_groups )
		   String include_payment_methods = getValueFromRequest("get_acct_hierarchy_details_m","//include_payment_methods")
		   logi.logInfo("include_payment_methods" +include_payment_methods )
		   String child = "SELECT acct_no FROM ariacore.acct A WHERE A.senior_acct_no IS NOT NULL AND A.acct_no <> " + acct_no +"START WITH A.acct_no ="+ acct_no +" AND A.client_no ="+ client_no+ "CONNECT BY NOCYCLE(PRIOR A.acct_no = A.senior_acct_no AND PRIOR A.client_no = A.client_no)"
		   String parent = "SELECT acct_no FROM ariacore.acct A WHERE A.acct_no <> "+acct_no+" START WITH acct_no = "+acct_no+" AND A.client_no = "+client_no+" CONNECT BY NOCYCLE(PRIOR A.senior_acct_no = A.acct_no AND PRIOR A.client_no = A.client_no) ORDER SIBLINGS BY acct_no "
		   LinkedHashMap <String, String> inner = new LinkedHashMap<String, String>();
		   LinkedHashMap <String, String> outer = new LinkedHashMap<String, String>();
		   if(hie_filter == "0" && include_current_acct == "1")
		   {
				   ResultSet rs=db.executePlaQuery(child)
				   while(rs.next())
				   {
					   
					   list[i]=rs.getObject(1)
					   i++
				   }
				   
				   
				   ResultSet rs1=db.executePlaQuery(parent)
				   while(rs1.next())
				   {
					   list[i]=rs1.getObject(1)
					   i++
				   }
				   list[i]=acct_no.toString()
				   logi.logInfo("List Working")
				   
				   
				   
		   }
		   if(hie_filter == "1" && include_current_acct == "1")
		   {
				   ResultSet rs=db.executePlaQuery(child)
				   while(rs.next())
				   {
					   
					   list[i]=rs.getObject(1)
					   i++
				   }
				   list[i]=acct_no.toString()
				   
		   }
		   if(hie_filter == "2" && include_current_acct == "1")
		   {
				   ResultSet rs=db.executePlaQuery(parent)
				   while(rs.next())
				   {
					   
					   list[i]=rs.getObject(1)
					   i++
				   }
			   list[i]=acct_no.toString()
		   }
		   if(hie_filter == "0" && include_current_acct == "0")
		   {
				   ResultSet rs=db.executePlaQuery(child)
				   while(rs.next())
				   {
					   
					   list[i]=rs.getObject(1)
					   i++
				   }
				   ResultSet rs1=db.executePlaQuery(parent)
				   while(rs.next())
				   {
					   list[i]=rs1.getObject(1)
					   i++
				   }
				   
		   }
		   if(hie_filter == "1" && include_current_acct == "0")
		   {
				   ResultSet rs=db.executePlaQuery(child)
				   while(rs.next())
				   {
					   
					   list[i]=rs.getObject(1)
					   i++
				   }
				   
				   
		   }
		   if(hie_filter == "2" && include_current_acct == "0")
		   {
				   ResultSet rs=db.executePlaQuery(parent)
				   while(rs.next())
				   {
					   
					   list[i]=rs.getObject(1)
					   i++
				   }
			   
		   }
		   logi.logInfo("TotalCount" + i)
		   int sizecount = list.size()
		   logi.logInfo("sizecount" + sizecount)
		   for(int j=1;j<=sizecount-1;j++)
		   {
			   logi.logInfo("List"+list[j])
			   inner = md_verifyGetAcctDetail_m(list[j].toString(),include_supp_plans,include_billing_groups,include_payment_methods)
			   outer.put("acct_hierarchy_dtls_row["+j+"]",inner.sort())
			   inner = new LinkedHashMap<String, String>();
		   }
		   return outer.sort()
	   }
	   /**
		* To verify the get_acct_hierarchy_details API response
		* @param TESTCASEID
		* @return LinkedHashMap
		*/
	   public  LinkedHashMap md_verifyGetAcctHie1_m(String TESTCASEID)
	   {
		   logi.logInfo("md_verifyGetAcctHie_m")
		   ConnectDB db = new ConnectDB()
		   ArrayList<String> list = new ArrayList<String>()
		   
		   int i =1
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   logi.logInfo("Client_No" +client_no )
		   String acct_no = getValueFromRequest("get_acct_hierarchy_details_m+a","//acct_no")
		   logi.logInfo("acct_no" +acct_no )
		   String hie_filter = getValueFromRequest("get_acct_hierarchy_details_m+a","//hierarchy_filter")
		   logi.logInfo("hie_filter" +hie_filter )
		   String include_current_acct = getValueFromRequest("get_acct_hierarchy_details_m+a","//include_current_acct")
		   logi.logInfo("include_current_acct" +include_current_acct )
		   String include_supp_plans = getValueFromRequest("get_acct_hierarchy_details_m+a","//include_supp_plans")
		   logi.logInfo("include_supp_plans" +include_supp_plans )
		   String include_billing_groups = getValueFromRequest("get_acct_hierarchy_details_m+a","//include_billing_groups")
		   logi.logInfo("include_billing_groups" +include_billing_groups )
		   String include_payment_methods = getValueFromRequest("get_acct_hierarchy_details_m+a","//include_payment_methods")
		   logi.logInfo("include_payment_methods" +include_payment_methods )
		   String child = "SELECT acct_no FROM ariacore.acct A WHERE A.senior_acct_no IS NOT NULL AND A.acct_no <> " + acct_no +"START WITH A.acct_no ="+ acct_no +" AND A.client_no ="+ client_no+ "CONNECT BY NOCYCLE(PRIOR A.acct_no = A.senior_acct_no AND PRIOR A.client_no = A.client_no)"
		   String parent = "SELECT acct_no FROM ariacore.acct A WHERE A.acct_no <> "+acct_no+" START WITH acct_no = "+acct_no+" AND A.client_no = "+client_no+" CONNECT BY NOCYCLE(PRIOR A.senior_acct_no = A.acct_no AND PRIOR A.client_no = A.client_no) ORDER SIBLINGS BY acct_no "
		   LinkedHashMap <String, String> inner = new LinkedHashMap<String, String>();
		   LinkedHashMap <String, String> outer = new LinkedHashMap<String, String>();
		   if(hie_filter == "0" && include_current_acct == "1")
		   {
				   ResultSet rs=db.executePlaQuery(child)
				   while(rs.next())
				   {
					   
					   list[i]=rs.getObject(1)
					   i++
				   }
				   
				   
				   ResultSet rs1=db.executePlaQuery(parent)
				   while(rs1.next())
				   {
					   list[i]=rs1.getObject(1)
					   i++
				   }
				   list[i]=acct_no.toString()
				   logi.logInfo("List Working")
				   
				   
				   
		   }
		   if(hie_filter == "1" && include_current_acct == "1")
		   {
				   ResultSet rs=db.executePlaQuery(child)
				   while(rs.next())
				   {
					   
					   list[i]=rs.getObject(1)
					   i++
				   }
				   list[i]=acct_no.toString()
				   
		   }
		   if(hie_filter == "2" && include_current_acct == "1")
		   {
				   ResultSet rs=db.executePlaQuery(parent)
				   while(rs.next())
				   {
					   
					   list[i]=rs.getObject(1)
					   i++
				   }
			   list[i]=acct_no.toString()
		   }
		   if(hie_filter == "0" && include_current_acct == "0")
		   {
				   ResultSet rs=db.executePlaQuery(child)
				   while(rs.next())
				   {
					   
					   list[i]=rs.getObject(1)
					   i++
				   }
				   ResultSet rs1=db.executePlaQuery(parent)
				   while(rs.next())
				   {
					   list[i]=rs1.getObject(1)
					   i++
				   }
				   
		   }
		   if(hie_filter == "1" && include_current_acct == "0")
		   {
				   ResultSet rs=db.executePlaQuery(child)
				   while(rs.next())
				   {
					   
					   list[i]=rs.getObject(1)
					   i++
				   }
				   
				   
		   }
		   if(hie_filter == "2" && include_current_acct == "0")
		   {
				   ResultSet rs=db.executePlaQuery(parent)
				   while(rs.next())
				   {
					   
					   list[i]=rs.getObject(1)
					   i++
				   }
		   }
		   logi.logInfo("TotalCount" + i)
		   int sizecount = list.size()
		   logi.logInfo("sizecount" + sizecount)
		   for(int j=1;j<=sizecount-1;j++)
		   {
			   logi.logInfo("List"+list[j])
			   inner = md_verifyGetAcctDetail_m(list[j].toString(),include_supp_plans,include_billing_groups,include_payment_methods)
			   outer.put("acct_hierarchy_dtls_row["+j+"]",inner.sort())
			   inner = new LinkedHashMap<String, String>();
		   }
		   return outer.sort()
	   }
	   
	   /**
		* To verify the get_acct_details of acct hierarchy
		* @param acct_no
		* @param suppind
		* @param billingind
		* @param paymentind
		* @return LinkedHashMap
		*/
	   public  LinkedHashMap md_verifyGetAcctDetail_m(String acct_no,String suppind,String billingind,String paymentind)
	   {
		   logi.logInfo("md_verifyGetAcctPlanMApi")
		   ConnectDB db = new ConnectDB()
		   int q=1,r=1,s=1,t=1,z=1,u=1
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   logi.logInfo("Client_No" +client_no )
		   logi.logInfo("acct_no" +acct_no )
		   LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
		   //Acct Part
		   //String query = "select a.acct_no as acct_no,a.userid as userid,ad.first_name,ad.middle_initial,ad.last_name,ad.company_name,ad.address1,ad.address2,ad.CITY,ad.STATE as state_prov,ad.zip as postal_cd,ad.country as country_cd,a.status_cd,a.notify_method,a.senior_acct_no,(select userid from ariacore.acct b where b.acct_no = a.senior_acct_no) as senior_acct_user_id,a.TEST_ACCT_IND,TO_CHAR(a.ACCT_START_DATE,'YYYY-MM-DD') as acct_start_date,a.CURRENCY_CD as acct_currency,a.acct_balance from ariacore.acct a join ariacore.acct_address ad on a.acct_address_seq = ad.address_seq where a.client_no= " +client_no+ "and a.acct_no ="+ acct_no
		   String query = "SELECT distinct a.acct_no AS acct_no, lc.locale_name as acct_locale_name,  a.locale_no as acct_locale_no,  ad.address_seq as acct_contact_no, a.userid       AS userid, ad.first_name, ad.middle_initial, ad.last_name, ad.company_name, ad.address1,ad.address2, ad.CITY,ad.STATE   AS state_prov, ad.zip     AS postal_cd,ad.country AS country_cd,a.status_cd,a.notify_method, a.senior_acct_no,(SELECT userid FROM ariacore.acct b WHERE b.acct_no = a.senior_acct_no ) AS senior_acct_user_id, a.TEST_ACCT_IND, TO_CHAR(a.ACCT_START_DATE,'YYYY-MM-DD') AS acct_start_date, a.CURRENCY_CD  AS acct_currency, a.acct_balance FROM ariacore.acct a JOIN ariacore.acct_address ad ON a.acct_address_seq = ad.address_seq JOIN ariacore.plan_instance pi ON pi.acct_no=a.acct_no  and pi.client_no=a.client_no JOIN  ARIACORE.GL_DETAIL GLD ON pi.client_no=gld.client_no and pi.plan_instance_no = gld.plan_instance_no JOIN ARIACORE.GL   ON GL.INVOICE_NO=GLD.INVOICE_NO AND  GL.CLIENT_NO=GLD.CLIENT_NO AND GL.acct_no = pi.acct_no JOIN ARIACORE.CLIENT_SERVICE CS ON  CS.SERVICE_NO= gld.SERVICE_NO and cs.client_no=gld.client_no JOIN ariacore.locale_config lc ON lc.locale_no= a.locale_no WHERE a.client_no     = " +client_no+ "and a.acct_no ="+ acct_no
		   ResultSet rs=db.executePlaQuery(query)
		   ResultSetMetaData md = rs.getMetaData();
		   int columns = md.getColumnCount();
		   while (rs.next())
		   {
			   for(int i=1; i<=columns; i++)
			   {
				   if((rs.getObject(i))!=null)
				   {
					   row.put((md.getColumnName(i)).toLowerCase(),(rs.getObject(i)));
				   }
			   }
		   }
		   //Collecttion acct_group
		   String query1 = "select agm.group_no as collection_acct_group_no, cag.client_group_id from ariacore.client_acct_groups cag join ariacore.acct_group_map agm on cag.group_no=agm.group_no and cag.client_no =agm.client_no where agm.acct_no = "+acct_no+" and agm.client_no ="+client_no
		   ResultSet rs1=db.executePlaQuery(query1)
		   while (rs1.next())
		   {
			   row.put("collections_acct_group_no",(rs1.getObject(1)))
			   row.put("client_collections_acct_group_id",(rs1.getObject(2)))
		   }
		   //Billing group
		   if(billingind== "1")
		   {
			   String query2 = "SELECT DECODE(pt.payment_terms_no, NULL, 'Methods', 'Terms') payment_option, DECODE(abg.ean_gln_number, NULL, 'Net Terms', 'EAN/GLN') payment_terms_type, abg.billing_group_no, abg.profile_name AS billing_group_name, abg.profile_description    AS billing_group_description, abg.profile_cdid AS client_def_billing_group_id, abg.primary_pmt_method_seq AS primary_payment_method_id, ad.FIRST_NAME AS stmt_first_name, ad.MIDDLE_INITIAL AS stmt_middle_initial, ad.last_name AS stmt_last_name, ad.company_name AS stmt_company_name, ad.address1 AS stmt_address1, ad.address2 AS stmt_address2, ad.CITY AS stmt_city, ad.STATE AS stmt_state_prov, ad.COUNTRY AS stmt_country, ad.ZIP AS stmt_postal_cd, ad.INTL_PHONE  AS stmt_phone FROM ariacore.acct_billing_group abg LEFT JOIN ariacore.payment_terms pt ON pt.payment_terms_no = abg.payment_terms_no JOIN ariacore.acct_address ad ON abg.statement_address_seq = ad.address_seq AND ABG.CLIENT_NO            =AD.CLIENT_NO where abg.client_no = "+client_no+"and abg.acct_no ="+acct_no
			   ResultSet rs2=db.executePlaQuery(query2)
			   ResultSetMetaData md1 = rs2.getMetaData();
			   int columns1 = md1.getColumnCount();
			   while (rs2.next())
			   {
				   for(int j=1; j<=columns1; j++)
				   {
					   if((rs2.getObject(j))!=null)
					   {
						   if(r==1)
						   {
							   row.put((md1.getColumnName(j)).toLowerCase(),(rs2.getObject(j)));
						   }
						   else
						   {
							   row.put((md1.getColumnName(j)).toLowerCase() +"["+r+"]",(rs2.getObject(j)));
						   }
						   
					   }
				   }
				   r++
			   }
		   }
		   
		   //Payment array
		   if((billingind== "1" && paymentind== "1"))
		   {
			   String query4 = "select bi.bill_address_seq as bill_contact_no,abg.billing_group_no,bi.seq_num AS payment_method_no,ad.first_name as bill_first_name,ad.middle_initial as bill_middle_name,ad.last_name as bill_last_name,ad.company_name as bill_company_name,ad.address1 as bill_address1,ad.address2 as bill_address2,ad.city as bill_city,ad.state as bill_state_prov,ad.country as bill_country,ad.zip as bill_postal_cd,ad.intl_phone as bill_phone,bi.pay_method as pay_method_type,bi.suffix as suffix,bi.CC_EXPIRE_MM,bi.CC_EXPIRE_YYYY,bi.BANK_ROUTING_NUM,cc.cc_type from ariacore.acct_billing_group abg left join ariacore.payment_terms pt on pt.payment_terms_no = abg.payment_terms_no join ariacore.billing_info bi on abg.acct_no = bi.acct_no and abg.primary_pmt_method_seq =seq_num left join ariacore.acct_address ad on bi.bill_address_seq=ad.address_seq left join ariacore.credit_cards cc on bi.cc_id = cc.cc_id where abg.client_no = "+client_no+ " and abg.acct_no = "+acct_no +"  union select bi.bill_address_seq as bill_contact_no,abg.billing_group_no,bi.seq_num AS payment_method_no,ad.first_name as bill_first_name,ad.middle_initial as bill_middle_name,ad.last_name as bill_last_name,ad.address1 as bill_address1,ad.address2 as bill_address2,ad.company_name as bill_company_name,ad.city as bill_city,ad.state as bill_state_prov,ad.country as bill_country,ad.zip as bill_postal_cd,ad.intl_phone as bill_phone,bi.pay_method as pay_method_type,bi.suffix as suffix,bi.CC_EXPIRE_MM,bi.CC_EXPIRE_YYYY,bi.BANK_ROUTING_NUM,cc.cc_type from ariacore.acct_billing_group abg join ariacore.billing_info bi on abg.acct_no = bi.acct_no and abg.secondary_pmt_method_seq =seq_num left join ariacore.acct_address ad on bi.bill_address_seq=ad.address_seq left join ariacore.credit_cards cc on bi.cc_id = cc.cc_id where abg.client_no =" +client_no +" and abg.acct_no = " +acct_no
			   ResultSet rs4=db.executePlaQuery(query4)
			   ResultSetMetaData md4 = rs4.getMetaData();
			   int columns4 = md4.getColumnCount();
			   while (rs4.next())
			   {
			   for(int l=1; l<=columns4; l++)
			   {
				   if((rs4.getObject(l))!=null)
				   {
					   if(t==1)
					   {
						   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
					   }
					   else
					   {
						   row.put((md4.getColumnName(l)).toLowerCase()+"["+t+"]",(rs4.getObject(l)));
					   }
					   if((md4.getColumnName(l)).equals("BILLING_GROUP_NO"))
					   {
						   row.put((md4.getColumnName(l)).toLowerCase()+"["+r+"]",(rs4.getObject(l)));
					   }
					   if((md4.getColumnName(l)).equals("CLIENT_DEF_BILLING_GROUP_ID"))
					   {
						   row.put((md4.getColumnName(l)).toLowerCase()+"["+r+"]",(rs4.getObject(l)));
					   }
				   }
			   }
			   t++
			   r++
			   }
		   }
		   //Master Plan instance
		   String query3 = "SELECT pi.plan_instance_no as master_plan_instance_no,pi.plan_instance_cdid as client_master_plan_instance_id,cp.CLIENT_PLAN_ID as client_master_plan_id,cp.plan_no as master_plan_no,pim.DUNNING_GROUP_NO as dunning_group_no,dg.DUNNING_GROUP_CDID as client_dunning_group_id,pim.billing_group_no as billing_group_no,abg.PROFILE_CDID as client_billing_group_id,pi.status_cd as master_plan_instance_status,g.label as mp_instance_status_label,pi.PLAN_UNITS as master_plan_units,pim.RESPONSIBILITY_LEVEL_CD as resp_level_cd,cp.DEFAULT_RATE_SCHED_NO as alt_rate_schedule_no,rs.CLIENT_RATE_SCHEDULE_ID as client_alt_rate_schedule_id,pim.bill_day,TO_CHAR(pi.LAST_ARREARS_BILL_THRU_DATE,'YYYY-MM-DD') as last_arrears_bill_thru_date,TO_CHAR(pi.LAST_BILL_DATE,'YYYY-MM-DD') as last_bill_date,TO_CHAR(pi.LAST_BILL_THRU_DATE,'YYYY-MM-DD') as last_bill_thru_date,TO_CHAR(pi.NEXT_BILL_DATE,'YYYY-MM-DD') as next_bill_date,TO_CHAR(pi.PLAN_DATE,'YYYY-MM-DD') as plan_date,TO_CHAR(pi.STATUS_DATE,'YYYY-MM-DD') as status_date,pim.STACK_BALANCE as master_plan_instance_balance,rs.RECUR_BILLING_INTERVAL AS recurring_billing_interval,rs.USAGE_BILLING_INTERVAL AS usage_billing_interval,cp.default_plan_inst_status_cd AS initial_plan_status,cp.rollover_inst_status_cd AS rollover_plan_status,cp.ROLLOVER_INST_STATUS_PTYPE AS rollover_plan_status_uom_cd,cp.INITIAL_FREE_PTYPE as init_free_period_uom_cd,pim.dunning_state,pi.plan_instance_description as master_plan_description FROM ariacore.plan_instance pi JOIN ariacore.plan_instance_master pim ON pi.plan_instance_no = pim.plan_instance_no join ariacore.client_plan cp on pi.plan_no =cp.plan_no AND PI.CLIENT_NO=CP.CLIENT_NO join ariacore.acct_dunning_group dg on pim.dunning_group_no = dg.dunning_group_no AND PIM.CLIENT_NO=DG.CLIENT_NO join ariacore.acct_billing_group abg on pim.billing_group_no = abg.billing_group_no AND PIM.CLIENT_NO=ABG.CLIENT_NO join ariacore.global_plan_status_code g ON g.plan_status_cd = pi.status_cd JOIN ARIACORE.NEW_RATE_SCHEDULES rs ON pi.schedule_no =rs.schedule_no AND PI.CLIENT_NO=RS.CLIENT_NO WHERE pi.client_no= "+client_no+" AND pi.acct_no ="+ acct_no
		   ResultSet rs13=db.executePlaQuery(query3)
		   ResultSetMetaData md2 = rs13.getMetaData();
		   int columns2 = md2.getColumnCount();
		   logi.logInfo("columns2" +columns2 )
		   int cntr = 0;
		   while(rs13.next()){
			   cntr++	   }
		   logi.logInfo("Row count " +cntr)
		   //Service No and Client Service Id
		   String query4 = "Select ps.service_no,cs.client_service_id FROM ariacore.plan_instance pi join ariacore.plan_services ps on ps.client_no = pi.client_no and ps.plan_no = pi.plan_no join ariacore.client_service cs on cs.client_no = ps.client_no and cs.service_no = ps.service_no WHERE pi.client_no= "+client_no+" AND pi.acct_no ="+ acct_no
		   ResultSet rs14=db.executePlaQuery(query4)
		   ResultSetMetaData md3 = rs14.getMetaData();
		   int columns3 = md3.getColumnCount();
		   logi.logInfo("columns3" +columns3 )
		   logi.logInfo("Out to While loop")
		   while (rs14.next())
		   {   logi.logInfo("Into While loop")
			   for(int x=1; x<=columns3; x++)
			   {
				   logi.logInfo("Inside for loop  "+x)
				   if((rs14.getObject(x))!=null)
				   {
					   logi.logInfo("Inside first IF")
					   if(x>0)
					   {
						   logi.logInfo("Inside 2nddd IF")
						   row.put("service_no ["+x+"]",(rs14.getObject(x)));
					   }
					   else
					   {
					   row.put((md3.getColumnName(x)).toLowerCase(),(rs14.getObject(x)));
					   }
					   if(md3.getColumnName(x).equalsIgnoreCase("service_no")){
						   logi.logInfo("md3.getColumnName(x)"+md3.getColumnName(x)+"Inside If condition   "+x)
								  }
				   } }
		   }
				  /*while (rs14.next())
			   {
				   logi.logInfo("***Entering into while loop****")
				   for(int j=1; j<=columns3; j++)
				   {
					   if((rs14.getObject(j))!=null)
					   {
						   logi.logInfo("***Entering 1st If condition ***")
						   if(r==1)
						   {
							   row.put((md3.getColumnName(j)).toLowerCase(),(rs14.getObject(j)));
							   logi.logInfo("md3.getColumnName(j)"+md3.getColumnName(j)+"Inside If condition   "+j)
							   if(md3.getColumnName(j).equals("service_no")){
								 logi.logInfo("md3.getColumnName(j)"+md3.getColumnName(j)+"hiiyhi   "+j)}
						   }
						   else
						   {
							   row.put((md3.getColumnName(j)).toLowerCase() +"["+r+"]",(rs14.getObject(j)));
							   if(md3.getColumnName(j).equalsIgnoreCase("service_no")){
								   logi.logInfo("md3.getColumnName(j)"+md3.getColumnName(j)+"hiiyhi  else "+j)}
						   }
					   }
				   }
				   r++
			   }		*/
		   int rowId =1
		   while(cntr>=1)
		   {
			   logi.logInfo("Biiing group count " +r)
			   ResultSet rs3=db.executePlaQuery(query3)
			   for(int i=1;i<=rowId; i++){
			   rs3.next()
			   }
			   logi.logInfo("columns244")
			   for(int k=1; k<=columns2; k++)
			   {
				   if((rs3.getObject(k))!=null)
				   {
					   logi.logInfo("Inner")
					   if(s==1)
					   {
						   row.put((md2.getColumnName(k)).toLowerCase(),(rs3.getObject(k)));
						   
						   logi.logInfo("(md2.getColumnName(k)).toLowerCase()" + (rs3.getObject(k)))
						   if((md2.getColumnName(k)).equals("MASTER_PLAN_DESCRIPTION"))
						   {
							   row.put("master_plan_instance_description",(row.get("master_plan_description").toString()));
							   row.remove("master_plan_description")
						   }
					   }
					   
					   else
					   {
						   if((md2.getColumnName(k)).equals("ALT_RATE_SCHEDULE_NO"))
						   {
							   row.put((md2.getColumnName(k)).toLowerCase() +"["+s+"]" ,(rs3.getObject(k)));
						   }
						   else if((md2.getColumnName(k)).equals("CLIENT_ALT_RATE_SCHEDULE_ID"))
						   {
							   row.put((md2.getColumnName(k)).toLowerCase() +"["+s+"]" ,(rs3.getObject(k)));
						   }
						   else if((md2.getColumnName(k)).equals("LAST_ARREARS_BILL_THRU_DATE"))
						   {
							   row.put((md2.getColumnName(k)).toLowerCase() +"["+s+"]" ,(rs3.getObject(k)));
						   }
						   else if((md2.getColumnName(k)).equals("LAST_BILL_THRU_DATE"))
						   {
							   row.put((md2.getColumnName(k)).toLowerCase() +"["+s+"]" ,(rs3.getObject(k)));
						   }
						   else if((md2.getColumnName(k)).equals("NEXT_BILL_DATE"))
						   {
							   row.put((md2.getColumnName(k)).toLowerCase() +"["+s+"]" ,(rs3.getObject(k)));
						   }
						   else if((md2.getColumnName(k)).equals("PLAN_DATE"))
						   {
							   row.put((md2.getColumnName(k)).toLowerCase() +"["+s+"]" ,(rs3.getObject(k)));
						   }
						   else if((md2.getColumnName(k)).equals("RECURRING_BILLING_INTERVAL"))
						   {
							   row.put((md2.getColumnName(k)).toLowerCase() +"["+s+"]" ,(rs3.getObject(k)));
						   }
						   else if((md2.getColumnName(k)).equals("ROLLOVER_PLAN_STATUS"))
						   {
							   row.put((md2.getColumnName(k)).toLowerCase() +"["+s+"]" ,(rs3.getObject(k)));
						   }
						   else if((md2.getColumnName(k)).equals("ROLLOVER_PLAN_STATUS_UOM_CD"))
						   {
							   row.put((md2.getColumnName(k)).toLowerCase() +"["+s+"]" ,(rs3.getObject(k)));
						   }
						   else if((md2.getColumnName(k)).equals("STATUS_DATE"))
						   {
							   row.put((md2.getColumnName(k)).toLowerCase() +"["+s+"]" ,(rs3.getObject(k)));
						   }
						   else if((md2.getColumnName(k)).equals("USAGE_BILLING_INTERVAL"))
						   {
							   row.put((md2.getColumnName(k)).toLowerCase() +"["+s+"]" ,(rs3.getObject(k)));
						   }
						   else if((md2.getColumnName(k)).equals("LAST_BILL_DATE"))
						   {
							   row.put((md2.getColumnName(k)).toLowerCase() +"["+s+"]" ,(rs3.getObject(k)));
						   }
						   else
						   {
							   row.put((md2.getColumnName(k)).toLowerCase()+"["+u+"]",(rs3.getObject(k)));
							   if((md2.getColumnName(k)).equals("MASTER_PLAN_DESCRIPTION"))
							   {
								   row.put("master_plan_instance_description" +"["+u+"]",(row.get("master_plan_description").toString()));
								   row.remove("master_plan_description")
							   }
						   }
						   
					   }
					   if((md2.getColumnName(k)).equals("BILLING_GROUP_NO"))
					   {
						   row.put((md2.getColumnName(k)).toLowerCase()+"["+r+"]",(rs3.getObject(k)));
					   }
				   }
			   }
			   s++
			   String master_plan_instance_no = rs3.getObject('master_plan_instance_no').toString()
			   ConnectDB db1 = new ConnectDB()
			   if(suppind== "1")
			   {
				   s=md_verifyGetSuppPlanDetail(acct_no,s,row,master_plan_instance_no,db1)
				   logi.logInfo("Returned S Value " + s)
			   }
			   u++
			   cntr--
			   rowId++
			   r++
			   logi.logInfo("Biiing group count incremeted " +r)
		   }
		   
		   if(suppind== "1")
		   {
		   //supp_plan
			   //String query9 = "select cs.service_no, cs.client_service_id,pi.plan_instance_no as supp_plan_instance_no,pi.PLAN_INSTANCE_CDID as client_supp_plan_instance_id,cp.plan_no as supp_plan_no,cp.CLIENT_PLAN_ID as client_supp_plan_id,pi.plan_units as supp_plan_units,pi.status_cd  AS supp_plan_instance_status_cd, g.label AS supp_plan_instance_status, (SELECT PLAN_INSTANCE_CDID FROM plan_instance b WHERE pi.client_no = b.client_no AND pi.PARENT_PLAN_INSTANCE_NO = b.plan_instance_no) AS client_parent_plan_instance_id,(SELECT PLAN_INSTANCE_NO FROM plan_instance b WHERE pi.client_no= b.client_no AND pi.PARENT_PLAN_INSTANCE_NO = b.plan_instance_no) AS parent_plan_instance_no from ariacore.plan_instance pi JOIN ARIACORE.GL_DETAIL GLD ON pi.client_no         =gld.client_no AND pi.plan_instance_no = gld.plan_instance_no JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO AND GL.acct_no  = pi.acct_no JOIN ARIACORE.CLIENT_SERVICE CS ON CS.SERVICE_NO= gld.SERVICE_NO AND cs.client_no=gld.client_no join ariacore.client_plan cp on pi.plan_no = cp.plan_no and pi.client_no=cp.client_no JOIN ARIACORE.NEW_RATE_SCHEDULES rs ON pi.schedule_no =rs.schedule_no AND PI.CLIENT_NO=RS.CLIENT_NO  JOIN ariacore.global_plan_status_code g ON g.plan_status_cd = pi.status_cd where pi.client_no =" +client_no+ " and pi.acct_no = " + acct_no +" and cp.SUPP_PLAN_IND =1"
			   String query9 = "select pi.plan_instance_no as supp_plan_instance_no,pi.PLAN_INSTANCE_CDID as client_supp_plan_instance_id,cp.plan_no as supp_plan_no,cp.CLIENT_PLAN_ID as client_supp_plan_id,pi.plan_units as supp_plan_units,pi.status_cd  AS supp_plan_instance_status_cd, g.label AS supp_plan_instance_status, (SELECT PLAN_INSTANCE_CDID FROM plan_instance b WHERE pi.client_no = b.client_no AND pi.PARENT_PLAN_INSTANCE_NO = b.plan_instance_no) AS client_parent_plan_instance_id,(SELECT PLAN_INSTANCE_NO FROM plan_instance b WHERE pi.client_no= b.client_no AND pi.PARENT_PLAN_INSTANCE_NO = b.plan_instance_no) AS parent_plan_instance_no from ariacore.plan_instance pi join ariacore.client_plan cp on pi.plan_no = cp.plan_no and pi.client_no=cp.client_no JOIN ARIACORE.NEW_RATE_SCHEDULES rs ON pi.schedule_no =rs.schedule_no AND PI.CLIENT_NO=RS.CLIENT_NO  JOIN ariacore.global_plan_status_code g ON g.plan_status_cd = pi.status_cd where pi.client_no =" +client_no+ " and pi.acct_no = " +acct_no+" and cp.SUPP_PLAN_IND =1"
			   ResultSet rs9=db.executePlaQuery(query9)
			   ResultSetMetaData md9 = rs9.getMetaData();
			   int columns9 = md9.getColumnCount();
			   logi.logInfo("columns5"+columns9)
			   while (rs9.next())
			   {
				   for(int n=1; n<=columns9; n++)
				   {
					   if((rs9.getObject(n))!=null)
					   {
						   if(z==1)
						   {
							   row.put((md9.getColumnName(n)).toLowerCase(),(rs9.getObject(n)));
							   
						   }
						   else
						   {
							   row.put((md9.getColumnName(n)).toLowerCase()+"["+z+"]",(rs9.getObject(n)));
						   }
					   }
					   
				   }
			   z++
			   
			   }
		   }
		   //senior Acct
		   String query10 = " select acct_no from ariacore.acct where senior_acct_no = "+acct_no+" and client_no ="+client_no
		   ResultSet rs10=db.executePlaQuery(query10)
		   while (rs10.next())
		   {
			   if((rs10.getObject(1))!=null)
			   {
				   row.put("acct_no[2]",(rs10.getObject(1)))
			   }
			   
		   }
		   return row.sort()
	   }
	   
	   /**
		* To verify the supplemental plan for acct hierarchy method
		* @param acct_no
		* @param s
		* @param row
		* @param master_plan_instance_no
		* @param dataBase
		* @return s
		*/
	   public  int md_verifyGetSuppPlanDetail(String acct_no,int s,LinkedHashMap row,String master_plan_instance_no,ConnectDB dataBase)
	   {
		   logi.logInfo("Calling Supp plan method")
		   logi.logInfo("s Value" + s)
   //		ConnectDB db = new ConnectDB()
		   int q=1
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   
		   
		   String acct_no1 = acct_no
		   String query5 = "select pi.plan_instance_no as supp_plan_instance_no,pi.PLAN_INSTANCE_CDID as client_supp_plan_instance_id,cp.plan_no as supp_plan_no,cp.CLIENT_PLAN_ID as client_supp_plan_id,pi.plan_units as supp_plan_units,pi.SCHEDULE_NO as alt_rate_schedule_no,rs.CLIENT_RATE_SCHEDULE_ID as client_alt_rate_schedule_id,TO_CHAR(pi.LAST_ARREARS_BILL_THRU_DATE,'YYYY-MM-DD') AS last_arrears_bill_thru_date, TO_CHAR(pi.LAST_BILL_DATE,'YYYY-MM-DD') AS last_bill_date, TO_CHAR(pi.LAST_BILL_THRU_DATE,'YYYY-MM-DD') AS last_bill_thru_date, TO_CHAR(pi.NEXT_BILL_DATE,'YYYY-MM-DD')AS next_bill_date,TO_CHAR(pi.PLAN_DATE,'YYYY-MM-DD') AS plan_date,TO_CHAR(pi.STATUS_DATE,'YYYY-MM-DD') AS status_date,pi.status_cd  AS supp_plan_instance_status_cd, g.label AS supp_plan_instance_status,pi.MASTER_PLAN_INSTANCE_NO AS master_plan_instance_no, (SELECT PLAN_INSTANCE_CDID FROM plan_instance b WHERE pi.client_no = b.client_no AND pi.PARENT_PLAN_INSTANCE_NO = b.plan_instance_no) AS client_parent_plan_instance_id, rs.RECUR_BILLING_INTERVAL AS recurring_billing_interval, rs.USAGE_BILLING_INTERVAL AS usage_billing_interval, cp.rollover_inst_status_cd AS rollover_plan_status, cp.ROLLOVER_INST_STATUS_PTYPE AS rollover_plan_status_uom_cd from ariacore.plan_instance pi join ariacore.client_plan cp on pi.plan_no = cp.plan_no and pi.client_no=cp.client_no JOIN ARIACORE.NEW_RATE_SCHEDULES rs ON pi.schedule_no =rs.schedule_no AND PI.CLIENT_NO=RS.CLIENT_NO JOIN ariacore.global_plan_status_code g ON g.plan_status_cd = pi.status_cd where pi.client_no =" +client_no+ " and pi.acct_no = " + acct_no1 +" and cp.SUPP_PLAN_IND =1 and pi.master_plan_instance_no =" +master_plan_instance_no
		   ResultSet rs5=dataBase.executePlaQuery(query5)
		   ResultSetMetaData md5 = rs5.getMetaData();
		   int columns5 = md5.getColumnCount();
		   while (rs5.next())
		   {
		   for(int m=1; m<=columns5; m++)
		   {
			   if((rs5.getObject(m))!=null)
			   {
				   if(q==1)
				   {
					   if((md5.getColumnName(m)).equals("SUPP_PLAN_INSTANCE_NO"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase(),(rs5.getObject(m)));
					   }
					   if((md5.getColumnName(m)).equals("CLIENT_SUPP_PLAN_INSTANCE_ID"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase(),(rs5.getObject(m)));
					   }
					   if((md5.getColumnName(m)).equals("SUPP_PLAN_NO"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase(),(rs5.getObject(m)));
					   }
					   if((md5.getColumnName(m)).equals("CLIENT_SUPP_PLAN_ID"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase(),(rs5.getObject(m)));
					   }
					   if((md5.getColumnName(m)).equals("SUPP_PLAN_UNITS"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase(),(rs5.getObject(m)));
					   }
					   if((md5.getColumnName(m)).equals("CLIENT_PARENT_PLAN_INSTANCE_ID"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase(),(rs5.getObject(m)));
					   }
					   if((md5.getColumnName(m)).equals("SUPP_PLAN_INSTANCE_STATUS_CD"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase(),(rs5.getObject(m)));
					   }
					   if((md5.getColumnName(m)).equals("SUPP_PLAN_INSTANCE_STATUS"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase(),(rs5.getObject(m)));
					   }
					   
				   }
				   else
				   {
						   if((md5.getColumnName(m)).equals("SUPP_PLAN_INSTANCE_NO"))
						   {
							   row.put((md5.getColumnName(m)).toLowerCase() +"["+q+"]" ,(rs5.getObject(m)))
						   }
						   if((md5.getColumnName(m)).equals("CLIENT_SUPP_PLAN_INSTANCE_ID"))
						   {
							   row.put((md5.getColumnName(m)).toLowerCase() +"["+q+"]" ,(rs5.getObject(m)))
						   }
						   if((md5.getColumnName(m)).equals("SUPP_PLAN_NO"))
						   {
							   row.put((md5.getColumnName(m)).toLowerCase() +"["+q+"]" ,(rs5.getObject(m)))
						   }
						   if((md5.getColumnName(m)).equals("CLIENT_SUPP_PLAN_ID"))
						   {
							   row.put((md5.getColumnName(m)).toLowerCase() +"["+q+"]" ,(rs5.getObject(m)))
						   }
						   if((md5.getColumnName(m)).equals("SUPP_PLAN_UNITS"))
						   {
							   row.put((md5.getColumnName(m)).toLowerCase() +"["+q+"]" ,(rs5.getObject(m)))
						   }
						   if((md5.getColumnName(m)).equals("CLIENT_PARENT_PLAN_INSTANCE_ID"))
						   {
							   row.put((md5.getColumnName(m)).toLowerCase() +"["+q+"]" ,(rs5.getObject(m)))
						   }
						   if((md5.getColumnName(m)).equals("SUPP_PLAN_INSTANCE_STATUS_CD"))
						   {
							   row.put((md5.getColumnName(m)).toLowerCase() +"["+q+"]" ,(rs5.getObject(m)))
						   }
						   if((md5.getColumnName(m)).equals("SUPP_PLAN_INSTANCE_STATUS"))
						   {
							   row.put((md5.getColumnName(m)).toLowerCase() +"["+q+"]" ,(rs5.getObject(m)))
						   }
						   
						   
						   logi.logInfo("Entering Loop")
				   }
				   
					   if((md5.getColumnName(m)).equals("ALT_RATE_SCHEDULE_NO"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase() +"["+s+"]" ,(rs5.getObject(m)));
					   }
					   else if((md5.getColumnName(m)).equals("CLIENT_ALT_RATE_SCHEDULE_ID"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase() +"["+s+"]" ,(rs5.getObject(m)));
					   }
					   else if((md5.getColumnName(m)).equals("LAST_BILL_DATE"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase() +"["+s+"]" ,(rs5.getObject(m)));
					   }
					   else if((md5.getColumnName(m)).equals("LAST_ARREARS_BILL_THRU_DATE"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase() +"["+s+"]" ,(rs5.getObject(m)));
					   }
					   else if((md5.getColumnName(m)).equals("LAST_BILL_THRU_DATE"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase() +"["+s+"]" ,(rs5.getObject(m)));
					   }
					   else if((md5.getColumnName(m)).equals("NEXT_BILL_DATE"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase() +"["+s+"]" ,(rs5.getObject(m)));
					   }
					   else if((md5.getColumnName(m)).equals("PLAN_DATE"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase() +"["+s+"]" ,(rs5.getObject(m)));
					   }
					   else if((md5.getColumnName(m)).equals("RECURRING_BILLING_INTERVAL"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase() +"["+s+"]" ,(rs5.getObject(m)));
					   }
					   else if((md5.getColumnName(m)).equals("ROLLOVER_PLAN_STATUS"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase() +"["+s+"]" ,(rs5.getObject(m)));
					   }
					   else if((md5.getColumnName(m)).equals("ROLLOVER_PLAN_STATUS_UOM_CD"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase() +"["+s+"]" ,(rs5.getObject(m)));
					   }
					   else if((md5.getColumnName(m)).equals("STATUS_DATE"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase() +"["+s+"]" ,(rs5.getObject(m)));
					   }
					   else if((md5.getColumnName(m)).equals("USAGE_BILLING_INTERVAL"))
					   {
						   row.put((md5.getColumnName(m)).toLowerCase() +"["+s+"]" ,(rs5.getObject(m)));
					   }
			   }
			   
		   }
		   q++
		   s++
		   }
		   //rs5.close()
		   return s
	   }
	   
	   /**
		* To update the value for DUPLICATE_SURCHARGE_ASSIGNMENT_BEHAVIOR paramater
		* @param paramval
		* @return
		*/
	   public String SurchargeUpdateAllowDuplicated(String paramval)
	   {
			   logi.logInfo("md_SurchargeUpdateRejectDuplicated")
			   ConnectDB db = new ConnectDB()
			   int i =0
			   String clientNo=Constant.client_no
			   logi.logInfo("Client_No" +clientNo )
			   //String query ="UPDATE ariacore.aria_client_params SET param_val  = '"+paramval.toUpperCase()+"' WHERE param_name  ='DUPLICATE_SURCHARGE_ASSIGNMENT_BEHAVIOR' and  client_no = " +clientNo
			   ResultSet rs=db.executePlaQuery("UPDATE ariacore.aria_client_params SET param_val  = '"+paramval.toUpperCase()+"' WHERE param_name  ='DUPLICATE_SURCHARGE_ASSIGNMENT_BEHAVIOR' and  client_no = " +clientNo);
			   rs.close();
			   String paramvalue ="test"
			   //String query2="select param_val from ariacore.aria_client_params where param_name = '"+paramval.toUpperCase()+"' and client_no ="+clientNo
			   //String paramvalue=db.executeQueryP2(query)
			   logi.logInfo("Query Executed")
			   
	   }
	   
	   /**
		* To verify the billing_group for get_billing_group_details_m API
		* @param TESTCASEID
		* @return LinkedHashMap
		*/
	   public  LinkedHashMap md_Verify_BillingGroupDetail(String TESTCASEID)
	   {
		   logi.logInfo("md_verifyGetAcctPlanMApi")
		   ConnectDB db = new ConnectDB()
		   int j=1
		   LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
		   LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
		   LinkedList<String> billing_group_no= new LinkedList<String>()
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   def acct_no = getValueFromResponse('create_acct_complete_m',ExPathRpc.CREATE_ACCT_COMPLETE_M_ACCTNO1)
		   logi.logInfo("acct_no" +acct_no )
		   String querys ="select billing_group_no from ariacore.acct_billing_group where acct_no= "+ acct_no +"order by 1"
		   ResultSet rs4s=db.executePlaQuery(querys)
		   ResultSetMetaData md4s = rs4s.getMetaData();
		   int columns4s = md4s.getColumnCount();
		   while (rs4s.next())
		   {
			   for(int l=1; l<=columns4s; l++)
			   {
				   if((rs4s.getObject(l))!=null)
				   {
					   
						   //row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
					   billing_group_no.add(rs4s.getObject(l))
				   
				   }
			   }
		   }
		   logi.logInfo("billing_group_no" +billing_group_no.toString() )
		
		   for(int kz=0;kz<billing_group_no.size();kz++)
		   
		  {
			  int flag=Integer.parseInt(db.executeQueryP2("select count(*) from ariacore.plan_instance_master where billing_group_no="+billing_group_no.get(kz)).toString())
			  int flag2=Integer.parseInt(db.executeQueryP2("select count(primary_pmt_method_seq) from ariacore.acct_billing_group where billing_group_no="+billing_group_no.get(kz)).toString())
			  if(flag>=1&&flag2>=1){
				  for(int got=0;got<flag;got++)
				  {
		   String query ="SELECT DECODE(pt.payment_terms_no, NULL, 'Methods', 'Terms') payment_option,DECODE(abg.ean_gln_number, NULL, 'Net Terms', 'EAN/GLN') payment_terms_type, abg.profile_description AS billing_group_description,abg.profile_name AS billing_group_name,abg.billing_group_no,abg.profile_cdid AS client_billing_group_id,pi.plan_instance_cdid AS client_plan_instance_id,pi.plan_instance_no,abg.primary_pmt_method_seq AS primary_payment_method_no,abg.secondary_pmt_method_seq as backup_payment_method_no,ad.first_name AS stmt_first_name,ad.last_name AS stmt_last_name,ad.company_name AS stmt_company_name,ad.address1 AS stmt_address1,ad.address2 AS stmt_address2,ad.address3 AS stmt_address3,ad.city AS stmt_city,ad.state AS stmt_state_prov,ad.country AS stmt_country,ad.zip AS stmt_postal_cd,ad.intl_phone AS stmt_phone,abg.status_cd AS status,ad.MIDDLE_INITIAL as stmt_mi,ad.locality as stmt_locality from ariacore.plan_instance pi join ariacore.plan_instance_master pim on pi.plan_instance_no=pim.plan_instance_no join ariacore.acct_billing_group abg on pim.billing_group_no =abg.billing_group_no left join ariacore.payment_terms pt on pt.payment_terms_no = abg.payment_terms_no join ariacore.billing_info bi on abg.PRIMARY_PMT_METHOD_SEQ= bi.seq_num and abg.acct_no = bi.acct_no join ariacore.acct_address ad on abg.STATEMENT_ADDRESS_SEQ = ad.address_seq  where pi.acct_no = "+ acct_no + " and pi.client_no ="+client_no+" and abg.billing_group_no ="+billing_group_no.get(kz)+" order by PLAN_INSTANCE_NO"
		   ResultSet rs4=db.executePlaQuery(query)
		   ResultSetMetaData md4 = rs4.getMetaData();
		   int columns4 = md4.getColumnCount();
		   int k=1,lk=1;
		   while (rs4.next())
		   {
			   for(int l=1; l<=columns4; l++)
			   {
				   if((rs4.getObject(l))!=null)
				   {
					   if(md4.getColumnName(l).toLowerCase().equals("client_plan_instance_id"))
						 {
							//for now I have hardcoded the two fields but in case if we need it dynamicaly in future we can change as per the requirment
							 if(k>1)row.put((md4.getColumnName(l)).toLowerCase()+"["+k.toString()+"]",(rs4.getObject(l)));
							 else{   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));}
							 k++;
						 }
						 else if(md4.getColumnName(l).toLowerCase().equals("plan_instance_no"))
						 {
							 if(lk>1)row.put((md4.getColumnName(l)).toLowerCase()+"["+lk.toString()+"]",(rs4.getObject(l)));
							 else{   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));}
							 lk++;
						 }
						else{   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));}
					   
				   
				   }
			   }
			  
		   
		   }
		   }
		   }
			  else
			  {
				  
				  String query ="select DECODE(pt.payment_terms_no, NULL, 'Methods', 'Terms') payment_option,DECODE(abg.ean_gln_number, NULL, 'Net Terms', 'EAN/GLN') payment_terms_type,abg.billing_group_no,abg.profile_name as billing_group_name,abg.profile_description as billing_group_description, abg.profile_cdid as client_billing_group_id,abg.primary_pmt_method_seq as primary_payment_method_no,aa.first_name AS stmt_first_name,  aa.first_name  AS stmt_first_name,aa.last_name  AS stmt_last_name, aa.COMPANY_NAME AS stmt_company_name, aa.ADDRESS1 AS stmt_address1, aa.ADDRESS2   AS stmt_address2,aa.ADDRESS3   AS stmt_address3, aa.city  AS stmt_city,  aa.STATE  AS stmt_state_prov,  aa.COUNTRY  AS stmt_country, aa.ZIP  AS stmt_postal_cd,  aa.INTL_PHONE  AS stmt_phone,abg.status_cd as status from ariacore.acct_billing_group abg join ariacore.acct_address aa on abg.STATEMENT_ADDRESS_SEQ = aa.address_seq left join ariacore.payment_terms pt on pt.payment_terms_no = abg.payment_terms_no where abg.acct_no = "+ acct_no + " and abg.client_no ="+client_no+"and abg.billing_group_no ="+billing_group_no.get(kz)
				  int getc=Integer.parseInt(db.executeQueryP2("select count(*) from ("+query+")").toString())
				  if(getc==0)query="SELECT DECODE(pt.payment_terms_no, NULL, 'Methods', 'Terms') payment_option,DECODE(abg.ean_gln_number, NULL, 'Net Terms', 'EAN/GLN') payment_terms_type,abg.billing_group_no, abg.profile_name   AS billing_group_name,  abg.profile_description   AS billing_group_description, abg.profile_cdid AS client_billing_group_id, abg.primary_pmt_method_seq AS primary_payment_method_no,abg.status_cd as status FROM ariacore.acct_billing_group abg left join ariacore.payment_terms pt on pt.payment_terms_no = abg.payment_terms_no where abg.acct_no = "+ acct_no + " and abg.client_no ="+client_no+"and abg.billing_group_no ="+billing_group_no.get(kz)
				  ResultSet rs4=db.executePlaQuery(query)
				  ResultSetMetaData md4 = rs4.getMetaData();
				  int columns4 = md4.getColumnCount();
				  while (rs4.next())
				  {
					  for(int l=1; l<=columns4; l++)
					  {
						  if((rs4.getObject(l))!=null)
						  {
							  
								  row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
							  
						  
						  }
					  }
					 
				  
				  }
				  
				  String query2 ="select pim.plan_instance_no,pi.plan_instance_cdid as client_plan_instance_id from ariacore.plan_instance_master pim join ariacore.plan_instance pi on pi.plan_instance_no=pim.plan_instance_no where billing_group_no ="+billing_group_no.get(kz)+" order by PLAN_INSTANCE_NO"
				  
				  ResultSet rs42=db.executePlaQuery(query2)
				  ResultSetMetaData md42 = rs42.getMetaData();
				  int columns42 = md42.getColumnCount();
				  int k=1,lk=1;
				  while (rs42.next())
				  {
					  for(int l=1; l<=columns42; l++)
					  {
						  if((rs42.getObject(l))!=null)
						  {
							  
								 if(md42.getColumnName(l).toLowerCase().equals("client_plan_instance_id"))
						 {
							//for now I have hardcoded the two fields but in case if we need it dynamicaly in future we can change as per the requirment
							 if(k>1)row.put((md42.getColumnName(l)).toLowerCase()+"["+k.toString()+"]",(rs42.getObject(l)));
							 else{   row.put((md42.getColumnName(l)).toLowerCase(),(rs42.getObject(l)));}
							 k++;
						 }
						 else if(md42.getColumnName(l).toLowerCase().equals("plan_instance_no"))
						 {
							 if(lk>1)row.put((md42.getColumnName(l)).toLowerCase()+"["+lk.toString()+"]",(rs42.getObject(l)));
							 else{   row.put((md42.getColumnName(l)).toLowerCase(),(rs42.getObject(l)));}
							 lk++;
						 }
						else{   row.put((md42.getColumnName(l)).toLowerCase(),(rs42.getObject(l)));}
							  
						  
						  }
					  }
					 
				  
				  }
				  
			  }
			  
		   outer.put("billing_groups_row["+j+"]",row.sort())
		   row = new LinkedHashMap<String, String>();
		   j++
	   
		  }
		   return outer.sort()
	   }
	   
	  
	   
	   /**
		* To verify the payment method details of get_acct_billing_group_details_m
		* @param TESTCASEID
		* @return LinkedHashMap
		*/
	   public  LinkedHashMap md_Verify_PayMethodDetail(String TESTCASEID)
	   {
		   logi.logInfo("md_verifyGetAcctPlanMApi")
		   ConnectDB db = new ConnectDB()
		   int j=1
		   LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
		   LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   def acct_no = getValueFromResponse('create_acct_complete_m',ExPathRpc.CREATE_ACCT_COMPLETE_M_ACCTNO1)
		   logi.logInfo("acct_no" +acct_no )
		   String query ="select a.payment_method_no,a.bill_first_name,a.bill_middle_name,a.bill_last_name,a.bill_company_name,a.bill_address1,a.bill_city,a.bill_state_prov,a.bill_country,a.bill_postal_cd,a.bill_phone,a.pay_method_type,a.suffix,a.CC_EXPIRE_MM,a.CC_EXPIRE_YYYY,a.PAY_METHOD_NAME,a.bill_AGREEMENT_ID from(SELECT bi.seq_num        AS payment_method_no, ad.first_name  AS bill_first_name, ad.middle_initial AS bill_middle_name, ad.last_name  AS bill_last_name,  ad.company_name AS bill_company_name,ad.address1 AS bill_address1, ad.city  AS bill_city, ad.state AS bill_state_prov, ad.country AS bill_country,ad.zip  AS bill_postal_cd,  ad.intl_phone  AS bill_phone, bi.pay_method  AS pay_method_type,bi.suffix AS suffix,bi.CC_EXPIRE_MM, bi.CC_EXPIRE_YYYY, bi.PAYMENT_METHOD_NAME as PAY_METHOD_NAME,bi.AGREEMENT_ID as bill_AGREEMENT_ID,bi.effective_date as edate FROM ariacore.acct_billing_group abg JOIN ariacore.billing_info bi ON abg.acct_no = bi.acct_no AND abg.primary_pmt_method_seq =seq_num LEFT JOIN ariacore.acct_address ad ON bi.bill_address_seq=ad.address_seq LEFT JOIN ariacore.credit_cards cc ON bi.cc_id         = cc.cc_id WHERE abg.client_no = " +client_no + " and abg.acct_no = " + acct_no + " UNION SELECT bi.seq_num   AS payment_method_no, ad.first_name     AS bill_first_name, ad.middle_initial AS bill_middle_name, ad.last_name      AS bill_last_name, ad.company_name   AS bill_company_name, ad.address1       AS bill_address1, ad.city           AS bill_city, ad.state          AS bill_state_prov, ad.country        AS bill_country, ad.zip            AS bill_postal_cd, ad.intl_phone     AS bill_phone, bi.pay_method     AS pay_method_type,  bi.suffix         AS suffix, bi.CC_EXPIRE_MM, bi.CC_EXPIRE_YYYY, bi.PAYMENT_METHOD_NAME as PAY_METHOD_NAME,bi.AGREEMENT_ID as bill_AGREEMENT_ID,bi.effective_date as edate FROM ariacore.acct_billing_group abg JOIN ariacore.billing_info bi ON abg.acct_no                   = bi.acct_no AND abg.secondary_pmt_method_seq =seq_num LEFT JOIN ariacore.acct_address ad ON bi.bill_address_seq=ad.address_seq LEFT JOIN ariacore.credit_cards cc ON bi.cc_id         = cc.cc_id  WHERE abg.client_no =" +client_no+" and abg.acct_no = "+acct_no+")a order by a.bill_first_name,a.bill_middle_name,a.bill_last_name"
		   ResultSet rs4=db.executePlaQuery(query)
		   ResultSetMetaData md4 = rs4.getMetaData();
		   int columns4 = md4.getColumnCount();
		   while (rs4.next())
		   {
			   for(int l=1; l<=columns4; l++)
			   {
				   if((rs4.getObject(l))!=null)
				   {
					   
						   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
					   
				   
				   }
			   }
			   outer.put("payment_methods_summary_row["+j+"]",row.sort())
			   row = new LinkedHashMap<String, String>();
			   j++
		   
		   }
		   return outer.sort()
	   }
	   
		public LinkedHashMap md_GET_ACCT_SURCHARGES_m_Surcharge_Details(String s){
		   logi.logInfo("********md_GET_ACCT_SURCHARGES_m_rate_details Start**************")
		   InputMethods im = new InputMethods()
		   LinkedHashMap<String, String> hm = new LinkedHashMap<String, String>()
		   ArrayList<String> list = new ArrayList<String>()
		   int i = 1
		   String acctNo = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   ConnectDB db = new ConnectDB()
		   String querySurchargeNo = "SELECT surcharge_no FROM ARIACORE.acct_surcharge WHERE ACCT_NO = "+acctNo+" AND CLIENT_NO = "+client_no+""
		   ResultSet rs_SurchargeNo = db.executePlaQuery(querySurchargeNo)
		   while(rs_SurchargeNo.next())
		   {
				 int c =i-1;
				 logi.logInfo("counting"+c)
				   list[i]=rs_SurchargeNo.getObject(1)
				   logi.logInfo "Showing Surcharge number"+list[i]
				   // hm.put("all_surcharges_row["+c+"]",md_GET_ACCT_SURCHARGES_m_Surcharge_Details_row(acctNo,list[i].toString()))
					   i++
		   }
		   logi.logInfo("I value BEFORE"+i)
		   String querySurchargeNo1 ="SELECT SURCHARGE_NO FROM ARIACORE.PLAN_INSTANCE_SURCHARGE WHERE PLAN_INSTANCE_NO IN (SELECT PIM.PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_INSTANCE_MASTER PIM ON PI.PLAN_INSTANCE_NO = PIM.PLAN_INSTANCE_NO WHERE PI.ACCT_NO = "+acctNo+" AND PI.CLIENT_NO = "+client_no+")"
		   ResultSet rs_SurchargeNo1 = db.executePlaQuery(querySurchargeNo1)
		   while(rs_SurchargeNo1.next())
		   {
				 int c =i-1;
				 logi.logInfo("counting1"+c)
				   list[i]=rs_SurchargeNo1.getObject(1)
				   logi.logInfo "Showing Surcharge number1"+list[i]
				   // hm.put("all_surcharges_row["+c+"]",md_GET_ACCT_SURCHARGES_m_Surcharge_Details_row(acctNo,list[i].toString()))
					   i++
		   }
		   logi.logInfo("I value"+i)
		   for (int j=1;j<=i-1;j++)
		   {
			   hm.put("all_surcharges_row["+ j +"]",md_GET_ACCT_SURCHARGES_m_Surcharge_Details_row(acctNo,list[j].toString()))
		   }
		   return hm.sort()
		   logi.logInfo("********md_GET_ACCT_SURCHARGES_m_rate_details Ends**************")
	 }
	 
	 
	 /**
		* To verify the get_dunning_group_details_m API response
		* @param TESTCASEID
		* @return LinkedHashMap
		*/
	   public  LinkedHashMap md_Verify_DunningGroupDetail_m(String TESTCASEID)
	   {
		   logi.logInfo("md_verifyGetAcctPlanMApi")
		   ConnectDB db = new ConnectDB()
		   int j=1
		   LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
		   LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   def acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
		   logi.logInfo("acct_no" +acct_no )
		   String query ="select  pim.status_cd as status,adg.dunning_group_no,adg.dunning_group_name,adg.dunning_group_desc as dunning_group_description,adg.dunning_group_cdid as client_dunning_group_id,adg.dunning_proc_no as dunning_process_no,cdp.dunning_proc_cdid as client_dunning_process_id,pim.plan_instance_no,pi.PLAN_INSTANCE_CDID as client_plan_instance_id  from ariacore.acct_dunning_group adg left join ariacore.client_dunning_proc cdp on adg.dunning_proc_no =cdp.dunning_proc_no and adg.client_no=cdp.client_no right join ariacore.plan_instance_master pim on adg.dunning_group_no=pim.dunning_group_no and adg.client_no=pim.client_no left join ariacore.plan_instance pi on pim.plan_instance_no = pi.plan_instance_no and pim.client_no=pi.client_no where adg.acct_no ="+acct_no +"order by dunning_group_no"
		   ResultSet rs4=db.executePlaQuery(query)
		   ResultSetMetaData md4 = rs4.getMetaData();
		   int columns4 = md4.getColumnCount();
		   while (rs4.next())
		   {
			   for(int l=1; l<=columns4; l++)
			   {
				   if((rs4.getObject(l))!=null)
				   {
					   
						   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
					   
				   
				   }
			   }
			   outer.put("dunning_groups_row["+j+"]",row.sort())
			   row = new LinkedHashMap<String, String>();
			   j++
		   
		   }
		   return outer.sort()
	   }
	   
	   
	   
	 /**
		* TO get all the contracts for the given account number
		* @param acct_hierarchy
		* @return all the fields in the response of md_get_all_acct_contracts_m
		*/
	   
	   LinkedHashMap md_get_all_acct_contracts_m(String acct_hierarchy)
	   {
   
		   logi.logInfo "**** into get_all_acct_contracts_m **** "
		   LinkedHashMap all_acct_contracts_hm= new LinkedHashMap()
		   LinkedHashMap l1= new LinkedHashMap ()
		   InputMethods im = new InputMethods()
		   String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		   logi.logInfo("Given Acct no:" + acct_no)
   
		   String count_mpc_query="SELECT COUNT(CONTRACT_NO) FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE acct_no="+acct_no
		   ConnectDB db = new ConnectDB()
		   String count_mpc=db.executeQueryP2(count_mpc_query)
   
		   if (count_mpc.toInteger() >= 1  )
		   {
			   String database_to_execute="ARIACORE.ACCT_MULTI_PLAN_CONTRACTS"
			   l1=md_get_all_acct_contracts_queries_m(count_mpc.toInteger(),acct_no,database_to_execute)
				  String test_1="SELECT UPDATE_COMMENTS from ariacore.ACCT_MULTI_PLAN_CONTRACTS where acct_no = "+acct_no+""
			   String update_cmts = db.executeQueryP2(count_mpc_query)
				  l1.put("update_comments", update_cmts)
		   }
   
   
		   String count_uc_query="SELECT COUNT(CONTRACT_NO) FROM ARIACORE.ACCT_UNIVERSAL_CONTRACTS WHERE acct_no="+acct_no
   
		   String count_uc=db.executeQueryP2(count_uc_query)
   
		   if (count_uc.toInteger() >= 1  )
		   {
			   String database_to_execute="ARIACORE.ACCT_UNIVERSAL_CONTRACTS"
			   
			   l1=md_get_all_acct_contracts_queries_m(count_uc.toInteger(),acct_no,database_to_execute)
   
		   }
   
	 
		   logi.logInfo "FINAL HASHMAP GET_ALL_ACCT_CONTRACTS "+all_acct_contracts_hm
   
		   logi.logInfo "**** out of  get_all_acct_contracts_m **** "
		   return l1
	   }
   
   
 
	LinkedHashMap md_get_all_acct_contracts_queries_m(int count,String acct_no,String database_to_execute)
	{

		LinkedHashMap all_acct_contracts_temp_hm =new LinkedHashMap()
		ConnectDB db = new ConnectDB()
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		for(int start=1;start<=count;start++)
		{
			//RESULT SET


			logi.logInfo "inside for"
			String  contract_query="SELECT A.CLIENT_CONTRACT_ID,A.CONTRACT_NO,A.TYPE_NO,A.LENGTH_MONTHS,TO_CHAR(A.CREATE_DATE,'YYYY-MM-DD') AS CREATE_DATE,TO_CHAR (A.UPDATE_DATE,'YYYY-MM-DD') AS UPDATE_DATE,TO_CHAR(A.START_DATE,'YYYY-MM-DD') AS START_DATE,TO_CHAR(A.END_DATE,'YYYY-MM-DD') AS END_DATE,A.STATUS_CD AS STATUS_CODE FROM (SELECT ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO,CLIENT_CONTRACT_ID,TYPE_NO,CONTRACT_MONTHS AS LENGTH_MONTHS,CREATE_DATE,UPDATE_DATE,START_DATE,END_DATE,STATUS_CD FROM "+database_to_execute+" WHERE ACCT_NO="+acct_no+") A WHERE A.SEQ="+start
			ResultSet rs_contract_query = db.executePlaQuery(contract_query)
   
			   ResultSetMetaData md1 = rs_contract_query.getMetaData()
			   int columns1 = md1.getColumnCount();
			   while (rs_contract_query.next())
			   {
   
				   for(int s=1; s<=columns1; ++s)
				   {
   
   
					   if(rs_contract_query.getObject(s).toString().equals("null"))
					   {
						   logi.logInfo("No value in "+rs_contract_query.getObject(s))
					   }
					   else
					   {
						   logi.logInfo("Value inserting in hash map")
						   all_acct_contracts_temp_hm.put(md1.getColumnName(s).toLowerCase(),rs_contract_query.getObject(s));
					   }
					   logi.logInfo(rs_contract_query.getObject(s)+" value inserted into "+md1.getColumnName(s))
   
   
				   }
			   }
			   
			   all_acct_contracts_temp_hm.put("contract_scope","MULTI PLAN")
			   
			   String contract_no_query="SELECT A.CONTRACT_NO FROM (SELECT  ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE ACCT_NO="+acct_no+") A WHERE A.SEQ="+start
							  String contract=db.executeQueryP2(contract_no_query)
   
			   logi.logInfo("Contract number : "+contract_query)
			   
			   String plan_instance_query="select pi.plan_instance_no as plan_instance_no,pi.plan_instance_cdid as client_plan_instance_id,cp.plan_name as plan_name,pi.status_cd as plan_instance_status_cd,g.label as plan_instance_status_label from ariacore.plan_instance pi join ariacore.ALL_PLAN_INST_ACTIVE_CONTRACTS ac on pi.plan_instance_no = ac.plan_instance_no and pi.client_no = ac.client_no and pi.acct_no=ac.acct_no join ariacore.client_plan cp on pi.plan_no =cp.plan_no and pi.client_no=cp.client_no join ariacore.global_plan_status_code g on pi.status_cd =g.plan_status_cd  where pi.acct_no = "+acct_no+" and ac.CONTRACT_NO = "+contract
			   ResultSet rs2 = db.executePlaQuery(plan_instance_query)
			   ResultSetMetaData md2 = rs2.getMetaData();
			   int columns3 = md2.getColumnCount();
			   int r=1
			   while (rs2.next())
			   {
				   for(int j=1; j<=columns3; j++)
							   {
								   logi.logInfo("Out of If" )
								   if((rs2.getObject(j))!=null)
								   {
									   if(r==1)
									   {
										   logi.logInfo("Into If condition" )
										   all_acct_contracts_temp_hm.put((md2.getColumnName(j)).toLowerCase(),(rs2.getObject(j)));
									   }
									   else
									   {
										   logi.logInfo("Into Else Part" )
										   all_acct_contracts_temp_hm.put((md2.getColumnName(j)).toLowerCase() +"["+r+"]",(rs2.getObject(j)));
									   }
									   
								   }
							   }
							   r++
			   
			   }
			   
   
			   
						   logi.logInfo "**** out of  get_all_acct_contracts_queries_m **** "
		   }
		   
		   logi.logInfo("temp1")
		   logi.logInfo("adsadvsadgasjgdasjkgdssa"+all_acct_contracts_temp_hm)
		   logi.logInfo("temp2")
		   return all_acct_contracts_temp_hm
		   
   
	   }
   
   
				
	   
	   /**
		* @param acct_no and row_sequence number
		* @param acct_no,i
		* @return billing row information
		*/
	public LinkedHashMap get_billing_row(String acct_no, int i){
		ConnectDB db = new ConnectDB()
		
		LinkedHashMap<String,String> billing = new LinkedHashMap<String,String>()
		String billing_query = "select b.payment_terms_name,b.payment_terms_no,b.payment_option,b.payment_terms_type,b.billing_group_no,b.billing_group_name,b.billing_group_description,b.client_def_billing_group_id, "+
		" b.notify_method,b.notify_template_group,b.statement_template,b.credit_note_template,b.client_primary_pay_method_id, "+
		" b.primary_payment_method_id,b.backup_payment_method_id from(select pt.payment_terms_name,pt.payment_terms_no,DECODE(pt.payment_terms_no, NULL, 'Methods', 'Terms') payment_option,DECODE(a.ean_gln_number, NULL, 'Net Terms', 'EAN/GLN') payment_terms_type,a.billing_group_no,a.profile_name as billing_group_name,a.profile_description as billing_group_description,a.profile_cdid as client_def_billing_group_id,"+
		" a.notify_method_no as notify_method,a.CLIENT_NTG_NO as notify_template_group,a.STMT_TMPL_NO as statement_template,a.CREDIT_NOTE_TEMPLATE_NO as credit_note_template,"+
		" a.PRIMARY_PMT_METHOD_SEQ as primary_payment_method_id,a.SECONDARY_PMT_METHOD_SEQ as backup_payment_method_id, bi.PAYMENT_METHOD_CDID                      AS client_primary_pay_method_id,"+
		" row_number() over(order by billing_group_no) as counter from ariacore.acct_billing_group a left join ariacore.payment_terms pt on pt.payment_terms_no = a.payment_terms_no left join ariacore.billing_info bi on a.acct_no = bi.acct_no and a.PRIMARY_PMT_METHOD_SEQ = bi.SEQ_NUM where a.acct_no = "+acct_no+")b where b.counter = " +i
		ResultSet rs = db.executePlaQuery(billing_query);
		ResultSetMetaData md = rs.getMetaData();
		 int columns = md.getColumnCount();
		 while (rs.next()){
			 
				  for(int s=1; s<=columns; ++s){
					  if(rs.getObject(s).toString().equals("null")){
						  logi.logInfo("No value in "+rs.getObject(s))
					  }else{logi.logInfo("Value inserting in hash map")
					  billing.put(md.getColumnName(s).toLowerCase(),rs.getObject(s));}
					  logi.logInfo(rs.getObject(s)+" value inserted into "+md.getColumnName(s))
				  }
				  
				 
			  }
		 String client_primary_pay_method_id = (String)billing.get("client_primary_pay_method_id")
		 if(client_primary_pay_method_id==null){
			logi.logInfo "no values in client_primary_pay_method_id"
		 }else{
		 logi.logInfo "values in client_primary_pay_method_id : "+client_primary_pay_method_id
		 billing.put("client_primary_payment_method_id",client_primary_pay_method_id)
		 billing.remove("client_primary_pay_method_id")
	 }
		 
		 String bill_group_no = (String)billing.get("billing_group_no")
		/*billing info address under construction*/
						 String addr_count = "select  count(*) as counts from ariacore.acct_address a join "+
						 "ariacore.acct_billing_group b on a.address_seq = b.STATEMENT_ADDRESS_SEQ where a.acct_no = " +acct_no+" and b.billing_group_no = "+bill_group_no
				 String a_count = db.executeQueryP2(addr_count).toString()
				 int cc = Integer.parseInt(a_count)
				 if(cc>0){
				
				
					 String billing_address = "select  a.first_name as stmt_first_name ,a.middle_initial as stmt_mi,a.last_name as stmt_last_name,a.company_name as stmt_company_name,a.address1 as stmt_address1,a.address2 as stmt_address2,a.address3 as stmt_address3,a.city as stmt_city,a.locality as stmt_locality,a.STATE as stmt_state_prov,"+
							 "a.country as stmt_country,a.zip as stmt_postal_cd,a.intl_phone as stmt_phone,a.phone_extension as stmt_phone_ext,a.cell_phone as stmt_cell_phone,a.work_phone as stmt_work_phone,a.work_phone_extension as stmt_work_phone_ext,"+
							 "a.fax_phone as stmt_fax,a.email as stmt_email,TO_CHAR(a.birthdate,'YYYY-MM-DD') AS stmt_birthdate,a.ADDRESS_SEQ AS stmt_contact_no,b.LIST_START_MASTER_FILE as bg_list_start_master_file from ariacore.acct_address a join "+
							 "ariacore.acct_billing_group b on a.address_seq = b.STATEMENT_ADDRESS_SEQ where a.acct_no = " +acct_no+" and b.billing_group_no = "+bill_group_no
					 ResultSet rs1 = db.executePlaQuery(billing_address);
					 ResultSetMetaData md1 = rs1.getMetaData();
					 int columns1 = md1.getColumnCount();
					 while (rs1.next()){
				
						 for(int s=1; s<=columns1; ++s){
							 if(rs1.getObject(s).toString().equals("null")){
								 logi.logInfo("No value in "+rs1.getObject(s))
							 }else{logi.logInfo("Value inserting in hash map")
								 billing.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
							 logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
						 }
				
				
					 }
				
				
				
				
				 }
				
				
				 return billing.sort()
		
	}
	   
	   /**main method to get billing info(include billing group should be 1 to return values
		* @param str - acct_no (if required, by default the first account number is taken(im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)))
		* @return returns all the billing info
		*/
	   public LinkedHashMap md_get_acct_billing_info_m(String str){
		   logi.logInfo("Entering billing info")
		   //String supp_plans = getValueFromRequest("get_acct_details_all_m",ExPathRpcEnc.Supp_plans);logi.logInfo("Supp Plans : "+supp_plans)
		   //String payment_info = getValueFromRequest("get_acct_details_all_m",ExPathRpcEnc.payment_methods);logi.logInfo("Supp Plans : "+payment_info)
		   InputMethods im = new InputMethods()
		   String billing_info = getValueFromRequest("get_acct_details_all_m",ExPathRpcEnc.Billing_info);logi.logInfo("Supp Plans : "+billing_info)
		   String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
		   ConnectDB db = new ConnectDB()
		   LinkedHashMap row = new LinkedHashMap<String , HashMap<String,String>>()
		   
		   String billing_count = "Select count(*) as counts from ariacore.acct_billing_group where acct_no = "+acct_no
		   String ccount = db.executeQueryP2(billing_count).toString()
		   int bcounts = Integer.parseInt(ccount)
		   if(billing_info.equals("1")){
		   for(int i=1;i<=bcounts;i++){
		   
		   
			   logi.logInfo("Information on entering the loop")
			   int s =i-1;
			   logi.logInfo("counting"+s)
			   row.put("billing_groups_info_row["+s+"]",get_billing_row(acct_no,i))
			   
			   logi.logInfo("billing_groups_info_row["+s+"]" )
			   
			   
				   }
		   
		   }
		   return row.sort();
		   
	   }
	   
			
			/**
			 * TO get all the coupon details from account
			 * @param acct_hierarchy
			 * @return all the fields in the response of get_acct_coupon_details_m
			 */
	  LinkedHashMap get_coupon_from_acct(String acct_no)
	  {
	  
		  def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	  
				  LinkedHashMap tempar = new LinkedHashMap()
				  //LinkedHashMap acct_details_hm_temp = new LinkedHashMap()
	  
	  
				  int value=1
	  
				  ConnectDB dbb = new ConnectDB()
				  String result=null
	  
				  String count_query="SELECT count(COUPON_CD) FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" and cancel_date is null and client_no="+client_no
				  String count=dbb.executeQueryP2(count_query)
	  
	  
				  for(int begin=1;begin<=count.toInteger();begin++)
				  {
						LinkedHashMap coupon_frm_mpi = new LinkedHashMap()
						LinkedList l1 =  new LinkedList()
	  
						logi.logInfo("NEW HASHAJSHJSHAKJSHKJAHJKSHA MAP    ____  "+begin)
	  
						//LinkedHashMap coupon_frm_mpi = new LinkedHashMap()
						//LinkedHashMap coupon_frm_mpi= new LinkedHashMap()
	  
						//LinkedHashMap acct_details_hm_temp = new LinkedHashMap()
	  
						logi.logInfo("Creating new HM "+begin+" - - - -- - -  "+coupon_frm_mpi)
						logi.logInfo(begin+"VAlue fo beginnnnnnn")
	  
	  
						// Checking if its CREDIT TEMPLATE
	  
						String credit_tt_query="SELECT count(RECURRING_CREDIT_TEMPLATE_NO) FROM ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE COUPON_CD =(SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by client_no ) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" and cancel_date is null and client_no="+client_no+")A)WHERE A.SEQ="+begin+")"
						//String credit_tt_query="SELECT count(RECURRING_CREDIT_TEMPLATE_NO) FROM ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE COUPON_CD =(SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by coupon_cd ) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" and cancel_date is null  )A)WHERE A.SEQ="+begin+")"
						String credit_tt=dbb.executeQueryP2(credit_tt_query)
						logi.logInfo(credit_tt+"credit_tt")
	  
	  
	  
						if(credit_tt.toInteger() >=1)
						{ //Its credit template
							  //String credit_coupon_cd_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by coupon_cd ) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" order by coupon_cd desc)A)WHERE A.SEQ="+begin
							  String credit_coupon_cd_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by client_no ) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+"and cancel_date is null and client_no="+client_no+")A)WHERE A.SEQ="+begin
							  String credit_coupon_cd=dbb.executeQueryP2(credit_coupon_cd_query)
							  logi.logInfo("<-----------------  Its credit template  ---------------------------->")
	  
							  String gen_coupon_query="SELECT COUPON_CD AS OUT_COUPON_CD, APPLICATIONS_ALLOWED-1 AS COUPON_SCOPE,COMMENTS AS DESCRIPTION,TOTAL_USES,CURRENCY_CD, RECUR_DISCOUNT_FLAT_AMT,ONE_TIME_DISCOUNT_FLAT_AMT, RECUR_DISCOUNT_PCT, ONE_TIME_DISCOUNT_PCT,1 AS STATUS_IND FROM ARIACORE.COUPONS WHERE COUPON_CD='"+credit_coupon_cd+"' AND CLIENT_NO="+client_no
							  ResultSet rs_credit_gen=dbb.executePlaQuery(gen_coupon_query)
	  
							  while (rs_credit_gen.next())
							  {
									logi.logInfo("inside while")
									ResultSetMetaData md = rs_credit_gen.getMetaData()
									logi.logInfo("inside while"+rs_credit_gen.getMetaData())
									int columns = md.getColumnCount()
									logi.logInfo("columns for"+columns)
									// Putting values into HashMap
	  
	  
									coupon_frm_mpi.put("out_acct_no",acct_no)
									l1.add("out_acct_no")
									//coupon_frm_mpi.put("coupon_scope","0")
									//l1.add("out_acct_no")
									for(int i=1; i<=columns; ++i)
	  
									{
	  
										  logi.logInfo("inside for")
										//  if(!(md.getColumnName(i).toString().equalsIgnoreCase("null")))
										  if(!(rs_credit_gen.getObject(i).toString().equalsIgnoreCase("null")))
	  
	  
										  {logi.logInfo("insie PUTTTT")
												l1.add(md.getColumnName(i).toLowerCase())
												int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	  
												logi.logInfo("frew of :+"+md.getColumnName
	  
															(i).toLowerCase()+" is :"+freq)
	  
												if(freq>1)
												{
	  
													  coupon_frm_mpi.put(md.getColumnName
	  
																  (i).toLowerCase()+"["+ freq +"]",rs_credit_gen.getObject(i))
												}
												else
	  
													  coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs_credit_gen.getObject(i))
	  
										  }
										  else
												logi.logInfo("insie else")
									}
							  }
	  
	  
	  
							  for(int y=1;y<=credit_tt.toInteger();y++)
							  {
	  
									String credit_temp_query="SELECT A.RECURRING_CREDIT_TEMPLATE_NO AS RECURRING_CREDIT_TEMPLATE_NO FROM ((SELECT RECURRING_CREDIT_TEMPLATE_NO,ROW_NUMBER() OVER(ORDER BY RECURRING_CREDIT_TEMPLATE_NO) AS SEQ FROM ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE COUPON_CD='"+credit_coupon_cd+"')A)WHERE A.SEQ="+y//+y
									String credit_temp_no=dbb.executeQueryP2(credit_temp_query)
									String Query="SELECT RECURRING_CREDIT_TEMPLATE_NO AS CREDIT_TEMPLATE_NO,LABEL AS TEMPLATE_NAME,FLAT_AMOUNT,CURRENCY_CD,NUM_CREDITS_REQUIRED,CREDIT_INTERVAL_MONTHS FROM ARIACORE.RECURRING_CREDIT_TEMPLATES WHERE RECURRING_CREDIT_TEMPLATE_NO="+credit_temp_no+"and client_no="+client_no
									ResultSet rs= dbb.executePlaQuery(Query)
									logi.logInfo("afte rs")
	  
	  
									while (rs.next())
									{
										  logi.logInfo("inside while")
										  ResultSetMetaData md = rs.getMetaData()
										  logi.logInfo("inside while"+rs.getMetaData())
										  int columns = md.getColumnCount()
										  logi.logInfo("columns for"+columns)
										  // Putting values into HashMap
										  coupon_frm_mpi.put("out_acct_no",acct_no)
										  for(int i=1; i<=columns; ++i)
										  {
												logi.logInfo("inside for")
												if(!(rs.getObject(i).toString().equalsIgnoreCase("null")))
												{logi.logInfo("insie PUTTTT")
	  
													  l1.add(md.getColumnName(i).toLowerCase())
	  
													  int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	  
													  logi.logInfo("frew of :+"+md.getColumnName
	  
																  (i).toLowerCase()+" is :"+freq)
	  
													  if(freq>1)
													  {
	  
															coupon_frm_mpi.put(md.getColumnName
	  
																		(i).toLowerCase()+"["+ freq +"]",rs.getObject(i))
													  }
	  
													  else
	  
	  
															coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
												}
	  
												else
													  logi.logInfo("insie else")
										  }
	  
	  
	  
										  /*    logi.logInfo("HASH #### 2"+coupon_frm_mpi)
										  value=begin
										  //value=value-1
										  logi.logInfo("before ruturn")
										  tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi) */
	  
									}
							  }
						}
						logi.logInfo("After credit template :"+coupon_frm_mpi)
	  
	  
	  
						/*    logi.logInfo("HASH #### 2"+coupon_frm_mpi)
						value=begin
						//value=value-1
						logi.logInfo("before ruturn")
						tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi)
						*/
	  
	  
	  
						//String disc_tt_query="SELECT count(RULE_NO)  FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD = (SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by coupon_cd ) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" and cancel_date is null and client_no="+client_no+")A)WHERE A.SEQ="+begin+")"
						String disc_tt_query="SELECT count(RULE_NO)  FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD = (SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by client_no ) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" and cancel_date is null and client_no="+client_no+")A)WHERE A.SEQ="+begin+")"
						String disc_tt=dbb.executeQueryP2(disc_tt_query)
	  
	  
	  
						if(disc_tt.toInteger() >=1)
						{ //Its discount rules
	  
							  logi.logInfo("<----------------- Its discount ------------------------->")
	  
							  String disc_coupon_cd_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by client_no ) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" and client_no="+client_no+" order by client_no )A)WHERE A.SEQ="+begin
							  //String disc_coupon_cd_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by coupon_cd desc) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" order by coupon_cd desc)A)WHERE A.SEQ="+begin
							  String disc_coupon_cd=dbb.executeQueryP2(disc_coupon_cd_query)
	  
	  
							  String gen_coupon_query="SELECT COUPON_CD AS OUT_COUPON_CD, APPLICATIONS_ALLOWED-1 AS COUPON_SCOPE,COMMENTS AS DESCRIPTION,TOTAL_USES,CURRENCY_CD, RECUR_DISCOUNT_FLAT_AMT,ONE_TIME_DISCOUNT_FLAT_AMT, RECUR_DISCOUNT_PCT, ONE_TIME_DISCOUNT_PCT,1 AS STATUS_IND FROM ARIACORE.COUPONS WHERE COUPON_CD='"+disc_coupon_cd+"' AND CLIENT_NO="+client_no
							  ResultSet rs_disc_gen=dbb.executePlaQuery(gen_coupon_query)
	  
							  while (rs_disc_gen.next())
							  {
									logi.logInfo("inside while")
									ResultSetMetaData md = rs_disc_gen.getMetaData()
									logi.logInfo("inside while"+rs_disc_gen.getMetaData())
									int columns = md.getColumnCount()
									logi.logInfo("columns for"+columns)
									// Putting values into HashMap
	  
	  
									coupon_frm_mpi.put("out_acct_no",acct_no)
									l1.add("out_acct_no")
									//coupon_frm_mpi.put("coupon_scope","0")
									for(int i=1; i<=columns; ++i)
	  
									{
	  
										  logi.logInfo("Inside for")
										//  if(!(md.getColumnName(i).toString().equalsIgnoreCase("null")))
										  if(!(rs_disc_gen.getObject(i).toString().equalsIgnoreCase("null")))
										  {
												logi.logInfo("insie PUTTTT")
												l1.add(md.getColumnName(i).toLowerCase())
												int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	  
												logi.logInfo("freqq of :+"+md.getColumnName
	  
															(i).toLowerCase()+" is :"+freq)
	  
												if(freq>1)
												{
	  
													  coupon_frm_mpi.put(md.getColumnName
	  
																  (i).toLowerCase()+"["+ freq +"]",rs_disc_gen.getObject(i))
												}
	  
												coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs_disc_gen.getObject(i))
	  
										  }
										  else
												logi.logInfo("insie else")
									}
							  }
	  
							  logi.logInfo("loop hash"+coupon_frm_mpi)
	  
	  
							  logi.logInfo("Inside ")
							  //keys1=coupon_frm_mpi.keySet()
	  
							  //String disc_rule_query="SELECT RULE_NO  FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD = (SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by coupon_cd desc) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+")A)WHERE A.SEQ="+begin+")"
							  String disc_rule_count_query="SELECT count(RULE_NO)  FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD ='"+disc_coupon_cd+"' and client_no = "+client_no
							  String disc_rule_count=dbb.executeQueryP2(disc_rule_count_query)
							  for(int r=1;r<=disc_rule_count.toInteger();r++)
							  {
									String disc_rule_query="SELECT A.RULE_NO AS RULE_NO FROM((SELECT RULE_NO,ROW_NUMBER() OVER (ORDER BY RULE_NO) AS SEQ FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD = '"+disc_coupon_cd+"')A)WHERE A.SEQ="+r
									String disc_rule=dbb.executeQueryP2(disc_rule_query)
	  
	  
	  
									//commented for max applications
									//String Query="SELECT RULE_NO,CLIENT_RULE_ID,LABEL AS RULE_NAME,SCOPE_NO, DESCRIPTION,FLAT_PERCENT_IND,CURRENCY_CD,AMOUNT,INLINE_OFFSET_IND,DURATION_TYPE_IND,MAX_APPLICATIONS_PER_ACCT,EXT_DESCRIPTION,ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE RULE_NO="+disc_rule
									String Query="SELECT RULE_NO,CLIENT_RULE_ID,LABEL AS RULE_NAME,SCOPE_NO, DESCRIPTION ,FLAT_PERCENT_IND,CURRENCY_CD,AMOUNT,INLINE_OFFSET_IND,DURATION_TYPE_IND,EXT_DESCRIPTION,ALT_SERVICE_NO_2_APPLY,MAX_APPLICABLE_MONTHS FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE RULE_NO="+disc_rule+" and client_no="+client_no
									ResultSet rs1= dbb.executePlaQuery(Query)
									ResultSetMetaData md = rs1.getMetaData()
									int columns = md.getColumnCount()
									// Putting values into HashMap
									coupon_frm_mpi.put("out_acct_no",acct_no)
									int temp_count=1
	  
	  
									while (rs1.next())
									{
	  
	  
										  for(int i=1; i<=columns; ++i)
										  {
	  
											//	if(!(md.getColumnName(i).toString().equalsIgnoreCase("null")))
											  if(!(rs1.getObject(i).toString().equalsIgnoreCase("null")))
												{
													  l1.add(md.getColumnName(i).toLowerCase())
	  
													  int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	  
													  logi.logInfo("frewww of :+"+md.getColumnName(i).toLowerCase()+" is :"+freq)
	  
													  if(freq>1)
													  {
	  
															coupon_frm_mpi.put(md.getColumnName(i).toLowerCase()+"["+ freq +"]",rs1.getObject(i))
													  }
	  
													  else
															coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs1.getObject(i))
	  
												}
	  
	  
	  
												//else
												//coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs1.getObject(i))
	  
	  
										  }
									}
	  
	  
							  }
						}
						logi.logInfo("After discount rules :"+coupon_frm_mpi)
	  
						/*    logi.logInfo("HASH #### 2"+coupon_frm_mpi)
						value=begin
						//value=value-1
						tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi)
						*/
	  
	  
						//romved desc form copun_cd
						String a="SELECT count(BUNDLE_NO) FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP WHERE COUPON_CD =(SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by acct_no) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+"and cancel_date is null and client_no="+client_no+")A)WHERE A.SEQ="+begin+")"
						String b=dbb.executeQueryP2(a)
						if(b.toInteger()>0)
						{
	  
							  String credit_coupon_cd_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by client_no ) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" and cancel_date is null and client_no="+client_no+" order by client_no )A)WHERE A.SEQ="+begin
							  //String credit_coupon_cd_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by coupon_cd desc) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" order by coupon_cd desc)A)WHERE A.SEQ="+begin
							  String credit_coupon_cd=dbb.executeQueryP2(credit_coupon_cd_query)
							  logi.logInfo("<--------------------- Its disc bundle ------------------->  ")
	  
							  String gen_coupon_query="SELECT COUPON_CD AS OUT_COUPON_CD, APPLICATIONS_ALLOWED-1 AS COUPON_SCOPE,COMMENTS AS DESCRIPTION,TOTAL_USES,CURRENCY_CD, RECUR_DISCOUNT_FLAT_AMT,ONE_TIME_DISCOUNT_FLAT_AMT, RECUR_DISCOUNT_PCT, ONE_TIME_DISCOUNT_PCT,1 AS STATUS_IND FROM ARIACORE.COUPONS WHERE COUPON_CD='"+credit_coupon_cd+"' AND CLIENT_NO="+client_no
							  ResultSet rs_credit_gen=dbb.executePlaQuery(gen_coupon_query)
	  
							  while (rs_credit_gen.next())
							  {
	  
									ResultSetMetaData md = rs_credit_gen.getMetaData()
									int columns = md.getColumnCount()
	  
									// Putting values into HashMap
	  
	  
									coupon_frm_mpi.put("out_acct_no",acct_no)
									l1.add("out_acct_no")
	  
									for(int i=1; i<=columns; ++i)
	  
									{
	  
										  logi.logInfo("inside for")
										  //if(!(md.getColumnName(i).toString().equalsIgnoreCase("null")))
										 // if(!(md.getColumnName(i).toString().contains("null")))
										 if(!( rs_credit_gen.getObject(i).toString().contains("null")))
	  
										  {
	  
												l1.add(md.getColumnName(i).toLowerCase())
												logi.logInfo "KEYSSS 1 : "+l1
												logi.logInfo("insie PUTTTT")
	  
												int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	  
												logi.logInfo("frew of :+"+md.getColumnName(i).toLowerCase()+" is :"+freq)
	  
												if(freq>1)
												{
	  
													  coupon_frm_mpi.put(md.getColumnName(i).toLowerCase()+"["+ ++freq
	  
																  +"]",rs_credit_gen.getObject(i))
												}
												else
	  
													  coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs_credit_gen.getObject(i))
	  
										  }
										  else
												logi.logInfo("insie else")
									}
							  }
	  
							  logi.logInfo("<--------------------- Its disc bundle 2222 ------------------->  ")
	  
							  for(int w=1;w<=b;++w)
							  {
									String coupon_code1_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by client_no ) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+"and cancel_date is null and client_no="+client_no+" order by client_no )A)WHERE A.SEQ="+w
									//String coupon_code1_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by coupon_cd desc) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="acct_no"+ order by coupon_cd desc)A)WHERE A.SEQ="+w
									String coupon_code1=dbb.executeQueryP2(coupon_code1_query)
									if(coupon_code1.toInteger() >= 1)
	  
									{String coupon_code_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by client_no ) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+"and cancel_date is null and client_no="+client_no+" order by client_no )A)WHERE A.SEQ="+w
										  //String coupon_code_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by coupon_cd desc) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="acct_no"+ order by coupon_cd desc)A)WHERE A.SEQ="+w
										  String coupon_code=dbb.executeQueryP2(coupon_code_query)
	  
										  logi.logInfo "coupon_code____________> is "+coupon_code
	  
	  
										  String bundle_no_query="SELECT count(BUNDLE_NO) FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP WHERE COUPON_CD='"+coupon_code+"'"
										  String bundle_no=dbb.executeQueryP2(bundle_no_query)
	  
										  String final_bundle_no_query="SELECT BUNDLE_NO FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP WHERE COUPON_CD='"+coupon_code+"'"
										  String final_bundle_no=dbb.executeQueryP2(final_bundle_no_query)
										  String final_coupon_code=coupon_code
										  logi.logInfo "Inside final_coupon_code"+ final_coupon_code
										  logi.logInfo "Inside final_bundle_no"+ final_bundle_no.toInteger()
	  
	  
										  //Its discount bundle
	  
	  
	  
	  
										  String bundle_count_query="SELECT COUNT(RULE_NO) FROM ARIACORE.CLIENT_DR_BUNDLE_MEMBERS WHERE BUNDLE_NO="+final_bundle_no.toInteger()
										  String bundle_count=dbb.executeQueryP2(bundle_count_query)
										  logi.logInfo "bundle_count "+ bundle_count
										  for(int start=1;start<=bundle_count.toInteger();start++)
										  {
												logi.logInfo "start "+ start
												String rule_no_query="SELECT A.RULE_NO FROM((SELECT RULE_NO, ROW_NUMBER() OVER(ORDER BY RULE_NO) AS SEQ FROM ARIACORE.CLIENT_DR_BUNDLE_MEMBERS WHERE BUNDLE_NO="+final_bundle_no+") A) WHERE A.SEQ="+start
												String rule_no=dbb.executeQueryP2(rule_no_query)
	  
												//commented for max applications
												String bundle_values_query="SELECT DISTINCT RULE_NO,CLIENT_RULE_ID,LABEL AS RULE_NAME,SCOPE_NO, DESCRIPTION ,FLAT_PERCENT_IND,CURRENCY_CD,AMOUNT,INLINE_OFFSET_IND,DURATION_TYPE_IND,MAX_APPLICATIONS_PER_ACCT,EXT_DESCRIPTION,ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE RULE_NO="+rule_no+" and client_no="+client_no
												//String bundle_values_query="SELECT DISTINCT RULE_NO,CLIENT_RULE_ID,LABEL AS RULE_NAME,SCOPE_NO, DESCRIPTION ,FLAT_PERCENT_IND,CURRENCY_CD,AMOUNT,INLINE_OFFSET_IND,DURATION_TYPE_IND,EXT_DESCRIPTION,ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE RULE_NO="+rule_no
												ResultSet rs_disc_bundle=dbb.executePlaQuery(bundle_values_query)
												ResultSetMetaData md_disc_bundle = rs_disc_bundle.getMetaData()
												int columns = md_disc_bundle.getColumnCount()
												LinkedHashMap disc_bundle_hm= new LinkedHashMap()
												// Putting values into HashMap
	  
												while (rs_disc_bundle.next())
												{
	  
													  for(int i=1; i<=columns; ++i)
													  {
	  
															//if(!(rs_disc_bundle.getObject(i).toString().equalsIgnoreCase("null")))
															if(!(rs_disc_bundle.getObject(i).toString().contains("null")))
															{
	  
																  l1.add(md_disc_bundle.getColumnName(i).toLowerCase())
	  
	  
																  logi.logInfo "KEYSSS 2 : "+l1
	  
	  
	  
	  
																  int freq = Collections.frequency(l1,md_disc_bundle.getColumnName(i).toLowerCase())
	  
																  logi.logInfo("frew of :+"+md_disc_bundle.getColumnName(i).toLowerCase()+" is :"+freq)
	  
																  if(freq>1)
																  {
	  
																		coupon_frm_mpi.put(md_disc_bundle.getColumnName(i).toLowerCase
	  
																					()+"["+ freq +"]",rs_disc_bundle.getObject(i))
																  }
	  
	  
	  
																  else
																  {
	  
																		coupon_frm_mpi.put(md_disc_bundle.getColumnName(i).toLowerCase
	  
																					(),rs_disc_bundle.getObject(i))
	  
																  }
	  
	  
															}
	  
													  }
	  
													  l1.add("bundle_no")
													  int freq1 = Collections.frequency(l1,"bundle_no")
													  if(freq1>1)
															coupon_frm_mpi.put("bundle_no["+  freq1 +"]",final_bundle_no)
													  else
															coupon_frm_mpi.put("bundle_no",final_bundle_no)
	  
													  logi.logInfo("HASH #### 2"+coupon_frm_mpi)
													  value=begin
	  
	  
													  tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi)
	  
	  
												}
	  
										  }
	  
										  String disc_bundle_final_query="SELECT BUNDLE_NO,CLIENT_BUNDLE_ID,LABEL AS BUNDLE_NAME,DESCRIPTION FROM ARIACORE.CLIENT_DISCOUNT_RULE_BUNDLES WHERE BUNDLE_NO =(SELECT BUNDLE_NO FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP WHERE COUPON_CD ='"+final_coupon_code+"')"
	  
										  ResultSet rs_disc_bundle_final=dbb.executePlaQuery(disc_bundle_final_query)
	  
										  ResultSetMetaData md_disc_bundle_final = rs_disc_bundle_final.getMetaData()
	  
										  int columns = md_disc_bundle_final.getColumnCount()
										  logi.logInfo("inot while #### 13")
										  // Putting values into HashMap
										  coupon_frm_mpi.put("out_acct_no",acct_no)
										  //coupon_frm_mpi.put("coupon_scope",0)
										  while (rs_disc_bundle_final.next())
	  
										  {
												logi.logInfo("inot while #### 1")
												for(int i=1; i<=columns; ++i)
												{
	  
													  if(!(rs_disc_bundle_final.getObject(i).toString().equalsIgnoreCase("null")))
													  {
															l1.add(md_disc_bundle_final.getColumnName(i).toLowerCase())
															logi.logInfo "KEYSSS 3 : "+l1
															logi.logInfo("inot while #### 2")
															int freq = Collections.frequency(l1,md_disc_bundle_final.getColumnName(i).toLowerCase())
	  
															logi.logInfo("frew of :+"+md_disc_bundle_final.getColumnName(i).toLowerCase()+" is :"+freq)
	  
															if(freq>1)
															{
	  
																  coupon_frm_mpi.put(md_disc_bundle_final.getColumnName(i).toLowerCase
	  
																			  ()+"["+ freq +"]",rs_disc_bundle_final.getObject(i))
															}
	  
	  
															else
																  coupon_frm_mpi.put(md_disc_bundle_final.getColumnName(i).toLowerCase
	  
																			  (),rs_disc_bundle_final.getObject(i))
	  
													  }
												}
	  
										  }
	  
	  
										  logi.logInfo("Keyss of final hash :"+l1)
	  
									}
	  
							  }
						}
						logi.logInfo("After discount bundle :"+coupon_frm_mpi)
	  
	  
	  
						logi.logInfo("HASHMAP AFTER ITERAATION "+begin+" "+tempar)
						logi.logInfo("ENND D 222")
						logi.logInfo("HASH #### 2"+coupon_frm_mpi)
						value=begin
						//value=value-1
						coupon_frm_mpi=coupon_frm_mpi.sort()
						tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi)
	  
				  }
	  
	  
				  tempar = tempar.sort()
	  
				  return tempar
	  
	  
			}
	  
	  
	  LinkedHashMap get_coupon_from_mpi(String mpi,String acct_no)
			{
				def client_no=Constant.mycontext.expand('${Properties#Client_No}')
				  LinkedHashMap tempar = new LinkedHashMap()
				  //LinkedHashMap acct_details_hm_temp = new LinkedHashMap()
	  
	  
				  int value=1
	  
				  ConnectDB dbb = new ConnectDB()
				  String result=null
	  
				  String count_query="SELECT count(COUPON_CD) FROM ARIACORE.PLAN_INST_COUPON_MAP WHERE PLAN_INSTANCE_NO="+mpi
				  String count=dbb.executeQueryP2(count_query)
	  
	  
				  for(int begin=1;begin<=count.toInteger();begin++)
				  {
						LinkedHashMap coupon_frm_mpi = new LinkedHashMap()
						LinkedList l1 =  new LinkedList()
	  
						logi.logInfo("NEW HASHAJSHJSHAKJSHKJAHJKSHA MAP    ____  "+begin)
	  
						//LinkedHashMap coupon_frm_mpi = new LinkedHashMap()
						//LinkedHashMap coupon_frm_mpi= new LinkedHashMap()
	  
						//LinkedHashMap acct_details_hm_temp = new LinkedHashMap()
	  
						logi.logInfo("Creating new HM "+begin+" - - - -- - -  "+coupon_frm_mpi)
						logi.logInfo(begin+"VAlue fo beginnnnnnn")
	  
	  
						// Checking if its CREDIT TEMPLATE
	  
						String credit_tt_query="SELECT count(RECURRING_CREDIT_TEMPLATE_NO) FROM ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE COUPON_CD =(SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by client_no ) AS seq FROM ARIACORE.PLAN_INST_COUPON_MAP WHERE PLAN_INSTANCE_NO="+mpi+")A)WHERE A.SEQ="+begin+")"
						String credit_tt=dbb.executeQueryP2(credit_tt_query)
						logi.logInfo(credit_tt+"credit_tt")
	  
	  
	  
						if(credit_tt.toInteger() >=1)
						{ //Its credit template
							  String credit_coupon_cd_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by client_no ) AS seq FROM ARIACORE.PLAN_INST_COUPON_MAP WHERE PLAN_INSTANCE_NO="+mpi+")A)WHERE A.SEQ="+begin
							  String credit_coupon_cd=dbb.executeQueryP2(credit_coupon_cd_query)
							  logi.logInfo("<-----------------  Its credit template  ---------------------------->")
	  
							  String gen_coupon_query="SELECT COUPON_CD AS OUT_COUPON_CD, APPLICATIONS_ALLOWED-1 AS COUPON_SCOPE,COMMENTS AS DESCRIPTION,TOTAL_USES,CURRENCY_CD, RECUR_DISCOUNT_FLAT_AMT,ONE_TIME_DISCOUNT_FLAT_AMT, RECUR_DISCOUNT_PCT, ONE_TIME_DISCOUNT_PCT,1 AS STATUS_IND FROM ARIACORE.COUPONS WHERE COUPON_CD='"+credit_coupon_cd+"' AND CLIENT_NO="+client_no
							  ResultSet rs_credit_gen=dbb.executePlaQuery(gen_coupon_query)
	  
							  while (rs_credit_gen.next())
							  {
									logi.logInfo("inside while")
									ResultSetMetaData md = rs_credit_gen.getMetaData()
									logi.logInfo("inside while"+rs_credit_gen.getMetaData())
									int columns = md.getColumnCount()
									logi.logInfo("columns for"+columns)
									// Putting values into HashMap
	  
	  
									coupon_frm_mpi.put("out_acct_no",acct_no)
									l1.add("out_acct_no")
									coupon_frm_mpi.put("out_client_master_plan_instance_id",mpi)
									l1.add("out_client_master_plan_instance_id")
									coupon_frm_mpi.put("out_master_plan_instance_no",mpi)
									l1.add("out_master_plan_instance_no")
	  
									//coupon_frm_mpi.put("coupon_scope","0")
									//l1.add("out_acct_no")
									for(int i=1; i<=columns; ++i)
	  
									{
	  
										  logi.logInfo("inside for")
										 // if(!(md.getColumnName(i).toString().equalsIgnoreCase("null")))
										  if(!(rs_credit_gen.getObject(i).toString().equalsIgnoreCase("null")))
	  
	  
										  {logi.logInfo("insie PUTTTT")
												l1.add(md.getColumnName(i).toLowerCase())
												int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	  
												logi.logInfo("frew of :+"+md.getColumnName
	  
															(i).toLowerCase()+" is :"+freq)
	  
												if(freq>1)
												{
	  
													  coupon_frm_mpi.put(md.getColumnName
	  
																  (i).toLowerCase()+"["+ freq +"]",rs_credit_gen.getObject(i))
												}
												else
	  
													  coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs_credit_gen.getObject(i))
	  
										  }
										  else
												logi.logInfo("insie else")
									}
							  }
	  
	  
	  
							  for(int y=1;y<=credit_tt.toInteger();y++)
							  {
	  
									String credit_temp_query="SELECT A.RECURRING_CREDIT_TEMPLATE_NO AS RECURRING_CREDIT_TEMPLATE_NO FROM ((SELECT RECURRING_CREDIT_TEMPLATE_NO,ROW_NUMBER() OVER(ORDER BY RECURRING_CREDIT_TEMPLATE_NO) AS SEQ FROM ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE COUPON_CD='"+credit_coupon_cd+"')A)WHERE A.SEQ="+y//+y
									String credit_temp_no=dbb.executeQueryP2(credit_temp_query)
									String Query="SELECT RECURRING_CREDIT_TEMPLATE_NO AS CREDIT_TEMPLATE_NO,LABEL AS TEMPLATE_NAME,FLAT_AMOUNT,CURRENCY_CD,NUM_CREDITS_REQUIRED,CREDIT_INTERVAL_MONTHS FROM ARIACORE.RECURRING_CREDIT_TEMPLATES WHERE RECURRING_CREDIT_TEMPLATE_NO="+credit_temp_no+"and client_no"+client_no
									ResultSet rs= dbb.executePlaQuery(Query)
									logi.logInfo("afte rs")
	  
	  
									while (rs.next())
									{
										  logi.logInfo("inside while")
										  ResultSetMetaData md = rs.getMetaData()
										  logi.logInfo("inside while"+rs.getMetaData())
										  int columns = md.getColumnCount()
										  logi.logInfo("columns for"+columns)
										  // Putting values into HashMap
										  coupon_frm_mpi.put("out_acct_no",acct_no)
										  for(int i=1; i<=columns; ++i)
										  {
												logi.logInfo("inside for")
												if(!(rs.getObject(i).toString().equalsIgnoreCase("null")))
												{logi.logInfo("insie PUTTTT")
	  
													  l1.add(md.getColumnName(i).toLowerCase())
	  
													  int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	  
													  logi.logInfo("frew of :+"+md.getColumnName
	  
																  (i).toLowerCase()+" is :"+freq)
	  
													  if(freq>1)
													  {
	  
															coupon_frm_mpi.put(md.getColumnName
	  
																		(i).toLowerCase()+"["+ freq +"]",rs.getObject(i))
													  }
	  
													  else
	  
	  
															coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
												}
	  
												else
													  logi.logInfo("insie else")
										  }
	  
	  
	  
										  /*    logi.logInfo("HASH #### 2"+coupon_frm_mpi)
										  value=begin
										  //value=value-1
										  logi.logInfo("before ruturn")
										  tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi) */
	  
									}
							  }
						}
						logi.logInfo("After credit template :"+coupon_frm_mpi)
	  
	  
	  
						/*    logi.logInfo("HASH #### 2"+coupon_frm_mpi)
						value=begin
						//value=value-1
						logi.logInfo("before ruturn")
						tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi)
						*/
	  
	  
						String disc_tt_query="SELECT count(RULE_NO)  FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD = (SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by client_no) AS seq FROM ARIACORE.PLAN_INST_COUPON_MAP WHERE PLAN_INSTANCE_NO="+mpi+")A)WHERE A.SEQ="+begin+")"
						String disc_tt=dbb.executeQueryP2(disc_tt_query)
	  
	  
	  
						if(disc_tt.toInteger() >=1)
						{ //Its discount rules
	  
							  logi.logInfo("<----------------- Its discount ------------------------->")
	  
	  
							  String disc_coupon_cd_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by client_no ) AS seq FROM ARIACORE.PLAN_INST_COUPON_MAP WHERE PLAN_INSTANCE_NO="+mpi+")A)WHERE A.SEQ="+begin
							  String disc_coupon_cd=dbb.executeQueryP2(disc_coupon_cd_query)
	  
	  
							  String gen_coupon_query="SELECT COUPON_CD AS OUT_COUPON_CD, APPLICATIONS_ALLOWED-1 AS COUPON_SCOPE,COMMENTS AS DESCRIPTION,TOTAL_USES,CURRENCY_CD, RECUR_DISCOUNT_FLAT_AMT,ONE_TIME_DISCOUNT_FLAT_AMT, RECUR_DISCOUNT_PCT, ONE_TIME_DISCOUNT_PCT,1 AS STATUS_IND FROM ARIACORE.COUPONS WHERE COUPON_CD='"+disc_coupon_cd+"' AND CLIENT_NO="+client_no
							  ResultSet rs_disc_gen=dbb.executePlaQuery(gen_coupon_query)
	  
							  while (rs_disc_gen.next())
							  {
									logi.logInfo("inside while")
									ResultSetMetaData md = rs_disc_gen.getMetaData()
									logi.logInfo("inside while"+rs_disc_gen.getMetaData())
									int columns = md.getColumnCount()
									logi.logInfo("columns for"+columns)
									// Putting values into HashMap
	  
	  
									coupon_frm_mpi.put("out_acct_no",acct_no)
									l1.add("out_acct_no")
									coupon_frm_mpi.put("out_client_master_plan_instance_id",mpi)
									l1.add("out_client_master_plan_instance_id")
									coupon_frm_mpi.put("out_master_plan_instance_no",mpi)
									l1.add("out_master_plan_instance_no")
	  
									//coupon_frm_mpi.put("coupon_scope","0")
									for(int i=1; i<=columns; ++i)
	  
									{
	  
										  logi.logInfo("Inside for")
										  //if(!(md.getColumnName(i).toString().equalsIgnoreCase("null")))
										  if(!(rs_disc_gen.getObject(i).toString().equalsIgnoreCase("null")))
										  {
												logi.logInfo("insie PUTTTT")
												l1.add(md.getColumnName(i).toLowerCase())
												int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	  
												logi.logInfo("freqq of :+"+md.getColumnName
	  
															(i).toLowerCase()+" is :"+freq)
	  
												if(freq>1)
												{
	  
													  coupon_frm_mpi.put(md.getColumnName
	  
																  (i).toLowerCase()+"["+ freq +"]",rs_disc_gen.getObject(i))
												}
	  
												coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs_disc_gen.getObject(i))
	  
										  }
										  else
												logi.logInfo("insie else")
									}
							  }
	  
							  logi.logInfo("loop hash"+coupon_frm_mpi)
	  
	  
							  logi.logInfo("Inside ")
							  //keys1=coupon_frm_mpi.keySet()
	  
							  //String disc_rule_query="SELECT RULE_NO  FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD = (SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by coupon_cd desc) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+")A)WHERE A.SEQ="+begin+")"
							  String disc_rule_count_query="SELECT count(RULE_NO)  FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD ='"+disc_coupon_cd+"' and client_no="+client_no
							  String disc_rule_count=dbb.executeQueryP2(disc_rule_count_query)
							  for(int r=1;r<=disc_rule_count.toInteger();r++)
							  {
									String disc_rule_query="SELECT A.RULE_NO AS RULE_NO FROM((SELECT RULE_NO,ROW_NUMBER() OVER (ORDER BY RULE_NO) AS SEQ FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD = '"+disc_coupon_cd+"')A)WHERE A.SEQ="+r
									String disc_rule=dbb.executeQueryP2(disc_rule_query)
	  
	  
	  
									//commented for max applications
									//String Query="SELECT RULE_NO,CLIENT_RULE_ID,LABEL AS RULE_NAME,SCOPE_NO, DESCRIPTION,FLAT_PERCENT_IND,CURRENCY_CD,AMOUNT,INLINE_OFFSET_IND,DURATION_TYPE_IND,MAX_APPLICATIONS_PER_ACCT,EXT_DESCRIPTION,ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE RULE_NO="+disc_rule
									String Query="SELECT RULE_NO,CLIENT_RULE_ID,LABEL AS RULE_NAME,SCOPE_NO, DESCRIPTION ,FLAT_PERCENT_IND,CURRENCY_CD,AMOUNT,INLINE_OFFSET_IND,DURATION_TYPE_IND,EXT_DESCRIPTION,ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE RULE_NO="+disc_rule+" and client_no = "+client_no
									ResultSet rs1= dbb.executePlaQuery(Query)
									ResultSetMetaData md = rs1.getMetaData()
									int columns = md.getColumnCount()
									// Putting values into HashMap
									coupon_frm_mpi.put("out_acct_no",acct_no)
									int temp_count=1
	  
	  
									while (rs1.next())
									{
	  
	  
										  for(int i=1; i<=columns; ++i)
										  {
	  
												//if(!(md.getColumnName(i).toString().equalsIgnoreCase("null")))
											  if(!(rs1.getObject(i).toString().equalsIgnoreCase("null")))
												{
													  l1.add(md.getColumnName(i).toLowerCase())
	  
													  int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	  
													  logi.logInfo("frewww of :+"+md.getColumnName(i).toLowerCase()+" is :"+freq)
	  
													  if(freq>1)
													  {
	  
															coupon_frm_mpi.put(md.getColumnName(i).toLowerCase()+"["+ freq +"]",rs1.getObject(i))
													  }
	  
													  else
															coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs1.getObject(i))
	  
												}
	  
	  
	  
												//else
												//coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs1.getObject(i))
	  
	  
										  }
									}
	  
	  
							  }
						}
						logi.logInfo("After discount rules :"+coupon_frm_mpi)
	  
						/*    logi.logInfo("HASH #### 2"+coupon_frm_mpi)
						value=begin
						//value=value-1
						tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi)
						*/
	  
	  
						//romved desc form copun_cd
						String a="SELECT count(BUNDLE_NO) FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP WHERE COUPON_CD =(SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by client_no) AS seq FROM ARIACORE.PLAN_INST_COUPON_MAP WHERE PLAN_INSTANCE_NO="+mpi+")A)WHERE A.SEQ="+begin+")"
						String b=dbb.executeQueryP2(a)
						if(b.toInteger()>0)
						{
	  
	  
							  String credit_coupon_cd_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by acct_no ) AS seq FROM ARIACORE.PLAN_INST_COUPON_MAP WHERE PLAN_INSTANCE_NO="+mpi+")A)WHERE A.SEQ="+begin
							  String credit_coupon_cd=dbb.executeQueryP2(credit_coupon_cd_query)
							  logi.logInfo("<--------------------- Its disc bundle ------------------->  ")
	  
							  String gen_coupon_query="SELECT COUPON_CD AS OUT_COUPON_CD, APPLICATIONS_ALLOWED-1 AS COUPON_SCOPE,COMMENTS AS DESCRIPTION,TOTAL_USES,CURRENCY_CD, RECUR_DISCOUNT_FLAT_AMT,ONE_TIME_DISCOUNT_FLAT_AMT, RECUR_DISCOUNT_PCT, ONE_TIME_DISCOUNT_PCT,1 AS STATUS_IND FROM ARIACORE.COUPONS WHERE COUPON_CD='"+credit_coupon_cd+"' AND CLIENT_NO="+client_no
							  ResultSet rs_credit_gen=dbb.executePlaQuery(gen_coupon_query)
	  
							  while (rs_credit_gen.next())
							  {
	  
									ResultSetMetaData md = rs_credit_gen.getMetaData()
									int columns = md.getColumnCount()
	  
									// Putting values into HashMap
	  
	  
									coupon_frm_mpi.put("out_acct_no",acct_no)
									l1.add("out_acct_no")
									coupon_frm_mpi.put("out_client_master_plan_instance_id",mpi)
									l1.add("out_client_master_plan_instance_id")
									coupon_frm_mpi.put("out_master_plan_instance_no",mpi)
									l1.add("out_master_plan_instance_no")
	  
	  
									for(int i=1; i<=columns; ++i)
	  
									{
	  
										  logi.logInfo("inside for")
										 
										 // if(!(md.getColumnName(i).toString().contains("null")))
										  if(!(rs_credit_gen.getObject(i).toString().contains("null")))
	  
	  
										  {
	  
												l1.add(md.getColumnName(i).toLowerCase())
												logi.logInfo "KEYSSS 1 : "+l1
												logi.logInfo("insie PUTTTT")
	  
												int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	  
												logi.logInfo("frew of :+"+md.getColumnName(i).toLowerCase()+" is :"+freq)
	  
												if(freq>1)
												{
	  
													  coupon_frm_mpi.put(md.getColumnName(i).toLowerCase()+"["+ ++freq
	  
																  +"]",rs_credit_gen.getObject(i))
												}
												else
	  
													  coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs_credit_gen.getObject(i))
	  
										  }
										  else
												logi.logInfo("insie else")
									}
							  }
	  
							  logi.logInfo("<--------------------- Its disc bundle 2222 ------------------->  ")
	  
							  for(int w=1;w<=b;++w)
							  {
									String coupon_code1_query="SELECT count(A.coupon_cd) FROM((SELECT coupon_cd,row_number() over(order by acct_no ) as seq FROM ARIACORE.PLAN_INST_COUPON_MAP WHERE PLAN_INSTANCE_NO="+mpi+")A) WHERE A.SEQ="+w
									String coupon_code1=dbb.executeQueryP2(coupon_code1_query)
									if(coupon_code1.toInteger() >= 1)
	  
									{
										  String coupon_code_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by acct_no ) as seq FROM ARIACORE.PLAN_INST_COUPON_MAP WHERE PLAN_INSTANCE_NO="+mpi+")A) WHERE A.SEQ="+w
										  String coupon_code=dbb.executeQueryP2(coupon_code_query)
	  
										  logi.logInfo "coupon_code____________> is "+coupon_code
	  
	  
										  String bundle_no_query="SELECT count(BUNDLE_NO) FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP WHERE COUPON_CD='"+coupon_code+"'"
										  String bundle_no=dbb.executeQueryP2(bundle_no_query)
	  
										  String final_bundle_no_query="SELECT BUNDLE_NO FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP WHERE COUPON_CD='"+coupon_code+"'"
										  String final_bundle_no=dbb.executeQueryP2(final_bundle_no_query)
										  String final_coupon_code=coupon_code
										  logi.logInfo "Inside final_coupon_code"+ final_coupon_code
										  logi.logInfo "Inside final_bundle_no"+ final_bundle_no.toInteger()
	  
	  
										  //Its discount bundle
	  
	  
	  
	  
										  String bundle_count_query="SELECT COUNT(RULE_NO) FROM ARIACORE.CLIENT_DR_BUNDLE_MEMBERS WHERE BUNDLE_NO="+final_bundle_no.toInteger()
										  String bundle_count=dbb.executeQueryP2(bundle_count_query)
										  logi.logInfo "bundle_count "+ bundle_count
										  for(int start=1;start<=bundle_count.toInteger();start++)
										  {
												logi.logInfo "start "+ start
												String rule_no_query="SELECT A.RULE_NO FROM((SELECT RULE_NO, ROW_NUMBER() OVER(ORDER BY RULE_NO) AS SEQ FROM ARIACORE.CLIENT_DR_BUNDLE_MEMBERS WHERE BUNDLE_NO="+final_bundle_no+") A) WHERE A.SEQ="+start
												String rule_no=dbb.executeQueryP2(rule_no_query)
	  
												//commented for max applications
												String bundle_values_query="SELECT DISTINCT RULE_NO,CLIENT_RULE_ID,LABEL AS RULE_NAME,SCOPE_NO, DESCRIPTION ,FLAT_PERCENT_IND,CURRENCY_CD,AMOUNT,INLINE_OFFSET_IND,DURATION_TYPE_IND,MAX_APPLICATIONS_PER_ACCT,EXT_DESCRIPTION,ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE RULE_NO="+rule_no+" and client_no="+client_no
												//String bundle_values_query="SELECT DISTINCT RULE_NO,CLIENT_RULE_ID,LABEL AS RULE_NAME,SCOPE_NO, DESCRIPTION ,FLAT_PERCENT_IND,CURRENCY_CD,AMOUNT,INLINE_OFFSET_IND,DURATION_TYPE_IND,EXT_DESCRIPTION,ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE RULE_NO="+rule_no
												ResultSet rs_disc_bundle=dbb.executePlaQuery(bundle_values_query)
												ResultSetMetaData md_disc_bundle = rs_disc_bundle.getMetaData()
												int columns = md_disc_bundle.getColumnCount()
												LinkedHashMap disc_bundle_hm= new LinkedHashMap()
												// Putting values into HashMap
	  
												while (rs_disc_bundle.next())
												{
	  
													  for(int i=1; i<=columns; ++i)
													  {
	  
															//if(!(rs_disc_bundle.getObject(i).toString().equalsIgnoreCase("null")))
															if(!(rs_disc_bundle.getObject(i).toString().contains("null")))
															{
	  
																  l1.add(md_disc_bundle.getColumnName(i).toLowerCase())
	  
	  
																  logi.logInfo "KEYSSS 2 : "+l1
	  
	  
	  
	  
																  int freq = Collections.frequency(l1,md_disc_bundle.getColumnName(i).toLowerCase())
	  
																  logi.logInfo("frew of :+"+md_disc_bundle.getColumnName(i).toLowerCase()+" is :"+freq)
	  
																  if(freq>1)
																  {
	  
																		coupon_frm_mpi.put(md_disc_bundle.getColumnName(i).toLowerCase
	  
																					()+"["+ freq +"]",rs_disc_bundle.getObject(i))
																  }
	  
	  
	  
																  else
																  {
	  
																		coupon_frm_mpi.put(md_disc_bundle.getColumnName(i).toLowerCase
	  
																					(),rs_disc_bundle.getObject(i))
	  
																  }
	  
	  
															}
	  
													  }
	  
													  l1.add("bundle_no")
													  int freq1 = Collections.frequency(l1,"bundle_no")
													  if(freq1>1)
															coupon_frm_mpi.put("bundle_no["+  freq1 +"]",final_bundle_no)
													  else
															coupon_frm_mpi.put("bundle_no",final_bundle_no)
	  
													  logi.logInfo("HASH #### 2"+coupon_frm_mpi)
													  value=begin
	  
	  
													  tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi)
	  
	  
												}
	  
										  }
	  
										  String disc_bundle_final_query="SELECT BUNDLE_NO,CLIENT_BUNDLE_ID,LABEL AS BUNDLE_NAME,DESCRIPTION FROM ARIACORE.CLIENT_DISCOUNT_RULE_BUNDLES WHERE BUNDLE_NO =(SELECT BUNDLE_NO FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP WHERE COUPON_CD ='"+final_coupon_code+"')"
	  
										  ResultSet rs_disc_bundle_final=dbb.executePlaQuery(disc_bundle_final_query)
	  
										  ResultSetMetaData md_disc_bundle_final = rs_disc_bundle_final.getMetaData()
	  
										  int columns = md_disc_bundle_final.getColumnCount()
										  logi.logInfo("inot while #### 13")
										  // Putting values into HashMap
										  coupon_frm_mpi.put("out_acct_no",acct_no)
										  //coupon_frm_mpi.put("coupon_scope",0)
										  while (rs_disc_bundle_final.next())
	  
										  {
												logi.logInfo("inot while #### 1")
												for(int i=1; i<=columns; ++i)
												{
	  
													  if(!(rs_disc_bundle_final.getObject(i).toString().equalsIgnoreCase("null")))
													  {
															l1.add(md_disc_bundle_final.getColumnName(i).toLowerCase())
															logi.logInfo "KEYSSS 3 : "+l1
															logi.logInfo("inot while #### 2")
															int freq = Collections.frequency(l1,md_disc_bundle_final.getColumnName(i).toLowerCase())
	  
															logi.logInfo("frew of :+"+md_disc_bundle_final.getColumnName(i).toLowerCase()+" is :"+freq)
	  
															if(freq>1)
															{
	  
																  coupon_frm_mpi.put(md_disc_bundle_final.getColumnName(i).toLowerCase
	  
																			  ()+"["+ freq +"]",rs_disc_bundle_final.getObject(i))
															}
	  
	  
															else
																  coupon_frm_mpi.put(md_disc_bundle_final.getColumnName(i).toLowerCase
	  
																			  (),rs_disc_bundle_final.getObject(i))
	  
													  }
												}
	  
										  }
	  
	  
										  logi.logInfo("Keyss of final hash :"+l1)
	  
									}
	  
							  }
						}
						logi.logInfo("After discount bundle :"+coupon_frm_mpi)
	  
	  
	  
						logi.logInfo("HASHMAP AFTER ITERAATION "+begin+" "+tempar)
						logi.logInfo("ENND D 222")
						logi.logInfo("HASH #### 2"+coupon_frm_mpi)
						coupon_frm_mpi.sort()
						value=begin
						//value=value-1
	  
						tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi)
				  }
	  
				  return tempar
	  
	  
			}
	  
	
				
			
						LinkedHashMap md_get_acct_plan_history_m(String acct_hierarchy)
						{
							
							logi.logInfo "**** into md_get_acct_plan_history_m **** "
							def client_no=Constant.mycontext.expand('${Properties#Client_No}')
							ConnectDB db = new ConnectDB()
							LinkedHashMap plan_hist_hm =  new LinkedHashMap()
							int count=1
							InputMethods im = new InputMethods()
							String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
							logi.logInfo("Given Acct no:" + acct_no)
							
							String plan_hist_query="SELECT PI.PLAN_INSTANCE_NO,PI.MASTER_PLAN_INSTANCE_NO AS MASTER_PLAN_INSTANCE_NO,PIPH.NEW_PLAN AS NEW_PLAN_NO,CP.CLIENT_PLAN_ID AS NEW_CLIENT_PLAN_ID,CP.PLAN_NAME AS NEW_PLAN_NAME,PIPH.OLD_STATUS_CD AS OLD_STATUS_CD, PIPH.NEW_STATUS_CD  AS NEW_STATUS_CD,  PIPH.NEW_RECURRING_FACTOR AS NEW_PLAN_UNITS, PIPH.NEW_ALT_SCHEDULE_NO AS NEW_RATE_SCHEDULE_NO,PRS.CLIENT_RATE_SCHEDULE_ID AS NEW_CLIENT_RATE_SCHEDULE_ID,  PIPH.NEW_PLAN_INSTANCE_CDID  AS NEW_CLIENT_PLAN_INSTANCE_ID, TO_CHAR(PIPH.FROM_DATE,'YYYY-MM-DD HH:MI:SS AM') AS UPDATE_DATE,PIPH.OLD_RECURRING_FACTOR AS old_plan_units,PIPH.OLD_ALT_SCHEDULE_NO AS old_rate_schedule_no,PIPH.OLD_PLAN as old_plan_no,PIPH.OLD_PLAN_INSTANCE_CDID as old_client_plan_instance_id,  CPO.PLAN_NAME old_plan_name,CPO.CLIENT_PLAN_ID as old_client_plan_id,PIPH.OLD_DUNNING_STATE,piph.new_dunning_state  from ARIACORE.PLAN_INSTANCE  pi join ariacore.PLAN_INSTANCE_PLAN_HIST piph on pi.plan_instance_no=piph.plan_instance_no AND PI.CLIENT_NO = PIPH.CLIENT_NO left outer join ARIACORE.PLAN_RATE_SCHEDULE prs on prs.SCHEDULE_NO=PIPH.NEW_ALT_SCHEDULE_NO AND PRS.CLIENT_NO=PI.CLIENT_NO LEFT OUTER JOIN ARIACORE.CLIENT_PLAN CP ON CP.PLAN_NO=PIPH.NEW_PLAN AND CP.CLIENT_NO = PIPH.CLIENT_NO LEFT OUTER JOIN ARIACORE.CLIENT_PLAN CPO ON CPO.PLAN_NO=PIPH.OLD_PLAN AND CPO.CLIENT_NO = PIPH.CLIENT_NO where pi.acct_no="+acct_no+" and pi.client_no="+client_no+" order by PI.PLAN_INSTANCE_NO,PIPH.FROM_DATE,piph.SEQ_NUM"
							ResultSet rs = db.executePlaQuery(plan_hist_query)
							ResultSetMetaData md3 = rs.getMetaData()
							int columns3 = md3.getColumnCount();
							while (rs.next())
							{
								LinkedHashMap temp = new LinkedHashMap()
								LinkedList l1 =  new LinkedList()
								for(int i=1; i<=columns3; ++i)
								{
									
									logi.logInfo("inside for")
									if(!(rs.getObject(i).toString().equalsIgnoreCase("null")))
					
					
									{
										logi.logInfo("insie PUTTTT")
										l1.add(md3.getColumnName(i).toLowerCase())
										int freq = Collections.frequency(l1,md3.getColumnName(i).toLowerCase())
					
										logi.logInfo("frew of :+"+md3.getColumnName(i).toLowerCase()+" is :"+freq)
					
										if(freq>1)
										{
					
											temp.put(md3.getColumnName(i).toLowerCase()+"["+ freq +"]",rs.getObject(i))
										}
										else
					
											temp.put(md3.getColumnName(i).toLowerCase(),rs.getObject(i))
										if(rs.getObject('NEW_RATE_SCHEDULE_NO')== null)
										{
										  temp.remove("new_client_rate_schedule_id")
										}
					
								}
							}
					temp=temp.sort()
								plan_hist_hm.put("plan_history_row["+count+"]",temp)
								++count
								
								
								}
							
							logi.logInfo(" Out of **** into md_get_acct_plan_history_m ****")
							return plan_hist_hm
							}
							
					
						/**
						 * To get the dunning group number for the account
						 * @param input
						 * @return
						 */
						String md_GET_DUNNING_NO(String input)
						{
							logi.logInfo("method md_GET_PLAN_NO")
							String planNo = null
							String tcid= Constant.TESTCASEID
							String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
							logi.logInfo(clientNo)
							String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
							logi.logInfo("Account No : "+acct_no)
							String planNum = "select dunning_group_no from (select dunning_group_no,rownum as rnum from ariacore.acct_dunning_group where acct_no = "+	acct_no + "order by dunning_group_no) where rnum= " +input
							logi.logInfo("Query for plan no : "+planNum)
							ConnectDB db = new ConnectDB()
							planNo = db.executeQueryP2(planNum)
							logi.logInfo("Plan No Is  :::: "+planNo)
							return planNo
						}

						
						
						
						
						/************get_order_m**********/
 
						
 /**
  * @param acct_no
  * @return hash of order rows or order details
  */
 public LinkedHashMap md_get_order_m(String param){
	 
		 logi.logInfo "Inside the method"
		 String order_no = getValueFromRequest("get_order_m",ExPathRpc.order_no);logi.logInfo("Order_no : "+order_no)
		 String limit =getValueFromRequest("get_order_m",ExPathRpc.limit_records);logi.logInfo("Limit : "+limit)
		 LinkedHashMap row = new LinkedHashMap<String , HashMap<String,String>>()
		 InputMethods im = new InputMethods()
		 ConnectDB db = new ConnectDB()
		 String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(param,null)
		 if(order_no.equals("NoVal")){
			 logi.logInfo"value not in order_no"
			 if(limit.equals("NoVal")){
				 logi.logInfo "value not in limit"
				 
				 String count_query = "SELECT count(*) FROM ARIACORE.ORDERS WHERE ACCT_NO = "+acct_no
				 String cc = db.executeQueryP2(count_query).toString()
				 int c = Integer.parseInt(cc)
				 for(int i=1;i<=c;i++){
					 logi.logInfo("entered into the loop")
					 int s =i-1;
					 logi.logInfo("counting : "+s)
				 row.put("orders_row["+s+"]",md_get_ORDER_m(acct_no,i))
				 
				 }
				 return row.sort()
				 
			 }else{
			 logi.logInfo "value in limit"
			 //construction is in process
			 int c = Integer.parseInt(limit)
			 for(int i=1;i<=c;i++){
				 logi.logInfo("entered into the loop")
				 int s =i-1;
				 logi.logInfo("counting : "+s)
			 row.put("orders_row["+s+"]",md_get_ORDER_m(acct_no,i))
			 
			 }
			 return row.sort()
		 }
		 }else{
				 logi.logInfo "value in order_no"
				 
				 String order_row = " select od.order_no,od.amount,od.currency_cd,os.LABEL AS STATUS_LABEL,od.MASTER_PLAN_INSTANCE_NO as plan_instance_no,"+
									 " PI.PLAN_INSTANCE_CDID AS client_plan_instance_id,od.invoice_no,TO_CHAR(od.create_date,'YYYY-MM-DD') AS CREATE_DATE,"+
									 " TO_CHAR(od.update_date,'YYYY-MM-DD') AS UPDATE_DATE,od.comments,od.CLIENT_ORDER_ID,AI.TRANS_ID AS transaction_id,"+
									 " OD.ORDER_CREATE_API_RECEIPT_ID AS order_create_client_receipt_id,"+
									 " OD.ORDER_STATUS_API_RECEIPT_ID AS order_status_client_receipt_id,OD.STATEMENT_MESSAGE,OD.PURCHASE_ORDER_NO AS po_num"+
									 " FROM ARIACORE.ORDERS od join ARIACORE.ORDER_STATUSES os on od.status_cd = os.status_cd"+
									 " LEFT JOIN ARIACORE.ALL_INVOICES AI ON OD.INVOICE_NO = AI.INVOICE_NO"+
									 " JOIN ARIACORE.PLAN_INSTANCE PI ON OD.MASTER_PLAN_INSTANCE_NO = PI.PLAN_INSTANCE_NO"+
									 " WHERE OD.ACCT_NO = "+acct_no+" AND OD.ORDER_NO = "+order_no
									 ResultSet rs1 = db.executePlaQuery(order_row)
									 ResultSetMetaData md1 = rs1.getMetaData();
									 int columns1 = md1.getColumnCount();
									 while (rs1.next()){
									 
										  for(int s=1; s<=columns1; ++s){
											  if(rs1.getObject(s).toString().equals("null")){
												  logi.logInfo("No value in "+rs1.getObject(s))
											  }else{logi.logInfo("Value inserting in hash map")
											  row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
											  logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
											  
										  }
									 }
		 
		 return row.sort()
		 }
		 
 
		 
		 
 }
						
 /**
  * @param acct_no
  * @param sequence no i
  * @return order details
  */
 public LinkedHashMap md_get_ORDER_m(String acct_no, int i){
	 LinkedHashMap row = new LinkedHashMap<String ,String>()
	 ConnectDB db = new ConnectDB()
	 String orders_row = "SELECT ord.order_no,ord.amount,ord.currency_cd,ord.STATUS_LABEL,ord.plan_instance_no,"+
						 " ord.client_plan_instance_id,ord.invoice_no,ord.CREATE_DATE,"+
						 " ord.UPDATE_DATE,ord.comments,ord.CLIENT_ORDER_ID,ord.transaction_id,"+
						 " ord.order_create_client_receipt_id,"+
						 " ord.order_status_client_receipt_id,ord.STATEMENT_MESSAGE,ord.po_num FROM "+
						 " (select od.order_no,od.amount,od.currency_cd,os.LABEL AS STATUS_LABEL,od.MASTER_PLAN_INSTANCE_NO as plan_instance_no,"+
						 " PI.PLAN_INSTANCE_CDID AS client_plan_instance_id,od.invoice_no,TO_CHAR(od.create_date,'YYYY-MM-DD') AS CREATE_DATE,"+
						 " TO_CHAR(od.update_date,'YYYY-MM-DD') AS UPDATE_DATE,od.comments,od.CLIENT_ORDER_ID,AI.TRANS_ID AS transaction_id,"+
						 " OD.ORDER_CREATE_API_RECEIPT_ID AS order_create_client_receipt_id,"+
						 " OD.ORDER_STATUS_API_RECEIPT_ID AS order_status_client_receipt_id,OD.STATEMENT_MESSAGE,OD.PURCHASE_ORDER_NO AS po_num,"+
						 " row_number() over(order by order_no desc) as seq"+
						 " FROM ARIACORE.ORDERS od join ARIACORE.ORDER_STATUSES os on od.status_cd = os.status_cd"+
						 " LEFT JOIN ARIACORE.ALL_INVOICES AI ON OD.INVOICE_NO = AI.INVOICE_NO"+
						 " JOIN ARIACORE.PLAN_INSTANCE PI ON OD.MASTER_PLAN_INSTANCE_NO = PI.PLAN_INSTANCE_NO"+
						 " WHERE OD.ACCT_NO = "+acct_no+")ord where ord.seq = "+i
						 ResultSet rs1 = db.executePlaQuery(orders_row)
						 ResultSetMetaData md1 = rs1.getMetaData();
						 int columns1 = md1.getColumnCount();
						 while (rs1.next()){
						 
							  for(int s=1; s<=columns1; ++s){
								  if(rs1.getObject(s).toString().equals("null")){
									  logi.logInfo("No value in "+rs1.getObject(s))
								  }else{logi.logInfo("Value inserting in hash map")
								  row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
								  logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
								  
							  }
						 }
	 return row.sort()
	 
 }
 /**
  * @param param (acct_no)
  * @return hash of line items of all the orders in an account
  */
 public LinkedHashMap md_get_order_line_items_m(String param){
	 LinkedHashMap row = new LinkedHashMap<String , HashMap<String,String>>()
	 InputMethods im = new InputMethods()
	 ConnectDB db = new ConnectDB()
	 String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(param,null)
	 String count_query = "SELECT count(*) FROM ARIACORE.ORDERS OD JOIN ARIACORE.ORDER_ITEMS OT ON OD.ORDER_NO = OT.ORDER_NO WHERE ACCT_NO = " +acct_no
	 String cc = db.executeQueryP2(count_query).toString()
	 int c = Integer.parseInt(cc)
	 for(int i=1;i<=c;i++){
		 logi.logInfo("entered into the loop")
		 int s =i-1;
		 logi.logInfo("counting : "+s)
	 row.put("order_items_row["+s+"]",md_get_order_line_items_row_m(acct_no,i))
	 
	 }
	 return row.sort()
 }
 /**
  * @param acct_no
  * @param  sequence no i
  * @return hash of line item details
  */
 public LinkedHashMap md_get_order_line_items_row_m(String acct_no,int i){
	 ConnectDB db = new ConnectDB()
	 LinkedHashMap row = new LinkedHashMap<String ,String>()
	 String item_query = "select LT.LINE_NO,LT.CLIENT_SKU,LT.label,LT.alt_label,LT.long_desc,LT.units,LT.unit_amount,LT.line_amount,LT.line_comments from( "+
						 " SELECT OT.LINE_NO,IT.CLIENT_SKU,IT.label,IT.CLIENT_ITEM_ID AS alt_label,IT.long_desc,ot.units,ot.unit_amount,ot.line_amount,ot.line_comments,"+
						 " row_number() over(order by od.order_no desc,ot.line_no) as seq "+
						 " FROM ARIACORE.ORDERS OD "+
						 " JOIN ARIACORE.ORDER_ITEMS OT"+
						 " ON OD.ORDER_NO = OT.ORDER_NO"+
						 " JOIN ARIACORE.INVENTORY_ITEMS IT"+
						 " ON OT.ITEM_NO = IT.ITEM_NO AND OD.CLIENT_NO = IT.CLIENT_NO"+
						 " WHERE ACCT_NO  = "+acct_no+")LT where lt.seq = "+i
						 ResultSet rs1 = db.executePlaQuery(item_query)
						 ResultSetMetaData md1 = rs1.getMetaData();
						 int columns1 = md1.getColumnCount();
						 while (rs1.next()){
						 
							  for(int s=1; s<=columns1; ++s){
								  if(rs1.getObject(s).toString().equals("null")){
									  logi.logInfo("No value in "+rs1.getObject(s))
								  }else{logi.logInfo("Value inserting in hash map")
								  row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
								  logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
								  
							  }
						 }
	 return row.sort()
	 
 }
						
	
 LinkedHashMap md_order_line_items_m(String acct_hierarchy1)
  {
	  
	  logi.logInfo("acct_hierarchy1:::"+acct_hierarchy1)
  
  String acct_hierarchy=acct_hierarchy1.split("#")[0]
  String seq_no=acct_hierarchy1.split("#")[1]
  String identifier=acct_hierarchy1.split("#")[2]
 
  
  logi.logInfo("seq_no:::"+seq_no+"   acct_hierarchy  "+acct_hierarchy)
  
	  LinkedHashMap h1= new LinkedHashMap() //	LinkedList l1= new LinkedList()
	  logi.logInfo("into md_order_line_items_m")
	  ConnectDB dbb = new ConnectDB()
  
	  InputMethods im=new InputMethods()
	  def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	  String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
	  logi.logInfo("acct_no"+acct_no)
	  
	  String invoice_no_query="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO="+acct_no
	  String invoice_no=dbb.executeQueryP2(invoice_no_query)
	 
	  String invoice_line_items_query="SELECT DISTINCT GL.SEQ_NUM  AS LINE_NO, GL.SERVICE_NO AS SERVICE_NO , ALS.SERVICE_NAME   AS SERVICE_NAME, GL.COMMENTS AS DESCRIPTION, TO_CHAR(GL.START_DATE, 'YYYY-MM-DD') AS DATE_RANGE_START,TO_CHAR(GL.END_DATE, 'YYYY-MM-DD')   AS DATE_RANGE_END , ACS.CLIENT_SERVICE_ID AS CLIENT_SERVICE_ID FROM ARIACORE.ALL_SERVICE ALS JOIN ARIACORE.GL_DETAIL GL ON ALS.SERVICE_NO = GL.SERVICE_NO AND GL.CLIENT_NO  = ALS.CLIENT_NO JOIN ARIACORE.ALL_CLIENT_SERVICE ACS ON ACS.SERVICE_NO = GL.SERVICE_NO JOIN ARIACORE.GL GL1 ON GL1.INVOICE_NO=GL.INVOICE_NO AND ACS.CLIENT_NO = GL.CLIENT_NO AND GL.INVOICE_NO ="+invoice_no+" AND GL.CLIENT_NO  ="+client_no+" ORDER BY GL.SEQ_NUM"
	  ResultSet rs = dbb.executePlaQuery(invoice_line_items_query)
	  ResultSetMetaData md = rs.getMetaData()
	  int columns = md.getColumnCount()
	  // Putting values into HashMap
	  logi.logInfo("COUNTTTTTTTTTT :"+columns)
	  
	  int count=0
	  while (rs.next())
	  {
		  LinkedHashMap temp= new LinkedHashMap()
		  LinkedList l1= new LinkedList()
		  
	  
		  for(int i=1; i<=columns; ++i)
		  {
	  
			  if(!(rs.getObject(i).toString().equalsIgnoreCase("null")))
			  
			  {
				  
				  logi.logInfo(" GET OBJJJJJJJJJ"+rs.getObject(i).toString()+" and IIII"+i)
				  l1.add(md.getColumnName(i).toLowerCase())
	  
				  int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	  
				  logi.logInfo("frewww of :+"+md.getColumnName(i).toLowerCase()+" is :"+freq)
	  
				  if(freq>1)
				  {
	  
					  
					  temp.put(md.getColumnName(i).toLowerCase()+"["+ freq +"]",rs.getObject(i))
				  }
	  
				  else
					  
				  temp.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
	  
			 
				 
				   }
			  logi.logInfo("hashmap of ORDER LINE :"+temp)
	  
		  }
		  temp=temp.sort()
		  if (identifier.toString().equalsIgnoreCase("order"))
			  {
				  h1.put("cart_invoice_line_items_row["+count+"]",temp)
			  }
			  else if(identifier.toString().equalsIgnoreCase("order_with_plan"))
		  {
			  h1.put("cart_inv_line_items_row["+count+"]",temp)
		  
		  
		  }
		  ++count
	  }
	  
	  
	  logi.logInfo("FINAL hashmap of ORDER_LINE ITEMS :"+h1)
	  
	  h1.sort()
	  return h1
	  
  }
 

 
  LinkedHashMap md_order_gen_details_m(String acct_hierarchy1)
  {
  
	  LinkedHashMap h1= new LinkedHashMap()
	  LinkedList l1= new LinkedList()
	  logi.logInfo("into md_order_gen_details_m")
	  ConnectDB dbb = new ConnectDB()
  
	  InputMethods im=new InputMethods()
	  String acct_hierarchy=acct_hierarchy1.split("#")[0]
	  String seq_no=acct_hierarchy1.split("#")[1]
	  
	  logi.logInfo("acct_hier  :"+acct_hierarchy+" seq_no  "+seq_no)
	  
	  String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
	  logi.logInfo("acct_no"+acct_no)
	  
	  
	  String gen_details_query
	  //String invoice_no_query="SELECT INVOICE_NO FROM ARIACORE.ORDERS WHERE ACCT_NO="+acct_no
	  String invoice_no_query="select max(invoice_no) from ariacore.gl where  LONG_COMMENTS like '%order no%' and acct_no="+acct_no
	  
	  //String invoice_no=getValueFromResponse(library.Constants.Constant.SERVICENAME, "//*/invoice_no[1]")
	  
 
 
	  String invoice_no=dbb.executeQueryP2(invoice_no_query)
	  
	  String payment_db=dbb.executeQueryP2("select count(*) from ariacore.payments where acct_no="+acct_no)
	  logi.logInfo("payment "+payment_db)
	  logi.logInfo("invoice_no"+invoice_no)
	  if(invoice_no==null || payment_db.toInteger() == 0)
	  {
		 //gen_details_query= "SELECT ORDER_NO,CASE when transaction_id=null then '0' else transaction_id END as transaction_id,INVOICE_NO FROM ARIACORE.ORDERS WHERE ACCT_NO="+acct_no
		  gen_details_query="SELECT O.ORDER_NO,CASE when T.EVENT_NO=null then '0' else TO_CHAR(T.EVENT_NO)  END as transaction_id,O.INVOICE_NO FROM ARIACORE.ORDERS O JOIN ARIACORE.ACCT_TRANSACTION T ON O.ACCT_NO=T.ACCT_NO  AND O.INVOICE_NO=nvl(T.INVOICE_NO,O.INVOICE_NO) AND O.ACCT_NO="+acct_no
		  
	  }
	  
	  else
	  {
	  String source_no_query="SELECT MAX(INVOICE_DETAIL_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"and usage_units is not null"
	  String source_no=dbb.executeQueryP2(source_no_query)
	  String transaction_id_query="SELECT EVENT_NO FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" AND SOURCE_NO="+source_no
	  String transaction_id=dbb.executeQueryP2(transaction_id_query)
	  
	  if(transaction_id==null)
	  {
	  h1.put("transaction_id","0")
	  gen_details_query=" SELECT ORDER_NO,INVOICE_NO,PROC_PYMNT_ID as proc_payment_id,total_charges_before_tax,total_charges_after_tax,proc_auth_code,proc_avs_response, Proc_cavv_response,proc_cvv_response, proc_status_code,proc_status_text,proc_merch_comments FROM(SELECT O.ORDER_NO,ROW_NUMBER() OVER(ORDER BY O.ORDER_NO) AS SEQ_NO ,O.INVOICE_NO,P.PROC_PYMNT_ID,P.LEFT_TO_APPLY as total_charges_before_tax,P. AMOUNT as total_charges_after_tax,P.AUTH_CODE as proc_auth_code,P.PROC_VALIDATE_ADDRESS as proc_avs_response, P.PROC_CARD_SECURE as proc_cavv_response,P.PROC_VALIDATE_CVV as proc_cvv_response,P.STATUS_CODE as proc_status_code,P.status_text as proc_status_text,P.merch_comments as proc_merch_comments from ARIACORE.PAYMENTS P  join ARIACORE.ORDERS O on P.ACCT_NO=O.ACCT_NO  where P.ACCT_NO="+acct_no+"  and O.INVOICE_NO="+invoice_no+" ORDER BY P.PROC_PYMNT_ID DESC)A WHERE A.SEQ_NO="+seq_no
	  
	  
	  }else
		  {
	  
	  h1.put("transaction_id",transaction_id)
	  
	  //String gen_details_query=" SELECT ORDER_NO,INVOICE_NO,PROC_PYMNT_ID as proc_payment_id,total_charges_before_tax,total_charges_after_tax,proc_auth_code,proc_avs_response, Proc_cavv_response,proc_cvv_response, proc_status_code FROM(SELECT O.ORDER_NO,ROW_NUMBER() OVER(ORDER BY O.ORDER_NO) AS SEQ_NO ,O.INVOICE_NO,P.PROC_PYMNT_ID,P.LEFT_TO_APPLY as total_charges_before_tax,P. AMOUNT as total_charges_after_tax,P.AUTH_CODE as proc_auth_code,P.PROC_VALIDATE_ADDRESS as proc_avs_response, P.PROC_CARD_SECURE as proc_cavv_response,P.PROC_VALIDATE_CVV as proc_cvv_response,P.STATUS_CODE as proc_status_code from ARIACORE.PAYMENTS P  join ARIACORE.ORDERS O on P.CREATE_SESSION_ID=O.CREATE_SESSION_ID  where P.ACCT_NO="+acct_no+"  ORDER BY P.PROC_PYMNT_ID DESC)A WHERE A.SEQ_NO="+seq_no
	 //  gen_details_query=" SELECT ORDER_NO,INVOICE_NO,PROC_PYMNT_ID as proc_payment_id,total_charges_before_tax,total_charges_after_tax,proc_auth_code,proc_avs_response, Proc_cavv_response,proc_cvv_response, proc_status_code FROM(SELECT O.ORDER_NO,ROW_NUMBER() OVER(ORDER BY O.ORDER_NO) AS SEQ_NO ,O.INVOICE_NO,P.PROC_PYMNT_ID,P.LEFT_TO_APPLY as total_charges_before_tax,P. AMOUNT as total_charges_after_tax,P.AUTH_CODE as proc_auth_code,P.PROC_VALIDATE_ADDRESS as proc_avs_response, P.PROC_CARD_SECURE as proc_cavv_response,P.PROC_VALIDATE_CVV as proc_cvv_response,P.STATUS_CODE as proc_status_code from ARIACORE.PAYMENTS P  join ARIACORE.ORDERS O on P.ACCT_NO=O.ACCT_NO  where P.ACCT_NO="+acct_no+"  ORDER BY P.PROC_PYMNT_ID DESC)A WHERE A.SEQ_NO="+seq_no
		  gen_details_query="SELECT ORDER_NO,INVOICE_NO,PROC_PAYMENT_ID,proc_auth_code,proc_avs_response,proc_cavv_response,proc_cvv_response,proc_status_code,proc_status_text,proc_merch_comments FROM(SELECT O.ORDER_NO,ROW_NUMBER() OVER(ORDER BY O.ORDER_NO) AS SEQ_NO ,O.INVOICE_NO,P.PROC_PYMNT_ID as PROC_PAYMENT_ID,P.AUTH_CODE as proc_auth_code,P.PROC_VALIDATE_ADDRESS as proc_avs_response, P.PROC_CARD_SECURE as proc_cavv_response,P.PROC_VALIDATE_CVV as proc_cvv_response,P.STATUS_CODE as proc_status_code,P.status_text as proc_status_text,P.merch_comments as proc_merch_comments from ARIACORE.PAYMENTS P  join ARIACORE.ORDERS O on P.CREATE_SESSION_ID=O.CREATE_SESSION_ID  where P.ACCT_NO="+acct_no+"  and O.Invoice_no="+invoice_no+"ORDER BY P.PROC_PYMNT_ID DESC)A WHERE A.SEQ_NO="+seq_no
	  }
	  }
	  ResultSet rs = dbb.executePlaQuery(gen_details_query)
	  ResultSetMetaData md = rs.getMetaData()
	  int columns = md.getColumnCount()
	  // Putting values into HashMap
	  
	  
	  while (rs.next())
	  {
	  
	  
		  for(int i=1; i<=columns; ++i)
		  {
	  
			  if(!(rs.getObject(i).toString().equalsIgnoreCase("null")))
			  {
				  l1.add(md.getColumnName(i).toLowerCase())
	  
				  int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	  
				  logi.logInfo("frewww of :+"+md.getColumnName(i).toLowerCase()+" is :"+freq)
	  
				  if(freq>1)
				  {
	  
					  h1.put(md.getColumnName(i).toLowerCase()+"["+ freq +"]",rs.getObject(i))
				  }
	  
				  else
					  h1.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
	  
			  }
			  logi.logInfo("hashmap of ORDER GEN :"+h1)
	  
		  }
	  }
	  
	  logi.logInfo("FINAL hashmap of ORDER_LINE GEN :"+h1)
	  
	  h1=h1.sort()
	  logi.logInfo("FINAL hashmap of ORDER_LINE GEN SORTEDDDD:"+h1)
	  return h1
	  
	  
	  }
 
 
 
	 
	 def md_Batch_query_verification(String str){
		 ConnectDB db = new ConnectDB()
		 boolean value
		 String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		 String del_1 = "DELETE revrec.rr_staged_invoices WHERE client_no = "+client_no
		 String commit = "commit;"
		 String exe_1 = db.executeQueryP3(del_1).toString()
		 db.connection.commit();
		 logi.logInfo "passed the first query"
	 //	db.executeQueryP3(commit)
		 String del_2 = "DELETE revrec.client_rr_job_stage_date_log WHERE client_no ="+client_no
		 String exe_2 = db.executeQueryP3(del_2).toString()
		 db.connection.commit();
		 logi.logInfo "passed the second query"
	 //	db.executeQueryP3(commit)
		 String del_3 = "DELETE revrec.rr_transformed_invoices WHERE client_no = "+client_no
		 String exe_3 = db.executeQueryP3(del_3).toString()
		 db.connection.commit();
	 //	db.executeQueryP3(commit)
		 String del_4 = "DELETE revrec.rr_summarized_invoices WHERE client_no = "+client_no
		 String exe_4 = db.executeQueryP3(del_4).toString()
	 //	db.executeQueryP3(commit)
		 db.connection.commit();
		 String del_5 = "Delete Revrec.Rr_Summarized_Periods_Rpt Where Client_No = "+client_no
		 String exe_5 = db.executeQueryP3(del_5).toString()
	 //	db.executeQueryP3(commit)
		 db.connection.commit();
		 logi.logInfo "complted all delete"
		 String batch_query = "BEGIN REVREC.REVRECETLMGR.STAGE_TRANS_AND_SUM_ALL(INCLIENTNO =>"+client_no+", INDAYSOFFSETSTART =>5); END;"
		 logi.logInfo "before query : "+batch_query
		 String batch = db.executeQueryP3(batch_query).toString()
		 logi.logInfo "after query "+batch
	 //	db.executeQueryP3(commit)
		 db.connection.commit();
		 if(batch.equals("true")){
			 value = true
			 
		 }else{
		 value = false
		 }
		 return value
	 }
	 
	 def md_UPDATE_ACCT_PLAN_m(String s){
		 logi.logInfo("********md_UPDATE_ACCT_PLAN_m Start**************")
		 ExpValueConstants exp = new ExpValueConstants()
		 InputMethods im = new InputMethods()
		 String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
		 logi.logInfo("Given Account number" + acct_no)
		 ConnectDB db = new ConnectDB()
		 //String invoice_no = vm.getValueFromResponse("update_acct_plan_m",ExPathRpcEnc.Invoice_No_Resp_Val)
		 //String invoice_no = md_getValFromSpecificnodeK_m("ns1:update_acct_plan_mResponse,1#k,13")
		// String invoice_no = getValueFromResponse("update_acct_plan_m+a","//ns1:update_acct_plan_mResponse[1]/invoice_no[1]");
		 //String invoice_no = db.executeQueryP2("SELECT MAX(INVOICE_NO) FROM ariacore.GL where acct_no = "+acct_no)
		 String invoice_no = md_getValFromNodeAndIndex_m("invoice_no,1")
		 /*HashMap hh=md_getSpecificnodeAsList_m("invoice_no")
		 ArrayList<String> invoice_no = new ArrayList<String>(hh.values());*/
		 logi.logInfo("Thrown Invoice No" + invoice_no)
		 LinkedHashMap<String, HashMap<String,String>> row = new LinkedHashMap<String, HashMap<String,String>>()
		 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		 logi.logInfo("Given Client no" + client_no)
		
		 logi.logInfo("Given Invoice No" + invoice_no)
		 int counts= Integer.parseInt(db.executeQueryP2("SELECT count(*) from ariacore.gl_detail where invoice_no="+invoice_no).toString())
 
	   //  String q_count = "SELECT a.plan_no,  a.client_plan_id,  a.plan_name,  a.line_no,  a.proration_factor,  a.date_range_start, a.date_range_end,a.service_no,a.service_name, a.service_coa_id, a.credit_coupon_code, a.units, a.rate_per_unit, a.client_service_id FROM (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,    gld.plan_no                                  AS plan_no,    gld.service_no AS service_no,    gld.usage_units AS units,gld.usage_rate AS rate_per_unit, gld.proration_factor AS proration_factor,gld.start_date AS date_range_start,gld.end_date AS date_range_end,gld.orig_coupon_cd AS credit_coupon_code,CS.SERVICE_NAME AS service_name,cs.client_service_id AS client_service_id,cs.coa_id AS service_coa_id,CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID AS client_plan_id, gld.comments AS line_description FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO       =PS.PLAN_NO  WHERE gld.invoice_no= 18112953  )a WHERE line_no=1"
		 for(int i=1;i<=counts;i++){
			   int t =i-1;
			   logi.logInfo("counting"+t)
 
			   row.put("acct_plan_line_items_row["+t+"]",md_UPDATE_ACCT_PLAN_m_row(invoice_no,i))
		 }
		 logi.logInfo("returning main hash : "+row)
		 row = row.sort()
		 return row
		 
		 logi.logInfo("********md_UPDATE_ACCT_PLAN_m Ends**************")
   }
 
   public LinkedHashMap md_UPDATE_ACCT_PLAN_m_row(String inv_no, int i){
	   logi.logInfo("********md_UPDATE_ACCT_PLAN_m Start**************")
	   ConnectDB db = new ConnectDB()
	   LinkedHashMap <String,String> row = new LinkedHashMap<String,String>()
	   String query = "SELECT a.plan_no,  a.client_plan_id,  a.plan_name,  a.line_no,  a.proration_factor,  a.date_range_start, a.date_range_end,a.description,a.line_amount,a.line_units,a.line_base_units,a.service_no,a.service_name, a.credit_coupon_code, a.rate_per_unit, a.client_service_id FROM (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,    gld.plan_no AS plan_no,    gld.service_no AS service_no,  gld.usage_rate AS rate_per_unit, gld.proration_factor AS proration_factor,TO_CHAR(gld.start_date, 'YYYY-MM-DD') AS date_range_start,TO_CHAR(gld.end_date, 'YYYY-MM-DD') AS date_range_end,gld.comments as description,gld.debit AS line_amount,gld.usage_units AS line_units,gld.base_plan_units AS line_base_units,gld.orig_coupon_cd AS credit_coupon_code,CS.SERVICE_NAME AS service_name,cs.client_service_id AS client_service_id,CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID AS client_plan_id, gld.comments AS line_description FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO =PS.PLAN_NO  WHERE gld.invoice_no = "+inv_no+"  )a WHERE line_no="+i+""
	   ResultSet rs1 = db.executePlaQuery(query)
	   ResultSetMetaData md1 = rs1.getMetaData();
	   int columns1 = md1.getColumnCount();
	   while (rs1.next()){
 
		   for(int s=1; s<=columns1; ++s){
			   if(rs1.getObject(s).toString().equals("null")||rs1.getObject(s).toString()==null){
				   logi.logInfo("No value in "+rs1.getObject(s))
			   }else{logi.logInfo("Value inserting in hash map")
				   row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
			   logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s).toLowerCase())
 
		   }
	   }
	 
	   logi.logInfo("returning the row hash : "+row)
 
	   return row.sort()
   }
	 
   def md_ASSIGN_ACCT_PLAN_m(String s){
	   logi.logInfo("********md_UPDATE_ACCT_PLAN_m Start**************")
	   VerificationMethods vm =new VerificationMethods()
	   //String invoice_no = vm.getValueFromResponse("update_acct_plan_m",ExPathRpcEnc.Invoice_No_Resp_Val)
	   String invoice_no = md_getValFromSpecificnodeK_m("ns1:assign_acct_plan_mResponse,1#k,8")
	   //String invoice_no = md_getValFromNodeAndIndex_m("invoice_no,1")
	   /*HashMap hh=md_getSpecificnodeAsList_m("invoice_no")
	   ArrayList<String> invoice_no = new ArrayList<String>(hh.values());*/
	   logi.logInfo("Thrown Invoice No" + invoice_no)
	   LinkedHashMap<String, HashMap<String,String>> row = new LinkedHashMap<String, HashMap<String,String>>()
	   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	   logi.logInfo("Given Client no" + client_no)
	   ConnectDB db = new ConnectDB()
	   InputMethods im = new InputMethods()
	   String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
	   logi.logInfo("Given Account number" + acct_no)
	   logi.logInfo("Given Invoice No" + invoice_no)
	   int counts= Integer.parseInt(db.executeQueryP2("SELECT count(*) from ariacore.gl_detail where invoice_no="+invoice_no).toString())
 
	 //  String q_count = "SELECT a.plan_no,  a.client_plan_id,  a.plan_name,  a.line_no,  a.proration_factor,  a.date_range_start, a.date_range_end,a.service_no,a.service_name, a.service_coa_id, a.credit_coupon_code, a.units, a.rate_per_unit, a.client_service_id FROM (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,    gld.plan_no                                  AS plan_no,    gld.service_no AS service_no,    gld.usage_units AS units,gld.usage_rate AS rate_per_unit, gld.proration_factor AS proration_factor,gld.start_date AS date_range_start,gld.end_date AS date_range_end,gld.orig_coupon_cd AS credit_coupon_code,CS.SERVICE_NAME AS service_name,cs.client_service_id AS client_service_id,cs.coa_id AS service_coa_id,CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID AS client_plan_id, gld.comments AS line_description FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO       =PS.PLAN_NO  WHERE gld.invoice_no= 18112953  )a WHERE line_no=1"
	   for(int i=1;i<=counts;i++){
			 int t =i-1;
			 logi.logInfo("counting"+t)
 
			 row.put("acct_plan_line_items_row["+t+"]",md_ASSIGN_ACCT_PLAN_m_row(invoice_no,i))
	   }
	   logi.logInfo("returning main hash : "+row)
	   row = row.sort()
	   return row
	   
	   logi.logInfo("********md_UPDATE_ACCT_PLAN_m Ends**************")
 }
	 
	 public LinkedHashMap md_ASSIGN_ACCT_PLAN_m_row(String inv_no, int i){
							logi.logInfo("********md_UPDATE_ACCT_PLAN_m Start**************")
							ConnectDB db = new ConnectDB()
							LinkedHashMap <String,String> row = new LinkedHashMap<String,String>()
							String query = "SELECT a.plan_no,  a.client_plan_id,  a.plan_name,  a.line_no,  a.proration_factor,  a.date_range_start, a.date_range_end,a.description,a.line_amount,a.line_units,a.line_base_units,a.service_no,a.service_name, a.credit_coupon_code, a.rate_per_unit, a.client_service_id FROM (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,    gld.plan_no AS plan_no,    gld.service_no AS service_no,  gld.usage_rate AS rate_per_unit, gld.proration_factor AS proration_factor,TO_CHAR(gld.start_date, 'YYYY-MM-DD') AS date_range_start,TO_CHAR(gld.end_date, 'YYYY-MM-DD') AS date_range_end,gld.comments as description,gld.debit AS line_amount,gld.usage_units AS line_units,gld.base_plan_units AS line_base_units,gld.orig_coupon_cd AS credit_coupon_code,CS.SERVICE_NAME AS service_name,cs.client_service_id AS client_service_id,CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID AS client_plan_id, gld.comments AS line_description FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO =PS.PLAN_NO and cp.client_no = ps.client_no WHERE gld.invoice_no = "+inv_no+"  )a WHERE line_no="+i+""
							ResultSet rs1 = db.executePlaQuery(query)
							ResultSetMetaData md1 = rs1.getMetaData();
							int columns1 = md1.getColumnCount();
							while (rs1.next()){
					  
								  for(int s=1; s<=columns1; ++s){
										if(rs1.getObject(s).toString().equals("null")||rs1.getObject(s).toString()==null){
											  logi.logInfo("No value in "+rs1.getObject(s))
										}else{logi.logInfo("Value inserting in hash map")
											  row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
										logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s).toLowerCase())
					  
								  }
							}
						  
							logi.logInfo("returning the row hash : "+row)
					  
							return row.sort()
					  }
						
	 def md_REPLACE_ACCT_PLAN_m(String s){
		 logi.logInfo("********md_REPLACE_ACCT_PLAN_m Start**************")
		 VerificationMethods vm =new VerificationMethods()
		 //String invoice_no = vm.getValueFromResponse("update_acct_plan_m",ExPathRpcEnc.Invoice_No_Resp_Val)
		 //String invoice_no = md_getValFromSpecificnodeK_m("ns1:update_acct_plan_mResponse,1#k,13")
		 //String invoice_no = getValueFromResponse("assign_acct_plan_m",ExPathRpc.INVOICE_NO);
		 String invoice_no = md_getValFromNodeAndIndex_m("invoice_no,1")
		 /*HashMap hh=md_getSpecificnodeAsList_m("invoice_no")
		 ArrayList<String> invoice_no = new ArrayList<String>(hh.values());*/
		 logi.logInfo("Thrown Invoice No" + invoice_no)
		 LinkedHashMap<String, HashMap<String,String>> row = new LinkedHashMap<String, HashMap<String,String>>()
		 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		 logi.logInfo("Given Client no" + client_no)
		 ConnectDB db = new ConnectDB()
		 InputMethods im = new InputMethods()
		 String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
		 logi.logInfo("Given Account number" + acct_no)
		 logi.logInfo("Given Invoice No" + invoice_no)
		 int counts= Integer.parseInt(db.executeQueryP2("SELECT count(*) from ariacore.gl_detail where invoice_no="+invoice_no).toString())
	 
	   //  String q_count = "SELECT a.plan_no,  a.client_plan_id,  a.plan_name,  a.line_no,  a.proration_factor,  a.date_range_start, a.date_range_end,a.service_no,a.service_name, a.service_coa_id, a.credit_coupon_code, a.units, a.rate_per_unit, a.client_service_id FROM (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,    gld.plan_no                                  AS plan_no,    gld.service_no AS service_no,    gld.usage_units AS units,gld.usage_rate AS rate_per_unit, gld.proration_factor AS proration_factor,gld.start_date AS date_range_start,gld.end_date AS date_range_end,gld.orig_coupon_cd AS credit_coupon_code,CS.SERVICE_NAME AS service_name,cs.client_service_id AS client_service_id,cs.coa_id AS service_coa_id,CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID AS client_plan_id, gld.comments AS line_description FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO       =PS.PLAN_NO  WHERE gld.invoice_no= 18112953  )a WHERE line_no=1"
		 for(int i=1;i<=counts;i++){
			   int t =i-1;
			   logi.logInfo("counting"+t)
	 
			   row.put("acct_plan_line_items_row["+t+"]",md_ASSIGN_ACCT_PLAN_m_row(invoice_no,i))
		 }
		 logi.logInfo("returning main hash : "+row)
		 row = row.sort()
		 return row
		 
		 logi.logInfo("********md_REPLACE_ACCT_PLAN_m Ends**************")
	 }
	 

	 
	 public LinkedHashMap md_REPLACE_ACCT_PLAN_m_row(String inv_no, int i){
	   logi.logInfo("********md_UPDATE_ACCT_PLAN_m Start**************")
	   ConnectDB db = new ConnectDB()
	   LinkedHashMap <String,String> row = new LinkedHashMap<String,String>()
	   String query = "SELECT a.plan_no,  a.client_plan_id,  a.plan_name,  a.line_no,  a.proration_factor,  a.date_range_start, a.date_range_end,a.description,a.line_amount,a.line_units,a.line_base_units,a.service_no,a.service_name, a.credit_coupon_code, a.rate_per_unit, a.client_service_id FROM (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,    gld.plan_no AS plan_no,    gld.service_no AS service_no,  gld.usage_rate AS rate_per_unit, gld.proration_factor AS proration_factor,TO_CHAR(gld.start_date, 'YYYY-MM-DD') AS date_range_start,TO_CHAR(gld.end_date, 'YYYY-MM-DD') AS date_range_end,gld.comments as description,gld.debit AS line_amount,gld.usage_units AS line_units,gld.base_plan_units AS line_base_units,gld.orig_coupon_cd AS credit_coupon_code,CS.SERVICE_NAME AS service_name,cs.client_service_id AS client_service_id,CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID AS client_plan_id, gld.comments AS line_description FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO =PS.PLAN_NO  WHERE gld.invoice_no = "+inv_no+"  )a WHERE line_no="+i+""
	   ResultSet rs1 = db.executePlaQuery(query)
	   ResultSetMetaData md1 = rs1.getMetaData();
	   int columns1 = md1.getColumnCount();
	   while (rs1.next()){
	 
		   for(int s=1; s<=columns1; ++s){
			   if(rs1.getObject(s).toString().equals("null")||rs1.getObject(s).toString()==null){
				   logi.logInfo("No value in "+rs1.getObject(s))
			   }else{logi.logInfo("Value inserting in hash map")
				   row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
			   logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s).toLowerCase())
	 
		   }
	   }
	 
	   logi.logInfo("returning the row hash : "+row)
	 
	   return row.sort()
	 }
	 
	 def md_Adjust_Billing_Dates_m(String day)
	 {
		 ClientParamUtils cp = new ClientParamUtils()
		 def client_no = Constant.mycontext.expand('${Properties#Client_No}')
		 String date = cp.getPastAndFutureMonthandDayP2(client_no,day,"yyyy-MM-dd")
		 return date
	 }
	
	 public def md_GET_Proration_Factor_m(String s)
	 {
		 logi.logInfo("P VT - " +s)
		 InputMethods im = new InputMethods()
		 String plan_instance = im.md_GET_ACCT_OR_INSTANCE_NO_m('1,1m',null)
		 String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		 ConnectDB database = new ConnectDB();
		 
		 logi.logInfo("parent_plan_instance_no VT - " +plan_instance)
		 
		 def result = md_ProrationFactorCalculation_m(plan_instance)
		 logi.logInfo("ProrationFactor - " +result)
		 return result
		 
	 }
	 
	

	public String md_DB_Pay_type_for_invoice_m (String input)
	{
		ConnectDB db = new ConnectDB()
		String type= db.executeQueryP2("SELECT pay_method from ariacore.billing_info bi join ariacore.acct_billing_group abg on bi.acct_no=abg.acct_no where abg.BILLING_GROUP_NO=(select BILLING_GROUP_NO from ariacore.gl where invoice_no= "+im.md_get_invoice_no_m(input,null)+" )").toString();
		if(type!="null") return type
		type= db.executeQueryP2("SELECT pay_method from ariacore.billing_info bi join ariacore.acct_billing_group abg on bi.acct_no=abg.acct_no where abg.BILLING_GROUP_NO=(select BILLING_GROUP_NO from ariacore.gl_pending where invoice_no= "+im.md_get_invoice_no_m(input,null)+" )").toString();
		if(type!="null") return type
	}
	
	public String md_invoice_status_in_DB_m (String input)
	{
		ConnectDB db = new ConnectDB()
		String invoice_status=db.executeQueryP2("select count(VOIDING_EVENT_NO) from ariacore.ALL_INVOICES where invoice_no="+im.md_get_invoice_no_m(input,null)).toString();
		logi.logInfo  ""
		if(invoice_status!="0") return 'VOIDED INVOICE'
		invoice_status=db.executeQueryP2("select REAL_PEND_IND from ariacore.ALL_INVOICES where invoice_no="+im.md_get_invoice_no_m(input,null)).toString();
		logi.logInfo  ""
		if(invoice_status=="R") return 'REAL INVOICE'
		else if(invoice_status=="P") return 'PENDING INVOICE'
		else
		return 'NO SUCH INVOICE'
		
		
	}
	
	public String md_usage_exclude_flag_of_last_usage_m(String input)
	{
		ConnectDB db = new ConnectDB()
		
		String out= db.executeQueryP2("SELECT EXCLUDE_IND from ariacore.usage where REC_NO="+getValueFromResponse('record_usage_m',"//*/usage_rec_no[1]")).toString()
			return out.toString()
	}
	
	public def md_rec_usage_DB_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		String Query="SELECT MASTER_PLAN_INSTANCE_NO,USAGE_UNITS,BILLABLE_UNITS from ariacore.usage where REC_NO="+getValueFromResponse('record_usage_m',"//*/usage_rec_no[1]");
		
		ResultSet resultSet = db.executePlaQuery(Query)
		ResultSetMetaData md = resultSet.getMetaData()
		int columns = md.getColumnCount();
		while (resultSet.next())
		{
		 for(int u=1; u<=columns; ++u)
			{line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));}
		}
			line_item.sort();
			return line_item
		
	}
	
	public def md_rec_usage_static_m(String input)
	{
		
		LinkedHashMap line_item = new LinkedHashMap()
		line_item.put("master_plan_instance_no",getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/master_plan_instance_no[1]"))
		line_item.put("usage_units",getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/usage_units[1]"))
		line_item.put("billable_units",getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/billable_units[1]"))
			
			line_item.sort();
			return line_item
		
	}
	
	public def md_get_invoice_historyDB_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		if(acct_no=="NoVal")acct_no=db.executeQueryP2("select ACCT_NO from ariacore.acct where client_no="+Constant.client_no+"and CLIENT_ACCT_ID='"+getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/client_acct_id[1]")+"'").toString();
		String MPI=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/master_plan_instance_id[1]")
		if(MPI=="NoVal")MPI=db.executeQueryP2("select PLAN_INSTANCE_NO from ariacore.plan_instance where  client_no="+Constant.client_no+"and PLAN_INSTANCE_CDID='"+getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/client_master_plan_instance_id[1]")+"'").toString();
		String Query="SELECT CASE WHEN ai.rebill_invoice_no is NOT NULL THEN 'True' ELSE 'False' END AS rb_status,cp.PLAN_NAME as master_plan_name,pi.plan_no as master_plan_no,ai.INVOICE_NO,ai.ACCT_NO as outacct_no,ai.BILLING_GROUP_NO,abg.PROFILE_CDID as client_billing_group_id,ai.CURRENCY as currency_cd,TO_CHAR(ai.BILL_DATE, 'YYYY-MM-DD') as BILL_DATE,TO_CHAR(ai.PAID_DATE, 'YYYY-MM-DD') as PAID_DATE,ai.DEBIT as amount,ai.CREDIT,TO_CHAR(ai.FROM_DATE, 'YYYY-MM-DD') as recurring_bill_from,TO_CHAR(ai.TO_DATE, 'YYYY-MM-DD') as recurring_bill_thru,TO_CHAR(ai.USAGE_FROM_DATE, 'YYYY-MM-DD') as usage_bill_from,TO_CHAR(ai.USAGE_TO_DATE, 'YYYY-MM-DD') as usage_bill_thru,cp.CLIENT_PLAN_ID as client_master_plan_id,ai.INVOICE_TYPE_CODE as invoice_type_cd,ai.VOIDING_EVENT_NO from ARIACORE.ALL_INVOICES ai join ariacore.acct_billing_group abg on ai.BILLING_GROUP_NO=abg.BILLING_GROUP_NO join ariacore.plan_instance_master pim on pim.BILLING_GROUP_NO=ai.BILLING_GROUP_NO and pim.client_no=ai.client_no join ariacore.plan_instance pi on pi.PLAN_INSTANCE_NO=pim.PLAN_INSTANCE_NO and pi.client_no=pim.client_no join ariacore.client_plan cp on cp.PLAN_NO=pi.PLAN_NO and pi.client_no=cp.client_no join ariacore.plan_instance pi on pi.client_no=pim.client_no and pi.plan_instance_no=pim.plan_instance_no join ariacore.client_plan cp on cp.client_no=pi.client_no and cp.plan_no=pi.plan_no where ai.acct_no="+acct_no+" and pi.PLAN_INSTANCE_NO="+MPI+" and pi.client_no="+Constant.client_no
		ResultSet resultSet = db.executePlaQuery(Query)
		ResultSetMetaData md = resultSet.getMetaData()
		int columns = md.getColumnCount();
		int loops=1
		while (resultSet.next())
		{
		
			 for(int u=1; u<=columns; ++u)
			{
				if(md.getColumnName(u)=="VOIDING_EVENT_NO")
				{
					if(resultSet.getObject(u)!=null)
					line_item.put("is_voided_ind","1")
					else line_item.put("is_voided_ind","0")
				}else{
				if(resultSet.getObject(u)!=null)
					line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));}
			}
			
			line_item.sort();
			row_item.put("invoice_hist_row["+(loops-1)+"]",line_item.sort())
			line_item = new LinkedHashMap();
			loops+=1;
		}
		row_item.sort();
		return row_item
		
	}
	/**
	 * Used to calculate the proration factor using dates
	 * @param PlanInstanceNo
	 * @return Proration Factor
	 */
   def md_ProrationFactorCalculation_m(String PlanInstanceNo)
	{
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB database = new ConnectDB();
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
		String Query = "SELECT TRUNC(ARIACORE.ARIAVIRTUALTIME(" + clientNo + ")) from DUAL"
		String currentVirtualTime = database.executeQueryP2(Query);
		Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
		logi.logInfo("Current VT - " +currentVirtualTime)
		String LastBillThruDateQry = "SELECT TRUNC(LAST_BILL_THRU_DATE+1) FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO =" + PlanInstanceNo
		String LastBillThruDateStr = database.executeQueryP2(LastBillThruDateQry);
		logi.logInfo("Current VT - " +LastBillThruDateStr)
		if (LastBillThruDateStr==null)
		{
			logi.logInfo("eNTER1")
			 LastBillThruDateQry = "SELECT TRUNC(NEXT_BILL_DATE) FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO =" + PlanInstanceNo
			 LastBillThruDateStr = database.executeQueryP2(LastBillThruDateQry);
		}
		logi.logInfo("eNTER")
		Date LastBillThruDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(LastBillThruDateStr);
		logi.logInfo("Current VT - " +LastBillThruDate)
	   
		String BillingIntervalQry ="select rs.recur_billing_interval from ariacore.plan_instance pi join ariacore.client_plan cp on pi.plan_no = cp.plan_no and pi.client_no = cp.client_no Join ariacore.NEW_RATE_SCHEDULES rs ON pi.schedule_no  =rs.schedule_no and pi.client_no=rs.client_no where pi.plan_instance_no ="+PlanInstanceNo
		int BillingInterval = database.executeQueryP2(BillingIntervalQry).toInteger();
		String billdayquery ="select bill_day from ariacore.plan_instance_master where plan_instance_no = (select master_plan_instance_no from ariacore.plan_instance where plan_instance_no = "+PlanInstanceNo+")"
	   
		def Billday = database.executeQueryP2(billdayquery).toInteger();
		Date virtualstartdate = subractMonthsFromGivenDate1(LastBillThruDate,BillingInterval,Billday)
		def usedays = getDaysBetween(virtualstartdate,currentVTDate)
		//usedays =usedays
		logi.logInfo("usedays - " +usedays)
		int durationOfPlan = getDaysBetween(virtualstartdate,LastBillThruDate )
		logi.logInfo("durationOfPlan - " +durationOfPlan)
		logi.logInfo("Current VT - " +virtualstartdate)
		def temp = (usedays.toInteger()/durationOfPlan.toInteger())
		def proration = 1 - temp
		logi.logInfo("proration - " +proration)
		return proration
		
	}
	/**
	 * Used to verify the response of get_acct_plans_m API
	 * @param dateGiven, No of Months, Bill day
	 * @return PrevDate
	 */
	public Date subractMonthsFromGivenDate1(Date dateGiven, int noOfMonths,int billday)
	 {
		 //go back by billing_interval number of months to find the prev bill date
		 int last_day_prev_bill_month,my_bill_day
		 Calendar cal = Calendar.getInstance();
		 cal.setTime(dateGiven);
		 cal.add(Calendar.MONTH,noOfMonths*-1)
		 SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		 String dateMonthsAdded = format1.format(cal.getTime());
		 Date PrevDate = format1.parse(dateMonthsAdded)
		 //Date PrevDate1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(PrevDate);
		 logi.logInfo("PrevDate as  - " +PrevDate)
		 Calendar cal1 = Calendar.getInstance();
		 cal1.setTime(PrevDate);
		 int prev_bill_year = cal1.get(Calendar.YEAR)
		 logi.logInfo("prev_bill_year as  - " +prev_bill_year)
		 int prev_bill_month = cal1.get(Calendar.MONTH)
		 logi.logInfo("prev_bill_month as  - " +prev_bill_month)
		 int prev_bill_days = cal1.get(Calendar.DAY_OF_MONTH)
		 logi.logInfo("prev_bill_days as  - " +prev_bill_days)
		 logi.logInfo("billday as  - " +billday)
		 if(prev_bill_days < billday)
		 {
			 logi.logInfo("Entering into the main if block")
			 cal1.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
				last_day_prev_bill_month= cal1.get(Calendar.DAY_OF_MONTH);
				System.out.println("prev_bill_days as  - " +prev_bill_days);
				System.out.println("last_day_prev_bill_month as  - " +last_day_prev_bill_month);
					 System.out.println("last_day_prev_bill_month as  - " +last_day_prev_bill_month);
			 if(billday >= last_day_prev_bill_month )
			 {
				 logi.logInfo("Entering into the Child if block")
				 my_bill_day = last_day_prev_bill_month
				 logi.logInfo("my_bill_day as  - " +my_bill_day)
				 
			 }
			 else
			 {
				 logi.logInfo("Entering into the Child else block")
				 my_bill_day =  billday;
				 logi.logInfo("my_bill_day as  - " +my_bill_day)
			 }
		 }
		 else
		 {
			 logi.logInfo("Entering into the Child if block")
			 my_bill_day = billday
			 logi.logInfo("my_bill_day as  - " +my_bill_day)
			
		 }
		 cal1.add(Calendar.DAY_OF_MONTH,my_bill_day)
		 cal1.setTime(PrevDate);
		 logi.logInfo("PrevDate as  - " +PrevDate)
		 return PrevDate
	 }
	
	public def md_invoice_items_taxDB_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		HashMap hh=md_getSpecificnodeAsList_m("ns1:invoice_no")
		ArrayList<String> invoice_no = new ArrayList<String>(hh.values());
		Collections.sort(invoice_no)
		int loops=1
		for(int i=0;i<invoice_no.size();i++)
		{
		String Query="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL  WHERE invoice_no="+invoice_no.get(i)+" order by TAX_DETAIL_LINE";
		//String Query="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL  WHERE invoice_no="+invoice_no.get(i)+" order by DEBIT";
		ResultSet resultSet = db.executePlaQuery(Query)
		if((db.executeQueryP2("select count(*) from ("+Query+")").toString()).equals("0"))
		Query="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL_PENDING  WHERE invoice_no="+invoice_no.get(i)+" order by TAX_DETAIL_LINE";
		//Query="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL_PENDING  WHERE invoice_no="+invoice_no.get(i)+" order by DEBIT";
		resultSet = db.executePlaQuery(Query)
		ResultSetMetaData md = resultSet.getMetaData()
		int columns = md.getColumnCount();
		while (resultSet.next())
		{
			 for(int u=1; u<=columns; ++u)
			{				if(resultSet.getObject(u)!=null)
					line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
			}
			line_item.sort();
			row_item.put("tax_details_row["+(loops-1)+"]",line_item.sort())
			line_item = new LinkedHashMap();
			loops+=1;
		}
		}
		row_item.sort();
		return row_item
	}
	
	/**
	 * @return MPI
	 * @param no param
	 */
	def md_MPI_DB_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		HashMap hh=md_getSpecificnodeAsList_m("ns1:acct_no")
		ArrayList<String> invoice_no = new ArrayList<String>(hh.values());
		Collections.sort(invoice_no)
		int loops=1
		for(int i=0;i<invoice_no.size();i++)
		{
		String Query="SELECT MASTER_PLAN_INSTANCE_NO AS plan_instance_no,PLAN_INSTANCE_CDID AS client_plan_instance_id FROM ARIACORE.PLAN_INSTANCE WHERE ACCT_NO = "+invoice_no.get(i)+" AND PARENT_PLAN_INSTANCE_NO IS NULL";
		ResultSet resultSet = db.executePlaQuery(Query)
		ResultSetMetaData md = resultSet.getMetaData()
		int columns = md.getColumnCount();
		while (resultSet.next())
		{
			 for(int u=1; u<=columns; ++u)
			{
				if(resultSet.getObject(u)!=null)
					line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
			}
			logi.logInfo "for other values"
			String Query1="select PLAN_UNIT_INSTANCE_NO as out_plan_unit_inst_no,PLAN_UNIT_INSTANCE_CDID as out_client_plan_unit_inst_id from ariacore.PLAN_INST_UNIT_INSTANCES where PLAN_INSTANCE_NO = "+line_item.get("plan_instance_no")
			ResultSet resultSet1 =db.executePlaQuery(Query1)
			ResultSetMetaData md1 = resultSet1.getMetaData()
			int columns1 = md1.getColumnCount();
			 while (resultSet1.next())
		   {
			   for(int l=1; l<=columns1; l++)
			   {
				   if((resultSet1.getObject(l))!=null)
				   {
							boolean is_having=true
							int k=1;
							while (is_having)
							{
								if(k==1)
								{if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase())){is_having=false}}
								else {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase()+"["+k.toString()+"]")){is_having=false}}
								k+=1;
							}
							k-=1;
							logi.logInfo "the value of k "+k;
							logi.logInfo "putting in output "+resultSet1.getObject(l);
							 if(k>1){line_item.put((md1.getColumnName(l)).toLowerCase()+"["+k.toString()+"]",(resultSet1.getObject(l)));}
							 else{   line_item.put((md1.getColumnName(l)).toLowerCase(),(resultSet1.getObject(l)));}
				   }
			   }
		   }
			line_item.sort();
			logi.logInfo "the value of line_items "+line_item;
			row_item.put("master_plans_assigned_row["+(loops-1)+"]",line_item.sort())
			line_item = new LinkedHashMap();
			loops+=1;
		}
		}
		row_item.sort();
		logi.logInfo "the value to return " + row_item;
		return row_item
	}

	 /**
	  * To Get the Account or Plan instance surcharges count from DB
	  * @param paramval
	  * @return
	  */
	 def md_VERIFY_SURCHARGES_COUNT_m(String s){
		 logi.logInfo("********md_VERIFY_SURCHARGES_COUNT_m Start**************")
		 InputMethods im = new InputMethods()
		 LinkedHashMap<String, String> hm = new LinkedHashMap<String, String>()
		 ArrayList<String> list = new ArrayList<String>()
		 int i = 1
		 String acctNo = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
		 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		 ConnectDB db = new ConnectDB()
		 String querySurchargeNo = "SELECT surcharge_no FROM ARIACORE.acct_surcharge WHERE ACCT_NO = "+acctNo+" AND CLIENT_NO = "+client_no+""
		 ResultSet rs_SurchargeNo = db.executePlaQuery(querySurchargeNo)
		 while(rs_SurchargeNo.next())
		 {
				 int c =i-1;
				 logi.logInfo("counting"+c)
					 list[c]=rs_SurchargeNo.getObject(i)
					 logi.logInfo "Showing Surcharge number"+list[c]
					 // hm.put("all_surcharges_row["+c+"]",md_GET_ACCT_SURCHARGES_m_Surcharge_Details_row(acctNo,list[i].toString()))
						   i++
		 }
		 logi.logInfo("I value BEFORE"+i)
		 String querySurchargeNo1 ="SELECT SURCHARGE_NO FROM ARIACORE.PLAN_INSTANCE_SURCHARGE WHERE PLAN_INSTANCE_NO IN (SELECT PIM.PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_INSTANCE_MASTER PIM ON PI.PLAN_INSTANCE_NO = PIM.PLAN_INSTANCE_NO WHERE PI.ACCT_NO = "+acctNo+" AND PI.CLIENT_NO = "+client_no+")"
		 ResultSet rs_SurchargeNo1 = db.executePlaQuery(querySurchargeNo1)
		 while(rs_SurchargeNo1.next())
		 {
				 int c =i-1;
				 logi.logInfo("counting1"+c)
					 list[c]=rs_SurchargeNo1.getObject(i)
					 logi.logInfo "Showing Surcharge number1"+list[c]
					 // hm.put("all_surcharges_row["+c+"]",md_GET_ACCT_SURCHARGES_m_Surcharge_Details_row(acctNo,list[i].toString()))
						   i++
		 }
		 logi.logInfo("I value After"+list.size())
		 return list.size()
	 }
	 
	 
	 
	 LinkedHashMap md_acct_stmt_details_m(String acct_hierarchy)
	 {
		 LinkedHashMap h1= new LinkedHashMap()
		 int count=0
		 //LinkedList l1= new LinkedList()
		 logi.logInfo("into md_acct_stmt_details_m")
		 ConnectDB dbb = new ConnectDB()
		 InputMethods im=new InputMethods()
		 String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		 String mpi_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1,1m",null)
		 logi.logInfo("acct_no"+acct_no)
		 
		 String acct_stmt_query="SELECT STATEMENT_NO,TO_CHAR(CREATE_DATE,'YYYY-MM-DD') AS CREATE_DATE,CURRENCY_CD,TO_CHAR(DUE_DATE,'YYYY-MM-DD') AS DUE_DATE,TO_CHAR(DUE_DATE,'YYYY-MM-DD') AS DUE_DATE_PLUS_GRACE_PERIOD,STATEMENT_NO AS SEQ_STATEMENT_ID FROM  ariacore.acct_statement where acct_no="+acct_no+" order by statement_no"
	 ResultSet rs = dbb.executePlaQuery(acct_stmt_query)
	 ResultSetMetaData md = rs.getMetaData()
	 int columns = md.getColumnCount()
	 // Putting values into HashMap
	 while (rs.next())
	 {
		 LinkedHashMap temp= new LinkedHashMap()
		 LinkedList l1= new LinkedList()
		 for(int i=1; i<=columns; ++i)
		 {
			 if(!(rs.getObject(i).toString().equalsIgnoreCase("null")))
			 {
				 l1.add(md.getColumnName(i).toLowerCase())
	 
				 int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	 
				 logi.logInfo("frewww of :+"+md.getColumnName(i).toLowerCase()+" is :"+freq)
	 
				 if(freq>1)
				 {
	 
					 temp.put(md.getColumnName(i).toLowerCase()+"["+ freq +"]",rs.getObject(i))
					 //h1.put(md.getColumnName(i).toLowerCase()+"["+ freq +"]",rs.getObject(i))
				 }
	 
				 else
				 temp.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
				 //h1.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
	 
			 }
			 logi.logInfo(" hashmap of STATEMENT_HISTORY :"+temp)
	 
		 }
		// temp.put("master_plan_instance_no",mpi_no)
		 //temp.put("client_master_plan_instance_id",mpi_no)
		 temp=temp.sort()
		 h1.put("statements_history_row["+count+"]",temp)
	 ++count
		 }
	 logi.logInfo("FINAL hashmap of STATEMENT_HISTORY :"+h1)
	 
	 h1=h1.sort()
	 return h1
	 }
	 public String md_getValFromNodeAndIndex_fornull_m(String node)
	 {
		 HashMap<String, String> hm = new HashMap<String, String>();
		 int index=Integer.parseInt(node.split(",")[1])
		 node=node.split(",")[0];
		 logi.logInfo "node value"+node
		 hm=md_getSpecificnodeAsListfornull_m(node)
		 LinkedList<String> temp2 = new LinkedList<String>();
		 temp2.addAll(hm.values())
		 return temp2.get(index-1).toString()
	 }

public   HashMap md_getSpecificnodeAsListfornull_m(String name1) throws Exception{
						logi.logInfo "into md_getSpecificnodeAsList_m"
						logi.logInfo "param given "+name1+Sindex
						String xmlRecords = library.Constants.Constant.RESPONSE
						   LinkedList<String> temp2 = new LinkedList<String>();
						   HashMap<String, String> hm = new HashMap<String, String>();
						   String temp = null;
						   DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
						   InputSource is = new InputSource();
						   is.setCharacterStream(new StringReader(xmlRecords));
						   Document doc = db.parse(is);
						   NodeList nodes = doc.getElementsByTagName(pnode);
							  Element element = (Element) nodes.item(Sindex);
							  
						   NodeList name = element.getElementsByTagName(name1);
						   int index=name.getLength();
						   for(int k=index;k>0;k--)
						   { Element line = (Element) name.item(k-1);
						   temp=getCharacterDataFromElement(line);
						   
							  if(!temp.equals(null)||temp!=null)
							  {
									temp2.add(temp);
									if(k>1)
									hm.put(name1.replace("ns1:","")+"["+(k)+"]",temp);
									else
										  hm.put(name1.replace("ns1:",""),temp);
							  }
						   }
							  return hm.sort();
			}

/**
 * To Verify the invoice line items details from the assign_acct_plan_m,replace_acct_plan_m,update_acct_plan_m,cancel_acct_plan_m APIs responses
 * @param paramval
 * @return
 */
def md_Acct_Plan_Line_Items_m(String s){
	 logi.logInfo("********md_Acct_Plan_Line_Items_m Start**************")
	 String invoice_no = md_getValFromNodeAndIndex_fornull_m("invoice_no,1")
	 logi.logInfo("Printing Invoice No"+invoice_no)
	 LinkedHashMap<String,LinkedHashMap> acct_plan_line_items_row = new LinkedHashMap<String,LinkedHashMap>();
	 LinkedHashMap<String,LinkedHashMap> invoice = new LinkedHashMap<String,LinkedHashMap>();
	 LinkedHashMap line_item = new LinkedHashMap();
	 int loops=1;
		 ConnectDB db = new ConnectDB()
		 int counts= Integer.parseInt(db.executeQueryP2("SELECT count(*) from ariacore.gl_detail where invoice_no="+invoice_no).toString())
		 for(int loop2=1;loop2<=counts;loop2++)
		 {
			 String  DB_ACCT_PLAN_LINE_ITEMS_ROW_m=""
			 ResultSet resultSet =null;
			 ResultSetMetaData md ;

			 String check_surcharge=db.executeQueryP2("select surcharge_rate_seq_no from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
			  String check_coupon=db.executeQueryP2("select orig_coupon_cd from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
			  String Check_NSO=db.executeQueryP2("select orig_client_sku from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
			  String Check_Credit=db.executeQueryP2("select ORIG_CREDIT_ID from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
			  logi.logInfo ""+check_surcharge
			  logi.logInfo ""+check_coupon
			  if((check_surcharge.contains("null")&&check_coupon.contains("null")&&Check_NSO.contains("null")&&Check_Credit.contains("null"))||(check_surcharge==null&&check_coupon==null&&Check_NSO==null)&&Check_Credit==null)
			  {
				  DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.plan_no,  a.client_plan_id,  a.plan_name,  a.line_no,  a.proration_factor,  a.date_range_start, a.date_range_end,a.description,a.line_amount,a.line_units,a.line_base_units,a.service_no,a.service_name, a.credit_coupon_code, a.rate_per_unit, a.client_service_id FROM (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,    gld.plan_no AS plan_no,    gld.service_no AS service_no,  gld.usage_rate AS rate_per_unit, gld.proration_factor AS proration_factor,TO_CHAR(gld.start_date, 'YYYY-MM-DD') AS date_range_start,TO_CHAR(gld.end_date, 'YYYY-MM-DD') AS date_range_end,gld.comments as description,gld.debit AS line_amount,gld.usage_units AS line_units,gld.base_plan_units AS line_base_units,gld.orig_coupon_cd AS credit_coupon_code,CS.SERVICE_NAME AS service_name,cs.client_service_id AS client_service_id,CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID AS client_plan_id, gld.comments AS line_description FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO =PS.PLAN_NO AND CP.CLIENT_NO=PS.CLIENT_NO WHERE gld.invoice_no = "+invoice_no+") a where line_no="+loop2
				   if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
				  { resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
					  md = resultSet.getMetaData()}
			  }
			  if(resultSet==null)
			  {
				  check_surcharge=db.executeQueryP2("select surcharge_rate_seq_no from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
				  logi.logInfo "Into the check_surcharge part"
				  if(!check_surcharge.contains("null"))
				  {
					  logi.logInfo "Into the Surcharge items part"
					  DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.line_no,a.service_no,a.client_service_id,a.service_name,a.rate_per_unit,a.date_range_start,a.date_range_end,a.description,a.line_amount,a.line_units from(SELECT gld.seq_num as line_no,GLD.service_no, GLD.debit  AS line_amount, GLD.comments AS description, GLD.usage_rate AS rate_per_unit,GLD.usage_units AS line_units, TO_CHAR(GLD.start_date, 'YYYY-MM-DD') AS date_range_start, TO_CHAR(GLD.end_date, 'YYYY-MM-DD') AS date_range_end, SER.client_service_id AS client_service_id,ALS.service_name AS service_name FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.SERVICES SER ON SER.SERVICE_NO   = gld.SERVICE_NO JOIN ARIACORE.ALL_SERVICE ALS ON SER.SERVICE_NO  = ALS.SERVICE_NO WHERE invoice_no = "+invoice_no+")a where line_no="+loop2
					  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
					  {resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
						  md = resultSet.getMetaData()}
				  }
			  }
			  if(resultSet==null)
			  {
				  Check_NSO=db.executeQueryP2("select orig_client_sku from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
				  if(!Check_NSO.contains("null"))
				  {
					  DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.line_no,a.service_no,a.client_service_id,a.service_name,a.rate_per_unit,a.date_range_start,a.date_range_end,a.description,a.line_amount,a.line_units from (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,gld.service_no AS service_no,cs.client_service_id AS client_service_id,CS.SERVICE_NAME AS service_name,gld.usage_rate AS rate_per_unit,TO_CHAR(gld.start_date, 'YYYY-MM-DD') AS date_range_start,TO_CHAR(gld.end_date, 'YYYY-MM-DD') AS date_range_end,gld.comments as description,gld.debit AS line_amount,gld.usage_units AS line_units FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO =PS.PLAN_NO and CP.client_no =PS.client_no  WHERE gld.invoice_no = "+invoice_no+")a where line_no="+loop2
					  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
					  { resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
						  md = resultSet.getMetaData()}
				  }
			  }
			  
			  if(resultSet==null)
			  {
				  Check_Credit=db.executeQueryP2("select ORIG_CREDIT_ID from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
				  if(!Check_Credit.contains("null"))
				  {
					   DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.line_no,a.plan_no,  a.client_plan_id,  a.plan_name, a.service_no, a.credit_coupon_code, a.line_base_units, a.proration_factor, a.date_range_start,a.date_range_end,a.client_service_id,a.service_name,a.description,a.line_amount,a.line_units FROM(SELECT gld.seq_num     AS line_no, gld.plan_no AS plan_no, CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID AS client_plan_id, gld.service_no        AS service_no,SER.client_service_id AS client_service_id,als.SERVICE_NAME      AS service_name,gld.orig_coupon_cd    AS credit_coupon_code,gld.base_plan_units   AS line_base_units,gld.proration_factor, gld.comments AS description,gld.debit AS line_amount, TO_CHAR(gld.start_date, 'YYYY-MM-DD') AS date_range_start,TO_CHAR(gld.end_date, 'YYYY-MM-DD') AS date_range_end, gld.usage_units  AS line_units FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.SERVICES SER ON SER.SERVICE_NO = gld.SERVICE_NO AND gld.client_no = SER.custom_to_client_no JOIN ARIACORE.ALL_SERVICE ALS ON SER.SERVICE_NO = ALS.SERVICE_NO AND SER.custom_to_client_no = ALS.client_no JOIN ARIACORE.PLAN_SERVICES PS ON ALS.CLIENT_NO = PS.CLIENT_NO AND PS.SERVICE_NO = ALS.SERVICE_NO JOIN ARIACORE.CLIENT_PLAN CP ON CP.PLAN_NO    = PS.PLAN_NO AND CP.CLIENT_NO = gld.CLIENT_NO AND CP.plan_no = gld.plan_no AND CP.client_no = PS.client_no  WHERE invoice_no ="+invoice_no+")a where line_no="+loop2
					  //DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.line_no, a.service_no,a.credit_coupon_code, a.line_base_units,a.proration_factor,a.date_range_start, a.client_service_id,  a.service_name,  a.description,  a.line_amount,  a.line_units FROM  (SELECT gld.seq_num as line_no,    gld.service_no  AS service_no,    SER.client_service_id AS client_service_id,als.SERVICE_NAME AS service_name,gld.orig_coupon_cd as credit_coupon_code,gld.base_plan_units AS line_base_units,gld.proration_factor,gld.comments AS description, gld.debit AS line_amount,TO_CHAR(gld.start_date, 'YYYY-MM-DD') AS date_range_start, gld.usage_units AS line_units FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.SERVICES SER ON SER.SERVICE_NO   = gld.SERVICE_NO JOIN ARIACORE.ALL_SERVICE ALS ON SER.SERVICE_NO = ALS.SERVICE_NO WHERE invoice_no =  "+invoice_no+")a where line_no="+loop2
					  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
					  { resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
						  md = resultSet.getMetaData()}
				  }
			  }
			  if(resultSet==null)
			  {
				  check_coupon=db.executeQueryP2("select orig_coupon_cd from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
				  if(!check_coupon.contains("null"))
				  {
					  logi.logInfo "Into the Coupon line items part"
					  DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT distinct a.line_no,a.service_no,a.client_service_id,a.service_name,a.rate_per_unit, a.date_range_start,a.date_range_end,a.description,a.line_amount,a.line_units,a.credit_coupon_code, a.plan_no, a.plan_name, a.client_plan_id FROM(SELECT gld.seq_num AS line_no, GLD.service_no, gld.plan_no AS plan_no, GLD.debit AS line_amount,GLD.comments AS description,    GLD.usage_rate  AS rate_per_unit,  GLD.usage_units AS line_units,  TO_CHAR(GLD.start_date, 'YYYY-MM-DD') AS date_range_start, TO_CHAR(GLD.end_date, 'YYYY-MM-DD')   AS date_range_end, SER.client_service_id AS client_service_id,ALS.service_name  AS service_name,GLD.orig_coupon_cd  AS credit_coupon_code,CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID AS client_plan_id FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.SERVICES SER ON SER.SERVICE_NO = gld.SERVICE_NO JOIN ARIACORE.ALL_SERVICE ALS ON SER.SERVICE_NO = ALS.SERVICE_NO JOIN ARIACORE.CLIENT_PLAN ALS  ON SER.SERVICE_NO = ALS.SERVICE_NO  JOIN ARIACORE.CLIENT_PLAN CP ON CP.PLAN_NO        =GLD.PLAN_NO  AND CP.CLIENT_NO =GLD.CLIENT_NO  WHERE invoice_no = "+invoice_no+")a where line_no="+loop2
					  if(Integer.parseInt(
						  db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
					  {
						  resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
						  md = resultSet.getMetaData()}
				  }
			  }

			  if(resultSet==null)
			  {
				  logi.logInfo "into the dummy datebase return part"
				  DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.line_no,a.service_no,a.client_service_id,a.service_name,a.rate_per_unit,a.date_range_start,a.date_range_end,a.description,a.line_amount,a.line_units from (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,gld.service_no AS service_no,cs.client_service_id AS client_service_id,CS.SERVICE_NAME AS service_name,gld.usage_rate AS rate_per_unit,TO_CHAR(gld.start_date, 'YYYY-MM-DD') AS date_range_start,TO_CHAR(gld.end_date, 'YYYY-MM-DD') AS date_range_end,gld.comments as description,gld.debit AS line_amount,gld.usage_units AS line_units FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO =PS.PLAN_NO and CP.client_no =PS.client_no  WHERE gld.invoice_no = "+invoice_no+")a where line_no="+loop2
				  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
				  {resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
					  md = resultSet.getMetaData()}
			  }

		 
			  int columns = md.getColumnCount();
			  while (resultSet.next())
			  {

				  for(int u=1; u<=columns; ++u)
				  {
					  if(resultSet.getObject(u).toString().equals("null")||resultSet.getObject(u).toString()==null){
						  logi.logInfo("No value in "+resultSet.getObject(u))
					  }else{
					  logi.logInfo("Value inserting in hash map")
					  line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
					  }
				  }

			  }

			  line_item.sort();
			  acct_plan_line_items_row.put("acct_plan_line_items_row["+(loops-1)+"]",line_item.sort())
			  line_item = new LinkedHashMap();
			  loops++;
		  }
		  if(counts==0){
			  counts= Integer.parseInt(db.executeQueryP2("SELECT count(*) from ariacore.GL_PENDING_DETAIL where invoice_no="+invoice_no).toString())

			  for(int loop2=1;loop2<=counts;loop2++)
			  {
				  String  DB_ACCT_PLAN_LINE_ITEMS_ROW_m=""
				  ResultSet resultSet =null;
				  ResultSetMetaData md ;

				  String check_surcharge=db.executeQueryP2("select surcharge_rate_seq_no from ariacore.GL_PENDING_DETAIL where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
				  String check_coupon=db.executeQueryP2("select orig_coupon_cd from ariacore.GL_PENDING_DETAIL where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
				  String Check_NSO=db.executeQueryP2("select orig_client_sku from ariacore.GL_PENDING_DETAIL where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
				  String Check_Credit=db.executeQueryP2("select orig_credit_id from ariacore.GL_PENDING_DETAIL where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
				  logi.logInfo ""+check_surcharge
				  logi.logInfo ""+check_coupon
				  if((check_surcharge.contains("null")&&check_coupon.contains("null")&&Check_NSO.contains("null"))||(check_surcharge==null&&check_coupon==null&&Check_NSO==null))
				  {
					  DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.plan_no,  a.client_plan_id,  a.plan_name,  a.line_no,  a.proration_factor,  a.date_range_start, a.date_range_end,a.description,a.line_amount,a.line_units,a.line_base_units,a.service_no,a.service_name, a.credit_coupon_code, a.rate_per_unit, a.client_service_id FROM (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,    gld.plan_no AS plan_no,    gld.service_no AS service_no,  gld.usage_rate AS rate_per_unit, gld.proration_factor AS proration_factor,TO_CHAR(gld.start_date, 'YYYY-MM-DD') AS date_range_start,TO_CHAR(gld.end_date, 'YYYY-MM-DD') AS date_range_end,gld.comments as description,gld.debit AS line_amount,gld.usage_units AS line_units,gld.base_plan_units AS line_base_units,gld.orig_coupon_cd AS credit_coupon_code,CS.SERVICE_NAME AS service_name,cs.client_service_id AS client_service_id,CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID AS client_plan_id, gld.comments AS line_description FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO =PS.PLAN_NO  WHERE gld.invoice_no = "+invoice_no+") a where line_no="+loop2
					  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
					  {resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
						  md = resultSet.getMetaData()}
				  }
				  if(resultSet==null)
				  {
					  check_surcharge=db.executeQueryP2("select surcharge_rate_seq_no from ariacore.GL_PENDING_DETAIL where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
					  if(!check_surcharge.contains("null"))
					  {
						  DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.line_no,a.service_no,a.client_service_id,a.service_name,a.rate_per_unit,a.date_range_start,a.date_range_end,a.description,a.line_amount,a.line_units from(SELECT gld.seq_num as line_no,GLD.service_no, GLD.debit  AS line_amount, GLD.comments AS description, GLD.usage_rate AS rate_per_unit,GLD.usage_units AS line_units, TO_CHAR(GLD.start_date, 'YYYY-MM-DD') AS date_range_start, TO_CHAR(GLD.end_date, 'YYYY-MM-DD') AS date_range_end, SER.client_service_id AS client_service_id,ALS.service_name AS service_name FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.SERVICES SER ON SER.SERVICE_NO   = gld.SERVICE_NO JOIN ARIACORE.ALL_SERVICE ALS ON SER.SERVICE_NO  = ALS.SERVICE_NO WHERE invoice_no = "+invoice_no+")a where line_no="+loop2
						  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
						  {  resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
							  md = resultSet.getMetaData()}
					  }
				  }
				  if(resultSet==null)
				  {
					  check_coupon=db.executeQueryP2("select orig_coupon_cd from ariacore.GL_PENDING_DETAIL where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
					  if(!check_coupon.contains("null"))
					  {
						  DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.line_no,a.service_no,a.client_service_id,a.service_name,a.rate_per_unit,a.date_range_start,a.date_range_end,a.description,a.line_amount,a.line_units from (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,gld.service_no AS service_no,cs.client_service_id AS client_service_id,CS.SERVICE_NAME AS service_name,gld.usage_rate AS rate_per_unit,TO_CHAR(gld.start_date, 'YYYY-MM-DD') AS date_range_start,TO_CHAR(gld.end_date, 'YYYY-MM-DD') AS date_range_end,gld.comments as description,gld.debit AS line_amount,gld.usage_units AS line_units FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO =PS.PLAN_NO and CP.client_no =PS.client_no  WHERE gld.invoice_no = "+invoice_no+")a where line_no="+loop2
						  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
						  { resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
							  md = resultSet.getMetaData()}
					  }
				  }
				  if(resultSet==null)
				  {
					  check_coupon=db.executeQueryP2("select orig_coupon_cd from ariacore.GL_PENDING_DETAIL where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
					  if(!check_coupon.contains("null"))
					  {
						  DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.line_no,a.service_no,a.client_service_id,a.service_name,a.rate_per_unit,a.date_range_start,a.date_range_end,a.description,a.line_amount,a.line_units from (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,gld.service_no AS service_no,cs.client_service_id AS client_service_id,CS.SERVICE_NAME AS service_name,gld.usage_rate AS rate_per_unit,TO_CHAR(gld.start_date, 'YYYY-MM-DD') AS date_range_start,TO_CHAR(gld.end_date, 'YYYY-MM-DD') AS date_range_end,gld.comments as description,gld.debit AS line_amount,gld.usage_units AS line_units FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO =PS.PLAN_NO and CP.client_no =PS.client_no  WHERE gld.invoice_no = "+invoice_no+")a where line_no="+loop2
						  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
						  { resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
							  md = resultSet.getMetaData()}
					  }
				  }
				  if(resultSet==null)
				  {
				  Check_Credit=db.executeQueryP2("select orig_credit_id from ariacore.GL_PENDING_DETAIL where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
				  if(!Check_Credit.contains("null"))
				  {
					  DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.line_no,a.service_no,a.client_service_id,a.service_name,a.rate_per_unit,a.description,a.line_amount,a.line_units from (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,gld.service_no AS service_no,cs.client_service_id AS client_service_id,CS.SERVICE_NAME AS service_name,gld.comments as description,gld.debit AS line_amount,gld.usage_units AS line_units FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO =PS.PLAN_NO and CP.client_no =PS.client_no  WHERE gld.invoice_no = "+invoice_no+")a where line_no="+loop2
					  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
					  { resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
						  md = resultSet.getMetaData()}
				  }
				  }
				  if(resultSet==null)
				  {
					  logi.logInfo "into the dummy datebase return part"

					  DB_ACCT_PLAN_LINE_ITEMS_ROW_m=" SELECT a.line_no,a.service_no,a.client_service_id,a.service_name,a.rate_per_unit,a.date_range_start,a.date_range_end,a.description,a.line_amount,a.line_units from (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,gld.service_no AS service_no,cs.client_service_id AS client_service_id,CS.SERVICE_NAME AS service_name,gld.usage_rate AS rate_per_unit,TO_CHAR(gld.start_date, 'YYYY-MM-DD') AS date_range_start,TO_CHAR(gld.end_date, 'YYYY-MM-DD') AS date_range_end,gld.comments as description,gld.debit AS line_amount,gld.usage_units AS line_units FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO =PS.PLAN_NO and CP.client_no =PS.client_no  WHERE gld.invoice_no = "+invoice_no+")a where line_no="+loop2
					  if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
					  {resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
						  md = resultSet.getMetaData()}
				  }

				  
			  int columns = md.getColumnCount();
				  while (resultSet.next())
				  {

					  for(int u=1; u<=columns; ++u)
					  {
						  if(resultSet.getObject(u).toString().equals("null")||resultSet.getObject(u).toString()==null){
							  logi.logInfo("No value in "+resultSet.getObject(u))
						  }else{logi.logInfo("Value inserting in hash map")
						  line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
						  }
					  }

				  }

				  line_item.sort();
				  acct_plan_line_items_row.put("acct_plan_line_items_row["+(loops-1)+"]",line_item.sort())
				  line_item = new LinkedHashMap();
				  loops++;
			  }
			  // invoice.put("invoice_no["+invoice_no+"]",invoice_info_row.sort())
			  // invoice_info_row = new LinkedHashMap<String,LinkedHashMap>();
		  }
	  acct_plan_line_items_row=acct_plan_line_items_row.sort()
	  return acct_plan_line_items_row


	  logi.logInfo("********md_Acct_Plan_Line_Items_m Ends**************")
	  }
	

String  md_order_with_plans_plan_no_m(String acct_hierarchy1)
{
	 logi.logInfo(" md_order_with_plans_plan_no_m-----------------------")
	 ConnectDB dbb = new ConnectDB()

	 InputMethods im=new InputMethods()
	 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	 logi.logInfo("acct_hierarchy1:::"+acct_hierarchy1)
	 String service_no=acct_hierarchy1.split("#")[0]
	 String acct_hierarchy=acct_hierarchy1.split("#")[1]
	 
	 String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
	 //String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
	 logi.logInfo("acct_no"+acct_no)
	 String invoice_no_query="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO="+acct_no
	 String invoice_no=dbb.executeQueryP2(invoice_no_query)
	 
	 String plan_name_count_query="SELECT CP.PLAN_NAME from ARIACORE.GL_DETAIL GL JOIN ARIACORE.CLIENT_PLAN CP on CP.PLAN_NO = GL. PLAN_NO WHERE INVOICE_NO ="+invoice_no+" AND CP.CLIENT_NO="+client_no+"AND GL.SERVICE_NO="+service_no
		 logi.logInfo("QUERYYYYYYYYYYY "+plan_name_count_query)
		String plan_name_count=dbb.executeQueryP2(plan_name_count_query)
		
	  logi.logInfo("PLANNNN NAME "+plan_name_count)
	  
	  return plan_name_count
		 }

public  LinkedHashMap md_queued_acct_plan_m(String TESTCASEID)
{
	 logi.logInfo("md_queued_acct_plan_m")
	 ConnectDB db = new ConnectDB()
	 int j=0
	 LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
	 LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
	 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	 def acct_no = getValueFromResponse('create_acct_complete_m',ExPathRpc.CREATE_ACCT_COMPLETE_M_ACCTNO1)
	 logi.logInfo("acct_no" +acct_no )
	 String query ="SELECT p1.plan_name original_plan,p2.plan_name new_plan,p1.plan_no as original_plan_no,p2.plan_no as new_plan_no,TO_CHAR(pi.update_date,'YYYY-MM-DD') as change_date,pi.queued_schedule_no as new_rate_schedule_no,pi.create_api_receipt_id as client_receipt_id,pi.QUEUED_UNITS as  new_plan_units, DECODE (p1.supp_plan_ind,1, 'S',0,'M') as new_plan_type,p1.client_plan_id client_original_plan_id,p2.client_plan_id client_new_plan_id,pi.SCHEDULE_NO as current_rate_schedule_no,prs1.CLIENT_RATE_SCHEDULE_ID as client_current_rate_id,prs2.CLIENT_RATE_SCHEDULE_ID as client_new_rate_schedule_id FROM ariacore.plan_instance pi LEFT JOIN ariacore.client_plan p1 ON pi.plan_no = p1.plan_no AND pi.client_no = p1.client_no LEFT JOIN ariacore.client_plan p2 ON pi.queued_plan_no = p2.plan_no AND pi.client_no = p2.client_no  LEFT OUTER JOIN ariacore.plan_rate_schedule prs1 on pi.plan_no = prs1.plan_no and pi.client_no=prs1.client_no LEFT OUTER JOIN ariacore.plan_rate_schedule prs2 on pi.QUEUED_PLAN_NO = prs2.plan_no and pi.client_no=prs2.client_no  WHERE(pi.queued_plan_no IS NOT NULL OR pi.queued_units IS NOT NULL OR pi.queued_schedule_no IS NOT NULL OR (TRUNC(pi.provision_date) >= TRUNC (ariacore.AriaVirtualTime ("+client_no+")) AND pi.status_cd = 32)) AND pi.client_no = "+client_no+" AND pi.acct_no = "+acct_no
	 ResultSet rs4=db.executePlaQuery(query)
	 ResultSetMetaData md4 = rs4.getMetaData();
	 int columns4 = md4.getColumnCount();
	 while (rs4.next())
	 {
		 for(int l=1; l<=columns4; l++)
		 {
			 if((rs4.getObject(l))!=null)
			 {
				 
					 row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
				 
			 
			 }
			 if((rs4.getObject('client_current_rate_id'))!=null)
			 {
			  row.put("client_current_rate_schedule_id",rs4.getObject('client_current_rate_id'))
			   row.remove("client_current_rate_id")
			}
			  
		 }
		 outer.put("queued_plan_instance_details_row["+j+"]",row.sort())
		 row = new LinkedHashMap<String, String>();
		 j++
	 
	 }
	 return outer.sort()
}

public String md_get_billing_date_m(String str){
	 InputMethods im = new InputMethods()
	 String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null)
	 String query = "SELECT TO_CHAR(NEXT_BILL_DATE,'YYYY-MM-DD') AS BILLING_DATE FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO = "+plan_instance_no
	 ConnectDB db = new ConnectDB()
	 String billing_date = db.executeQueryP2(query).toString()
	 
	 return billing_date
}

public String md_get_last_recur_bill_date_m(String str){
	 InputMethods im = new InputMethods()
	 String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null)
	 String query = "SELECT TO_CHAR(LAST_BILL_DATE,'YYYY-MM-DD') AS BILLING_DATE FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO = "+plan_instance_no
	 ConnectDB db = new ConnectDB()
	 String billing_date = db.executeQueryP2(query).toString()
	 
	 return billing_date
}

public def md_verify_bill_thru_date_by_VT_m(String str){
	 ConnectDB db = new ConnectDB()
	 logi.logInfo "into the md_verify_bill_thru_date_by_VT_m"
	 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	 int i = Integer.parseInt(str)
	 logi.logInfo "before query"
	 String Query = "SELECT TO_CHAR(ARIACORE.ARIAVIRTUALTIME(" + client_no + "),'yyyy-MM-dd')AS CURRENT_DATE FROM DUAL";
	 String st = db.executeQueryP2(Query)
	 logi.logInfo "Stratergy" + st
	 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH)
	 Date current_Date=dateFormat.parse(st);
	 logi.logInfo("Current date "+current_Date)
	 Calendar now = Calendar.getInstance()
	 now.setTime(current_Date);
	 logi.logInfo("GOT TIME "+now.getTime())
	 now.add(Calendar.MONTH,i)
	 logi.logInfo("GOT TIME 222 "+now.getTime())
	 /*Date Pastdate = now.getTime();
	 logi.logInfo("GOT TIME111 "+Pastdate)
	 def startDate= dateFormat.format(Pastdate);*/
	 
	 //logi.logInfo "date has been added "+calendar
	 //String billing_date = now.toString()
	 logi.logInfo "date converted to string "+now.getTime()
	 String finaldate = dateFormat.format(now.getTime())
	 
	 return finaldate
}

/**
 * To get the invoice line items details for the updated plan unit instances using update_acct_plan_unit_instance_m API
 * @param paramval
 * @return
 */
def md_Unit_Inst_Invoice_Line_items_m(String s){
	 logi.logInfo("********md_Unit_Inst_Invoice_Line_items_m Start**************")
	 String invoice_no = md_getValFromNodeAndIndex_fornull_m("invoice_no,1")
	 logi.logInfo("Printing Invoice No"+invoice_no)
	 LinkedHashMap<String,LinkedHashMap> acct_plan_line_items_row = new LinkedHashMap<String,LinkedHashMap>();
	 LinkedHashMap<String,LinkedHashMap> invoice = new LinkedHashMap<String,LinkedHashMap>();
	 LinkedHashMap line_item = new LinkedHashMap();
	 int loops=1;
		 ConnectDB db = new ConnectDB()
		 int counts= Integer.parseInt(db.executeQueryP2("SELECT count(*) from ariacore.gl_detail where invoice_no="+invoice_no).toString())
		 for(int loop2=1;loop2<=counts;loop2++)
		 {
			 String  DB_ACCT_PLAN_LINE_ITEMS_ROW_m=""
			 ResultSet resultSet =null;
			 ResultSetMetaData md ;

			 String Check_Credit=db.executeQueryP2("select ORIG_CREDIT_ID from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
			  logi.logInfo "Check Credit value"+Check_Credit
			 
			  if((Check_Credit.contains("null"))||(Check_Credit==null))
			  {
				  
					logi.logInfo "into the dummy datebase return part"
					DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.line_no,a.plan_no,a.plan_name,a.client_plan_id,a.service_no, a.client_service_id, a.service_name, a.rate_per_unit, a.date_range_start, a.date_range_end, a.description, a.amount,a.base_plan_units, a.proration_factor, a.credit_coupon_code, a.units FROM(SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,  gld.plan_no AS plan_no,CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID  AS client_plan_id, gld.service_no AS service_no,  cs.client_service_id AS client_service_id, CS.SERVICE_NAME AS service_name,gld.usage_rate AS rate_per_unit, TO_CHAR(gld.start_date, 'YYYY-MM-DD')  AS date_range_start, TO_CHAR(gld.end_date, 'YYYY-MM-DD') AS date_range_end,gld.base_plan_units AS base_plan_units,gld.comments AS description,gld.orig_coupon_cd  AS credit_coupon_code,gld.debit AS amount, gld.proration_factor, gld.usage_units AS units FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.ALL_SERVICE ALS ON ALS.SERVICE_NO=GLD.SERVICE_NO AND ALS.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.PLAN_SERVICES PS ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO        =PS.PLAN_NO  AND CP.client_no     =PS.client_no  WHERE gld.invoice_no = "+invoice_no+")a where line_no="+loop2
					if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
					{resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
						  md = resultSet.getMetaData()}
					int columns = md.getColumnCount();
			  }
			  if(resultSet==null)
			  {
				  Check_Credit=db.executeQueryP2("SELECT ORIG_CREDIT_ID from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
				  if(!Check_Credit.contains("null"))
				  {
					 DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.line_no,a.plan_no,a.plan_name,a.client_plan_id,a.service_no, a.client_service_id, a.service_name, a.date_range_start, a.description, a.amount,a.base_plan_units, a.proration_factor, a.credit_coupon_code, a.units FROM(SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no,  gld.plan_no AS plan_no,CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID  AS client_plan_id, gld.service_no AS service_no,  cs.client_service_id AS client_service_id, CS.SERVICE_NAME AS service_name, TO_CHAR(gld.start_date, 'YYYY-MM-DD')  AS date_range_start, gld.base_plan_units AS base_plan_units,gld.comments AS description,gld.orig_coupon_cd  AS credit_coupon_code,gld.debit AS amount, gld.proration_factor, gld.usage_units AS units FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.ALL_SERVICE ALS ON ALS.SERVICE_NO=GLD.SERVICE_NO AND ALS.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.PLAN_SERVICES PS ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO        =PS.PLAN_NO  AND CP.client_no     =PS.client_no  WHERE gld.invoice_no = "+invoice_no+")a where line_no="+loop2
					if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
					{resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
				   md = resultSet.getMetaData()}
				  }
			  }
			  int columns = md.getColumnCount();
			  while (resultSet.next())
			  {
				  for(int u=1; u<=columns; ++u)
				  {
					  if(resultSet.getObject(u).toString().equals("null")||resultSet.getObject(u).toString()==null){
						  logi.logInfo("No value in "+resultSet.getObject(u))
					  }else{
					  logi.logInfo("Value inserting in hash map")
					  line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
					  }
				  }
			  }
			  line_item.sort();
			  acct_plan_line_items_row.put("invoice_line_items_row["+(loops-1)+"]",line_item.sort())
			  line_item = new LinkedHashMap();
			  loops++;
		  }
	  acct_plan_line_items_row=acct_plan_line_items_row.sort()
	  return acct_plan_line_items_row
	  logi.logInfo("********md_Unit_Inst_Invoice_Line_items_m Ends**************")
	  }
public def md_Sub_verify_adjust_dates_m(String str){
	 ConnectDB db = new ConnectDB()
	 logi.logInfo "into the md_verify_bill_thru_date_by_VT_m"
	 logi.logInfo "values from the str param : "+str
	
	 logi.logInfo "value before comma: "+str.split(",")[0];
	 logi.logInfo "value after comma: "+str.split(",")[1];
	 
	 String month = str.split(",")[0]
	 String days = str.split(",")[1]
	 
	 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	 int i = Integer.parseInt(month)
	 int d = Integer.parseInt(days)
	 d=d+1
	 logi.logInfo "value of d after 1 ; "+d
	 logi.logInfo "before query"
	 String Query = "SELECT TO_CHAR(ARIACORE.ARIAVIRTUALTIME(" + client_no + "),'yyyy-MM-dd')AS CURRENT_DATE FROM DUAL";
	 String st = db.executeQueryP2(Query)
	 logi.logInfo "Stratergy" + st
	 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH)
	 Date current_Date=dateFormat.parse(st);
	 logi.logInfo("Current date "+current_Date)
	 Calendar now = Calendar.getInstance()
	 now.setTime(current_Date);
	 logi.logInfo("GOT TIME "+now.getTime())
	 now.add(Calendar.MONTH,i)
	 now.add(Calendar.DAY_OF_MONTH,-d)
	 logi.logInfo("GOT TIME 222 "+now.getTime())
	 /*Date Pastdate = now.getTime();
	 logi.logInfo("GOT TIME111 "+Pastdate)
	 def startDate= dateFormat.format(Pastdate);*/
	 
	 //logi.logInfo "date has been added "+calendar
	 //String billing_date = now.toString()
	 logi.logInfo "date converted to string "+now.getTime()
	 String finaldate = dateFormat.format(now.getTime())
	 
	 return finaldate
}

public String md_get_bill_thru_date_m(String str){
	 InputMethods im = new InputMethods()
	 String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null)
	 String query = "SELECT TO_CHAR(LAST_BILL_THRU_DATE,'YYYY-MM-DD') AS BILL_THRU_DATE FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO = "+plan_instance_no
	 ConnectDB db = new ConnectDB()
	 String billing_date = db.executeQueryP2(query).toString()
	 
	 return billing_date
}

 
public LinkedHashMap md_get_acct_plans_all_plandetails_m(String param){
	 LinkedHashMap row = new LinkedHashMap<String , HashMap<String,String>>()
	 ConnectDB db = new ConnectDB()
	 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	 InputMethods im = new InputMethods()
	 String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
	 String query_count = "SELECT  count(*) as counts FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.CLIENT_PLAN CP ON PI.PLAN_NO = CP.PLAN_NO AND PI.CLIENT_NO = CP.CLIENT_NO WHERE ACCT_NO = "+acct_no+" and cp.supp_plan_ind = '0' order by cp.supp_plan_ind asc,pi.plan_instance_no asc "
	 String plancount = db.executeQueryP2(query_count).toString()
	 int plan_count = Integer.parseInt(plancount)
	 logi.logInfo("value converted to integer : "+plan_count)
	 int s = 0
	 for(int i=1;i<=plan_count;i++){
		   logi.logInfo("entered into the loop")
			
		   logi.logInfo("counting in master plan : "+s)
		   row.put("all_acct_plans_m_row["+s+"]",md_get_plans_all_master_plan_m(acct_no,i))
		   s++
	 }
	 logi.logInfo " value of s after master plan details : "+s
	 String query_count1 = "SELECT  count(*) as counts FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.CLIENT_PLAN CP ON PI.PLAN_NO = CP.PLAN_NO AND PI.CLIENT_NO = CP.CLIENT_NO WHERE ACCT_NO = "+acct_no+" and cp.supp_plan_ind = '1' order by cp.supp_plan_ind asc,pi.plan_instance_no asc "
	 String plancount1 = db.executeQueryP2(query_count1).toString()
	 int plan_count1 = Integer.parseInt(plancount1)
	 logi.logInfo("value converted to integer : "+plan_count)
	 for(int i=1;i<=plan_count1;i++){
		   logi.logInfo("entered into the loop")
		   
		   logi.logInfo("counting in supp plan : "+s)
		   row.put("all_acct_plans_m_row["+s+"]",md_get_plans_all_Supp_plan_m(acct_no,i))
		   s++
	 }
	 
	 return row.sort()
}
	 
	public LinkedHashMap md_get_plans_all_master_plan_m(String acct_no,int i){
			ConnectDB db = new ConnectDB()
			LinkedHashMap<String,String> mpi_plans_row = new LinkedHashMap<String,String>()

	  //String query = "SELECT MP.rollover_plan_status_cd,  MP.initial_plan_status_cd,  MP.cont_rollover_client_plan_id,  MP.c_roll_client_rate_sched_id,  MP.contract_rollover_plan_no,MP.CONT_ROLLOVER_RATE_SCHED_NO,  MP.plan_no,  MP.CLIENT_RATE_SCHEDULE_ID,  MP.plan_instance_no,  MP.PLAN_NAME,  MP.PLAN_DESC,  MP.PRORATION_INVOICE_TIMING_CD,  MP.client_plan_instance_id,  MP.PLAN_DATE,  MP.PLAN_UNITS,  MP.LAST_BILL_DATE,  MP.BILL_THRU_DATE ,  MP.NEXT_BILL_DATE,  MP.BILL_DAY,  MP.RECURRING_BILLING_INTERVAL,  MP.USAGE_BILLING_INTERVAL,  MP.BILLING_IND,  MP.DISPLAY_IND,  MP.ROLLOVER_MONTHS,  MP.ROLLOVER_PLAN_NO,  MP.EARLY_CANCEL_FEE,  MP.EARLY_CANCEL_MIN_MONTHS,  MP.plan_2_assign_on_susp,  MP.initial_plan_status,  MP.init_free_period_duration,  MP.init_free_period_uom_cd,  MP.rollover_plan_status,  MP.rollover_plan_status_duration,  MP.rollover_plan_status_uom_cd,  MP.billing_group_no,  MP.client_billing_group_id,  MP.DUNNING_GROUP_NO,  MP.client_DUNNING_group_id,  MP.default_notify_method,  MP.prepaid_ind,  MP.currency_cd,  MP.rate_schedule_no,  MP.rate_schedule_name,  MP.rate_sched_is_default_ind,  MP.SUPP_PLAN_IND,  MP.plan_instance_status_cd,  MP.plan_instance_status_date,  MP.dunning_state,  MP.DUNNING_STEP,  MP.dunning_degrade_date,  MP.plan_assignment_date,  MP.plan_deprovisioned_date,  MP.CLIENT_PLAN_ID,  MP.client_receipt_id,  MP.MASTER_PLAN_INSTANCE_NO,  MP.client_master_plan_instance_id,  MP.parent_plan_instance_no,MP.CONT_ROLLOVER_RATE_SCHED_NO,  MP.available_from_dt,  MP.available_to_dt, MP.ROLLOVER_PLAN_DURATION,  MP.ROLLOVER_PLAN_UOM_CD,  mp.CLIENT_ROLLOVER_PLAN_ID FROM  (SELECT  CP2.CLIENT_PLAN_ID AS CLIENT_ROLLOVER_PLAN_ID,CP.ROLLOVER_PLAN_PNUM AS ROLLOVER_PLAN_DURATION, CP.ROLLOVER_PLAN_PTYPE AS ROLLOVER_PLAN_UOM_CD, TO_CHAR(NRS.EFFECTIVE_FROM_DATE,'YYYY-MM-DD') AS available_from_dt,    TO_CHAR(NRS.EFFECTIVE_TO_DATE,'YYYY-MM-DD') AS available_to_dt, CP.ROLLOVER_INST_STATUS_CD AS rollover_plan_status_cd, CP.DEFAULT_PLAN_INST_STATUS_CD AS initial_plan_status_cd, CP2.client_plan_id AS cont_rollover_client_plan_id, prs2.client_rate_schedule_id AS c_roll_client_rate_sched_id, amci.rollover_plan_no AS contract_rollover_plan_no, pi.plan_no, pi.plan_instance_no, CP.PLAN_NAME, cp.EU_HTML_LONG AS PLAN_DESC, PI.PLAN_INSTANCE_CDID AS client_plan_instance_id, TO_CHAR(PI.PLAN_DATE,'YYYY-MM-DD') AS PLAN_DATE, PI.PLAN_UNITS, TO_CHAR(PI.LAST_BILL_DATE,'YYYY-MM-DD') AS LAST_BILL_DATE, TO_CHAR(PI.LAST_BILL_THRU_DATE,'YYYY-MM-DD') AS BILL_THRU_DATE ,    TO_CHAR(PI.NEXT_BILL_DATE,'YYYY-MM-DD') AS NEXT_BILL_DATE,EXTRACT (DAY FROM PI.NEXT_BILL_DATE)  AS BILL_DAY, CP.BILLING_INTERVAL AS RECURRING_BILLING_INTERVAL, CP.USAGE_BILLING_INTERVAL, CP.BILLING_IND, CASE WHEN CP.NO_DISPLAY_IND = 0 THEN '1' ELSE '0' END AS DISPLAY_IND, CP.ROLLOVER_MONTHS, CP.ROLLOVER_PLAN_NO, CP.EARLY_CANCEL_FEE, CP.EARLY_CANCEL_MIN_MONTHS, CP.PLAN_2_ASSIGN_ON_SUSPENSION AS plan_2_assign_on_susp,    CP.DEFAULT_PLAN_INST_STATUS_CD AS initial_plan_status, CP.INITIAL_FREE_MONTHS AS init_free_period_duration, CP.INITIAL_FREE_PNUM AS init_free_period_uom_cd,    CP.ROLLOVER_ACCT_STATUS_CD     AS rollover_plan_status,    CP.ROLLOVER_INST_STATUS_PNUM   AS rollover_plan_status_duration,    CP.ROLLOVER_INST_STATUS_CD     AS rollover_plan_status_uom_cd,    PIM.billing_group_no,    ABG.PROFILE_CDID AS client_billing_group_id,    PIM.DUNNING_GROUP_NO,    ADG.DUNNING_GROUP_CDID   AS client_DUNNING_group_id,    PD.NOTIFY_METHOD         AS default_notify_method,    PD.PREPAID_PLAN_IND      AS prepaid_ind,    PRS.SCHEDULE_CURRENCY_CD AS currency_cd,    PI.SCHEDULE_NO           AS rate_schedule_no,    PRS.SCHEDULE_NAME        AS rate_schedule_name,    PRS.PLAN_DEFAULT_IND     AS rate_sched_is_default_ind,    CP.SUPP_PLAN_IND,    PI.STATUS_CD                         AS plan_instance_status_cd,    TO_CHAR(PI.STATUS_DATE,'YYYY-MM-DD') AS plan_instance_status_date,    PIM.dunning_state,    PIM.DUNNING_STEP,    TO_CHAR(PIM.DUNNING_DEGRADE_DATE,'YYYY-MM-DD') AS dunning_degrade_date,    TO_CHAR(PI.PROVISION_DATE,'YYYY-MM-DD')        AS plan_assignment_date,    TO_CHAR(PI.DEPROVISION_DATE,'YYYY-MM-DD')      AS plan_deprovisioned_date,    CP.CLIENT_PLAN_ID,    PI.CREATE_API_RECEIPT_ID AS client_receipt_id,    PI.MASTER_PLAN_INSTANCE_NO,    PI.PLAN_INSTANCE_CDID AS client_master_plan_instance_id,    PI.parent_plan_instance_no,    CP.PRORATION_INVOICE_TIMING_CD,    PRS.CLIENT_RATE_SCHEDULE_ID, PRS2.SCHEDULE_NO as CONT_ROLLOVER_RATE_SCHED_NO,   ROW_NUMBER() OVER(order by cp.supp_plan_ind ASC,pi.plan_instance_no ASC) AS SEQ  FROM ARIACORE.PLAN_INSTANCE PI  LEFT JOIN ARIACORE.CLIENT_PLAN CP  ON PI.PLAN_NO    = CP.PLAN_NO  AND PI.CLIENT_NO = CP.CLIENT_NO  LEFT JOIN ARIACORE.PLAN_INSTANCE_MASTER PIM  ON PI.PLAN_INSTANCE_NO = PIM.PLAN_INSTANCE_NO  AND PI.CLIENT_NO       = PIM.CLIENT_NO  LEFT JOIN ARIACORE.ACCT_BILLING_GROUP ABG  ON PIM.BILLING_GROUP_NO = ABG.BILLING_GROUP_NO  AND PI.CLIENT_NO        = ABG.CLIENT_NO  LEFT JOIN ariacore.acct_dunning_group adg  ON adg.acct_no    =pi.acct_no  AND ADG.CLIENT_NO = PI.CLIENT_NO  LEFT JOIN ARIACORE.PLAN_DEFAULTS PD  ON PI.PLAN_NO    = PD.PLAN_NO  AND PI.CLIENT_NO = PD.CLIENT_NO  LEFT JOIN ARIACORE.PLAN_RATE_SCHEDULE PRS  ON PI.SCHEDULE_NO = PRS.SCHEDULE_NO  AND PI.CLIENT_NO  = PRS.CLIENT_NO  LEFT JOIN ariacore.acct_multiplan_contract_inst amci  ON PI.PLAN_INSTANCE_NO = amci.PLAN_INSTANCE_NO  LEFT JOIN ariacore.PLAN_CONTRACT_RO_RATE_SCHED pcrs  ON PI.CLIENT_NO = pcrs.CLIENT_NO  AND pcrs.plan_no=pi.plan_no  LEFT JOIN ARIACORE.NEW_RATE_SCHEDULES NRS  ON PRS.SCHEDULE_NO =NRS.schedule_no  AND PI.client_no   =NRS.client_no  LEFT JOIN ARIACORE.CLIENT_PLAN CP2  ON CP2.PLAN_NO = amci.rollover_plan_no AND cp.CLIENT_NO = CP2.CLIENT_NO  LEFT JOIN ARIACORE.PLAN_RATE_SCHEDULE PRS2  ON PRS2.PLAN_NO = cp2.plan_no  AND CP.CLIENT_NO  = PRS2.CLIENT_NO  WHERE PI.ACCT_NO = "+acct_no+"  )MP WHERE MP.SEQ = "+i
		String query = "SELECT   MP.cont_rollover_client_plan_id,  MP.c_roll_client_rate_sched_id,  MP.contract_rollover_plan_no,MP.CONT_ROLLOVER_RATE_SCHED_NO,  MP.plan_no,  MP.CLIENT_RATE_SCHEDULE_ID,  MP.plan_instance_no,  MP.PLAN_NAME,  MP.PLAN_DESC,  MP.PRORATION_INVOICE_TIMING_CD,  MP.client_plan_instance_id,  MP.PLAN_DATE,  MP.PLAN_UNITS,  MP.BILL_THRU_DATE ,  MP.NEXT_BILL_DATE,  MP.BILL_DAY,  MP.RECURRING_BILLING_INTERVAL,  MP.USAGE_BILLING_INTERVAL,  MP.BILLING_IND,  MP.DISPLAY_IND,  MP.ROLLOVER_MONTHS,  MP.ROLLOVER_PLAN_NO,  MP.EARLY_CANCEL_FEE,  MP.EARLY_CANCEL_MIN_MONTHS,  MP.plan_2_assign_on_susp,    MP.init_free_period_duration,  MP.init_free_period_uom_cd,   MP.rollover_plan_status_duration,  MP.billing_group_no,  MP.client_billing_group_id,  MP.DUNNING_GROUP_NO,  MP.client_DUNNING_group_id,  MP.default_notify_method,  MP.prepaid_ind,  MP.currency_cd,  MP.rate_schedule_no,  MP.rate_schedule_name,  MP.rate_sched_is_default_ind,  MP.SUPP_PLAN_IND,  MP.plan_instance_status_cd,  MP.plan_instance_status_date,  MP.dunning_state,  MP.DUNNING_STEP,  MP.dunning_degrade_date,  MP.plan_assignment_date,  MP.plan_deprovisioned_date,  MP.CLIENT_PLAN_ID,  MP.client_receipt_id,  MP.MASTER_PLAN_INSTANCE_NO,  MP.parent_plan_instance_no,MP.CONT_ROLLOVER_RATE_SCHED_NO,  MP.available_from_dt,  MP.available_to_dt, MP.ROLLOVER_PLAN_DURATION,  MP.ROLLOVER_PLAN_UOM_CD,  mp.CLIENT_ROLLOVER_PLAN_ID FROM  (SELECT  CP2.CLIENT_PLAN_ID AS CLIENT_ROLLOVER_PLAN_ID,CP.ROLLOVER_PLAN_PNUM AS ROLLOVER_PLAN_DURATION, CP.ROLLOVER_PLAN_PTYPE AS ROLLOVER_PLAN_UOM_CD, TO_CHAR(NRS.EFFECTIVE_FROM_DATE,'YYYY-MM-DD') AS available_from_dt,    TO_CHAR(NRS.EFFECTIVE_TO_DATE,'YYYY-MM-DD') AS available_to_dt, CP2.client_plan_id AS cont_rollover_client_plan_id, prs2.client_rate_schedule_id AS c_roll_client_rate_sched_id, amci.rollover_plan_no AS contract_rollover_plan_no, pi.plan_no, pi.plan_instance_no, CP.PLAN_NAME, cp.EU_HTML_LONG AS PLAN_DESC, PI.PLAN_INSTANCE_CDID AS client_plan_instance_id, TO_CHAR(PI.PLAN_DATE,'YYYY-MM-DD') AS PLAN_DATE, PI.PLAN_UNITS,  TO_CHAR(PI.LAST_BILL_THRU_DATE,'YYYY-MM-DD') AS BILL_THRU_DATE ,  CP.BILLING_INTERVAL AS RECURRING_BILLING_INTERVAL, CP.USAGE_BILLING_INTERVAL, CP.BILLING_IND, CASE WHEN CP.NO_DISPLAY_IND = 0 THEN '1' ELSE '0' END AS DISPLAY_IND, CP.ROLLOVER_MONTHS, CP.ROLLOVER_PLAN_NO, CP.EARLY_CANCEL_FEE, CP.EARLY_CANCEL_MIN_MONTHS, CP.PLAN_2_ASSIGN_ON_SUSPENSION AS plan_2_assign_on_susp,    CP.INITIAL_FREE_MONTHS AS init_free_period_duration, CP.INITIAL_FREE_PNUM AS init_free_period_uom_cd,    CP.ROLLOVER_INST_STATUS_PNUM   AS rollover_plan_status_duration,    CP.ROLLOVER_INST_STATUS_CD     AS rollover_plan_status_uom_cd,    PIM.billing_group_no,    ABG.PROFILE_CDID AS client_billing_group_id,    PIM.DUNNING_GROUP_NO,    ADG.DUNNING_GROUP_CDID   AS client_DUNNING_group_id,    PD.NOTIFY_METHOD         AS default_notify_method,    PD.PREPAID_PLAN_IND      AS prepaid_ind,    PRS.SCHEDULE_CURRENCY_CD AS currency_cd,    PI.SCHEDULE_NO           AS rate_schedule_no,    PRS.SCHEDULE_NAME        AS rate_schedule_name,    PRS.PLAN_DEFAULT_IND     AS rate_sched_is_default_ind,    CP.SUPP_PLAN_IND,    PI.STATUS_CD                         AS plan_instance_status_cd,    TO_CHAR(PI.STATUS_DATE,'YYYY-MM-DD') AS plan_instance_status_date,    PIM.dunning_state,    PIM.DUNNING_STEP,    TO_CHAR(PIM.DUNNING_DEGRADE_DATE,'YYYY-MM-DD') AS dunning_degrade_date,    TO_CHAR(PI.PROVISION_DATE,'YYYY-MM-DD')        AS plan_assignment_date,    TO_CHAR(PI.DEPROVISION_DATE,'YYYY-MM-DD')      AS plan_deprovisioned_date,    CP.CLIENT_PLAN_ID,    PI.CREATE_API_RECEIPT_ID AS client_receipt_id,    PI.MASTER_PLAN_INSTANCE_NO,    PI.parent_plan_instance_no,    CP.PRORATION_INVOICE_TIMING_CD,    PRS.CLIENT_RATE_SCHEDULE_ID, PRS2.SCHEDULE_NO as CONT_ROLLOVER_RATE_SCHED_NO,   ROW_NUMBER() OVER(order by cp.supp_plan_ind ASC,pi.plan_instance_no ASC) AS SEQ  FROM ARIACORE.PLAN_INSTANCE PI  LEFT JOIN ARIACORE.CLIENT_PLAN CP  ON PI.PLAN_NO    = CP.PLAN_NO  AND PI.CLIENT_NO = CP.CLIENT_NO  LEFT JOIN ARIACORE.PLAN_INSTANCE_MASTER PIM  ON PI.PLAN_INSTANCE_NO = PIM.PLAN_INSTANCE_NO  AND PI.CLIENT_NO       = PIM.CLIENT_NO  LEFT JOIN ARIACORE.ACCT_BILLING_GROUP ABG  ON PIM.BILLING_GROUP_NO = ABG.BILLING_GROUP_NO  AND PI.CLIENT_NO        = ABG.CLIENT_NO  LEFT JOIN ariacore.acct_dunning_group adg  ON adg.acct_no    =pi.acct_no  AND ADG.CLIENT_NO = PI.CLIENT_NO  LEFT JOIN ARIACORE.PLAN_DEFAULTS PD  ON PI.PLAN_NO    = PD.PLAN_NO  AND PI.CLIENT_NO = PD.CLIENT_NO  LEFT JOIN ARIACORE.PLAN_RATE_SCHEDULE PRS  ON PI.SCHEDULE_NO = PRS.SCHEDULE_NO  AND PI.CLIENT_NO  = PRS.CLIENT_NO  LEFT JOIN ariacore.acct_multiplan_contract_inst amci  ON PI.PLAN_INSTANCE_NO = amci.PLAN_INSTANCE_NO  LEFT JOIN ariacore.PLAN_CONTRACT_RO_RATE_SCHED pcrs  ON PI.CLIENT_NO = pcrs.CLIENT_NO  AND pcrs.plan_no=pi.plan_no  LEFT JOIN ARIACORE.NEW_RATE_SCHEDULES NRS  ON PRS.SCHEDULE_NO =NRS.schedule_no  AND PI.client_no   =NRS.client_no  LEFT JOIN ARIACORE.CLIENT_PLAN CP2  ON CP2.PLAN_NO = amci.rollover_plan_no AND cp.CLIENT_NO = CP2.CLIENT_NO  LEFT JOIN ARIACORE.PLAN_RATE_SCHEDULE PRS2  ON PRS2.PLAN_NO = cp2.plan_no  AND CP.CLIENT_NO  = PRS2.CLIENT_NO  WHERE PI.ACCT_NO = "+acct_no+"  )MP WHERE MP.SEQ = "+i
			ResultSet rs1 = db.executePlaQuery(query);
			ResultSetMetaData md1 = rs1.getMetaData();
			int columns1 = md1.getColumnCount();
			while (rs1.next()){

				  for(int s=1; s<=columns1; ++s){
						if(rs1.getObject(s).toString().equals("null")){
							  logi.logInfo("No value in "+md1.getColumnName(s))
						}else{logi.logInfo("Value inserting in hash map "+md1.getColumnName(s)+" "+rs1.getObject(s))
						
							  
							  if(md1.getColumnName(s)=='CONT_ROLLOVER_RATE_SCHED_NO')
									mpi_plans_row.put('contract_rollover_rate_sched_no',rs1.getObject(s));
							  else if(md1.getColumnName(s)=='CONT_ROLLOVER_CLIENT_PLAN_ID')
									mpi_plans_row.put('contract_rollover_client_plan_id',rs1.getObject(s));
							  else if(md1.getColumnName(s)=='C_ROLL_CLIENT_RATE_SCHED_ID')
									mpi_plans_row.put('contract_rollover_client_rate_sched_id',rs1.getObject(s));
							  else
									mpi_plans_row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
						logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))

				  }

				  logi.logInfo "after for loop ABCDEFG"
				  String plan_instance_status = (String)mpi_plans_row.get("plan_instance_status_cd")
				  logi.logInfo "VALUE OF INSTANCE_STATUS "+plan_instance_status
				  if(plan_instance_status.equals("1")){
						logi.logInfo "Active"
						mpi_plans_row.put("plan_instance_status_label","Active")
				  }
				  if(plan_instance_status.equals("0")){
						logi.logInfo "Inactive"
						mpi_plans_row.put("plan_instance_status_label","Inactive")
				  }
				  if(plan_instance_status.equals("41")){
						logi.logInfo "Trial"
						mpi_plans_row.put("plan_instance_status_label","Trial")
				  }
				  if(plan_instance_status.equals("61")){
						logi.logInfo "Active Non-Billable"
						mpi_plans_row.put("plan_instance_status_label","Active Non-Billable")
				  }
				  if(plan_instance_status.equals("-1")){
						logi.logInfo "Suspended"
						mpi_plans_row.put("plan_instance_status_label","Suspended")
				  }
				  if(plan_instance_status.equals("32")){
						logi.logInfo "Pending Activation"
						mpi_plans_row.put("plan_instance_status_label","Pending Activation")
				  }
				  if(plan_instance_status.equals("-3")){
						logi.logInfo "Terminated"
						mpi_plans_row.put("plan_instance_status_label","Terminated")
				  }
				  if(plan_instance_status.equals("3")){
						logi.logInfo "Pending Termination"
						mpi_plans_row.put("plan_instance_status_label","Pending Termination")
				  }


				  if(plan_instance_status.equals("-2")){
						logi.logInfo "Canceled"
						mpi_plans_row.put("plan_instance_status_label","Cancelled")
				  }
				  if(plan_instance_status.equals("2")){
						logi.logInfo "Pending Cancellation"
						mpi_plans_row.put("plan_instance_status_label","Pending Cancellation")
				  }


			}
			logi.logInfo "exiting while loop of mpi_plans_row hash"

			return mpi_plans_row.sort()
}

	public LinkedHashMap md_get_plans_all_Supp_plan_m(String acct_no,int i){
		ConnectDB db = new ConnectDB()
		LinkedHashMap<String,String> supp_plans_row = new LinkedHashMap<String,String>()
		String query = "SELECT SP.plan_no,SP.CLIENT_RATE_SCHEDULE_ID,SP.plan_instance_no,SP.PLAN_NAME,SP.PRORATION_INVOICE_TIMING_CD,SP.PLAN_DESC,SP.client_plan_instance_id,"+
				" SP.PLAN_DATE,SP.PLAN_UNITS,SP.LAST_BILL_DATE,SP.BILL_THRU_DATE,SP.NEXT_BILL_DATE,SP.BILL_DAY,SP.RECURRING_BILLING_INTERVAL,"+
				" SP.USAGE_BILLING_INTERVAL,SP.BILLING_IND,SP.DISPLAY_IND,SP.ROLLOVER_MONTHS,SP.ROLLOVER_PLAN_NO,SP.EARLY_CANCEL_FEE,SP.EARLY_CANCEL_MIN_MONTHS,"+
				" SP.plan_2_assign_on_susp,SP.initial_plan_status,SP.init_free_period_duration,SP.init_free_period_uom_cd,SP.rollover_plan_status,SP.rollover_plan_status_duration,"+
				" SP.rollover_plan_status_uom_cd,SP.default_notify_method,SP.prepaid_ind,SP.currency_cd,SP.rate_schedule_no,SP.rate_schedule_name,SP.rate_sched_is_default_ind,SP.SUPP_PLAN_IND,"+
				" SP.plan_instance_status_cd,SP.plan_instance_status_date,SP.plan_assignment_date,SP.plan_deprovisioned_date,SP.CLIENT_PLAN_ID,SP.client_receipt_id,SP.MASTER_PLAN_INSTANCE_NO,"+
				" SP.client_master_plan_instance_id,SP.parent_plan_instance_no ,SP.available_from_dt,  SP.available_to_dt"+
				" FROM (SELECT TO_CHAR(NRS.EFFECTIVE_FROM_DATE,'YYYY-MM-DD') AS available_from_dt,    TO_CHAR(NRS.EFFECTIVE_TO_DATE,'YYYY-MM-DD')         AS available_to_dt, pi.plan_no,pi.plan_instance_no,CP.PLAN_NAME,cp.EU_HTML_LONG AS PLAN_DESC,PI.PLAN_INSTANCE_CDID AS client_plan_instance_id,"+
				" TO_CHAR(PI.PLAN_DATE,'YYYY-MM-DD') AS PLAN_DATE,PI.PLAN_UNITS,TO_CHAR(PI.LAST_BILL_DATE,'YYYY-MM-DD') AS LAST_BILL_DATE,TO_CHAR(PI.LAST_BILL_THRU_DATE,'YYYY-MM-DD') AS BILL_THRU_DATE "+
				" ,TO_CHAR(PI.NEXT_BILL_DATE,'YYYY-MM-DD') AS NEXT_BILL_DATE,EXTRACT (DAY FROM PI.NEXT_BILL_DATE) AS BILL_DAY,CP.BILLING_INTERVAL AS RECURRING_BILLING_INTERVAL,"+
				" CP.USAGE_BILLING_INTERVAL,CP.BILLING_IND,CASE WHEN CP.NO_DISPLAY_IND = 0 THEN '1' ELSE '0' END AS DISPLAY_IND,CP.ROLLOVER_MONTHS,CP.ROLLOVER_PLAN_NO,"+
				" CP.EARLY_CANCEL_FEE,CP.EARLY_CANCEL_MIN_MONTHS,CP.PLAN_2_ASSIGN_ON_SUSPENSION AS plan_2_assign_on_susp,CP.DEFAULT_PLAN_INST_STATUS_CD AS initial_plan_status,CP.INITIAL_FREE_MONTHS AS init_free_period_duration,"+
				" CP.INITIAL_FREE_PNUM AS init_free_period_uom_cd,CP.ROLLOVER_ACCT_STATUS_CD AS rollover_plan_status,CP.ROLLOVER_INST_STATUS_PNUM AS rollover_plan_status_duration,"+
				" CP.ROLLOVER_INST_STATUS_CD AS rollover_plan_status_uom_cd,"+
				" PD.NOTIFY_METHOD AS default_notify_method,PD.PREPAID_PLAN_IND AS prepaid_ind,PRS.SCHEDULE_CURRENCY_CD AS currency_cd,PI.SCHEDULE_NO AS rate_schedule_no,"+
				" PRS.SCHEDULE_NAME AS rate_schedule_name,PRS.PLAN_DEFAULT_IND AS rate_sched_is_default_ind,CP.SUPP_PLAN_IND,PI.STATUS_CD AS plan_instance_status_cd,TO_CHAR(PI.STATUS_DATE,'YYYY-MM-DD') AS plan_instance_status_date,"+
				" TO_CHAR(PI.PROVISION_DATE,'YYYY-MM-DD') AS plan_assignment_date,"+
				" TO_CHAR(PI.DEPROVISION_DATE,'YYYY-MM-DD') AS plan_deprovisioned_date,CP.CLIENT_PLAN_ID,PI.CREATE_API_RECEIPT_ID AS client_receipt_id,PI.MASTER_PLAN_INSTANCE_NO,"+
				"PIm.PLAN_INSTANCE_CDID AS client_master_plan_instance_id,PI.parent_plan_instance_no,row_number() over(order by cp.supp_plan_ind asc,pi.plan_instance_no asc) as seq,CP.PRORATION_INVOICE_TIMING_CD,PRS.CLIENT_RATE_SCHEDULE_ID  "+
				" FROM ARIACORE.PLAN_INSTANCE PI "+
				" JOIN ARIACORE.CLIENT_PLAN CP ON PI.PLAN_NO = CP.PLAN_NO AND PI.CLIENT_NO = CP.CLIENT_NO "+
				" LEFT OUTER JOIN ariacore.acct_dunning_group adg ON adg.acct_no=pi.acct_no AND ADG.CLIENT_NO = PI.CLIENT_NO "+
				" JOIN ARIACORE.PLAN_DEFAULTS PD ON PI.PLAN_NO = PD.PLAN_NO AND PI.CLIENT_NO = PD.CLIENT_NO "+
				" JOIN ARIACORE.PLAN_RATE_SCHEDULE PRS ON PI.SCHEDULE_NO = PRS.SCHEDULE_NO AND PI.CLIENT_NO = PRS.CLIENT_NO "+
				" JOIN ariacore.plan_instance pim ON pi.MASTER_PLAN_INSTANCE_NO=pim.plan_instance_no AND PI.CLIENT_NO= pim.CLIENT_NO "+
				" LEFT JOIN ARIACORE.NEW_RATE_SCHEDULES NRS  ON PRS.SCHEDULE_NO =NRS.schedule_no  AND PI.client_no   =NRS.client_no"+
				" WHERE PI.ACCT_NO    = "+acct_no+" AND CP.SUPP_PLAN_IND = 1)SP WHERE SP.SEQ = "+i
		ResultSet rs1 = db.executePlaQuery(query);
		ResultSetMetaData md1 = rs1.getMetaData();
		int columns1 = md1.getColumnCount();
		while (rs1.next()){

			for(int s=1; s<=columns1; ++s){
				if(rs1.getObject(s).toString().equals("null")){
					logi.logInfo("No value in "+rs1.getObject(s))
				}else{logi.logInfo("Value inserting in hash map")
					if(md1.getColumnName(s)=='CONT_ROLLOVER_RATE_SCHED_NO')
						supp_plans_row.put('contract_rollover_rate_sched_no',rs1.getObject(s));
					else
						supp_plans_row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
				logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))

			}
			logi.logInfo "after for loop QWERTY"
			String plan_instance_status = (String)supp_plans_row.get("plan_instance_status_cd")
			logi.logInfo "VALUE OF SUPP INSTANCE_STATUS "+plan_instance_status
			if(plan_instance_status.equals("1")){
				supp_plans_row.put("plan_instance_status_label","Active")
			}
			if(plan_instance_status.equals("0")){
				supp_plans_row.put("plan_instance_status_label","Inactive")
			}
			if(plan_instance_status.equals("41")){
				supp_plans_row.put("plan_instance_status_label","Trial")
			}
			if(plan_instance_status.equals("61")){
				supp_plans_row.put("plan_instance_status_label","Active Non-Billable")
			}
			if(plan_instance_status.equals("-1")){
				supp_plans_row.put("plan_instance_status_label","Suspended")
			}
			if(plan_instance_status.equals("32")){
				supp_plans_row.put("plan_instance_status_label","Pending Activation")
			}
			if(plan_instance_status.equals("-3")){
				supp_plans_row.put("plan_instance_status_label","Terminated")
			}
			if(plan_instance_status.equals("-2")){
				supp_plans_row.put("plan_instance_status_label","Cancelled")
			}
			String instance_no = (String)supp_plans_row.get("parent_plan_instance_no")
			logi.logInfo "parent_plan_instance_no value : "+instance_no
			String parent_inst_query = "SELECT PLAN_INSTANCE_CDID AS client_parent_plan_instance_id FROM ARIACORE.PLAN_INSTANCE WHERE  PLAN_INSTANCE_NO = "+instance_no
			String client_parent_plan = db.executeQueryP2(parent_inst_query).toString()
			logi.logInfo "query value for client_parent_plan: "+client_parent_plan
			supp_plans_row.put("client_parent_plan_instance_id",client_parent_plan)
		}
		return supp_plans_row.sort()
		}

	public LinkedHashMap md_get_plans_plan_services_m(String param){
		LinkedHashMap row = new LinkedHashMap<String , HashMap<String,String>>()
		ConnectDB db = new ConnectDB()
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		InputMethods im = new InputMethods()
		String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		String query_count = "SELECT COUNT(*) AS COUNTS FROM ARIACORE.PLAN_INSTANCE_SERVICE_MAP WHERE ACCT_NO = "+acct_no
		String planserv = db.executeQueryP2(query_count).toString()
		int serv_count = Integer.parseInt(planserv)
		logi.logInfo("value converted to integer : "+serv_count)
		int s;
		for(int i=1;i<=serv_count;i++){
			logi.logInfo("entered into the loop")
			s=i-1
			logi.logInfo("counting in master plan : "+s)
			row.put("plan_services_row["+s+"]",md_plan_service_row_m(acct_no,i))

		}


		return row.sort()
	}

public LinkedHashMap md_plan_service_row_m(String acct_no,int i){
	 ConnectDB db = new ConnectDB()
	 LinkedHashMap<String,String> plan_service_row = new LinkedHashMap<String,String>()
	 
	 String query = "SELECT  PSR.SERVICE_NO, PSR.SERVICE_DESC,PSR.CLIENT_RATE_SCHEDULE_ID,"+
					"  PSR.IS_RECURRING_IND, PSR.IS_USAGE_BASED_IND, PSR.USAGE_TYPE,PSR.IS_TAX_IND,PSR.IS_ARREARS_IND,PSR.IS_SETUP_IND,PSR.IS_MISC_IND,"+
					"  PSR.IS_DONATION_IND,PSR.IS_ORDER_BASED_IND,PSR.IS_CANCELLATION_IND,PSR.COA_ID,PSR.CLIENT_COA_CODE,"+
					"  PSR.DISPLAY_IND,PSR.TIERED_PRICING_RULE,PSR.IS_MIN_FEE_IND,PSR.CLIENT_SERVICE_ID,PSR.FULFILLMENT_BASED_IND,PSR.USAGE_TYPE_NAME,PSR.USAGE_TYPE_DESC,"+
					"  PSR.USAGE_TYPE_CODE,PSR.TAXABLE_IND,PSR.USAGE_UNIT_LABEL,PSR.APPLY_USAGE_RATES_DAILY"+
					"  FROM("+
					" SELECT PISM.SERVICE_NO,ACS.TAXABLE_IND,ARS.CLIENT_RATE_SCHEDULE_ID, ACS.SERVICE_NAME  AS SERVICE_DESC,"+
					"   ACS.IS_RECURRING  AS IS_RECURRING_IND, ACS.IS_USAGE AS IS_USAGE_BASED_IND,"+
					"   ACS.USAGE_TYPE_NO AS USAGE_TYPE,ACS.IS_TAX AS IS_TAX_IND,ACS.IS_ARREARS AS IS_ARREARS_IND,ACS.IS_SETUP AS IS_SETUP_IND,"+
					"   ACS.IS_MISC AS IS_MISC_IND,ACS.IS_DONATION AS IS_DONATION_IND,ACS.IS_ORDER_BASED AS IS_ORDER_BASED_IND,ACS.IS_CANCELLATION AS IS_CANCELLATION_IND,"+
					"  ACS.COA_ID,COA.CLIENT_COA_CODE,  CASE WHEN ACS.NO_DISPLAY_IND = 0 THEN '1' ELSE '0' END AS DISPLAY_IND,PS.TIERED_PRICING_RULE_NO AS TIERED_PRICING_RULE,"+
					"  ACS.IS_MIN_FEE AS IS_MIN_FEE_IND,ACS.CLIENT_SERVICE_ID,PS.FULFILLMENT_BASED_IND,ACUT.SUPP_DISPLAY_STRING AS USAGE_TYPE_NAME,ACUT.DESCRIPTION AS USAGE_TYPE_DESC,"+
					"  ACUT.USAGE_TYPE_CD AS USAGE_TYPE_CODE,AUT.DESCRIPTION AS USAGE_UNIT_LABEL,PS.APPLY_DAILY_RATES_IND AS APPLY_USAGE_RATES_DAILY,"+
					"  row_number() over(order by pism.plan_instance_no ASC) as seq FROM ARIACORE.PLAN_INSTANCE_SERVICE_MAP PISM"+
					" JOIN ARIACORE.ALL_CLIENT_SERVICE ACS"+
					" ON PISM.SERVICE_NO = ACS.SERVICE_NO"+
					" AND PISM.CLIENT_NO = ACS.CLIENT_NO"+
					" JOIN ARIACORE.PLAN_SERVICES PS ON PS.SERVICE_NO = PISM.SERVICE_NO AND PISM.CLIENT_NO = PS.CLIENT_NO AND PISM.PLAN_NO = PS.PLAN_NO"+
					" LEFT OUTER JOIN ARIACORE.CHART_OF_ACCTS COA ON COA.COA_ID = ACS.COA_ID AND COA.CUSTOM_TO_CLIENT_NO = PISM.CLIENT_NO"+
					" LEFT OUTER JOIN ARIACORE.ALL_CLIENT_USAGE_TYPE ACUT ON ACUT.USAGE_TYPE_NO = ACS.USAGE_TYPE_NO AND PISM.CLIENT_NO = ACUT.CLIENT_NO"+
					" LEFT OUTER JOIN ARIACORE.ALL_USAGE_UNIT_TYPE AUT ON ACUT.USAGE_UNIT_TYPE_NO = AUT.USAGE_UNIT_TYPE_NO"+
					" JOIN ARIACORE.ALL_RATE_SCHEDULES ARS ON PISM.RATE_SCHED_NO = ARS.SCHEDULE_NO AND PISM.CLIENT_NO = ARS.CLIENT_NO"+
					" WHERE ACCT_NO      = "+acct_no+")PSR WHERE PSR.SEQ = "+i
					 ResultSet rs1 = db.executePlaQuery(query);
					 ResultSetMetaData md1 = rs1.getMetaData();
					 int columns1 = md1.getColumnCount();
					 while (rs1.next()){
						 
							  for(int s=1; s<=columns1; ++s){
								  if(rs1.getObject(s).toString().equals("null")){
									  logi.logInfo("No value in "+rs1.getObject(s))
								  }else{logi.logInfo("Value inserting in hash map")
								  plan_service_row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
								  logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
								  
							  }
							 
							  logi.logInfo "after for loop POTUS"
							  
					
				  }
					logi.logInfo "exiting while loop of mpi_plans_row hash"
					
			return plan_service_row.sort()
}

public LinkedHashMap md_get_plans_surcharge_m(String param){
	 LinkedHashMap row = new LinkedHashMap<String , HashMap<String,String>>()
	 ConnectDB db = new ConnectDB()
	 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	 InputMethods im = new InputMethods()
	 String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(param,null)
	 String query_count = "SELECT COUNT(*) AS COUNTS FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_SURCHARGE PS ON PS.PLAN_NO = PI.PLAN_NO AND PI.CLIENT_NO = PS.CLIENT_NO WHERE ACCT_NO = "+acct_no
	 String surcharge = db.executeQueryP2(query_count).toString()
	 int sur_count = Integer.parseInt(surcharge)
	 logi.logInfo("value converted to integer : "+sur_count)
	 int s;
	 for(int i=1;i<=sur_count;i++){
		   logi.logInfo("entered into the loop")
			s=i-1
		   logi.logInfo("counting in surcharge : "+s)
		   row.put("surcharges_all_row["+s+"]",md_get_plans_surcharge_rows_m(acct_no,i))
		   
	 }
	 return row.sort()
}

public LinkedHashMap md_get_plans_surcharge_rows_m(String acct_no,int i){
	 ConnectDB db = new ConnectDB()
	 LinkedHashMap<String,String> plan_service_row = new LinkedHashMap<String,String>()
	 
	 String query = "SELECT SUR.SURCHARGE_NO,SUR.SURCHARGE_NAME,SUR.CLIENT_SURCHARGE_ID,SUR.DESCRIPTION,SUR.EXT_DESCRIPTION,"+
					" SUR.SURCHARGE_TYPE,SUR.CURRENCY,SUR.INVOICE_APP_METHOD FROM"+
					" (SELECT PS.SURCHARGE_NO,CS.SURCHARGE_NAME,CS.CLIENT_SURCHARGE_ID,CS.DESCRIPTION,CS.EXT_DESC AS EXT_DESCRIPTION,"+
					" CS.SURCHARGE_TYPE_CD AS SURCHARGE_TYPE,SRC.CURRENCY_CD AS CURRENCY,CS.INLINE_OFFSET_IND AS INVOICE_APP_METHOD,"+
					" ROW_NUMBER() OVER(ORDER BY PI.PLAN_INSTANCE_NO) AS SEQ FROM ARIACORE.PLAN_INSTANCE PI"+
					" JOIN ARIACORE.PLAN_SURCHARGE PS ON PS.PLAN_NO = PI.PLAN_NO AND PI.CLIENT_NO = PS.CLIENT_NO"+
					" JOIN ARIACORE.CLIENT_SURCHARGE CS ON PS.SURCHARGE_NO = CS.SURCHARGE_NO AND PS.CLIENT_NO = CS.CLIENT_NO"+
					" JOIN ARIACORE.SURCHARGE_RATE_SCHEDULE  SRC ON PS.SURCHARGE_NO = SRC.SURCHARGE_NO AND PS.CLIENT_NO = SRC.CLIENT_NO"+
					" WHERE PI.ACCT_NO = "+acct_no+")SUR WHERE SUR.SEQ ="+i
					 ResultSet rs1 = db.executePlaQuery(query);
					 ResultSetMetaData md1 = rs1.getMetaData();
					 int columns1 = md1.getColumnCount();
					 while (rs1.next()){
						 
							  for(int s=1; s<=columns1; ++s){
								  if(rs1.getObject(s).toString().equals("null")){
									  logi.logInfo("No value in "+rs1.getObject(s))
								  }else{logi.logInfo("Value inserting in hash map")
								  plan_service_row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
								  logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
								  
							  }
							 
							  logi.logInfo "after for loop SPARK"
							  
					
				  }
					logi.logInfo "exiting while loop of mpi_plans_row hash"
					
			return plan_service_row.sort()
}

public  LinkedHashMap md_getAcctUnitInstanceAll_m(String TESTCASEID)
{
	 logi.logInfo("md_getAcctUnitInstanceAll_m")
	 ConnectDB db = new ConnectDB()
	 ArrayList<String> list = new ArrayList<String>()
	 
	 int i =1
	 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	 logi.logInfo("Client_No" +client_no )
	 String acct_no = getValueFromRequest("get_acct_pln_unit_inst_all_m","//acct_no")
	 logi.logInfo("acct_no" +acct_no )
	 String plan_instance_no = getValueFromRequest("get_acct_pln_unit_inst_all_m","//plan_instance_no")
	 logi.logInfo("plan_instance_no" +plan_instance_no )
	 String fulfilment = getValueFromRequest("get_acct_pln_unit_inst_all_m","//fulfillment_only")
	 logi.logInfo("include_current_acct" +fulfilment )
	 String acct_no1 = "select plan_instance_no from ariacore.plan_instance where acct_no = "+acct_no+" and client_no = "+client_no+" order by plan_instance_no "
	 LinkedHashMap <String, String> inner = new LinkedHashMap<String, String>();
	 LinkedHashMap <String, String> outer = new LinkedHashMap<String, String>();
	 if(plan_instance_no == "NoVal")
	 {
			 ResultSet rs=db.executePlaQuery(acct_no1)
			 while(rs.next())
			 {
				 
				 list[i]=rs.getObject(1)
				 i++
			 }
	 }
	 else if(plan_instance_no != "NoVal")
	 {
		  list[i]=plan_instance_no.toString()
		  i++
	 }
	 
	 logi.logInfo("TotalCount" + i)
	 int sizecount = list.size()
	 logi.logInfo("sizecount" + sizecount)
	 for(int j=1;j<=sizecount-1;j++)
	 {
		 logi.logInfo("List"+list[j])
		inner = md_getAcctUnitInstanceAllDtl_m(list[j].toString(),fulfilment)
		 outer.put("all_plan_instances_row["+j+"]",inner.sort())
		 inner = new LinkedHashMap<String, String>();
	 }
	 return outer.sort()
}

public  LinkedHashMap md_getAcctUnitInstanceAllDtl_m(String plan_instance_no,String fulfilment)
{
	 logi.logInfo("md_verifyGetAcctPlanMApi")
	 ConnectDB db = new ConnectDB()
	 int q=1,r=1,s=1,t=1,z=1,u=1
	 int count=1
	 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	 logi.logInfo("Client_No" +client_no )
	 logi.logInfo("plan_instance_no" +plan_instance_no )
	 LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
	 ArrayList<String> list = new ArrayList<String>()
	 //Acct Part
	 String query = "select pi.plan_instance_no,pi.plan_instance_cdid as client_plan_instance_id,pi.PARENT_PLAN_INSTANCE_NO AS PARENT_PLAN_INSTANCE_NO,cp.plan_name as plan_name,decode(supp_plan_ind,0,'MASTER',1,'SUPPLEMENTAL') as plan_type from ariacore.plan_instance pi join ariacore.client_plan cp on pi.plan_no = cp.plan_no where pi.plan_instance_no = "+plan_instance_no+" and pi.client_no =" +client_no
	 ResultSet rs=db.executePlaQuery(query)
	 ResultSetMetaData md = rs.getMetaData();
	 int columns = md.getColumnCount();
	 
	 while (rs.next())
	 {
		 for(int i=1; i<=columns; i++)
		 {
			 if((rs.getObject(i))!=null)
			 {
				 row.put((md.getColumnName(i)).toLowerCase(),(rs.getObject(i)));
			 }
		 }
	 }
	 //
	 // Stroing the Plan unit instance in list
	 String query2="select plan_unit_instance_no from ariacore.PLAN_INST_UNIT_INSTANCES where client_no = "+client_no+" and plan_instance_no =" +plan_instance_no
	 ResultSet rs1=db.executePlaQuery(query2)
	 ResultSetMetaData md1 = rs1.getMetaData();
	 while (rs1.next())
	 {
			  list[count]=rs1.getObject(1)
			  count++
	 }
	 for( int j=1 ; j <=count-1 ;j++)
	 {
		logi.logInfo("List"+list[j])
	 }
	 
	 //Getting Plan unit instance detail
	 for( int j=1 ; j <=count-1 ;j++)
	 {
		 String query3="select plan_unit_instance_no as plan_unit_inst_no,plan_unit_instance_cdid as client_plan_unit_inst_id,status as plan_unit_inst_status from ariacore.PLAN_INST_UNIT_INSTANCES where plan_unit_instance_no = "+list[j]
		 ResultSet rs2=db.executePlaQuery(query3)
		 ResultSetMetaData md2 = rs2.getMetaData();
		 int columns2 = md2.getColumnCount();
		 while (rs2.next())
		  {
			  for(int k=1; k<=columns2; k++)
			  {
				  if((rs2.getObject(k))!=null)
				  {
					  if(j==1)
					  {
						  row.put((md2.getColumnName(k)).toLowerCase(),(rs2.getObject(k)));
					  }
					  else
					  {
						  row.put((md2.getColumnName(k)).toLowerCase()+"["+j+"]",(rs2.getObject(k)));
					  }
				  }
			  }
		  }
		  
		 if(fulfilment!='1')
		 {
			 String query4="select piuis.service_no,acs.service_name,acs.client_service_id,piuis.fulfillment_status from ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis join ariacore.ALL_CLIENT_SERVICE acs on piuis.service_no =acs.service_no and piuis.client_no=acs.client_no where piuis.plan_unit_instance_no ="+list[j]
			 ResultSet rs3=db.executePlaQuery(query4)
			 ResultSetMetaData md3 = rs3.getMetaData();
			 int columns3 = md3.getColumnCount();
			 while (rs3.next())
			  {
				  for(int l=1; l<=columns3; l++)
				  {
					  if((rs3.getObject(l))!=null)
					  {
						  
						  if(u==1)
						  {
							  row.put((md3.getColumnName(l)).toLowerCase(),(rs3.getObject(l)));
						  }
						  else
						  {
							  row.put((md3.getColumnName(l)).toLowerCase()+"["+u+"]",(rs3.getObject(l)));
						  }
						 
					  }
				  }
				  u++
			  }
		 }
		 else
		 {
			 String query4="select piuis.service_no,acs.service_name,acs.client_service_id,piuis.fulfillment_status from ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis join ariacore.ALL_CLIENT_SERVICE acs on piuis.service_no =acs.service_no and piuis.client_no=acs.client_no where piuis.plan_unit_instance_no ="+list[j]+" and piuis.fulfillment_based_ind =1 order by service_no"
			 ResultSet rs3=db.executePlaQuery(query4)
			 ResultSetMetaData md3 = rs3.getMetaData();
			 int columns3 = md3.getColumnCount();
			 while (rs3.next())
			  {
				  for(int l=1; l<=columns3; l++)
				  {
					  if((rs3.getObject(l))!=null)
					  {
						  
						  if(u==1)
						  {
							  row.put((md3.getColumnName(l)).toLowerCase(),(rs3.getObject(l)));
						  }
						  else
						  {
							  row.put((md3.getColumnName(l)).toLowerCase()+"["+u+"]",(rs3.getObject(l)));
						  }
						 
					  }
				  }
				  u++
			  }
		 }
		 
	 }
	 
	 row.put("queued_ind",'A')
	 

		 
 
	 return row.sort()
}




public def md_get_acct_contacts_statement_DB_m(String param)
{
	 ConnectDB db = new ConnectDB()
	 LinkedHashMap output = new LinkedHashMap()
	 LinkedHashMap line_item = new LinkedHashMap()
	 LinkedHashMap statement_contacts_row = new LinkedHashMap()
	String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
	 String bg_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/billing_group_no[1]")
	 String Query;
	 //statement
	 if(bg_no.toString().contains("NoVal"))
	 Query="SELECT aa.first_name as stat_first_name,aa.middle_initial as stat_middle_initial,aa.last_name as stat_last_name, aa.company_name as stat_company_name,aa.address1 as stat_address1,aa.address2 as stat_address2,aa.address3 as stat_address3,aa.city as stat_city,aa.locality as stat_locality,aa.state as stat_state_prov,aa.country as stat_country,aa.zip as stat_postal_cd,aa.intl_phone as stat_phone,aa.phone_extension as stat_phone_ext,aa.cell_phone as stat_cell_phone ,aa.work_phone as stat_work_phone,aa.work_phone_extension as stat_work_phone_ext, aa.fax_phone as stat_fax,aa.email as stat_email,TO_CHAR(aa.birthdate,'YYYY-MM-DD') as stat_birthdate, abg.billing_group_no as stat_billing_group_no, abg.profile_cdid as stat_client_billing_group_id,aa.ADDRESS_SEQ AS stat_contact_no FROM  ARIACORE.ACCT_DETAILS ad join  ariacore.acct_address aa on ad.acct_no=aa.acct_no join ariacore.acct_billing_group abg on abg.statement_address_seq=aa.address_seq and aa.address_type_cd=1 and source_no=0 and aa.acct_no="+acct_no+" order by 21";
	   //Query="SELECT aa.first_name as stat_first_name,aa.middle_initial as stat_middle_initial,aa.last_name as stat_last_name, aa.company_name as stat_company_name,aa.address1 as stat_address1,aa.address2 as stat_address2,aa.address3 as stat_address3,aa.city as stat_city,aa.locality as stat_locality,aa.state as stat_state_prov,aa.country as stat_country,aa.zip as stat_postal_cd,aa.intl_phone as stat_phone,aa.phone_extension as stat_phone_ext,aa.cell_phone as stat_cell_phone ,aa.work_phone as stat_work_phone,aa.work_phone_extension as stat_work_phone_ext, aa.fax_phone as stat_fax,aa.email as stat_email,aa.birthdate as stat_birthdate, abg.billing_group_no as stat_billing_group_no, abg.profile_cdid as stat_client_billing_group_id FROM  ARIACORE.ACCT_DETAILS ad join  ariacore.acct_address aa on ad.acct_no=aa.acct_no join ariacore.acct_billing_group abg on abg.statement_address_seq=aa.address_seq and aa.address_type_cd=1 and source_no=0 and aa.acct_no="+acct_no+" order by 21";
	 else Query="SELECT aa.first_name as stat_first_name,aa.middle_initial as stat_middle_initial,aa.last_name as stat_last_name, aa.company_name as stat_company_name,aa.address1 as stat_address1,aa.address2 as stat_address2,aa.address3 as stat_address3,aa.city as stat_city,aa.locality as stat_locality,aa.state as stat_state_prov,aa.country as stat_country,aa.zip as stat_postal_cd,aa.intl_phone as stat_phone,aa.phone_extension as stat_phone_ext,aa.cell_phone as stat_cell_phone ,aa.work_phone as stat_work_phone,aa.work_phone_extension as stat_work_phone_ext, aa.fax_phone as stat_fax,aa.email as stat_email,TO_CHAR(aa.birthdate,'YYYY-MM-DD') as stat_birthdate, abg.billing_group_no as stat_billing_group_no, abg.profile_cdid as stat_client_billing_group_id,aa.ADDRESS_SEQ AS stat_contact_no FROM  ARIACORE.ACCT_DETAILS ad join  ariacore.acct_address aa on ad.acct_no=aa.acct_no join ariacore.acct_billing_group abg on abg.statement_address_seq=aa.address_seq and aa.address_type_cd=1 and source_no=0 and aa.acct_no="+acct_no+"and abg.billing_group_no="+bg_no+" order by 21";
	   //else Query="SELECT aa.first_name as stat_first_name,aa.middle_initial as stat_middle_initial,aa.last_name as stat_last_name, aa.company_name as stat_company_name,aa.address1 as stat_address1,aa.address2 as stat_address2,aa.address3 as stat_address3,aa.city as stat_city,aa.locality as stat_locality,aa.state as stat_state_prov,aa.country as stat_country,aa.zip as stat_postal_cd,aa.intl_phone as stat_phone,aa.phone_extension as stat_phone_ext,aa.cell_phone as stat_cell_phone ,aa.work_phone as stat_work_phone,aa.work_phone_extension as stat_work_phone_ext, aa.fax_phone as stat_fax,aa.email as stat_email,aa.birthdate as stat_birthdate, abg.billing_group_no as stat_billing_group_no, abg.profile_cdid as stat_client_billing_group_id FROM  ARIACORE.ACCT_DETAILS ad join  ariacore.acct_address aa on ad.acct_no=aa.acct_no join ariacore.acct_billing_group abg on abg.statement_address_seq=aa.address_seq and aa.address_type_cd=1 and source_no=0 and aa.acct_no="+acct_no+"and abg.billing_group_no="+bg_no+" order by 21";
	 ResultSet resultSet = db.executePlaQuery(Query)
	 ResultSetMetaData md = resultSet.getMetaData()
	 int columns = md.getColumnCount();
	 int loops=1
	 while (resultSet.next())
	 {
	 
			 for(int u=1; u<=columns; ++u)
		   {
				 if(resultSet.getObject(u)!=null)
					   line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
		   }
		   
			line_item.sort();
		   statement_contacts_row.put("statement_contacts_row["+(loops-1)+"]",line_item.sort())
		   line_item = new LinkedHashMap();
		   loops+=1;
	 }
	 statement_contacts_row.sort();
	 

	 return statement_contacts_row
	 
	  
}


public def md_get_acct_contacts_billing_DB_m(String param)
{
	 ConnectDB db = new ConnectDB()
	 LinkedHashMap output = new LinkedHashMap()
	 LinkedHashMap line_item = new LinkedHashMap()
	 LinkedHashMap billing_contacts_row = new LinkedHashMap()
	 String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
	 String bg_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/billing_group_no[1]")
	 String Query;
	 //billing
	 if(bg_no.toString().contains("NoVal"))
	 Query="SELECT aa.ADDRESS_SEQ as bill_contact_no,aa.first_name as bill_first_name,aa.middle_initial as bill_middle_initial,aa.last_name as bill_last_name, aa.company_name as bill_company_name,aa.address1 as bill_address1,aa.address2 as bill_address2,aa.address3 as bill_address3,aa.city as bill_city,aa.locality as bill_locality,aa.state as bill_state_prov,aa.country as bill_country,aa.zip as bill_postal_cd,aa.intl_phone as bill_phone,aa.phone_extension as bill_phone_ext,aa.cell_phone as bill_cell_phone ,aa.work_phone as bill_work_phone,aa.work_phone_extension as bill_work_phone_ext, aa.fax_phone as bill_fax,aa.email as bill_email,TO_CHAR(aa.birthdate,'YYYY-MM-DD') as bill_birthdate,abg.BILLING_GROUP_NO as bill_billing_group_no,abg.PROFILE_CDID as bill_client_billing_group_id FROM  ARIACORE.ACCT_DETAILS ad join  ariacore.acct_address aa on ad.acct_no=aa.acct_no and aa.address_type_cd=1 join ariacore.billing_info bi on bi.bill_address_seq=aa.address_seq join ariacore.acct_billing_group abg on abg.PRIMARY_PMT_METHOD_SEQ=bi.SEQ_NUM and bi.acct_no=abg.acct_no and bi.client_no=abg.client_no and source_no!=0 and aa.acct_no="+acct_no+"  order by 1"
	 else Query="SELECT aa.ADDRESS_SEQ as bill_contact_no,aa.first_name as bill_first_name,aa.middle_initial as bill_middle_initial,aa.last_name as bill_last_name, aa.company_name as bill_company_name,aa.address1 as bill_address1,aa.address2 as bill_address2,aa.address3 as bill_address3,aa.city as bill_city,aa.locality as bill_locality,aa.state as bill_state_prov,aa.country as bill_country,aa.zip as bill_postal_cd,aa.intl_phone as bill_phone,aa.phone_extension as bill_phone_ext,aa.cell_phone as bill_cell_phone ,aa.work_phone as bill_work_phone,aa.work_phone_extension as bill_work_phone_ext, aa.fax_phone as bill_fax,aa.email as bill_email,TO_CHAR(aa.birthdate,'YYYY-MM-DD') as bill_birthdate,abg.BILLING_GROUP_NO as bill_billing_group_no,abg.PROFILE_CDID as bill_client_billing_group_id FROM  ARIACORE.ACCT_DETAILS ad join  ariacore.acct_address aa on ad.acct_no=aa.acct_no and aa.address_type_cd=1 join ariacore.billing_info bi on bi.bill_address_seq=aa.address_seq join ariacore.acct_billing_group abg on abg.PRIMARY_PMT_METHOD_SEQ=bi.SEQ_NUM and bi.acct_no=abg.acct_no and bi.client_no=abg.client_no and source_no!=0 and aa.acct_no="+acct_no+"and abg.billing_group_no="+bg_no+" order by 1"
	 ResultSet resultSet = db.executePlaQuery(Query)
	 ResultSetMetaData md = resultSet.getMetaData()
	 int columns = md.getColumnCount();
	 int loops=1
	 while (resultSet.next())
	 {
	 
			 for(int u=1; u<=columns; ++u)
		   {
				 if(resultSet.getObject(u)!=null)
					   line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
		   }
		   
			line_item.sort();
		   billing_contacts_row.put("billing_contacts_row["+(loops-1)+"]",line_item.sort())
		   line_item = new LinkedHashMap();
		   loops+=1;
	 }
	 billing_contacts_row.sort();
	 

	 return billing_contacts_row
	 
	  
}


def md_Get_DB_BG_No_By_Plan_Inst_m(String input){
	logi.logInfo("********md_Get_DB_BG_No_By_Plan_Inst_m Start**************")
	InputMethods im = new InputMethods()
	ConnectDB db = new ConnectDB()
	String plan_inst_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(input,null)
	logi.logInfo "PlanInstance_no" + plan_inst_no
	   String query = "SELECT billing_group_no from ariacore.plan_instance_master where plan_instance_no =" + plan_inst_no
	   logi.logInfo query
	   String billing_group_no = db.executeQueryP2(query).toString();
	   logi.logInfo "billing group no : "+billing_group_no
	   return billing_group_no
  }
public def md_get_acct_contacts_acct_DB_m(String param)
{
	 ConnectDB db = new ConnectDB()
	 LinkedHashMap output = new LinkedHashMap()
	 LinkedHashMap line_item = new LinkedHashMap()
	 LinkedHashMap acct_contacts_row = new LinkedHashMap()
	 String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
	 
	 
	 //account
	 String Query="SELECT * from (SELECT aa.ADDRESS_SEQ as contact_no,aa.first_name,aa.middle_initial,aa.last_name, aa.company_name,aa.address1,aa.address2,aa.address3,aa.city,aa.locality,aa.state as state_prov,aa.country as country_cd,aa.zip as postal_cd,aa.intl_phone as phone,aa.phone_extension as phone_ext,aa.cell_phone,aa.work_phone,aa.work_phone_extension as work_phone_ext, aa.fax_phone as fax,aa.email,TO_CHAR(aa.birthdate,'YYYY-MM-DD') as birthdate FROM  ARIACORE.ACCT_DETAILS ad join  ariacore.acct_address aa on ad.acct_no=aa.acct_no and aa.address_type_cd=0 and source_no=0 and aa.acct_no="+acct_no+" order by contact_no desc)a where rownum=1";
	 ResultSet resultSet = db.executePlaQuery(Query)
	 ResultSetMetaData md = resultSet.getMetaData()
	 int columns = md.getColumnCount();
	 int loops=1
	 while (resultSet.next())
	 {
	 
		  for(int u=1; u<=columns; ++u)
		 {
			 if(resultSet.getObject(u)!=null)
				 line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
		 }
		 
		 line_item.sort();
		 acct_contacts_row.put("account_contact_row["+(loops-1)+"]",line_item.sort())
		 line_item = new LinkedHashMap();
		 loops+=1;
	 }
	 acct_contacts_row.sort();
	
	 return acct_contacts_row
	 
	 
}




public LinkedHashMap md_get_acct_DG_details_m(String a)
{
	 LinkedHashMap output = new LinkedHashMap()
	 LinkedHashMap output2 = new LinkedHashMap()
	 ConnectDB db=new ConnectDB()
	 String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//lit:get_acct_dunning_group_details_m[1]/acct_no[1]")
	 String Query=""
	 int counts=Integer.parseInt(db.executeQueryP2("select count(*) from ariacore.acct_dunning_group where acct_no="+acct_no).toString())
	 for (int loop=1;loop<=counts;loop++)
	 {
		 String DGN=db.executeQueryP2("select DUNNING_GROUP_NO from (select row_number()over(order by CLIENT_NO) as seq,DUNNING_GROUP_NO from ariacore.acct_dunning_group where acct_no="+acct_no+")where seq="+loop.toString()).toString()
		 
		 if((Integer.parseInt(db.executeQueryP2("select count(DUNNING_PROC_NO) from (select row_number()over(order by DUNNING_GROUP_NO) as seq,DUNNING_PROC_NO  from ariacore.acct_dunning_group where acct_no="+acct_no+" and DUNNING_GROUP_NO="+DGN+")").toString()))>=1)
		 {Query="SELECT DUNNING_GROUP_NO,DUNNING_GROUP_NAME, dunning_group_description, client_dunning_group_id, dunning_process_no, client_dunning_process_id,status from (SELECT row_number()over(order by adg.CLIENT_NO) as seq,adg.DUNNING_GROUP_NO,adg.DUNNING_GROUP_NAME,adg.DUNNING_GROUP_DESC as dunning_group_description,adg.DUNNING_GROUP_CDID AS client_dunning_group_id,adg.DUNNING_PROC_NO as dunning_process_no,cdp.DUNNING_PROC_CDID AS client_dunning_process_id,adg.STATUS_CD as status from ariacore.acct_dunning_group adg join ariacore.client_dunning_proc cdp on cdp.dunning_proc_no=adg.dunning_proc_no and adg.client_no=cdp.CLIENT_NO where acct_no="+acct_no+" )where  DUNNING_GROUP_NO="+DGN}
		 else
		 {Query="SELECT DUNNING_GROUP_NO,DUNNING_GROUP_NAME, dunning_group_description, client_dunning_group_id, dunning_process_no,status from (SELECT row_number()over(order by adg.CLIENT_NO) as seq,adg.DUNNING_GROUP_NO,adg.DUNNING_GROUP_NAME,adg.DUNNING_GROUP_DESC as dunning_group_description,adg.DUNNING_GROUP_CDID AS client_dunning_group_id,adg.DUNNING_PROC_NO as dunning_process_no,adg.STATUS_CD as status from ariacore.acct_dunning_group adg where acct_no="+acct_no+" )where  DUNNING_GROUP_NO="+DGN}
		 
		
		 ResultSet resultSet =db.executePlaQuery(Query)
		 ResultSetMetaData md = resultSet.getMetaData()
		 int columns = md.getColumnCount();
		 
		 while (resultSet.next())
		 {
		 
			for(int u=1; u<=columns; ++u)
			{
				if((resultSet.getObject(u))!=null)
				 output.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
				 
			}
			logi.logInfo "for plan instance number"
			String Query1="select PLAN_INSTANCE_NO,PLAN_INSTANCE_CDID as client_plan_instance_id from ariacore.plan_instance where PLAN_INSTANCE_NO in (select PLAN_INSTANCE_NO from ariacore.plan_instance_master where dunning_group_no ="+resultSet.getObject("DUNNING_GROUP_NO").toString()+")"
			ResultSet resultSet1 =db.executePlaQuery(Query1)
			ResultSetMetaData md1 = resultSet1.getMetaData()
			int columns1 = md1.getColumnCount();
			int k=1,lk=1;
		   while (resultSet1.next())
		   {
			   for(int l=1; l<=columns1; l++)
			   {
				   if((resultSet1.getObject(l))!=null)
				   {
					   if(md1.getColumnName(l).toLowerCase().equals("client_plan_instance_id"))
						{
							//for now I have hardcoded the two fields but in case if we need it dynamicaly in future we can change as per the requirment
							 if(k>1)output.put((md1.getColumnName(l)).toLowerCase()+"["+k.toString()+"]",(resultSet1.getObject(l)));
							 else{   output.put((md1.getColumnName(l)).toLowerCase(),(resultSet1.getObject(l)));}
							 k++;
						 }
						 else if(md1.getColumnName(l).toLowerCase().equals("plan_instance_no"))
						{
							 if(lk>1)output.put((md1.getColumnName(l)).toLowerCase()+"["+lk.toString()+"]",(resultSet1.getObject(l)));
							 else{   output.put((md1.getColumnName(l)).toLowerCase(),(resultSet1.getObject(l)));}
							 lk++;
						 }
						else{   output.put((md1.getColumnName(l)).toLowerCase(),(resultSet1.getObject(l)));}
					   
				   
				   }
			   }
			  
		   
		   }
		   logi.logInfo ""+output
		   output=output.sort()
		}
		 
		 output2.put("dunning_groups_row["+loop.toString()+"]",output)
			output=new LinkedHashMap()
		 }
	 
	 output2=output2.sort()
	 return output2
	 }


public String md_DB_Dunning_State_m(String input)
	{
		ConnectDB db = new ConnectDB()
		String out= db.executeQueryP2("SELECT dunning_state from ariacore.plan_instance_master where plan_instance_no="+im.md_GET_ACCT_OR_INSTANCE_NO_m(input.split ("#")[0],null).toString()).toString()
		if(out==null) return 'null'
		else return out
	}
	 
	public String md_DB_Dunnint_Proc_m(String input)
	{
		ConnectDB db = new ConnectDB()
		
		String out= db.executeQueryP2("select dunning_proc_no from ariacore.acct_dunning_group where dunning_group_no=(select dunning_group_no from ariacore.plan_instance_master where plan_instance_no="+im.md_GET_ACCT_OR_INSTANCE_NO_m(input.split ("#")[0],null).toString()+")").toString()
			if(out==null||out.contains("null")){
				logi.logInfo ""+out;
				  return 'null'}
			else return out.toString()
	}
	
	public String md_DB_Dunnint_step_m(String input)
	{
		ConnectDB db = new ConnectDB()
		
		String out= db.executeQueryP2("select dunning_step from ariacore.plan_instance_master where plan_instance_no="+im.md_GET_ACCT_OR_INSTANCE_NO_m(input.split ("#")[0],null).toString()).toString()
			if(out==null||out.contains("null")){
				logi.logInfo ""+out;
				  return 'null'}
			else return out.toString()
	}
	
	
	public String md_DB_Latest_Invoice_paymethod_m(String input)
	{
		ConnectDB db = new ConnectDB()
		
		String out= db.executeQueryP2("SELECT pay_method  from ariacore.gl where invoice_no=(select max(invoice_no)from ariacore.gl where acct_no="+im.md_GET_ACCT_OR_INSTANCE_NO_m(input,null).toString()+")").toString()
			return out.toString()
	}
	
	public def md_VOID_INVOICE_DB_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap output = new LinkedHashMap()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		if(acct_no.equals("NoVal"))
		acct_no=db.executeQueryP2("SELECT ACCT_NO from ariacore.ACCT where client_no= "+library.Constants.Constant.client_no+" and  CLIENT_ACCT_ID = '"+getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/client_acct_id[1]")+"'").toString()
		String Query="SELECT ORIG_EVENT_NO AS invoice_transaction_id ,EVENT_NO AS void_transaction_id FROM ARIACORE.ACCT_TRANS_VOID_HISTORY_VIEW WHERE ACCT_NO="+acct_no+" order by 1";
		ResultSet resultSet = db.executePlaQuery(Query)
		ResultSetMetaData md = resultSet.getMetaData()
		int columns = md.getColumnCount();
		int loops=1
		while (resultSet.next())
		{
		
			 for(int u=1; u<=columns; ++u)
			{
				if(resultSet.getObject(u)!=null)
					line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
			}
			
			line_item.sort();
			row_item.put("void_transactions_row["+(loops-1)+"]",line_item.sort())
			line_item = new LinkedHashMap();
			loops+=1;
		}
		row_item.sort();
		return row_item
		
	}
	
	def md_invoice_detailsDB_m (String s)
	  {
		  logi.logInfo"Entered into md_invoice_detailsDB_m"
		  String invoice_no= getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/invoice_no[1]")
		  ConnectDB db = new ConnectDB()
		  LinkedHashMap line_item = new LinkedHashMap()
		  LinkedHashMap row_item = new LinkedHashMap()
		  String Query="SELECT GLD.SEQ_NUM AS LINE_NO, GLD.SERVICE_NO, ALS.SERVICE_NAME,CASE WHEN GLD.SERVICE_NO NOT IN (401,402,403,201,202,203,204) THEN NVL(GLD.USAGE_UNITS,1) END AS UNITS,  NVL(CSP.RATE_PER_UNIT,GLD.USAGE_RATE) AS RATE_PER_UNIT,  GLD.DEBIT AS AMOUNT,  GLD.COMMENTS AS DESCRIPTION,  CASE WHEN (ALS.IS_RECURRING = 1 OR ALS.IS_SURCHARGE =1  OR ALS.IS_MIN_FEE =1) THEN TO_CHAR(GLD.START_DATE, 'YYYY-MM-DD') END AS DATE_RANGE_START,  CASE    WHEN (ALS.IS_RECURRING = 1 OR ALS.IS_SURCHARGE =1 OR ALS.IS_MIN_FEE    =1) THEN TO_CHAR(GLD.END_DATE, 'YYYY-MM-DD')  END AS DATE_RANGE_END,AU.USAGE_TYPE AS USAGE_TYPE_NO, GLD.PLAN_NO, CP.PLAN_NAME, CR.REASON_CD AS CREDIT_REASON_CD, CR.REASON_TEXT AS CREDIT_REASON_CODE_DESCRIPTION,CR.COMMENTS AS CSR_COMMENTS, GLD.ORIG_CLIENT_SKU AS CLIENT_SKU,  O.ORDER_NO,  GLD.ITEM_NO,NVL(CS.CLIENT_SERVICE_ID,GLD.SERVICE_NO) AS CLIENT_SERVICE_ID,  ACUT.USAGE_TYPE_CD,  CP.CLIENT_PLAN_ID,  IT.CLIENT_ITEM_ID ,  GLD.BASE_PLAN_UNITS,  GLD.PRORATION_FACTOR ,CASE WHEN GLD.SERVICE_NO NOT IN (401,402,403,0) THEN CASE WHEN GL.INVOICE_TYPE_CODE = 'P' THEN NVL(GLD.PRORATION_REMAINING_DAYS,GLD.ACTUAL_ADV_PERIOD_TOTAL_DAYS) ||' of ' || GLD.ACTUAL_ADV_PERIOD_TOTAL_DAYS ||' days.' END  END AS PRORATION_TEXT,GLD.ACTUAL_ADV_PERIOD_TOTAL_DAYS AS adv_billing_period_total_days,CASE WHEN (ALS.SERVICE_TYPE ='RC' OR ALS.SERVICE_TYPE   = 'RA') THEN GLD.PRORATION_REMAINING_DAYS END AS PRORATION_REMAINING_DAYS, CASE WHEN NVL(ALS.IS_ARREARS,0) = 1 THEN '6'  WHEN GLD.ORIG_COUPON_CD IS NOT NULL THEN '4' WHEN GLD.ORIG_CREDIT_ID IS NOT NULL THEN '3' WHEN CS.SERVICE_TYPE ='RC' THEN '1' WHEN GLD.ORIG_CREDIT_ID IS NULL AND ALS.SERVICE_TYPE ='AC' THEN '5' WHEN GLD.ORIG_CREDIT_ID IS NULL AND (ALS.SERVICE_TYPE ='SC' OR ALS.SERVICE_TYPE ='MN') THEN '1' ELSE '2'  END AS LINE_TYPE,  GLD.RATE_SCHEDULE_NO, RT.CLIENT_RATE_SCHEDULE_ID,  CASE  WHEN (GLD.DEBIT>=0) AND GLD.SERVICE_NO NOT IN (201,202,203,204) THEN NVL(RS.RATE_SEQ_NO,1) ELSE RS.RATE_SEQ_NO  END AS RATE_SCHEDULE_TIER_NO,GLD.ORIG_COUPON_CD AS CREDIT_COUPON_CODE,  GLD.PLAN_INSTANCE_NO,  PI.PLAN_INSTANCE_CDID AS CLIENT_PLAN_INSTANCE_ID, CASE WHEN GLD.SERVICE_NO IN (401,402,403,201,202,203,204) THEN '1' ELSE '0'  END AS TAX_RELATED_IND,GLD.ORIG_CHARGE_INVOICE_NO AS SVC_CREDIT_SOURCE_INVOICE_NO,  GLD.ORIG_CHARGE_SEQ_NUM AS SVC_CREDIT_SOURCE_LINE_NO,  ATR.EVENT_NO AS SVC_CREDIT_SOURCE_TRANS_ID, GCD.CREDITED_SEQ_NUM AS SVC_CREDIT_APPLIED_LINE_NO,  ACT.CLIENT_ACCT_ID,  GLD.MASTER_PLAN_INSTANCE_NO,  PI2.PLAN_INSTANCE_CDID AS CLIENT_MP_INSTANCE_ID,  ATN.EVENT_NO AS INVOICE_TRANSACTION_ID,  GLD.PURCHASE_ORDER_NO  AS PONUM,  GLD.BILL_FROM_LOCATION_NO,  GLD.SHIP_FROM_LOCATION_NO,  GLD.BILL_TO_ADDR_SEQ AS BILL_TO_ADDRESS_SEQ,  GLD.SHIP_TO_ADDR_SEQ AS SHIP_TO_ADDRESS_SEQ  FROM ARIACORE.ALL_INVOICE_DETAILS GLD  LEFT JOIN ARIACORE.PLAN_INSTANCE PI  ON PI.PLAN_INSTANCE_NO=GLD.PLAN_INSTANCE_NO  AND PI.CLIENT_NO =GLD.CLIENT_NO  LEFT JOIN ARIACORE.PLAN_INSTANCE PI2  ON GLD.MASTER_PLAN_INSTANCE_NO=PI2.PLAN_INSTANCE_NO  AND PI2.CLIENT_NO =GLD.CLIENT_NO  LEFT JOIN ARIACORE.ALL_SERVICE ALS  ON ALS.SERVICE_NO=GLD.SERVICE_NO  AND ALS.CLIENT_NO=GLD.CLIENT_NO  LEFT JOIN ARIACORE.PLAN_SERVICES PS  ON PS.CLIENT_NO  =ALS.CLIENT_NO  AND PS.SERVICE_NO=ALS.SERVICE_NO  AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  LEFT JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= GLD.SERVICE_NO  AND CS.CLIENT_NO=PS.CLIENT_NO  LEFT JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO   =GLD.PLAN_NO  AND CP.CLIENT_NO=GLD.CLIENT_NO  LEFT JOIN ARIACORE.NEW_RATE_SCHEDULES RT  ON RT.PLAN_NO =GLD.PLAN_NO  AND RT.SCHEDULE_NO=GLD.RATE_SCHEDULE_NO  AND RT.CLIENT_NO  =GLD.CLIENT_NO  LEFT JOIN ARIACORE.NEW_RATE_SCHED_RATES RS  ON RS.SCHEDULE_NO=RT.SCHEDULE_NO  AND RS.CLIENT_NO =RT.CLIENT_NO  AND RS.SERVICE_NO=GLD.SERVICE_NO and gld.usage_units between rs.from_unit-1 and rs.to_unit+1 LEFT JOIN ARIACORE.ACCT_TRANSACTION ATN  ON ATN.SOURCE_NO =GLD.INVOICE_DETAIL_NO  AND ATN.TRANSACTION_TYPE IN (21,22)  LEFT JOIN ARIACORE.CLIENT_SERVICE SER  ON SER.SERVICE_NO=GLD.SERVICE_NO  AND SER.CLIENT_NO=GLD.CLIENT_NO  LEFT JOIN ARIACORE.CLIENT_SURCHARGE CS  ON CS.ALT_SERVICE_NO_2_APPLY=SER.SERVICE_NO  AND CS.CLIENT_NO =GLD.CLIENT_NO  LEFT JOIN ARIACORE.CLIENT_SURCHARGE_PRICE CSP  ON CSP.SURCHARGE_NO =CS.SURCHARGE_NO  AND CSP.CLIENT_NO   =CS.CLIENT_NO  LEFT JOIN ARIACORE.ALL_USAGE AU  ON AU.CLIENT_NO =GLD.CLIENT_NO  AND AU.BILLED_ACCT_NO  = PI.ACCT_NO  AND AU.PLAN_INSTANCE_NO=GLD.PLAN_INSTANCE_NO  LEFT JOIN ARIACORE.ALL_CREDITS CR  ON CR.CREDIT_ID  = GLD.ORIG_CREDIT_ID  AND CR.CLIENT_NO = GLD.CLIENT_NO  LEFT JOIN ariacore.orders o ON o.client_no = gld.client_no AND o.invoice_no = gld.invoice_no  AND o.amount = gld.debit  and o.master_plan_instance_no = gld.master_plan_instance_no  LEFT JOIN ARIACORE.PLAN_INSTANCE_SERVICE_MAP PISM  ON PISM.CLIENT_NO = GLD.CLIENT_NO  AND PISM.PLAN_INSTANCE_NO = GLD.PLAN_INSTANCE_NO  AND PISM.SERVICE_NO = GLD.SERVICE_NO  LEFT JOIN ARIACORE.ALL_CLIENT_SERVICE ACS  ON ACS.CLIENT_NO   = PISM.CLIENT_NO  AND ACS.SERVICE_NO = PISM.SERVICE_NO  LEFT JOIN ARIACORE.ALL_CLIENT_USAGE_TYPE ACUT  ON ACUT.CLIENT_NO     =ACS.CLIENT_NO  AND ACUT.USAGE_TYPE_NO=ACS.USAGE_TYPE_NO  LEFT JOIN ARIACORE.INVENTORY_ITEMS IT  ON IT.CLIENT_SKU = GLD.ORIG_CLIENT_SKU  AND IT.CLIENT_NO =GLD.CLIENT_NO  LEFT JOIN ARIACORE.ACCT_TRANSACTION ATR  ON ATR.SOURCE_NO =GLD.ORIG_CHARGE_DETAIL_NO  AND ATR.ACCT_NO  =PI.ACCT_NO  LEFT JOIN ARIACORE.GL_CREDIT_DETAIL GCD  ON GCD.INVOICE_NO =GLD.INVOICE_NO  AND GCD.GL_DTL_CREDIT_SEQ_NUM=GLD.SEQ_NUM  AND GCD.CREDIT_ID = GLD.ORIG_CREDIT_ID LEFT JOIN ARIACORE.ALL_INVOICES GL ON GL.CLIENT_NO=GLD.CLIENT_NO AND GL.INVOICE_NO=GLD.INVOICE_NO LEFT JOIN ARIACORE.ACCT ACT ON ACT.ACCT_NO  =GL.ACCT_NO AND ACT.CLIENT_NO=GL.CLIENT_NO LEFT JOIN ARIACORE.ALL_SERVICE ALS      ON ALS.SERVICE_NO =GLD.SERVICE_NO AND ALS.CLIENT_NO =GLD.CLIENT_NO WHERE GLD.INVOICE_NO = "+invoice_no+" AND GLD.CLIENT_NO = " +library.Constants.Constant.client_no+" ORDER BY GLD.SEQ_NUM";
		  ResultSet resultSet = db.executePlaQuery(Query)
		  ResultSetMetaData md = resultSet.getMetaData()
		  int columns = md.getColumnCount();
		  int loops=1
		  while (resultSet.next())
		  {
				for(int u=1; u<=columns; ++u)
				{
					  if(resultSet.getObject(u)!=null)
							line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
				}
				
				line_item.sort();
				row_item.put("invoice_line_details_row["+(loops-1)+"]",line_item.sort())
				line_item = new LinkedHashMap();
				loops+=1;
		  }
		  row_item = row_item.sort();
		  return row_item
			}

	/**Returns the unapplied service credits details for get_unapplied_service_credits_m
	 * @param str (acct_hierarchy)
	 * @return hash values of service credits
	 */
	public LinkedHashMap md_get_unapplied_service_credits_m(String str){
		logi.logInfo("********md_Get_DB_BG_No_By_Plan_Inst_m Start**************")
		InputMethods im = new InputMethods()
		ConnectDB db = new ConnectDB()
		LinkedHashMap<String,String> row = new LinkedHashMap<String ,String>()
		String mpi = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//lit:get_unapplied_service_credits_m[1]/master_plan_instance_no[1]")
		String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null)
		if(mpi.toString().equals("NoVal")){
		String counter = "select count(*) as counts from ariacore.all_credits where acct_no = "+acct_no+" and LEFT_TO_APPLY > 0"
		String q_count = db.executeQueryP2(counter).toString()
		int row_count = Integer.parseInt(q_count)
		logi.logInfo("value converted to integer : "+row_count)
		int s;
		for(int i=1;i<=row_count;i++){
			logi.logInfo("entered into the loop")
			 s=i-1
			logi.logInfo("counting in service credits for account : "+s)
			row.put("unapplied_service_credits_details_row["+s+"]",md_service_credits_m(acct_no,i))
			
	  }}else{
 
 String mp_instance=mpi.toString()
 logi.logInfo "acct_no : "+acct_no
 logi.logInfo "mpi : "+mp_instance
	  String querys = "select count(*) as counts from ariacore.all_credits where acct_no = "+acct_no+" and MASTER_PLAN_INSTANCE_NO = "+mp_instance
	  
	  String m_count = db.executeQueryP2(querys).toString()
	  int row_count = Integer.parseInt(m_count)
	  logi.logInfo("value converted to integer : "+row_count)
	  int s;
	  for(int i=1;i<=row_count;i++){
		  logi.logInfo("entered into the loop")
		   s=i-1
		  logi.logInfo("counting in service credits for account : "+s)
		  row.put("unapplied_service_credits_details_row["+s+"]",md_service_credits_with_plan_m(acct_no,mp_instance,i))
	  }
	  }
		logi.logInfo "outer ring hash : "+row
		return row.sort()
	}
	
	public LinkedHashMap md_service_credits_with_plan_m(String acct_no,String mp_instance,int i){
		ConnectDB db = new ConnectDB()
		HashMap credits_row = new HashMap()
		LinkedHashMap tester = new LinkedHashMap()
		logi.logInfo "inside md_service_credits_with_plan_m BETA MATRIX "
		String query ="SELECT US.out_acct_no,  US.credit_id, US.create_user,US.create_date,US.initial_amount , US.amount_left_to_apply,US.reason_cd,"+
						" US.reason_text, US.comments,  US.CURRENCY_CD,  US.service_no_to_apply,US.service_name_to_apply, US.client_service_id_to_apply,"+
						" US.ELIGIBLE_PLAN_NO,  US.ELIGIBLE_SERVICE_NO, US.ELIGIBLE_PLAN_NAME, US.CLIENT_ELIGIBLE_PLAN_ID,"+
						" US.LOCKED_AMOUNT,US.OUT_MASTER_PLAN_INSTANCE_NO, US.OUT_CLIENT_MP_INSTANCE_ID FROM ("+
						" SELECT a.acct_no   AS out_acct_no,  a.credit_id AS credit_id, a.create_user AS create_user, TO_CHAR(a.create_date,'YYYY-MM-DD') AS create_date,"+
						"  a.amount AS initial_amount , a.left_to_apply AS amount_left_to_apply,a.reason_cd,"+
						"  a.reason_text, a.comments,  C.CURRENCY_CD,  C.ALT_SERVICE_NO_2_APPLY AS service_no_to_apply,  CS.SERVICE_NAME          AS service_name_to_apply,"+
						"  CASE  WHEN CS.CLIENT_SERVICE_ID IS NULL THEN TO_CHAR(CS.SERVICE_NO) ELSE TO_CHAR(CS.CLIENT_SERVICE_ID)  END     AS client_service_id_to_apply,  C.ELIGIBLE_PLAN_NO,  C.ELIGIBLE_SERVICE_NO, CP.PLAN_NAME  AS ELIGIBLE_PLAN_NAME,"+
						"  CP.CLIENT_PLAN_ID AS CLIENT_ELIGIBLE_PLAN_ID,  A.LOCKED_AMOUNT,A.MASTER_PLAN_INSTANCE_NO AS OUT_MASTER_PLAN_INSTANCE_NO,"+
						"  PI.PLAN_INSTANCE_CDID AS OUT_CLIENT_MP_INSTANCE_ID,  row_number() over(order by A.credit_id) AS seq"+
						" FROM ariacore.all_credits a"+
						" JOIN ARIACORE.CREDITS C"+
						" ON A.CREDIT_ID  = C.CREDIT_ID"+
						" AND A.CLIENT_NO = C.CLIENT_NO"+
						" LEFT OUTER JOIN ARIACORE.ALL_CLIENT_SERVICE CS"+
						" ON C.ALT_SERVICE_NO_2_APPLY = CS.SERVICE_NO"+
						" AND A.CLIENT_NO             = CS.CLIENT_NO"+
						" LEFT OUTER JOIN ARIACORE.CLIENT_PLAN CP"+
						" ON CP.PLAN_NO   = C.ELIGIBLE_PLAN_NO"+
						" AND A.CLIENT_NO = CP.CLIENT_NO"+
						" LEFT JOIN ARIACORE.PLAN_INSTANCE PI ON PI.PLAN_INSTANCE_NO = A.MASTER_PLAN_INSTANCE_NO AND PI.CLIENT_NO = A.CLIENT_NO"+
						" WHERE A.acct_no             = "+acct_no+" and A.MASTER_PLAN_INSTANCE_NO = "+mp_instance+")US WHERE US.SEQ = "+i
		ResultSet rs1 = db.executePlaQuery(query);
		ResultSetMetaData md1 = rs1.getMetaData();
		int columns1 = md1.getColumnCount();
		while (rs1.next()){
		 for(int s=1; s<=columns1; ++s){
				 if(rs1.getObject(s).toString().equals("null")){
						 logi.logInfo("No value in "+rs1.getObject(s))
							 }else{logi.logInfo("Value inserting in hash map")
							 credits_row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
							 logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
						 }
					 logi.logInfo "after for loop LOCK101"
					 String anniversary = (String)credits_row.get("locked_amount")
					 if(anniversary.equals("null")||anniversary.equals("NULL")){
						 logi.logInfo "no value in anniversary "+anniversary
						 credits_row.remove("locked_amount")
					 }else{
					 credits_row.put("amount_reserved_for_anniversary",anniversary)
					 logi.logInfo "value in anniversary : "+anniversary
					 credits_row.remove("locked_amount")
					 }
					 }
		   logi.logInfo "printing inner credits_row hash "+credits_row.sort()
			   return credits_row.sort()
	}
	
	public LinkedHashMap md_service_credits_m(String acct_no,int i){
		
		ConnectDB db = new ConnectDB()
		HashMap credits_row = new HashMap()
		LinkedHashMap tester = new LinkedHashMap()
		logi.logInfo "inside md_service_credits_with_plan_m ALPHA MATRIX "
		String query = "SELECT US.out_acct_no,  US.credit_id, US.create_user,US.create_date,US.initial_amount , US.amount_left_to_apply,US.reason_cd,"+
						" US.reason_text, US.comments,  US.CURRENCY_CD,  US.service_no_to_apply,US.service_name_to_apply, US.client_service_id_to_apply,"+
						" US.ELIGIBLE_PLAN_NO,  US.ELIGIBLE_SERVICE_NO, US.ELIGIBLE_PLAN_NAME, US.CLIENT_ELIGIBLE_PLAN_ID,"+
						" US.LOCKED_AMOUNT,US.OUT_MASTER_PLAN_INSTANCE_NO, US.OUT_CLIENT_MP_INSTANCE_ID FROM ("+
						" SELECT a.acct_no   AS out_acct_no,  a.credit_id AS credit_id, a.create_user AS create_user, TO_CHAR(a.create_date,'YYYY-MM-DD') AS create_date,"+
						"  a.amount AS initial_amount , a.left_to_apply AS amount_left_to_apply,a.reason_cd,"+
						"  a.reason_text, a.comments,  C.CURRENCY_CD,  C.ALT_SERVICE_NO_2_APPLY AS service_no_to_apply,  CS.SERVICE_NAME          AS service_name_to_apply,"+
						"  CASE  WHEN CS.CLIENT_SERVICE_ID IS NULL THEN TO_CHAR(CS.SERVICE_NO) ELSE TO_CHAR(CS.CLIENT_SERVICE_ID)  END    AS client_service_id_to_apply,  C.ELIGIBLE_PLAN_NO,  C.ELIGIBLE_SERVICE_NO, CP.PLAN_NAME  AS ELIGIBLE_PLAN_NAME,"+
						"  CP.CLIENT_PLAN_ID AS CLIENT_ELIGIBLE_PLAN_ID,  A.LOCKED_AMOUNT,A.MASTER_PLAN_INSTANCE_NO AS OUT_MASTER_PLAN_INSTANCE_NO,"+
						"  PI.PLAN_INSTANCE_CDID AS OUT_CLIENT_MP_INSTANCE_ID,  row_number() over(order by A.credit_id) AS seq"+
						" FROM ariacore.all_credits a"+
						" JOIN ARIACORE.CREDITS C"+
						" ON A.CREDIT_ID  = C.CREDIT_ID"+
						" AND A.CLIENT_NO = C.CLIENT_NO AND A.LEFT_TO_APPLY > 0"+
						" LEFT OUTER JOIN ARIACORE.ALL_CLIENT_SERVICE CS"+
						" ON C.ALT_SERVICE_NO_2_APPLY = CS.SERVICE_NO"+
						" AND A.CLIENT_NO             = CS.CLIENT_NO"+
						" LEFT OUTER JOIN ARIACORE.CLIENT_PLAN CP"+
						" ON CP.PLAN_NO   = C.ELIGIBLE_PLAN_NO"+
						" AND A.CLIENT_NO = CP.CLIENT_NO"+
						" LEFT JOIN ARIACORE.PLAN_INSTANCE PI ON PI.PLAN_INSTANCE_NO = A.MASTER_PLAN_INSTANCE_NO AND PI.CLIENT_NO = A.CLIENT_NO"+
						" WHERE A.acct_no             = "+acct_no+")US WHERE US.SEQ = "+i
						ResultSet rs1 = db.executePlaQuery(query);
						ResultSetMetaData md1 = rs1.getMetaData();
						int columns1 = md1.getColumnCount();
						while (rs1.next()){
						 for(int s=1; s<=columns1; ++s){
								 if(rs1.getObject(s).toString().equals("null")){
										 logi.logInfo("No value in "+rs1.getObject(s))
											 }else{logi.logInfo("Value inserting in hash map")
											 credits_row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
											 logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
										 }
									 logi.logInfo "after for loop LOCK101"
									 String anniversary = (String)credits_row.get("locked_amount")
									 if(anniversary.equals("null")||anniversary.equals("NULL")){
										 logi.logInfo "no value in anniversary "+anniversary
										 credits_row.remove("locked_amount")
									 }else{
									 credits_row.put("amount_reserved_for_anniversary",anniversary)
									 logi.logInfo "value in anniversary : "+anniversary
									 credits_row.remove("locked_amount")
									 }
									 }
						   logi.logInfo "printing inner credits_row hash "+credits_row.sort()
							   return credits_row.sort()
	}
	
	
	/*
	public LinkedHashMap md_void_invoice_m (String param)
	
		{
	
					ConnectDB db = new ConnectDB();
							
					//InputMethods im = new InputMethods()
							
					LinkedHashMap <String,String> void_invoice_m1 = new LinkedHashMap <String,String>()
					String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME,"//lit:get_acct_dunning_group_details_m[1]/acct_no[1]")
					String query = "SELECT ORIG_EVENT_NO AS invoice_transaction_id ,EVENT_NO AS void_transaction_id  FROM ARIACORE.ACCT_TRANS_VOID_HISTORY_VIEW WHERE ACCT_NO ="+acct_no
					ResultSet rs1 = db.executePlaQuery(query)
					ResultSetMetaData  md1 = rs1.getMetaData()
					int columns1 = md1.getColumnCount();
					for(int s=1; s<=columns1; ++s){
					while (rs1.next()){
					if(!rs1.getObject(s).toString().equals("null")){
						logi.logInfo("No value in "+rs1.getObject(s))
					}
					else
					
					{
					
						logi.logInfo ("Value inserting in hash map")
						void_invoice_m1.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
						logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
										
					
					  }
					
				return void_invoice_m1.sort()
	
	}*/
	
	
	/**
	* @param acct_hierarchy
	* @return HashMap -> contract general details
	*/
  LinkedHashMap md_get_instance_contract_gen_details_m(String acct_hierarchy)
	{
		logi.logInfo("----------> into md_get_instance_contract_den_details_m")
		ConnectDB dbb = new ConnectDB()
	
		InputMethods im=new InputMethods()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		
	   
		String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		logi.logInfo("acct_no"+acct_no)
		
		LinkedHashMap gen_det= new LinkedHashMap()
		int start=0
		
		String contract_no_count_q="Select count(TYPE_NO) from ariacore.ACCT_UNIVERSAL_CONTRACTS where  ACCT_NO="+acct_no
		String contract_no_count=dbb.executeQueryP2(contract_no_count_q)
		int flag=0
		String db_to_execute=null
		if(contract_no_count.toInteger() <=0)
		{
		String contract_no_count1_q="Select count(TYPE_NO) from ariacore.ACCT_MULTI_PLAN_CONTRACTS where  ACCT_NO="+acct_no
		String contract_no_count1=dbb.executeQueryP2(contract_no_count1_q)
		flag=1
		}
		
		if (flag==1)
		 db_to_execute="ariacore.ACCT_MULTI_PLAN_CONTRACTS"
		else
		 db_to_execute="ariacore.ACCT_UNIVERSAL_CONTRACTS"
		 
		 String gen_details_q="SELECT  APC.TYPE_NO,APC.CONTRACT_MONTHS AS LENGTH_MONTHS,APC.CREATE_COMMENTS,APC.UPDATE_COMMENTS,TO_CHAR(APC.CREATE_DATE,'YYYY-MM-DD') AS CREATE_DATE ,TO_CHAR(APC.UPDATE_DATE,'YYYY-MM-DD') AS UPDATE_DATE,TO_CHAR(APC.START_DATE,'YYYY-MM-DD') AS START_DATE,TO_CHAR(APC.END_DATE,'YYYY-MM-DD') AS END_DATE,APC.STATUS_CD AS STATUS_CODE FROM "+db_to_execute+" APC WHERE ACCT_NO ="+acct_no
		ResultSet rs = dbb.executePlaQuery(gen_details_q)
		ResultSetMetaData md = rs.getMetaData()
		int columns = md.getColumnCount()
		LinkedHashMap temp= new LinkedHashMap()
		while (rs.next())
		{
			
		   
			LinkedList l1= new LinkedList()
		
			for(int i=1; i<=columns; ++i)
			{
		
				if(rs.getObject(i)!=null)
				{
					l1.add(md.getColumnName(i).toLowerCase())
		
					int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
		
					logi.logInfo("frewww of :+"+md.getColumnName(i).toLowerCase()+" is :"+freq)
		
					if(freq>1)
					{
						 temp.put(md.getColumnName(i).toLowerCase()+"["+ freq +"]",rs.getObject(i))
			   
					}
		
					else
					temp.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
		
				}
			}
		   temp= temp.sort()
		   
			 }
		temp=temp.sort()
  return temp
		
	}
	
  
	 /**
	* @param acct_hierarchy1 - account hierarchy and sequence number
	* @return Hashmap -> contract details
	*/
  LinkedHashMap md_get_instance_contract_contract_details_m(String acct_hierarchy1)
	{
		logi.logInfo("------------>> into md_get_instance_contract_contract_details_m")
	   ConnectDB dbb = new ConnectDB()
	
	   String acct_hierarchy=acct_hierarchy1.split("#")[0]
	   String seq=acct_hierarchy1.split("#")[1]
	   
	   
		InputMethods im=new InputMethods()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	
		String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		logi.logInfo("acct_no"+acct_no)
		
		String contract_no_count_q="Select count(TYPE_NO) from ariacore.ACCT_UNIVERSAL_CONTRACTS where  ACCT_NO="+acct_no
		String contract_no_count=dbb.executeQueryP2(contract_no_count_q)
		int flag=0
		String db_to_execute=null
		if(contract_no_count.toInteger() <=0)
		{
		String contract_no_count1_q="Select count(TYPE_NO) from ariacore.ACCT_MULTI_PLAN_CONTRACTS where  ACCT_NO="+acct_no
		String contract_no_count1=dbb.executeQueryP2(contract_no_count1_q)
		flag=1
		}
		
		if (flag==1)
		 db_to_execute="ariacore.ACCT_MULTI_PLAN_CONTRACTS"
		else
		 db_to_execute="ariacore.ACCT_UNIVERSAL_CONTRACTS"
		 
		 
			 LinkedHashMap contract = new LinkedHashMap()
			 
		String plan_no_q="SELECT A.PNO FROM (SELECT PLAN_NO AS PNO,ROW_NUMBER() OVER (ORDER BY CLIENT_NO) AS SEQ FROM ARIACORE.PLAN_INSTANCE WHERE ACCT_NO="+acct_no +")A WHERE A.SEQ="+seq
		String plan_no=dbb.executeQueryP2(plan_no_q)
		String plan_inst_no_q="SELECT PLAN_INSTANCE_NO from ariacore.ACCT_MULTIPLAN_CONTRACT_INST where contract_no="+getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/contract_no[1]")+" and client_no="+clientNo
		//String plan_inst_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1,"+seq+"m", contract)
		String plan_inst_no=dbb.executeQueryP2(plan_inst_no_q)
		 String contract_details_q="SELECT DISTINCT PI.PLAN_INSTANCE_NO, CP.PLAN_NAME,PI.PLAN_INSTANCE_CDID AS CLIENT_PLAN_INSTANCE_ID,  PI.STATUS_CD AS plan_instance_status_cd FROM ARIACORE.CLIENT_PLAN CP JOIN ARIACORE.PLAN_INSTANCE PI ON CP.PLAN_NO=PI.PLAN_NO WHERE PI.CLIENT_NO="+clientNo+"  AND CP.PLAN_NO ="+plan_no+" AND PI.PLAN_INSTANCE_NO="+plan_inst_no
		 ResultSet rs1 = dbb.executePlaQuery(contract_details_q)
		ResultSetMetaData md1 = rs1.getMetaData()
		int columns1 = md1.getColumnCount()
		while (rs1.next())
	   {
			
			LinkedHashMap temp= new LinkedHashMap()
			LinkedList l1= new LinkedList()
		
			for(int i=1; i<=columns1; ++i)
			{
		
				if(rs1.getObject(i)!=null)
				{
					l1.add(md1.getColumnName(i).toLowerCase())
		
					int freq = Collections.frequency(l1,md1.getColumnName(i).toLowerCase())
		
					logi.logInfo("frewww of :+"+md1.getColumnName(i).toLowerCase()+" is :"+freq)
		
					if(freq>1)
					{
						 temp.put(md1.getColumnName(i).toLowerCase()+"["+ freq +"]",rs1.getObject(i))
			   
					}
		
					else
					temp.put(md1.getColumnName(i).toLowerCase(),rs1.getObject(i))
					
					
		
				}
			}
	   
			String a_q="select contract_no from ariacore.ACCT_MULTI_PLAN_CONTRACTS where acct_no="+acct_no
			String contract_no=dbb.executeQueryP2(a_q)
		   // String test_q="select count(PLAN_NO) from ARIACORE.PLAN_CONTRACT_RO_RATE_SCHED where CLIENT_NO ="+clientNo+" AND PLAN_NO="+plan_no
			String test_q="select count(*) from ariacore.ACCT_MULTIPLAN_CONTRACT_INST where contract_no="+contract_no+" and plan_instance_no="+plan_inst_no+" and ROLLOVER_SCHEDULE_NO is not null "
			String test=dbb.executeQueryP2(test_q)
			int a = test.toInteger()
			
			 test_q="select count(*) from ariacore.ACCT_MULTIPLAN_CONTRACT_INST where contract_no="+contract_no+"and plan_instance_no="+plan_inst_no+" and ROLLOVER_PLAN_NO is not null "
			 test=dbb.executeQueryP2(test_q)
			int b = test.toInteger()
			
		   
			if( a >0)
			{
				test_q="select PLAN_NO from ariacore.CLIENT_PLAN WHERE CLIENT_NO="+clientNo+"  and plan_no ="+plan_no
				String roll_plan_no=dbb.executeQueryP2(test_q)
				test_q="select client_plan_id from ariacore.CLIENT_PLAN WHERE CLIENT_NO="+clientNo+"  and plan_no ="+plan_no
				String rollover_client_plan_id=dbb.executeQueryP2(test_q)
			   if(roll_plan_no!=null)
			   temp.put("rollover_plan_no",roll_plan_no)
			   if(rollover_client_plan_id!=null)
			   temp.put("rollover_client_plan_id",rollover_client_plan_id)
				}
			   
			   
				   if(b>0)
				   {
					a_q="select ROLLOVER_SCHEDULE_NO from ariacore.ACCT_MULTIPLAN_CONTRACT_INST where contract_no="+contract_no+" and ROLLOVER_SCHEDULE_NO is not null"
					String roll_rs_no=dbb.executeQueryP2(a_q)
					a_q="select distinct(client_rate_schedule_id) from ariacore.all_rate_schedules where schedule_no="+roll_rs_no
					String rs_id=dbb.executeQueryP2(a_q)
								  
					if(roll_rs_no!=null)
					temp.put("rollover_rate_sched_no",roll_rs_no)
					if(rs_id!=null)
					temp.put("rollover_client_rate_sched_id",rs_id)
				   }
					
				   temp= temp.sort()
					   
			   
			logi.logInfo("OOOOOOOOPOPOPOPOOP :"+temp)
			temp=temp.sort()
			contract= temp
			
			
			 }
		
		return contract
		
	}
		
  
  /**
	* @param acct_hierarchy
	* @return Hashmap
	*/
  LinkedHashMap md_get_avail_plans_for_acct_m(String acct_hierarchy)
  {
	   logi.logInfo("------------>> into md_get_avail_plans_for_acct_m")
	   ConnectDB dbb = new ConnectDB()
	
			InputMethods im=new InputMethods()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	
		String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		logi.logInfo("acct_no"+acct_no)
		
		String avail_plans_q="SELECT PLAN_NO,CLIENT_PLAN_ID,PLAN_NAME,BILLING_IND from ariacore.client_plan where client_no="+clientNo+" and currency in ('usd') and status_cd in (1)"
		ResultSet rs1 = dbb.executePlaQuery(avail_plans_q)
		ResultSetMetaData md1 = rs1.getMetaData()
		int columns1 = md1.getColumnCount()
		while (rs1.next())
	   {
			
			LinkedHashMap temp= new LinkedHashMap()
			LinkedList l1= new LinkedList()
		
			for(int i=1; i<=columns1; ++i)
			{
		
				if(rs1.getObject(i)!=null)
				{
					l1.add(md1.getColumnName(i).toLowerCase())
		
					int freq = Collections.frequency(l1,md1.getColumnName(i).toLowerCase())
		
					logi.logInfo("frewww of :+"+md1.getColumnName(i).toLowerCase()+" is :"+freq)
		
					if(freq>1)
					{
						 temp.put(md1.getColumnName(i).toLowerCase()+"["+ freq +"]",rs1.getObject(i))
			   
					}
		
					else
					temp.put(md1.getColumnName(i).toLowerCase(),rs1.getObject(i))
					
					
		
				}
			}
				
  }
  }
		 
  /**
   * To get the invoice line items details for the modified plan unit instances using modify_acct_plan_unit_instances_m API
   * @param paramval
   * @return
   */
	def md_Modify_Unit_Inst_Invoice_Line_items_m(String s){
		logi.logInfo("********md_Modify_Unit_Inst_Invoice_Line_items_m Start**************")
		String invoice_no = md_getValFromNodeAndIndex_fornull_m("invoice_no,1")
		logi.logInfo("Printing Invoice No"+invoice_no)
		LinkedHashMap<String,LinkedHashMap> acct_plan_line_items_row = new LinkedHashMap<String,LinkedHashMap>();
		LinkedHashMap<String,LinkedHashMap> invoice = new LinkedHashMap<String,LinkedHashMap>();
		LinkedHashMap line_item = new LinkedHashMap();
		int loops=1;
			ConnectDB db = new ConnectDB()
			int counts= Integer.parseInt(db.executeQueryP2("SELECT count(*) from ariacore.gl_detail where invoice_no="+invoice_no).toString())
			for(int loop2=1;loop2<=counts;loop2++)
			{
				String  DB_ACCT_PLAN_LINE_ITEMS_ROW_m=""
				ResultSet resultSet =null;
				ResultSetMetaData md ;
  
				String Check_Credit=db.executeQueryP2("select ORIG_CREDIT_ID from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
				 logi.logInfo "Check Credit value"+Check_Credit
				 String check_surcharge=db.executeQueryP2("SELECT surcharge_rate_seq_no from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
				 String check_coupon=db.executeQueryP2("select orig_coupon_cd from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
				if(((Check_Credit.equalsIgnoreCase("null"))||(Check_Credit==null))&&((check_surcharge.equalsIgnoreCase("null"))||(check_surcharge==null))&&check_coupon.equalsIgnoreCase("null"))
				 {
					 
					   logi.logInfo "into the dummy datebase return part"
					   DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.line_no, a.plan_no,  a.plan_name,  a.client_plan_id,  a.service_no,  a.client_service_id,  a.service_name,  a.rate_per_unit,  a.date_range_start,  a.date_range_end,  a.description,  a.line_amount,  a.line_base_units,  a.proration_factor,  a.credit_coupon_code,  a.line_units FROM  (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no, gld.plan_no  AS plan_no, CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID   AS client_plan_id, gld.service_no   AS service_no,cs.client_service_id AS client_service_id, CS.SERVICE_NAME   AS service_name,  gld.usage_rate   AS rate_per_unit, TO_CHAR(gld.start_date, 'YYYY-MM-DD')        AS date_range_start, TO_CHAR(gld.end_date, 'YYYY-MM-DD')          AS date_range_end, gld.base_plan_units                          AS line_base_units,  gld.comments  AS description, gld.orig_coupon_cd  AS credit_coupon_code, gld.debit   AS line_amount,  gld.proration_factor,  gld.usage_units AS line_units FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.ALL_SERVICE ALS ON ALS.SERVICE_NO=GLD.SERVICE_NO AND ALS.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.PLAN_SERVICES PS ON PS.CLIENT_NO  =ALS.CLIENT_NO AND PS.SERVICE_NO=ALS.SERVICE_NO AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO        =PS.PLAN_NO  AND CP.client_no     =PS.client_no WHERE gld.invoice_no = "+invoice_no+")a where line_no="+loop2
					   if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
					   {resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
							 md = resultSet.getMetaData()}
					   int columns = md.getColumnCount();
				 }
				 
				 if(resultSet==null)
				 {
					 
					 Check_Credit=db.executeQueryP2("SELECT ORIG_CREDIT_ID from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+invoice_no).toString()
					 if(!Check_Credit.contains("null"))
					 {
						 DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT a.line_no, a.plan_no,  a.plan_name,  a.client_plan_id,  a.service_no,  a.client_service_id,  a.service_name,  a.date_range_start,a.date_range_end,  a.description,  a.line_amount,  a.line_base_units,  a.proration_factor,  a.credit_coupon_code,  a.line_units FROM  (SELECT ROW_NUMBER()OVER(ORDER BY gld.seq_num) AS line_no, gld.plan_no  AS plan_no, CP.plan_name AS plan_name, CP.CLIENT_PLAN_ID   AS client_plan_id, gld.service_no   AS service_no,cs.client_service_id AS client_service_id, CS.SERVICE_NAME   AS service_name,  gld.usage_rate   AS rate_per_unit, TO_CHAR(gld.start_date, 'YYYY-MM-DD')        AS date_range_start, TO_CHAR(gld.end_date, 'YYYY-MM-DD')          AS date_range_end, gld.base_plan_units                          AS line_base_units,  gld.comments  AS description, gld.orig_coupon_cd  AS credit_coupon_code, gld.debit   AS line_amount,  gld.proration_factor,  gld.usage_units AS line_units FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.GL ON GL.INVOICE_NO=GLD.INVOICE_NO AND GL.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.ALL_SERVICE ALS ON ALS.SERVICE_NO=GLD.SERVICE_NO AND ALS.CLIENT_NO=GLD.CLIENT_NO JOIN ARIACORE.PLAN_SERVICES PS ON PS.CLIENT_NO  =ALS.CLIENT_NO AND PS.SERVICE_NO=ALS.SERVICE_NO AND PS.PLAN_NO   =GLD.PLAN_NO  AND PS.CLIENT_NO =GLD.CLIENT_NO  JOIN ARIACORE.CLIENT_SERVICE CS  ON CS.SERVICE_NO= gld.SERVICE_NO  AND cs.client_no=ps.client_no  JOIN ARIACORE.CLIENT_PLAN CP  ON CP.PLAN_NO        =PS.PLAN_NO  AND CP.client_no     =PS.client_no WHERE gld.invoice_no = "+invoice_no+")a where line_no="+loop2
					   if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
					   {resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
					  md = resultSet.getMetaData()}
					 }
				   
				 }
				 
				 if(resultSet==null)
				 {
					 logi.logInfo "Check surcharge value"+check_surcharge
					 if(!check_surcharge.contains("null"))
					 {
						 DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT seq_num as line_no,gld.proration_factor,TO_CHAR(GLD.START_DATE, 'YYYY-MM-DD') AS DATE_RANGE_START,TO_CHAR(GLD.END_DATE, 'YYYY-MM-DD')   AS DATE_RANGE_END, gld.base_plan_units, gld.usage_units as units,gld.comments AS description,csp.rate_per_unit,gld.service_no, als.service_name,ser.client_service_id from ariacore.gl_detail gld join  ariacore.services  ser on ser.service_no=gld.service_no join ariacore.all_service als on als.service_no=ser.service_no join ariacore.client_surcharge cs on cs.alt_service_no_2_apply=ser.service_no join ariacore.CLIENT_SURCHARGE_PRICE csp on csp.surcharge_no=cs.surcharge_no where ser.is_surcharge=1 and gld.invoice_no= "+invoice_no+" and seq_num="+loop2
						 if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
						 {resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
						 md = resultSet.getMetaData()}
					 }
				 }
				 
				 if(resultSet==null)
				 {
					 logi.logInfo "Check Coupon value"+check_surcharge
					 if(!check_coupon.contains("null"))
					 {
						 DB_ACCT_PLAN_LINE_ITEMS_ROW_m="SELECT seq_num as line_no,gld.debit AS amount,gld.proration_factor,TO_CHAR(GLD.START_DATE, 'YYYY-MM-DD') AS DATE_RANGE_START,TO_CHAR(GLD.END_DATE, 'YYYY-MM-DD')   AS DATE_RANGE_END, gld.base_plan_units, gld.usage_units as units,gld.comments AS description,csp.rate_per_unit,gld.service_no, als.service_name,ser.client_service_id from ariacore.gl_detail gld join  ariacore.services  ser on ser.service_no=gld.service_no join ariacore.all_service als on als.service_no=ser.service_no join ariacore.client_surcharge cs on cs.alt_service_no_2_apply=ser.service_no join ariacore.CLIENT_SURCHARGE_PRICE csp on csp.surcharge_no=cs.surcharge_no where ser.is_surcharge=1 and gld.invoice_no= "+invoice_no+" and seq_num="+loop2
						 if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_ACCT_PLAN_LINE_ITEMS_ROW_m+")"))!=0)
						 {resultSet = db.executePlaQuery(DB_ACCT_PLAN_LINE_ITEMS_ROW_m)
						 md = resultSet.getMetaData()}
					 }
				 }
				 
				 int columns = md.getColumnCount();
				 while (resultSet.next())
				 {
  
					 for(int u=1; u<=columns; ++u)
					 {
						 if(resultSet.getObject(u).toString().equals("null")||resultSet.getObject(u).toString()==null){
							 logi.logInfo("No value in "+resultSet.getObject(u))
						 }else{
						 logi.logInfo("Value inserting in hash map")
						 line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
						 }
					 }
  
				 }
  
				 line_item.sort();
				 acct_plan_line_items_row.put("invoice_line_item_row["+(loops-1)+"]",line_item.sort())
				 line_item = new LinkedHashMap();
				 loops++;
			 }
		 acct_plan_line_items_row=acct_plan_line_items_row.sort()
		 return acct_plan_line_items_row
  
  
		 logi.logInfo("********md_Modify_Unit_Inst_Invoice_Line_items_m Ends**************")
		 }
  
 

	public HashMap getPlanServiceTiredPricing(String[] planNo,String Plan_instance_no)
	{
		 
		 logi.logInfo "getAssignsuppPlanServiceAndTieredPricing3" + planNo.length
		 ConnectDB database = new ConnectDB();
		 String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		 HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		 
		 String query = "SELECT plan_no,service_no,TIERED_PRICING_RULE_NO FROM ariacore.plan_services WHERE plan_no IN(SELECT plan_no FROM ariacore.plan_instance WHERE plan_instance_no = "+Plan_instance_no+")AND client_no   = "+client_no+" AND FULFILLMENT_BASED_IND = 0 AND service_no IN(SELECT service_no FROM ariacore.services WHERE custom_to_client_no = "+client_no+" AND usage_based= 0)ORDER BY plan_no,service_no"
		 int count = Integer.parseInt(database.executeQueryP2("SELECT count(plan_no) FROM ariacore.plan_services WHERE plan_no IN(SELECT plan_no FROM ariacore.plan_instance WHERE plan_instance_no = "+Plan_instance_no+")AND client_no   = "+client_no+" AND FULFILLMENT_BASED_IND = 0 AND service_no IN(SELECT service_no FROM ariacore.services WHERE custom_to_client_no = "+client_no+" AND usage_based= 0)ORDER BY plan_no,service_no"))
		 ResultSet resultSet = database.executeQuery(query)
  
		
		 HashMap<String,String> serviceHash
  
		 String[] plan = new String[count]
		 String[] service = new String[count]
		 String[] tieredPricing = new String[count]
  
		 int i= 0
		 while(resultSet.next()){
			 plan[i]=resultSet.getString(1)
			 service[i]=resultSet.getString(2)
			 tieredPricing[i]=resultSet.getString(3)
			 i++
		 }
		 String plan_no
		 int serviceCount=0
		 for(int planCount=0;planCount<plan.length;){
			 plan_no = plan[planCount]
			 serviceHash = new HashMap<String,String> ()
			 for(;serviceCount<service.length;serviceCount++){
				 if(plan_no.equalsIgnoreCase(plan[serviceCount])){
					 serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
					 planCount ++
				 }
				 else
					 break
			 }
			 resultHash.put(plan_no, serviceHash)
			 logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
		 }
		 logi.logInfo "Total Services" + resultHash.toString()
		 
		 return resultHash
		 
		 
	}
  public HashMap getPlanServiceTiredPricing1(String[] planNo,String Plan_instance_no)
  {
	   
	   logi.logInfo "getAssignsuppPlanServiceAndTieredPricing3" + planNo.length
	   ConnectDB database = new ConnectDB();
	   String client_no=Constant.mycontext.expand('${Properties#Client_No}')
	   HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
	   
	   String query = "SELECT plan_no,service_no,TIERED_PRICING_RULE_NO FROM ariacore.plan_services WHERE plan_no IN(SELECT plan_no FROM ariacore.plan_instance WHERE plan_instance_no = "+Plan_instance_no+")AND client_no   = "+client_no+" AND FULFILLMENT_BASED_IND=0 AND service_no IN(SELECT service_no FROM ariacore.services WHERE custom_to_client_no = "+client_no+" AND usage_based= 0 and is_setup=0 and IS_CANCELLATION=0)ORDER BY plan_no,service_no"
	   int count = Integer.parseInt(database.executeQueryP2("SELECT count(plan_no) FROM ariacore.plan_services WHERE plan_no IN(SELECT plan_no FROM ariacore.plan_instance WHERE plan_instance_no = "+Plan_instance_no+")AND client_no   = "+client_no+" AND FULFILLMENT_BASED_IND=0 AND service_no IN(SELECT service_no FROM ariacore.services WHERE custom_to_client_no = "+client_no+" AND usage_based= 0 and is_setup=0 and IS_CANCELLATION=0)ORDER BY plan_no,service_no"))
	   ResultSet resultSet = database.executeQuery(query)

	  
	   HashMap<String,String> serviceHash

	   String[] plan = new String[count]
	   String[] service = new String[count]
	   String[] tieredPricing = new String[count]

	   int i= 0
	   while(resultSet.next()){
		   plan[i]=resultSet.getString(1)
		   service[i]=resultSet.getString(2)
		   tieredPricing[i]=resultSet.getString(3)
		   i++
	   }
	   String plan_no
	   int serviceCount=0
	   for(int planCount=0;planCount<plan.length;){
		   plan_no = plan[planCount]
		   serviceHash = new HashMap<String,String> ()
		   for(;serviceCount<service.length;serviceCount++){
			   if(plan_no.equalsIgnoreCase(plan[serviceCount])){
				   serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
				   planCount ++
			   }
			   else
				   break
		   }
		   resultHash.put(plan_no, serviceHash)
		   logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
	   }
	   logi.logInfo "Total Services" + resultHash.toString()
	   
	   return resultHash
	   
	   
  }
  public String md_assign_supp_Proration_amount_m(String val)
  {
				   logi.logInfo("Proration Amount Assigning the supplemental plan")
				   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
				   DecimalFormat d = new DecimalFormat("0.##");
				   String[] plan_no
				   logi.logInfo("Client No ====>"+ clientNo)
				   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
				   logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
				   ConnectDB database = new ConnectDB();
				   SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
				   String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
				   String currentVirtualTime = database.executeQueryP2(Query);
				   logi.logInfo "Current Virtual Time : "+ currentVirtualTime
				   //Getting master plan instance
				   String Master_plan_instance_query = "SELECT MASTER_PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
				   String Master_plan_instance = database.executeQueryP2(Master_plan_instance_query);
				   logi.logInfo "Master Plan Instance No : "+ Master_plan_instance
				   def proration_factor = md_ProrationFactorCalculation_m(plan_instance_no)
				   d.format(proration_factor)
				   logi.logInfo "Proration Factor: "+ proration_factor
				   String plan_query = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
				   plan_no = database.executeQueryP2(plan_query);
				   logi.logInfo "Plan Number: "+ plan_no
				   String plan_query1 = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
				   String plan_no1 = database.executeQueryP2(plan_query1);
				   logi.logInfo "Plan Number1: "+ plan_no1
				   Map<String, HashMap<String,String>> hash
				   Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
				   Iterator iterator
				   hash = getPlanServiceTiredPricing(plan_no,plan_instance_no)
				   resultHash = new TreeMap<String, TreeMap<String,String>> ()
				   resultHash.putAll(hash)
				   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
				   iterator = resultHash.entrySet().iterator()
				   logi.logInfo "Before Key setting "
				   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
				   def totalAmount = 0
				   int counts = 0;
				   while (iterator.hasNext())
				   {
				   
								   Map.Entry pairs = (Map.Entry) iterator.next()
								   String planNo = pairs.getKey().toString()
								   logi.logInfo "Execution for the plan :  "+ planNo
								   
								   Map<String,String> valueMap = pairs.getValue()
								   Map<String,String> value = new TreeMap<String,String> ()
								   value.putAll(valueMap)
								   Iterator service = value.entrySet().iterator()
								   String unitsquery="SELECT PLAN_UNITS FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO=" + plan_instance_no
								   int noofUnits =database.executeQueryP2(unitsquery).toInteger()
								   logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo
								
								
								   
								   while(service.hasNext())
								   {
												   counts++
												   int ChargeMultipleAct = 0
												   Map.Entry servicePairs = (Map.Entry) service.next()
												   String serviceNo = servicePairs.getKey().toString()
												   logi.logInfo "Invoice calculation for a service :: "+serviceNo
												   String srvqry="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO ="+serviceNo+ "AND CLIENT_NO="+clientNo
												   String srvctype=database.executeQueryP2(srvqry)
												   logi.logInfo "Service type: "+srvctype
												   String flag_qry = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT' AND CLIENT_NO="+clientNo
												   String fla_val=database.executeQueryP2(flag_qry)
												   logi.logInfo "CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT : "+fla_val
												   String flag_qry1 = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT' AND CLIENT_NO="+clientNo
												   String fla_val1=database.executeQueryP2(flag_qry1)
												   logi.logInfo "MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT : "+fla_val1
												   if (srvctype=='AC' && fla_val== 'TRUE')
												   {
																   logi.logInfo "Entering into if loop"
																   if(fla_val1 == 'TRUE')
																   {
																				   ChargeMultipleAct = 1
																				   logi.logInfo "Entering into inner if loop"
																   }
																   else if(fla_val1 == 'FALSE')
																   {
																				   ChargeMultipleAct = 2
																				   logi.logInfo "Entering into inner clerif loop"
																   }
												   }
												   logi.logInfo "ChargeMultipleAct Value" +   ChargeMultipleAct
												   
												   int seqNo=1;
												   String tieredPricing = servicePairs.getValue().toString()
												   int tieredPricingInt = Integer.parseInt(tieredPricing)
												   logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
												   String altFee = "NoVal"
												   double serviceInvoice
												   
												   String query
												   switch(tieredPricingInt)
												   {
																   case 1:
																				   if(ChargeMultipleAct==2)
																				   {
																								   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
																								   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
																								   logi.logInfo "Service Amount Calculation" +   serviceInvoice
																								   totalAmount = totalAmount + serviceInvoice
																								   logi.logInfo "Total Amount Calculation" +   totalAmount
																								   break;
																								   
																				   }
																				   else if(ChargeMultipleAct==1)
																				   {
																								   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
																								   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
																								   logi.logInfo "Service Amount Calculation" +   serviceInvoice
																								   totalAmount = totalAmount + serviceInvoice
																								   logi.logInfo "Total Amount Calculation" +   totalAmount
																								   break;
																								   
																				   }
																				   else if(ChargeMultipleAct==0 && srvctype =='AC')
																				   {
																								  
																								   serviceInvoice = 0
																								   logi.logInfo "Service Amount Calculation" +   serviceInvoice
																								   totalAmount = totalAmount + serviceInvoice
																								   logi.logInfo "Total Amount Calculation" +   totalAmount
																								   break;
																								   
																				   }
																				   else if (srvctype != 'MN')
																				   {
																								   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and NRSR.RATE_SEQ_NO="+seqNo
																								   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
																								   double total = (serviceInvoice * proration_factor)
																								   logi.logInfo "total amount " + total
																								   totalAmount = totalAmount + total
																								   logi.logInfo "Total Amount Calculation" +   totalAmount
																								   break;
																				   }
																   case 2:
																								   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
																								   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
																								   double total = (serviceInvoice * proration_factor)
																								   logi.logInfo "total amount " + total
																								   totalAmount = totalAmount + total
																								   logi.logInfo "Total Amount Calculation" +   totalAmount
																								   break;
																   case 3:
																								   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
																								   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
																								   double total = (serviceInvoice * proration_factor)
																								   logi.logInfo "total amount " + total
																								   totalAmount = totalAmount + total
																								   logi.logInfo "Total Amount Calculation" +   totalAmount
																								   break;
												   }
								   }
								   logi.logInfo "Total Amount Calculation Final" +   totalAmount
				   }
				   logi.logInfo "services count "+counts
				   //Adding surcharge if mapped
				   String acct_no=database.executeQueryP2(" select acct_no  from ariacore.plan_instance  where  plan_instance_no = "+Master_plan_instance+" and client_no="+clientNo+"")
				   String sq="select surcharge_no from ariacore.plan_instance_surcharge  where  plan_instance_no = "+plan_instance_no+"  union   select surcharge_no from ariacore.acct_surcharge  where  acct_no="+acct_no
				   String cntQuery="select count(*) from ( select surcharge_no from ariacore.plan_instance_surcharge  where  plan_instance_no = "+plan_instance_no+"  union   select surcharge_no from ariacore.acct_surcharge  where  acct_no="+acct_no+")"
				   int count_surcharges=Integer.parseInt(database.executeQueryP2(cntQuery))
				   if(count_surcharges>0)
				   {
				   int surcharges=Integer.parseInt(database.executeQueryP2(sq))
				   def samt=database.executeQueryP2(" select rate_per_unit from ariacore.CLIENT_SURCHARGE_PRICE where surcharge_no="+surcharges)
				   def ts=samt.toInteger()*counts
				  totalAmount=(totalAmount.toDouble().round(2) )+ts
				   }
				   return d.format(totalAmount)
				   
  }
  public String md_cancel_proration_credit_amount_m(String val)
  {
	   logi.logInfo("Proration Amount Assigning the supplemental plan")
	   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
	   DecimalFormat d = new DecimalFormat("0.##");
	   String[] plan_no
	   logi.logInfo("Client No ====>"+ clientNo)
	   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
	   logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
	   ConnectDB database = new ConnectDB();
	   SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
	   String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
	   String currentVirtualTime = database.executeQueryP2(Query);
	   logi.logInfo "Current Virtual Time : "+ currentVirtualTime
	   //Getting master plan instance
	   String Master_plan_instance_query = "SELECT MASTER_PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   String Master_plan_instance = database.executeQueryP2(Master_plan_instance_query);
	   logi.logInfo "Master Plan Instance No : "+ Master_plan_instance
	   def proration_factor = md_ProrationFactorCalculation_m(Master_plan_instance)
	   d.format(proration_factor)
	   logi.logInfo "Proration Factor: "+ proration_factor
	   String plan_query = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   plan_no = database.executeQueryP2(plan_query);
	   logi.logInfo "Plan Number: "+ plan_no
	   String plan_query1 = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   String plan_no1 = database.executeQueryP2(plan_query1);
	   logi.logInfo "Plan Number1: "+ plan_no1
	   Map<String, HashMap<String,String>> hash
	   Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
	   Iterator iterator
	   hash = getPlanServiceTiredPricing1(plan_no,plan_instance_no)
	   resultHash = new TreeMap<String, TreeMap<String,String>> ()
	   resultHash.putAll(hash)
	   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	   iterator = resultHash.entrySet().iterator()
	   logi.logInfo "Before Key setting "
	   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	   def totalAmount = 0
	   while (iterator.hasNext())
	   {
	   
		   Map.Entry pairs = (Map.Entry) iterator.next()
		   String planNo = pairs.getKey().toString()
		   logi.logInfo "Execution for the plan :  "+ planNo
		   
		   Map<String,String> valueMap = pairs.getValue()
		   Map<String,String> value = new TreeMap<String,String> ()
		   value.putAll(valueMap)
		   Iterator service = value.entrySet().iterator()
		   String unitsquery="SELECT PLAN_UNITS FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO=" + plan_instance_no
		   int noofUnits =database.executeQueryP2(unitsquery).toInteger()
		   logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo
	   
		   while(service.hasNext())
		   {
			   int ChargeMultipleAct = 0
			   Map.Entry servicePairs = (Map.Entry) service.next()
			   String serviceNo = servicePairs.getKey().toString()
			   logi.logInfo "Invoice calculation for a service : "+serviceNo
			   String srvqry="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO ="+serviceNo+ "AND CLIENT_NO="+clientNo
			   String srvctype=database.executeQueryP2(srvqry)
			   logi.logInfo "Service type: "+srvctype
			   String flag_qry = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT' AND CLIENT_NO="+clientNo
			   String fla_val=database.executeQueryP2(flag_qry)
			   logi.logInfo "CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT : "+fla_val
			   String flag_qry1 = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT' AND CLIENT_NO="+clientNo
			   String fla_val1=database.executeQueryP2(flag_qry1)
			   logi.logInfo "MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT : "+fla_val1
			   if (srvctype=='AC' && fla_val== 'TRUE')
			   {
				   logi.logInfo "Entering into if loop"
				   if(fla_val1 == 'TRUE')
				   {
					   ChargeMultipleAct = 1
					   logi.logInfo "Entering into inner if loop"
				   }
				   else if(fla_val1 == 'FALSE')
				   {
					   ChargeMultipleAct = 2
					   logi.logInfo "Entering into inner clerif loop"
				   }
			   }
			   logi.logInfo "ChargeMultipleAct Value" +   ChargeMultipleAct
			   
			   int seqNo=1;
			   String tieredPricing = servicePairs.getValue().toString()
			   int tieredPricingInt = Integer.parseInt(tieredPricing)
			   logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
			   String altFee = "NoVal"
			   double serviceInvoice
			   
			   String query
			   switch(tieredPricingInt)
			   {
				   case 1:
					   if(ChargeMultipleAct==2)
					   {
						   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   logi.logInfo "Service Amount Calculation" +   serviceInvoice
						   totalAmount = totalAmount + serviceInvoice
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
						   
					   }
					   else if(ChargeMultipleAct==1)
					   {
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   logi.logInfo "Service Amount Calculation" +   serviceInvoice
						   totalAmount = totalAmount + serviceInvoice
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
						   
					   }
					   else if (srvctype != 'MN')
					   {
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and NRSR.RATE_SEQ_NO="+seqNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
					   }
				   case 2:
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
				   case 3:
						   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
			   }
		   }
		   logi.logInfo "Total Amount Calculation Final" +   totalAmount
	   }
	   
	   return d.format(totalAmount)
	   
  }
  public String md_cancel_invoice_amount_with_usage_m(String val)
  {
	   logi.logInfo("md_cancel_invoice_amount_with_usage_m")
	   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
	   DecimalFormat d = new DecimalFormat("0.##");
	   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
	   logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
	   ConnectDB database = new ConnectDB();
	   String Usage_Amount_query = "SELECT AMT FROM ARIACORE.USAGE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   def Usage_Amount= database.executeQueryP2(Usage_Amount_query);
	   def Cancel_plan_credit_amt = md_cancel_proration_credit_amount_m(val)
	   def total_amt = Usage_Amount.toDouble() - Cancel_plan_credit_amt.toDouble()
	   return d.format(total_amt)
	   
  }
  
  /**
	* To get the proration details for the updated plan units using update_acct_plan_unit_instance_m API
	* @param paramval
	* @return
	*/
 public String md_Proration_update_unit_inst_m(String val)
  {
				   logi.logInfo("********md_Proration_update_unit_inst_m Start**************")
				   VerificationMethods vm =new VerificationMethods()
				   DecimalFormat d = new DecimalFormat("0.##");
				   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
				   logi.logInfo("Client No =>"+ clientNo)
				   String plan_unit_inst = vm.getValueFromRequest("update_acct_pln_unit_inst_m", "//*/plan_unit_inst_no").toString()
				   logi.logInfo("plan_unit_inst_no =>"+ plan_unit_inst)
				   ArrayList<String> plan_unit_inst_service = new ArrayList<String>()
				   String servicesCount = vm.getNodeCountFromRequest("update_acct_pln_unit_inst_m","//*/plan_unit_instance_services[1]/lit:plan_unit_instance_services_row").toString()
				   logi.logInfo("servicesCount =>"+ servicesCount)
				   for(int i=1;i<=servicesCount.toInteger();i++){
								  logi.logInfo("//*/plan_unit_instance_services[1]/lit:plan_unit_instance_services_row["+i+"]/lit:service_no[1]")
												plan_unit_inst_service[i-1] = vm.getValueFromRequest("update_acct_pln_unit_inst_m", "//*/plan_unit_instance_services[1]/lit:plan_unit_instance_services_row["+i+"]/lit:service_no[1]").toString()
				   }
				   logi.logInfo("plan_unit_inst_service =>"+ plan_unit_inst_service.toString())
				   
				   String plan_inst_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
				   logi.logInfo("PlanIntsnaceNo ====>"+ plan_inst_no)
				   
				   ConnectDB db = new ConnectDB()
				  /* String query_PI = "SELECT plan_instance_no from ariacore.PLAN_INST_UNIT_INSTANCES where plan_unit_instance_no = "+plan_unit_inst
				   String plan_inst_no = db.executeQueryP2(query_PI)*/
				   
				   String query_MPI = "SELECT master_plan_instance_no from ariacore.plan_instance where plan_instance_no = "+plan_inst_no
				   String MPI = db.executeQueryP2(query_MPI)
				   def proration_factor = md_ProrationFactorCalculation_m(plan_inst_no)
				   logi.logInfo("Proration factor value =>"+ proration_factor)
				   d.format(proration_factor)
				   String query_PlanNo = "SELECT plan_no from ariacore.plan_instance where plan_instance_no = "+plan_inst_no
				   String[] planNo = db.executeQueryP2(query_PlanNo)
				   logi.logInfo("PLan number returned from DB =>"+ planNo.toString())
				   String plan_query1 = "SELECT plan_no from ariacore.plan_instance where plan_instance_no = "+plan_inst_no
				   String plan_no1 = db.executeQueryP2(plan_query1);
				   logi.logInfo "Plan Number1: "+ plan_no1
				   Map<String, HashMap<String,String>> serviceHash
				   logi.logInfo("Declaration Service hash")
				   Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
				   logi.logInfo("Declaration resultHash")
				   Iterator iterator
				   logi.logInfo("Declaration iterator")
				   serviceHash = getPlanServiceTiredPricingFulfillment(planNo, plan_inst_no)
				   resultHash = new TreeMap<String, TreeMap<String,String>> ()
				   resultHash.putAll(serviceHash)
				   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
				   iterator = resultHash.entrySet().iterator()
				   logi.logInfo "Before Key setting "
				   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
				   def totalAmount = 0
				   int counts=0
				   while (iterator.hasNext())
				   {
				   
								   Map.Entry pairs = (Map.Entry) iterator.next()
								   String plan_No = pairs.getKey().toString()
								   logi.logInfo "Execution for the plan :  "+ plan_No
								   
								   Map<String,String> valueMap = pairs.getValue()
								   Map<String,String> value = new TreeMap<String,String> ()
								   value.putAll(valueMap)
								   Iterator service = value.entrySet().iterator()
								   String unitsquery="SELECT PLAN_UNITS FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO=" + plan_inst_no
								   int noofUnits =db.executeQueryP2(unitsquery).toInteger()
								   logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +plan_No
				   
								   while(service.hasNext())
								   {
												  
												   counts++
												   int ChargeMultipleAct = 0
												   Map.Entry servicePairs = (Map.Entry) service.next()
												   String serviceNo = servicePairs.getKey().toString()
												   logi.logInfo "Invoice calculation for a service : "+serviceNo
												   if(plan_unit_inst_service.contains(serviceNo)==true)
												   {
												   String srvqry="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO ="+serviceNo+ "AND CLIENT_NO="+clientNo
												  String srvctype=db.executeQueryP2(srvqry)
												   logi.logInfo "Service type: "+srvctype
												   String flag_qry = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT' AND CLIENT_NO="+clientNo
												   String fla_val=db.executeQueryP2(flag_qry)
												   logi.logInfo "CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT : "+fla_val
												   String flag_qry1 = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT' AND CLIENT_NO="+clientNo
												   String fla_val1=db.executeQueryP2(flag_qry1)
												   logi.logInfo "MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT : "+fla_val1
												   if (srvctype=='AC' && fla_val== 'TRUE')
												   {
																   logi.logInfo "Entering into if loop"
																   if(fla_val1 == 'TRUE')
																   {
																				   ChargeMultipleAct = 1
																				   logi.logInfo "Entering into inner if loop"
																   }
																   else if(fla_val1 == 'FALSE')
																   {
																				   ChargeMultipleAct = 2
																				   logi.logInfo "Entering into inner clerif loop"
																   }
												   }
												   logi.logInfo "ChargeMultipleAct Value" +   ChargeMultipleAct
												   
												   int seqNo=1;
												   String tieredPricing = servicePairs.getValue().toString()
												   int tieredPricingInt = Integer.parseInt(tieredPricing)
												   logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
												   String altFee = "NoVal"
												   double serviceInvoice
												   
												   String query
												   switch(tieredPricingInt)
												   {
																   case 1:
																				   if(ChargeMultipleAct==2)
																				   {
																								   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
																								   serviceInvoice = Integer.parseInt(db.executeQueryP2(query))
																								   logi.logInfo "Service Amount Calculation" +   serviceInvoice
																								   totalAmount = totalAmount + serviceInvoice
																								   logi.logInfo "Total Amount Calculation" +   totalAmount
																								   break;
																								   
																				   }
																				   else if(ChargeMultipleAct==1)
																				   {
																								   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
																								   serviceInvoice = Integer.parseInt(db.executeQueryP2(query))
																								   logi.logInfo "Service Amount Calculation" +   serviceInvoice
																								   totalAmount = totalAmount + serviceInvoice
																								   logi.logInfo "Total Amount Calculation" +   totalAmount
																								   break;
																								   
																				   }
																				   else if (srvctype != 'MN')
																				   {
																								   
																								   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and NRSR.RATE_SEQ_NO="+seqNo
																								   serviceInvoice = Integer.parseInt(db.executeQueryP2(query))
																								   double total = (serviceInvoice * proration_factor)
																								   logi.logInfo "total amount " + total
																								   totalAmount = totalAmount + total
																								   logi.logInfo "Total Amount Calculation" +   totalAmount
																								   break;
																				   }
																   case 2:
																								   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
																								   serviceInvoice = Integer.parseInt(db.executeQueryP2(query))
																								   double total = (serviceInvoice * proration_factor)
																								   logi.logInfo "total amount " + total
																								   totalAmount = totalAmount + total
																								   logi.logInfo "Total Amount Calculation" +   totalAmount
																								   break;
																   case 3:
																								   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
																								   serviceInvoice = Integer.parseInt(db.executeQueryP2(query))
																								   double total = (serviceInvoice * proration_factor)
																								   logi.logInfo "total amount " + total
																								   totalAmount = totalAmount + total
																								   logi.logInfo "Total Amount Calculation" +   totalAmount
																								   break;
												   }
								   }
								   }
								   
								   logi.logInfo "Total Amount Calculation Final" +   totalAmount
				   }
				   logi.logInfo "Total Amount Calculation Again" +   totalAmount.toDouble().round(2)
				   logi.logInfo "services count "+counts
				   //Adding surcharge if mapped
				   String acct_no=db.executeQueryP2(" select acct_no  from ariacore.plan_instance  where  plan_instance_no = "+MPI+" and client_no="+clientNo+"")
				   String sq="select surcharge_no from ariacore.plan_instance_surcharge  where  plan_instance_no = "+plan_inst_no+"  union   select surcharge_no from ariacore.acct_surcharge  where  acct_no="+acct_no
								   String cntQuery="select count(*) from ( select surcharge_no from ariacore.plan_instance_surcharge  where  plan_instance_no = "+plan_inst_no+"  union   select surcharge_no from ariacore.acct_surcharge  where  acct_no="+acct_no+")"
				   int count_surcharges=Integer.parseInt(db.executeQueryP2(cntQuery))
				   if(count_surcharges>0)
				   {
				   int surcharges=Integer.parseInt(db.executeQueryP2(sq))
				   def samt=db.executeQueryP2(" select rate_per_unit from ariacore.CLIENT_SURCHARGE_PRICE where surcharge_no="+surcharges)
				   def ts=samt.toInteger()*counts
				   totalAmount=(totalAmount.toDouble().round(2) )+ts
				   }
				   
				   
				   return d.format(totalAmount)
				   logi.logInfo("********md_Proration_update_unit_inst_m Ends**************")
				   
  }
 
  public HashMap getPlanServiceTiredPricingFulfillment(String[] planNo,String Plan_instance_no)
  {
		  
		  logi.logInfo "getAssignsuppPlanServiceAndTieredPricing3" + planNo.length
		  ConnectDB database = new ConnectDB();
		  String client_no=Constant.mycontext.expand('${Properties#Client_No}')
		  HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
		  
		  String query = "SELECT plan_no,service_no,TIERED_PRICING_RULE_NO FROM ariacore.plan_services WHERE plan_no IN(SELECT plan_no FROM ariacore.plan_instance WHERE plan_instance_no = "+Plan_instance_no+" and fulfillment_based_ind = 1)AND client_no   = "+client_no+" AND service_no IN(SELECT service_no FROM ariacore.services WHERE custom_to_client_no = "+client_no+" AND usage_based= 0)ORDER BY plan_no,service_no"
		  int count = Integer.parseInt(database.executeQueryP2("SELECT count(plan_no) FROM ariacore.plan_services WHERE plan_no IN(SELECT plan_no FROM ariacore.plan_instance WHERE plan_instance_no = "+Plan_instance_no+" and fulfillment_based_ind = 1)AND client_no   = "+client_no+" AND service_no IN(SELECT service_no FROM ariacore.services WHERE custom_to_client_no = "+client_no+" AND usage_based= 0)ORDER BY plan_no,service_no"))
		  ResultSet resultSet = database.executeQuery(query)

	  
		  HashMap<String,String> serviceHash

		  String[] plan = new String[count]
		  String[] service = new String[count]
		  String[] tieredPricing = new String[count]

		  int i= 0
		  while(resultSet.next()){
				 plan[i]=resultSet.getString(1)
				 service[i]=resultSet.getString(2)
				 tieredPricing[i]=resultSet.getString(3)
				 i++
		  }
		  String plan_no
		  int serviceCount=0
		  for(int planCount=0;planCount<plan.length;){
				 plan_no = plan[planCount]
				 serviceHash = new HashMap<String,String> ()
				 for(;serviceCount<service.length;serviceCount++){
					   if(plan_no.equalsIgnoreCase(plan[serviceCount])){
							  serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
							  planCount ++
					   }
					   else
							  break
				 }
				 resultHash.put(plan_no, serviceHash)
				 logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
		  }
		  logi.logInfo "Total Services" + resultHash.toString()
		  
		  return resultHash
			  
  }
  
  
  /**
	* To get the invoice line items details for the modified plan unit instances using modify_acct_plan_unit_instance_m API
	* @param paramval
	* @return
	*/
  public String md_cancel_proration_credit_amount_fulfilment_m(String val)
  {
	   logi.logInfo("Proration Amount Assigning the supplemental plan")
	   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
	   DecimalFormat d = new DecimalFormat("0.##");
	   String[] plan_no
	   logi.logInfo("Client No ====>"+ clientNo)
	   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
	   logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
	   ConnectDB database = new ConnectDB();
	   SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
	   String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
	   String currentVirtualTime = database.executeQueryP2(Query);
	   logi.logInfo "Current Virtual Time : "+ currentVirtualTime
	   //Getting master plan instance
	   String Master_plan_instance_query = "SELECT MASTER_PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   String Master_plan_instance = database.executeQueryP2(Master_plan_instance_query);
	   logi.logInfo "Master Plan Instance No : "+ Master_plan_instance
	   def proration_factor = md_ProrationFactorCalculation_m(plan_instance_no)
	   d.format(proration_factor)
	   logi.logInfo "Proration Factor: "+ proration_factor
	   String plan_query = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   plan_no = database.executeQueryP2(plan_query);
	   logi.logInfo "Plan Number: "+ plan_no
	   String plan_query1 = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   String plan_no1 = database.executeQueryP2(plan_query1);
	   logi.logInfo "Plan Number1: "+ plan_no1
	   Map<String, HashMap<String,String>> hash
	   Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
	   Iterator iterator
	   hash = getPlanServiceTiredPricing1(plan_no,plan_instance_no)
	   resultHash = new TreeMap<String, TreeMap<String,String>> ()
	   resultHash.putAll(hash)
	   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	   iterator = resultHash.entrySet().iterator()
	   logi.logInfo "Before Key setting "
	   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	   def totalAmount = 0
	   while (iterator.hasNext())
	   {
	   
		   Map.Entry pairs = (Map.Entry) iterator.next()
		   String planNo = pairs.getKey().toString()
		   logi.logInfo "Execution for the plan :  "+ planNo
		   
		   Map<String,String> valueMap = pairs.getValue()
		   Map<String,String> value = new TreeMap<String,String> ()
		   value.putAll(valueMap)
		   Iterator service = value.entrySet().iterator()
		   String unitsquery="SELECT PLAN_UNITS FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO=" + plan_instance_no
		   int noofUnits =database.executeQueryP2(unitsquery).toInteger()
		   logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo
	   
		   while(service.hasNext())
		   {
			   int ChargeMultipleAct = 0
			   Map.Entry servicePairs = (Map.Entry) service.next()
			   String serviceNo = servicePairs.getKey().toString()
			   logi.logInfo "Invoice calculation for a service : "+serviceNo
			   String srvqry="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO ="+serviceNo+ "AND CLIENT_NO="+clientNo
			   String srvctype=database.executeQueryP2(srvqry)
			   logi.logInfo "Service type: "+srvctype
			   String flag_qry = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT' AND CLIENT_NO="+clientNo
			   String fla_val=database.executeQueryP2(flag_qry)
			   logi.logInfo "CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT : "+fla_val
			   String flag_qry1 = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT' AND CLIENT_NO="+clientNo
			   String fla_val1=database.executeQueryP2(flag_qry1)
			   logi.logInfo "MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT : "+fla_val1
			   if (srvctype=='AC' && fla_val== 'TRUE')
			   {
				   logi.logInfo "Entering into if loop"
				   if(fla_val1 == 'TRUE')
				   {
					   ChargeMultipleAct = 1
					   logi.logInfo "Entering into inner if loop"
				   }
				   else if(fla_val1 == 'FALSE')
				   {
					   ChargeMultipleAct = 2
					   logi.logInfo "Entering into inner clerif loop"
				   }
			   }
			   logi.logInfo "ChargeMultipleAct Value" +   ChargeMultipleAct
			   
			   int seqNo=1;
			   String tieredPricing = servicePairs.getValue().toString()
			   int tieredPricingInt = Integer.parseInt(tieredPricing)
			   logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
			   String altFee = "NoVal"
			   double serviceInvoice
			   
			   String query
			   switch(tieredPricingInt)
			   {
				   case 1:
					   if(ChargeMultipleAct==2)
					   {
						   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   logi.logInfo "Service Amount Calculation" +   serviceInvoice
						   totalAmount = totalAmount + serviceInvoice
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
						   
					   }
					   else if(ChargeMultipleAct==1)
					   {
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   logi.logInfo "Service Amount Calculation" +   serviceInvoice
						   totalAmount = totalAmount + serviceInvoice
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
						   
					   }
					   else if (srvctype != 'MN')
					   {
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and NRSR.RATE_SEQ_NO="+seqNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
					   }
				   case 2:
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
				   case 3:
						   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
			   }
		   }
		   logi.logInfo "Total Amount Calculation Final" +   totalAmount
	   }
	   
	   return d.format(totalAmount)
	   
  }
	
  public String md_upgrade_plan_units_m(String val)
  {
	   logi.logInfo("Proration Amount Assigning the supplemental plan")
	   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
	   DecimalFormat d = new DecimalFormat("0.##");
	   String[] plan_no
	   logi.logInfo("Client No ====>"+ clientNo)
	   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
	   logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
	   ConnectDB database = new ConnectDB();
	   SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
	   String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
	   String currentVirtualTime = database.executeQueryP2(Query);
	   logi.logInfo "Current Virtual Time : "+ currentVirtualTime
	   //Getting master plan instance
	   String Master_plan_instance_query = "SELECT MASTER_PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   String Master_plan_instance = database.executeQueryP2(Master_plan_instance_query);
	   logi.logInfo "Master Plan Instance No : "+ Master_plan_instance
	   def proration_factor = md_ProrationFactorCalculation_m(plan_instance_no)
	   d.format(proration_factor)
	   logi.logInfo "Proration Factor: "+ proration_factor
	   String plan_query = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   plan_no = database.executeQueryP2(plan_query);
	   logi.logInfo "Plan Number: "+ plan_no
	   String plan_query1 = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   String plan_no1 = database.executeQueryP2(plan_query1);
	   logi.logInfo "Plan Number1: "+ plan_no1
	   Map<String, HashMap<String,String>> hash
	   Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
	   Iterator iterator
	   hash = getPlanServiceTiredPricing(plan_no,plan_instance_no)
	   resultHash = new TreeMap<String, TreeMap<String,String>> ()
	   resultHash.putAll(hash)
	   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	   iterator = resultHash.entrySet().iterator()
	   logi.logInfo "Before Key setting "
	   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	   def totalAmount = 0
	   int ActivationUnits
	   while (iterator.hasNext())
	   {
	   
		   Map.Entry pairs = (Map.Entry) iterator.next()
		   String planNo = pairs.getKey().toString()
		   logi.logInfo "Execution for the plan :  "+ planNo
		   
		   Map<String,String> valueMap = pairs.getValue()
		   Map<String,String> value = new TreeMap<String,String> ()
		   value.putAll(valueMap)
		   Iterator service = value.entrySet().iterator()
		   String newunitsquery="select new_recurring_factor from ariacore.PLAN_INSTANCE_PLAN_HIST where client_no="+clientNo+" and PLAN_INSTANCE_NO="+plan_instance_no+" and old_recurring_factor is not null"
		   int NewnoofUnits =database.executeQueryP2(newunitsquery).toInteger()
		   logi.logInfo "NewnoofUnits : "+NewnoofUnits+ " plan : " +planNo
		   String oldunitsquery="select old_recurring_factor from ariacore.PLAN_INSTANCE_PLAN_HIST where client_no="+clientNo+" and PLAN_INSTANCE_NO="+plan_instance_no+" and old_recurring_factor is not null"
		   int OldnoofUnits =database.executeQueryP2(oldunitsquery).toInteger()
		   logi.logInfo "OldnoofUnits : "+OldnoofUnits+ " plan : " +planNo
		   if(NewnoofUnits>OldnoofUnits)
		   {
				ActivationUnits =NewnoofUnits.toInteger()-OldnoofUnits.toInteger()
		   }
		   else
		   {
				ActivationUnits = 0;
				
		   }
		   logi.logInfo "no_of_unitsssss : "+ActivationUnits+ " plan : " +planNo
		   while(service.hasNext())
		   {
			   int ChargeMultipleAct = 0
			   Map.Entry servicePairs = (Map.Entry) service.next()
			   String serviceNo = servicePairs.getKey().toString()
			   logi.logInfo "Invoice calculation for a service : "+serviceNo
			   String srvqry="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO ="+serviceNo+ "AND CLIENT_NO="+clientNo
			   String srvctype=database.executeQueryP2(srvqry)
			   logi.logInfo "Service type: "+srvctype
			   String flag_qry = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT' AND CLIENT_NO="+clientNo
			   String fla_val=database.executeQueryP2(flag_qry)
			   logi.logInfo "CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT : "+fla_val
			   String flag_qry1 = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT' AND CLIENT_NO="+clientNo
			   String fla_val1=database.executeQueryP2(flag_qry1)
			   logi.logInfo "MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT : "+fla_val1
			   if (srvctype=='AC' && fla_val== 'TRUE')
			   {
				   logi.logInfo "Entering into if loop"
				   if(fla_val1 == 'TRUE')
				   {
					   ChargeMultipleAct = 1
					   logi.logInfo "Entering into inner if loop"
				   }
				   else if(fla_val1 == 'FALSE')
				   {
					   ChargeMultipleAct = 2
					   logi.logInfo "Entering into inner clerif loop"
				   }
			   }
			   logi.logInfo "ChargeMultipleAct Value" +   ChargeMultipleAct
			   
			   int seqNo=1;
			   String tieredPricing = servicePairs.getValue().toString()
			   int tieredPricingInt = Integer.parseInt(tieredPricing)
			   logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
			   String altFee = "NoVal"
			   double serviceInvoice
			   
			   String query
			   switch(tieredPricingInt)
			   {
				   case 1:
					   if(ChargeMultipleAct==2)
					   {
						   query = "select rate_per_unit * 0 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   logi.logInfo "Service Amount Calculation" +   serviceInvoice
						   totalAmount = totalAmount + serviceInvoice
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
						   
					   }
					   else if(ChargeMultipleAct==1)
					   {
						   query = "select rate_per_unit * "+ActivationUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   logi.logInfo "Service Amount Calculation" +   serviceInvoice
						   totalAmount = totalAmount + serviceInvoice
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
						   
					   }
					   else if (srvctype != 'MN')
					   {
						   query = "select rate_per_unit * "+NewnoofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and NRSR.RATE_SEQ_NO="+seqNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
					   }
				   case 2:
						   query = "select rate_per_unit * "+NewnoofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
				   case 3:
						   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
			   }
		   }
		   logi.logInfo "Total Amount Calculation Final" +   totalAmount
	   }
	   
	   return d.format(totalAmount)
	   
  }
  
  public String md_update_plan_unit_credit_m(String val)
  {
	   logi.logInfo("Proration Amount Assigning the supplemental plan")
	   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
	   DecimalFormat d = new DecimalFormat("0.##");
	   DecimalFormat d1 = new DecimalFormat("#.##");
	   String[] plan_no
	   logi.logInfo("Client No ====>"+ clientNo)
	   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
	   logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
	   ConnectDB database = new ConnectDB();
	   SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
	   String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
	   String currentVirtualTime = database.executeQueryP2(Query);
	   logi.logInfo "Current Virtual Time : "+ currentVirtualTime
	   //Getting master plan instance
	   String Master_plan_instance_query = "SELECT MASTER_PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   String Master_plan_instance = database.executeQueryP2(Master_plan_instance_query);
	   logi.logInfo "Master Plan Instance No : "+ Master_plan_instance
	   def proration_factor = md_ProrationFactorCalculation_m(plan_instance_no)
	   d1.format(proration_factor)
	   logi.logInfo "Proration Factor: "+ proration_factor
	   String plan_query = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   plan_no = database.executeQueryP2(plan_query);
	   logi.logInfo "Plan Number: "+ plan_no
	   String plan_query1 = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   String plan_no1 = database.executeQueryP2(plan_query1);
	   logi.logInfo "Plan Number1: "+ plan_no1
	   Map<String, HashMap<String,String>> hash
	   Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
	   Iterator iterator
	   hash = getPlanServiceTiredPricing1(plan_no,plan_instance_no)
	   resultHash = new TreeMap<String, TreeMap<String,String>> ()
	   resultHash.putAll(hash)
	   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	   iterator = resultHash.entrySet().iterator()
	   logi.logInfo "Before Key setting "
	   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	   def totalAmount = 0
	   while (iterator.hasNext())
	   {
	   
		   Map.Entry pairs = (Map.Entry) iterator.next()
		   String planNo = pairs.getKey().toString()
		   logi.logInfo "Execution for the plan :  "+ planNo
		   
		   Map<String,String> valueMap = pairs.getValue()
		   Map<String,String> value = new TreeMap<String,String> ()
		   value.putAll(valueMap)
		   Iterator service = value.entrySet().iterator()
		   String newunitsquery="select new_recurring_factor from ariacore.PLAN_INSTANCE_PLAN_HIST where client_no="+clientNo+" and PLAN_INSTANCE_NO="+plan_instance_no+" and old_recurring_factor is not null"
		   int NewnoofUnits =database.executeQueryP2(newunitsquery).toInteger()
		   logi.logInfo "NewnoofUnits : "+NewnoofUnits+ " plan : " +planNo
		   String oldunitsquery="select old_recurring_factor from ariacore.PLAN_INSTANCE_PLAN_HIST where client_no="+clientNo+" and PLAN_INSTANCE_NO="+plan_instance_no+" and old_recurring_factor is not null"
		   int noofUnits =database.executeQueryP2(oldunitsquery).toInteger()
		   logi.logInfo "OldnoofUnits : "+noofUnits+ " plan : " +planNo
		   //int noofUnits = NewnoofUnits.toInteger()-OldnoofUnits.toInteger()
		   logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo
	   
		   while(service.hasNext())
		   {
			   int ChargeMultipleAct = 0
			   Map.Entry servicePairs = (Map.Entry) service.next()
			   String serviceNo = servicePairs.getKey().toString()
			   logi.logInfo "Invoice calculation for a service : "+serviceNo
			   String srvqry="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO ="+serviceNo+ "AND CLIENT_NO="+clientNo
			   String srvctype=database.executeQueryP2(srvqry)
			   logi.logInfo "Service type: "+srvctype
			   String flag_qry = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT' AND CLIENT_NO="+clientNo
			   String fla_val=database.executeQueryP2(flag_qry)
			   logi.logInfo "CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT : "+fla_val
			   String flag_qry1 = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT' AND CLIENT_NO="+clientNo
			   String fla_val1=database.executeQueryP2(flag_qry1)
			   logi.logInfo "MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT : "+fla_val1
			   if (srvctype=='AC' && fla_val== 'TRUE')
			   {
				   logi.logInfo "Entering into if loop"
				   if(fla_val1 == 'TRUE')
				   {
					   ChargeMultipleAct = 1
					   logi.logInfo "Entering into inner if loop"
				   }
				   else if(fla_val1 == 'FALSE')
				   {
					   ChargeMultipleAct = 2
					   logi.logInfo "Entering into inner clerif loop"
				   }
			   }
			   logi.logInfo "ChargeMultipleAct Value" +   ChargeMultipleAct
			   
			   int seqNo=1;
			   String tieredPricing = servicePairs.getValue().toString()
			   int tieredPricingInt = Integer.parseInt(tieredPricing)
			   logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
			   String altFee = "NoVal"
			   double serviceInvoice
			   
			   String query
			   switch(tieredPricingInt)
			   {
				   case 1:
					   if(ChargeMultipleAct==2)
					   {
						   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   logi.logInfo "Service Amount Calculation" +   serviceInvoice
						   totalAmount = totalAmount + serviceInvoice
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
						   
					   }
					   else if(ChargeMultipleAct==1)
					   {
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   logi.logInfo "Service Amount Calculation" +   serviceInvoice
						   totalAmount = totalAmount + serviceInvoice
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
						   
					   }
					   else if (srvctype != 'MN')
					   {
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and NRSR.RATE_SEQ_NO="+seqNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
					   }
				   case 2:
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
				   case 3:
						   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
			   }
		   }
		   logi.logInfo "Total Amount Calculation Final" +   totalAmount
	   }
	   
	   return d.format(totalAmount)
	   
  }
  public String md_update_plan_units_prorated_amount_m(String val)
  {
				   logi.logInfo("md_cancel_invoice_amount_with_usage_m")
				   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
				   DecimalFormat d = new DecimalFormat("0.##");
				   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
				   logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
				   ConnectDB db = new ConnectDB();
				   def Prorated_charge_amount = md_upgrade_plan_units_m(val)
				   def Prorated_credit_amount = md_update_plan_unit_credit_m(val)
				   def total_amt = Prorated_charge_amount.toDouble() - Prorated_credit_amount.toDouble()
				   if(total_amt<=0)
				   {
												total_amt = 0
				   }
				   logi.logInfo("total_amt ====>"+ total_amt)
				  
				   String plan_query = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
				  String  plan_no = db.executeQueryP2(plan_query);
				   logi.logInfo "Plan Number: "+ plan_no
								int counts=Integer.parseInt(db.executeQueryP2("select count( distinct service_no)  FROM ariacore.plan_services where plan_no="+plan_no))
				   //Adding surcharge if mapped
				   String acct_no=db.executeQueryP2(" select acct_no  from ariacore.plan_instance  where  plan_instance_no = "+plan_instance_no+" and client_no="+clientNo+"")
				   String sq="select surcharge_no from ariacore.plan_instance_surcharge  where  plan_instance_no = "+plan_instance_no+"  union   select surcharge_no from ariacore.acct_surcharge  where  acct_no="+acct_no
												  String cntQuery="select count(*) from ( select surcharge_no from ariacore.plan_instance_surcharge  where  plan_instance_no = "+plan_instance_no+"  union   select surcharge_no from ariacore.acct_surcharge  where  acct_no="+acct_no+")"
				   int count_surcharges=Integer.parseInt(db.executeQueryP2(cntQuery))
				   if(count_surcharges>0)
				   {
				   int surcharges=Integer.parseInt(db.executeQueryP2(sq))
				   def samt=db.executeQueryP2(" select rate_per_unit from ariacore.CLIENT_SURCHARGE_PRICE where surcharge_no="+surcharges)
				   def ts=samt.toInteger()*counts
				   total_amt=(total_amt.toDouble().round(2) )+ts
				   }
				   
				   
				   return d.format(total_amt)
}
  public String md_updateinvoice_amount_with_previous_credit_m(String val)
  {
	   logi.logInfo("md_cancel_invoice_amount_with_usage_m")
	   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
	   DecimalFormat d = new DecimalFormat("0.##");
	   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
	   logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
	   ConnectDB database = new ConnectDB();
	   String Usage_Amount_query = "SELECT SUM(Amount) FROM ARIACORE.credits WHERE master_plan_instance_no ="+plan_instance_no
	   def Usage_Amount= database.executeQueryP2(Usage_Amount_query).toDouble();
	   def Prorated_charge_amount = md_upgrade_plan_units_m(val)
	   def total_amt = Prorated_charge_amount.toDouble() - Usage_Amount.toDouble()
	   return d.format(total_amt)
  }
  public String md_cancel_both_Master_asupp_m(String val)
  {
	   logi.logInfo("md_cancel_invoice_amount_with_usage_m")
	   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
	   DecimalFormat d = new DecimalFormat("0.##");
	   String val1 = "1,2m,1s"
	   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
	   logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
	   ConnectDB database = new ConnectDB();
	  
	   def Cancel_plan_MP_credit_amt = md_cancel_proration_credit_amount_m(val)
	   def Cancel_plan_SP_credit_amt = md_cancel_proration_credit_amount_m(val1)
	   def total_amt =Cancel_plan_MP_credit_amt.toDouble() + Cancel_plan_SP_credit_amt.toDouble()
	   return d.format(total_amt)
	   
  }
  
  public String md_replace_proration_credit_amount_m(String val)
  {
	   logi.logInfo("md_replace_proration_credit_amount_m")
	   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
	   DecimalFormat d = new DecimalFormat("0.##");
	   String[] plan_no
	   logi.logInfo("Client No ====>"+ clientNo)
	   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
	   logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
	   ConnectDB database = new ConnectDB();
	   SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
	   String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
	   String currentVirtualTime = database.executeQueryP2(Query);
	   logi.logInfo "Current Virtual Time : "+ currentVirtualTime
	   //Getting master plan instance
	   String Master_plan_instance_query = "SELECT MASTER_PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   String Master_plan_instance = database.executeQueryP2(Master_plan_instance_query);
	   logi.logInfo "Master Plan Instance No : "+ Master_plan_instance
	   def proration_factor = md_ProrationFactorCalculation_m(Master_plan_instance)
	   d.format(proration_factor)
	   logi.logInfo "Proration Factor: "+ proration_factor
	   String plan_query = "select old_plan from ariacore.PLAN_INSTANCE_PLAN_HIST where client_no="+clientNo+" and PLAN_INSTANCE_NO="+plan_instance_no+" and old_plan is not null"
	   plan_no = database.executeQueryP2(plan_query);
	   logi.logInfo "Plan Number: "+ plan_no
	   String plan_query1 = "select old_plan from ariacore.PLAN_INSTANCE_PLAN_HIST where client_no="+clientNo+" and PLAN_INSTANCE_NO="+plan_instance_no+" and old_plan is not null"
	   String plan_no1 = database.executeQueryP2(plan_query1);
	   logi.logInfo "Plan Number1: "+ plan_no1
	   Map<String, HashMap<String,String>> hash
	   Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
	   Iterator iterator
	   hash = getPlanServiceTiredPricing1(plan_no,plan_instance_no)
	   resultHash = new TreeMap<String, TreeMap<String,String>> ()
	   resultHash.putAll(hash)
	   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	   iterator = resultHash.entrySet().iterator()
	   logi.logInfo "Before Key setting "
	   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	   def totalAmount = 0
	   while (iterator.hasNext())
	   {
	   
		   Map.Entry pairs = (Map.Entry) iterator.next()
		   String planNo = pairs.getKey().toString()
		   logi.logInfo "Execution for the plan :  "+ planNo
		   
		   Map<String,String> valueMap = pairs.getValue()
		   Map<String,String> value = new TreeMap<String,String> ()
		   value.putAll(valueMap)
		   Iterator service = value.entrySet().iterator()
		   String unitsquery="select old_recurring_factor from ariacore.PLAN_INSTANCE_PLAN_HIST where client_no="+clientNo+" and PLAN_INSTANCE_NO="+plan_instance_no+" and old_plan is not null"
		   int noofUnits =database.executeQueryP2(unitsquery).toInteger()
		   logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo
	   
		   while(service.hasNext())
		   {
			   int ChargeMultipleAct = 0
			   Map.Entry servicePairs = (Map.Entry) service.next()
			   String serviceNo = servicePairs.getKey().toString()
			   logi.logInfo "Invoice calculation for a service : "+serviceNo
			   String srvqry="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO ="+serviceNo+ "AND CLIENT_NO="+clientNo
			   String srvctype=database.executeQueryP2(srvqry)
			   logi.logInfo "Service type: "+srvctype
			   String flag_qry = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT' AND CLIENT_NO="+clientNo
			   String fla_val=database.executeQueryP2(flag_qry)
			   logi.logInfo "CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT : "+fla_val
			   String flag_qry1 = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT' AND CLIENT_NO="+clientNo
			   String fla_val1=database.executeQueryP2(flag_qry1)
			   logi.logInfo "MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT : "+fla_val1
			   if (srvctype=='AC' && fla_val== 'TRUE')
			   {
				   logi.logInfo "Entering into if loop"
				   if(fla_val1 == 'TRUE')
				   {
					   ChargeMultipleAct = 1
					   logi.logInfo "Entering into inner if loop"
				   }
				   else if(fla_val1 == 'FALSE')
				   {
					   ChargeMultipleAct = 2
					   logi.logInfo "Entering into inner clerif loop"
				   }
			   }
			   logi.logInfo "ChargeMultipleAct Value" +   ChargeMultipleAct
			   
			   int seqNo=1;
			   String tieredPricing = servicePairs.getValue().toString()
			   int tieredPricingInt = Integer.parseInt(tieredPricing)
			   logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
			   String altFee = "NoVal"
			   double serviceInvoice
			   
			   String query
			   switch(tieredPricingInt)
			   {
				   case 1:
					   if(ChargeMultipleAct==2)
					   {
						   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   logi.logInfo "Service Amount Calculation" +   serviceInvoice
						   totalAmount = totalAmount + serviceInvoice
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
						   
					   }
					   else if(ChargeMultipleAct==1)
					   {
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   logi.logInfo "Service Amount Calculation" +   serviceInvoice
						   totalAmount = totalAmount + serviceInvoice
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
						   
					   }
					   else if (srvctype != 'MN')
					   {
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and NRSR.RATE_SEQ_NO="+seqNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
					   }
				   case 2:
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
				   case 3:
						   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
			   }
		   }
		   logi.logInfo "Total Amount Calculation Final" +   totalAmount
	   }
	   
	   return d.format(totalAmount)
	   
  }
  
 public HashMap getPlanServiceTiredPricingCancelFulfilment(String[] planNo,String Plan_instance_no)
  {
	   
	   logi.logInfo "getAssignsuppPlanServiceAndTieredPricing3" + planNo.length
	   ConnectDB database = new ConnectDB();
	   String client_no=Constant.mycontext.expand('${Properties#Client_No}')
	   HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
	   
	   String query = "SELECT plan_no,service_no,TIERED_PRICING_RULE_NO FROM ariacore.plan_services WHERE plan_no IN(SELECT plan_no FROM ariacore.plan_instance WHERE plan_instance_no = "+Plan_instance_no+" and fulfillment_based_ind = 1)AND client_no   = "+client_no+" AND service_no IN(SELECT service_no FROM ariacore.services WHERE custom_to_client_no = "+client_no+" AND usage_based= 0 and is_setup=0)ORDER BY plan_no,service_no"
	   int count = Integer.parseInt(database.executeQueryP2("SELECT count(plan_no) FROM ariacore.plan_services WHERE plan_no IN(SELECT plan_no FROM ariacore.plan_instance WHERE plan_instance_no = "+Plan_instance_no+" and fulfillment_based_ind = 1)AND client_no   = "+client_no+" AND service_no IN(SELECT service_no FROM ariacore.services WHERE custom_to_client_no = "+client_no+" AND usage_based= 0 and is_setup=0)ORDER BY plan_no,service_no"))
	   ResultSet resultSet = database.executeQuery(query)

	  
	   HashMap<String,String> serviceHash

	   String[] plan = new String[count]
	   String[] service = new String[count]
	   String[] tieredPricing = new String[count]

	   int i= 0
	   while(resultSet.next()){
		   plan[i]=resultSet.getString(1)
		   service[i]=resultSet.getString(2)
		   tieredPricing[i]=resultSet.getString(3)
		   i++
	   }
	   String plan_no
	   int serviceCount=0
	   for(int planCount=0;planCount<plan.length;){
		   plan_no = plan[planCount]
		   serviceHash = new HashMap<String,String> ()
		   for(;serviceCount<service.length;serviceCount++){
			   if(plan_no.equalsIgnoreCase(plan[serviceCount])){
				   serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
				   planCount ++
			   }
			   else
				   break
		   }
		   resultHash.put(plan_no, serviceHash)
		   logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
	   }
	   logi.logInfo "Total Services" + resultHash.toString()
	   
	   return resultHash
  }
  
  public String md_replace_plan_prorated_amount_m(String val)
  {
	   logi.logInfo("md_replace_plan_prorated_amount_m")
	   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
	   DecimalFormat d = new DecimalFormat("0.##");
	   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
	   logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
	   ConnectDB database = new ConnectDB();
	   def Prorated_charge_amount = md_assign_supp_Proration_amount_m(val)
	   def Prorated_credit_amount = md_replace_proration_credit_amount_m(val)
	   def total_amt = Prorated_charge_amount.toDouble() - Prorated_credit_amount.toDouble()
	   if(total_amt<=0)
	   {
			total_amt = 0
	   }
	   return d.format(total_amt)
 }
  public String md_assign_supp_Proration_fullfill_amount_m(String val)
  {
	   logi.logInfo("Proration Amount Assigning the supplemental plan")
	   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
	   DecimalFormat d = new DecimalFormat("0.##");
	   String[] plan_no
	   logi.logInfo("Client No ====>"+ clientNo)
	   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
	   logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
	   ConnectDB database = new ConnectDB();
	   SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
	   String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
	   String currentVirtualTime = database.executeQueryP2(Query);
	   logi.logInfo "Current Virtual Time : "+ currentVirtualTime
	   //Getting master plan instance
	   String Master_plan_instance_query = "SELECT MASTER_PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   String Master_plan_instance = database.executeQueryP2(Master_plan_instance_query);
	   logi.logInfo "Master Plan Instance No : "+ Master_plan_instance
	   def proration_factor = md_ProrationFactorCalculation_m(plan_instance_no)
	   d.format(proration_factor)
	   logi.logInfo "Proration Factor: "+ proration_factor
	   String plan_query = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   plan_no = database.executeQueryP2(plan_query);
	   logi.logInfo "Plan Number: "+ plan_no
	   String plan_query1 = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	   String plan_no1 = database.executeQueryP2(plan_query1);
	   logi.logInfo "Plan Number1: "+ plan_no1
	   Map<String, HashMap<String,String>> hash
	   Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
	   Iterator iterator
	   hash = getPlanServiceTiredPricing2(plan_no,plan_instance_no)
	   resultHash = new TreeMap<String, TreeMap<String,String>> ()
	   resultHash.putAll(hash)
	   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	   iterator = resultHash.entrySet().iterator()
	   logi.logInfo "Before Key setting "
	   logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	   def totalAmount = 0
	   while (iterator.hasNext())
	   {
	   
		   Map.Entry pairs = (Map.Entry) iterator.next()
		   String planNo = pairs.getKey().toString()
		   logi.logInfo "Execution for the plan :  "+ planNo
		   
		   Map<String,String> valueMap = pairs.getValue()
		   Map<String,String> value = new TreeMap<String,String> ()
		   value.putAll(valueMap)
		   Iterator service = value.entrySet().iterator()
		   String unitsquery="SELECT PLAN_UNITS FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO=" + plan_instance_no
		   int noofUnits =database.executeQueryP2(unitsquery).toInteger()
		   logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo
	   
		   while(service.hasNext())
		   {
			   int ChargeMultipleAct = 0
			   Map.Entry servicePairs = (Map.Entry) service.next()
			   String serviceNo = servicePairs.getKey().toString()
			   logi.logInfo "Invoice calculation for a service : "+serviceNo
			   String srvqry="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO ="+serviceNo+ "AND CLIENT_NO="+clientNo
			   String srvctype=database.executeQueryP2(srvqry)
			   logi.logInfo "Service type: "+srvctype
			   String flag_qry = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT' AND CLIENT_NO="+clientNo
			   String fla_val=database.executeQueryP2(flag_qry)
			   logi.logInfo "CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT : "+fla_val
			   String flag_qry1 = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT' AND CLIENT_NO="+clientNo
			   String fla_val1=database.executeQueryP2(flag_qry1)
			   logi.logInfo "MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT : "+fla_val1
			   if (srvctype=='AC' && fla_val== 'TRUE')
			   {
				   logi.logInfo "Entering into if loop"
				   if(fla_val1 == 'TRUE')
				   {
					   ChargeMultipleAct = 1
					   logi.logInfo "Entering into inner if loop"
				   }
				   else if(fla_val1 == 'FALSE')
				   {
					   ChargeMultipleAct = 2
					   logi.logInfo "Entering into inner clerif loop"
				   }
			   }
			   logi.logInfo "ChargeMultipleAct Value" +   ChargeMultipleAct
			   
			   int seqNo=1;
			   String tieredPricing = servicePairs.getValue().toString()
			   int tieredPricingInt = Integer.parseInt(tieredPricing)
			   logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
			   String altFee = "NoVal"
			   double serviceInvoice
			   
			   String query
			   switch(tieredPricingInt)
			   {
				   case 1:
					   if(ChargeMultipleAct==2)
					   {
						   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   logi.logInfo "Service Amount Calculation" +   serviceInvoice
						   totalAmount = totalAmount + serviceInvoice
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
						   
					   }
					   else if(ChargeMultipleAct==1)
					   {
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   logi.logInfo "Service Amount Calculation" +   serviceInvoice
						   totalAmount = totalAmount + serviceInvoice
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
						   
					   }
					   else if(ChargeMultipleAct==0 && srvctype =='AC')
					   {
						  
						   serviceInvoice = 0
						   logi.logInfo "Service Amount Calculation" +   serviceInvoice
						   totalAmount = totalAmount + serviceInvoice
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
						   
					   }
					   else if (srvctype != 'MN')
					   {
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and NRSR.RATE_SEQ_NO="+seqNo
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
					   }
				   case 2:
						   query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
				   case 3:
						   query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						   serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						   double total = (serviceInvoice * proration_factor)
						   logi.logInfo "total amount " + total
						   totalAmount = totalAmount + total
						   logi.logInfo "Total Amount Calculation" +   totalAmount
						   break;
			   }
		   }
		   logi.logInfo "Total Amount Calculation Final" +   totalAmount
	   }
	   
	   return d.format(totalAmount)
	   
  }
  public HashMap getPlanServiceTiredPricing2(String[] planNo,String Plan_instance_no)
  {
	   
	   logi.logInfo "getAssignsuppPlanServiceAndTieredPricing3" + planNo.length
	   ConnectDB database = new ConnectDB();
	   String client_no=Constant.mycontext.expand('${Properties#Client_No}')
	   HashMap<String, HashMap<String,String>> resultHash = new HashMap<String, HashMap<String,String>> ()
	   
	   String query = "SELECT plan_no,service_no,TIERED_PRICING_RULE_NO FROM ariacore.plan_services WHERE plan_no IN(SELECT plan_no FROM ariacore.plan_instance WHERE plan_instance_no = "+Plan_instance_no+")AND client_no   = "+client_no+" AND FULFILLMENT_BASED_IND=0 and service_no IN(SELECT service_no FROM ariacore.services WHERE custom_to_client_no = "+client_no+" AND usage_based= 0)ORDER BY plan_no,service_no"
	   int count = Integer.parseInt(database.executeQueryP2("SELECT count(plan_no) FROM ariacore.plan_services WHERE plan_no IN(SELECT plan_no FROM ariacore.plan_instance WHERE plan_instance_no = "+Plan_instance_no+")AND client_no   = "+client_no+" AND FULFILLMENT_BASED_IND=0 and service_no IN(SELECT service_no FROM ariacore.services WHERE custom_to_client_no = "+client_no+" AND usage_based= 0)ORDER BY plan_no,service_no"))
	   ResultSet resultSet = database.executeQuery(query)

	  
	   HashMap<String,String> serviceHash

	   String[] plan = new String[count]
	   String[] service = new String[count]
	   String[] tieredPricing = new String[count]

	   int i= 0
	   while(resultSet.next()){
		   plan[i]=resultSet.getString(1)
		   service[i]=resultSet.getString(2)
		   tieredPricing[i]=resultSet.getString(3)
		   i++
	   }
	   String plan_no
	   int serviceCount=0
	   for(int planCount=0;planCount<plan.length;){
		   plan_no = plan[planCount]
		   serviceHash = new HashMap<String,String> ()
		   for(;serviceCount<service.length;serviceCount++){
			   if(plan_no.equalsIgnoreCase(plan[serviceCount])){
				   serviceHash.put(service[serviceCount],tieredPricing[serviceCount])
				   planCount ++
			   }
			   else
				   break
		   }
		   resultHash.put(plan_no, serviceHash)
		   logi.logInfo "Services for a plan : "+plan_no + " are " + resultHash.get(plan_no)
	   }
	   logi.logInfo "Total Services" + resultHash.toString()
	   
	   return resultHash
	   
	   
  }
  
  /** To get the Proration result amount of non fulfilled services when plan units are modified in modify_acct_plan_units
	* @param str (acct_hierarchy)
	* @return Sum - proration result amount
	*/
  public String md_modify_acct_pln_unit_inst_Proration_m(String str){
		  
				ConnectDB db = new ConnectDB()
				LinkedHashMap hm1 = new LinkedHashMap<String,String>()
				InputMethods im = new InputMethods()
				String param = str.split("#")[0]
				String old_units = str.split("#")[1]
				String new_units = str.split("#")[2]
				DecimalFormat d = new DecimalFormat("#.##");
				int ou = Integer.parseInt(old_units)
				int nu = Integer.parseInt(new_units)
				logi.logInfo "----param---"+param+"------oldunits-----"+ou+"-----new_units----"+nu
				String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(param,null)
				def proration_factor = d.format(md_ProrationFactorCalculation_m(plan_instance_no))
				String plan_no_query = "SELECT PI.PLAN_NO FROM ARIACORE.PLAN_INSTANCE PI  WHERE PI.PLAN_INSTANCE_NO = "+plan_instance_no
				String plan_no = db.executeQueryP2(plan_no_query).toString()
				logi.logInfo "---ASAS---"+plan_instance_no+"---ASAS----"+plan_no
				String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
				logi.logInfo "client_no ---------- "+clientNo
				String counter1 = "SELECT COUNT(*) AS COUNTS FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_SERVICES PS ON PI.PLAN_NO    = PS.PLAN_NO AND PI.CLIENT_NO = PS.CLIENT_NO JOIN ARIACORE.NEW_RATE_SCHED_RATES NRS ON NRS.SERVICE_NO  = PS.SERVICE_NO AND PI.SCHEDULE_NO = NRS.SCHEDULE_NO AND PI.CLIENT_NO   = NRS.CLIENT_NO WHERE PI.PLAN_INSTANCE_NO =  "+plan_instance_no
				String counting = db.executeQueryP2(counter1)
				int cn = Integer.parseInt(counting)
				for(int i=1;i<=cn;i++){
					logi.logInfo("entered into the loop")
					 
					logi.logInfo("counting in DARPA : "+i)
					hm1.put(i,md_services_no_m(plan_instance_no,i))
					
			  }
				logi.logInfo "zombiefied "+hm1
				Map<String, TreeMap<String, String>> servicelistHash = new TreeMap<String, TreeMap<String,String>>();
				servicelistHash = new TreeMap<String, TreeMap<String,String>>();
				servicelistHash.putAll(hm1);
				logi.logInfo "servicelisthash  "+servicelistHash
				Iterator iterator = servicelistHash.entrySet().iterator();
				//charge
				def sum = 0
				//credits
			   def used_amount = 0
				logi.logInfo "proration factor value : "+proration_factor
				while (iterator.hasNext())
				 {
					 logi.logInfo "entered first iterator"
						Map.Entry pairs = (Map.Entry) iterator.next();
					   String outer = pairs.getKey().toString();
					   logi.logInfo "cdcd "+outer
					   Map<String,String> valueMap = (Map<String, String>) pairs.getValue();
					   Map<String,String> value = new TreeMap<String,String>();
					   value.putAll(valueMap);
					   Iterator service = value.entrySet().iterator();
						while (service.hasNext())
						 {
						 
								Map.Entry pairs2 = (Map.Entry) service.next();
								String servicehash = pairs2.getKey().toString();
							   //System.out.println("cdcd "+planNo2);
							   logi.logInfo "Value of key "+servicehash+" : "+pairs2.getValue()
							   if(servicehash.equals("service_no")){
								  String service_no = pairs2.getValue()
								  String query = "SELECT SERVICE_TYPE FROM ARIACORE.CLIENT_SERVICE WHERE SERVICE_NO = "+service_no
								  String type = db.executeQueryP2(query).toString()
								  if(type.equals("AC")){
									  //if AC checking the flag and adding the value
									  String supp_ind_query = "SELECT CP.SUPP_PLAN_IND FROM ARIACORE.CLIENT_PLAN CP JOIN ARIACORE.PLAN_SERVICES PS ON CP.PLAN_NO = PS.PLAN_NO AND CP.CLIENT_NO = PS.CLIENT_NO WHERE CP.PLAN_NO = "+plan_no+" AND PS.SERVICE_NO = "+service_no
									  String supp_ind = db.executeQueryP2(supp_ind_query).toString()
									  logi.logInfo "supp_plan_indication : "+supp_ind
									  if(supp_ind.equals("0")){
										  logi.logInfo "it is a master plan ---- WERSW"
										  
										  String o_units = old_units.toString()
										  String n_units = new_units.toString()
										  
										  def ou1 = o_units.toDouble()
										  def n_u1 = n_units.toDouble()
										  def new_val = (n_u1 - ou1)
										  
									   sum = md_check_flags_m(plan_no,service_no,plan_instance_no,new_val,clientNo,sum)
										  
									  }else if(supp_ind.equals("1")){
									  logi.logInfo " supp plan indicator on ---SUPPER"
									  String supp_flag_query = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME = 'CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT' AND CLIENT_NO = "
									  String supp_flag = db.executeQueryP2(supp_flag_query).toString()
										  if(supp_flag.equals("true")||supp_flag.equals("TRUE")){
											  logi.logInfo "SUPP FLAG ON ------SFLAGGE"
											  
											  String o_units = old_units.toString()
										  String n_units = new_units.toString()
										  
										  def ou1 = o_units.toDouble()
										  def n_u1 = n_units.toDouble()
										  def new_val = (n_u1 - ou1)
										  
									   sum = md_check_flags_m(plan_no,service_no,plan_instance_no,new_val,clientNo,sum)
										  }
										  else{
											  logi.logInfo "SUPP PLAN FLAG OFF -NO ACTIVATION -- - SFLGOFF"
												 
										  }
									  }
								  }
								  else if(type.equals("RC")){
									  //checking fulfilled or not
									  String fuin = "SELECT PS.FULFILLMENT_BASED_IND FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_SERVICES PS ON PI.PLAN_NO = PS.PLAN_NO AND PI.CLIENT_NO = PS.CLIENT_NO WHERE PS.PLAN_NO = "+plan_no+" AND PS.SERVICE_NO = "+service_no+" AND PI.PLAN_INSTANCE_NO = "+plan_instance_no
									  String indicator = db.executeQueryP2(fuin).toString()
									  if(indicator.equals("0")){
										  logi.logInfo "indicated as non fulfillment-----PF"+proration_factor
										  String rcrate_query = "SELECT NRS.RATE_PER_UNIT FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_SERVICES PS ON PI.PLAN_NO = PS.PLAN_NO AND PI.CLIENT_NO = PS.CLIENT_NO JOIN ARIACORE.NEW_RATE_SCHED_RATES NRS ON NRS.SERVICE_NO = PS.SERVICE_NO AND PI.SCHEDULE_NO = NRS.SCHEDULE_NO AND PI.CLIENT_NO = NRS.CLIENT_NO WHERE PI.PLAN_INSTANCE_NO = "+plan_instance_no+" AND PS.SERVICE_NO = "+service_no
										  String rc_rate = db.executeQueryP2(rcrate_query).toString()
										  String nw_units = new_units.toString()
										  String pf = proration_factor.toString()
										  
										  def rr = rc_rate.toDouble()
										  def nw = nw_units.toDouble()
										  def ppf = pf.toDouble()
										  
										  logi.logInfo "ASDF "+rr+" dfdsad "+nw+" dsafjlkdj "+ppf
										  
										  def fv = (rr * nw)
										  def p_amount = (fv * ppf)
										  def rc_amount = d.format(p_amount)
										  def rc_amount1 = rc_amount.toDouble()
										  logi.logInfo "sum value previously "+sum
										  def sum1 = sum.toDouble()
										  sum = sum+rc_amount1
										  logi.logInfo "finalizer "+fv+" RC final amount "+rc_amount1+" sum "+sum
										  
										  //used rate of values
										  String used_proration = old_units.toString()
										  String pf1 = proration_factor.toString()
										  def used_pro_units = used_proration.toDouble()
										  def p_f1 = pf1.toDouble()
										  logi.logInfo " used pro "+used_pro_units
											 used_amount = (p_f1 * used_pro_units * rr)
											logi.logInfo " used amount "+used_amount
											
											
										  //sum = sum1.toDouble()
									  }
									  else if (indicator.equals("1")){
										  logi.logInfo "indicated as fulfilled services"
									  }
								  }
							   }
							   
						 }
				 }
			   
				 sum = (sum - used_amount)
				 logi.logInfo "Proration result amount "+sum
				String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
				String currentVirtualTime = db.executeQueryP2(Query);
				logi.logInfo "Current Virtual Time : "+ currentVirtualTime
				return sum
		 }
	   
	  
	  /** to check the flags for a service of a plan
	* @param plan_no
	* @param service_no
	* @param plan_instance_no
	* @param nu
	* @param clientNo
	* @param val
	* @return value for the service in a plan
	*/
  def md_check_flags_m(String plan_no,String service_no,String plan_instance_no,def nu,String clientNo,def val){
		
		  ConnectDB db = new ConnectDB()
		   //check for MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT flag
		  String flag_mul_query = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME = 'MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT' AND CLIENT_NO = "+clientNo
		  String param_val = db.executeQueryP2(flag_mul_query).toString()
		  logi.logInfo "param val : "+param_val
		  if(param_val.equals("true")||param_val.equals("TRUE")){
			  String rate_query = "SELECT NRS.RATE_PER_UNIT FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_SERVICES PS ON PI.PLAN_NO = PS.PLAN_NO AND PI.CLIENT_NO = PS.CLIENT_NO JOIN ARIACORE.NEW_RATE_SCHED_RATES NRS ON NRS.SERVICE_NO = PS.SERVICE_NO AND PI.SCHEDULE_NO = NRS.SCHEDULE_NO AND PI.CLIENT_NO = NRS.CLIENT_NO WHERE PI.PLAN_INSTANCE_NO = "+plan_instance_no+" AND PS.SERVICE_NO = "+service_no
			  String rate = db.executeQueryP2(rate_query).toString()
			  def frate = rate.toDouble()
			  String nuu = nu.toString();logi.logInfo " converted to string "+nuu
			  def nu1 = nuu.toDouble()
			  logi.logInfo "retr value : "+frate+"-------"+nuu
			  def mul = (frate * nu1)
			  logi.logInfo "mul value "+mul
			  val = (val + mul)
			  logi.logInfo "val value : "+val
		  }else {
		  
			 String rate_query = "SELECT NRS.RATE_PER_UNIT FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_SERVICES PS ON PI.PLAN_NO = PS.PLAN_NO AND PI.CLIENT_NO = PS.CLIENT_NO JOIN ARIACORE.NEW_RATE_SCHED_RATES NRS ON NRS.SERVICE_NO = PS.SERVICE_NO AND PI.SCHEDULE_NO = NRS.SCHEDULE_NO AND PI.CLIENT_NO = NRS.CLIENT_NO WHERE PI.PLAN_INSTANCE_NO = "+plan_instance_no+" AND PS.SERVICE_NO = "+service_no
			 def rate = db.executeQueryP2(rate_query)
			 logi.logInfo "retr2 value : "+rate
			 val = val + rate
		  
		  }
		  
		  return val
	  }
	  
		
	  
		 /**List of services in hash value
		* @param plan_instance_no
		* @param i
		* @return hash of services in an account
		*/
	   public LinkedHashMap md_services_no_m(String plan_instance_no,int i){
			 
			 ConnectDB db = new ConnectDB()
			 LinkedHashMap row = new LinkedHashMap<String,String>()
			 String query = "SELECT PN.SERVICE_NO"+
							  " FROM"+
							  "  (SELECT PS.SERVICE_NO,"+
							  "  ROW_NUMBER() OVER(ORDER BY PI.PLAN_INSTANCE_NO) AS SEQ"+
							  "  FROM ARIACORE.PLAN_INSTANCE PI"+
							  "  JOIN ARIACORE.PLAN_SERVICES PS"+
							  "  ON PI.PLAN_NO             = PS.PLAN_NO"+
							  "  AND PI.CLIENT_NO          = PS.CLIENT_NO"+
							  "  JOIN ARIACORE.NEW_RATE_SCHED_RATES NRS"+
							  "  ON NRS.SERVICE_NO         = PS.SERVICE_NO"+
							  "  AND PI.SCHEDULE_NO        = NRS.SCHEDULE_NO"+
							  "  AND PI.CLIENT_NO          = NRS.CLIENT_NO"+
							  "  WHERE PI.PLAN_INSTANCE_NO = "+plan_instance_no+
							  "  )PN"+
							  " WHERE PN.SEQ ="+i
					  
							  ResultSet rs1 = db.executePlaQuery(query);
							  ResultSetMetaData md1 = rs1.getMetaData();
							  int columns1 = md1.getColumnCount();
							  while (rs1.next()){
							   for(int s=1; s<=columns1; ++s){
									   if(rs1.getObject(s).toString().equals("null")){
											   logi.logInfo("No value in "+rs1.getObject(s))
												   }else{logi.logInfo("Value inserting in hash map")
												   row.put(md1.getColumnName(s).toLowerCase(),rs1.getObject(s));}
												   logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))
											   }
									   }
							  logi.logInfo "inner hash QWERTYLOL : "+row.sort()
							  return row
							  
							  
		 }
		  
	   /**Proration Credit for modify_acct_plan_unit_instance_m
		* @param str (acct_hierarchy)
		* @return sum of proration amount
		*/
	   def md_modify_unit_inst_remove_proration_m(String str){
		  
			  ConnectDB db = new ConnectDB()
			  String param = str.split("#")[0]
			  def units = str.split("#")[1]
			  String plan_unit_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(param,null)
			  //get_plan_unit_instance from request
			  LinkedHashMap hm1 = new LinkedHashMap<String,HashMap<String,String>>()
			  DecimalFormat d = new DecimalFormat("0.##");
			  String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
			  String plan_instance_no = "SELECT PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INST_UNIT_INSTANCES WHERE PLAN_UNIT_INSTANCE_NO = "+plan_unit_instance_no+" AND CLIENT_NO = "+clientNo
			  String plan_inst_no = db.executeQueryP2(plan_instance_no).toString()
			  def proration_factor = d.format(md_ProrationFactorCalculation_m(plan_inst_no))
			  String plan_no_query = "SELECT PLAN_NO FROM ARIACORE.PLAN_INST_UNIT_INSTANCES WHERE PLAN_UNIT_INSTANCE_NO = "+plan_unit_instance_no+" AND CLIENT_NO = "+clientNo
			  String plan_no = db.executeQueryP2(plan_no_query).toString()
			  logi.logInfo "----PLAN_UNIT_INSTANCE_NO---"+plan_unit_instance_no+"---client_no----"+clientNo+"---plan_no---"+plan_no+"----P.F------"+proration_factor
			  String service_count_query = "SELECT COUNT(*) AS COUNTS FROM ARIACORE.PLAN_SERVICES WHERE PLAN_NO = "+plan_no
			  String service_c = db.executeQueryP2(service_count_query).toString()
			  int scount =Integer.parseInt(service_c)
			  for(int i=1;i<=scount;i++){
				  logi.logInfo("entered into the loop")
				   
				  logi.logInfo("counting in NORAD : "+i)
				  hm1.put(i,md_services_no_m(plan_inst_no,i))
				  
			  }
			   
			  logi.logInfo "AMORA "+hm1
			  Map<String, TreeMap<String, String>> servicelistHash = new TreeMap<String, TreeMap<String,String>>();
			  servicelistHash = new TreeMap<String, TreeMap<String,String>>();
			  servicelistHash.putAll(hm1);
			  logi.logInfo "servicelisthash  "+servicelistHash
			  Iterator iterator = servicelistHash.entrySet().iterator();
			  //charge
			  def initial = 0
			  def sum = initial.toDouble()
			  //credits
			 def used_amount = 0
			  logi.logInfo "proration factor value : "+proration_factor
			  while (iterator.hasNext())
			   {
				   logi.logInfo "entered first iterator"
					  Map.Entry pairs = (Map.Entry) iterator.next();
					 String outer = pairs.getKey().toString();
					 logi.logInfo "cdcd "+outer
					 Map<String,String> valueMap = (Map<String, String>) pairs.getValue();
					 Map<String,String> value = new TreeMap<String,String>();
					 value.putAll(valueMap);
					 Iterator service = value.entrySet().iterator();
					  while (service.hasNext())
					   {
					   
							  Map.Entry pairs2 = (Map.Entry) service.next();
							  String servicehash = pairs2.getKey().toString();
							 //System.out.println("cdcd "+planNo2);
							 logi.logInfo "Value of key "+servicehash+" : "+pairs2.getValue()
							 if(servicehash.equals("service_no")){
								String service_no = pairs2.getValue()
								String query = "SELECT SERVICE_TYPE FROM ARIACORE.CLIENT_SERVICE WHERE SERVICE_NO = "+service_no
								String type = db.executeQueryP2(query).toString()
							   if(type.equals("RC")){
									//checking fulfilled or not
									String fuin = "SELECT PS.FULFILLMENT_BASED_IND FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_SERVICES PS ON PI.PLAN_NO = PS.PLAN_NO AND PI.CLIENT_NO = PS.CLIENT_NO WHERE PS.PLAN_NO = "+plan_no+" AND PS.SERVICE_NO = "+service_no+" AND PI.PLAN_INSTANCE_NO = "+plan_inst_no
									String indicator = db.executeQueryP2(fuin).toString()
									if(indicator.equals("0")){
										String rcrate_query = "SELECT NRS.RATE_PER_UNIT FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_SERVICES PS ON PI.PLAN_NO = PS.PLAN_NO AND PI.CLIENT_NO = PS.CLIENT_NO JOIN ARIACORE.NEW_RATE_SCHED_RATES NRS ON NRS.SERVICE_NO = PS.SERVICE_NO AND PI.SCHEDULE_NO = NRS.SCHEDULE_NO AND PI.CLIENT_NO = NRS.CLIENT_NO WHERE PI.PLAN_INSTANCE_NO = "+plan_inst_no+" AND PS.SERVICE_NO = "+service_no
										String rc_rate = db.executeQueryP2(rcrate_query).toString()
										def rr = rc_rate.toDouble()
										def prf = proration_factor.toDouble()
										def uni = units.toDouble()
										logi.logInfo "ASDF--"+rr+"----sldkf-----"+prf+"----fdsfa----"+uni
										def fv = (rr * prf)
										def total_amount = (fv * uni)
										sum = sum+total_amount
										logi.logInfo "finalizer "+fv+" RC final amount "+total_amount+" sum "+sum
																		}
									else if (indicator.equals("1")){
										logi.logInfo "indicated as fulfilled services"
										
										String fs_query = "SELECT FULFILLMENT_STATUS FROM ARIACORE.PLAN_INST_UNIT_INSTANCE_SRVCS WHERE PLAN_UNIT_INSTANCE_NO = "+plan_unit_instance_no+" AND FULFILLMENT_BASED_IND = 1"
										String status = db.executeQueryP2(fs_query).toString()
										if(status.equals("1")){
											String rcrate_query = "SELECT NRS.RATE_PER_UNIT FROM ARIACORE.PLAN_INSTANCE PI JOIN ARIACORE.PLAN_SERVICES PS ON PI.PLAN_NO = PS.PLAN_NO AND PI.CLIENT_NO = PS.CLIENT_NO JOIN ARIACORE.NEW_RATE_SCHED_RATES NRS ON NRS.SERVICE_NO = PS.SERVICE_NO AND PI.SCHEDULE_NO = NRS.SCHEDULE_NO AND PI.CLIENT_NO = NRS.CLIENT_NO WHERE PI.PLAN_INSTANCE_NO = "+plan_inst_no+" AND PS.SERVICE_NO = "+service_no
											String rc_rate2 = db.executeQueryP2(rcrate_query).toString()
											def rr2 = rc_rate2.toDouble()
											def prf2 = proration_factor.toDouble()
											def uni2 = units.toDouble()
											logi.logInfo "QWERTY--"+rr2+"----sldkf-----"+prf2+"----fdsfa----"+uni2
											def fv2 = (rr2 * prf2)
											def total_amount2 = (fv2 * uni2)
											sum = sum+total_amount2
											logi.logInfo "finalizer "+fv2+" RC final amount "+total_amount2+" sum "+sum
										}
										
									}
								}
							 }
							 
					   }
			   }
			  
			  sum = d.format(sum)
			  return sum
			  }
	   
	   /**
		* @param acct_hierarchy
		* @return HashMap -> Universal contract general details
		*/
	 LinkedHashMap md_get_acct_universal_contract_gen_details_m(String acct_hierarchy)
		{
			logi.logInfo("----------> into md_get_acct_universal_contract_gen_details_m")
			ConnectDB dbb = new ConnectDB()
		
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			
		   
			String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
			logi.logInfo("acct_no"+acct_no)
			
			LinkedHashMap gen_det= new LinkedHashMap()
			int start=0
			
			String contract_no_count_q="Select count(TYPE_NO) from ariacore.ACCT_UNIVERSAL_CONTRACTS where  ACCT_NO="+acct_no
			String contract_no_count=dbb.executeQueryP2(contract_no_count_q)
			int flag=0
			String db_to_execute=null
			if(contract_no_count.toInteger() <=0)
			{
			String contract_no_count1_q="Select count(TYPE_NO) from ariacore.ACCT_MULTI_PLAN_CONTRACTS where  ACCT_NO="+acct_no
			String contract_no_count1=dbb.executeQueryP2(contract_no_count1_q)
			flag=1
			}
			
			String gen_details_q="SELECT  APC.TYPE_NO,APC.CONTRACT_NO,APC.CLIENT_CONTRACT_ID,APC.CONTRACT_MONTHS AS LENGTH_MONTHS,APC.CREATE_COMMENTS,APC.UPDATE_COMMENTS,TO_CHAR(APC.CREATE_DATE,'YYYY-MM-DD') AS CREATE_DATE ,TO_CHAR(APC.UPDATE_DATE,'YYYY-MM-DD') AS UPDATE_DATE,TO_CHAR(APC.START_DATE,'YYYY-MM-DD') AS START_DATE,TO_CHAR(APC.END_DATE,'YYYY-MM-DD') AS END_DATE,APC.STATUS_CD AS STATUS_CODE FROM ariacore.ACCT_UNIVERSAL_CONTRACTS APC WHERE ACCT_NO ="+acct_no+" AND APC.STATUS_CD = 1"
			ResultSet rs = dbb.executePlaQuery(gen_details_q)
			ResultSetMetaData md = rs.getMetaData()
			int columns = md.getColumnCount()
			LinkedHashMap temp= new LinkedHashMap()
			while (rs.next())
			{
				LinkedList l1= new LinkedList()
			
				for(int i=1; i<=columns; ++i)
				{
			
					if(rs.getObject(i)!=null)
					{
						l1.add(md.getColumnName(i).toLowerCase())
			
						int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
			
						logi.logInfo("frewww of :+"+md.getColumnName(i).toLowerCase()+" is :"+freq)
			
						if(freq>1)
						{
							 temp.put(md.getColumnName(i).toLowerCase()+"["+ freq +"]",rs.getObject(i))
				   
						}
			
						else
						temp.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
			
					}
				}
			
				//temp.put("contract_scope","UNIVERSAL")
			   temp= temp.sort()
			   
				 }
			temp=temp.sort()
	   return temp
			
		}
  

	   
	   LinkedHashMap md_get_unbilled_usage_summary_gen_details_m (String acct_hierarchy)
	   {
			logi.logInfo("**** INTO md_get_unbilled_usage_summary_gen_details_m ****")
		   ConnectDB dbb = new ConnectDB()
		   LinkedHashMap main= new LinkedHashMap()
		   LinkedHashMap temp = new LinkedHashMap()
		   String clientNo =Constant.client_no
		   String acct_string=acct_hierarchy.charAt(0)
		   String acct_no = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		   String mpi_no = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/master_plan_instance_id[1]")
		   
			//plan instance cdid
		  
		   String cdid_q="select plan_instance_cdid from ariacore.plan_instance where plan_instance_no="+mpi_no
		   String cdid=dbb.executeQueryP2(cdid_q)
		   
		   //other details
		   String details_q="SELECT MTD_UNBILLED_USG_BAL_MEASURED AS MTD_BALANCE_AMOUNT,PTD_UNBILLED_USG_BAL_MEASURED AS PTD_BALANCE_AMOUNT,CURRENT_UNAPPLIED_CREDIT_BAL AS UNAPP_SVC_CREDIT_BAL_AMOUNT,CASE when PTD_UNBILLED_USG_BAL_MEASURED = 0 then '=' When PTD_UNBILLED_USG_BAL_MEASURED > 0 then '+'  When PTD_UNBILLED_USG_BAL_MEASURED < 0  then '-' END  AS UNAPP_SVC_CREDIT_DELTA_SIGN,PTD_UNBILLED_USG_BAL_MEASURED AS UNAPP_SVC_CREDIT_DELTA_AMOUNT,CUR.CURRENCY_CD,CUR.CURRENCY_NAME FROM ARIACORE.PLAN_INST_USG_SUMMARY PIUS JOIN ARIACORE.ACCT ON  ACCT.ACCT_NO=PIUS.ACCT_NO JOIN ARIACORE.CURRENCY CUR ON ACCT.CURRENCY_CD= CUR.CURRENCY_CD AND PIUS.ACCT_NO="+acct_no+" AND PIUS.PLAN_INSTANCE_NO="+mpi_no
		   ResultSet rs= dbb.executePlaQuery(details_q)
		   LinkedHashMap md_get_unbilled_usage_summary_m = new LinkedHashMap()
		   int count =0
   
		   while (rs.next())
		   {
			  // LinkedHashMap temp = new LinkedHashMap()
				   LinkedList l1 =  new LinkedList()
			   ResultSetMetaData md = rs.getMetaData()
			   int columns = md.getColumnCount()
			   for(int i=1; i<=columns; ++i)
			   {
   
				   if(!(rs.getObject(i).toString().equalsIgnoreCase("null")))
				   {
   
					   l1.add(md.getColumnName(i).toLowerCase())
   
					   int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
   
   
					   if(freq>1)
					   {
   
						   temp.put(md.getColumnName(i).toLowerCase()+"["+ freq +"]",rs.getObject(i))
					   }
   
					   else
   
   
						   temp.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
   
				   }
				   
				   
				   
				   
				   
			   }
			   temp.put("acct_no",acct_no)
			   temp.put("master_plan_instance_id",mpi_no)
			   temp.put("client_master_plan_instance_id",cdid)
		   
			  
			   temp=temp.sort()
				/* Incase of multiple rows uncoment this
			   md_get_unbilled_usage_summary_m.put("md_get_unbilled_usage_summary_m["+count+"]",temp)
			   ++count
			   
			   */
			   }
		   return temp
		   
		  
	   }
	   
	   
	   LinkedHashMap md_get_unbilled_usage_summary_gen_details1_m(String acct_hierarchy)
	   {
			logi.logInfo("**** INTO md_get_unbilled_usage_summary_gen_details_m ****")
		   ConnectDB dbb = new ConnectDB()
		   LinkedHashMap main= new LinkedHashMap()
		   LinkedHashMap temp = new LinkedHashMap()
		   String clientNo =Constant.client_no
		   String acct_string=acct_hierarchy.charAt(0)
		   String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO2_m("1#b",null)
		   String mpi_no = im.md_GET_ACCT_OR_INSTANCE_NO2_m(acct_hierarchy,null)
		   
			//plan instance cdid
		  
		   String cdid_q="select plan_instance_cdid from ariacore.plan_instance where plan_instance_no="+mpi_no
		   String cdid=dbb.executeQueryP2(cdid_q)
		   
		   //other details
		   String details_q="SELECT MTD_UNBILLED_USG_BAL_MEASURED AS MTD_BALANCE_AMOUNT,PTD_UNBILLED_USG_BAL_MEASURED AS PTD_BALANCE_AMOUNT,CURRENT_UNAPPLIED_CREDIT_BAL AS UNAPP_SVC_CREDIT_BAL_AMOUNT,CASE when PTD_UNBILLED_USG_BAL_MEASURED = 0 then '=' When PTD_UNBILLED_USG_BAL_MEASURED > 0 then '+'  When PTD_UNBILLED_USG_BAL_MEASURED < 0  then '-' END  AS UNAPP_SVC_CREDIT_DELTA_SIGN,PTD_UNBILLED_USG_BAL_MEASURED AS UNAPP_SVC_CREDIT_DELTA_AMOUNT,CUR.CURRENCY_CD,CUR.CURRENCY_NAME FROM ARIACORE.PLAN_INST_USG_SUMMARY PIUS JOIN ARIACORE.ACCT ON  ACCT.ACCT_NO=PIUS.ACCT_NO JOIN ARIACORE.CURRENCY CUR ON ACCT.CURRENCY_CD= CUR.CURRENCY_CD AND PIUS.ACCT_NO="+acct_no+" AND PIUS.PLAN_INSTANCE_NO="+mpi_no
		   ResultSet rs= dbb.executePlaQuery(details_q)
		   LinkedHashMap md_get_unbilled_usage_summary_m = new LinkedHashMap()
		   int count =0
   
		   while (rs.next())
		   {
			  // LinkedHashMap temp = new LinkedHashMap()
				   LinkedList l1 =  new LinkedList()
			   ResultSetMetaData md = rs.getMetaData()
			   int columns = md.getColumnCount()
			   for(int i=1; i<=columns; ++i)
			   {
   
				   if(!(rs.getObject(i).toString().equalsIgnoreCase("null")))
				   {
   
					   l1.add(md.getColumnName(i).toLowerCase())
   
					   int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
   
   
					   if(freq>1)
					   {
   
						   temp.put(md.getColumnName(i).toLowerCase()+"["+ freq +"]",rs.getObject(i))
					   }
   
					   else
   
   
						   temp.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
   
				   }
				   
				   
				   
				   
				   
			   }
			   temp.put("acct_no",acct_no)
			   temp.put("master_plan_instance_id",mpi_no)
			   temp.put("client_master_plan_instance_id",cdid)
		   
			  
			   temp=temp.sort()
				/* Incase of multiple rows uncoment this
			   md_get_unbilled_usage_summary_m.put("md_get_unbilled_usage_summary_m["+count+"]",temp)
			   ++count
			   
			   */
			   }
		   
		 return temp
	
	   }
	   
	   
	   
	   LinkedHashMap md_get_unbilled_usage_summary_rec_m (String acct_hierarchy)
	   {
		   logi.logInfo("**** INTO md_get_unbilled_usage_summary_rec_m ****")
		   ConnectDB dbb = new ConnectDB()
		   LinkedHashMap main= new LinkedHashMap()
		   LinkedHashMap temp= new LinkedHashMap()
		   String clientNo =Constant.client_no
		   String acct_string=acct_hierarchy.charAt(0)
		   String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_string,null)
		   String mpi_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		   
		   String usage_details_q="SELECT USAGE_TYPE AS USAGE_TYPE_NO ,USAGE_TYPE_DESCRIPTION,to_char(RAW_USAGE_DATE,'YYYY-MM-DD') AS USAGE_DATE,USAGE_UNITS AS UNITS,EXTENDED_CHARGE AS SPECIFIC_RECORD_CHARGE_AMOUNT ,NOTATIONAL_RATE AS PRE_RATED_RATE,RECORDED_USAGE_UNITS AS RECORDED_UNITS,REC_NO AS USAGE_REC_NO FROM ARIACORE.ALL_USAGE  WHERE BILLED_ACCT_NO="+acct_no+"AND PLAN_INSTANCE_NO="+mpi_no
		   ResultSet rs= dbb.executePlaQuery(usage_details_q)
		   
		   
				   LinkedHashMap md_get_unbilled_usage_summary_m = new LinkedHashMap()
				   int count =0
		   
				   while (rs.next())
				   {
					   /* Incase of multiple rows uncoment this
						 
						   LinkedHashMap temp = new LinkedHashMap()
						
						*/
						   LinkedList l1 =  new LinkedList()
					   ResultSetMetaData md = rs.getMetaData()
					   int columns = md.getColumnCount()
					   for(int i=1; i<=columns; ++i)
					   {
		   
						   if(!(rs.getObject(i).toString().equalsIgnoreCase("null")))
						   {
		   
							   l1.add(md.getColumnName(i).toLowerCase())
		   
							   int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
		   
		   
							   if(freq>1)
							   {
		   
								   temp.put(md.getColumnName(i).toLowerCase()+"["+ freq +"]",rs.getObject(i))
							   }
		   
							   else
		   
		   
								   temp.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
		   
						   }
						   
						   
						   
						   
						   
					   }
					  temp.put("plan_instance_id",mpi_no)
					  
					 
					   temp=temp.sort()
					 /*
					  * IN case of multiple rows uncomment this
					  * md_get_unbilled_usage_summary_m.put("md_get_unbilled_usage_summary_m["+count+"]",temp)
					   ++count
					   
					   */
					   }
		   
		   
		   
		   
		   return temp
		   
		   
		   
		   
	   }
	   public  LinkedHashMap md_getUsageHistory_m(String s)
	   {
		   logi.logInfo("md_getUsageHistory_m")
		   ConnectDB db = new ConnectDB()
		   int j=0
		   LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
		   LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   def plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
		   logi.logInfo("plan_instance_no" +plan_instance_no )
			String query ="select usg.acct_no as billable_acct_no,usg.plan_instance_no as plan_instance_id,Case when  usg.plan_instance_no IS null then null else pi.PLAN_INSTANCE_CDID end as client_plan_instance_id,usg.usage_type as usage_type_no,utp.description as usage_type_description,TO_CHAR(usg.usage_date ,'YYYY-MM-DD') as usage_date,TO_CHAR(usg.usage_date ,'HH24:MI:SS') as usage_time,usg.BILLABLE_UNITS AS units,usg.BILLABLE_UNITS ||' '||uut.DESCRIPTION||'s' as units_description, uut.DESCRIPTION||'s' as usage_units_description,usg.amt as specific_record_charge_amount,DECODE (usg.EXCLUDE_IND,0, 'false',1,'true') as is_excluded,usg.rate as pre_rated_rate,usg.USAGE_UNITS as recorded_units,usg.REC_NO as usage_rec_no,usg.EXCLUDE_REASON_CD,usg.invoice_no as invoice_no,usg.QUALIFIER_1,usg.QUALIFIER_2,usg.QUALIFIER_3,usg.QUALIFIER_4,utp.USAGE_TYPE_CODE   from ariacore.usage usg  join ariacore.plan_instance pi on pi.plan_instance_no=usg.PLAN_INSTANCE_NO and usg.client_no=pi.client_no join ariacore.usage_types utp on usg.usage_type =utp.usage_type join ariacore.USAGE_UNIT_TYPES uut on utp.USAGE_UNIT_TYPE = uut.USAGE_UNIT_TYPE where pi.PLAN_INSTANCE_NO = "+plan_instance_no+" and usg.client_no ="+client_no+"order by usg.REC_NO desc"
		   ResultSet rs4=db.executePlaQuery(query)
		   ResultSetMetaData md4 = rs4.getMetaData();
		   int columns4 = md4.getColumnCount();
		   while (rs4.next())
		   {
			   for(int l=1; l<=columns4; l++)
			   {
				   if((rs4.getObject(l))!=null)
				   {
					   
						   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
					   
				   
				   }
			   }
			   if(rs4.getObject('IS_EXCLUDED')== 'true')
			   {
				   row.put("pre_rated_rate",0)
			   }
			   
			   outer.put("usage_history_recs_row["+j+"]",row.sort())
			   row = new LinkedHashMap<String, String>();
			   j++
		   
		   }
		   return outer.sort()
	   }
	   public  LinkedHashMap md_getUsageHistory1_m(String s)
	   {
		   logi.logInfo("md_getUsageHistory_m")
		   ConnectDB db = new ConnectDB()
		   int j=0
		   LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
		   LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   def plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO2_m(s,null)
		   logi.logInfo("plan_instance_no" +plan_instance_no )
		   String query ="select usg.acct_no as billable_acct_no,usg.plan_instance_no as plan_instance_id,Case when  usg.plan_instance_no IS null then null else pi.PLAN_INSTANCE_CDID end as client_plan_instance_id,usg.usage_type as usage_type_no,utp.description as usage_type_description,TO_CHAR(usg.usage_date ,'YYYY-MM-DD') as usage_date,TO_CHAR(usg.usage_date ,'HH24:MI:SS') as usage_time,usg.BILLABLE_UNITS AS units,usg.BILLABLE_UNITS ||' '||uut.DESCRIPTION||'s' as units_description, uut.DESCRIPTION||'s' as usage_units_description,usg.amt as specific_record_charge_amount,DECODE (usg.EXCLUDE_IND,0, 'false',1,'true') as is_excluded,usg.rate as pre_rated_rate,usg.USAGE_UNITS as recorded_units,usg.REC_NO as usage_rec_no,usg.EXCLUDE_REASON_CD as  from ariacore.usage usg  join ariacore.plan_instance pi on pi.plan_instance_no=usg.PLAN_INSTANCE_NO and usg.client_no=pi.client_no join ariacore.usage_types utp on usg.usage_type =utp.usage_type join ariacore.USAGE_UNIT_TYPES uut on utp.USAGE_UNIT_TYPE = uut.USAGE_UNIT_TYPE where pi.plan_instance_no = "+plan_instance_no+" and usg.client_no ="+client_no
		   ResultSet rs4=db.executePlaQuery(query)
		   ResultSetMetaData md4 = rs4.getMetaData();
		   int columns4 = md4.getColumnCount();
		   while (rs4.next())
		   {
			   for(int l=1; l<=columns4; l++)
			   {
				   if((rs4.getObject(l))!=null)
				   {
					   
						   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
					   
				   
				   }
			   }
			   outer.put("usage_history_recs_row["+j+"]",row.sort())
			   row = new LinkedHashMap<String, String>();
			   j++
		   
		   }
		   return outer.sort()
	   }
	   public  LinkedHashMap md_getUsageHistory2_m(String s)
	   {
		   logi.logInfo("md_getUsageHistory_m")
		   ConnectDB db = new ConnectDB()
		   int j=0
		   LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
		   LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   def plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
		   logi.logInfo("plan_instance_no" +plan_instance_no )
		   String query ="select usg.acct_no as billable_acct_no,usg.plan_instance_no as plan_instance_id,Case when  usg.plan_instance_no IS null then null else pi.PLAN_INSTANCE_CDID end as client_plan_instance_id,usg.usage_type as usage_type_no,utp.description as usage_type_description,TO_CHAR(usg.usage_date ,'YYYY-MM-DD') as usage_date,TO_CHAR(usg.usage_date ,'HH24:MI:SS') as usage_time,usg.BILLABLE_UNITS AS units,usg.BILLABLE_UNITS ||' '||uut.DESCRIPTION||'s' as units_description, uut.DESCRIPTION||'s' as usage_units_description,usg.amt as specific_record_charge_amount,DECODE (usg.EXCLUDE_IND,0, 'false',1,'true') as is_excluded,usg.rate as pre_rated_rate,usg.USAGE_UNITS as recorded_units,usg.REC_NO as usage_rec_no,usg.EXCLUDE_REASON_CD,usg.invoice_no  from ariacore.usage usg  join ariacore.plan_instance pi on pi.plan_instance_no=usg.MASTER_PLAN_INSTANCE_NO and usg.client_no=pi.client_no join ariacore.usage_types utp on usg.usage_type =utp.usage_type join ariacore.USAGE_UNIT_TYPES uut on utp.USAGE_UNIT_TYPE = uut.USAGE_UNIT_TYPE where usg.MASTER_PLAN_INSTANCE_NO = "+plan_instance_no+" and usg.client_no ="+client_no+"order by usg.REC_NO desc"
		   ResultSet rs4=db.executePlaQuery(query)
		   ResultSetMetaData md4 = rs4.getMetaData();
		   int columns4 = md4.getColumnCount();
		   while (rs4.next())
		   {
			   for(int l=1; l<=columns4; l++)
			   {
				   if((rs4.getObject(l))!=null)
				   {
					   
						   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
					   
				   
				   }
			   }
			   outer.put("usage_history_recs_row["+j+"]",row.sort())
			   row = new LinkedHashMap<String, String>();
			   j++
		   
		   }
		   return outer.sort()
	   }
	   public  LinkedHashMap md_getUsageHistoryWithBothMPSP_m(String s)
	   {
		   logi.logInfo("md_getUsageHistory_m")
		   ConnectDB db = new ConnectDB()
		   int j=0
		   LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
		   LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   def plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
		   logi.logInfo("plan_instance_no" +plan_instance_no )
		   String query ="select utp.USAGE_TYPE_CODE,usg.acct_no as billable_acct_no,usg.plan_instance_no as plan_instance_id,Case when  usg.plan_instance_no IS null then null else pi.PLAN_INSTANCE_CDID end as client_plan_instance_id,usg.usage_type as usage_type_no,utp.description as usage_type_description,TO_CHAR(usg.usage_date ,'YYYY-MM-DD') as usage_date,TO_CHAR(usg.usage_date ,'HH24:MI:SS') as usage_time,usg.BILLABLE_UNITS AS units,usg.BILLABLE_UNITS ||' '||uut.DESCRIPTION||'s' as units_description, uut.DESCRIPTION||'s' as usage_units_description,usg.amt as specific_record_charge_amount,DECODE (usg.EXCLUDE_IND,0, 'false',1,'true') as is_excluded,usg.rate as pre_rated_rate,usg.USAGE_UNITS as recorded_units,usg.REC_NO as usage_rec_no,usg.EXCLUDE_REASON_CD,usg.invoice_no  from ariacore.usage usg  join ariacore.plan_instance pi on pi.plan_instance_no=usg.PLAN_INSTANCE_NO and usg.client_no=pi.client_no join ariacore.usage_types utp on usg.usage_type =utp.usage_type join ariacore.USAGE_UNIT_TYPES uut on utp.USAGE_UNIT_TYPE = uut.USAGE_UNIT_TYPE where usg.MASTER_PLAN_INSTANCE_NO = "+plan_instance_no+" and usg.client_no ="+client_no+"order by usg.REC_NO desc"
		   ResultSet rs4=db.executePlaQuery(query)
		   ResultSetMetaData md4 = rs4.getMetaData();
		   int columns4 = md4.getColumnCount();
		   while (rs4.next())
		   {
			   for(int l=1; l<=columns4; l++)
			   {
				   if((rs4.getObject(l))!=null)
				   {
					   
						   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
					   
				   
				   }
			   }
			   outer.put("usage_history_recs_row["+j+"]",row.sort())
			   row = new LinkedHashMap<String, String>();
			   j++
		   
		   }
		   return outer.sort()
	   }
	   
	   /**method for get_aria_xml_statement_m
		* @param something
		* @return XML_docuement
		*/
	   String md_get_aria_xml_statement_m(String input)
	{
		ConnectDB db = new ConnectDB()
		String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		String xml_statement_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/xml_statement_no[1]")
		return db.executeQueryP2("SELECT XML_STATEMENT as XML_STATEMENT from ariacore.XML_DOCUMENT where CLIENT_NO = "+library.Constants.Constant.client_no+" and ACCT_NO = "+acct_no+" and XML_STATEMENT_NO = "+xml_statement_no).toString().trim()
		
	}
	
	/**method for md_MP_contract_no_m
	 * @param no needed
	 */
	String md_MP_contract_no_m(String input)
	{
		return getValueFromResponse(library.Constants.Constant.SERVICENAME, "//*/contract_no[1]").toString()
	}
	
	String md_create_instance_contract_m(String input)
	{
		logi.logInfo "entered md_create_instance_contract_m"
		String val="PASSED";
		String contract_no=md_MP_contract_no_m("input");
		String req_acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		String req_type_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/type_no[1]")
		String req_plan_instance=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/contract_plan_instances[1]/lit:contract_plan_instances_row[1]/lit:plan_instance_no[1]")
		String req_start_date=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/start_date[1]")
		String req_end_date=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/end_date[1]")
		ConnectDB db = new ConnectDB()
		String DB_acct_no=db.executeQueryP2("select ACCT_NO from ariacore.ACCT_MULTI_PLAN_CONTRACTS where contract_no="+contract_no);
		String DB_type_no=db.executeQueryP2("select TYPE_NO from ariacore.ACCT_MULTI_PLAN_CONTRACTS where contract_no="+contract_no);
		String DB_plan_instance=db.executeQueryP2("select PLAN_INSTANCE_NO from ariacore.ACCT_MULTIPLAN_CONTRACT_INST where contract_no="+contract_no);
		String DB_start_date=db.executeQueryP2("select TO_CHAR(START_DATE,'YYYY-MM-DD') from ariacore.ACCT_MULTI_PLAN_CONTRACTS where contract_no="+contract_no);
		String DB_end_date=db.executeQueryP2("select TO_CHAR(END_DATE,'YYYY-MM-DD') from ariacore.ACCT_MULTI_PLAN_CONTRACTS where contract_no="+contract_no);
		if(req_acct_no!=DB_acct_no)val="FAILED"
		if(req_type_no!=DB_type_no)val="FAILED"
		if(req_plan_instance!=DB_plan_instance)val="FAILED"
		if(req_start_date!=DB_start_date)val="FAILED"
		if(req_end_date!=DB_end_date)val="FAILED"
		return val
	}
	
		LinkedHashMap md_get_all_acct_contracts_final_m(String acct_hierarchy)
	{
		LinkedHashMap main =new LinkedHashMap()
		logi.logInfo("into test")
		int count =1
		ConnectDB db = new ConnectDB()
		String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		String  contract_query="Select pi.plan_instance_no as plan_instance_no,ac.CLIENT_CONTRACT_ID,pi.plan_instance_cdid as client_plan_instance_id,cp.plan_name as plan_name,pi.status_cd as plan_instance_status_cd,g.label as plan_instance_status_label,ampc.CONTRACT_NO,ampc.TYPE_NO,ampc.CONTRACT_MONTHS AS LENGTH_MONTHS,ampc.EARLY_CANCEL_FEE AS CANCEL_FEE,TO_CHAR(ampc.CREATE_DATE,'YYYY-MM-DD') as CREATE_DATE,TO_CHAR(ampc.UPDATE_DATE,'YYYY-MM-DD') as UPDATE_DATE,TO_CHAR(ampc.START_DATE,'YYYY-MM-DD') AS START_DATE,TO_CHAR(ampc.END_DATE,'YYYY-MM-DD') AS END_DATE,ampc.STATUS_CD AS STATUS_CODE,ampc.CREATE_COMMENTS from ariacore.plan_instance pi join ariacore.ALL_PLAN_INST_ACTIVE_CONTRACTS ac on pi.plan_instance_no = ac.plan_instance_no and pi.client_no = ac.client_no and pi.acct_no=ac.acct_no join ariacore.client_plan cp on pi.plan_no =cp.plan_no and pi.client_no=cp.client_no join ariacore.global_plan_status_code g on pi.status_cd =g.plan_status_cd join ARIACORE.ACCT_MULTI_PLAN_CONTRACTS ampc on ac.CONTRACT_NO=ampc.CONTRACT_no where pi.acct_no=+"+acct_no+" ORDER BY ampc.CONTRACT_no"
		ResultSet rs_contract_query = db.executePlaQuery(contract_query)

			ResultSetMetaData md1 = rs_contract_query.getMetaData()
			int columns1 = md1.getColumnCount();
			while (rs_contract_query.next())
			{
				LinkedHashMap all_acct_contracts_temp_hm =new LinkedHashMap()
				
				for(int s=1; s<=columns1; ++s)
				{

					if(rs_contract_query.getObject(s).toString().equals("null"))
					{
						logi.logInfo("No value in "+rs_contract_query.getObject(s))
					}
					else
					{
						logi.logInfo("Value inserting in hash map")
						all_acct_contracts_temp_hm.put(md1.getColumnName(s).toLowerCase(),rs_contract_query.getObject(s));
					}
					
		
				}
				all_acct_contracts_temp_hm.put("contract_scope","MULTI PLAN")
				all_acct_contracts_temp_hm=all_acct_contracts_temp_hm.sort()
			main.put("all_acct_contracts_m_row["+count+"]",  all_acct_contracts_temp_hm)
			++count
				}
			
		logi.logInfo ("tedaskjhdask"+main)
		return main
	}
	
	/**
	 * mehtod name md_rec_usage_billed_m
	 * @param no param
	 * @return returns weather record is billed or not bases on mapping of invoice to the usage
	 */
	public String md_rec_usage_billed_m (String input)
	{
		String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		String master_plan_instance=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/master_plan_instance_no[1]")
		ConnectDB db = new ConnectDB()
		ResultSet rs4= db.executePlaQuery("SELECT INVOICE_NO from ariacore.USAGE where acct_no="+acct_no+" and MASTER_PLAN_INSTANCE_NO="+master_plan_instance).toString();
		logi.logInfo "11"
		ResultSetMetaData md4 = rs4.getMetaData();
		logi.logInfo "10"
		int columns4 = md4.getColumnCount();
		logi.logInfo "1"
		String result = " ";
		while (rs4.next())
		{logi.logInfo "2"
			for(int l=1; l<=columns4; l++)
			{ logi.logInfo "3"
				if((rs4.getObject(l))!=null)
				{
					logi.logInfo "4"
						result=rs4.getObject(l).toString();
				
				}
				else result="ORDER NOT BILLED"
			}
			
		}
		if(result!="ORDER NOT BILLED")result="ORDER BILLED"
		return result
	}
	
	def md_MPI_row_CAC_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		ArrayList MPI = new ArrayList();
		ArrayList SPI = new ArrayList();
		String acct_no=getValueFromResponse(library.Constants.Constant.SERVICENAME, "//*/out_acct[1]/ns1:out_acct_row[1]/ns1:acct_no[1]")
		String Query="SELECT pi.plan_instance_no as client_plan_instance_id from ariacore.plan_instance_master pim join ariacore.plan_instance pi on pi.plan_instance_no=pim.plan_instance_no and pim.client_no=pi.client_no  where pi.client_no= "+library.Constants.Constant.client_no+" and pi.acct_no="+acct_no+" order by 1";
		ResultSet resultSet = db.executePlaQuery(Query)
		ResultSetMetaData md = resultSet.getMetaData()
		int columns = md.getColumnCount();
		int loops=1
		while (resultSet.next())
		{
		
			 for(int u=1; u<=columns; ++u)
			{
				if(resultSet.getObject(u)!=null)
					MPI.add(resultSet.getObject(u));
			}
					
		}
		
		for(String MPINO : MPI)
		{
			//String Query1="SELECT pi.plan_instance_no,pi.PLAN_INSTANCE_CDID as client_plan_instance_id from ariacore.plan_instance_master pim join ariacore.plan_instance pi on pi.plan_instance_no=pim.plan_instance_no and pim.client_no=pi.client_no  where pi.client_no= "+library.Constants.Constant.client_no+" and pi.plan_instance_no = "+ MPINO;
			String Query1="SELECT pi.plan_instance_no, pi.PLAN_INSTANCE_CDID AS client_plan_instance_id, o.order_no AS nso_order_no, os.LABEL AS nso_order_status_label FROM ariacore.plan_instance_master pim JOIN ariacore.plan_instance pi ON pi.plan_instance_no  =pim.plan_instance_no AND pim.client_no =pi.client_no LEFT JOIN ARIACORE.ORDERS o ON o.master_plan_instance_no = pim.plan_instance_no LEFT JOIN ARIACORE.ORDER_STATUSES os on o.STATUS_CD=os.STATUS_CD  where pi.client_no= "+library.Constants.Constant.client_no+" and pi.plan_instance_no = "+ MPINO;
			ResultSet resultSet1 =db.executePlaQuery(Query1)
			ResultSetMetaData md1 = resultSet1.getMetaData()
			int columns1 = md1.getColumnCount();
			 while (resultSet1.next())
		   {
			   for(int l=1; l<=columns1; l++)
			   {
				   
				   if((resultSet1.getObject(l))!=null)
				   {
							
							boolean is_having=true
							int k=1;
							while (is_having)
							{
								if(k==1)
								{if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase())){is_having=false}}
								else {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase()+"["+k.toString()+"]")){is_having=false}}
								k+=1;
							}
							k-=1;
							 if(k>1){line_item.put((md1.getColumnName(l)).toLowerCase()+"["+k.toString()+"]",(resultSet1.getObject(l)));}
							 else{   line_item.put((md1.getColumnName(l)).toLowerCase(),(resultSet1.getObject(l)));}
							
				   }
			   }
		   }
		   
		   Query1="SELECT PLAN_UNIT_INSTANCE_NO as out_plan_unit_inst_no ,PLAN_UNIT_INSTANCE_CDID as out_client_plan_unit_inst_id  from ariacore.PLAN_INST_UNIT_INSTANCES piui where  client_no= "+library.Constants.Constant.client_no+" and piui.PLAN_INSTANCE_NO = "+ MPINO;
		   resultSet1 =db.executePlaQuery(Query1)
		   md1 = resultSet1.getMetaData()
		   columns1 = md1.getColumnCount();
			while (resultSet1.next())
		  {
			  for(int l=1; l<=columns1; l++)
			  {
				  
				  if((resultSet1.getObject(l))!=null)
				  {
						   
						   boolean is_having=true
						   int k=1;
						   while (is_having)
						   {
							   if(k==1)
							   {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase())){is_having=false}}
							   else {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase()+"["+k.toString()+"]")){is_having=false}}
							   k+=1;
						   }
						   k-=1;
							if(k>1){line_item.put((md1.getColumnName(l)).toLowerCase()+"["+k.toString()+"]",(resultSet1.getObject(l)));}
							else{   line_item.put((md1.getColumnName(l)).toLowerCase(),(resultSet1.getObject(l)));}
						   
				  }
			  }
			}
		  
		  Query1="select pi.plan_instance_no as supp_plan_instance_no from ariacore.plan_instance pi  where pi.client_no= "+library.Constants.Constant.client_no+" and pi.PARENT_PLAN_INSTANCE_NO = "+ MPINO;
		  resultSet1 =db.executePlaQuery(Query1)
		  md1 = resultSet1.getMetaData()
		  columns1 = md1.getColumnCount();
		   while (resultSet1.next())
		 {
			 for(int l=1; l<=columns1; l++)
			 {
				 
				 if((resultSet1.getObject(l))!=null)
				 {
						 SPI.add(resultSet1.getObject(l))
				 }
			 }
		 }
		 logi.logInfo "SPI list "+SPI
		 for(String SPINO : SPI)
		 {
			 Query1="SELECT pi.plan_instance_no as supp_plan_instance_no,pi.PLAN_INSTANCE_CDID as client_plan_instance_id from ariacore.plan_instance pi  where pi.client_no = "+library.Constants.Constant.client_no+" and pi.PLAN_INSTANCE_NO = "+ SPINO +" and pi.parent_plan_instance_no = "+MPINO;
			 resultSet1 =db.executePlaQuery(Query1)
			 md1 = resultSet1.getMetaData()
			 columns1 = md1.getColumnCount();
			  while (resultSet1.next())
			{
				for(int l=1; l<=columns1; l++)
				{
					
					if((resultSet1.getObject(l))!=null)
					{
							 
							 boolean is_having=true
							 int k=1;
							 while (is_having)
							 {
								 if(k==1)
								 {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase())){is_having=false}}
								 else {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase()+"["+k.toString()+"]")){is_having=false}}
								 k+=1;
							 }
							 k-=1;
							  if(k>1){line_item.put((md1.getColumnName(l)).toLowerCase()+"["+k.toString()+"]",(resultSet1.getObject(l)));}
							  else{   line_item.put((md1.getColumnName(l)).toLowerCase(),(resultSet1.getObject(l)));}
							 
					}
				}
			 }
			 
			 Query1="SELECT PLAN_UNIT_INSTANCE_NO as out_plan_unit_inst_no ,PLAN_UNIT_INSTANCE_CDID as out_client_plan_unit_inst_id  from ariacore.PLAN_INST_UNIT_INSTANCES piui join ariacore.plan_instance pi on pi.plan_instance_no = piui.plan_instance_no and piui.client_no=pi.client_no  where pi.client_no = "+library.Constants.Constant.client_no+" and piui.PLAN_INSTANCE_NO = "+ SPINO;
			 resultSet1 =db.executePlaQuery(Query1)
			 md1 = resultSet1.getMetaData()
			 columns1 = md1.getColumnCount();
			  while (resultSet1.next())
			{
				for(int l=1; l<=columns1; l++)
				{
					
					if((resultSet1.getObject(l))!=null)
					{
							 
							 boolean is_having=true
							 int k=1;
							 while (is_having)
							 {
								 if(k==1)
								 {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase())){is_having=false}}
								 else {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase()+"["+k.toString()+"]")){is_having=false}}
								 k+=1;
							 }
							 k-=1;
							  if(k>1){line_item.put((md1.getColumnName(l)).toLowerCase()+"["+k.toString()+"]",(resultSet1.getObject(l)));}
							  else{   line_item.put((md1.getColumnName(l)).toLowerCase(),(resultSet1.getObject(l)));}
							 
					}
				}
			 }
		 
		 }
		 SPI = new ArrayList();
		  
		   line_item.sort();
		   row_item.put("master_plans_assigned_row["+(loops)+"]",line_item.sort())
		   line_item = new LinkedHashMap();
		   loops+=1;
		}
		
		row_item.sort();
		return row_item
		
	}

	/** method to verify get_acct_credits_m API response for acct_no,client_acct_id,master_plan_instance_no,client_master_plan_instance_id,limit_records input combinations
	 * @param any string
	 * @return list of cash credits & service credits for the specified input
	 */
	public  LinkedHashMap md_get_Acct_Credits_m(String s)
	{
		logi.logInfo("In md_get_Acct_Credits_m ")
		int a=0
		def mpi_no=null
		def acct_no
		def lmt=null
		ConnectDB db = new ConnectDB()
		LinkedHashMap <String, String> gacrow = new LinkedHashMap<String, String>();
		LinkedHashMap<String, HashMap> gacouter = new LinkedHashMap<String, HashMap>();
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		def mpi_id=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/client_master_plan_instance_id")
		def cl_acc_id=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/client_acct_id")
		lmt=(getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/limit_records").equals('NoVal'))? lmt : getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/limit_records")
		
		if(!cl_acc_id==null)
		{
			String acctnoquery="SELECT ACCT_NO FROM ARIACORE.ACCT WHERE CLIENT_ACCT_ID='"+cl_acc_id
			acct_no=db.executeQueryP2(acctnoquery)
		}
		else
			acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")

		if(!mpi_id==null)
		{
			String mpiquery="SELECT PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_CDID='"+mpi_id
			mpi_no=db.executeQueryP2(mpiquery)
		}

		else if(!mpi_no==null)
			mpi_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/master_plan_instance_no")

		String acctcdtsqry ="SELECT * FROM (SELECT CCDT.ACCT_NO AS OUT_ACCT_NO,NULL AS OUT_MASTER_PLAN_INSTANCE_NO,NULL AS OUT_CLIENT_MP_INSTANCE_ID,CCDT.CREDIT_NO,CCDT.CREATE_USER AS CREATED_BY,TO_CHAR(CCDT.CREATE_DATE,'YYYY-MM-DD') AS CREATED_DATE,'C' AS CREDIT_TYPE,CCDT.REASON_CD AS REASON_CODE,CR.TEXT AS REASON_TEXT FROM ARIACORE.CASH_CREDITS CCDT JOIN ARIACORE.CREDIT_REASONS CR ON CR.REASON_CD=CCDT.REASON_CD WHERE CCDT.ACCT_NO="+acct_no+" AND CCDT.CLIENT_NO="+client_no+" UNION SELECT CDT.ACCT_NO AS OUT_ACCT_NO,CDT.MASTER_PLAN_INSTANCE_NO AS OUT_MASTER_PLAN_INSTANCE_NO,PI.PLAN_INSTANCE_CDID AS OUT_CLIENT_MP_INSTANCE_ID,CDT.CREDIT_ID AS CREDIT_NO,CDT.CREATE_USER AS CREATED_BY,TO_CHAR(CDT.CREATE_DATE,'YYYY-MM-DD') AS CREATED_DATE,'S' AS CREDIT_TYPE,CDT.REASON_CD AS REASON_CODE,CR.TEXT AS REASON_TEXT FROM ARIACORE.CREDITS CDT LEFT OUTER JOIN ARIACORE.PLAN_INSTANCE PI ON PI.ACCT_NO=CDT.ACCT_NO AND PI.CLIENT_NO=CDT.CLIENT_NO  AND CDT.MASTER_PLAN_INSTANCE_NO = PI.PLAN_INSTANCE_NO JOIN ARIACORE.CREDIT_REASONS CR ON CR.REASON_CD=CDT.REASON_CD WHERE CDT.ACCT_NO="+acct_no+" AND NVL(CDT.MASTER_PLAN_INSTANCE_NO,-1)=NVL("+mpi_no+", NVL(CDT.MASTER_PLAN_INSTANCE_NO,-1))AND CDT.CLIENT_NO="+client_no+" ORDER BY CREDIT_NO DESC) WHERE ROWNUM<=NVL("+lmt+",1000)"
		ResultSet gacrs=db.executePlaQuery(acctcdtsqry)
		ResultSetMetaData gacrsmd = gacrs.getMetaData();

		while(gacrs.next())
		{
			for(int e=1;e<=gacrsmd.getColumnCount();e++)
			{
				if((gacrs.getObject(e))!=null)
					gacrow.put((gacrsmd.getColumnName(e)).toLowerCase(),(gacrs.getObject(e)));

			}

			gacouter.put("all_credits_row["+a+++"]",gacrow.sort())
			gacrow = new LinkedHashMap<String, String>();

		}

		return gacouter.sort()
	}
	
	public String md_invoice_status_in_DB_duplicate_m (String input)
	{
		//this method is only for usage please dont use this
		ConnectDB db = new ConnectDB()
		String invoice_status=db.executeQueryP2("select count(VOIDING_EVENT_NO) from ariacore.ALL_INVOICES where invoice_no="+im.md_get_invoice_duplicate_no_m(input,null)).toString();
		logi.logInfo  ""
		if(invoice_status!="0") return 'VOIDED INVOICE'
		invoice_status=db.executeQueryP2("select REAL_PEND_IND from ariacore.ALL_INVOICES where invoice_no="+im.md_get_invoice_duplicate_no_m(input,null)).toString();
		logi.logInfo  ""
		if(invoice_status=="R") return 'REAL INVOICE'
		else if(invoice_status=="P") return 'PENDING INVOICE'
		else
		return 'NO SUCH INVOICE'
		
		
	}
	
	
	def md_VTdate_1_m(String numberOfMonths)
	{
		logi.logInfo "into md_VTdate_1_m "+numberOfMonths
		
		db = new ConnectDB();
		Date date = new Date();
		Calendar now = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

		String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + Constant.client_no + ") from DUAL";
		ResultSet resultSet = db.executeQuery(Query);
		while(resultSet.next()){
			// Get only the current Date

			date = resultSet.getDate(1);
		}
		now.setTime(date);
		if(numberOfMonths.contains("-")){
			if(numberOfMonths.contains(".")) {
				String[] days = numberOfMonths.split("\\.");
				now.add(Calendar.MONTH, Integer.parseInt(days[0]));
				now.add(Calendar.DATE, -(Integer.parseInt(days[1])));
				now.add(Calendar.DATE, -1);
			}
			else
				{now.add(Calendar.DATE,Integer.parseInt(numberOfMonths));
				now.add(Calendar.DATE, -1);}
		}
		else{
			if(numberOfMonths.contains(".")) {
				String[] days = numberOfMonths.split("\\.");
				now.add(Calendar.MONTH, Integer.parseInt(days[0]));
				now.add(Calendar.DATE, Integer.parseInt(days[1]));
				now.add(Calendar.DATE, -1);
			}
			else
				{now.add(Calendar.DATE, Integer.parseInt(numberOfMonths));
				now.add(Calendar.DATE, -1);}
		}

		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String date1 = format1.format(now.getTime());
		//db.closeConnection()
		return date1;
	}
	
	def md_VTdate_m(String numberOfDays)
	{
		logi.logInfo "into md_daysVT_m "+numberOfDays
		return im.md_VTdate_m(numberOfDays,null)
	}
	
	
	LinkedHashMap md_Get_all_acct_contracts_inst_method_m(String acct_hierarchy1)
	{
		
		
		String acct_hierarchy=acct_hierarchy1.split("#")[0]
		String seq=acct_hierarchy1.split("#")[1]
		String seq_contract=acct_hierarchy1.split("#")[2]
		
		
		
		LinkedHashMap all_acct_contracts_temp_hm =new LinkedHashMap()
		LinkedHashMap a1 =new LinkedHashMap()
		ConnectDB db = new ConnectDB()
		int count=0
		String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		logi.logInfo("Given Acct no:" + acct_no)

		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		//for(int start=1;start<=count;start++)
		//{
			//RESULT SET


			
			String contract_no_query="SELECT A.CONTRACT_NO FROM (SELECT  ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE ACCT_NO="+acct_no+") A WHERE A.SEQ="+seq
			String contract=db.executeQueryP2(contract_no_query)

			logi.logInfo("Contract number : "+contract)
			
			String plan_instance_query="select a.plan_instance_no,a.client_plan_instance_id,a.plan_name, a.plan_instance_status_cd,a.plan_instance_status_label from (Select row_number() over (order by  pi.plan_instance_no) as seq ,  pi.plan_instance_no as plan_instance_no,pi.plan_instance_cdid as client_plan_instance_id,cp.plan_name as plan_name,pi.status_cd as plan_instance_status_cd,g.label as plan_instance_status_label from ariacore.plan_instance pi join ariacore.ALL_PLAN_INST_ACTIVE_CONTRACTS ac on pi.plan_instance_no = ac.plan_instance_no and pi.client_no = ac.client_no and pi.acct_no=ac.acct_no join ariacore.client_plan cp on pi.plan_no =cp.plan_no and pi.client_no=cp.client_no join ariacore.global_plan_status_code g on pi.status_cd =g.plan_status_cd  where pi.acct_no ="+acct_no+"and ac.CONTRACT_NO ="+contract+") A where A.seq="+seq_contract
			ResultSet rs2 = db.executePlaQuery(plan_instance_query)
			ResultSetMetaData md2 = rs2.getMetaData();
			int columns3 = md2.getColumnCount();
			int r=1
			while (rs2.next())
			{all_acct_contracts_temp_hm =new LinkedHashMap()
				
				for(int j=1; j<=columns3; j++)
							{
								if((rs2.getObject(j))!=null)
								{
									//if(r==1)
									//{
										//all_acct_contracts_temp_hm.put((md2.getColumnName(j)).toLowerCase(),(rs2.getObject(j)));
									all_acct_contracts_temp_hm.put((md2.getColumnName(j)).toLowerCase(),(rs2.getObject(j)));
									/*}
									else
									{
										all_acct_contracts_temp_hm.put((md2.getColumnName(j)).toLowerCase() +"["+r+"]",

(rs2.getObject(j)));
									}
									*/
								}
							}
							r++
							all_acct_contracts_temp_hm=all_acct_contracts_temp_hm.sort()
							a1.put("contract_plan_instance_info_row["+count+"]",all_acct_contracts_temp_hm )
							++count
							
							//all_acct_contracts_temp_hm =new LinkedHashMap()
			}
			

			
						logi.logInfo "**** out of  get_all_acct_contracts_queries_m **** "
		//}
		
		logi.logInfo("temp1")
		//logi.logInfo("csdsadada"+all_acct_contracts_temp_hm)
		//a1.put("contract_plan_instance_info_row["+count+"]",all_acct_contracts_temp_hm )
		logi.logInfo("temp2")
		//all_acct_contracts_temp_hm=all_acct_contracts_temp_hm.sort()
		//a1=a1.sort()
		
		//return all_acct_contracts_temp_hm
		//return a1
		return all_acct_contracts_temp_hm
	}
	
		
	/**
	 * method md_GetAllNestedChildNodesasList_m
	 * @param any parent node
	 * @return nested hash map
	 *
	 */
	def md_GetAllNestedChildNodesasList_m(String parentnode) throws Exception{
		LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
		LinkedHashMap<String, HashMap> outer1 = new LinkedHashMap<String, HashMap>();
		logi.logInfo "the parent node :"+parentnode
		int counts=Integer.parseInt(md_get_node_count_m(parentnode.split(",")[0]));
		logi.logInfo "the count is here :"+counts
		for(int i=0;i<counts;i++)
		{
			outer=(md_getAllnodesAsList_m(parentnode,(i+1)))
			outer=outer.sort()
			outer1.put(parentnode.split(",")[0].replace("ns1:","")+"["+(i)+"]",outer)
			outer = new LinkedHashMap<String, String>();
		}
		outer1=outer1.sort()
		return outer1
	}
	
	/**
	 * for objQuery get_plan_instance_information_m
	 * @param no param required
	 * @return hashmap
	 */
	public def md_get_plan_instance_information_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		String query_string=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/query_string[1]")
		int offset = 0;
		int limit = 100;
		String offset_s =getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/offset[1]")
		String limit_s = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/limit[1]")
		if(!offset_s.contains("NoVal"))offset=Integer.parseInt(offset_s);
		if(!limit_s.contains("NoVal"))limit=Integer.parseInt(limit_s);
		query_string = query_string.replaceAll("acct_no","act.acct_no");
		query_string = query_string.replaceAll("user_id","act.userid");
		query_string = query_string.replaceAll("client_acct_id","act.client_acct_id");
		query_string = query_string.replaceAll("plan_instance_no","pi.plan_instance_no");
		query_string = query_string.replaceAll("client_plan_instance_id","pi.plan_instance_cdid");
		query_string = query_string.replaceAll("product_field_name","cosf.FIELD_NAME");
		query_string = query_string.replaceAll("product_field_value","pipf.VALUE_TEXT");
		query_string = query_string.replaceAll("plan_no","pi.plan_no");
		query_string = query_string.replaceAll("client_plan_id","cp.CLIENT_PLAN_ID");
		query_string = query_string.replaceAll("last_bill_date","pi.last_bill_date");
		query_string = query_string.replaceAll("next_bill_date","pi.next_bill_date");
		query_string = query_string.replaceAll("bill_thru_date","pi.LAST_BILL_THRU_DATE");
		query_string = query_string.replaceAll("status_cd","pi.status_cd");
		query_string = query_string.replaceAll("master_plan_instance_balance","pim.STACK_BALANCE");
		query_string = query_string.replaceAll("\"","");
		query_string = query_string.replaceAll("= ","= '");
		query_string = query_string.replaceAll("> ","> '");
		query_string = query_string.replaceAll("< ","< '");
		query_string = query_string.replaceAll(" and","' and");
		query_string += "'";
		
		ArrayList MPIS = new ArrayList();
		ResultSet rs
		if(query_string.contains("cosf.FIELD")||query_string.contains("pipf.VALUE"))
		{rs = db.executePlaQuery("SELECT pi.plan_instance_no from ariacore.acct act join ariacore.plan_instance pi on pi.acct_no=act.acct_no and pi.client_no=act.client_no join ariacore.client_plan cp on cp.client_no=act.client_no and cp.plan_no=pi.plan_no join ariacore.plan_instance_master pim on pim.client_no=cp.client_no and pim.PLAN_INSTANCE_NO=pi.PLAN_INSTANCE_NO join ariacore.acct_billing_group abg on abg.client_no=pi.client_no and abg.billing_group_no=pim.billing_group_no and abg.acct_no=pi.acct_no join ariacore.PLAN_INST_PRODUCT_FIELDS_VAL pipf on pipf.client_no=act.client_no and pipf.plan_instance_no=pi.plan_instance_no join ariacore.client_obj_supp_fields cosf on cosf.client_no=pipf.client_no and pipf.field_no=cosf.field_no where act.client_no = "+library.Constants.Constant.client_no+" and "+query_string)}
		else{rs = db.executePlaQuery("SELECT pi.plan_instance_no from ariacore.acct act join ariacore.plan_instance pi on pi.acct_no=act.acct_no and pi.client_no=act.client_no join ariacore.client_plan cp on cp.client_no=act.client_no and cp.plan_no=pi.plan_no join ariacore.plan_instance_master pim on pim.client_no=cp.client_no and pim.PLAN_INSTANCE_NO=pi.PLAN_INSTANCE_NO join ariacore.acct_billing_group abg on abg.client_no=pi.client_no and abg.billing_group_no=pim.billing_group_no and abg.acct_no=pi.acct_no where act.client_no = "+library.Constants.Constant.client_no+" and "+query_string)}
		
		ResultSetMetaData meta = rs.getMetaData()
		int columns = meta.getColumnCount();
		while (rs.next())
		{
			for(int u=1; u<=columns; ++u)
			{
				if(rs.getObject(u)!=null)
					MPIS.add(rs.getObject(u));
			}
		}
		Collections.sort(MPIS);
		
		logi.logInfo  "MPIs in the list "+MPIS
		int loops=1,loopss=1;
		for(String MPI : MPIS)
		{
			if(loopss>offset)
			{
				String Query="SELECT act.acct_no,act.userid as user_id,act.client_acct_id,pi.plan_instance_no,pi.plan_instance_cdid as client_plan_instance_id,pi.plan_no,cp.CLIENT_PLAN_ID,cp.plan_name,pi.plan_units,TO_CHAR(pi.last_bill_date, 'YYYY-MM-DD HH24:MI:SSxFF') as last_bill_date,TO_CHAR(pi.next_bill_date, 'YYYY-MM-DD HH24:MI:SSxFF') as next_bill_date,TO_CHAR(pi.LAST_BILL_THRU_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as bill_thru_date,pi.status_cd,TO_CHAR(pi.status_date, 'YYYY-MM-DD HH24:MI:SSxFF') as status_date,pi.SCHEDULE_NO,pim.STACK_BALANCE as master_plan_instance_balance,pim.BILLING_GROUP_NO,abg.profile_cdid as client_billing_group_id,pim.dunning_group_no from ariacore.acct act join ariacore.plan_instance pi on pi.acct_no=act.acct_no and pi.client_no=act.client_no join ariacore.client_plan cp on cp.client_no=act.client_no and cp.plan_no=pi.plan_no join ariacore.plan_instance_master pim on pim.client_no=cp.client_no and pim.PLAN_INSTANCE_NO=pi.PLAN_INSTANCE_NO join ariacore.acct_billing_group abg on abg.client_no=pi.client_no and abg.billing_group_no=pim.billing_group_no and abg.acct_no=pi.acct_no where act.client_no = "+library.Constants.Constant.client_no+" and pi.plan_instance_no = "+MPI;
		ResultSet resultSet = db.executePlaQuery(Query)
		ResultSetMetaData md = resultSet.getMetaData()
		columns = md.getColumnCount();
		
		while (resultSet.next())
		{
			for(int u=1; u<=columns; ++u)
			{
				if(resultSet.getObject(u)!=null)
					line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
			}
		}
			String Query1="select pipf.FIELD_NO as product_field_no,cosf.DESCRIPTION as product_field_desc,cosf.FIELD_NAME as product_field_name,pipf.VALUE_TEXT as product_field_value from ariacore.PLAN_INST_PRODUCT_FIELDS_VAL pipf join ariacore.client_obj_supp_fields cosf on cosf.client_no=pipf.client_no and pipf.field_no=cosf.field_no where pipf.client_no="+library.Constants.Constant.client_no+" and pipf.PLAN_INSTANCE_NO = "+MPI;
			ResultSet resultSet1 =db.executePlaQuery(Query1)
			ResultSetMetaData md1 = resultSet1.getMetaData()
			int columns1 = md1.getColumnCount();
			 while (resultSet1.next())
		   {
			   for(int l=1; l<=columns1; l++)
			   {
				   
				   if((resultSet1.getObject(l))!=null)
				   {
							
							boolean is_having=true
							int k=1;
							while (is_having)
							{
								if(k==1)
								{if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase())){is_having=false}}
								else {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase()+"["+k.toString()+"]")){is_having=false}}
								k+=1;
							}
							k-=1;
							logi.logInfo "the value of k "+k;
							logi.logInfo "putting in output "+resultSet1.getObject(l);
							 if(k>1){line_item.put((md1.getColumnName(l)).toLowerCase()+"["+k.toString()+"]",(resultSet1.getObject(l)));}
							 else{   line_item.put((md1.getColumnName(l)).toLowerCase(),(resultSet1.getObject(l)));}
							
				   }
			   }
			 }
		  
		   Query1="SELECT aa.FIRST_NAME as billing_first_name,aa.MIDDLE_INITIAL as billing_middle_initial, aa.LAST_NAME as billing_last_name,aa.ADDRESS1 as billing_address1,aa.ADDRESS2 as billing_address2, aa.city as billing_city,aa.state as billing_state,aa.locality as billing_locality,aa.zip as billing_zip,aa.country as billing_country,aa.INTL_PHONE as billing_intl_phone,aa.EMAIL as billing_email,bi.PAY_METHOD AS pay_method_type,bi.CC_EXPIRE_MM,bi.CC_EXPIRE_YYYY,bi.BANK_ROUTING_NUM,bi.SUFFIX AS payment_instrument_suffix,bi.PAYMENT_METHOD_NAME  AS primary_payment_method_name,bi.PAYMENT_METHOD_DESCRIPTION ,bi.PAYMENT_METHOD_CDID   from ariacore.ACCT_ADDRESS aa join ariacore.billing_info bi on bi.client_no=aa.client_no and bi.acct_no=aa.acct_no join ariacore.acct_billing_group abg on abg.client_no=bi.client_no and abg.acct_no=bi.acct_no and abg.PRIMARY_PMT_METHOD_SEQ = bi.seq_num   and abg.PRIMARY_PMT_METHOD_SEQ=aa.SOURCE_NO   join ariacore.plan_instance_master pim on pim.client_no=abg.client_no and pim.billing_group_no=abg.billing_group_no where  aa.client_no = "+library.Constants.Constant.client_no+" AND pim.plan_instance_no = "+MPI;
		   resultSet1 =db.executePlaQuery(Query1)
		   md1 = resultSet1.getMetaData()
		   int test = 0;
			columns1 = md1.getColumnCount();
			while (resultSet1.next())
		  {
			  for(int l=1; l<=columns1; l++)
			  {
				  
				  if((resultSet1.getObject(l))!=null)
				  {
						   test+=1;
						   boolean is_having=true
						   int k=1;
						   while (is_having)
						   {
							   if(k==1)
							   {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase())){is_having=false}}
							   else {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase()+"["+k.toString()+"]")){is_having=false}}
							   k+=1;
						   }
						   k-=1;
						   logi.logInfo "the value of k "+k;
						   logi.logInfo "putting in output "+resultSet1.getObject(l);
							if(k>1){
								if((md1.getColumnName(l)).toLowerCase().equals("payment_method_description"))
								{line_item.put(("primary_payment_method_description"+"["+k.toString()+"]"),(resultSet1.getObject(l)));}
								else if((md1.getColumnName(l)).toLowerCase().equals("payment_method_cdid"))
								{line_item.put(("primary_payment_method_client_defined_id"+"["+k.toString()+"]"),(resultSet1.getObject(l)));}
								else{line_item.put((md1.getColumnName(l)).toLowerCase()+"["+k.toString()+"]",(resultSet1.getObject(l)));}
								}
							else{
								if((md1.getColumnName(l)).toLowerCase().equals("payment_method_description"))
								{line_item.put(("primary_payment_method_description"),(resultSet1.getObject(l)));}
								else if((md1.getColumnName(l)).toLowerCase().equals("payment_method_cdid"))
								{line_item.put(("primary_payment_method_client_defined_id"),(resultSet1.getObject(l)));}
								else { line_item.put((md1.getColumnName(l)).toLowerCase(),(resultSet1.getObject(l)));}
							}
						   
				  }
			  }
			}
		  
		  if(test==0)
	   {
		   logi.logInfo "no address in the acct "
		   Query1="SELECT bi.PAY_METHOD AS pay_method_type,bi.CC_EXPIRE_MM,bi.CC_EXPIRE_YYYY,bi.BANK_ROUTING_NUM,bi.SUFFIX AS payment_instrument_suffix,bi.PAYMENT_METHOD_NAME  AS primary_payment_method_name,bi.PAYMENT_METHOD_DESCRIPTION ,bi.PAYMENT_METHOD_CDID   from  ariacore.billing_info bi  join ariacore.acct_billing_group abg on abg.client_no=bi.client_no and abg.acct_no=bi.acct_no and abg.PRIMARY_PMT_METHOD_SEQ = bi.seq_num  join ariacore.plan_instance_master pim on pim.client_no=abg.client_no and pim.billing_group_no=abg.billing_group_no where  bi.client_no = "+library.Constants.Constant.client_no+" AND pim.plan_instance_no = "+MPI;
		   resultSet1 =db.executePlaQuery(Query1)
		   md1 = resultSet1.getMetaData()
		   columns1 = md1.getColumnCount();
		   while (resultSet1.next())
		   {
			   for(int l=1; l<=columns1; l++)
			   {
				   
				   if((resultSet1.getObject(l))!=null)
				   {
							
							boolean is_having=true
							int k=1;
							while (is_having)
							{
								if(k==1)
								{if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase())){is_having=false}}
								else {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase()+"["+k.toString()+"]")){is_having=false}}
								k+=1;
							}
							k-=1;
							logi.logInfo "the value of k "+k;
							logi.logInfo "putting in output "+resultSet1.getObject(l);
							 if(k>1){
								 if((md1.getColumnName(l)).toLowerCase().equals("payment_method_description"))
								 {line_item.put(("primary_payment_method_description"+"["+k.toString()+"]"),(resultSet1.getObject(l)));}
								 else if((md1.getColumnName(l)).toLowerCase().equals("payment_method_cdid"))
								 {line_item.put(("primary_payment_method_client_defined_id"+"["+k.toString()+"]"),(resultSet1.getObject(l)));}
								 else{line_item.put((md1.getColumnName(l)).toLowerCase()+"["+k.toString()+"]",(resultSet1.getObject(l)));}
								 }
							 else{
								 if((md1.getColumnName(l)).toLowerCase().equals("payment_method_description"))
								 {line_item.put(("primary_payment_method_description"),(resultSet1.getObject(l)));}
								 else if((md1.getColumnName(l)).toLowerCase().equals("payment_method_cdid"))
								 {line_item.put(("primary_payment_method_client_defined_id"),(resultSet1.getObject(l)));}
								 else { line_item.put((md1.getColumnName(l)).toLowerCase(),(resultSet1.getObject(l)));}
							 }
							
				   }
			   }
			 }
		   
	   }
		  
		  Query1="SELECT aa.FIRST_NAME as bkup_billing_first_name,aa.MIDDLE_INITIAL as bkup_billing_middle_initial, aa.LAST_NAME as bkup_billing_last_name,aa.ADDRESS1 as bkup_billing_address1,aa.ADDRESS2 as bkup_billing_address2, aa.city as bkup_billing_city,aa.state as bkup_billing_state,aa.locality as bkup_billing_locality,aa.zip as bkup_billing_zip,aa.country as bkup_billing_country,aa.INTL_PHONE as bkup_billing_intl_phone,aa.EMAIL as bkup_billing_email,bi.PAY_METHOD AS bkup_pay_method_type,bi.CC_EXPIRE_MM as bkup_cc_expire_mm,bi.CC_EXPIRE_YYYY as bkup_cc_expire_yyyy,bi.BANK_ROUTING_NUM as bkup_bank_routing_num,bi.SUFFIX AS bkup_payment_instrument_suffix,bi.PAYMENT_METHOD_NAME  AS backup_payment_method_name,bi.PAYMENT_METHOD_DESCRIPTION ,bi.PAYMENT_METHOD_CDID   from ariacore.ACCT_ADDRESS aa join ariacore.billing_info bi on bi.client_no=aa.client_no and bi.acct_no=aa.acct_no join ariacore.acct_billing_group abg on abg.client_no=bi.client_no and abg.acct_no=bi.acct_no and abg.SECONDARY_PMT_METHOD_SEQ = bi.seq_num and abg.SECONDARY_PMT_METHOD_SEQ=aa.SOURCE_NO join ariacore.plan_instance_master pim on pim.client_no=abg.client_no and pim.billing_group_no=abg.billing_group_no where  aa.client_no ="+library.Constants.Constant.client_no+" AND pim.plan_instance_no = "+MPI;
		  resultSet1 =db.executePlaQuery(Query1)
		  md1 = resultSet1.getMetaData()
		  columns1 = md1.getColumnCount();
		   while (resultSet1.next())
		 {
			 for(int l=1; l<=columns1; l++)
			 {
				 
				 if((resultSet1.getObject(l))!=null)
				 {
						  
						  boolean is_having=true
						  int k=1;
						  while (is_having)
						  {
							  if(k==1)
							  {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase())){is_having=false}}
							  else {if(!line_item.keySet().asList().contains(md1.getColumnName(l).toLowerCase()+"["+k.toString()+"]")){is_having=false}}
							  k+=1;
						  }
						  k-=1;
						  logi.logInfo "the value of k "+k;
						  logi.logInfo "putting in output "+resultSet1.getObject(l);
						   if(k>1){
							   if((md1.getColumnName(l)).toLowerCase().equals("payment_method_description"))
							   {line_item.put(("backup_payment_method_description"+"["+k.toString()+"]"),(resultSet1.getObject(l)));}
								   if((md1.getColumnName(l)).toLowerCase().equals("payment_method_cdid"))
							   {line_item.put(("backup_payment_method_client_defined_id"+"["+k.toString()+"]"),(resultSet1.getObject(l)));}
							   else{line_item.put((md1.getColumnName(l)).toLowerCase()+"["+k.toString()+"]",(resultSet1.getObject(l)));}
							   }
						   else{
							   if((md1.getColumnName(l)).toLowerCase().equals("payment_method_description"))
							   {line_item.put(("backup_payment_method_description"),(resultSet1.getObject(l)));}
								   if((md1.getColumnName(l)).toLowerCase().equals("payment_method_cdid"))
							   {line_item.put(("backup_payment_method_client_defined_id"),(resultSet1.getObject(l)));}
							   else {line_item.put((md1.getColumnName(l)).toLowerCase(),(resultSet1.getObject(l)));}
							   
							   }
						  
				 }
			 }
		   }
		  line_item.sort();
		  row_item.put("plan_instance_details_row["+(loops-1)+"]",line_item.sort())
		  line_item = new LinkedHashMap();
		  if(loops==limit)break;
		  loops+=1;
		  
			}
			loopss+=1;
		}
		row_item=row_item.sort();
		return row_item
		
	}
	
	/**
	 * for objQuery md_get_plan_instance_information_total_m
	 * @param no param required
	 * @return total records to be in the response
	 */
	public String md_get_plan_instance_information_total_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		String query_string=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/query_string[1]")
		int offset = 0;
		int limit = 100;
		String offset_s =getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/offset[1]")
		String limit_s = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/limit[1]")
		if(!offset_s.contains("NoVal"))offset=Integer.parseInt(offset_s);
		if(!limit_s.contains("NoVal"))limit=Integer.parseInt(limit_s);
		query_string = query_string.replaceAll("acct_no","act.acct_no");
		query_string = query_string.replaceAll("user_id","act.userid");
		query_string = query_string.replaceAll("client_acct_id","act.client_acct_id");
		query_string = query_string.replaceAll("plan_instance_no","pi.plan_instance_no");
		query_string = query_string.replaceAll("client_plan_instance_id","pi.plan_instance_cdid");
		query_string = query_string.replaceAll("product_field_name","cosf.FIELD_NAME");
		query_string = query_string.replaceAll("product_field_value","pipf.VALUE_TEXT");
		query_string = query_string.replaceAll("plan_no","pi.plan_no");
		query_string = query_string.replaceAll("client_plan_id","cp.CLIENT_PLAN_ID");
		query_string = query_string.replaceAll("last_bill_date","pi.last_bill_date");
		query_string = query_string.replaceAll("next_bill_date","pi.next_bill_date");
		query_string = query_string.replaceAll("bill_thru_date","pi.LAST_BILL_THRU_DATE");
		query_string = query_string.replaceAll("status_cd","pi.status_cd");
		query_string = query_string.replaceAll("master_plan_instance_balance","pim.STACK_BALANCE");
		query_string = query_string.replaceAll("\"","");
		query_string = query_string.replaceAll("= ","= '");
		query_string = query_string.replaceAll("> ","> '");
		query_string = query_string.replaceAll("< ","< '");
		query_string = query_string.replaceAll(" and","' and");
		query_string += "'";
		ArrayList MPIS = new ArrayList();
		ResultSet rs
		if(query_string.contains("cosf.FIELD")||query_string.contains("pipf.VALUE"))
		{rs = db.executePlaQuery("SELECT pi.plan_instance_no from ariacore.acct act join ariacore.plan_instance pi on pi.acct_no=act.acct_no and pi.client_no=act.client_no join ariacore.client_plan cp on cp.client_no=act.client_no and cp.plan_no=pi.plan_no join ariacore.plan_instance_master pim on pim.client_no=cp.client_no and pim.PLAN_INSTANCE_NO=pi.PLAN_INSTANCE_NO join ariacore.acct_billing_group abg on abg.client_no=pi.client_no and abg.billing_group_no=pim.billing_group_no and abg.acct_no=pi.acct_no join ariacore.PLAN_INST_PRODUCT_FIELDS_VAL pipf on pipf.client_no=act.client_no and pipf.plan_instance_no=pi.plan_instance_no join ariacore.client_obj_supp_fields cosf on cosf.client_no=pipf.client_no and pipf.field_no=cosf.field_no where act.client_no = "+library.Constants.Constant.client_no+" and "+query_string)}
		else{rs = db.executePlaQuery("SELECT pi.plan_instance_no from ariacore.acct act join ariacore.plan_instance pi on pi.acct_no=act.acct_no and pi.client_no=act.client_no join ariacore.client_plan cp on cp.client_no=act.client_no and cp.plan_no=pi.plan_no join ariacore.plan_instance_master pim on pim.client_no=cp.client_no and pim.PLAN_INSTANCE_NO=pi.PLAN_INSTANCE_NO join ariacore.acct_billing_group abg on abg.client_no=pi.client_no and abg.billing_group_no=pim.billing_group_no and abg.acct_no=pi.acct_no where act.client_no = "+library.Constants.Constant.client_no+" and "+query_string)}
		
		ResultSetMetaData meta = rs.getMetaData()
		int columns = meta.getColumnCount();
		while (rs.next())
		{
			for(int u=1; u<=columns; ++u)
			{
				if(rs.getObject(u)!=null)
					MPIS.add(rs.getObject(u));
			}
		}
		int a=MPIS.size();
		a=a-offset;
		if(a>limit)a=limit;
		return a.toString();
	}
	
	/**
	 * for objQuery md_get_plan_instance_information_total_m
	 * @param no param required
	 * @return total records to be in the response
	 */
	public String md_get_plan_instance_information_total_records_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		String query_string=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/query_string[1]")
		
		query_string = query_string.replaceAll("acct_no","act.acct_no");
		query_string = query_string.replaceAll("user_id","act.userid");
		query_string = query_string.replaceAll("client_acct_id","act.client_acct_id");
		query_string = query_string.replaceAll("plan_instance_no","pi.plan_instance_no");
		query_string = query_string.replaceAll("client_plan_instance_id","pi.plan_instance_cdid");
		query_string = query_string.replaceAll("product_field_name","cosf.FIELD_NAME");
		query_string = query_string.replaceAll("product_field_value","pipf.VALUE_TEXT");
		query_string = query_string.replaceAll("plan_no","pi.plan_no");
		query_string = query_string.replaceAll("client_plan_id","cp.CLIENT_PLAN_ID");
		query_string = query_string.replaceAll("last_bill_date","pi.last_bill_date");
		query_string = query_string.replaceAll("next_bill_date","pi.next_bill_date");
		query_string = query_string.replaceAll("bill_thru_date","pi.LAST_BILL_THRU_DATE");
		query_string = query_string.replaceAll("status_cd","pi.status_cd");
		query_string = query_string.replaceAll("master_plan_instance_balance","pim.STACK_BALANCE");
		query_string = query_string.replaceAll("\"","");
		query_string = query_string.replaceAll("= ","= '");
		query_string = query_string.replaceAll("> ","> '");
		query_string = query_string.replaceAll("< ","< '");
		query_string = query_string.replaceAll(" and","' and");
		query_string += "'";
		ArrayList MPIS = new ArrayList();
		ResultSet rs
		if(query_string.contains("cosf.FIELD")||query_string.contains("pipf.VALUE"))
		{rs = db.executePlaQuery("SELECT pi.plan_instance_no from ariacore.acct act join ariacore.plan_instance pi on pi.acct_no=act.acct_no and pi.client_no=act.client_no join ariacore.client_plan cp on cp.client_no=act.client_no and cp.plan_no=pi.plan_no join ariacore.plan_instance_master pim on pim.client_no=cp.client_no and pim.PLAN_INSTANCE_NO=pi.PLAN_INSTANCE_NO join ariacore.acct_billing_group abg on abg.client_no=pi.client_no and abg.billing_group_no=pim.billing_group_no and abg.acct_no=pi.acct_no join ariacore.PLAN_INST_PRODUCT_FIELDS_VAL pipf on pipf.client_no=act.client_no and pipf.plan_instance_no=pi.plan_instance_no join ariacore.client_obj_supp_fields cosf on cosf.client_no=pipf.client_no and pipf.field_no=cosf.field_no where act.client_no = "+library.Constants.Constant.client_no+" and "+query_string)}
		else{rs = db.executePlaQuery("SELECT pi.plan_instance_no from ariacore.acct act join ariacore.plan_instance pi on pi.acct_no=act.acct_no and pi.client_no=act.client_no join ariacore.client_plan cp on cp.client_no=act.client_no and cp.plan_no=pi.plan_no join ariacore.plan_instance_master pim on pim.client_no=cp.client_no and pim.PLAN_INSTANCE_NO=pi.PLAN_INSTANCE_NO join ariacore.acct_billing_group abg on abg.client_no=pi.client_no and abg.billing_group_no=pim.billing_group_no and abg.acct_no=pi.acct_no where act.client_no = "+library.Constants.Constant.client_no+" and "+query_string)}
		
		ResultSetMetaData meta = rs.getMetaData()
		int columns = meta.getColumnCount();
		while (rs.next())
		{
			for(int u=1; u<=columns; ++u)
			{
				if(rs.getObject(u)!=null)
					MPIS.add(rs.getObject(u));
			}
		}
		int a=MPIS.size();
		
		return a.toString();
	}
	
	/**
	 * it is uses to get the same time stamp to reuse it from the above module md_unique_id_m
	 * @param input
	 * @return the UID
	 * @throws Exception
	 */
	String md_get_UniqueID_m(String input) throws Exception{
		logi.logInfo "into md_get_UniqueID_m"
		return input+"_"+library.Constants.Constant.UniqueID.get(input).toString()
	}
	
	/**
	 * this method specific to verify the date fields in the ObjQueryAPI
	 * @param input - pass the input field to md_getvalfromnadeandindex_m
	 * @return the date only without the timestamp from API
	 */
	String md_ObjDate_m(String input)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
		String dateInString = md_getValFromNodeAndIndex_m(input);
		
		try{
			
		   Date date = formatter.parse(dateInString);
		   dateInString=formatter2.format(date);
			
		   }
		catch(Exception e)
		   {e.printStackTrace();}
		   return dateInString;
	}
	
	public  LinkedHashMap md_getAllnodesAsList_m(String parentnode,int indexs) throws Exception
	{
		  logi.logInfo "into md_getAllnodesAsList_m"
		  String xmlRecords = library.Constants.Constant.RESPONSE
		  pnode=parentnode.split("\\,")[0];
		  Sindex=indexs;
		  Sindex-=1;
		  logi.logInfo "param given "+parentnode
		  String [] rname=parentnode.split(",");
		  parentnode=parentnode.split(",")[0];
		  LinkedList<String> childnodes = new LinkedList<String>();
		  LinkedList<String> childnodes2 = new LinkedList<String>();
		  LinkedHashMap<String, String> inner = new LinkedHashMap<String, String>();
		  //for getting the values of child nodes

		  childnodes=getnode(xmlRecords,pnode,Sindex);
		  logi.logInfo "param given "+parentnode
		  int sizze=childnodes.size();
		  int innerindex=0;
		  for (int i=0;i<sizze;i++)
		  {
		 
				innerindex=Collections.frequency(childnodes2, childnodes.get(i));
	
				
				if(getnode(xmlRecords,childnodes.get(i),0).size()<1)
		  {
					  
							if(Collections.frequency(childnodes2, childnodes.get(i))==0)
							childnodes2.add(childnodes.get(i));
			
					  
		  }
				
		  else {
				childnodes2.add(childnodes.get(i));
				childnodes.addAll(getnode(xmlRecords,childnodes.get(i),innerindex));
				
				   }
		  
		  sizze=childnodes.size();
		  }
	
		
		  for(String a : childnodes2)
		  {
			  if(!Arrays.asList(rname).contains(a.replace("ns1:","")))
				if(getnode(xmlRecords,a,0).size()<1)
				inner.putAll(md_getSpecificnodeAsList_m(a));
		  
		  }
		  System.out.println(inner.size());
		  return inner.sort();

	}
	
	
	def md_get_All_node_count_m(String input)
	{
	   logi.logInfo("into md_get_node_count_m")
	  String xmlRecords = library.Constants.Constant.RESPONSE
	  DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	  InputSource is = new InputSource();
	  is.setCharacterStream(new StringReader(xmlRecords));
	  Document doc = db.parse(is);
	  NodeList nodes = doc.getElementsByTagName(pnode);
	  Element element = (Element) nodes.item(Sindex);
	  NodeList name = element.getElementsByTagName(input);
	  int index=name.getLength();
	  return index.toString()
	}
	

	/**
	 * for objQuery get_all_invoice_information_m
	 * @param no param required
	 * @return hashmap
	 */
	public def md_get_all_invoice_information_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		String query_string=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/query_string[1]")
		int offset = 0;
		int limit = 100;
		String offset_s =getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/offset[1]")
		String limit_s = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/limit[1]")
		if(!offset_s.contains("NoVal"))offset=Integer.parseInt(offset_s);
		if(!limit_s.contains("NoVal"))limit=Integer.parseInt(limit_s);
		query_string = query_string.replaceAll("invoice_no","ai.invoice_no");
		query_string = query_string.replaceAll("acct_no","acct.acct_no");
		query_string = query_string.replaceAll("user_id","acct.userid");
		query_string = query_string.replaceAll("client_acct_id","acct.CLIENT_ACCT_ID");
		query_string = query_string.replaceAll("invoice_type","ai.REAL_PEND_IND");
		query_string = query_string.replaceAll("bill_company_name","ai.BILL_COMPANY_NAME");
		query_string = query_string.replaceAll("pay_method_type","pm.METHOD_ID");
		query_string = query_string.replaceAll("pay_method_name","ai.PAY_METHOD_NAME");
		query_string = query_string.replaceAll("custom_status_label","ai.CUSTOM_STATUS_LABEL");
		query_string = query_string.replaceAll("client_notes","ai.CLIENT_NOTES");
		query_string = query_string.replaceAll("client_master_plan_instance_id","pi.PLAN_INSTANCE_CDID");
		query_string = query_string.replaceAll("master_plan_instance_id","aid.master_plan_instance_no");
		query_string = query_string.replaceAll("invoice_transaction_id","atn.EVENT_NO");
		query_string = query_string.replaceAll("po_num","aid.PURCHASE_ORDER_NO");
		query_string = query_string.replaceAll("\"","");
		query_string = query_string.replaceAll("= ","= '");
		query_string = query_string.replaceAll("> ","> '");
		query_string = query_string.replaceAll("< ","< '");
		query_string = query_string.replaceAll(" and","' and");
		query_string = query_string.replaceAll(" or","' or");
		query_string += "'";
		query_string = query_string.replaceAll("''","''")
		ArrayList INVOICES = new ArrayList();
		String trans = "",trans2="";
		if(query_string.contains("atn.")){trans = " left join ariacore.acct_transaction atn on atn.client_no=aid.client_no and atn.invoice_no=aid.invoice_no and aid.INVOICE_DETAIL_NO=atn.SOURCE_NO join ariacore.transaction_types tt on tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE ";}
		ResultSet rs = db.executePlaQuery("SELECT DISTINCT ai.invoice_no from ariacore.all_invoices ai join ariacore.acct acct on acct.client_no=ai.client_no and ai.acct_no=acct.acct_no join ariacore.acct_address aa on aa.client_no=acct.client_no and aa.acct_no=acct.acct_no and aa.ADDRESS_TYPE_CD = 0 and aa.SOURCE_NO = 0 join ariacore.PAYMENT_METHODS pm on pm.METHOD_NAME=ai.PAY_METHOD_NAME join ariacore.acct_billing_group abg  on abg.client_no=acct.client_no and abg.acct_no=acct.acct_no and abg.BILLING_GROUP_NO = ai.billing_group_no join ariacore.all_invoice_details aid on aid.client_no=acct.client_no and aid.invoice_no=ai.invoice_no  join ariacore.plan_instance pi on pi.client_no=ai.client_no and pi.plan_instance_no=aid.master_plan_instance_no "+trans+" where acct.client_no = "+library.Constants.Constant.client_no+" and "+query_string)
		
		ResultSetMetaData meta = rs.getMetaData()
		int columns = meta.getColumnCount();
		while (rs.next())
		{
			for(int u=1; u<=columns; ++u)
			{
				if(rs.getObject(u)!=null)
					INVOICES.add(rs.getObject(u));
			}
		}
		Collections.sort(INVOICES);
		
		logi.logInfo  "INVOICES in the list "+INVOICES
		int loops=1,loopss=1;
		for(String INVOICE : INVOICES)
		{
			if(loopss>offset)
		{
		String stat = "",stat2 = "";
		if(!query_string.contains("IND = 'P'")){	stat = " left join ariacore.acct_statement actst on actst.client_no=acct.client_no and actst.acct_no=acct.acct_no  and actst.INVOICE_NO = ai.INVOICE_NO "; stat2 = " actst.BALANCE_FORWARD as statement_balance_forward,actst.STATEMENT_NO as aria_statement_no,actst.ACCT_STATEMENT_SEQ_STR,actst.SECOND_ACCT_STATEMENT_SEQ_STR, "}
		String Query="SELECT "+ stat2+"ai.invoice_no,acct.acct_no,acct.userid as user_id,acct.CLIENT_ACCT_ID,ai.REAL_PEND_IND as invoice_type,TO_CHAR(ai.from_date, 'YYYY-MM-DD HH24:MI:SSxFF') as from_date,TO_CHAR(ai.to_date, 'YYYY-MM-DD HH24:MI:SSxFF') as to_date,TO_CHAR(ai.USAGE_FROM_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  usage_bill_from_date,TO_CHAR(ai.USAGE_TO_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  usage_bill_thru_date,aa.email as taxed_email,aa.FIRST_NAME as taxed_first_name , aa.MIDDLE_INITIAL as taxed_middle_initial , aa.LAST_NAME as taxed_last_name,aa.ADDRESS1 as taxed_address1, aa.ADDRESS2 as taxed_address2,aa.ADDRESS3 as taxed_address3,aa.CITY as taxed_city ,aa.STATE as taxed_state,aa.TAX_LOCALITY as taxed_locality,aa.ZIP as taxed_zip,aa.COUNTRY as taxed_country,ai.DEBIT as amount,TO_CHAR(ai.BILL_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  BILL_DATE,TO_CHAR(ai.DUE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  DUE_DATE,TO_CHAR(ai.PAID_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  PAID_DATE,TO_CHAR(ai.NOTIFY_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  NOTIFY_DATE,ai.CURRENCY as currency_cd,ai.BAL_FWD as balance_forward,ai.TOTAL_DUE ,ai.COMMENTS,ai.LONG_COMMENTS as additional_comments,TO_CHAR(ai.UPDATE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  last_updated,CASE when(select count(*) from ariacore.ALL_INVOICE_DETAILS where client_no = acct.client_no  and invoice_no=ai.invoice_no) > 0 then 1 else 0 end as invoice_line_items,ai.VOIDING_EVENT_NO,ai.BILL_COMPANY_NAME,pm.METHOD_ID as pay_method_type,ai.PAY_METHOD_NAME,ai.CUSTOM_STATUS_LABEL,ai.CLIENT_NOTES,ai.BILLING_GROUP_NO,abg.PROFILE_CDID as client_billing_group_id from ariacore.all_invoices ai join ariacore.acct acct on acct.client_no=ai.client_no and ai.acct_no=acct.acct_no join ariacore.acct_address aa on aa.client_no=acct.client_no and aa.acct_no=acct.acct_no and aa.ADDRESS_TYPE_CD = 0 and aa.SOURCE_NO = 0 "+stat+" join ariacore.PAYMENT_METHODS pm on pm.METHOD_NAME=ai.PAY_METHOD_NAME join ariacore.acct_billing_group abg  on abg.client_no=acct.client_no and abg.acct_no=acct.acct_no and abg.BILLING_GROUP_NO = ai.billing_group_no where acct.client_no = "+library.Constants.Constant.client_no+" and ai.invoice_no = "+INVOICE;
		ResultSet resultSet = db.executePlaQuery(Query)
		ResultSetMetaData md = resultSet.getMetaData()
		columns = md.getColumnCount();
		
		while (resultSet.next())
		{
			for(int u=1; u<=columns; ++u)
			{
				if(resultSet.getObject(u)!=null)
					line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
			}
		}
		/*if(query_string.contains("aid.PURCHASE_ORDER_NO")||query_string.contains("atn.EVENT_NO")){INVOICE+=" and "+query_string}
		if(!query_string.contains("IND = 'P'")){trans = " left join ariacore.acct_transaction atn on atn.client_no=aid.client_no and atn.invoice_no=aid.invoice_no and aid.INVOICE_DETAIL_NO=atn.SOURCE_NO join ariacore.transaction_types tt on tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE ";trans2 = "atn.EVENT_NO as invoice_transaction_id,tt.DESCRIPTION as TRANSACTION_TYPE,"}
		Query="SELECT "+trans2+"ars.client_rate_schedule_id,aid.PLAN_INSTANCE_NO,pi.PLAN_INSTANCE_CDID as client_plan_instance_id,aid.SEQ_NUM as line_item_no,aid.DEBIT as amount,aid.COMMENTS,aid.PLAN_NAME,aid.PLAN_NO,cs.service_name,aid.service_no,coa.coa_id,coa.CLIENT_COA_CODE as ledger_code,COA.COA_DESCRIPTION,aid.USAGE_UNITS,aid.USAGE_RATE,aid.USAGE_TYPE as usage_type_no,TO_CHAR(aid.START_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as START_DATE,TO_CHAR(aid.END_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as END_DATE,aid.ORIG_CLIENT_SKU  AS client_sku,aid.ORDER_NO,aid.ITEM_NO,aid.BASE_PLAN_UNITS,aid.PRORATION_FACTOR,aid.ACTUAL_ADV_PERIOD_TOTAL_DAYS as adv_billing_period_total_days,aid.PRORATION_REMAINING_DAYS,aid.master_plan_instance_no as master_plan_instance_id,pi.PLAN_INSTANCE_CDID as client_master_plan_instance_id,aid.PURCHASE_ORDER_NO as po_num,aid.RATE_SCHEDULE_NO from ariacore.all_invoice_details aid join ariacore.client_service cs on cs.client_no=aid.client_no and cs.SERVICE_NO=aid.SERVICE_NO   JOIN ARIACORE.CHART_OF_ACCTS COA ON coa.CUSTOM_TO_CLIENT_NO=aid.client_no  and  COA.COA_ID=CS.COA_ID "+trans+" join ariacore.plan_instance pi on pi.client_no=aid.client_no and pi.PLAN_INSTANCE_NO=aid.master_plan_instance_no join ariacore.ALL_RATE_SCHEDULES  ars on ars.client_no=pi.client_no and ars.plan_no=pi.plan_no  where aid.client_no = "+library.Constants.Constant.client_no+" and aid.invoice_no = "+INVOICE+" order by 1 ";
		resultSet = db.executePlaQuery(Query)
		md = resultSet.getMetaData()
		columns = md.getColumnCount();
		
		while (resultSet.next())
		{
			for(int u=1; u<=columns; ++u)
			{
				if(resultSet.getObject(u)!=null)
				{
					 boolean is_having=true
						  int k=1;
						  while (is_having)
						  {
							  if(k==1)
							  {if(!line_item.keySet().asList().contains(md.getColumnName(u).toLowerCase())){is_having=false}}
							  else {if(!line_item.keySet().asList().contains(md.getColumnName(u).toLowerCase()+"["+k.toString()+"]")){is_having=false}}
							  k+=1;
						  }
						  k-=1;
						  logi.logInfo "the value of k "+k;
						  logi.logInfo "putting in output "+resultSet.getObject(u);
						   if(k>1){
							   line_item.put((md.getColumnName(u)).toLowerCase()+"["+k.toString()+"]",(resultSet.getObject(u)));
							   }
						   else{
							   line_item.put((md.getColumnName(u)).toLowerCase(),(resultSet.getObject(u)));
							   
							   }
				}
			}
		}*/
		
					  
		  line_item.sort();
		  row_item.put("all_invoice_details_m_row["+(loops-1)+"]",line_item.sort())
		  line_item = new LinkedHashMap();
		  if(loops==limit)break;
		  loops+=1;
		}
		loopss+=1;
		}
		row_item=row_item.sort();
		return row_item
		
	}
	
	
	
	/**
	 * for objQuery get_all_invoice_information_m
	 * @param no param required
	 * @return hashmap
	 */
	public def md_get_all_invoice_information_inner_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap line = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		String query_string=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/query_string[1]")
		int offset = 0;
		int limit = 100;
		String offset_s =getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/offset[1]")
		String limit_s = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/limit[1]")
		if(!offset_s.contains("NoVal"))offset=Integer.parseInt(offset_s);
		if(!limit_s.contains("NoVal"))limit=Integer.parseInt(limit_s);
		query_string = query_string.replaceAll("invoice_no","ai.invoice_no");
		query_string = query_string.replaceAll("acct_no","acct.acct_no");
		query_string = query_string.replaceAll("user_id","acct.userid");
		query_string = query_string.replaceAll("client_acct_id","acct.CLIENT_ACCT_ID");
		query_string = query_string.replaceAll("invoice_type","ai.REAL_PEND_IND");
		query_string = query_string.replaceAll("bill_company_name","ai.BILL_COMPANY_NAME");
		query_string = query_string.replaceAll("pay_method_type","pm.METHOD_ID");
		query_string = query_string.replaceAll("pay_method_name","ai.PAY_METHOD_NAME");
		query_string = query_string.replaceAll("custom_status_label","ai.CUSTOM_STATUS_LABEL");
		query_string = query_string.replaceAll("client_notes","ai.CLIENT_NOTES");
		query_string = query_string.replaceAll("client_master_plan_instance_id","pi.PLAN_INSTANCE_CDID");
		query_string = query_string.replaceAll("master_plan_instance_id","aid.master_plan_instance_no");
		query_string = query_string.replaceAll("invoice_transaction_id","atn.EVENT_NO");
		query_string = query_string.replaceAll("po_num","aid.PURCHASE_ORDER_NO");
		query_string = query_string.replaceAll("\"","");
		query_string = query_string.replaceAll("= ","= '");
		query_string = query_string.replaceAll("> ","> '");
		query_string = query_string.replaceAll("< ","< '");
		query_string = query_string.replaceAll(" and","' and");
		query_string = query_string.replaceAll(" or","' or");
		query_string += "'";
		query_string = query_string.replaceAll("''","''")
		ArrayList INVOICES = new ArrayList();
		String trans = "",trans2="";
		if(query_string.contains("atn.")){trans = " left join ariacore.acct_transaction atn on atn.client_no=aid.client_no and atn.invoice_no=aid.invoice_no and aid.INVOICE_DETAIL_NO=atn.SOURCE_NO join ariacore.transaction_types tt on tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE ";}
		ResultSet rs = db.executePlaQuery("SELECT DISTINCT ai.invoice_no from ariacore.all_invoices ai join ariacore.acct acct on acct.client_no=ai.client_no and ai.acct_no=acct.acct_no join ariacore.acct_address aa on aa.client_no=acct.client_no and aa.acct_no=acct.acct_no and aa.ADDRESS_TYPE_CD = 0 and aa.SOURCE_NO = 0 join ariacore.PAYMENT_METHODS pm on pm.METHOD_NAME=ai.PAY_METHOD_NAME join ariacore.acct_billing_group abg  on abg.client_no=acct.client_no and abg.acct_no=acct.acct_no and abg.BILLING_GROUP_NO = ai.billing_group_no join ariacore.all_invoice_details aid on aid.client_no=acct.client_no and aid.invoice_no=ai.invoice_no  join ariacore.plan_instance pi on pi.client_no=ai.client_no and pi.plan_instance_no=aid.master_plan_instance_no "+trans+" where acct.client_no = "+library.Constants.Constant.client_no+" and "+query_string)
		
		ResultSetMetaData meta = rs.getMetaData()
		int columns = meta.getColumnCount();
		while (rs.next())
		{
			for(int u=1; u<=columns; ++u)
			{
				if(rs.getObject(u)!=null)
					INVOICES.add(rs.getObject(u));
			}
		}
		Collections.sort(INVOICES);
		
		
		logi.logInfo  "INVOICES in the list "+INVOICES
		int loops=1,loopss=1;
		for(String INVOICE : INVOICES)
		{
			if(loopss>offset)
		{
		String stat = "",stat2 = "";
		if(!query_string.contains("IND = 'P'")){	stat = " left join ariacore.acct_statement actst on actst.client_no=acct.client_no and actst.acct_no=acct.acct_no  and actst.INVOICE_NO = ai.INVOICE_NO "; stat2 = " actst.BALANCE_FORWARD as statement_balance_forward,actst.STATEMENT_NO as aria_statement_no,actst.ACCT_STATEMENT_SEQ_STR,actst.SECOND_ACCT_STATEMENT_SEQ_STR, "}
		String Query="SELECT "+ stat2+"ai.invoice_no,acct.acct_no,acct.userid as user_id,acct.CLIENT_ACCT_ID,ai.REAL_PEND_IND as invoice_type,TO_CHAR(ai.from_date, 'YYYY-MM-DD HH24:MI:SSxFF') as from_date,TO_CHAR(ai.to_date, 'YYYY-MM-DD HH24:MI:SSxFF') as to_date,TO_CHAR(ai.USAGE_FROM_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  usage_bill_from_date,TO_CHAR(ai.USAGE_TO_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  usage_bill_thru_date,aa.email as taxed_email,aa.FIRST_NAME as taxed_first_name , aa.MIDDLE_INITIAL as taxed_middle_initial , aa.LAST_NAME as taxed_last_name,aa.ADDRESS1 as taxed_address1, aa.ADDRESS2 as taxed_address2,aa.ADDRESS3 as taxed_address3,aa.CITY as taxed_city ,aa.STATE as taxed_state,aa.TAX_LOCALITY as taxed_locality,aa.ZIP as taxed_zip,aa.COUNTRY as taxed_country,ai.DEBIT as amount,TO_CHAR(ai.BILL_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  BILL_DATE,TO_CHAR(ai.DUE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  DUE_DATE,TO_CHAR(ai.PAID_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  PAID_DATE,TO_CHAR(ai.NOTIFY_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  NOTIFY_DATE,ai.CURRENCY as currency_cd,ai.BAL_FWD as balance_forward,ai.TOTAL_DUE ,ai.COMMENTS,ai.LONG_COMMENTS as additional_comments,TO_CHAR(ai.UPDATE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as  last_updated,CASE when(select count(*) from ariacore.ALL_INVOICE_DETAILS where client_no = acct.client_no  and invoice_no=ai.invoice_no) > 0 then 1 else 0 end as invoice_line_items,ai.VOIDING_EVENT_NO,ai.BILL_COMPANY_NAME,pm.METHOD_ID as pay_method_type,ai.PAY_METHOD_NAME,ai.CUSTOM_STATUS_LABEL,ai.CLIENT_NOTES,ai.BILLING_GROUP_NO,abg.PROFILE_CDID as client_billing_group_id from ariacore.all_invoices ai join ariacore.acct acct on acct.client_no=ai.client_no and ai.acct_no=acct.acct_no join ariacore.acct_address aa on aa.client_no=acct.client_no and aa.acct_no=acct.acct_no and aa.ADDRESS_TYPE_CD = 0 and aa.SOURCE_NO = 0 "+stat+" join ariacore.PAYMENT_METHODS pm on pm.METHOD_NAME=ai.PAY_METHOD_NAME join ariacore.acct_billing_group abg  on abg.client_no=acct.client_no and abg.acct_no=acct.acct_no and abg.BILLING_GROUP_NO = ai.billing_group_no where acct.client_no = "+library.Constants.Constant.client_no+" and ai.invoice_no = "+INVOICE;
		ResultSet resultSet = db.executePlaQuery(Query)
		ResultSetMetaData md = resultSet.getMetaData()
		columns = md.getColumnCount();
		
		/*while (resultSet.next())
		{
			for(int u=1; u<=columns; ++u)
			{
				if(resultSet.getObject(u)!=null)
					line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
			}
		}*/
		if(query_string.contains("aid.PURCHASE_ORDER_NO")||query_string.contains("atn.EVENT_NO")){INVOICE+=" and "+query_string}
		if(!query_string.contains("IND = 'P'")){trans = " left join ariacore.acct_transaction atn on atn.client_no=aid.client_no and atn.invoice_no=aid.invoice_no and aid.INVOICE_DETAIL_NO=atn.SOURCE_NO join ariacore.transaction_types tt on tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE ";trans2 = "atn.EVENT_NO as invoice_transaction_id,tt.DESCRIPTION as TRANSACTION_TYPE,"}
		Query="SELECT "+trans2+"ars.client_rate_schedule_id,aid.PLAN_INSTANCE_NO,pi.PLAN_INSTANCE_CDID as client_plan_instance_id,aid.SEQ_NUM as line_item_no,aid.DEBIT as amount,aid.COMMENTS,aid.PLAN_NAME,aid.PLAN_NO,cs.service_name,aid.service_no,coa.coa_id,coa.CLIENT_COA_CODE as ledger_code,COA.COA_DESCRIPTION,aid.USAGE_UNITS,aid.USAGE_RATE,aid.USAGE_TYPE as usage_type_no,TO_CHAR(aid.START_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as START_DATE,TO_CHAR(aid.END_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as END_DATE,aid.ORIG_CLIENT_SKU  AS client_sku,aid.ORDER_NO,aid.ITEM_NO,aid.BASE_PLAN_UNITS,aid.PRORATION_FACTOR,aid.ACTUAL_ADV_PERIOD_TOTAL_DAYS as adv_billing_period_total_days,aid.PRORATION_REMAINING_DAYS,aid.master_plan_instance_no as master_plan_instance_id,pi2.PLAN_INSTANCE_CDID as client_master_plan_instance_id,aid.PURCHASE_ORDER_NO as po_num,aid.RATE_SCHEDULE_NO from ariacore.all_invoice_details aid join ariacore.ALL_CLIENT_SERVICE cs on cs.client_no=NVL(aid.client_no,cs.client_no) and cs.SERVICE_NO=aid.SERVICE_NO   JOIN ARIACORE.CHART_OF_ACCTS COA ON cs.client_no=NVL(coa.CUSTOM_TO_CLIENT_NO,cs.client_no)  and  COA.COA_ID=CS.COA_ID "+trans+" left join ariacore.plan_instance pi on pi.client_no=aid.client_no and pi.PLAN_INSTANCE_NO=aid.plan_instance_no left join ariacore.ALL_RATE_SCHEDULES  ars on ars.client_no=pi.client_no and ars.schedule_no=aid.RATE_SCHEDULE_NO left join ariacore.plan_instance pi2 on pi2.client_no=aid.client_no and pi2.PLAN_INSTANCE_NO=aid.master_plan_instance_no where aid.client_no = "+library.Constants.Constant.client_no+" and aid.invoice_no = "+INVOICE+" order by aid.SEQ_NUM ";
		resultSet = db.executePlaQuery(Query)
		md = resultSet.getMetaData()
		columns = md.getColumnCount();
		int kz = 1;
		while (resultSet.next())
		{
			for(int u=1; u<=columns; ++u)
			{
				if(resultSet.getObject(u)!=null)
				{
					 boolean is_having=true
						  int k=1;
						  while (is_having)
						  {
							  if(k==1)
							  {if(!line_item.keySet().asList().contains(md.getColumnName(u).toLowerCase())){is_having=false}}
							  else {if(!line_item.keySet().asList().contains(md.getColumnName(u).toLowerCase()+"["+k.toString()+"]")){is_having=false}}
							  k+=1;
						  }
						  k-=1;
						  logi.logInfo "the value of k "+k;
						  logi.logInfo "putting in output "+resultSet.getObject(u);
						   if(k>1){
							   line_item.put((md.getColumnName(u)).toLowerCase()+"["+k.toString()+"]",(resultSet.getObject(u)));
							   }
						   else{
							   line_item.put((md.getColumnName(u)).toLowerCase(),(resultSet.getObject(u)));
							   
							   }
				}
			}
			line.sort();
			if(kz>1)
			line.put("all_invoice_line_m_row["+kz+"]", line_item.sort())
			else line.put("all_invoice_line_m_row", line_item.sort())
			line_item = new LinkedHashMap();
			kz++;
		}
		
					  
		  line.sort();
		  if(loops==1)row_item.put("all_invoice_line_m",line.sort())
		  else
		  row_item.put("all_invoice_line_m["+(loops)+"]",line.sort())
		  line = new LinkedHashMap();
		  if(loops==limit)break;
		  loops+=1;
		}
		loopss+=1;
		}
		row_item=row_item.sort();
		return row_item
		
	}
	
	/**
	 * for objQuery get_all_invoice_information_m
	 * @param no param required
	 * @return total returned records
	 */
	public def md_get_all_invoice_information_total_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		String query_string=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/query_string[1]")
		int offset = 0;
		int limit = 100;
		String offset_s =getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/offset[1]")
		String limit_s = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/limit[1]")
		if(!offset_s.contains("NoVal"))offset=Integer.parseInt(offset_s);
		if(!limit_s.contains("NoVal"))limit=Integer.parseInt(limit_s);
		query_string = query_string.replaceAll("invoice_no","ai.invoice_no");
		query_string = query_string.replaceAll("acct_no","acct.acct_no");
		query_string = query_string.replaceAll("user_id","acct.userid");
		query_string = query_string.replaceAll("client_acct_id","acct.CLIENT_ACCT_ID");
		query_string = query_string.replaceAll("invoice_type","ai.REAL_PEND_IND");
		query_string = query_string.replaceAll("bill_company_name","ai.BILL_COMPANY_NAME");
		query_string = query_string.replaceAll("pay_method_type","pm.METHOD_ID");
		query_string = query_string.replaceAll("pay_method_name","ai.PAY_METHOD_NAME");
		query_string = query_string.replaceAll("custom_status_label","ai.CUSTOM_STATUS_LABEL");
		query_string = query_string.replaceAll("client_notes","ai.CLIENT_NOTES");
		query_string = query_string.replaceAll("client_master_plan_instance_id","pi.PLAN_INSTANCE_CDID");
		query_string = query_string.replaceAll("master_plan_instance_id","aid.master_plan_instance_no");
		query_string = query_string.replaceAll("invoice_transaction_id","atn.EVENT_NO");
		query_string = query_string.replaceAll("po_num","aid.PURCHASE_ORDER_NO");
		query_string = query_string.replaceAll("\"","");
		query_string = query_string.replaceAll("= ","= '");
		query_string = query_string.replaceAll("> ","> '");
		query_string = query_string.replaceAll("< ","< '");
		query_string = query_string.replaceAll(" and","' and");
		query_string = query_string.replaceAll(" or","' or");
		query_string += "'";
		String trans = "",trans2="";
		if(query_string.contains("atn.")){trans = " left join ariacore.acct_transaction atn on atn.client_no=aid.client_no and atn.invoice_no=aid.invoice_no and aid.INVOICE_DETAIL_NO=atn.SOURCE_NO join ariacore.transaction_types tt on tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE ";}
		int a = Integer.parseInt(db.executeQueryP2("SELECT count(DISTINCT ai.invoice_no) from ariacore.all_invoices ai join ariacore.acct acct on acct.client_no=ai.client_no and ai.acct_no=acct.acct_no join ariacore.acct_address aa on aa.client_no=acct.client_no and aa.acct_no=acct.acct_no and aa.ADDRESS_TYPE_CD = 0 and aa.SOURCE_NO = 0 join ariacore.PAYMENT_METHODS pm on pm.METHOD_NAME=ai.PAY_METHOD_NAME join ariacore.acct_billing_group abg  on abg.client_no=acct.client_no and abg.acct_no=acct.acct_no and abg.BILLING_GROUP_NO = ai.billing_group_no join ariacore.all_invoice_details aid on aid.client_no=acct.client_no and aid.invoice_no=ai.invoice_no  join ariacore.plan_instance pi on pi.client_no=ai.client_no and pi.plan_instance_no=aid.master_plan_instance_no "+trans+" where acct.client_no = "+library.Constants.Constant.client_no+" and "+query_string).toString())
		a=a-offset;
		if(a>limit)a=limit;
		return a.toString();
		
	}
	
	/**
	 * for objQuery get_all_invoice_information_m
	 * @param no param required
	 * @return total records node value
	 */
	public def md_get_all_invoice_information_total_records_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		String query_string=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/query_string[1]")
		
		query_string = query_string.replaceAll("invoice_no","ai.invoice_no");
		query_string = query_string.replaceAll("acct_no","acct.acct_no");
		query_string = query_string.replaceAll("user_id","acct.userid");
		query_string = query_string.replaceAll("client_acct_id","acct.CLIENT_ACCT_ID");
		query_string = query_string.replaceAll("invoice_type","ai.REAL_PEND_IND");
		query_string = query_string.replaceAll("bill_company_name","ai.BILL_COMPANY_NAME");
		query_string = query_string.replaceAll("pay_method_type","pm.METHOD_ID");
		query_string = query_string.replaceAll("pay_method_name","ai.PAY_METHOD_NAME");
		query_string = query_string.replaceAll("custom_status_label","ai.CUSTOM_STATUS_LABEL");
		query_string = query_string.replaceAll("client_notes","ai.CLIENT_NOTES");
		query_string = query_string.replaceAll("client_master_plan_instance_id","pi.PLAN_INSTANCE_CDID");
		query_string = query_string.replaceAll("master_plan_instance_id","aid.master_plan_instance_no");
		query_string = query_string.replaceAll("invoice_transaction_id","atn.EVENT_NO");
		query_string = query_string.replaceAll("po_num","aid.PURCHASE_ORDER_NO");
		query_string = query_string.replaceAll("\"","");
		query_string = query_string.replaceAll("= ","= '");
		query_string = query_string.replaceAll("> ","> '");
		query_string = query_string.replaceAll("< ","< '");
		query_string = query_string.replaceAll(" and","' and");
		query_string = query_string.replaceAll(" or","' or");
		query_string += "'";
		String trans = "",trans2="";
		if(query_string.contains("atn.")){trans = " left join ariacore.acct_transaction atn on atn.client_no=aid.client_no and atn.invoice_no=aid.invoice_no and aid.INVOICE_DETAIL_NO=atn.SOURCE_NO join ariacore.transaction_types tt on tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE ";}
		int a = Integer.parseInt(db.executeQueryP2("SELECT count(DISTINCT ai.invoice_no) from ariacore.all_invoices ai join ariacore.acct acct on acct.client_no=ai.client_no and ai.acct_no=acct.acct_no join ariacore.acct_address aa on aa.client_no=acct.client_no and aa.acct_no=acct.acct_no and aa.ADDRESS_TYPE_CD = 0 and aa.SOURCE_NO = 0 join ariacore.PAYMENT_METHODS pm on pm.METHOD_NAME=ai.PAY_METHOD_NAME join ariacore.acct_billing_group abg  on abg.client_no=acct.client_no and abg.acct_no=acct.acct_no and abg.BILLING_GROUP_NO = ai.billing_group_no join ariacore.all_invoice_details aid on aid.client_no=acct.client_no and aid.invoice_no=ai.invoice_no  join ariacore.plan_instance pi on pi.client_no=ai.client_no and pi.plan_instance_no=aid.master_plan_instance_no "+trans+" where acct.client_no = "+library.Constants.Constant.client_no+" and "+query_string).toString())
		return a.toString();
		
	}
	
	/**
	 * md_prorated_credit_m to calculate the credit produced
	 * @param s proration days
	 * @return credit amount
	 */
	
	
	
	LinkedHashMap md_Get_all_acct_contracts_gen_details_m(String acct_hierarchy1)
	{
		
		logi.logInfo "**** acct_hierarchy1"+acct_hierarchy1
		String acct_hierarchy=acct_hierarchy1.split("#")[0]
		logi.logInfo "**** acct_hierarchy+"+acct_hierarchy
		String type=acct_hierarchy1.split("#")[1]
		String seq=acct_hierarchy1.split("#")[2]
		String branch=acct_hierarchy1.split("#")[3]
		
		logi.logInfo "**** into type "+type+"...seq   "
		logi.logInfo "**** into get_all_acct_contracts_m **** "
		
		String filter_status_code=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//lit:get_all_acct_contracts_m[1]/filter_status_code[1]")
		logi.logInfo "---------------------------------- filter "+filter_status_code
		LinkedHashMap all_acct_contracts_hm= new LinkedHashMap()
		LinkedHashMap all_acct_contracts_temp_hm =new LinkedHashMap()
		LinkedList l1 = new LinkedList()
		InputMethods im = new InputMethods()
		ConnectDB db = new ConnectDB()
		String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		
		logi.logInfo("Given Acct no:" + acct_no)
		
if(branch.equalsIgnoreCase("I"))
		{
		
		if(type.equalsIgnoreCase("m"))
		{
			logi.logInfo("ITS MULTI PLAAAAN")
			
		String count_mpc_query="SELECT COUNT(CONTRACT_NO) FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE acct_no="+acct_no
		
		String count_mpc=db.executeQueryP2(count_mpc_query)
		
		

		if (count_mpc.toInteger() >= 1 && (filter_status_code.equalsIgnoreCase("0") || filter_status_code.equalsIgnoreCase("NoVal")) )
		{
			String database_to_execute="ARIACORE.ACCT_MULTI_PLAN_CONTRACTS"
			
			 for(int start=1;start<=count_mpc.toInteger();start++)
			{
				//RESULT SET
	
	
				logi.logInfo "inside for"
				String  contract_query="SELECT A.CONTRACT_NO,A.TYPE_NO,A.LENGTH_MONTHS,A.UPDATE_COMMENTS,TO_CHAR(A.CREATE_DATE,'YYYY-MM-DD') AS CREATE_DATE,TO_CHAR(A.UPDATE_DATE,'YYYY-MM-DD') AS UPDATE_DATE,TO_CHAR(A.START_DATE,'YYYY-MM-DD') AS START_DATE,TO_CHAR(A.END_DATE,'YYYY-MM-DD') AS END_DATE,A.STATUS_CD AS STATUS_CODE FROM (SELECT ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO,TYPE_NO,UPDATE_COMMENTS,CONTRACT_MONTHS AS LENGTH_MONTHS,CREATE_DATE,UPDATE_DATE,START_DATE,END_DATE,STATUS_CD FROM "+database_to_execute+" WHERE ACCT_NO="+acct_no+") A WHERE A.SEQ="+seq
				// WHERE A.SEQ="+start
				ResultSet rs_contract_query = db.executePlaQuery(contract_query)
	
				ResultSetMetaData md1 = rs_contract_query.getMetaData()
				int columns1 = md1.getColumnCount();
				while (rs_contract_query.next())
				{
				   
					for(int s=1; s<=columns1; ++s)
					{
	
						if(!(rs_contract_query.getObject(s).toString().equals("null")))
						{

							  l1.add(md1.getColumnName(s).toLowerCase())

							  logi.logInfo "KEYSSS 2 : "+l1
							  int freq = Collections.frequency(l1,md1.getColumnName(s).toLowerCase())

							  logi.logInfo("frew of :+"+md1.getColumnName(s).toLowerCase()+" is :"+freq)

							  if(freq>1)
							  {

									all_acct_contracts_temp_hm.put(md1.getColumnName(s).toLowerCase()+"["+ freq +"]",rs_contract_query.getObject(s))
							  }
							  else
							  {
									all_acct_contracts_temp_hm.put(md1.getColumnName(s).toLowerCase(),rs_contract_query.getObject(s))

							  }
					}
				}
					l1.add("contract_scope")
					int freq = Collections.frequency(l1,"contract_scope")
					
												  logi.logInfo("frew of  contract_scope is :"+freq)
					
												  if(freq>1)
												  {
					
														
														all_acct_contracts_temp_hm.put("contract_scope["+ freq+"]","MULTI PLAN")
												  }
												  else
												  {
														all_acct_contracts_temp_hm.put("contract_scope","MULTI PLAN")
					
												  }
				//all_acct_contracts_temp_hm.put("contract_scope","MULTI PLAN")
	
			}
			
			
		 //  all_acct_contracts_hm= md_get_all_acct_contracts_queries_m(count_mpc.toInteger(),acct_no,database_to_execute)

		}
		}
		}
		
		
if(type.equalsIgnoreCase("u"))
{
	logi.logInfo("ITS UNIVERSAL ")
	
	logi.logInfo("ITS UNIVERSAL - ACCT NO "+acct_no)
String count_uc_query="SELECT COUNT(CONTRACT_NO) FROM ARIACORE.ACCT_UNIVERSAL_CONTRACTS WHERE acct_no="+acct_no

		String count_uc=db.executeQueryP2(count_uc_query)
		logi.logInfo("ITS UNIVERSAL - Acount_uc "+count_uc)
		
		
		logi.logInfo("ITS MULTI PLAAAAN")
		
	String count_mpc_query="SELECT COUNT(CONTRACT_NO) FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE acct_no="+acct_no
	
	String count_mpc=db.executeQueryP2(count_mpc_query)
		
	logi.logInfo("ITS UNIVERSAL - Acount_uc "+count_mpc)
				
		
	
		logi.logInfo("ITS UNIVERSAL----- here 1 ")
		if (count_uc.toInteger() >= 1  && (filter_status_code.equalsIgnoreCase("0") || filter_status_code.equalsIgnoreCase("NoVal")) )
		{
			String database_to_execute="ARIACORE.ACCT_UNIVERSAL_CONTRACTS"
			
			logi.logInfo("ITS UNIVERSAL----- here 2 ")
			for(int start=1;start<=count_uc.toInteger();start++)
			{
				//RESULT SET
	
	
				logi.logInfo "inside for"
				
				String  contract_query="SELECT A.CONTRACT_NO,A.CLIENT_CONTRACT_ID,A.TYPE_NO,A.LENGTH_MONTHS,A.UPDATE_COMMENTS,TO_CHAR(A.CREATE_DATE,'YYYY-MM-DD') AS CREATE_DATE,TO_CHAR(A.UPDATE_DATE,'YYYY-MM-DD') AS UPDATE_DATE,TO_CHAR(A.START_DATE,'YYYY-MM-DD') AS START_DATE,TO_CHAR(A.END_DATE,'YYYY-MM-DD') AS END_DATE,A.STATUS_CD AS STATUS_CODE FROM (SELECT ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO,CLIENT_CONTRACT_ID,TYPE_NO,UPDATE_COMMENTS,CONTRACT_MONTHS AS LENGTH_MONTHS,CREATE_DATE,UPDATE_DATE,START_DATE,END_DATE,STATUS_CD FROM "+database_to_execute+" WHERE ACCT_NO="+acct_no+") A WHERE A.SEQ="+seq
				ResultSet rs_contract_query = db.executePlaQuery(contract_query)
				
				ResultSetMetaData md1 = rs_contract_query.getMetaData()
				int columns1 = md1.getColumnCount();
				
				//String  contract_query="SELECT A.CONTRACT_NO,A.TYPE_NO,A.LENGTH_MONTHS,TO_CHAR(A.CREATE_DATE,'YYYY-MM-DD') AS CREATE_DATE,TO_CHAR(A.UPDATE_DATE,'YYYY-MM-DD') AS UPDATE_DATE,TO_CHAR(A.START_DATE,'YYYY-MM-DD') AS START_DATE,TO_CHAR(A.END_DATE,'YYYY-MM-DD') AS END_DATE,A.STATUS_CD AS STATUS_CODE FROM (SELECT ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO,TYPE_NO,CONTRACT_MONTHS AS LENGTH_MONTHS,CREATE_DATE,UPDATE_DATE,START_DATE,END_DATE,STATUS_CD FROM "+database_to_execute+" WHERE ACCT_NO="+acct_no+") A WHERE A.SEQ="+startResultSet rs_contract_query = db.executePlaQuery(contract_query)ResultSetMetaData md1 = rs_contract_query.getMetaData()	int columns1 = md1.getColumnCount();
				while (rs_contract_query.next())
				{
				   
					for(int s=1; s<=columns1; ++s)
					{
	
						if(!(rs_contract_query.getObject(s).toString().equals("null")))
						{

							  l1.add(md1.getColumnName(s).toLowerCase())

							  logi.logInfo "KEYSSS 2 : "+l1
							  int freq = Collections.frequency(l1,md1.getColumnName(s).toLowerCase())

							  logi.logInfo("frew of :+"+md1.getColumnName(s).toLowerCase()+" is :"+freq)

							  if(freq>1)
							  {

									all_acct_contracts_temp_hm.put(md1.getColumnName(s).toLowerCase()+"["+ freq +"]",rs_contract_query.getObject(s))
							  }
							  else
							  {
									all_acct_contracts_temp_hm.put(md1.getColumnName(s).toLowerCase(),rs_contract_query.getObject(s))

							  }
					}
				}
				l1.add("contract_scope")
					int freq = Collections.frequency(l1,"contract_scope")
					
												  logi.logInfo("frew of  contract_scope is :"+freq)
					
												  if(freq>1)
												  {
					
														
														all_acct_contracts_temp_hm.put("contract_scope["+ freq +"]","UNIVERSAL")
												  }
												  else
												  {
														all_acct_contracts_temp_hm.put("contract_scope","UNIVERSAL")
					
												  }
	
			}
	
			}
			
		   //all_acct_contracts_hm= md_get_all_acct_contracts_queries_m(count_uc.toInteger(),acct_no,database_to_execute)

		}

}



	}
		
		else if(branch.equalsIgnoreCase("A"))
		{
		
		if(type.equalsIgnoreCase("m"))
		{
			logi.logInfo("ITS MULTI PLAAAAN")
			
		String count_mpc_query="SELECT COUNT(CONTRACT_NO) FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE acct_no="+acct_no
		
		String count_mpc=db.executeQueryP2(count_mpc_query)
		
		

		if (count_mpc.toInteger() >= 1 && (filter_status_code.equalsIgnoreCase("0") || filter_status_code.equalsIgnoreCase("NoVal")) )
		{
			String database_to_execute="ARIACORE.ACCT_MULTI_PLAN_CONTRACTS"
			
			 //for(int start=1;start<=count_mpc.toInteger();start++)
			//{
				//RESULT SET
	
	
				logi.logInfo "inside for"
				String  contract_query="SELECT A.CONTRACT_NO,A.TYPE_NO,A.LENGTH_MONTHS,A.UPDATE_COMMENTS,TO_CHAR(A.CREATE_DATE,'YYYY-MM-DD') AS CREATE_DATE,TO_CHAR(A.UPDATE_DATE,'YYYY-MM-DD') AS UPDATE_DATE,TO_CHAR(A.START_DATE,'YYYY-MM-DD') AS START_DATE,TO_CHAR(A.END_DATE,'YYYY-MM-DD') AS END_DATE,A.STATUS_CD AS STATUS_CODE FROM (SELECT ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO,TYPE_NO,UPDATE_COMMENTS,CONTRACT_MONTHS AS LENGTH_MONTHS,CREATE_DATE,UPDATE_DATE,START_DATE,END_DATE,STATUS_CD FROM "+database_to_execute+" WHERE ACCT_NO="+acct_no+") A WHERE A.SEQ="+seq
				// WHERE A.SEQ="+start
				ResultSet rs_contract_query = db.executePlaQuery(contract_query)
	
				ResultSetMetaData md1 = rs_contract_query.getMetaData()
				int columns1 = md1.getColumnCount();
				while (rs_contract_query.next())
				{
				   
					for(int s=1; s<=columns1; ++s)
					{
	
						if(!(rs_contract_query.getObject(s).toString().equals("null")))
						{

							  l1.add(md1.getColumnName(s).toLowerCase())

							  logi.logInfo "KEYSSS 2 : "+l1
							  int freq = Collections.frequency(l1,md1.getColumnName(s).toLowerCase())

							  logi.logInfo("frew of :+"+md1.getColumnName(s).toLowerCase()+" is :"+freq)

							  if(freq>1)
							  {

									all_acct_contracts_temp_hm.put(md1.getColumnName(s).toLowerCase()+"["+ freq +"]",rs_contract_query.getObject(s))
							  }
							  else
							  {
									all_acct_contracts_temp_hm.put(md1.getColumnName(s).toLowerCase(),rs_contract_query.getObject(s))

							  }
					}
				}
					l1.add("contract_scope")
					int freq = Collections.frequency(l1,"contract_scope")
					
												  logi.logInfo("frew of  contract_scope is :"+freq)
					
												  if(freq>1)
												  {
					
														
														all_acct_contracts_temp_hm.put("contract_scope["+ freq+"]","MULTI PLAN")
												  }
												  else
												  {
														all_acct_contracts_temp_hm.put("contract_scope","MULTI PLAN")
					
												  }
				//all_acct_contracts_temp_hm.put("contract_scope","MULTI PLAN")
	
			}
			
			
		 //  all_acct_contracts_hm= md_get_all_acct_contracts_queries_m(count_mpc.toInteger(),acct_no,database_to_execute)

		//}
		}
		}
		
		
if(type.equalsIgnoreCase("u"))
{
	logi.logInfo("ITS UNIVERSAL ")
	
	logi.logInfo("ITS UNIVERSAL - ACCT NO "+acct_no)
String count_uc_query="SELECT COUNT(CONTRACT_NO) FROM ARIACORE.ACCT_UNIVERSAL_CONTRACTS WHERE acct_no="+acct_no

		String count_uc=db.executeQueryP2(count_uc_query)
		logi.logInfo("ITS UNIVERSAL - Acount_uc "+count_uc)
		
		
		logi.logInfo("ITS MULTI PLAAAAN")
		
	String count_mpc_query="SELECT COUNT(CONTRACT_NO) FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE acct_no="+acct_no
	
	String count_mpc=db.executeQueryP2(count_mpc_query)
		
	logi.logInfo("ITS UNIVERSAL - Acount_uc "+count_mpc)
				
		
	
		logi.logInfo("ITS UNIVERSAL----- here 1 ")
		if (count_uc.toInteger() >= 1  && (filter_status_code.equalsIgnoreCase("0") || filter_status_code.equalsIgnoreCase("NoVal")) )
		{
			String database_to_execute="ARIACORE.ACCT_UNIVERSAL_CONTRACTS"
			
			logi.logInfo("ITS UNIVERSAL----- here 2 ")
			//for(int start=1;start<=count_uc.toInteger();start++)
			//{
				//RESULT SET
	
	
				logi.logInfo "inside for"
				
				String  contract_query="SELECT A.CONTRACT_NO,A.CLIENT_CONTRACT_ID,A.TYPE_NO,A.LENGTH_MONTHS,A.UPDATE_COMMENTS,TO_CHAR(A.CREATE_DATE,'YYYY-MM-DD') AS CREATE_DATE,TO_CHAR(A.UPDATE_DATE,'YYYY-MM-DD') AS UPDATE_DATE,TO_CHAR(A.START_DATE,'YYYY-MM-DD') AS START_DATE,TO_CHAR(A.END_DATE,'YYYY-MM-DD') AS END_DATE,A.STATUS_CD AS STATUS_CODE FROM (SELECT ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO,CLIENT_CONTRACT_ID,TYPE_NO,UPDATE_COMMENTS,CONTRACT_MONTHS AS LENGTH_MONTHS,CREATE_DATE,UPDATE_DATE,START_DATE,END_DATE,STATUS_CD FROM "+database_to_execute+" WHERE ACCT_NO="+acct_no+") A WHERE A.SEQ="+seq
				ResultSet rs_contract_query = db.executePlaQuery(contract_query)
				
				ResultSetMetaData md1 = rs_contract_query.getMetaData()
				int columns1 = md1.getColumnCount();
				
				//String  contract_query="SELECT A.CONTRACT_NO,A.TYPE_NO,A.LENGTH_MONTHS,TO_CHAR(A.CREATE_DATE,'YYYY-MM-DD') AS CREATE_DATE,TO_CHAR(A.UPDATE_DATE,'YYYY-MM-DD') AS UPDATE_DATE,TO_CHAR(A.START_DATE,'YYYY-MM-DD') AS START_DATE,TO_CHAR(A.END_DATE,'YYYY-MM-DD') AS END_DATE,A.STATUS_CD AS STATUS_CODE FROM (SELECT ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO,TYPE_NO,CONTRACT_MONTHS AS LENGTH_MONTHS,CREATE_DATE,UPDATE_DATE,START_DATE,END_DATE,STATUS_CD FROM "+database_to_execute+" WHERE ACCT_NO="+acct_no+") A WHERE A.SEQ="+startResultSet rs_contract_query = db.executePlaQuery(contract_query)ResultSetMetaData md1 = rs_contract_query.getMetaData()	int columns1 = md1.getColumnCount();
				while (rs_contract_query.next())
				{
				   
					for(int s=1; s<=columns1; ++s)
					{
	
						if(!(rs_contract_query.getObject(s).toString().equals("null")))
						{

							  l1.add(md1.getColumnName(s).toLowerCase())

							  logi.logInfo "KEYSSS 2 : "+l1
							  int freq = Collections.frequency(l1,md1.getColumnName(s).toLowerCase())

							  logi.logInfo("frew of :+"+md1.getColumnName(s).toLowerCase()+" is :"+freq)

							  if(freq>1)
							  {

									all_acct_contracts_temp_hm.put(md1.getColumnName(s).toLowerCase()+"["+ freq +"]",rs_contract_query.getObject(s))
							  }
							  else
							  {
									all_acct_contracts_temp_hm.put(md1.getColumnName(s).toLowerCase(),rs_contract_query.getObject(s))

							  }
					}
				}
				l1.add("contract_scope")
					int freq = Collections.frequency(l1,"contract_scope")
					
												  logi.logInfo("frew of  contract_scope is :"+freq)
					
												  if(freq>1)
												  {
					
														
														all_acct_contracts_temp_hm.put("contract_scope["+ freq +"]","UNIVERSAL")
												  }
												  else
												  {
														all_acct_contracts_temp_hm.put("contract_scope","UNIVERSAL")
					
												  }
	
			}
	
			//}
			
		   //all_acct_contracts_hm= md_get_all_acct_contracts_queries_m(count_uc.toInteger(),acct_no,database_to_execute)

		}

}



	}


		logi.logInfo "FINAL HASHMAP GET_ALL_ACCT_CONTRACTS hereee "+all_acct_contracts_temp_hm

		all_acct_contracts_temp_hm=all_acct_contracts_temp_hm.sort()
		logi.logInfo "**** out of  get_all_acct_contracts_m hereee **** "+all_acct_contracts_temp_hm
		
		all_acct_contracts_temp_hm=all_acct_contracts_temp_hm.sort()
		return all_acct_contracts_temp_hm
	
	}
	
	/**
	 * This is the verification method to get the data for get_acct_statement_history_m
	 * @param not needed
	 * @return
	 */
	public def md_get_acct_statement_history_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		ArrayList Statement = new ArrayList();
		String Query="SELECT STATEMENT_NO  from ariacore.acct_statement where client_no = "+library.Constants.Constant.client_no+" and  acct_no ="+acct_no;
		ResultSet resultSet = db.executePlaQuery(Query)
		ResultSetMetaData meta = resultSet.getMetaData()
		int columns = meta.getColumnCount();
		while (resultSet.next())
		{
			for(int u=1; u<=columns; ++u)
			{
				if(resultSet.getObject(u)!=null)
					Statement.add(resultSet.getObject(u));
			}
		}
		Collections.sort(Statement);
		int loops=1;
		for(String stat : Statement)
		{
			Query="SELECT STATEMENT_NO,TO_CHAR(CREATE_DATE, 'YYYY-MM-DD') AS CREATE_DATE,CURRENCY_CD,TO_CHAR(DUE_DATE, 'YYYY-MM-DD') AS DUE_DATE,NEW_CHARGES as new_charges_amount,NEW_PAYMENTS as new_payments_amount,BALANCE_FORWARD as balance_forward_amount,BALANCE as  total_amount,case when STATEMENT_STATUS = 2 then 1 else 0 end as is_paid_ind,ACCT_STATEMENT_SEQ_STR as seq_statement_id  from ariacore.acct_statement where client_no = "+library.Constants.Constant.client_no+" and  acct_no ="+acct_no+" and STATEMENT_NO = "+stat;
			resultSet = db.executePlaQuery(Query)
			ResultSetMetaData md = resultSet.getMetaData()
			columns = md.getColumnCount();
			while (resultSet.next())
			{
				for(int u=1; u<=columns; ++u)
				{
					if(resultSet.getObject(u)!=null)
						line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
				}
			}
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			cal.setTime(sdf.parse(line_item.get("due_date").toString()));
			cal.add(Calendar.DATE, Integer.parseInt(db.executeQueryP2("SELECT PARAM_VAL from ariacore.aria_client_params where PARAM_NAME like '%STATEMENT_GRACE_PERIOD_DAYS%' and client_no = "+library.Constants.Constant.client_no)));
			line_item.put("due_date_plus_grace_period",sdf.format(cal.getTime()));
			Query="select s.master_plan_instance_no,s.client_master_plan_instance_id from ( SELECT distinct (gld.master_plan_instance_no) as master_plan_instance_no,pim.PLAN_INSTANCE_CDID as client_master_plan_instance_id FROM ariacore.gl_detail gld JOIN ariacore.plan_instance pim on gld.master_plan_instance_no = pim.plan_instance_no and gld.client_no = pim.client_no WHERE invoice_no IN (SELECT invoice_no from ariacore.acct_statement WHERE client_no = "+library.Constants.Constant.client_no+" AND acct_no = "+acct_no+" AND STATEMENT_NO = "+stat+"))s";
			resultSet = db.executePlaQuery(Query)
			md = resultSet.getMetaData()
			columns = md.getColumnCount();
			while (resultSet.next())
			{
				for(int u=1; u<=columns; ++u)
				{
					if(resultSet.getObject(u)!=null)
					{
						 boolean is_having=true
							  int k=1;
							  while (is_having)
							  {
								  if(k==1)
								  {if(!line_item.keySet().asList().contains(md.getColumnName(u).toLowerCase())){is_having=false}}
								  else {if(!line_item.keySet().asList().contains(md.getColumnName(u).toLowerCase()+"["+k.toString()+"]")){is_having=false}}
								  k+=1;
							  }
							  k-=1;
							  logi.logInfo "the value of k "+k;
							  logi.logInfo "putting in output "+resultSet.getObject(u);
							   if(k>1){
								   line_item.put((md.getColumnName(u)).toLowerCase()+"["+k.toString()+"]",(resultSet.getObject(u)));
								   }
							   else{
								   line_item.put((md.getColumnName(u)).toLowerCase(),(resultSet.getObject(u)));
								   
								   }
					}
				}
			}
		
		line_item.sort();
		row_item.put("statements_history_row["+(loops-1)+"]",line_item.sort())
		line_item = new LinkedHashMap();
		loops+=1;
	  }
	
		row_item.sort();
		return row_item
		
	}
	
	/**
	 * This method if to verify the total amount in get_acct_preview_statement_m API
	 * This works only for statement template 106
	 */
	String md_TotalInXML106_for_preview_statement_m(String input)
	{
		String content = md_getValFromNodeAndIndex_m("out_statement,1")
		content = content.substring(content.indexOf( "Total Charges This Statement:" ));
		content = content.substring(content.indexOf( "<td class=\"totals-rightalign\">" )+30);
		String cont2=content.substring(content.indexOf( "</td>" ));
		content = content.replace(cont2,"");
		return content
	}
	
	/**
	 * This is the method for get_invoices_to_writeoff_or_dispute_m API
	 * @param no param needed
	 * @return hashmap
	 */
	public def md_get_invoices_to_writeoff_or_dispute_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		String MPI = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/master_plan_instance_no[1]")
		ArrayList Invoices = new ArrayList();
		String joiningMPI = "";
		String appendMPI = "";
		if(!MPI.equals("NoVal")){joiningMPI = " join ariacore.plan_instance pi on pi.client_no = ai.client_no and pi.acct_no=ai.acct_no join ariacore.plan_instance_master pim on pim.client_no=pi.client_no and pi.plan_instance_no  = pim.plan_instance_no and ai.billing_group_no = pim.billing_group_no ";appendMPI = " and pim.plan_instance_no = "+MPI}
		String Query="SELECT ai.INVOICE_NO from ariacore.all_invoices ai "+joiningMPI+" where ai.client_no ="+library.Constants.Constant.client_no+" and ai.acct_no = "+acct_no+appendMPI;
		ResultSet resultSet = db.executePlaQuery(Query)
		ResultSetMetaData meta = resultSet.getMetaData()
		int columns = meta.getColumnCount();
		while (resultSet.next())
		{
			for(int u=1; u<=columns; ++u)
			{
				if(resultSet.getObject(u)!=null)
					Invoices.add(resultSet.getObject(u));
			}
		}
		Collections.sort(Invoices);
		int loops=1;
		for(String invoice : Invoices)
		{
			Query="SELECT ai.INVOICE_NO,TO_CHAR(ai.BILL_DATE, 'YYYY-MM-DD') AS BILL_DATE,TO_CHAR(ai.DUE_DATE, 'YYYY-MM-DD') AS DUE_DATE,ai.DEBIT as invoice_amount,ai.CREDIT as total_paid,ai.TOTAL_DUE as balance_due,ai.INVOICE_TYPE_CODE as invoice_type_cd from ariacore.all_invoices ai "+joiningMPI+" where ai.client_no ="+library.Constants.Constant.client_no+" and ai.acct_no = "+acct_no+appendMPI+" and ai.invoice_no = "+invoice;
			resultSet = db.executePlaQuery(Query)
			ResultSetMetaData md = resultSet.getMetaData()
			columns = md.getColumnCount();
			while (resultSet.next())
			{
				for(int u=1; u<=columns; ++u)
				{
					if(resultSet.getObject(u)!=null)
						line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
				}
			}
		
			line_item.put("days_past_due",db.executeQueryP2("SELECT (SELECT trunc(ARIACORE.ARIAVIRTUALTIME("+library.Constants.Constant.client_no+")) from DUAL) -  trunc(DD) as days  FROM (SELECT max(Due_DATE) as DD from ariacore.all_invoices ai JOIN ariacore.all_invoice_details aid  ON aid.client_no =ai.client_no  AND ai.invoice_no  = aid.invoice_no  WHERE ai.client_no  = "+library.Constants.Constant.client_no+"  AND aid.invoice_no ="+line_item.get("invoice_no")+")"))
			line_item.sort();
			row_item.put("invoice_details_row["+(loops-1)+"]",line_item.sort())
			line_item = new LinkedHashMap();
			loops+=1;
		}
		row_item.sort();
		return row_item
		
	}
	
	/**
	 * method for create_writeoff_or_dispute_m
	 * @param not needed
	 * @return hashmap verification
	 */
	public def md_create_writeoff_or_dispute_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		String date = md_VTdate_m("0");
		int param_date_number = Integer.parseInt(db.executeQueryP2("SELECT PARAM_VAL from ariacore.aria_client_params where PARAM_NAME like '%DISPUTE_HOLD_EXPIRATION_DAYS%' and client_no = "+library.Constants.Constant.client_no))
		String invoice_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/invoice_no[1]")
		String Query="SELECT atn.EVENT_NO as writeoff_transaction_id,wo.INVOICE_NO,aid.service_no,cs.client_service_id,atn.APPLIED_AMOUNT as original_amount,wo.amount,TO_CHAR(wo.WRITEOFF_DATE, 'dd-MON-yy hh.mi.ss AM') as datea,wo.WRITE_OFF_CD as writeoff_reasoncode from ariacore.WRITE_OFFS wo join ariacore.acct_transaction atn on atn.client_no=wo.client_no and atn.source_no=wo.write_off_no join ariacore.all_invoice_details aid on aid.client_no=wo.client_no and aid.invoice_no = wo.invoice_no join ariacore.CLIENT_SERVICE cs on cs.client_no = wo.client_no and cs.service_no = aid.service_no where wo.client_no ="+library.Constants.Constant.client_no+" and wo.invoice_no ="+invoice_no;
		ResultSet resultSet = db.executePlaQuery(Query)
		ResultSetMetaData md = resultSet.getMetaData()
		int columns = md.getColumnCount();
		int loops=1
		while (resultSet.next())
		{
		
			 for(int u=1; u<=columns; ++u)
			{
				if(resultSet.getObject(u)!=null)
				{
				if(md.getColumnName(u).toLowerCase().equals("datea")) line_item.put("date",resultSet.getObject(u));
				else line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
				}
			}
			line_item.put("dispute_ind",getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/do_dispute[1]"));
			if(getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/do_dispute[1]").equals("1"))
			{	line_item.put("can_unsettle","True")
				line_item.put("dispute_creation_date",date);
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
				cal.setTime(sdf.parse(date));
				cal.add(Calendar.DATE, param_date_number);
				line_item.put("dispute_expiry_date",sdf.format(cal.getTime()));
			}
			else line_item.put("can_unsettle","False")
			line_item.sort();
			row_item.put("writeoff_transactions_row["+(loops-1)+"]",line_item.sort())
			line_item = new LinkedHashMap();
			loops+=1;
		}
		row_item.sort();
		return row_item
		
	}
	
	/**
	 * This method is to verify get_writeoff_details_m
	 * @param not needed
	 * @return hashmap
	 */
	public def md_get_writeoff_details_m(String param)
	{
		ConnectDB db = new ConnectDB()
		LinkedHashMap line_item = new LinkedHashMap()
		LinkedHashMap row_item = new LinkedHashMap()
		String writeoff_transaction_id=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/writeoff_transaction_id[1]")
		String Query="SELECT wo.invoice_no,TO_CHAR(ai.BILL_DATE, 'YYYY-MM-DD') AS invoice_bill_date,atn.EVENT_NO as invoice_transaction_id,atn.AMOUNT as original_transaction_amount, atn.APPLIED_AMOUNT as writeoff_transaction_amount,TO_CHAR(aid.start_date, 'YYYY-MM-DD') AS writeoff_trn_start_date,TO_CHAR(aid.end_date, 'YYYY-MM-DD') AS writeoff_trn_end_date from ariacore.write_offs wo join ariacore.all_invoices ai on ai.client_no=wo.client_no and ai.invoice_no =wo.invoice_no join ariacore.acct_transaction atn on atn.client_no=wo.client_no and atn.acct_no=ai.acct_no and atn.invoice_no=ai.invoice_no join ariacore.all_invoice_details aid on aid.client_no = atn.client_no and aid.invoice_no = atn.invoice_no and aid.INVOICE_DETAIL_NO=atn.source_no where wo.client_no = "+library.Constants.Constant.client_no+" and wo.WRITE_OFF_NO = (SELECT atn.SOURCE_NO from ariacore.acct_transaction atn where atn.client_no = wo.client_no and transaction_type = 6 and atn.EVENT_NO ="+writeoff_transaction_id+")";
		ResultSet resultSet = db.executePlaQuery(Query)
		ResultSetMetaData md = resultSet.getMetaData()
		int columns = md.getColumnCount();
		int loops=1
		while (resultSet.next())
		{
		
			 for(int u=1; u<=columns; ++u)
			{
				if(resultSet.getObject(u)!=null)
				{
					if(md.getColumnName(u).toLowerCase().equals("writeoff_trn_start_date"))
					line_item.put("writeoff_transaction_start_date",resultSet.getObject(u));
					else if (md.getColumnName(u).toLowerCase().equals("writeoff_trn_end_date"))
					line_item.put("writeoff_transaction_end_date",resultSet.getObject(u));
					else line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
				}
			}
			line_item.sort();
			row_item.put("writeoff_detail_row["+(loops-1)+"]",line_item.sort())
			line_item = new LinkedHashMap();
			loops+=1;
		}
		row_item.sort();
		return row_item
		
	}
	
	/**
	 * This method  it to know the BG  balance for the given MPI
	 * @param MPI param
	 * @return the amount balance of the BG
	 */
	public def md_BG_balance_m(String MPI)
	{
		ConnectDB db = new ConnectDB()
		return db.executeQueryP2("SELECT SUM(TOTAL_DUE) from ariacore.all_invoices ai join ariacore.plan_instance_master pim on pim.client_no = ai.client_no and pim.billing_group_no=ai.billing_group_no where pim.client_no ="+library.Constants.Constant.client_no+" and pim.plan_instance_no = "+im.md_GET_ACCT_OR_INSTANCE_NO_m(MPI,null).toString()).toString();
	}
	
	/**
	 * This method  it to know the MPI  balance for the given MPI
	 * @param MPI param
	 * @return the amount balance of the BG
	 */
	public def md_MPI_balance_m(String MPI)
	{
		ConnectDB db = new ConnectDB()
		return db.executeQueryP2("SELECT STACK_BALANCE from ariacore.plan_instance_master where client_no ="+library.Constants.Constant.client_no+" and plan_instance_no = "+im.md_GET_ACCT_OR_INSTANCE_NO_m(MPI,null).toString()).toString();
	}
	
	LinkedHashMap md_Contract_Plan_Inst_Info_m(String acct_hierarchy1)
	{
		
		
		logi.logInfo("INTO md_Contract_Plan_Inst_Info_m . . . ")
		String acct_hierarchy=acct_hierarchy1.split("#")[0]
		String seq=acct_hierarchy1.split("#")[1]
		logi.logInfo("seq "+seq)
			LinkedHashMap all_acct_contracts_temp_hm =new LinkedHashMap()
			LinkedHashMap a1 =new LinkedHashMap()
			ConnectDB db = new ConnectDB()
			String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
			logi.logInfo("Given Acct no:" + acct_no)
			//int count =1
	
	
				
				String contract_no_query="SELECT A.CONTRACT_NO FROM (SELECT  ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE ACCT_NO="+acct_no+") A WHERE A.SEQ=1"
				String contract=db.executeQueryP2(contract_no_query)
	
				logi.logInfo("Contract number : "+contract)
				
				String plan_instance_query="select a.plan_instance_no,a.client_plan_instance_id,a.plan_name, a.plan_instance_status_cd,a.plan_instance_status_label from (Select row_number() over (order by  pi.plan_instance_no) as seq ,  pi.plan_instance_no as plan_instance_no,pi.plan_instance_cdid as client_plan_instance_id,cp.plan_name as plan_name,pi.status_cd as plan_instance_status_cd,g.label as plan_instance_status_label from ariacore.plan_instance pi join ariacore.ALL_PLAN_INST_ACTIVE_CONTRACTS ac on pi.plan_instance_no = ac.plan_instance_no and pi.client_no = ac.client_no and pi.acct_no=ac.acct_no join ariacore.client_plan cp on pi.plan_no =cp.plan_no and pi.client_no=cp.client_no join ariacore.global_plan_status_code g on pi.status_cd =g.plan_status_cd  where pi.acct_no ="+acct_no+" and ac.CONTRACT_NO ="+contract+") A where A.seq="+seq
				ResultSet rs2 = db.executePlaQuery(plan_instance_query)
				ResultSetMetaData md2 = rs2.getMetaData();
				int columns3 = md2.getColumnCount();
				int r=1
				while (rs2.next())
				{
					all_acct_contracts_temp_hm =new LinkedHashMap()
					logi.logInfo("Created contract )"+count)
					
					for(int j=1; j<=columns3; j++)
								{
									if((rs2.getObject(j))!=null)
									{
										all_acct_contracts_temp_hm.put((md2.getColumnName(j)).toLowerCase(),(rs2.getObject

(j)));
									}
								}
								
								logi.logInfo "BEFORRRRRRRRRREEEEEEEEEEEEE :"+all_acct_contracts_temp_hm
								all_acct_contracts_temp_hm=all_acct_contracts_temp_hm.sort()
								logi.logInfo "AFTERRRRRRRRRRRRRRRRRRRRR :"+all_acct_contracts_temp_hm
								a1.put("contract_plan_instance_info_row["+count+"]",all_acct_contracts_temp_hm )
								++count
								
				}
				
	
				
							logi.logInfo "**** out of  get_all_acct_contracts_queries_m **** "
			
			
			logi.logInfo("temp1")
			
			a1=a1.sort()
			logi.logInfo("sorteed"+a1)
			
			//return a1
			all_acct_contracts_temp_hm=all_acct_contracts_temp_hm.sort()
			return all_acct_contracts_temp_hm
			
	}
	

	LinkedHashMap md_get_inst_contract_gen_detail_m(String acct_hierarchy1)
	
	
	{
		String acct_hierarchy=acct_hierarchy1.split("#")[0]
		String type=acct_hierarchy1.split("#")[1]
		String seq=acct_hierarchy1.split("#")[2]
		
		logi.logInfo "**** into get_all_acct_contracts_m **** "
		
		//String filter_status_code=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//lit:get_all_acct_contracts_m[1]/filter_status_code[1]")
		//logi.logInfo "---------------------------------- filter "+filter_status_code
		LinkedHashMap all_acct_contracts_hm= new LinkedHashMap()
		LinkedHashMap all_acct_contracts_temp_hm =new LinkedHashMap()
		LinkedList l1 = new LinkedList()
		InputMethods im = new InputMethods()
		ConnectDB db = new ConnectDB()
		String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		String contract_no=getValueFromRequest(library.Constants.Constant.SERVICENAME,"//*/contract_no[1]")
		logi.logInfo "**** CONTRACT NO : **** "+contract_no
		
		
		/*logi.logInfo("Given Acct no:" + acct_no)
		if(type.equalsIgnoreCase("m"))
		{
			logi.logInfo("ITS MULTI PLAAAAN")
			
		String count_mpc_query="SELECT COUNT(CONTRACT_NO) FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE acct_no="+acct_no
		
		String count_mpc=db.executeQueryP2(count_mpc_query)
		
		

		if (count_mpc.toInteger() >= 1 )
		{
			String database_to_execute="ARIACORE.ACCT_MULTI_PLAN_CONTRACTS"
			
			 for(int start=1;start<=count_mpc.toInteger();start++)
			{
				//RESULT SET
	
	
				logi.logInfo "inside for"
				
				*/
				//String  contract_query="SELECT A.TYPE_NO,A.LENGTH_MONTHS,A.UPDATE_COMMENTS,TO_CHAR(A.CREATE_DATE,'YYYY-MM-DD') AS CREATE_DATE,TO_CHAR(A.UPDATE_DATE,'YYYY-MM-DD') AS UPDATE_DATE,TO_CHAR(A.START_DATE,'YYYY-MM-DD') AS START_DATE,TO_CHAR(A.END_DATE,'YYYY-MM-DD') AS END_DATE,A.STATUS_CD AS STATUS_CODE FROM (SELECT ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO,TYPE_NO,UPDATE_COMMENTS,CONTRACT_MONTHS AS LENGTH_MONTHS,CREATE_DATE,UPDATE_DATE,START_DATE,END_DATE,STATUS_CD FROM "+database_to_execute+" WHERE ACCT_NO="+acct_no+") A WHERE A.SEQ="+seq
		String  contract_query="SELECT A.TYPE_NO,A.LENGTH_MONTHS,A.UPDATE_COMMENTS,TO_CHAR(A.CREATE_DATE,'YYYY-MM-DD') AS CREATE_DATE,TO_CHAR(A.UPDATE_DATE,'YYYY-MM-DD') AS UPDATE_DATE,TO_CHAR(A.START_DATE,'YYYY-MM-DD') AS START_DATE,TO_CHAR(A.END_DATE,'YYYY-MM-DD') AS END_DATE,A.STATUS_CD AS STATUS_CODE FROM (SELECT ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO,TYPE_NO,UPDATE_COMMENTS,CONTRACT_MONTHS AS LENGTH_MONTHS,CREATE_DATE,UPDATE_DATE,START_DATE,END_DATE,STATUS_CD FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE CONTRACT_NO="+contract_no+")A"
				ResultSet rs_contract_query = db.executePlaQuery(contract_query)
	
				ResultSetMetaData md1 = rs_contract_query.getMetaData()
				int columns1 = md1.getColumnCount();
				while (rs_contract_query.next())
				{
				   
					for(int s=1; s<=columns1; ++s)
					{
	
						if(!(rs_contract_query.getObject(s).toString().equals("null")))
						{

							  l1.add(md1.getColumnName(s).toLowerCase())

							  logi.logInfo "KEYSSS 2 : "+l1
							  int freq = Collections.frequency(l1,md1.getColumnName(s).toLowerCase())

							  logi.logInfo("frew of :+"+md1.getColumnName(s).toLowerCase()+" is :"+freq)

							  if(freq>1)
							  {

									all_acct_contracts_temp_hm.put(md1.getColumnName(s).toLowerCase()+"["+ freq

+"]",rs_contract_query.getObject(s))
							  }
							  else
							  {
									all_acct_contracts_temp_hm.put(md1.getColumnName(s).toLowerCase(),rs_contract_query.getObject

(s))

							  }
					}
				}
					
	
			}
			
			
		 //  all_acct_contracts_hm= md_get_all_acct_contracts_queries_m(count_mpc.toInteger(),acct_no,database_to_execute)

		//}
		//}
		//}
		
		
		logi.logInfo "FINAL HASHMAP GET_ALL_ACCT_CONTRACTS "+all_acct_contracts_hm

		logi.logInfo "**** out of  get_all_acct_contracts_m **** "+all_acct_contracts_temp_hm
		
		all_acct_contracts_temp_hm=all_acct_contracts_temp_hm.sort()
		return all_acct_contracts_temp_hm
	
	}
	
	
	LinkedHashMap md_get_inst_contract_instance_det_m(String acct_hierarchy1)
	{
		String acct_hierarchy=acct_hierarchy1.split("#")[0]
		Integer seq=acct_hierarchy1.split("#")[1].toInteger()
		
			LinkedHashMap all_acct_contracts_temp_hm =new LinkedHashMap()
			LinkedHashMap a1 =new LinkedHashMap()
			ConnectDB db = new ConnectDB()
			String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
			//String acct_no ="11459903"
			logi.logInfo("Given Acct no:" + acct_no)
			LinkedList plan_inst_no_list= new LinkedList()
			int count =1
	
			String count_pino_q="select count(PLAN_INSTANCE_NO) from ariacore.ALL_PLAN_INST_ACTIVE_CONTRACTS where acct_no="+acct_no+" and contract_scope='MULTI PLAN'"
			String count_pino=db.executeQueryP2(count_pino_q).toInteger()
			
			logi.logInfo("count_pino" + count_pino)
			
			
			for(int t=1;t<=count_pino;t++)
			{
				  String plan_inst_q="SELECT a.PLAN_INSTANCE_NO FROM (SELECT PLAN_INSTANCE_NO,row_number() over(order by PLAN_INSTANCE_NO ) as pno from ariacore.ALL_PLAN_INST_ACTIVE_CONTRACTS where acct_no="+acct_no+" and contract_scope='MULTI PLAN')A where a.pno="+t
				   String plan_inst=db.executeQueryP2(plan_inst_q)
				  plan_inst_no_list.add(plan_inst)
				  logi.logInfo("ADDINg in plan_inst_no_list" + plan_inst_no_list)
				  
			}

				  
				  String contract_no_query="SELECT A.CONTRACT_NO FROM (SELECT  ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE ACCT_NO="+acct_no+") A WHERE A.SEQ=1"
				  String contract=db.executeQueryP2(contract_no_query)

				  logi.logInfo("  SEQQQQQ : "+seq)
				  
				  logi.logInfo("  SEQQQQQ : "+seq)
				String mpi_no = plan_inst_no_list.get(--seq)
				logi.logInfo("  MPIIII : "+mpi_no)
	
				logi.logInfo("Contract number : "+contract)
				
				String plan_instance_query="select pi.plan_instance_no as plan_instance_no,pi.plan_instance_cdid as client_plan_instance_id,cp.plan_name as plan_name,pi.status_cd as plan_instance_status_cd,g.label as plan_instance_status_label from ariacore.plan_instance pi join ariacore.ALL_PLAN_INST_ACTIVE_CONTRACTS ac on pi.plan_instance_no = ac.plan_instance_no and pi.client_no = ac.client_no and pi.acct_no=ac.acct_no join ariacore.client_plan cp on pi.plan_no =cp.plan_no and pi.client_no=cp.client_no join ariacore.global_plan_status_code g on pi.status_cd =g.plan_status_cd  where pi.acct_no = "+acct_no+" and ac.CONTRACT_NO = "+contract
				//+" and pi.plan_instance_no="+mpi_no
				ResultSet rs2 = db.executePlaQuery(plan_instance_query)
				ResultSetMetaData md2 = rs2.getMetaData();
				int columns3 = md2.getColumnCount();
				int r=1
				while (rs2.next())
				{
					all_acct_contracts_temp_hm =new LinkedHashMap()
					logi.logInfo("Created contract )"+count)
					
					for(int j=1; j<=columns3; j++)
								{
									if((rs2.getObject(j))!=null)
									{
										all_acct_contracts_temp_hm.put((md2.getColumnName(j)).toLowerCase(),(rs2.getObject(j)));
										all_acct_contracts_temp_hm=all_acct_contracts_temp_hm.sort()
										}
								}
								
								
								
				}
				
	
				
							logi.logInfo "**** out of  get_all_acct_contracts_queries_m **** "
			
			
			logi.logInfo("temp1")
			logi.logInfo("BEFOREEEEE "+all_acct_contracts_temp_hm)
			all_acct_contracts_temp_hm=all_acct_contracts_temp_hm.sort()
			logi.logInfo("AFTER "+all_acct_contracts_temp_hm)
			return all_acct_contracts_temp_hm
			}
	
	
	
	String md_get_contract_status_m(String input)
	{
		logi.logInfo("Getting contract status . . . .")
		ConnectDB db = new ConnectDB()
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		String contract_no=null
		
		if(input.equalsIgnoreCase("create_acct_complete_m"))

		{
			// contract_no=getValueFromRequest("create_acct_complete_m", "//*/contract_no[1]")
			contract_no=getValueFromResponse("create_acct_complete_m", "//ns1:contract_no[1]")
			logi.logInfo("The contract CREAYEEE  no . . . ."+contract_no)
		}
		else if (input.equalsIgnoreCase("create_instance_contract_m"))

		{
			 contract_no=getValueFromResponse("create_instance_contract_m", "//*/contract_no[1]")
			logi.logInfo("The contract  GET no . . . ."+contract_no)
		}
		
		
		String contract_status_q="SELECT LABEL from ariacore.ACCT_PLAN_CONTRACT_STATUS where status_cd = (select STATUS_CD from ariacore.ACCT_MULTI_PLAN_CONTRACTS where contract_no="+contract_no+" and client_no="+client_no+")"
		String contract_status=db.executeQueryP2(contract_status_q)
		
		
		logi.logInfo("Tllllllll :"+contract_status_q)
		return contract_status
		
		
		
	}
	
	
	
	String md_get_aria_xml_stmt_m(String acct_hierarchy)
	{
	
		logi.logInfo("Intoo md_get_aria_xml_stmt_m. . . ")
		ConnectDB db = new ConnectDB()
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		String result=null
		String acct_no="11393086"
		String xml_stmt_q="select XML_STATEMENT from ariacore.xml_document where acct_no="+acct_no
		String xml_stmt=db.executeQueryP2(xml_stmt_q)
		
		//logi.logInfo("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA DB XML : "+xml_stmt)
		
		xml_stmt.replaceAll("\\s+","")
		xml_stmt.trim()
		xml_stmt=xml_stmt.substring(0, xml_stmt.length()-1)
		
		logi.logInfo("length DB :"+xml_stmt.length())
		
		
		
				String api_xml=getValueFromResponse("get_aria_xml_statement", "//*/xml_statement_content[1]")
		
		//logi.logInfo("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO  API XML : "+api_xml)
		api_xml.replaceAll("\\s+","")
		api_xml.trim()
		logi.logInfo("length API :"+api_xml.length())
		
		for(int i=0;i<xml_stmt.length();i++)
		{
			
			
			if(!(xml_stmt.charAt(i).compareTo(api_xml.charAt(i))))
			logi.logInfo("DB -> : "+xml_stmt.charAt(i)+" API -> : "+api_xml.charAt(i) +" Lenght :"+i)
			else
			{
				logi.logInfo("Y DAAAA DB -> : "+xml_stmt.charAt(i)+" API -> : "+api_xml.charAt(i)+" Lenght :"+i)
				
			}
				
		}
		
		
		if(xml_stmt.equalsIgnoreCase(api_xml))
			
		result ="PASS"
		else
		
		result "FAIL"
		
		
		





		
		
		return result
	
	}
	

	String md_get_dunning_state_m(String acct_hierarchy)
	{
		
		LinkedHashMap a1 =new LinkedHashMap()
		ConnectDB db = new ConnectDB()
		String plan_inst_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		String dunning_status_q="SELECT DUNNING_STATE FROM ARIACORE.PLAN_INSTANCE_MASTER WHERE PLAN_INSTANCE_NO="+plan_inst_no
		return db.executeQueryP2(dunning_status_q)
	}
		
LinkedHashMap md_get_payments_on_invoice_m (String acct_hierarchy)
{
	
	logi.logInfo "**** into md_get_payments_on_invoice_m ****  "
	ConnectDB db = new ConnectDB()
	String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
	String invoice_no=getValueFromRequest(library.Constants.Constant.SERVICENAME,"//*/invoice_no[1]")
	
	LinkedList l1= new LinkedList()
	
	LinkedHashMap final_hm = new LinkedHashMap()
	int count=0;
	String payments_q=null
	
	int count1=db.executeQueryP2("select count(*) from ariacore.EXTERNAL_PAYMENTS where ACCT_NO ="+acct_no)
	
	
	if(count1>0)
	{
		
		payments_q="SELECT ACT.EVENT_NO AS TRANSACTION_ID,ACT.TRANSACTION_TYPE,'External Payment' AS DESCRIPTION,ACT.AMOUNT,ACT.APPLIED_AMOUNT,ACT.CURRENCY_CD AS CURRENCY_CODE,TO_CHAR(ACT.CREATE_DATE,'YYYY-MM-DD') AS transaction_date,'-1' AS PAYMENT_TYPE from ariacore.ACCT_TRANSACTION ACT JOIN ARIACORE.EXTERNAL_PAYMENTS EP ON EP.PAYMENT_ID=ACT.SOURCE_NO where ACT.acct_no="+acct_no
	
	}
	else
	{
	
	  payments_q="SELECT  distinct AP.PAYMENT_TRANS_EVENT_NO as TRANSACTION_ID,AP.PAYMENT_TRANS_TYPE as  TRANSACTION_TYPE,AP.AMOUNT,AP.PAY_SOURCE AS PAYMENT_SRC_SUFFIX,AP.PAY_METHOD AS PAYMENT_METHOD_NO,AP.CURRENCY_CD AS CURRENCY_CODE,AI.PAY_METHOD AS PAYMENT_TYPE,AI.PAY_METHOD_NAME as DESCRIPTION,ATN.APPLIED_AMOUNT,TO_CHAR(ATN.CREATE_DATE,'YYYY-MM-DD') AS TRANSACTION_DATE, ATN.STATEMENT_NO FROM ariacore.ALL_PAYMENTS AP JOIN  ariacore.ALL_INVOICES AI on AI.ACCT_NO=AP.ACCT_NO JOIN ariacore.ACCT_TRANSACTION ATN on ATN.ACCT_NO = AP.ACCT_NO WHERE  AP.ACCT_NO="+acct_no+" AND AP.PROC_STATUS_CODE  IS NOT NULL AND ATN.INVOICE_NO="+invoice_no+" AND ATN.transaction_type=21"
	}
			ResultSet rs_payments_q = db.executePlaQuery(payments_q)

			ResultSetMetaData md1 = rs_payments_q.getMetaData()
			int columns1 = md1.getColumnCount();
			while (rs_payments_q.next())
			{
				LinkedHashMap payments_info = new LinkedHashMap()
				for(int s=1; s<=columns1; ++s)
				{

					if(!(rs_payments_q.getObject(s).toString().equals("null")))
					{

						  l1.add(md1.getColumnName(s).toLowerCase())

						  logi.logInfo "KEYSSS 2 : "+l1
						  int freq = Collections.frequency(l1,md1.getColumnName(s).toLowerCase())

						  logi.logInfo("frew of :+"+md1.getColumnName(s).toLowerCase()+" is :"+freq)

						  if(freq>1)
						  {

								payments_info.put(md1.getColumnName(s).toLowerCase()+"["+ freq

+"]",rs_payments_q.getObject(s))
						  }
						  else
						  {
								payments_info.put(md1.getColumnName(s).toLowerCase(),rs_payments_q.getObject

(s))

						  }
				}
			}
				
				payments_info=payments_info.sort()
				final_hm.put("invoice_payment_details_row["+count+"]",payments_info)
				++count
		}
			
			
		
		return final_hm
	
	
}




public  LinkedHashMap md_getspecificchildnodesRname_m(String parentnode) {
		try{
								logi.logInfo(parentnode);
								logi.logInfo("inside md_getspecificchildnodesRname_m");
								pnode=parentnode.split("\\#")[0].split("\\,")[0]
								int index = Integer.parseInt(parentnode.split("\\#")[0].split("\\,")[1])
								logi.logInfo "nodes to remove : "+parentnode.split("\\#")[1].split(",");
								String [] rname=parentnode.split("\\#")[1].split(",");
								logi.logInfo ""+pnode;
							//	int index=Integer.parseInt(parentnode.split("\\,")[1]);
								index-=1;
								logi.logInfo ""+index;
								ArrayList<String> childnodes = new ArrayList<String>();
								ArrayList<ArrayList> all = new ArrayList<ArrayList>();
								LinkedHashMap<String, String> inner = new LinkedHashMap<String, String>();

								String xmlRecords = library.Constants.Constant.RESPONSE


								//for getting the values of child nodes

								childnodes=getnode(xmlRecords,pnode,index);
								logi.logInfo("CHILDDD :"+childnodes)
								for (String number : childnodes)

								{
												String a=library.Constants.Constant.service.toString()

												logi.logInfo("values :"+getval2(xmlRecords,number,index))

												if(getval2(xmlRecords,number,index)!=null)
												
													{
														if(!Arrays.asList(rname).contains(number.replace("ns1:","")))
													inner.put(number.replace("ns1:",""),getval2(xmlRecords,number,index))
													
													}
								}
								logi.logInfo ("hashmap"+inner)
								inner=inner.sort();
								return inner
								}
								catch(Exception e)
										{
											return "NoVal"
										}
								}

public String md_get_aria_xml_stmt_m(String input, LinkedHashMap<String,String> m)
{

	   logi.logInfo("Intoo md_get_aria_xml_stmt_m. . . ")
	   ConnectDB db = new ConnectDB()
	   //def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	   //String client_no=Constant.mycontext.expand('${Properties#Client_No}')
	   String result=null
	   logi.logInfo("Into Test ")
	  // String acct_no=vm.getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
	   String acct_no=md_GET_ACCT_OR_INSTANCE_NO_m(input,m)
	   String db_xml_stmt_q="select XML_STATEMENT from ariacore.xml_document where acct_no="+acct_no
	   String db_xml_stmt=db.executeQueryP2(db_xml_stmt_q)
	   logi.logInfo("222222222")
					
	   db_xml_stmt.replaceAll("\\s+","")
	   db_xml_stmt.trim()
	   db_xml_stmt=db_xml_stmt.substring(0, db_xml_stmt.length()-1)
	   
	   logi.logInfo("length DB :"+db_xml_stmt.length())
	   
	   String api_xml_stmt=vm.getValueFromResponse("get_aria_xml_statement_m", "//*/xml_statement_content[1]")
	   logi.logInfo("API Xml Statement"+api_xml_stmt)
	   api_xml_stmt.replaceAll("\\s+","")
	   api_xml_stmt.trim()
	   logi.logInfo("length API :"+api_xml_stmt.length())
	   
	   if(api_xml_stmt.equalsIgnoreCase(db_xml_stmt))
			  
	   result ="PASS"
	   else
	   
	   result "FAIL"
	   
	   
	   return result

}


/**
 * md_prorated_credit_m to calculate the credit produced
 * @param s proration days
 * @return credit amount
 */
 public def md_prorated_credit_m(String s)
 {
	   logi.logInfo "into the md_prorated_credit_m"
	   ConnectDB db = new ConnectDB()
	   Calendar cal = Calendar.getInstance();
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	   cal.setTime(sdf.parse(md_VTdate_m("0").toString()));
	   int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	   int days2 = days-(Integer.parseInt(s.split("#")[0]))
	   int mamount = 0;
	   ResultSet rs = db.executePlaQuery("SELECT DEBIT from ariacore.all_invoice_details where client_no = "+library.Constants.Constant.client_no+" and invoice_no = "+im.md_get_invoice_no_m(s.split("#")[1],null)+" and comments not like '%Activation%'")
	   ResultSetMetaData meta = rs.getMetaData()
	   while (rs.next())
	   {
			 for(int u=1; u<=meta.getColumnCount(); ++u)
			 {
				   if(rs.getObject(u)!=null)
						 mamount+=Integer.parseInt((rs.getObject(u).toString()));
			 }
	   }
	   double amount = (mamount)*(days2/days);
	   double roundOff = Math.round(amount*100000)/100000;
	   String.format("%.5f", roundOff);
	   logi.logInfo "expected amount "+amount +" "+roundOff
	   NumberFormat f = NumberFormat.getInstance();
	   f.setMaximumFractionDigits(5);
	   f.setMinimumFractionDigits(5);
	   return f.format(amount);
	   
 }

 public def md_Credits_fro_acct_m (String testcaseID){
	 
	 
	 logi.logInfo "into md_Credits_fro_acct_m"
	 //String acct_no=getValueFromResponse('create_acct_complete_m',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1_M)
	 InputMethods im=new InputMethods()
	 // TO GET TJE ACCT NUMBER
	 ConnectDB db=new ConnectDB();
	 String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
	 logi.logInfo("acct_no"+acct_no)
	 //String Credit_dtls = "select credit_id,acct_no,MASTER_PLAN_INSTANCE_NO,CREATE_USER,TO_CHAR(CREATE_DATE, 'YYYY-MM-DD'),AMOUNT,REASON_TEXT,REASON_CD,LEFT_TO_APPLY,TOTAL_APPLIED,CREDIT_TYPE from ariacore.all_credits where acct_no ="+acct_no
	 String Credit_dtls = "select a.credit_id as credit_no,a.acct_no as out_acct_no,a.MASTER_PLAN_INSTANCE_NO as out_master_plan_instance_no,TO_CHAR(a.CREATE_DATE, 'YYYY-MM-DD') as created_date,a.CREATE_USER as created_by,a.AMOUNT as amount,a.REASON_TEXT as reason_text,a.REASON_CD as reason_code,a.LEFT_TO_APPLY as unapplied_amount,a.TOTAL_APPLIED as applied_amount from ariacore.all_credits a where acct_no ="+acct_no
	 HashMap<String,String> dummy = new LinkedHashMap<String,String>()
	 ResultSet resultSet = db.executePlaQuery(Credit_dtls)
	 ResultSetMetaData md = resultSet.getMetaData();
	 int columns = md.getColumnCount();
	 if(columns>1)
	 {
	 //LinkedHashMap row = new LinkedHashMap(columns)
		   HashMap row = new HashMap(columns);
	   while (resultSet.next()){
		   for(int u=1; u<=columns; ++u){
			 row.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
		   }
	   }
	 return  row.sort();
	 }
	 else {if(resultSet.next()){return resultSet.getObject(1).toString()}                                                    }
}

 
  /** To get the coupon get_coupon_history_m
  * @param str ()
  * @return History - coupon history details
  */
   public LinkedHashMap md_get_coupon_history_m(String str){
	 logi.logInfo("********md_get_coupon_history_m Starts**************")
	 ConnectDB db = new ConnectDB()

	 LinkedHashMap line_item = new LinkedHashMap()
	 
	 LinkedHashMap line_item1 = new LinkedHashMap()

	 LinkedHashMap row_item = new LinkedHashMap()
	 String query1 = null
	 String query2 = null
	 int loopVal = 0;

	 String query_string=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/query_string[1]")
	 
	 query_string = query_string.replaceAll("acct_no","act.acct_no");

	 query_string = query_string.replaceAll("user_id","act.userid");

	 query_string = query_string.replaceAll("client_acct_id","act.client_acct_id");

	 query_string = query_string.replaceAll("master_plan_instance_id","pi.master_plan_instance_no");

	 query_string = query_string.replaceAll("client_master_plan_instance_id","pi.plan_instance_cdid");

	 query_string = query_string.replaceAll("coupon_cd","picm.coupon_cd");

	 query_string = query_string.replaceAll("coupon_create_date","picm.create_date");
	 
	 /*	if(query_string.contains("picm.cancel_date"))
	 {
		 logi.logInfo("Inside the picm.cancel_date if condition")
		 query_string = "TO_CHAR(picm.cancel_date, 'DD-MMM-YY')";
	 }*/
	 
	 query_string = query_string.replaceAll("coupon_cancel_date","picm.cancel_date");
	 

	 query_string = query_string.replaceAll("\"","");
	 query_string = query_string.replaceAll("= ","= '");
	 query_string = query_string.replaceAll("> ","> '");
	 query_string = query_string.replaceAll("< ","< '");
	 query_string = query_string.replaceAll(" and","' and");
	 query_string += "'";
	 ArrayList MPIS = new ArrayList();
	 
//		if(library.Constants.Constant.TESTCASEID == "TC_OBJ_005")
//		{
//			if(query_string.contains("picm.create_date"))
//			{
//				query_string = query_string.replaceAll("> ","= '");
//			}
//		}
	 if(query_string.contains("picm.create_date") || query_string.contains("picm.cancel_date"))
	 {
		 logi.logInfo("Enter into if condition")
		 //DateFormat df = new SimpleDateFormat("DD-MMM-YY HH.mm.ss.000000000");
		 //Date result =  df.parse(target);
		 String dquery_string = query_string.substring(0, query_string.length() - 1);
		 logi.logInfo("dquery_string value"+dquery_string);
		 String dquery_string1 = dquery_string.substring(dquery_string.indexOf("'")+1, dquery_string.length() - 0);
		 logi.logInfo("dquery_string1 value"+dquery_string1);
		 DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		 DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yy");
		 Date date = originalFormat.parse(dquery_string1);
		 String current_Date = targetFormat.format(date);
		 logi.logInfo("current_Date value"+current_Date);
		 query_string = query_string.replaceAll(dquery_string1,current_Date.toString());
		 logi.logInfo("query_string Final"+query_string);
	 }
	 
	 if(query_string.contains("picm.create_date") || query_string.contains("picm.cancel_date") || query_string.contains("pi.master_plan_instance_no") || query_string.contains("pi.plan_instance_cdid") || query_string.contains("picm.coupon_cd")||query_string.contains("act.userid")||query_string.contains("act.client_acct_id"))
	 {
	 ResultSet rs
	 query1 = "SELECT count(*) FROM ariacore.Plan_Inst_Coupon_Map picm JOIN ariacore.plan_instance pi ON pi.plan_instance_no      =picm.plan_instance_no AND pi.client_no  = picm.client_no JOIN ariacore.acct act ON pi.acct_no = act.acct_no WHERE act.client_no = "+library.Constants.Constant.client_no+" and "+query_string
	 int counts= Integer.parseInt(db.executeQueryP2(query1).toString())
	 if(counts>100)
	 counts=100;
	 for(int loop=1;loop<=counts;loop++)
	 {
	 query2 = "SELECT a.acct_no,a.user_id,a.client_acct_id,a.currency_cd,a.master_plan_instance_id,a.client_master_plan_instance_id,a.coupon_cd, a.coupon_create_date, a.coupon_cancel_date	from (SELECT ROW_NUMBER()OVER(ORDER BY picm.plan_instance_no) as seq,act.acct_no,act.userid as user_id,act.client_acct_id, act.currency_cd, picm.plan_instance_no AS master_plan_instance_id,pi.plan_instance_cdid      AS client_master_plan_instance_id, picm.coupon_cd, TO_CHAR (picm.create_date, 'YYYY-MM-DD HH24:MI:SS') AS coupon_create_date, TO_CHAR(picm.CANCEL_DATE, 'YYYY-MM-DD HH24:MI:SS') AS coupon_cancel_date FROM ariacore.Plan_Inst_Coupon_Map picm JOIN ariacore.plan_instance pi ON pi.plan_instance_no      =picm.plan_instance_no JOIN ariacore.acct act ON pi.acct_no = act.acct_no WHERE act.client_no = "+library.Constants.Constant.client_no+" and	"+query_string+")a where a.seq= "+loop
	 rs = db.executePlaQuery(query2)
	 ResultSetMetaData meta = rs.getMetaData()
	 int columns = meta.getColumnCount();
	 
	 if(!rs.isBeforeFirst()){
		 logi.logInfo "No rows for the pquery"
	  }else{
	 while(rs.next()){
		 for(int u=1; u<=columns; u++)
		 {
			 if(rs.getObject(u)!=null)
			 {
				 logi.logInfo("AFTER Execute the Query"+rs.getObject(u))
				 line_item.put(meta.getColumnName(u).toLowerCase(),rs.getObject(u))
				 logi.logInfo("AFTER Execute the line item"+line_item.toString())
				 
			 //MPIS.add(rs.getObject(u));
			 }
			 
		 }
	  }
	 
	 line_item=line_item.sort();
	 logi.logInfo("Line Itme after sort"+line_item.toString())
	 row_item.put("coupon_history_m_row["+(loop-1)+"]",line_item)
	 line_item = new LinkedHashMap()
	 logi.logInfo("Line Itme Upper"+row_item.toString())
	 }
	  loopVal = loop;
	 }
	
	 }
	 if(query_string.contains("act.userid")||query_string.contains("act.client_acct_id")||query_string.contains("act.acct_no"))
	 {
	 logi.logInfo "Enter into Else part"
	 ResultSet rs1
	 String query3 = "SELECT count(*) FROM ariacore.acct_coupons acpn JOIN ariacore.acct act ON acpn.acct_no = act.acct_no where act.client_no = "+library.Constants.Constant.client_no+" and "+query_string
	 int counts1= (Integer.parseInt(db.executeQueryP2(query3).toString()))
	 //Collections.sort(MPIS);
	 for(int loop1=1;loop1<=counts1;loop1++)
	 {
	 String query4 = "SELECT a.acct_no,a.user_id,a.client_acct_id,a.currency_cd,a.coupon_cd, a.coupon_create_date, a.coupon_cancel_date	from (SELECT ROW_NUMBER()OVER(ORDER BY act.acct_no) as seq,act.acct_no,act.userid as user_id,act.client_acct_id, act.currency_cd, acpn.coupon_cd, TO_CHAR (acpn.create_date, 'YYYY-MM-DD HH24:MI:SS') AS coupon_create_date, TO_CHAR(acpn.CANCEL_DATE, 'YYYY-MM-DD HH24:MI:SS') AS coupon_cancel_date FROM ariacore.acct_coupons acpn JOIN ariacore.acct act ON acpn.acct_no = act.acct_no WHERE act.client_no = "+library.Constants.Constant.client_no+" and "+query_string+")a where a.seq= "+loop1

	 rs1 = db.executePlaQuery(query4)
	 ResultSetMetaData meta1 = rs1.getMetaData()
	 int columns1 = meta1.getColumnCount();
	 
	 if(!rs1.isBeforeFirst()){
		 logi.logInfo "No rows for the pquery"
	  }else{
	 while(rs1.next()){
		 for(int u1=1; u1<=columns1; ++u1)
		 {
			 if(rs1.getObject(u1)!=null)
			 {
				 logi.logInfo("AFTER Execute the Query"+rs1.getObject(u1))
				 line_item1.put(meta1.getColumnName(u1).toLowerCase(),rs1.getObject(u1))
				 logi.logInfo("Line Itme Lower before"+line_item1.toString())
			 //MPIS.add(rs.getObject(u));
			 }
			 
		 }
	  }
		  
	 line_item1=line_item1.sort();
	 logi.logInfo "Line Itme loop1+loopVal" +(loop1+loopVal)
	 row_item.put("coupon_history_m_row["+((loop1+loopVal)-1)+"]",line_item1)
	 line_item1 = new LinkedHashMap()
	 logi.logInfo("Line Itme Lower"+row_item.toString())
	 }
	 }
	 }
	 logi.logInfo  "MPIs in the list "+row_item

	 row_item = row_item.sort()
	 return row_item.sort()
	 logi.logInfo("********md_get_coupon_history_m Ends**************")
 }
 
 public int md_DB_template_email_m(String str){
	 /***pa&1#a&24553***/
	 String s1,acct_no;
	 logi.logInfo "inside db template versions...."+str
	  ConnectDB db = new ConnectDB()
	  logi.logInfo "inside db template versions.2.0.."
	 String s = str.split("&")[0]
	 logi.logInfo "string split "+s
	 if(s.contains("pa")){
		  s1 = str.split("&")[1]
		  logi.logInfo "string split "+s1
		  acct_no = im.md_GET_ACCT_OR_INSTANCE_NO2_m(s1,null)
	 }else{
	 s1 = str.split("&")[1]
	 logi.logInfo "string split in else  "+s1
	  acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(s1,null)
	 }
	 String template_no = str.split("&")[2]
	 logi.logInfo "string split on template "+template_no
	 String s3 = "SELECT COUNT(*) AS COUNTS FROM ARIACORE.EMAIL_HIST WHERE ACCT_NO = "+acct_no+" AND TEMPLATE_NO = "+template_no
	 String query = db.executeQueryP2(s3)
	 int reval = Integer.parseInt(query)
	 logi.logInfo "return value "+reval
	 return reval
 }
 
 
 public  LinkedHashMap md_VerifyMPIAllTransactionLoop_m(String input)
 {
				 logi.logInfo("md_VerifyMPIAllTransactionLoop_m")
				 ConnectDB db = new ConnectDB()
				 int j=1,i=1
				 ArrayList<String> list = new ArrayList<String>()
				 LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
				 LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
				 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
				 logi.logInfo("client_no" +client_no )
				 def plan_instance
				 if(input.contains("#")){
					 plan_instance = im.md_GET_ACCT_OR_INSTANCE_NO2_m(input,null);
				 }else{
				 plan_instance = im.md_GET_ACCT_OR_INSTANCE_NO_m(input,null);
				 }
				 logi.logInfo("plan_instance" +plan_instance )
				 String query ="select max(invoice_no) as invoice_no from ariacore.gl_detail where master_plan_instance_no = " + plan_instance + " and client_no = "+client_no
				 String invoice_no=db.executeQueryP2(query)
				 String query3 = "Select acct_no from ariacore.plan_instance where plan_instance_no = "+ plan_instance +"and client_no=" +client_no
				 String acct_no =db.executeQueryP2(query3)
				 logi.logInfo("acct_no " +acct_no )
				 logi.logInfo("invoice_no" +invoice_no )
				 String query1 = "Select STATEMENT_NO from ariacore.acct_statement where invoice_no =" +invoice_no+ " and acct_no = "+ acct_no +"and client_no=" +client_no
				 String statement_no=db.executeQueryP2(query1)
				 logi.logInfo("statement_no" +statement_no )
				 //event no retrieve
				 String query4 = "select event_no from ariacore.acct_transaction where acct_no = "+acct_no+" and transaction_type in (4,-4,22,21,3,-3,15,-21,-10,-6,-2,2,6,10,12,13) and statement_no = "+statement_no
				 ResultSet rs=db.executePlaQuery(query4)
				 while(rs.next())
				 {
					 
					 list[i]=rs.getObject(1)
					 i++
				 }
				 int sizecount = list.size()
				 logi.logInfo("sizecount" + sizecount)
				 for(int f=1;f<=sizecount-1;f++)
				 {
				 //line item details query
					 logi.logInfo "event no "+list[f]
				 String query2="SELECT gld.MASTER_PLAN_INSTANCE_NO AS MPINo,CASE WHEN gld.PLAN_NO IS NOT NULL THEN pi.PLAN_INSTANCE_CDID ELSE NULL END  AS ClientMPIId,"+
								" at.EVENT_NO AS MPITxnNo,tt.DESCRIPTION AS MPITxnTypeDesc,TO_CHAR(TO_DATE(TO_CHAR(AT.CREATE_DATE,'MM/fmDD/YYYY'),'MM/DD/YYYY'),'FMMM/DD/YYYY') AS MPITxnDt,"+
								" CASE WHEN gld.SERVICE_NO IS NOT NULL THEN gld.SERVICE_NO ELSE 0 END AS ItemServiceNo,"+
								" gld.PLAN_NO AS ItemPlanNo, gld.PLAN_NAME AS ItemPlanName,CASE WHEN AT.TRANSACTION_TYPE IN (4,-4,7,10,-10,11,-21,15,22,23,24,12,13,3,-3,-6,-2,2,-12,-13) THEN 'Credit' ELSE aid.COMMENTS END  AS ItemSimpleLabel,"+
								" case when aid.service_no is null then '0.00' else (to_char(case when "+
								" (select sum(debit) as taxsum from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no) is null then aid.debit"+
								" else (aid.debit + (select sum(debit) as taxsum from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no))"+
								" end,'fm999990.90')) end  as itemgrosstotal,"+
								" TO_CHAR(TO_DATE(TO_CHAR(gld.START_DATE,'MM/fmDD/YYYY'),'MM/DD/YYYY'),'FMMM/DD/YYYY') AS ItemStartDate,"+
								" TO_CHAR(TO_DATE(TO_CHAR(gld.END_DATE,'MM/fmDD/YYYY'),'MM/DD/YYYY'),'FMMM/DD/YYYY')   AS ItemEndDate,"+
								" (TRUNC(gld.END_DATE)-TRUNC(gld.START_DATE))+1 AS ITEMPERIODDAYS, gld.TIER_BASE_PLAN_UNITS AS ItemBaseUnits,"+
								" CASE WHEN gld.MASTER_PLAN_INSTANCE_NO != gld.PLAN_INSTANCE_NO THEN gld.PLAN_INSTANCE_NO  ELSE NULL  END AS SPINo,"+
								" (select (DEBIT-OFFSET_DISC)as net_price from ariacore.gl_detail_after_credit"+
								"	where client_no = at.client_no and invoice_no = at.invoice_no and invoice_detail_no = at.source_no) as ITEMNETPRICE,"+
								"	(select (DEBIT-OFFSET_DISC)as net_price from ariacore.gl_detail_after_credit"+
								"	where client_no = at.client_no and invoice_no = at.invoice_no and invoice_detail_no = at.source_no) as ITEMMONTHLYAMOUNT,"+
								" CASE WHEN gld.MASTER_PLAN_INSTANCE_NO != gld.PLAN_INSTANCE_NO  THEN ARIACORE.eom_planmgr.get_plan_inst_id (gld.client_no,gld.PLAN_INSTANCE_NO,at.acct_no)"+
								" ELSE NULL  END AS ClientSPIId,gld.COMMENTS AS ItemComments,CASE WHEN AID.PLAN_NAME IS NULL AND AID.COMMENTS IS NULL THEN 'Credit' else aid.plan_name||' '||aid.comments end AS ItemPlanandComments,"+
								" gld.USAGE_UNITS AS ItemUnits,CASE WHEN AID.USAGE_RATE IS NULL THEN '0.00' ELSE TO_CHAR(aid.USAGE_RATE,'fm99999.90')END AS ItemRate,"+
								" CASE WHEN gld.RATE_SCHEDULE_NO IS NOT NULL AND GLD.SURCHARGE_RATE_SEQ_NO IS NULL THEN '1+' ELSE NULL END AS ItemTierLimits,"+
								" CASE WHEN gld.SURCHARGE_RATE_SEQ_NO IS NOT NULL THEN (select SURCHARGE_NO from ariacore.CLIENT_SURCHARGE WHERE ALT_SERVICE_NO_2_APPLY = GLD.SERVICE_NO AND CLIENT_NO = GLD.CLIENT_NO)"+
								" ELSE NULL END AS ItemSurchargeNo,case when (select sum(debit) as taxsum from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no) is null then '0.00'"+
								" else to_char((select sum(debit) as taxsum from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no),'fm99990.90')end as ItemTaxTotal,"+
								" gld.TIER_BASE_PLAN_UNITS AS ItemTierBasePlanUnits,"+
								" case when ( select count(taxed_seq_num) from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no  ) > 1"+
								" then '[Multiple Taxes]' ELSE (select tax_type from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no) end as ItemTaxType,"+
								" case when gld.seq_num not in (select taxed_seq_num from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and client_no = gld.client_no)then '0.00' else case when ( select count(taxed_seq_num) from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no  ) > 1"+
								" then '[Many]' ELSE to_char((select (tax_rate*100) as taxrate from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no),'fm99990.90') end end AS ItemTaxRate,CASE WHEN cs.TAXABLE_IND = 1 THEN '2' ELSE TO_CHAR(CS.TAXABLE_IND) END as ItemTaxTInd,"+
								" TO_CHAR(((gld.PRORATION_FACTOR*100)),'fm9999.90') AS ItemPercentProrationFactor,TO_CHAR(gld.PRORATION_FACTOR,'fm9.9000000000') AS ItemProrationFactor,"+
								" GLD.ORIG_CHARGE_DETAIL_NO AS ItemOrigChargeInvoiceNo, at.ORIG_EVENT_NO as ItemOrigChargeTxnNo,rs.SCHEDULE_NAME AS RateScheduleDescription,"+
								" rs.CLIENT_RATE_SCHEDULE_ID AS ClientRateScheduleId,GLD.ORIG_COUPON_CD AS ItemCouponCode,CP.COMMENTS AS ItemCouponDesc,CD.REASON_CD AS ItemCreditReasonCode,"+
								" CD.COMMENTS AS ItemCreditReasonText,"+
								" (select TO_CHAR(sum(DECODE (tt2.is_charge, 1, acct_txn.amount, -1 * acct_txn.amount)),'fm99990.90') as txnamount from ariacore.acct_transaction acct_txn left join ariacore.transaction_types tt2 on acct_txn.transaction_type = tt2.transaction_type where acct_no = at.acct_no and statement_no = at.statement_no) AS TotalBillingAmount,case when at.transaction_type in (3) then TO_CHAR((at.amount*-1),'fm99990.90') else TO_CHAR(at.amount,'fm99990.90')end AS MPITxnAmt"+
								" FROM ariacore.acct_transaction at"+
								" LEFT JOIN ariacore.gl_detail gld"+
								" ON at.client_no   = gld.client_no"+
								" AND at.invoice_no = gld.invoice_no"+
								" AND at.source_no  =gld.invoice_detail_no"+
								" LEFT OUTER JOIN ariacore.plan_instance pi"+
								" ON gld.MASTER_PLAN_INSTANCE_NO=pi.plan_instance_no"+
								" LEFT JOIN ariacore.all_invoice_details aid ON at.client_no   = aid.client_no AND at.invoice_no = aid.invoice_no and at.source_no  =aid.invoice_detail_no LEFT OUTER JOIN ariacore.transaction_types tt"+
								" ON at.TRANSACTION_TYPE =tt.TRANSACTION_TYPE"+
								" LEFT OUTER JOIN ariacore.client_service cs"+
								" on gld.service_no = cs.service_no"+
								" and gld.client_no = cs.client_no"+
								" LEFT OUTER JOIN ariacore.NEW_RATE_SCHEDULES rs"+
								" ON gld.RATE_SCHEDULE_NO =rs.schedule_no"+
								" AND gld.client_no       =rs.client_no"+
								" LEFT OUTER JOIN ARIACORE.COUPONS CP ON"+
								" CP.COUPON_CD = GLD.ORIG_COUPON_CD AND CP.CLIENT_NO = GLD.CLIENT_NO"+
								" LEFT OUTER JOIN ARIACORE.CREDITS CD ON"+
								" GLD.ORIG_CREDIT_ID = CD.CREDIT_ID AND CD.CLIENT_NO = GLD.CLIENT_NO"+
								" WHERE at.EVENT_NO ="+list[f]+
								" ORDER BY at.master_plan_instance_no,at.event_no"
				 ResultSet rs4=db.executePlaQuery(query2)
				 ResultSetMetaData md4 = rs4.getMetaData();
				 int columns4 = md4.getColumnCount();
				 while (rs4.next())
				 {
									   logi.logInfo "inside while"
								 for(int l=1; l<=columns4; l++)
								 {
															  // logi.logInfo "inside for"
												 if((rs4.getObject(l))!=null)
												 {
																 
																				 row.put((md4.getColumnName(l)),(rs4.getObject(l)).toString().trim());
																 
												 
												 }
												 /*if(rs4.getObject('ITEMSIMPLELABEL')=="" || rs4.getObject('ITEMSIMPLELABEL')==null)
																				 
															  {
																											//logi.logInfo "inside ITEMPLANANDCOMMENTS "
																			  row.put("ITEMPLANANDCOMMENTS","Credit")
																			  row.put("ITEMSIMPLELABEL","Credit")
																															  logi.logInfo "inside ITEMPLANANDCOMMENTS 22222"
																			  
															  }
																									  if(rs4.getObject('ITEMRATE')==null)
															  {
																											//logi.logInfo "inside ITEMPLANANDCOMMENTS 1"
																			  row.put("ITEMRATE","0.00")
															  }
																									  if(rs4.getObject('ITEMGROSSTOTAL')==null)
																									  {
																											//logi.logInfo "inside ITEMPLANANDCOMMENTS 1"
																															  row.put("ITEMGROSSTOTAL","0.00")
																									  }
															 if(rs4.getObject('ITEMTAXTOTAL')==null)
															  {
																											//logi.logInfo "inside ITEMPLANANDCOMMENTS 2"
																			  row.put("ITEMTAXRATE","0.00")
																			  row.put("ITEMTAXTOTAL","0.00")
															  }*/
															  
															  /*if(rs4.getObject('ITEMTIERLIMITS')!=null)
															  {
																  logi.logInfo "inside ITEMPLANANDCOMMENTS 3"
																  String senior_acct = (String)row.get("ITEMTIERLIMITS")
																  logi.logInfo "inside senior_acct 2"+senior_acct
																  row.put("ITEMTIERLIMITS",senior_acct+'+');
															  }*/
															  
								 }
								/* if(rs4.getObject('ITEMTIERLIMITS')!=null)
								 {
									 //logi.logInfo "inside ITEMPLANANDCOMMENTS 3"
									 String senior_acct = (String)row.get("ITEMTIERLIMITS")
									 logi.logInfo "inside senior_acct 2"+senior_acct
									 row.put("ITEMTIERLIMITS",senior_acct+'+');
								 }
								 if((rs4.getObject('TRANSACTION_TYPE')== -21)||(rs4.getObject('TRANSACTION_TYPE')== 3))
								 {
									 logi.logInfo "inside ITEMPLANANDCOMMENTS 2"
									 
									 
									 String senior_acct1 = (String)row.get("MPITXNAMT")
									 logi.logInfo "inside senior_acct 2"+senior_acct1
									 row.put("MPITXNAMT",'-'+senior_acct1);
									 
								 }*/
									 
								 
								 outer.put(list[f].toString().trim(),row.sort())
								 row = new LinkedHashMap<String, String>();
								 j++
				 
				 }
				 }
				 return outer.sort()
 }
 
 
 public def md_getResult_db_m (String input){
	 
	 logi.logInfo "into md_getResult_db_m"+input

	 String[] QueryStrings=input.split("\\*")
	 logi.logInfo "QueryStrings"+QueryStrings.size().toString()
	 String Query=getExpValue(QueryStrings[0]);
	 logi.logInfo "into md_getResult_db_m"+Query
	 String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	 Query=Query.replaceAll('TD_ClientNo',clientNo)
	 Object[] param = new Object[2];
	 ConnectDB db=new ConnectDB();

	 
	 def resultSet
	 if(QueryStrings.size()==2)	 {
	  param[0] = QueryStrings[1]
		resultSet= db.executeQueryP2( String.format(Query,param[0]))
	 
	
   }
	 else if (QueryStrings.size()==3){
		 param[1] = QueryStrings[2]
		 resultSet= db.executeQueryP2( String.format(Query,param[0],param[1]))
	 }
	return resultSet
 }
 public def md_getTransactionIdFromDB(String str)
 {
	 String id=im.md_GET_PID_NO_M(str,null);
	 
	 return id
 }
 public String md_IsStatementGenerated(String input)
 {
	 logi.logInfo "Calling md_IsStatementGenerated "+input
	 boolean flg=false
	 String inv=im.md_get_invoice_no_m(input,null);

	String acct_no=im.md_GET_ACCT_NO_m(input.split (",")[0],null).toString()
	 String query="select statement_no from ariacore.acct_statement where invoice_no= %s and acct_no=%s"
	 ConnectDB db=new ConnectDB();
	 String val=null
	 val=db.executeQueryP2(String.format(query,inv,acct_no))
	 if(val!=null)
	 flg=true

	 return flg.toString().toUpperCase()
 }
 
   def md_verify_xml_node_get_aria_xml_statement_m(String param)
			  {
				  logi.logInfo("********md_verify_xml_node_get_aria_xml_statement_m Starts**************")
				  //String accountNo=Constant.mycontext.expand('${Properties#AccountNo}')
				  String out_statement_temp = getValueFromResponse("get_aria_xml_statement_m", "//ns1:get_aria_xml_statement_mResponse[1]/xml_statement_content[1]")
				  logi.logInfo("out_statement_temp : " + out_statement_temp)
				  out_statement_temp=out_statement_temp.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>","")
				  String result =xmlparser(param,out_statement_temp)
				  return result
			  }
			  
			  
			  
	   public String xmlparser(String input,String xml)
			  {   try{
				  logi.logInfo "into the xmlparser"+input
				  String xmlRecords = xml
				  LinkedList<String> temp2 = new LinkedList<String>();
				  HashMap<String, String> hm = new HashMap<String, String>();
				  String temp = null;
				  DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				  InputSource is = new InputSource();
				  is.setCharacterStream(new StringReader(xmlRecords));
				  Document doc = db.parse(is);
				  String true_input=input;
				  String  [] inp=input.split("\\|")
				  int count=0;
				  for (int i = 0; i < input.length(); i++)
				  {
						if (input.charAt(i) == '|')
						{
							   count++;
						}
				  }
				  logi.logInfo 'got the count '+count
				  //store the target node as input value
				  input=	inp[count]
				  NodeList nodes = doc.getElementsByTagName(doc.getDocumentElement().getNodeName());
				  Element element = (Element) nodes.item((0));
				  if(count>0)
				  {
					  for(int t=0;t<count;t++)
					  {
						  logi.logInfo "Setting boundary "+ true_input.split('\\|')[t]
						  nodes = element.getElementsByTagName(true_input.split('\\|')[t].split(',')[0]);
						  if(nodes.getLength()<Integer.parseInt(true_input.split('\\|')[t].split(',')[1])) return 'NO NODE'
						  else element = (Element) nodes.item(((Integer.parseInt(true_input.split('\\|')[t].split(',')[1]))-1));
					  }
				  }
				  NodeList name = element.getElementsByTagName(input.split(",")[0]);
				  int index=Integer.parseInt(input.split(",")[1])
				  index-=1;
				  if(name.getLength()<=index)return 'NO NODE'
				  else {Element line = (Element) name.item(index);
				  temp=getCharacterDataFromElement2(line);
				  logi.logInfo 'value got '+temp;
				  return temp;}
				  }catch(Exception e){return 'Exception'}
															
			  }
 
 
 public  LinkedHashMap md_getsomeorder_m(String param) throws Exception
 {
	 logi.logInfo "into md_getsomeorder_m"
	 String place = param.split("#")[1]
	 param = param.split("#")[0]
	 String input = param.split(",")[0]
	 String orderby = param.split(",")[1]
	 String xmlRecords = library.Constants.Constant.RESPONSE
	 LinkedHashMap<Integer,Integer> Total_nodes = new  LinkedHashMap<Integer,Integer>();
	 LinkedHashMap<String,LinkedHashMap> outer = new  LinkedHashMap<String,LinkedHashMap>();
	 LinkedHashMap<String,LinkedHashMap> Result = new  LinkedHashMap<String,LinkedHashMap>();
	 DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	 InputSource is = new InputSource();
	 is.setCharacterStream(new StringReader(xmlRecords));
	 Document doc = db.parse(is);
	 NodeList Totalnodes = doc.getElementsByTagName(input);
	 int Total_records = Totalnodes.getLength();
	 //System.out.println(Total_records);
	 for(int i=0;i<Total_records;i++)
	 {
		 Node parent = Totalnodes.item(i);
		 Element element = (Element) Totalnodes.item(i);
		 NodeList child = parent.getChildNodes();
		 Element elementary= (Element) child.item(1);
		  NodeList childnodes = element.getElementsByTagName(elementary.getTagName());
		  Total_nodes.put(i+1,childnodes.getLength());
		  childnodename =  elementary.getTagName();
	 }
	 
	 //System.out.println(Total_nodes);
	 //System.out.println(childnodename);
	 int Initial = 0;
	 int Total = 0;
	 for(int i=1;i<=Total_records;i++)
	 {
		 Total += Total_nodes.get(i);
		 //System.out.println(Initial+":"+Total);
		 int count =1;
		 for(int k=Initial;k<Total;k++)
		 {
			 LinkedHashMap inner = md_getspecificchildnodesorder_m(childnodename,place,k);
			 inner=inner.sort();
			 if(count==1)
			 outer.put(childnodename.replaceAll("ns1:",""),inner);
			 else outer.put(childnodename.replaceAll("ns1:","")+"["+count+"]",inner);
			 count++;
			 
		 }
		 
		 outer = find_order(outer,orderby);
		 outer = outer.sort();
		 if(i==1)
		 Result.put(input.replaceAll("ns1:",""),outer);
		 else
		 Result.put(input.replaceAll("ns1:","")+"["+i+"]",outer);
		 outer = new  LinkedHashMap<String,LinkedHashMap>();
		 Initial +=Total_nodes.get(i);
	 }
	 Result=Result.sort();
	 return Result;
	
 }
 
 public   LinkedHashMap md_getspecificchildnodesorder_m(String parentnode,String place,int index) {
	 try{
		 String xmlRecords = library.Constants.Constant.RESPONSE
							 pnode=parentnode;
							 /*pnode=parentnode.split("\\,")[0];
							 String [] rname=parentnode.split(",");
							 parentnode=parentnode.split(",")[0];*/
							// String place = "line_type,rate_schedule_tier_no,credit_reason_code_description"
							 String [] rname=place.split(",");
						 //	int index=Integer.parseInt(parentnode.split("\\,")[1]);
							 //index-=1;
							 //logi.logInfo ""+index;
							 LinkedList<String> childnodes = new LinkedList<String>();
							 ArrayList<ArrayList> all = new ArrayList<ArrayList>();
							 LinkedHashMap inner = new LinkedHashMap();

							 //for getting the values of child nodes

							 childnodes=getnode(xmlRecords,pnode,index);
							 
							 for (String number : childnodes)

							 {
								 if(getval2(xmlRecords,number,index)!=null)
											 
												 {
													 if(!Arrays.asList(rname).contains(number.replace("ns1:","")))
												 inner.put(number.replace("ns1:",""),getval2(xmlRecords,number,index));
													 
												 
												 }
											 
							 }
						 
							 return inner;
							 }
							 catch(Exception e)
									 {
										 return null;
									 }
							 }
 
	public  LinkedHashMap find_order (LinkedHashMap<String,LinkedHashMap> outer, String orderBy)
	{
	 List<String> InKeys = new ArrayList<>(outer.keySet());
	 LinkedHashMap<String,LinkedHashMap> outers =  new LinkedHashMap<String,LinkedHashMap>();
	 LinkedHashMap<String,String> out =  new LinkedHashMap<String,String>();
	 LinkedHashMap<Integer,LinkedHashMap> incomming =  new LinkedHashMap<Integer,LinkedHashMap>();
	 LinkedHashMap<Integer,Integer> income =  new LinkedHashMap<Integer,Integer>();
	 
	 Set<String> keys = outer.keySet();
	 int i=1;
	 for(String k:keys){
		 incomming.put(i,outer.get(k));
		 out=outer.get(k);
		 income.put(i,Integer.parseInt(out.get(orderBy)));
		 i++;
	 }
	 //System.out.println(income);
	 List<Integer> Keys = new ArrayList<>(income.keySet());
	 List<Integer> venum = new ArrayList<>();
	 List<Integer> original = new ArrayList<>(income.values());
	 List<Integer> sorted = new ArrayList<>(income.values());
	 Collections.sort(sorted);
	 
	 //System.out.println(original);
	 //System.out.println(sorted);
	 
	 for(int s:sorted)
	 {
		 for(int key:Keys)
		 {
			 if(s==income.get(key))venum.add(key);
		 }
		 
	 }
	 
	 //System.out.println(venum);
	 
	 int z=0;
	 for(String a:InKeys)
	 {
		 outers.put(a,incomming.get(venum.get(z)));
		 z++;
	 }
	 return outers;
	 }
	
	/**
	 * method md_GetAllNestedChildNodesasList_m
	 * @param any parent node
	 * @return nested hash map
	 *
	 */
	def md_GetAllNestedChildNodesasList2_m(String parentnode) throws Exception{
		LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
		LinkedHashMap<String, HashMap> outer1 = new LinkedHashMap<String, HashMap>();
		logi.logInfo "the parent node :"+parentnode
		int counts=Integer.parseInt(md_get_node_count_m(parentnode.split(",")[0]));
		logi.logInfo "the count is here :"+counts
		for(int i=0;i<counts;i++)
		{
			outer=(md_getspecificchildnodesRname_m(parentnode,(i+1)))
			outer=outer.sort()
			outer1.put(parentnode.split(",")[0].replace("ns1:","")+"["+(i)+"]",outer)
			outer = new LinkedHashMap<String, String>();
		}
		outer1=outer1.sort()
		return outer1
	}
	
	public LinkedHashMap md_GetAllNestedChildNodesOrderRname_m(String parentnode)
	{
		logi.logInfo "Enter into md_GetAllNestedChildNodesOrderRname_m method"
		LinkedHashMap<String, String> listing = new LinkedHashMap<String, String>();
		LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
		LinkedHashMap<String, HashMap> outer1 = new LinkedHashMap<String, HashMap>();

		listing=md_getSpecificnodeAsList_m(parentnode.split(",")[0])

		int counts=listing.size()
		logi.logInfo "the count is here :"+counts
		for(int i=0;i<counts;i++)
		{
			logi.logInfo "Print i value :"+i
			outer=(md_getspecificchildnodesRname_m(parentnode.split(",")[0]+","+parentnode.split("#")[1],(i+1)))
						
			outer1.put(parentnode.split(",")[0].replace("ns1:","")+"["+(i)+"]",outer.sort())

			outer = new LinkedHashMap<String, String>();
		}
		outer1 = find_order(outer1,parentnode.split(",")[1].split("#")[0]);
		outer1=outer1.sort()
		return outer1
	}
	
	public String md_Past_due_amt_m(String testcaseID)
	{
	
		   logi.logInfo("Intoo md_Past_due_amt_m. . . ")
		   ConnectDB db = new ConnectDB()
		   InputMethods im=new InputMethods()
		   String result=null
		   logi.logInfo("IntoPOSASAA ")
	String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(testcaseID,null)
		   String db_xml_stmt_q="select nvl(amount,0) from ariacore.acct_transaction where acct_no = "+acct_no+"and transaction_type = 3"
		   String db_xml_stmt="-"+db.executeQueryP2(db_xml_stmt_q)
		   logi.logInfo("222222222")
		   return db_xml_stmt.trim();
	}
	LinkedHashMap md_coupon_disc_rule_m(String acct_hiereachy1)
	{
		
		logi.logInfo("intooo md_coupon_test_m ")
		String acct_hiereachy=acct_hiereachy1.split("#")[0]
		String branch=acct_hiereachy1.split("#")[1]
		
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	
				LinkedHashMap tempar = new LinkedHashMap()
				//LinkedHashMap acct_details_hm_temp = new LinkedHashMap()
	
	
				int value=1
	
				ConnectDB dbb = new ConnectDB()
				String result=null
				
				String count_query=null
				String acct_no=null
				String mpi=null
				
				acct_no = getValueFromRequest('get_acct_coupon_details_m',"//acct_no[1]")
				logi.logInfo("acct_noooo "+acct_no)
				
				if(branch.equalsIgnoreCase("a"))
				{
						   // acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hiereachy,null)
						
					   
								   
					count_query="SELECT COUPON_CD FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" and cancel_date is null and client_no="+client_no
				}
				
				
				else if(branch.equalsIgnoreCase("m"))
				{
					mpi =getValueFromRequest('get_acct_coupon_details_m',"//*/*:master_plan_instance_no[1]")
					
					logi.logInfo("mpiiiiiiiii "+mpi)
					count_query="SELECT COUPON_CD FROM ariacore.PLAN_INST_COUPON_MAP where PLAN_INSTANCE_NO="+mpi
					
				}
				
						   
						   
	
				
				 ResultSet coupon_codes=dbb.executePlaQuery(count_query)
				LinkedList al1= new LinkedList()
				while (coupon_codes.next())
				{
					  logi.logInfo("inside while")
					  ResultSetMetaData md = coupon_codes.getMetaData()
					  int columns = md.getColumnCount()
					  logi.logInfo("columns for"+columns)
					  // Putting values into HashMap
	
					  for(int i=1; i<=columns; ++i)
	
					  {
						  
						  logi.logInfo("adding into list :"+ coupon_codes.getObject(i).toString())
						  al1.add(coupon_codes.getObject(i).toString())
						  
					  }
				}
				
				logi.logInfo("AL1 "+al1)
	
	
			  
	
	
					  LinkedList dr1 =  new LinkedList()
					  LinkedList l1 =  new LinkedList()
					  for(int k=0;k<al1.size();k++)
					  {
					  String disc_tt_query="SELECT count(RULE_NO)  FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD='"+al1.get(k)+"'"
					  //String credit_tt_query="SELECT count(RECURRING_CREDIT_TEMPLATE_NO) FROM ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE COUPON_CD =(SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by coupon_cd ) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" and cancel_date is null  )A)WHERE A.SEQ="+begin+")"
					  String disc_tt=dbb.executeQueryP2(disc_tt_query)
					  logi.logInfo(disc_tt+"credit_tt")
					  if(disc_tt.toInteger()>=1)
					  dr1.add(al1.get(k))
					  
					  }
					  logi.logInfo("FINnAL RULE"+dr1)
	
					  for(int k1=0;k1<dr1.size();k1++)
					 { //Its discount rules
						 LinkedHashMap coupon_frm_mpi2 = new LinkedHashMap()
							logi.logInfo("<----------------- Its discount ------------------------->")
	
							String gen_coupon_query="SELECT COUPON_CD AS OUT_COUPON_CD, APPLICATIONS_ALLOWED-1 AS COUPON_SCOPE,COMMENTS AS DESCRIPTION,TOTAL_USES,CURRENCY_CD, RECUR_DISCOUNT_FLAT_AMT,ONE_TIME_DISCOUNT_FLAT_AMT, RECUR_DISCOUNT_PCT, ONE_TIME_DISCOUNT_PCT,1 AS STATUS_IND FROM ARIACORE.COUPONS WHERE COUPON_CD='"+dr1.get(k1)+"' AND CLIENT_NO="+client_no
							ResultSet rs_disc_gen=dbb.executePlaQuery(gen_coupon_query)
	
							while (rs_disc_gen.next())
							{
							  
								  logi.logInfo("inside while")
								  ResultSetMetaData md = rs_disc_gen.getMetaData()
								  logi.logInfo("inside while"+rs_disc_gen.getMetaData())
								  int columns = md.getColumnCount()
								  logi.logInfo("columns for"+columns)
								  // Putting values into HashMap
	
								  int freq=0
								  freq = Collections.frequency(l1,"out_acct_no")
								  if(freq>1)
								  
								  coupon_frm_mpi2.put("out_acct_no["+freq+"]",acct_no)
								  else
								  coupon_frm_mpi2.put("out_acct_no",acct_no)
								  l1.add("out_acct_no")
								  
								  
								  if(branch.equalsIgnoreCase("m"))
								  {
									 
								 
								 freq = Collections.frequency(l1,"out_client_master_plan_instance_id")
								  if(freq>1)
								  
								  coupon_frm_mpi2.put("out_client_master_plan_instance_id["+freq+"]",mpi)
								  else
								  coupon_frm_mpi2.put("out_client_master_plan_instance_id",mpi)
								  l1.add("out_client_master_plan_instance_id")
								  
								  
								  freq = Collections.frequency(l1,"out_master_plan_instance_no")
								  if(freq>1)
								  
								  coupon_frm_mpi2.put("out_master_plan_instance_no["+freq+"]",mpi)
								  else
								  coupon_frm_mpi2.put("out_master_plan_instance_no",mpi)
								  l1.add("out_master_plan_instance_no")
								  }
	
								  //coupon_frm_mpi.put("coupon_scope","0")
								  for(int i=1; i<=columns; ++i)
	
								  {
	
										logi.logInfo("Inside for")
									  //  if(!(md.getColumnName(i).toString().equalsIgnoreCase("null")))
										if(!(rs_disc_gen.getObject(i).toString().equalsIgnoreCase("null")))
										{
											  logi.logInfo("insie PUTTTT")
											  l1.add(md.getColumnName(i).toLowerCase())
											   freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	
											  logi.logInfo("freqq of :+"+md.getColumnName
	
														  (i).toLowerCase()+" is :"+freq)
	
											  if(freq>1)
											  {
	
													coupon_frm_mpi2.put(md.getColumnName
	
																(i).toLowerCase()+"["+ freq +"]",rs_disc_gen.getObject(i))
											  }
	
											  coupon_frm_mpi2.put(md.getColumnName(i).toLowerCase(),rs_disc_gen.getObject(i))
	
										}
										else
											  logi.logInfo("insie else")
								  }
							}
	
							logi.logInfo("loop hash"+coupon_frm_mpi2)
	
	
							logi.logInfo("Inside ")
							//keys1=coupon_frm_mpi.keySet()
	
							//String disc_rule_query="SELECT RULE_NO  FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD = (SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by coupon_cd desc) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+")A)WHERE A.SEQ="+begin+")"
							String disc_rule_count_query="SELECT count(RULE_NO)  FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD ='"+dr1.get(k1)+"' and client_no = "+client_no
							String disc_rule_count=dbb.executeQueryP2(disc_rule_count_query)
							for(int r=1;r<=disc_rule_count.toInteger();r++)
							{
								  String disc_rule_query="SELECT A.RULE_NO AS RULE_NO FROM((SELECT RULE_NO,ROW_NUMBER() OVER (ORDER BY RULE_NO) AS SEQ FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD = '"+dr1.get(k1)+"')A)"
								  String disc_rule=dbb.executeQueryP2(disc_rule_query)
	
	
	
								  //commented for max applications
								  //String Query="SELECT RULE_NO,CLIENT_RULE_ID,LABEL AS RULE_NAME,SCOPE_NO, DESCRIPTION,FLAT_PERCENT_IND,CURRENCY_CD,AMOUNT,INLINE_OFFSET_IND,DURATION_TYPE_IND,MAX_APPLICATIONS_PER_ACCT,EXT_DESCRIPTION,ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE RULE_NO="+disc_rule
								  String Query="SELECT RULE_NO,CLIENT_RULE_ID,LABEL AS RULE_NAME,SCOPE_NO, DESCRIPTION ,FLAT_PERCENT_IND,CURRENCY_CD,AMOUNT,INLINE_OFFSET_IND,DURATION_TYPE_IND,EXT_DESCRIPTION,ALT_SERVICE_NO_2_APPLY,MAX_APPLICABLE_MONTHS FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE RULE_NO="+disc_rule+" and client_no="+client_no
								  ResultSet rs1= dbb.executePlaQuery(Query)
								  ResultSetMetaData md = rs1.getMetaData()
								  int columns = md.getColumnCount()
								  // Putting values into HashMap
								  
								  int freq = Collections.frequency(l1,"out_acct_no")
								  if(freq>1)
								  
								  coupon_frm_mpi2.put("out_acct_no["+freq+"]",acct_no)
								  else
								  coupon_frm_mpi2.put("out_acct_no",acct_no)
								  int temp_count=1
	
	
								  while (rs1.next())
								  {
	
	
										for(int i=1; i<=columns; ++i)
										{
	
										  //	if(!(md.getColumnName(i).toString().equalsIgnoreCase("null")))
											if(!(rs1.getObject(i).toString().equalsIgnoreCase("null")))
											  {
													l1.add(md.getColumnName(i).toLowerCase())
	
													 freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	
													logi.logInfo("frewww of :+"+md.getColumnName(i).toLowerCase()+" is :"+freq)
	
													if(freq>1)
													{
	
														  coupon_frm_mpi2.put(md.getColumnName(i).toLowerCase()+"["+ freq +"]",rs1.getObject(i))
													}
	
													else
														  coupon_frm_mpi2.put(md.getColumnName(i).toLowerCase(),rs1.getObject(i))
	
											  }
	
	
	
											  //else
											  //coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs1.getObject(i))
	
	
										}
								  String rule_no=dbb.executeQueryP2("select rule_no from ARIACORE.COUPON_DISCOUNT_RULE_MAP  where COUPON_CD='"+dr1.get(k1)+"'")
								  String bundle_no=dbb.executeQueryP2("select bundle_no from ARIACORE.CLIENT_DR_BUNDLE_MEMBERS where rule_no="+rule_no)
								  
								  if(bundle_no!=null)
								  {
									  coupon_frm_mpi2.put("bundle_no",bundle_no)
								  }
										
										}
	
	
							}
							coupon_frm_mpi2=coupon_frm_mpi2.sort()
							tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi2)
							++value
							 }
					  
	
	
	
			 
	
				tempar = tempar.sort()
	
				return tempar
	
	
	}
	
	
	
	
	LinkedHashMap md_coupon_credit_template_m(String acct_hiereachy1)
	{
		
		logi.logInfo("intooo md_coupon_test_m ")
		String acct_hiereachy=acct_hiereachy1.split("#")[0]
		String branch=acct_hiereachy1.split("#")[1]
	 
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	
				LinkedHashMap tempar = new LinkedHashMap()
				//LinkedHashMap acct_details_hm_temp = new LinkedHashMap()
	
	
				int value=1
	
				ConnectDB dbb = new ConnectDB()
				String result=null
				
				
				String count_query=null
				String acct_no=null
				String mpi=null
				
				acct_no = getValueFromRequest('get_acct_coupon_details_m',"//acct_no[1]")
				logi.logInfo("acct_noooo "+acct_no)
			 
				
				if(branch.equalsIgnoreCase("a"))
	 {
				// acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hiereachy,null)
			 
			
						
		 count_query="SELECT COUPON_CD FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" and cancel_date is null and client_no="+client_no
	 }
	 
	 
	 else if(branch.equalsIgnoreCase("m"))
	 {
		 mpi =getValueFromRequest('get_acct_coupon_details_m',"//*/*:master_plan_instance_no[1]")
		 
		 logi.logInfo("mpiiiiiiiii "+mpi)
		 count_query="SELECT COUPON_CD FROM ariacore.PLAN_INST_COUPON_MAP where PLAN_INSTANCE_NO="+mpi
		 
	 }
	 
				
				
	
				
				 ResultSet coupon_codes=dbb.executePlaQuery(count_query)
				LinkedList al1= new LinkedList()
				while (coupon_codes.next())
				{
					  logi.logInfo("inside while")
					  ResultSetMetaData md = coupon_codes.getMetaData()
					  int columns = md.getColumnCount()
					  logi.logInfo("columns for"+columns)
					  // Putting values into HashMap
	
					  for(int i=1; i<=columns; ++i)
	
					  {
						  
						  logi.logInfo("adding into list :"+ coupon_codes.getObject(i).toString())
						  al1.add(coupon_codes.getObject(i).toString())
						  
					  }
				}
				
				logi.logInfo("AL1 "+al1)
	
	
				for(int begin=1;begin<=count.toInteger();begin++)
				{
					  LinkedHashMap coupon_frm_mpi = new LinkedHashMap()
					  LinkedList l1 =  new LinkedList()
					  LinkedList cc1 =  new LinkedList()
					  logi.logInfo("______ NEW HASHMAP  Created     ____  "+begin)
	
					  //LinkedHashMap coupon_frm_mpi = new LinkedHashMap()
					  //LinkedHashMap coupon_frm_mpi= new LinkedHashMap()
	
					  //LinkedHashMap acct_details_hm_temp = new LinkedHashMap()
	
	//5-26						logi.logInfo("Creating new HM "+begin+" - - - -- - -  "+coupon_frm_mpi)
					  logi.logInfo(begin+"VAlue fo beginnnnnnn")
	
	
					  // Checking if its CREDIT TEMPLATE
					  for(int i=0;i<al1.size();i++)
					  {
					  String credit_tt_query="SELECT count(RECURRING_CREDIT_TEMPLATE_NO) FROM ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE COUPON_CD ='"+al1.get(i)+"'"
					  //String credit_tt_query="SELECT count(RECURRING_CREDIT_TEMPLATE_NO) FROM ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE COUPON_CD =(SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by coupon_cd ) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" and cancel_date is null  )A)WHERE A.SEQ="+begin+")"
					  String credit_tt=dbb.executeQueryP2(credit_tt_query)
					  logi.logInfo(credit_tt+"credit_tt")
					  if(credit_tt.toInteger()>=1)
					  cc1.add(al1.get(i))
					  
					  }
					  logi.logInfo("FINnAL CREDI"+cc1)
	
					  for(int j=0;j<cc1.size();j++)
					  { //Its credit template
							//String credit_coupon_cd_query="SELECT A.coupon_cd FROM((SELECT coupon_cd,row_number() over(order by coupon_cd ) AS seq FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" order by coupon_cd desc)A)WHERE A.SEQ="+begin
							logi.logInfo("<-----------------  Its credit template  ---------------------------->")
	
							String gen_coupon_query="SELECT COUPON_CD AS OUT_COUPON_CD, APPLICATIONS_ALLOWED-1 AS COUPON_SCOPE,COMMENTS AS DESCRIPTION,TOTAL_USES,CURRENCY_CD, RECUR_DISCOUNT_FLAT_AMT,ONE_TIME_DISCOUNT_FLAT_AMT, RECUR_DISCOUNT_PCT, ONE_TIME_DISCOUNT_PCT,1 AS STATUS_IND,TO_CHAR(START_DATE, 'DD-MON-YY HH.MI.SSxFF PM') AS START_DATE FROM ARIACORE.COUPONS WHERE COUPON_CD='"+cc1.get(j)+"' AND CLIENT_NO="+client_no
							ResultSet rs_credit_gen=dbb.executePlaQuery(gen_coupon_query)
	
							while (rs_credit_gen.next())
							{
								  logi.logInfo("inside while")
								  ResultSetMetaData md = rs_credit_gen.getMetaData()
								  logi.logInfo("inside while"+rs_credit_gen.getMetaData())
								  int columns = md.getColumnCount()
								  logi.logInfo("columns for"+columns)
								  // Putting values into HashMap
	
	
								  coupon_frm_mpi.put("out_acct_no",acct_no)
								  l1.add("out_acct_no")
								  int freq=0
								  if(branch.equalsIgnoreCase("m"))
								  {
									 
								 
								 freq = Collections.frequency(l1,"out_client_master_plan_instance_id")
								  if(freq>1)
								  
								  coupon_frm_mpi.put("out_client_master_plan_instance_id["+freq+"]",mpi)
								  else
								  coupon_frm_mpi.put("out_client_master_plan_instance_id",mpi)
								  l1.add("out_client_master_plan_instance_id")
								  
								  
								  freq = Collections.frequency(l1,"out_master_plan_instance_no")
								  if(freq>1)
								  
								  coupon_frm_mpi.put("out_master_plan_instance_no["+freq+"]",mpi)
								  else
								  coupon_frm_mpi.put("out_master_plan_instance_no",mpi)
								  l1.add("out_master_plan_instance_no")
								  }
								  //coupon_frm_mpi.put("coupon_scope","0")
								  //l1.coupon_frm_mpi("out_acct_no")
								  for(int i=1; i<=columns; ++i)
	
								  {
	
										logi.logInfo("inside for")
									  //  if(!(md.getColumnName(i).toString().equalsIgnoreCase("null")))
										if(!(rs_credit_gen.getObject(i).toString().equalsIgnoreCase("null")))
	
	
										{logi.logInfo("insie PUTTTT")
											  l1.add(md.getColumnName(i).toLowerCase())
											   freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	
											  logi.logInfo("frew of :+"+md.getColumnName
	
														  (i).toLowerCase()+" is :"+freq)
	
											  if(freq>1)
											  {
	
													coupon_frm_mpi.put(md.getColumnName
	
																(i).toLowerCase()+"["+ freq +"]",rs_credit_gen.getObject(i))
											  }
											  else
	
													coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs_credit_gen.getObject(i))
	
										}
										else
											  logi.logInfo("insie else")
								  }
							}
	
	
	
							 for(int j1=0;j1<cc1.size();j1++)
							{
	
								  String credit_temp_query="SELECT A.RECURRING_CREDIT_TEMPLATE_NO AS RECURRING_CREDIT_TEMPLATE_NO FROM ((SELECT RECURRING_CREDIT_TEMPLATE_NO,ROW_NUMBER() OVER(ORDER BY RECURRING_CREDIT_TEMPLATE_NO) AS SEQ FROM ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE COUPON_CD='"+cc1.get(j1)+"')A)"
								  String credit_temp_no=dbb.executeQueryP2(credit_temp_query)
								  String Query="SELECT RECURRING_CREDIT_TEMPLATE_NO AS CREDIT_TEMPLATE_NO,LABEL AS TEMPLATE_NAME,FLAT_AMOUNT,CURRENCY_CD,NUM_CREDITS_REQUIRED,CREDIT_INTERVAL_MONTHS FROM ARIACORE.RECURRING_CREDIT_TEMPLATES WHERE RECURRING_CREDIT_TEMPLATE_NO="+credit_temp_no+"and client_no="+client_no
								  ResultSet rs= dbb.executePlaQuery(Query)
								  logi.logInfo("afte rs")
	
	
								  while (rs.next())
								  {
										logi.logInfo("inside while")
										ResultSetMetaData md = rs.getMetaData()
										logi.logInfo("inside while"+rs.getMetaData())
										int columns = md.getColumnCount()
										logi.logInfo("columns for"+columns)
										// Putting values into HashMap
										coupon_frm_mpi.put("out_acct_no",acct_no)
										for(int i=1; i<=columns; ++i)
										{
											  logi.logInfo("inside for")
											  if(!(rs.getObject(i).toString().equalsIgnoreCase("null")))
											  {logi.logInfo("insie PUTTTT")
	
													l1.add(md.getColumnName(i).toLowerCase())
	
													int freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	
													logi.logInfo("frew of :+"+md.getColumnName
	
																(i).toLowerCase()+" is :"+freq)
	
													if(freq>1)
													{
	
														  coupon_frm_mpi.put(md.getColumnName
	
																	  (i).toLowerCase()+"["+ freq +"]",rs.getObject(i))
													}
	
													else
	
	
														  coupon_frm_mpi.put(md.getColumnName(i).toLowerCase(),rs.getObject(i))
											  }
	
											  else
													logi.logInfo("insie else")
										}
	
	
	
										/*    logi.logInfo("HASH #### 2"+coupon_frm_mpi)
										value=begin
										//value=value-1
										logi.logInfo("before ruturn")
										tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi) */
	
								  }
							}
							coupon_frm_mpi=coupon_frm_mpi.sort()
							tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi)
							++value
							
					  }
				}
					  
					  tempar = tempar.sort()
		  
					  return tempar
				}
	
	
	
	
	
	LinkedHashMap md_coupon_disc_bundle_m(String acct_hiereachy1)
	{
		
		
		logi.logInfo("intooo md_coupon_test_m ")
		String acct_hiereachy=acct_hiereachy1.split("#")[0]
		String branch=acct_hiereachy1.split("#")[1]
		def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		LinkedHashMap tempar = new LinkedHashMap()
		//LinkedHashMap acct_details_hm_temp = new LinkedHashMap()
	
	
		int value=1
	
		ConnectDB dbb = new ConnectDB()
		String result=null
		String count_query=null
		String acct_no=null
		String mpi=null
		
		acct_no = getValueFromRequest('get_acct_coupon_details_m',"//acct_no[1]")
		logi.logInfo("acct_noooo "+acct_no)
	 
		
		if(branch.equalsIgnoreCase("a"))
	{
		// acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hiereachy,null)
	 
	
				
	count_query="SELECT COUPON_CD FROM ariacore.ACCT_COUPONS WHERE acct_no="+acct_no+" and cancel_date is null and client_no="+client_no
	}
	
	
	else if(branch.equalsIgnoreCase("m"))
	{
	mpi =getValueFromRequest('get_acct_coupon_details_m',"//*/*:master_plan_instance_no[1]")
	
	logi.logInfo("mpiiiiiiiii "+mpi)
	count_query="SELECT COUPON_CD FROM ariacore.PLAN_INST_COUPON_MAP where PLAN_INSTANCE_NO="+mpi
	
	}
	
				 ResultSet coupon_codes=dbb.executePlaQuery(count_query)
				LinkedList al1= new LinkedList()
				LinkedList l1= new LinkedList()
				while (coupon_codes.next())
				{
					  logi.logInfo("inside while")
					  ResultSetMetaData md = coupon_codes.getMetaData()
					  int columns = md.getColumnCount()
					  logi.logInfo("columns for"+columns)
					  // Putting values into HashMap
	
					  for(int i=1; i<=columns; ++i)
	
					  {
						  
						  logi.logInfo("adding into list :"+ coupon_codes.getObject(i).toString())
						  al1.add(coupon_codes.getObject(i).toString())
						  
					  }
				}
				
				logi.logInfo("AL1 "+al1)
	
	
					  
					 for(int k2=0;k2<al1.size();k2++)
							
					  {
						  LinkedHashMap coupon_frm_mpi3 = new LinkedHashMap()
							logi.logInfo("<--------------------- Its disc bundle ------------------->  ")
	
							String gen_coupon_query="SELECT COUPON_CD AS OUT_COUPON_CD, APPLICATIONS_ALLOWED-1 AS COUPON_SCOPE,COMMENTS AS DESCRIPTION,TOTAL_USES,CURRENCY_CD, RECUR_DISCOUNT_FLAT_AMT,ONE_TIME_DISCOUNT_FLAT_AMT, RECUR_DISCOUNT_PCT, ONE_TIME_DISCOUNT_PCT,1 AS STATUS_IND FROM ARIACORE.COUPONS WHERE COUPON_CD='"+al1.get(k2)+"' AND CLIENT_NO="+client_no
							ResultSet rs_credit_gen=dbb.executePlaQuery(gen_coupon_query)
	
							while (rs_credit_gen.next())
							{
								
								
								  ResultSetMetaData md = rs_credit_gen.getMetaData()
								  int columns = md.getColumnCount()
	
								  // Putting values into HashMap
	
	
								  coupon_frm_mpi3.put("out_acct_no",acct_no)
								  l1.add("out_acct_no")
								  int freq
								  
								  if(branch.equalsIgnoreCase("m"))
								  {
									 
								 
								 freq = Collections.frequency(l1,"out_client_master_plan_instance_id")
								  if(freq>1)
								  
								  coupon_frm_mpi3.put("out_client_master_plan_instance_id["+freq+"]",mpi)
								  else
								  coupon_frm_mpi3.put("out_client_master_plan_instance_id",mpi)
								  l1.add("out_client_master_plan_instance_id")
								  
								  
								  freq = Collections.frequency(l1,"out_master_plan_instance_no")
								  if(freq>1)
								  
								  coupon_frm_mpi3.put("out_master_plan_instance_no["+freq+"]",mpi)
								  else
								  coupon_frm_mpi3.put("out_master_plan_instance_no",mpi)
								  l1.add("out_master_plan_instance_no")
								  }
								  for(int i=1; i<=columns; ++i)
	
								  {
	
										logi.logInfo("inside for")
										//if(!(md.getColumnName(i).toString().equalsIgnoreCase("null")))
									   // if(!(md.getColumnName(i).toString().contains("null")))
									   if(!( rs_credit_gen.getObject(i).toString().contains("null")))
	
										{
	
											  l1.add(md.getColumnName(i).toLowerCase())
											  logi.logInfo "KEYSSS 1 : "+l1
											  logi.logInfo("insie PUTTTT")
	
											   freq = Collections.frequency(l1,md.getColumnName(i).toLowerCase())
	
											  logi.logInfo("frew of :+"+md.getColumnName(i).toLowerCase()+" is :"+freq)
	
											  if(freq>1)
											  {
	
													coupon_frm_mpi3.put(md.getColumnName(i).toLowerCase()+"["+ ++freq
	
																+"]",rs_credit_gen.getObject(i))
											  }
											  else
	
													coupon_frm_mpi3.put(md.getColumnName(i).toLowerCase(),rs_credit_gen.getObject(i))
	
										}
										else
											  logi.logInfo("insie else")
								  }
							}
	
							logi.logInfo("<--------------------- Its disc bundle 2222 ------------------->  ")
	
							for(int m=0;m<al1.size();m++)
							{
								  
										String bundle_no_query="SELECT count(BUNDLE_NO) FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP WHERE COUPON_CD='"+al1.get(m)+"'"
										String bundle_no=dbb.executeQueryP2(bundle_no_query)
										
										if(bundle_no.toInteger()>=1)
	{
		
										String final_bundle_no_query="SELECT BUNDLE_NO FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP WHERE COUPON_CD='"+al1.get(m)+"'"
										String final_bundle_no=dbb.executeQueryP2(final_bundle_no_query)
										String final_coupon_code=al1.get(m)
										logi.logInfo "Inside final_coupon_code"+ final_coupon_code
										logi.logInfo "Inside final_bundle_no"+ final_bundle_no.toInteger()
	
	
										//Its discount bundle
	
	
	
	
										String bundle_count_query="SELECT COUNT(RULE_NO) FROM ARIACORE.CLIENT_DR_BUNDLE_MEMBERS WHERE BUNDLE_NO="+final_bundle_no.toInteger()
										String bundle_count=dbb.executeQueryP2(bundle_count_query)
										logi.logInfo "bundle_count "+ bundle_count
										for(int start=1;start<=bundle_count.toInteger();start++)
										{
											  logi.logInfo "start "+ start
											  String rule_no_query="SELECT A.RULE_NO FROM((SELECT RULE_NO, ROW_NUMBER() OVER(ORDER BY RULE_NO) AS SEQ FROM ARIACORE.CLIENT_DR_BUNDLE_MEMBERS WHERE BUNDLE_NO="+final_bundle_no+") A) WHERE A.SEQ="+start
											  String rule_no=dbb.executeQueryP2(rule_no_query)
	
											  //commented for max applications
											  String bundle_values_query="SELECT DISTINCT RULE_NO,CLIENT_RULE_ID,LABEL AS RULE_NAME,SCOPE_NO, DESCRIPTION ,FLAT_PERCENT_IND,CURRENCY_CD,AMOUNT,INLINE_OFFSET_IND,DURATION_TYPE_IND,MAX_APPLICATIONS_PER_ACCT,EXT_DESCRIPTION,ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE RULE_NO="+rule_no+" and client_no="+client_no
											  //String bundle_values_query="SELECT DISTINCT RULE_NO,CLIENT_RULE_ID,LABEL AS RULE_NAME,SCOPE_NO, DESCRIPTION ,FLAT_PERCENT_IND,CURRENCY_CD,AMOUNT,INLINE_OFFSET_IND,DURATION_TYPE_IND,EXT_DESCRIPTION,ALT_SERVICE_NO_2_APPLY FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE RULE_NO="+rule_no
											  ResultSet rs_disc_bundle=dbb.executePlaQuery(bundle_values_query)
											  ResultSetMetaData md_disc_bundle = rs_disc_bundle.getMetaData()
											  int columns = md_disc_bundle.getColumnCount()
											  LinkedHashMap disc_bundle_hm= new LinkedHashMap()
											  // Putting values into HashMap
	
											  while (rs_disc_bundle.next())
											  {
	
													for(int i=1; i<=columns; ++i)
													{
	
														  //if(!(rs_disc_bundle.getObject(i).toString().equalsIgnoreCase("null")))
														  if(!(rs_disc_bundle.getObject(i).toString().contains("null")))
														  {
	
																l1.add(md_disc_bundle.getColumnName(i).toLowerCase())
	
	
																logi.logInfo "KEYSSS 2 : "+l1
	
	
	
	
																int freq = Collections.frequency(l1,md_disc_bundle.getColumnName(i).toLowerCase())
	
																logi.logInfo("frew of :+"+md_disc_bundle.getColumnName(i).toLowerCase()+" is :"+freq)
	
																if(freq>1)
																{
	
																	  coupon_frm_mpi3.put(md_disc_bundle.getColumnName(i).toLowerCase
	
																				  ()+"["+ freq +"]",rs_disc_bundle.getObject(i))
																}
	
	
	
																else
																{
	
																	  coupon_frm_mpi3.put(md_disc_bundle.getColumnName(i).toLowerCase
	
																				  (),rs_disc_bundle.getObject(i))
	
																}
	
	
														  }
	
													}
	
													l1.add("bundle_no")
													int freq1 = Collections.frequency(l1,"bundle_no")
													if(freq1>1)
														  coupon_frm_mpi3.put("bundle_no["+  freq1 +"]",final_bundle_no)
													else
														  coupon_frm_mpi3.put("bundle_no",final_bundle_no)
	
													
	
	
												 //   tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi3)
	
	
											  }
	
										}
	
										String disc_bundle_final_query="SELECT BUNDLE_NO,CLIENT_BUNDLE_ID,LABEL AS BUNDLE_NAME,DESCRIPTION FROM ARIACORE.CLIENT_DISCOUNT_RULE_BUNDLES WHERE BUNDLE_NO =(SELECT BUNDLE_NO FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP WHERE COUPON_CD ='"+final_coupon_code+"')"
	
										ResultSet rs_disc_bundle_final=dbb.executePlaQuery(disc_bundle_final_query)
	
										ResultSetMetaData md_disc_bundle_final = rs_disc_bundle_final.getMetaData()
	
										int columns = md_disc_bundle_final.getColumnCount()
										logi.logInfo("inot while #### 13")
										// Putting values into HashMap
										 
								  int freq = Collections.frequency(l1,"out_acct_no")
								  if(freq>1)
								  
								  coupon_frm_mpi3.put("out_acct_no["+freq+"]",acct_no)
								  else
								  coupon_frm_mpi3.put("out_acct_no",acct_no)
								  
								  l1.add("out_acct_no")
										//coupon_frm_mpi.put("coupon_scope",0)
										while (rs_disc_bundle_final.next())
	
										{
											  logi.logInfo("inot while #### 1")
											  for(int i=1; i<=columns; ++i)
											  {
	
													if(!(rs_disc_bundle_final.getObject(i).toString().equalsIgnoreCase("null")))
													{
														  l1.add(md_disc_bundle_final.getColumnName(i).toLowerCase())
														  logi.logInfo "KEYSSS 3 : "+l1
														  logi.logInfo("inot while #### 2")
														   freq = Collections.frequency(l1,md_disc_bundle_final.getColumnName(i).toLowerCase())
	
														  logi.logInfo("frew of :+"+md_disc_bundle_final.getColumnName(i).toLowerCase()+" is :"+freq)
	
														  if(freq>1)
														  {
	
																coupon_frm_mpi3.put(md_disc_bundle_final.getColumnName(i).toLowerCase
	
																			()+"["+ freq +"]",rs_disc_bundle_final.getObject(i))
														  }
	
	
														  else
																coupon_frm_mpi3.put(md_disc_bundle_final.getColumnName(i).toLowerCase
	
																			(),rs_disc_bundle_final.getObject(i))
	
													}
											  }
	
										}
	
	
										logi.logInfo("Keyss of final hash :"+l1)
	
								  }
	
							}
							coupon_frm_mpi3=coupon_frm_mpi3.sort()
							tempar.put("coupons_detail_row["+value+"]",coupon_frm_mpi3)
							++value
					  }
				 
	
				tempar = tempar.sort()
	
				return tempar
	
	
	
	}
	
	/**
	 * for get_credit_details_m
	 * @param no param required
	 * @return hashmap
	 */
	public LinkedHashMap md_Verify_credit_details_DB_m(String TESTCASEID)
	{
		logi.logInfo("into md_Verify_credit_details_DB_m")
		ConnectDB db = new ConnectDB()
		int j=1
		LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
		LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
	   
		def acct_no = getValueFromResponse('create_acct_complete_m',ExPathRpc.CREATE_ACCT_COMPLETE_M_ACCTNO1)
		logi.logInfo("acct_no" +acct_no )
		String query ="select create_user as created_by,TO_CHAR(create_date, 'YYYY-MM-DD') as created_date,amount,credit_type,total_applied as applied_amount,left_to_apply as unapplied_amount,reason_cd as reason_code,reason_text,comments,acct_no as out_acct_no,master_plan_instance_no as out_master_plan_instance_no,master_plan_instance_no as out_client_mp_instance_id from ariacore.all_credits where acct_no = "+acct_no
		ResultSet rs4=db.executePlaQuery(query)
		ResultSetMetaData md4 = rs4.getMetaData();
		int columns4 = md4.getColumnCount();
		while (rs4.next())
		{
			for(int l=1; l<=columns4; l++)
			{
				if((rs4.getObject(l))!=null)
				{
					
						row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
					
				
				}
			}
			outer.put("get_credit_details_mResponse["+(j-1)+"]",row.sort())
			row = new LinkedHashMap<String, String>();
			j++
		
		}
		return outer.sort()
	}
	
	/**
	 * for get_cash_credits_details_m
	 * @param no param required
	 * @return hashmap
	 */
	public LinkedHashMap md_Verify_cash_credit_row_DB_m(String TESTCASEID)
	{
		logi.logInfo("into md_Verify_cash_credit_row_DB_m")
		ConnectDB db = new ConnectDB()
		int j=1
		LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
		LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
	   
		def acct_no = getValueFromResponse('create_acct_complete_m',ExPathRpc.CREATE_ACCT_COMPLETE_M_ACCTNO1)
		logi.logInfo("acct_no" +acct_no )
		String query ="select credit_id as credit_no,create_user as created_by,TO_CHAR(create_date, 'YYYY-MM-DD HH24:MI:SSxFF') as created_date,amount,reason_cd as reason_code,reason_text from ariacore.all_credits where acct_no = "+acct_no
		ResultSet rs4=db.executePlaQuery(query)
		ResultSetMetaData md4 = rs4.getMetaData();
		int columns4 = md4.getColumnCount();
		while (rs4.next())
		{
			for(int l=1; l<=columns4; l++)
			{
				if((rs4.getObject(l))!=null)
				{
					
						row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
					
				
				}
			}
			outer.put("cash_credits_row["+(j-1)+"]",row.sort())
			row = new LinkedHashMap<String, String>();
			j++
		
		}
		return outer.sort()
	}



/**
	 * for apply_cash_credits_details_m
	 * @param no param required
	 * @return hashmap
	 */
	public LinkedHashMap md_Cash_Credits_from_acct_m(String testcaseID){
		logi.logInfo "into md_Credits_fro_acct_m"
		String Invoice_no=getValueFromResponse('apply_cash_credit_m',ExPathRpcEnc.CREATE_ACCT_COMPLETE_INVOICENO1)
logi.logInfo("Invoice_no"+Invoice_no)
int j=1;
LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
		LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
if(Invoice_no.equalsIgnoreCase("NoVal"))
{
		InputMethods im=new InputMethods()
		// TO GET TJE ACCT NUMBER
		ConnectDB db=new ConnectDB();
		String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(testcaseID,null)
		logi.logInfo("acct_no"+acct_no)
		//String Credit_dtls = "select credit_id,acct_no,MASTER_PLAN_INSTANCE_NO,CREATE_USER,TO_CHAR(CREATE_DATE, 'YYYY-MM-DD'),AMOUNT,REASON_TEXT,REASON_CD,LEFT_TO_APPLY,TOTAL_APPLIED,CREDIT_TYPE from ariacore.all_credits where acct_no ="+acct_no
		String cash_Credit_dtls = "select a.CREDIT_ID as credit_no,a.REASON_CD as reason_code,a.CREATE_USER as created_by,a.AMOUNT as amount, TO_CHAR(a.CREATE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as created_date,a.reason_text from ariacore.ALL_CREDITS a where acct_no ="+acct_no
		
		ResultSet resultSet = db.executePlaQuery(cash_Credit_dtls)
		ResultSetMetaData md = resultSet.getMetaData();
		int columns = md.getColumnCount();
		if(columns>1)
		{
		
		  while (resultSet.next()){
						for(int u=1; u<=columns; ++u){
						  row.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
						}
						outer.put("apply_cash_credit_mResponse["+(j-1)+"]",row.sort())
						row = new LinkedHashMap<String, String>();
						j++
		  }
		return  outer.sort();
		}
		else {if(resultSet.next()){return resultSet.getObject(1).toString()
		}
		}
}
else{
ConnectDB db=new ConnectDB();
		String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(testcaseID,null)
		logi.logInfo("acct_no"+acct_no)
		String cash_Credit_dtls = "select a.CREDIT_NO as credit_no,a.REASON_CD as reason_code,a.CREATE_USER as created_by,a.AMOUNT as amount, TO_CHAR(a.CREATE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') as created_date from ariacore.CASH_CREDITS a where invoice_no ="+Invoice_no
		
		ResultSet resultSet = db.executePlaQuery(cash_Credit_dtls)
		ResultSetMetaData md = resultSet.getMetaData();
		int columns = md.getColumnCount();
		if(columns>1)
		{
		
		  while (resultSet.next()){
						for(int u=1; u<=columns; ++u){
						  row.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
						}
						outer.put("apply_cash_credit_mResponse["+(j-1)+"]",row.sort())
						row = new LinkedHashMap<String, String>();
						j++
		  }
		return  outer.sort();
		}
		else {if(resultSet.next()){return resultSet.getObject(1).toString()
		}
		}
		}
}

	
	/**
	 * for collect_from_accct_m
	 * @param acct hierarchy
	 * @return hashmap
	 */
	
   
	   LinkedHashMap md_collect_from_acct_m(String acct_hierarchy)
	   {
		   
		   logi.logInfo "into md_collect_from_accct_m"
		   
		   InputMethods im=new InputMethods()
		   ConnectDB db=new ConnectDB();
		   LinkedHashMap coll_frm_acct = new LinkedHashMap()
		   int count=0
		   String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		   logi.logInfo("acct_no"+acct_no)
		   //String Credit_dtls = "select credit_id,acct_no,MASTER_PLAN_INSTANCE_NO,CREATE_USER,TO_CHAR(CREATE_DATE, 'YYYY-MM-DD'),AMOUNT,REASON_TEXT,REASON_CD,LEFT_TO_APPLY,TOTAL_APPLIED,CREDIT_TYPE from ariacore.all_credits where acct_no ="+acct_no
		   String collect_details = "select atr.EVENT_NO as transaction_id,pm.PROC_VALIDATE_ADDRESS as proc_cvv_response,pm.PROC_CARD_SECURE as proc_avs_response,pm.PMT_BATCH_NO as proc_cavv_response,STATUS_CODE as proc_status_code,pm.STATUS_TEXT as proc_status_text,pm.PROC_PYMNT_ID as proc_payment_id,pm.AUTH_CODE as proc_auth_code,pm.MERCH_COMMENTS as proc_merch_comments from ariacore.payments pm join ariacore.acct_transaction atr on pm.PAYMENT_ID=atr.SOURCE_NO and pm.client_no=atr.client_no  where pm.acct_no ="+acct_no+" order by TRANSACTION_ID"
		   LinkedHashMap dummy = new LinkedHashMap()
		   ResultSet resultSet = db.executePlaQuery(collect_details)
		   ResultSetMetaData md = resultSet.getMetaData();
		   int columns = md.getColumnCount();
		   if(columns>=1)
		   {
		   LinkedHashMap row = new LinkedHashMap(columns)
				
			 while (resultSet.next())
			 {
				 for(int u=1; u<=columns; ++u)
				 {
					 if(resultSet.getObject(u)!=null)
				   row.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
				   row=row.sort();
					 }
				 
				 logi.logInfo "beforeeeeeee :"+row
				 row=row.sort();
				 logi.logInfo "afteeer :"+row
				 coll_frm_acct.put("collect_from_account_mResponse["+count+"]",row)
				 }
		   
		   }
	   return 	coll_frm_acct
	   }
	   
	   
	   /**
		* for get_refundable_payments_m
		* @param acct hierarchy
		* @return hashmap
		*/
	   
	   LinkedHashMap md_get_refundable_payments_m(String acct_hierarchy)
	   {
		   
		   logi.logInfo "into md_get_refundable_payments_m"
		   
		   InputMethods im=new InputMethods()
		   ConnectDB db=new ConnectDB();
		   LinkedHashMap pay_details = new LinkedHashMap()
		   int count=0
		   String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		   logi.logInfo("acct_no"+acct_no)
		   //String payment_transaction_id=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/payment_transaction_id[1]")
		   //logi.logInfo("payment_transaction_id"+payment_transaction_id)
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   String pay_details_q = "SELECT PAYMENT_TRANS_EVENT_NO AS payment_transaction_id,TO_CHAR(PAYMENT_DATE,'FMMM/DD/YYYY') AS payment_date,AMOUNT as payment_amount,REFUNDED_AMOUNT as payment_refunded_amount,REFUNDABLE_AMOUNT as payment_refundable_amount FROM ARIACORE.ALL_REFUNDABLE_PAYMENTS WHERE CLIENT_NO="+client_no+" AND ACCT_NO="+acct_no+"order by payment_transaction_id "
		   LinkedHashMap dummy = new LinkedHashMap()
		   ResultSet resultSet = db.executePlaQuery(pay_details_q)
		   ResultSetMetaData md = resultSet.getMetaData();
		   int columns = md.getColumnCount();
		   if(columns>=1)
		   {
		   LinkedHashMap row = new LinkedHashMap(columns)
				
			 while (resultSet.next()){
				 for(int u=1; u<=columns; ++u){
					 if(resultSet.getObject(u)!=null)
				   row.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
				   row=row.sort();
					 }
				 
				
				 row=row.sort();
				 
				 
				String pay_desc=db.executeQueryP2("SELECT PROC_STATUS_TEXT FROM ARIACORE.ALL_REFUNDABLE_PAYMENTS WHERE ACCT_NO="+acct_no+" order by PAYMENT_TRANS_EVENT_NO ")
				logi.logInfo "PAY DESC----------"+pay_desc
				String pay_src,pay
			 pay_src=db.executeQueryP2("SELECT PAY_SOURCE FROM ARIACORE.ALL_REFUNDABLE_PAYMENTS WHERE ACCT_NO="+acct_no)
				logi.logInfo "PAY SOURCE----------"+pay_src
				if(!(pay_desc.equalsIgnoreCase("Approved")))
				pay=pay_desc+" "+pay_src
			   else
			   pay="Visa ending in"+" "+pay_src
				
				 
				logi.logInfo "PAY ----------"+pay
				row.put("payment_description",pay)
				row=row.sort();
				 pay_details.put("refundable_payments_row["+count+"]",row)
				 }
		   
		   }
	   return 	pay_details
	   }
	   
	   /**
		* for issue_refund_to_acct
		* @param acct hierarchy # transaction_type
		* @return hashmap
		*/
	   
	   LinkedHashMap md_issue_refund_to_acct_m(String acct_hierarchy1)
	   {
		   
		   logi.logInfo "into md_issue_refund_to_acct_m_m"
		   String acct_hierarchy=acct_hierarchy1.split("#")[0]
		   String trans_type_no=acct_hierarchy1.split("#")[1]
		   logi.logInfo "into ,,,,,,,,,,,,,,,"+acct_hierarchy+"acct_hierarchy"+trans_type_no
		   
		   InputMethods im=new InputMethods()
		   ConnectDB db=new ConnectDB();
		   LinkedHashMap issue_refund = new LinkedHashMap()
		   int count=0
		   String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		   logi.logInfo("acct_no"+acct_no)
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   //String pay_details_q = "SELECT EVENT_NO AS out_transaction_id,AMOUNT AS applied_total_refund_amount, 0 AS applied_total_reversal_amount FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" AND TRANSACTION_TYPE="+trans_type_no
		   //
		   String pay_details_q = "SELECT EVENT_NO AS out_transaction_id,AMOUNT AS applied_total_refund_amount FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" AND TRANSACTION_TYPE="+trans_type_no
		   LinkedHashMap dummy = new LinkedHashMap()
		   ResultSet resultSet = db.executePlaQuery(pay_details_q)
		   ResultSetMetaData md = resultSet.getMetaData();
		   int columns = md.getColumnCount();
		   if(columns>1)
		   {
		   LinkedHashMap row = new LinkedHashMap(columns)
				
			 while (resultSet.next()){
				 for(int u=1; u<=columns; ++u)
				 {
					 if(resultSet.getObject(u)!=null)
				   row.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
				   row=row.sort();
					 }
				 
				 String applied_total_reversal_amount_count1 = db.executeQueryP2("SELECT COUNT(AMOUNT) FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" AND TRANSACTION_TYPE=8")
				 int applied_total_reversal_amount_count=applied_total_reversal_amount_count1.toInteger()
				 if(!(applied_total_reversal_amount_count==0))
				{
				 //String applied_total_reversal_amount = db.executeQueryP2("SELECT AMOUNT FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" AND TRANSACTION_TYPE="+trans_type_no)
					String applied_total_reversal_amount = db.executeQueryP2("SELECT APPLIED_AMOUNT FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" AND TRANSACTION_TYPE=8")
				 row.put("applied_total_reversal_amount",applied_total_reversal_amount)
				}
				else
				row.put("applied_total_reversal_amount","0")
				  row=row.sort();
				
				 issue_refund.put("issue_refund_to_acct_mResponse["+count+"]",row)
				 }
		   
		   }
	   return 	issue_refund
	   }
	   
	   /**
		* for issue_refund_to_acct
		* @param acct hierarchy
		* @return hashmap
		*/
	   
	   LinkedHashMap md_issue_refund_to_acct_reversed_invoice_transactions_m(String acct_hierarchy)
	   {
		   logi.logInfo "**** into md_issue_refund_to_acct_reversed_invoice_transactions_m ****"
	   
		   InputMethods im=new InputMethods()
		   ConnectDB db=new ConnectDB();
		   LinkedHashMap dummy = new LinkedHashMap()
			   
		   String invoice_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//lit:invoice_no")
		   String seq_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//lit:invoice_line_no")
		   String trans_id=db.executeQueryP2("select a.event_no from(select row_number() over (order by acct_no) as seq,event_no from ariacore.ACCT_TRANSACTION where invoice_no="+invoice_no+")a where a.seq="+seq_no)
		   String trans_q="select aid.invoice_no,aid.seq_num as invoice_line_no,aid.service_no,ser.CLIENT_SERVICE_ID as CLIENT_SERVICE_ID, aid.COMMENTS as transaction_comments,rd.credit as reversed_amount,TO_CHAR(rd.REVERSING_DATE,'YYYY-MM-DD') as reversing_date  from ariacore.ALL_INVOICE_DETAILS aid join ariacore.REFUND_DETAIL rd on rd.invoice_no=aid.invoice_no  join ariacore.SERVICES ser on ser.SERVICE_NO=aid.service_no and ser.CUSTOM_TO_CLIENT_NO=aid.client_no  where aid.invoice_no="+invoice_no+" and aid.seq_num="+seq_no
		   LinkedHashMap row = new LinkedHashMap()
		   ResultSet resultSet = db.executePlaQuery(trans_q)
		   ResultSetMetaData md = resultSet.getMetaData();
		   int columns = md.getColumnCount();
				
				 while (resultSet.next()){
					 for(int u=1; u<=columns; ++u)
					 {
						 if(resultSet.getObject(u)!=null)
					   row.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
					   row=row.sort();
					   
						 }
					 row.put("reversed_transaction_id",trans_id)
					 row=row.sort();
					 
				 }
				 dummy.put("reversed_invoice_transactions_row[0]",row)
		   return dummy
		   
		   
	   }
	   
	   /**
		* for all apis
		* @param acct hierarchy # transaction type
		* @return hashmap
		*/
	   LinkedHashMap md_get_transaction_id_m(String acct_hierarchy1)
	   {
		   
		   logi.logInfo "into md_get_transaction_id__m"
		   String acct_hierarchy=acct_hierarchy1.split("#")[0]
		   String trans_type_no=acct_hierarchy1.split("#")[1]
		   
		   InputMethods im=new InputMethods()
		   ConnectDB db=new ConnectDB();
		   int count=0
		   String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		   logi.logInfo("acct_no"+acct_no)
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   String trans_no = db.executeQueryP2("SELECT EVENT_NO FROM ARIACORE.ACCT_TRANSACTION WHERE  TRANSACTION_TYPE="+trans_type_no+" AND ACCT_NO="+acct_no)
		   LinkedHashMap dummy = new LinkedHashMap()
		   dummy.put("transaction_id",trans_no)
		   LinkedHashMap dummy1 = new LinkedHashMap()
		   dummy1.put("record_external_payment_mResponse[0]", dummy)
		   
		   
	   return 	dummy1
	   }
	   
	   
	   /**
		* for all apis
		* @param acct hierarchy
		* @return hashmap
		*/
	   String md_get_transaction_amt_m(String acct_hierarchy1)
	   {
		   
		   logi.logInfo "into md_get_transaction_id__m"
		   String acct_hierarchy=acct_hierarchy1.split("#")[0]
		   String trans_type_no=acct_hierarchy1.split("#")[1]
		   
		   InputMethods im=new InputMethods()
		   ConnectDB db=new ConnectDB();
		   int count=0
		   String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		   logi.logInfo("acct_no"+acct_no)
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   String trans_no = db.executeQueryP2("SELECT AMOUNT FROM ARIACORE.ALL_PAYMENTS WHERE  PAYMENT_TRANS_TYPE ="+trans_type_no+" and ACCT_NO="+acct_no)
		   //LinkedHashMap dummy = new LinkedHashMap()
		   //dummy.put("record_external_payment_mResponse[0]",trans_no)
		   
	   return 	trans_no
	   }
   
	   /**
		* for md_get_reversible_invs_by_pay_method
		* @param acct hierarchy
		* @return hashmap
		*/
	   LinkedHashMap md_get_reversible_invs_by_pay_m(String acct_hierarchy)
	   {
		   logi.logInfo "into get_reversible_invs_by_pay_m"
			   
		   InputMethods im=new InputMethods()
		   ConnectDB db=new ConnectDB();
		   int count=0
		   String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
		   logi.logInfo("acct_no"+acct_no)
		   
		   String invoice_no = db.executeQueryP2("select max(invoice_no) from ariacore.gl where acct_no="+acct_no)
		   
		   
		   //String  reversible_invs_q="SELECT  AID.SEQ_NUM AS invoice_line_no,AID.INVOICE_NO, AID.COMMENTS AS invoice_trans_description, TO_CHAR(AID.START_DATE,'YYYY-MM-DD') AS invoice_trans_recur_start_date,TO_CHAR(AID.UPDATE_DATE,'fmMM/DD/YYYY') AS invoice_trans_date,RIL.ORIGINAL_AMT as invoice_trans_amount, RIL.REFUNDABLE_AMT as inv_trans_reversible_amount from ariacore.ALL_INVOICE_DETAILS  AID JOIN ariacore.REFUNDABLE_INVOICE_LINES RIL ON RIL.INVOICE_NO=AID.INVOICE_NO and AID.usage_rate=RIL.ORIGINAL_AMT where RIL.invoice_no="+invoice_no+" and RIL.acct_no="+acct_no
		   String  reversible_invs_q="SELECT  AID.SEQ_NUM AS invoice_line_no,AID.INVOICE_NO, AID.COMMENTS AS invoice_trans_description, TO_CHAR(AID.START_DATE,'YYYY-MM-DD') AS invoice_trans_recur_start_date,TO_CHAR(TO_DATE(TO_CHAR(AID.UPDATE_DATE,'MM/fmDD/YYYY'),'MM/DD/YYYY'),'FMMM/DD/YYYY') AS invoice_trans_date,RIL.ORIGINAL_AMT as invoice_trans_amount, RIL.REFUNDABLE_AMT as inv_trans_reversible_amount from ariacore.ALL_INVOICE_DETAILS  AID JOIN ariacore.REFUNDABLE_INVOICE_LINES RIL ON RIL.INVOICE_NO=AID.INVOICE_NO and AID.usage_rate=RIL.ORIGINAL_AMT where RIL.invoice_no="+invoice_no+" and RIL.acct_no="+acct_no+" order by INVOICE_LINE_NO"
		   
		   LinkedHashMap dummy = new LinkedHashMap()
		   ResultSet resultSet = db.executePlaQuery(reversible_invs_q)
		   ResultSetMetaData md = resultSet.getMetaData();
		   int columns = md.getColumnCount();
		   if(columns>=1)
		   {
		   
				
			 while (resultSet.next()){
				 LinkedHashMap row = new LinkedHashMap()
				 for(int u=1; u<=columns; ++u)
				 {
					 if(resultSet.getObject(u)!=null)
				   row.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
				   row.putAt("invoiced_acct_no", acct_no)
				   row=row.sort();
				   
					 }
				 logi.logInfo "RORRRRRRRRRRRRR"+row
				 dummy.put("reversible_inv_trans_row["+count+"]",row)
				 ++count
				 }
	   
			
				 }
		   
	   return dummy
	   }
		
	   
	  /** To get the Invoice line items get_invoice_information_m
		* @param str ()
		* @return Invoice information - Invoice details
		*/
	   public def md_get_invoice_information_outer_m(String param)
	   {
		   logi.logInfo("********md_get_invoice_information_m Starts**************")
		   ConnectDB db = new ConnectDB()
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   LinkedHashMap line_item = new LinkedHashMap()
		   LinkedHashMap row_item = new LinkedHashMap()
		   LinkedHashMap output = new LinkedHashMap()
		   LinkedHashMap check = new LinkedHashMap()
		   LinkedHashMap invoice_info_row = new LinkedHashMap()
		   LinkedHashMap invoice_line_m_row = new LinkedHashMap()
		   String query1 = null
		   String query_string = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/query_string[1]")
		   int offset = 0;
		   int limit = 100;
		   String offset_s =getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/offset[1]")
		   String limit_s = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/limit[1]")
		   if(!offset_s.contains("NoVal"))offset=Integer.parseInt(offset_s);
		   if(!limit_s.contains("NoVal"))limit=Integer.parseInt(limit_s);
		   query_string = query_string.replaceAll("invoice_type_cd = F","ai.invoice_type_code = 'F'");
		   query_string = query_string.replaceAll("custom_status_label","ai.CUSTOM_STATUS_LABEL");
		   query_string = query_string.replaceAll("client_notes","ai.CLIENT_NOTES");
		   query_string = query_string.replaceAll("Test","'Test'");
		   query_string = query_string.replaceAll("testnote","'testnote'");
		   query_string = query_string.replaceAll("second_acct_statement_seq_str","sass");
		   query_string = query_string.replaceAll("sass","actst.second_acct_statement_seq_str");
		   query_string = query_string.replaceAll("total_due","ai.total_due");
		   if(!query_string.contains("order by"))
		   {query_string = query_string.replaceAll("001 order by invoice_no","001");
		   query_string = query_string.replaceAll("invoice_no","ai.invoice_no");
		   query_string = query_string.replaceAll("acct_no","acct.acct_no");
		   query_string = query_string.replaceAll("user_id","acct.userid");
		   query_string = query_string.replaceAll("client_acct_id","acct.CLIENT_ACCT_ID");
		   //query_string = query_string.replaceAll("invoice_type","ai.REAL_PEND_IND");
		   query_string = query_string.replaceAll("invoice_type_cd","ai.invoice_type_code");
		   query_string = query_string.replaceAll("second_acct_statement_seq_str","sass");
		   query_string = query_string.replaceAll("acct_statement_seq_str","actst.acct_statement_seq_str");
		   query_string = query_string.replaceAll("sass","actst.second_acct_statement_seq_str");
		   query_string = query_string.replaceAll("bill_company_name","ai.BILL_COMPANY_NAME");
		   query_string = query_string.replaceAll("pay_method_type","pm.METHOD_ID");
		   query_string = query_string.replaceAll("pay_method_name","ai.PAY_METHOD_NAME");
		   query_string = query_string.replaceAll("aria_statement_no","actst.statement_no");
		   query_string = query_string.replaceAll("client_master_plan_instance_id","pi.PLAN_INSTANCE_CDID");
		   query_string = query_string.replaceAll("master_plan_instance_id","aid.master_plan_instance_no");
		   query_string = query_string.replaceAll("po_num","aid.PURCHASE_ORDER_NO");
		   query_string = query_string.replaceAll("invoice_transaction_id","atn.EVENT_NO");
		   query_string = query_string.replaceAll("last_updated","ai.UPDATE_DATE");
		   //query_string = query_string.replaceAll("total_due","ai.total_due");
		   query_string = query_string.replaceAll("\"","");
		   query_string = query_string.replaceAll("= ","= '");
		   query_string = query_string.replaceAll("> ","> '");
		   query_string = query_string.replaceAll("< ","< '");
		   query_string = query_string.replaceAll(" and","' and");
		   query_string += "'";
		   }
		   ArrayList INVOICES = new ArrayList();
		   ResultSet rs
		   Collections.sort(INVOICES);
		   if(query_string.contains("ai.UPDATE_DATE") )
		   {
			   query_string = query_string.replaceAll("=","like")
			   query_string = query_string.replaceAll(" '"," '%")
			   query_string = query_string.replaceAll("' ","%' ")
		   }
		   else
		   {
			   
		   String qry = null;
		   if(query_string.contains("ai.total_due")||query_string.contains("ai.invoice_type_code = 'F'"))
		   {
			   qry = "SELECT distinct ai.invoice_no FROM ariacore.gl ai WHERE ai.client_no = "+client_no+" and	"+query_string
			   }
		   else{
			   qry = "SELECT distinct ai.invoice_no FROM ariacore.gl ai JOIN ariacore.acct acct ON acct.client_no=ai.client_no AND ai.acct_no   =acct.acct_no	JOIN ariacore.acct_address aa ON aa.client_no        =acct.client_no AND aa.acct_no         =acct.acct_no AND aa.ADDRESS_TYPE_CD = 0 AND aa.SOURCE_NO       = 0 JOIN ariacore.acct_statement actst ON actst.client_no=acct.client_no and actst.invoice_no  =ai.invoice_no and actst.acct_no = ai.acct_no join ariacore.acct_transaction atn on atn.client_no=ai.client_no and atn.invoice_no=ai.invoice_no join ariacore.plan_instance pi on pi.client_no=ai.client_no JOIN ariacore.acct_billing_group abg ON abg.client_no         =acct.client_no	AND abg.acct_no          =acct.acct_no AND abg.BILLING_GROUP_NO = ai.billing_group_no JOIN ariacore.gl_detail aid ON aid.client_no     =acct.client_no AND aid.invoice_no   =ai.invoice_no WHERE acct.client_no = "+client_no+" and "+query_string
			   }
			   rs = db.executePlaQuery(qry)
		   }
		   ResultSetMetaData meta = rs.getMetaData()
		   int columns = meta.getColumnCount();
		   while (rs.next())
		   {
			   for(int u=1; u<=columns; ++u)
			   {
				   if(rs.getObject(u)!=null)
					   INVOICES.add(rs.getObject(u));
			   }
		   }
		   
		   Collections.sort(INVOICES);
	  
		   logi.logInfo  "INVOICES in the list "+INVOICES
		   logi.logInfo  "columns in the list "+columns
		   int abcd= 1, loopss=1;
		   for(String INVOICE : INVOICES)
		   {
			   if(loopss>offset)
			   {
			   String Query = null;
			   if(query_string.contains("ai.total_due"))
			   {
				   Query="SELECT distinct gl.invoice_no,gl.INVOICE_TYPE_CODE as invoice_type_cd, acct.acct_no,  acct.userid AS user_id,acct.CLIENT_ACCT_ID,  TO_CHAR(gl.from_date, 'YYYY-MM-DD HH24:MI:SSxFF')       AS from_date, TO_CHAR(gl.to_date, 'YYYY-MM-DD HH24:MI:SSxFF') AS to_date,  gl.DEBIT  AS	amount, TO_CHAR(gl.BILL_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS BILL_DATE, TO_CHAR(gl.DUE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS DUE_DATE, TO_CHAR(gl.PAID_DATE, 'YYYY-MM-DD	HH24:MI:SSxFF')       AS PAID_DATE, TO_CHAR(gl.NOTIFY_DATE, 'YYYY-MM-DD HH24:MI:SSxFF')     AS NOTIFY_DATE, gl.CURRENCY AS currency_cd, gl.BAL_FWD AS balance_forward,	actst.BALANCE_FORWARD AS statement_balance_forward, gl.TOTAL_DUE , gl.COMMENTS, gl.LONG_COMMENTS AS additional_comments, TO_CHAR(gl.UPDATE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS last_updated, actst.STATEMENT_NO AS aria_statement_no,actst.ACCT_STATEMENT_SEQ_STR, actst.SECOND_ACCT_STATEMENT_SEQ_STR, gl.VOIDING_EVENT_NO,gl.CUSTOM_STATUS_LABEL, gl.CLIENT_NOTES, gl.BILLING_GROUP_NO, abg.PROFILE_CDID AS client_billing_group_id FROM ariacore.all_invoices ai JOIN ariacore.gl_detail gld ON	gld.invoice_no = ai.invoice_no JOIN ariacore.gl gl ON gl.invoice_no = gld.invoice_no JOIN ariacore.acct acct ON acct.client_no=ai.client_no AND ai.acct_no	=acct.acct_no JOIN ariacore.acct_address aa ON aa.client_no        =acct.client_no AND aa.acct_no         =acct.acct_no AND aa.ADDRESS_TYPE_CD = 0 AND aa.SOURCE_NO = 0 left JOIN ariacore.acct_statement actst ON actst.client_no   =acct.client_no join ariacore.plan_instance pi on pi.client_no=ai.client_no JOIN ariacore.acct_billing_group abg ON abg.client_no         =acct.client_no AND abg.acct_no          =acct.acct_no AND abg.BILLING_GROUP_NO = ai.billing_group_no WHERE	acct.client_no  = "+client_no+" AND ai.invoice_no = "+INVOICE+""
				   }
			   else{
			   Query="SELECT distinct gl.invoice_no,gl.INVOICE_TYPE_CODE as invoice_type_cd, acct.acct_no,  acct.userid AS user_id,  acct.CLIENT_ACCT_ID,TO_CHAR(gl.from_date, 'YYYY-MM-DD HH24:MI:SSxFF')       AS from_date, TO_CHAR(gl.to_date, 'YYYY-MM-DD HH24:MI:SSxFF') AS to_date,  gl.DEBIT  AS amount, TO_CHAR(gl.BILL_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS BILL_DATE, TO_CHAR(gl.DUE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS DUE_DATE, TO_CHAR(gl.PAID_DATE, 'YYYY-MM-DD HH24:MI:SSxFF')       AS PAID_DATE, TO_CHAR(gl.NOTIFY_DATE, 'YYYY-MM-DD HH24:MI:SSxFF')     AS NOTIFY_DATE, gl.CURRENCY AS currency_cd, gl.BAL_FWD AS balance_forward,	actst.BALANCE_FORWARD AS statement_balance_forward, gl.TOTAL_DUE , gl.COMMENTS, gl.LONG_COMMENTS AS additional_comments, TO_CHAR(gl.UPDATE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS last_updated, actst.STATEMENT_NO AS aria_statement_no,actst.ACCT_STATEMENT_SEQ_STR, actst.SECOND_ACCT_STATEMENT_SEQ_STR, gl.VOIDING_EVENT_NO,gl.CUSTOM_STATUS_LABEL, gl.CLIENT_NOTES, gl.BILLING_GROUP_NO, abg.PROFILE_CDID AS client_billing_group_id FROM ariacore.all_invoices ai JOIN ariacore.gl_detail gld ON	gld.invoice_no = ai.invoice_no JOIN ariacore.gl gl ON gl.invoice_no = gld.invoice_no JOIN ariacore.acct acct ON acct.client_no=ai.client_no AND ai.acct_no	=acct.acct_no JOIN ariacore.acct_address aa ON aa.client_no        =acct.client_no AND aa.acct_no         =acct.acct_no AND aa.ADDRESS_TYPE_CD = 0 AND aa.SOURCE_NO	  = 0 left JOIN ariacore.acct_statement actst ON actst.client_no   =acct.client_no AND actst.acct_no    =acct.acct_no AND actst.INVOICE_NO = ai.INVOICE_NO join	ariacore.plan_instance pi on pi.client_no=ai.client_no JOIN ariacore.acct_billing_group abg ON abg.client_no         =acct.client_no AND abg.acct_no	=acct.acct_no AND abg.BILLING_GROUP_NO = ai.billing_group_no WHERE acct.client_no  = "+client_no+" AND ai.invoice_no = "+INVOICE+""
			   }
			   ResultSet resultSet = db.executePlaQuery(Query)
			   ResultSetMetaData md = resultSet.getMetaData()
			   int columns1 = md.getColumnCount();
	  
	  
			   while (resultSet.next())
			   {
				   for(int u=1; u<=columns1; ++u)
				   {
					   if(resultSet.getObject(u)!=null)
						   line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
				   }
			   }
			   
			   logi.logInfo  "Line_item Values"+line_item.toString()
			  // int counts= Integer.parseInt(db.executeQueryP2("SELECT count(*) from ariacore.gl_detail where invoice_no="+INVOICE).toString())
			  
			   if(abcd==1)
			   row_item.put("invoice_details_m_row",line_item.sort())
				  else
					  row_item.put("invoice_details_m_row["+(abcd)+"]",line_item.sort())
					  line_item = new LinkedHashMap()
					  logi.logInfo  "Printing Overall looping---"+row_item
					  if(abcd==1&&abcd == limit)output.put("invoice_details_m",row_item)
					  
					  if(abcd == limit)break;
					  abcd+=1;
				loopss+=1;
			   }
				  row_item=row_item.sort();
				  output.put("invoice_details_m",row_item)
				  logi.logInfo  "Before return md_get_invoice_information_m "
			  }
				  return output
				  logi.logInfo("********md_get_invoice_information_m Ends**************")
			  
		  }
	   
	   /** To get the Invoice line items get_invoice_information_m
		* @param str ()
		* @return Invoice information - Invoice line items details
		*/
	   public def md_get_invoice_information_inner_m(String param)
	   {
			  logi.logInfo("********md_get_invoice_information_inner_m Starts**************")
		   ConnectDB db = new ConnectDB()
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   LinkedHashMap line_item = new LinkedHashMap()
		   LinkedHashMap line = new LinkedHashMap()
		   LinkedHashMap row_item = new LinkedHashMap()
		   LinkedHashMap output = new LinkedHashMap()
		   LinkedHashMap check = new LinkedHashMap()
		   LinkedHashMap invoice_info_row = new LinkedHashMap()
		   LinkedHashMap invoice_line_m_row = new LinkedHashMap()
		   String query1 = null
		   int offset = 0;
		   int limit = 100;
		   String offset_s =getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/offset[1]")
		   String limit_s = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/limit[1]")
		   if(!offset_s.contains("NoVal"))offset=Integer.parseInt(offset_s);
		   if(!limit_s.contains("NoVal"))limit=Integer.parseInt(limit_s);
		   
		   String query_string = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/query_string[1]")
		   query_string = query_string.replaceAll("invoice_type_cd = F","ai.invoice_type_code = 'F'");
		   query_string = query_string.replaceAll("custom_status_label","ai.CUSTOM_STATUS_LABEL");
		   query_string = query_string.replaceAll("client_notes","ai.CLIENT_NOTES");
		   query_string = query_string.replaceAll("Test","'Test'");
		   query_string = query_string.replaceAll("testnote","'testnote'");
		   query_string = query_string.replaceAll("second_acct_statement_seq_str","sass");
		   query_string = query_string.replaceAll("sass","actst.second_acct_statement_seq_str");
		   if(!query_string.contains("order by")){
		   query_string = query_string.replaceAll("001 order by invoice_no","001");
		   query_string = query_string.replaceAll("invoice_no","ai.invoice_no");
		   query_string = query_string.replaceAll("acct_no","acct.acct_no");
		   query_string = query_string.replaceAll("user_id","acct.userid");
		   query_string = query_string.replaceAll("client_acct_id","acct.CLIENT_ACCT_ID");
		   query_string = query_string.replaceAll("second_acct_statement_seq_str","sass");
		   query_string = query_string.replaceAll("acct_statement_seq_str","actst.acct_statement_seq_str");
		   query_string = query_string.replaceAll("sass","actst.second_acct_statement_seq_str");
		   query_string = query_string.replaceAll("bill_company_name","ai.BILL_COMPANY_NAME");
		   query_string = query_string.replaceAll("pay_method_type","pm.METHOD_ID");
		   query_string = query_string.replaceAll("pay_method_name","ai.PAY_METHOD_NAME");
		   query_string = query_string.replaceAll("custom_status_label","ai.CUSTOM_STATUS_LABEL");
		   query_string = query_string.replaceAll("client_notes","ai.CLIENT_NOTES");
		   query_string = query_string.replaceAll("aria_statement_no","actst.statement_no");
		   query_string = query_string.replaceAll("client_master_plan_instance_id","pi.PLAN_INSTANCE_CDID");
		   query_string = query_string.replaceAll("master_plan_instance_id","aid.master_plan_instance_no");
		   query_string = query_string.replaceAll("po_num","aid.PURCHASE_ORDER_NO");
		   query_string = query_string.replaceAll("invoice_transaction_id","atn.EVENT_NO");
		   query_string = query_string.replaceAll("last_updated","ai.UPDATE_DATE");
		   //query_string = query_string.replaceAll("total_due","ai.total_due");
		   query_string = query_string.replaceAll("\"","");
		   query_string = query_string.replaceAll("= ","= '");
		   query_string = query_string.replaceAll("> ","> '");
		   query_string = query_string.replaceAll("< ","< '");
		   query_string = query_string.replaceAll(" and","' and");
		   query_string += "'";
		   }
		   
			  
			  ArrayList INVOICES = new ArrayList();
		   ResultSet rs
		   if(query_string.contains("ai.UPDATE_DATE") )
		   {
			   query_string = query_string.replaceAll("=","like")
			   query_string = query_string.replaceAll(" '"," '%")
			   query_string = query_string.replaceAll("' ","%' ")
		   }
		   
			   
		   String qry = null;
		   if(query_string.contains("ai.total_due")||query_string.contains("ai.invoice_type_code = 'F'"))
		   {
			   qry = "SELECT distinct ai.invoice_no FROM ariacore.gl ai WHERE ai.client_no = "+client_no+" and "+query_string
			   }
		   else{
			   qry = "SELECT distinct ai.invoice_no FROM ariacore.gl ai JOIN ariacore.acct acct ON acct.client_no=ai.client_no AND ai.acct_no   =acct.acct_no	JOIN ariacore.acct_address aa ON aa.client_no        =acct.client_no AND aa.acct_no         =acct.acct_no AND aa.ADDRESS_TYPE_CD = 0 AND aa.SOURCE_NO       = 0 JOIN ariacore.acct_statement actst ON actst.client_no=acct.client_no and actst.acct_no = ai.acct_no join ariacore.acct_transaction atn on atn.client_no=ai.client_no and atn.invoice_no=ai.invoice_no join ariacore.plan_instance pi on pi.client_no=ai.client_no JOIN ariacore.acct_billing_group abg ON abg.client_no         =acct.client_no	AND abg.acct_no          =acct.acct_no AND abg.BILLING_GROUP_NO = ai.billing_group_no JOIN ariacore.gl_detail aid ON aid.client_no     =acct.client_no AND aid.invoice_no   =ai.invoice_no WHERE acct.client_no = "+client_no+" and "+query_string
			   }
			   rs = db.executePlaQuery(qry)
		   
		   ResultSetMetaData meta = rs.getMetaData()
		   int columns = meta.getColumnCount();
		   
		   while (rs.next())
		   {
			   for(int u=1; u<=columns; ++u)
			   {
				   if(rs.getObject(u)!=null)
					   INVOICES.add(rs.getObject(u));
			   }
		   }
		   Collections.sort(INVOICES);
		   
			  
			  logi.logInfo  "INVOICES in the list "+INVOICES
			  int loops=1,loopss=1;
			  for(String INVOICE : INVOICES)
			  {
				  if(loopss>offset)
			  {
			  String Query = null;
			   if(query_string.contains("ai.total_due"))
			   {
				   Query="SELECT distinct gl.invoice_no,gl.INVOICE_TYPE_CODE as invoice_type_cd, acct.acct_no,  acct.userid AS user_id,acct.CLIENT_ACCT_ID,  TO_CHAR(gl.from_date, 'YYYY-MM-DD HH24:MI:SSxFF')       AS from_date, TO_CHAR(gl.to_date, 'YYYY-MM-DD HH24:MI:SSxFF') AS to_date,  gl.DEBIT  AS	amount, TO_CHAR(gl.BILL_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS BILL_DATE, TO_CHAR(gl.DUE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS DUE_DATE, TO_CHAR(gl.PAID_DATE, 'YYYY-MM-DD	HH24:MI:SSxFF')       AS PAID_DATE, TO_CHAR(gl.NOTIFY_DATE, 'YYYY-MM-DD HH24:MI:SSxFF')     AS NOTIFY_DATE, gl.CURRENCY AS currency_cd, gl.BAL_FWD AS balance_forward,	actst.BALANCE_FORWARD AS statement_balance_forward, gl.TOTAL_DUE , gl.COMMENTS, gl.LONG_COMMENTS AS additional_comments, TO_CHAR(gl.UPDATE_DATE, 'YYYY-MM-DD	HH24:MI:SSxFF') AS last_updated, actst.STATEMENT_NO AS aria_statement_no,actst.ACCT_STATEMENT_SEQ_STR, actst.SECOND_ACCT_STATEMENT_SEQ_STR, gl.VOIDING_EVENT_NO,gl.CUSTOM_STATUS_LABEL, gl.CLIENT_NOTES, gl.BILLING_GROUP_NO, abg.PROFILE_CDID AS client_billing_group_id FROM ariacore.all_invoices ai JOIN ariacore.gl_detail gld ON	gld.invoice_no = ai.invoice_no JOIN ariacore.gl gl ON gl.invoice_no = gld.invoice_no JOIN ariacore.acct acct ON acct.client_no=ai.client_no AND ai.acct_no	=acct.acct_no JOIN ariacore.acct_address aa ON aa.client_no        =acct.client_no AND aa.acct_no         =acct.acct_no AND aa.ADDRESS_TYPE_CD = 0 AND aa.SOURCE_NO = 0 left JOIN ariacore.acct_statement actst ON actst.client_no   =acct.client_no join ariacore.plan_instance pi on pi.client_no=ai.client_no JOIN ariacore.acct_billing_group abg ON abg.client_no         =acct.client_no AND abg.acct_no          =acct.acct_no AND abg.BILLING_GROUP_NO = ai.billing_group_no WHERE	acct.client_no  = "+client_no+" AND ai.invoice_no = "+INVOICE+""
				   }
			   else{
			   Query="SELECT distinct gl.invoice_no,gl.INVOICE_TYPE_CODE as invoice_type_cd, acct.acct_no,  acct.userid AS user_id,  acct.CLIENT_ACCT_ID,TO_CHAR(gl.from_date, 'YYYY-MM-DD HH24:MI:SSxFF')       AS from_date, TO_CHAR(gl.to_date, 'YYYY-MM-DD HH24:MI:SSxFF') AS to_date,  gl.DEBIT  AS amount, TO_CHAR(gl.BILL_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS BILL_DATE, TO_CHAR(gl.DUE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS DUE_DATE, TO_CHAR(gl.PAID_DATE, 'YYYY-MM-DD HH24:MI:SSxFF')       AS PAID_DATE, TO_CHAR(gl.NOTIFY_DATE, 'YYYY-MM-DD HH24:MI:SSxFF')     AS NOTIFY_DATE, gl.CURRENCY AS currency_cd, gl.BAL_FWD AS balance_forward,	actst.BALANCE_FORWARD AS statement_balance_forward, gl.TOTAL_DUE , gl.COMMENTS, gl.LONG_COMMENTS AS additional_comments, TO_CHAR(gl.UPDATE_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS last_updated, actst.STATEMENT_NO AS aria_statement_no,actst.ACCT_STATEMENT_SEQ_STR, actst.SECOND_ACCT_STATEMENT_SEQ_STR, gl.VOIDING_EVENT_NO,gl.CUSTOM_STATUS_LABEL, gl.CLIENT_NOTES, gl.BILLING_GROUP_NO, abg.PROFILE_CDID AS client_billing_group_id FROM ariacore.all_invoices ai JOIN ariacore.gl_detail gld ON	gld.invoice_no = ai.invoice_no JOIN ariacore.gl gl ON gl.invoice_no = gld.invoice_no JOIN ariacore.acct acct ON acct.client_no=ai.client_no AND ai.acct_no	=acct.acct_no JOIN ariacore.acct_address aa ON aa.client_no        =acct.client_no AND aa.acct_no         =acct.acct_no AND aa.ADDRESS_TYPE_CD = 0 AND aa.SOURCE_NO	  = 0 left JOIN ariacore.acct_statement actst ON actst.client_no   =acct.client_no AND actst.acct_no    =acct.acct_no AND actst.INVOICE_NO = ai.INVOICE_NO join	ariacore.plan_instance pi on pi.client_no=ai.client_no JOIN ariacore.acct_billing_group abg ON abg.client_no         =acct.client_no AND abg.acct_no	=acct.acct_no AND abg.BILLING_GROUP_NO = ai.billing_group_no WHERE acct.client_no  = "+client_no+" AND ai.invoice_no = "+INVOICE+""
			   }
			   ResultSet resultSet = db.executePlaQuery(Query)
			   ResultSetMetaData md = resultSet.getMetaData()
			   int columns1 = md.getColumnCount();
	  
	  
			   /*while (resultSet.next())
			   {
				   for(int u=1; u<=columns1; ++u)
				   {
					   if(resultSet.getObject(u)!=null)
						   line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
				   }
			   }*/
			   String query_test = null
			   if (query_string.contains("atn.EVENT_NO") || query_string.contains("aid.PURCHASE_ORDER_NO"))
			   query_test = "SELECT count(*) FROM ariacore.gl_detail aid JOIN ariacore.client_service cs ON cs.client_no  =aid.client_no AND cs.SERVICE_NO=aid.SERVICE_NO JOIN ARIACORE.CHART_OF_ACCTS COA ON coa.CUSTOM_TO_CLIENT_NO=aid.client_no AND COA.COA_ID            =CS.COA_ID LEFT JOIN ariacore.acct_transaction atn ON atn.client_no         =aid.client_no AND atn.invoice_no       =aid.invoice_no AND aid.INVOICE_DETAIL_NO=atn.SOURCE_NO JOIN ariacore.transaction_types tt ON tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE JOIN ariacore.plan_instance pi ON pi.client_no        =aid.client_no AND pi.PLAN_INSTANCE_NO=aid.master_plan_instance_no WHERE aid.client_no = "+client_no+" AND aid.invoice_no = "+INVOICE+" and "+query_string
			   else
			   query_test = "SELECT count(*) from ariacore.gl_detail where invoice_no="+INVOICE
			   int counts= Integer.parseInt(db.executeQueryP2(query_test).toString())
			   int kz=1
			   for(int loop2=1;loop2<=counts;loop2++)
			   {
				   String  DB_INVOICE_ITEMS_ROW_m=""
	  
				   String invoice1 = INVOICE
				   ResultSet rs1=db.executePlaQuery("select surcharge_rate_seq_no as surcharge,orig_coupon_cd as coupon,orig_client_sku as sku from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+INVOICE)
				   logi.logInfo  "Printing rs1" + rs1
				   ResultSetMetaData md1 = resultSet.getMetaData()
				   int column = md1.getColumnCount();
				   
				   while (rs1.next())
				   {
	  
					   for(int u=1; u<=column; ++u)
					   {
					   logi.logInfo  "Inside for loop"
							  if(rs1.getObject(u)!=null)
								  check.put(md1.getColumnName(u).toLowerCase(),rs1.getObject(u));
						   }
				   }
					  int check_size = check.size()
					  logi.logInfo  "Check size value in the list "+ check_size
					  if(check_size==0)
					  {
						  logi.logInfo  "Check size value is 0"
						  if(query_string.contains("gld.PURCHASE_ORDER_NO") )
						  {
							  logi.logInfo  "Inside if condition for Event no"
							  INVOICE+=" and "+query_string}
						  if( query_string.contains("pi.PLAN_INSTANCE_CDID") || query_string.contains("atn.EVENT_NO"))
						  {
							  query1 = "SELECT aid.SEQ_NUM AS line_item_no,aid.RATE_SEQ_NO as rate_schedule_tier_no, aid.DEBIT AS amount, aid.COMMENTS, aid.PLAN_NAME, aid.PLAN_NO,cs.service_name,aid.service_no, coa.coa_id,coa.CLIENT_COA_CODE AS ledger_code, COA.COA_DESCRIPTION, aid.USAGE_UNITS, aid.USAGE_RATE, aid.USAGE_TYPE   AS usage_type_no, TO_CHAR(aid.START_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS START_DATE, TO_CHAR(aid.END_DATE, 'YYYY-MM-DD HH24:MI:SSxFF')   AS END_DATE, aid.ORIG_CLIENT_SKU AS client_sku, aid.ORDER_NO,  aid.ITEM_NO,  aid.BASE_PLAN_UNITS, aid.PRORATION_FACTOR, aid.ACTUAL_ADV_PERIOD_TOTAL_DAYS AS adv_billing_period_total_days, aid.PRORATION_REMAINING_DAYS, tt.DESCRIPTION   AS TRANSACTION_TYPE, aid.master_plan_instance_no AS master_plan_instance_id, pi.PLAN_INSTANCE_CDID       AS client_master_plan_instance_id,  atn.EVENT_NO                AS invoice_transaction_id,  aid.PURCHASE_ORDER_NO       AS po_num,  aid.RATE_SCHEDULE_NO FROM ariacore.gl_detail aid JOIN ariacore.client_service cs ON cs.client_no  =aid.client_no AND cs.SERVICE_NO=aid.SERVICE_NO JOIN ARIACORE.CHART_OF_ACCTS COA ON coa.CUSTOM_TO_CLIENT_NO=aid.client_no AND COA.COA_ID            =CS.COA_ID LEFT JOIN ariacore.acct_transaction atn ON atn.client_no         =aid.client_no AND atn.invoice_no       =aid.invoice_no AND aid.INVOICE_DETAIL_NO=atn.SOURCE_NO JOIN ariacore.transaction_types tt ON tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE JOIN ariacore.plan_instance pi ON pi.client_no        =aid.client_no AND pi.PLAN_INSTANCE_NO=aid.master_plan_instance_no WHERE aid.client_no    = "+client_no+" AND aid.invoice_no = "+INVOICE+" AND aid.SEQ_NUM = "+loop2+" and "+query_string
						  }
						  else
						  {
						  query1 = "SELECT gld.SEQ_NUM AS line_item_no,gld.RATE_SEQ_NO as rate_schedule_tier_no, gld.DEBIT AS amount, gld.COMMENTS, gld.PLAN_NAME, gld.PLAN_NO,cs.service_name,gld.service_no, coa.coa_id,coa.CLIENT_COA_CODE AS ledger_code, COA.COA_DESCRIPTION, gld.USAGE_UNITS, gld.USAGE_RATE, gld.USAGE_TYPE   AS usage_type_no, TO_CHAR(gld.START_DATE, 'YYYY-MM-DD HH24:MI:SSxFF') AS START_DATE, TO_CHAR(gld.END_DATE, 'YYYY-MM-DD HH24:MI:SSxFF')   AS END_DATE, gld.ORIG_CLIENT_SKU AS client_sku, gld.ORDER_NO,  gld.ITEM_NO,  gld.BASE_PLAN_UNITS, gld.PRORATION_FACTOR, gld.ACTUAL_ADV_PERIOD_TOTAL_DAYS AS adv_billing_period_total_days, gld.PRORATION_REMAINING_DAYS, tt.DESCRIPTION   AS TRANSACTION_TYPE, gld.master_plan_instance_no AS master_plan_instance_id, pi.PLAN_INSTANCE_CDID       AS client_master_plan_instance_id,  atn.EVENT_NO                AS invoice_transaction_id,  gld.PURCHASE_ORDER_NO       AS po_num,  gld.RATE_SCHEDULE_NO FROM ariacore.gl_detail gld JOIN ariacore.client_service cs ON cs.client_no  =gld.client_no AND cs.SERVICE_NO=gld.SERVICE_NO JOIN ARIACORE.CHART_OF_ACCTS COA ON coa.CUSTOM_TO_CLIENT_NO=gld.client_no AND COA.COA_ID            =CS.COA_ID LEFT JOIN ariacore.acct_transaction atn ON atn.client_no         =gld.client_no AND atn.invoice_no       =gld.invoice_no AND gld.INVOICE_DETAIL_NO=atn.SOURCE_NO JOIN ariacore.transaction_types tt ON tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE JOIN ariacore.plan_instance pi ON pi.client_no        =gld.client_no AND pi.PLAN_INSTANCE_NO=gld.master_plan_instance_no WHERE gld.client_no    = "+client_no+" AND gld.invoice_no = "+INVOICE+" AND gld.SEQ_NUM = "+loop2
						  }
						  logi.logInfo  "After Check size value is 0"
	  
					  }
					  if(check_size==1){
						  logi.logInfo  "Check size value is 1"
						  for ( String key : check.keySet() ) {
							  logi.logInfo  "key Value "+key
							  logi.logInfo  "check.keySet "+check.keySet()
							  logi.logInfo  "loop2 Value "+loop2
							  logi.logInfo  "query1 Value "+ query1
							  
							  switch(key)
							  {
								  case "surcharge": query1 = "SELECT distinct aid.SEQ_NUM AS line_item_no, aid.DEBIT        AS amount, aid.COMMENTS, als.service_name,aid.service_no,coa.coa_id, coa.CLIENT_COA_CODE AS ledger_code, COA.COA_DESCRIPTION,tt.DESCRIPTION  AS TRANSACTION_TYPE, aid.master_plan_instance_no AS master_plan_instance_id,pi.PLAN_INSTANCE_CDID       AS client_master_plan_instance_id, atn.EVENT_NO AS invoice_transaction_id FROM ariacore.gl_detail aid JOIN ariacore.services cs ON cs.SERVICE_NO=aid.SERVICE_NO join ariacore.all_service als on als.service_no=cs.service_no JOIN ARIACORE.CHART_OF_ACCTS COA ON cs.coa_id = coa.coa_id LEFT JOIN ariacore.acct_transaction atn ON atn.client_no         =aid.client_no AND atn.invoice_no       =aid.invoice_no AND aid.INVOICE_DETAIL_NO=atn.SOURCE_NO JOIN ariacore.transaction_types tt ON tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE JOIN ariacore.plan_instance pi ON pi.client_no        =aid.client_no AND pi.PLAN_INSTANCE_NO=aid.master_plan_instance_no WHERE aid.client_no    = "+client_no+" AND aid.invoice_no = "+INVOICE+" AND aid.SEQ_NUM = "+loop2+" and "+query_string;
								  
								  break;
								  
								  case "coupon":
								  
									  if( query_string.contains("pi.PLAN_INSTANCE_CDID") || query_string.contains("atn.EVENT_NO"))
									  {query1 = "SELECT distinct aid.SEQ_NUM AS line_item_no, aid.DEBIT        AS amount, aid.COMMENTS, als.service_name,aid.service_no,coa.coa_id, coa.CLIENT_COA_CODE AS ledger_code, COA.COA_DESCRIPTION,tt.DESCRIPTION  AS TRANSACTION_TYPE, aid.master_plan_instance_no AS master_plan_instance_id,pi.PLAN_INSTANCE_CDID       AS client_master_plan_instance_id, atn.EVENT_NO AS invoice_transaction_id FROM ariacore.gl_detail aid JOIN ariacore.services cs ON cs.SERVICE_NO=aid.SERVICE_NO join ariacore.all_service als on als.service_no=cs.service_no JOIN ARIACORE.CHART_OF_ACCTS COA ON cs.coa_id = coa.coa_id LEFT JOIN ariacore.acct_transaction atn ON atn.client_no         =aid.client_no AND atn.invoice_no       =aid.invoice_no AND aid.INVOICE_DETAIL_NO=atn.SOURCE_NO JOIN ariacore.transaction_types tt ON tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE JOIN ariacore.plan_instance pi ON pi.client_no        =aid.client_no AND pi.PLAN_INSTANCE_NO=aid.master_plan_instance_no WHERE aid.client_no    = "+client_no+" AND aid.invoice_no = "+INVOICE+" AND aid.SEQ_NUM = "+loop2+" and "+query_string}
									  else
									  {query1 = "SELECT distinct gld.SEQ_NUM AS line_item_no, gld.DEBIT        AS amount, gld.COMMENTS, als.service_name,gld.service_no,coa.coa_id, coa.CLIENT_COA_CODE AS ledger_code, COA.COA_DESCRIPTION,tt.DESCRIPTION  AS TRANSACTION_TYPE, gld.master_plan_instance_no AS master_plan_instance_id,pi.PLAN_INSTANCE_CDID       AS client_master_plan_instance_id, atn.EVENT_NO AS invoice_transaction_id FROM ariacore.gl_detail gld JOIN ariacore.services cs ON cs.SERVICE_NO=gld.SERVICE_NO join ariacore.all_service als on als.service_no=cs.service_no JOIN ARIACORE.CHART_OF_ACCTS COA ON cs.coa_id = coa.coa_id LEFT JOIN ariacore.acct_transaction atn ON atn.client_no         =gld.client_no AND atn.invoice_no       =gld.invoice_no AND gld.INVOICE_DETAIL_NO=atn.SOURCE_NO JOIN ariacore.transaction_types tt ON tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE JOIN ariacore.plan_instance pi ON pi.client_no        =gld.client_no AND pi.PLAN_INSTANCE_NO=gld.master_plan_instance_no WHERE gld.client_no    = "+client_no+" AND gld.invoice_no = "+INVOICE+" AND gld.SEQ_NUM = "+loop2}
			  
									  logi.logInfo  "query1 Value first switch "+ query1
									  break;
								  
								  case "nso": query1 = "SELECT distinct aid.SEQ_NUM AS line_item_no, aid.DEBIT        AS amount, aid.COMMENTS, als.service_name,aid.service_no,coa.coa_id, coa.CLIENT_COA_CODE AS ledger_code, COA.COA_DESCRIPTION,tt.DESCRIPTION  AS TRANSACTION_TYPE, aid.master_plan_instance_no AS master_plan_instance_id,pi.PLAN_INSTANCE_CDID       AS client_master_plan_instance_id, atn.EVENT_NO AS invoice_transaction_id FROM ariacore.gl_detail aid JOIN ariacore.services cs ON cs.SERVICE_NO=aid.SERVICE_NO join ariacore.all_service als on als.service_no=cs.service_no JOIN ARIACORE.CHART_OF_ACCTS COA ON cs.coa_id = coa.coa_id LEFT JOIN ariacore.acct_transaction atn ON atn.client_no         =aid.client_no AND atn.invoice_no       =aid.invoice_no AND aid.INVOICE_DETAIL_NO=atn.SOURCE_NO JOIN ariacore.transaction_types tt ON tt.TRANSACTION_TYPE=atn.TRANSACTION_TYPE JOIN ariacore.plan_instance pi ON pi.client_no        =aid.client_no AND pi.PLAN_INSTANCE_NO=aid.master_plan_instance_no WHERE aid.client_no    = "+library.Constants.Constant.client_no+" AND aid.invoice_no = "+INVOICE+" AND aid.SEQ_NUM = "+loop2+" and "+query_string; break;
							  }
						  }
					  }
					  ResultSet rs2 = db.executePlaQuery(query1)
					  logi.logInfo  "started rs2"
					  ResultSetMetaData mdata = rs2.getMetaData();
					  logi.logInfo  "started rs2 line 2"
					  int columns_count = mdata.getColumnCount();
					  logi.logInfo  "Print values of rs2" + columns_count
	  
					  
					  while (rs2.next())
					  {
	  
						  for(int u=1; u<=columns_count; ++u)
						  {
	  
							  if(rs2.getObject(u)!=null)
							  {
								  boolean is_having=true
								  int k=1;
								  while (is_having)
								  {
									  if(k==1)
									  {if(!line_item.keySet().asList().contains(md.getColumnName(u).toLowerCase())){is_having=false}}
									  else {if(!line_item.keySet().asList().contains(md.getColumnName(u).toLowerCase()+"["+k.toString()+"]")){is_having=false}}
									  k+=1;
								  }
								  k-=1;
								  logi.logInfo "the value of k "+k;
								  logi.logInfo "putting in output "+rs2.getObject(u);
								  if(k>1){
									  logi.logInfo  "enter into if condition with k"
									  line_item.put((md.getColumnName(u)).toLowerCase()+"["+k.toString()+"]",(rs2.getObject(u)));
									  
								  }
								  else{
									  line_item.put((md.getColumnName(u)).toLowerCase(),(rs2.getObject(u)));
	  
								  }
	  
							  
							  }
						  }
						  logi.logInfo  "Printing line item"+line_item.toString()
					  }
				  line.sort();
				  if(kz>1)
				  line.put("invoice_line_m_row["+kz+"]", line_item.sort())
				  else line.put("invoice_line_m_row", line_item.sort())
				  line_item = new LinkedHashMap();
				  kz++;
			  }
			  
			   
				line.sort();
				if(loops==1)row_item.put("invoice_line_m",line.sort())
				else
				row_item.put("invoice_line_m["+(loops)+"]",line.sort())
				line = new LinkedHashMap();
				if(loops==limit)break;
				loops+=1;
			  }
			  loopss+=1;
			  }
			  row_item=row_item.sort();
			  return row_item
			  
		  }
	   
	   /**
		* for objQuery get_invoice_information_m
		* @param no param required
		* @return total returned records
		*/
	   public def md_get_invoice_information_total_m(String param)
	   {
		   logi.logInfo("********md_get_invoice_information_m Starts**************")
		   ConnectDB db = new ConnectDB()
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   LinkedHashMap line_item = new LinkedHashMap()
		   LinkedHashMap row_item = new LinkedHashMap()
		   LinkedHashMap output = new LinkedHashMap()
		   LinkedHashMap check = new LinkedHashMap()
		   LinkedHashMap invoice_info_row = new LinkedHashMap()
		   LinkedHashMap invoice_line_m_row = new LinkedHashMap()
		   String query1 = null
		   String query_string = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/query_string[1]")
		   int offset = 0;
		   int limit = 100;
		   String offset_s =getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/offset[1]")
		   String limit_s = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/limit[1]")
		   if(!offset_s.contains("NoVal"))offset=Integer.parseInt(offset_s);
		   if(!limit_s.contains("NoVal"))limit=Integer.parseInt(limit_s);
		   query_string = query_string.replaceAll("invoice_type_cd = F","ai.invoice_type_code = 'F'");
		   query_string = query_string.replaceAll("custom_status_label","ai.CUSTOM_STATUS_LABEL");
		   query_string = query_string.replaceAll("client_notes","ai.CLIENT_NOTES");
		   //query_string = query_string.replaceAll("Test","Test");
		   //query_string = query_string.replaceAll("testnote","'testnote'");
		   query_string = query_string.replaceAll("second_acct_statement_seq_str","sass");
		   query_string = query_string.replaceAll("sass","actst.second_acct_statement_seq_str");
		   query_string = query_string.replaceAll("total_due","ai.total_due");
		   if(!query_string.contains("order by"))
		   {query_string = query_string.replaceAll("001 order by invoice_no","001");
		   query_string = query_string.replaceAll("invoice_no","ai.invoice_no");
		   query_string = query_string.replaceAll("acct_no","acct.acct_no");
		   query_string = query_string.replaceAll("user_id","acct.userid");
		   query_string = query_string.replaceAll("client_acct_id","acct.CLIENT_ACCT_ID");
		   //query_string = query_string.replaceAll("invoice_type","ai.REAL_PEND_IND");
		   query_string = query_string.replaceAll("invoice_type_cd","ai.invoice_type_code");
		   query_string = query_string.replaceAll("second_acct_statement_seq_str","sass");
		   query_string = query_string.replaceAll("acct_statement_seq_str","actst.acct_statement_seq_str");
		   query_string = query_string.replaceAll("sass","actst.second_acct_statement_seq_str");
		   query_string = query_string.replaceAll("bill_company_name","ai.BILL_COMPANY_NAME");
		   query_string = query_string.replaceAll("pay_method_type","pm.METHOD_ID");
		   query_string = query_string.replaceAll("pay_method_name","ai.PAY_METHOD_NAME");
		   query_string = query_string.replaceAll("aria_statement_no","actst.statement_no");
		   query_string = query_string.replaceAll("client_master_plan_instance_id","pi.PLAN_INSTANCE_CDID");
		   query_string = query_string.replaceAll("master_plan_instance_id","aid.master_plan_instance_no");
		   query_string = query_string.replaceAll("po_num","aid.PURCHASE_ORDER_NO");
		   query_string = query_string.replaceAll("invoice_transaction_id","atn.EVENT_NO");
		   query_string = query_string.replaceAll("last_updated","ai.UPDATE_DATE");
		   query_string = query_string.replaceAll("total_due","ai.total_due");
		   query_string = query_string.replaceAll("\"","");
		   query_string = query_string.replaceAll("= ","= '");
		   query_string = query_string.replaceAll("> ","> '");
		   query_string = query_string.replaceAll("< ","< '");
		   query_string = query_string.replaceAll(" and","' and");
		   query_string += "'";
		   }
		   ArrayList INVOICES = new ArrayList();
		   ResultSet rs
		   Collections.sort(INVOICES);
		   if(query_string.contains("ai.UPDATE_DATE") )
		   {
			   query_string = query_string.replaceAll("=","like")
			   query_string = query_string.replaceAll(" '"," '%")
			   query_string = query_string.replaceAll("' ","%' ")
		   }
		   else
		   {
			   
		   String qry = null;
		   if(query_string.contains("ai.total_due")||query_string.contains("ai.invoice_type_code = 'F'"))
		   {
			   qry = "SELECT distinct ai.invoice_no FROM ariacore.gl ai WHERE ai.client_no = "+client_no+" and	"+query_string
			   }
		   else{
			   qry = "SELECT distinct ai.invoice_no FROM ariacore.gl ai JOIN ariacore.acct acct ON acct.client_no=ai.client_no AND ai.acct_no   =acct.acct_no	JOIN ariacore.acct_address aa ON aa.client_no        =acct.client_no AND aa.acct_no         =acct.acct_no AND aa.ADDRESS_TYPE_CD = 0 AND aa.SOURCE_NO       = 0 JOIN ariacore.acct_statement actst ON actst.client_no=acct.client_no and actst.acct_no = ai.acct_no join ariacore.acct_transaction atn on atn.client_no=ai.client_no and atn.invoice_no=ai.invoice_no join ariacore.plan_instance pi on pi.client_no=ai.client_no JOIN ariacore.acct_billing_group abg ON abg.client_no         =acct.client_no	AND abg.acct_no          =acct.acct_no AND abg.BILLING_GROUP_NO = ai.billing_group_no JOIN ariacore.gl_detail aid ON aid.client_no     =acct.client_no AND aid.invoice_no   =ai.invoice_no WHERE acct.client_no = "+client_no+" and "+query_string
			   }
			   rs = db.executePlaQuery(qry)
		   }
		   ResultSetMetaData meta = rs.getMetaData()
		   int columns = meta.getColumnCount();
		   while (rs.next())
		   {
			   for(int u=1; u<=columns; ++u)
			   {
				   if(rs.getObject(u)!=null)
					   INVOICES.add(rs.getObject(u));
			   }
		   }
		   
		   Collections.sort(INVOICES);
	  
		   logi.logInfo  "INVOICES in the list "+INVOICES
		   logi.logInfo  "Returning the ans as  "+INVOICES.size()
		   if(limit<INVOICES.size())
		   return limit
		   else
				  return INVOICES.size();
				  logi.logInfo("********md_get_invoice_information_m Ends**************")
			  
		  }
	   
	 
	   
	   /**
		* for objQuery get_invoice_information_m
		* @param no param required
		* @return total records node value
		*/
	   public String md_get_invoice_information_total_records_m(String param)
	   {
	   logi.logInfo("********md_get_invoice_information_total_records_m Starts**************")
		   ConnectDB db = new ConnectDB()
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   LinkedHashMap line_item = new LinkedHashMap()
		   LinkedHashMap line = new LinkedHashMap()
		   LinkedHashMap row_item = new LinkedHashMap()
		   LinkedHashMap output = new LinkedHashMap()
		   LinkedHashMap check = new LinkedHashMap()
		   LinkedHashMap invoice_info_row = new LinkedHashMap()
		   LinkedHashMap invoice_line_m_row = new LinkedHashMap()
		   String query1 = null
		   int a = 0
		   int offset = 0;
		   int limit = 100;
		   String offset_s =getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/offset[1]")
		   String limit_s = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/limit[1]")
		   if(!offset_s.contains("NoVal"))offset=Integer.parseInt(offset_s);
		   if(!limit_s.contains("NoVal"))limit=Integer.parseInt(limit_s);
		   
		  if(!offset_s.contains("NoVal"))offset=Integer.parseInt(offset_s);
		   if(!limit_s.contains("NoVal"))limit=Integer.parseInt(limit_s);
		   
		   String query_string = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/query_string[1]")
		   query_string = query_string.replaceAll("invoice_type_cd = F","ai.invoice_type_code = 'F'");
		   query_string = query_string.replaceAll("custom_status_label","ai.CUSTOM_STATUS_LABEL");
		   query_string = query_string.replaceAll("client_notes","ai.CLIENT_NOTES");
		   query_string = query_string.replaceAll("Test","'Test'");
		   query_string = query_string.replaceAll("testnote","'testnote'");
		   query_string = query_string.replaceAll("001 order by invoice_no","001");
		   logi.logInfo("Outside if conditionss")
		   if(!query_string.contains("order by")){
			   
		   logi.logInfo("Inside if conditionss")
		   
		   query_string = query_string.replaceAll("invoice_no","ai.invoice_no");
		   query_string = query_string.replaceAll("acct_no","acct.acct_no");
		   query_string = query_string.replaceAll("user_id","acct.userid");
		   query_string = query_string.replaceAll("client_acct_id","acct.CLIENT_ACCT_ID");
		   query_string = query_string.replaceAll("second_acct_statement_seq_str","sass");
		   query_string = query_string.replaceAll("acct_statement_seq_str","actst.acct_statement_seq_str");
		   query_string = query_string.replaceAll("sass","actst.second_acct_statement_seq_str");
		   query_string = query_string.replaceAll("bill_company_name","ai.BILL_COMPANY_NAME");
		   query_string = query_string.replaceAll("pay_method_type","pm.METHOD_ID");
		   query_string = query_string.replaceAll("pay_method_name","ai.PAY_METHOD_NAME");
		   query_string = query_string.replaceAll("custom_status_label","ai.CUSTOM_STATUS_LABEL");
		   query_string = query_string.replaceAll("client_notes","ai.CLIENT_NOTES");
		   query_string = query_string.replaceAll("aria_statement_no","actst.statement_no");
		   query_string = query_string.replaceAll("client_master_plan_instance_id","pi.PLAN_INSTANCE_CDID");
		   query_string = query_string.replaceAll("master_plan_instance_id","aid.master_plan_instance_no");
		   query_string = query_string.replaceAll("po_num","aid.PURCHASE_ORDER_NO");
		   query_string = query_string.replaceAll("invoice_transaction_id","atn.EVENT_NO");
		   query_string = query_string.replaceAll("last_updated","ai.UPDATE_DATE");
		   query_string = query_string.replaceAll("total_due","ai.total_due");
		   query_string = query_string.replaceAll("\"","");
		   query_string = query_string.replaceAll("= ","= '");
		   query_string = query_string.replaceAll("> ","> '");
		   query_string = query_string.replaceAll("< ","< '");
		   query_string = query_string.replaceAll(" and","' and");
		   query_string += "'";
		   }
		   
		   
		   ArrayList INVOICES = new ArrayList();
		   ResultSet rs
		  
		   if(query_string.contains("ai.UPDATE_DATE") )
		   {
			   query_string = query_string.replaceAll("=","like")
			   query_string = query_string.replaceAll(" '"," '%")
			   query_string = query_string.replaceAll("' ","%' ")
		   }
		  
		   query_string = query_string.replaceAll("''","'")
		   String qry = null;
		   if(query_string.contains("ai.total_due")||query_string.contains("ai.invoice_type_code = 'F'"))
		   {
			   qry = "SELECT COUNT(ai.invoice_no) FROM ariacore.gl ai WHERE ai.client_no = "+client_no+" and	"+query_string
			   }
		   else{
			   qry = "SELECT COUNT(*) FROM ariacore.gl ai JOIN ariacore.acct_statement actst ON actst.invoice_no  =ai.invoice_no and actst.client_no=ai.client_no WHERE ai.client_no  = "+client_no+" and "+query_string
			   }
		  
		   
		   a = Integer.parseInt(db.executeQueryP2(qry))
		   return a;
	   }
	  
	
	   LinkedHashMap md_Contract_Plan_Inst_Info2_m(String acct_hierarchy1)
	   {
		   
		   
		   logi.logInfo("INTO md_Contract_Plan_Inst_Info_m22222 . . . ")
		   String acct_hierarchy=acct_hierarchy1.split("#")[0]
		   String seq=acct_hierarchy1.split("#")[1]
		   String seq2=acct_hierarchy1.split("#")[2]
		   logi.logInfo("seq "+seq)
			   LinkedHashMap all_acct_contracts_temp_hm =new LinkedHashMap()
			   LinkedHashMap a1 =new LinkedHashMap()
			   ConnectDB db = new ConnectDB()
			   String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(acct_hierarchy,null)
			   logi.logInfo("Given Acct no:" + acct_no)
			   //int count =1
	   
	   
				   
				   String contract_no_query="SELECT A.CONTRACT_NO FROM (SELECT  ROW_NUMBER() OVER(ORDER BY CONTRACT_NO) AS SEQ,CONTRACT_NO FROM ARIACORE.ACCT_MULTI_PLAN_CONTRACTS WHERE ACCT_NO="+acct_no+") A WHERE A.SEQ="+seq
				   String contract=db.executeQueryP2(contract_no_query)
	   
				   logi.logInfo("Contract number : "+contract)
				   
				   String plan_instance_query="select a.plan_instance_no,a.client_plan_instance_id,a.plan_name, a.plan_instance_status_cd,a.plan_instance_status_label from (Select row_number() over (order by  pi.plan_instance_no) as seq ,  pi.plan_instance_no as plan_instance_no,pi.plan_instance_cdid as client_plan_instance_id,cp.plan_name as plan_name,pi.status_cd as plan_instance_status_cd,g.label as plan_instance_status_label from ariacore.plan_instance pi join ariacore.ALL_PLAN_INST_ACTIVE_CONTRACTS ac on pi.plan_instance_no = ac.plan_instance_no and pi.client_no = ac.client_no and pi.acct_no=ac.acct_no join ariacore.client_plan cp on pi.plan_no =cp.plan_no and pi.client_no=cp.client_no join ariacore.global_plan_status_code g on pi.status_cd =g.plan_status_cd  where pi.acct_no ="+acct_no+" and ac.CONTRACT_NO ="+contract+") A where A.seq="+seq2
				   ResultSet rs2 = db.executePlaQuery(plan_instance_query)
				   ResultSetMetaData md2 = rs2.getMetaData();
				   int columns3 = md2.getColumnCount();
				   int r=1
				   while (rs2.next())
				   {
					   all_acct_contracts_temp_hm =new LinkedHashMap()
					   logi.logInfo("Created contract )"+count)
					   
					   for(int j=1; j<=columns3; j++)
								   {
									   if((rs2.getObject(j))!=null)
									   {
										   all_acct_contracts_temp_hm.put((md2.getColumnName(j)).toLowerCase(),(rs2.getObject
   
   (j)));
									   }
								   }
								   
								   logi.logInfo "BEFORRRRRRRRRREEEEEEEEEEEEE :"+all_acct_contracts_temp_hm
								   all_acct_contracts_temp_hm=all_acct_contracts_temp_hm.sort()
								   logi.logInfo "AFTERRRRRRRRRRRRRRRRRRRRR :"+all_acct_contracts_temp_hm
								   a1.put("contract_plan_instance_info_row["+count+"]",all_acct_contracts_temp_hm )
								   ++count
								   
				   }
				   
	   
				   
							   logi.logInfo "**** out of  get_all_acct_contracts_queries_m **** "
			   
			   
			   logi.logInfo("temp1")
			   
			   a1=a1.sort()
			   logi.logInfo("sorteed"+a1)
			   
			   //return a1
			   all_acct_contracts_temp_hm=all_acct_contracts_temp_hm.sort()
			   return all_acct_contracts_temp_hm
			   
	   }
	   /** DB invoice line items for MPIAppliedTxns 579331
		* @param s
		* @return values from DB for all payments and transactions
		*/
	   public  LinkedHashMap md_mpiappliedtrxn_579331_DB_m(String s)
		  {
			logi.logInfo("Inside *********md_mpiappliedtrxn_579331_DB_m*******"+s)
			LinkedHashMap<String,String> alltrnx=new LinkedHashMap<String,String>()
			ConnectDB db = new ConnectDB()
			String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
			String cquery = "SELECT count(*) as counts FROM ARIACORE.ACCT_TRANSACTION AT JOIN ARIACORE.PAYMENTS AP ON AT.SOURCE_NO= AP.PAYMENT_ID AND AT.CLIENT_NO = AP.CLIENT_NO"+
							  " LEFT JOIN ARIACORE.TRANSACTION_TYPES TT ON"+
							  " TT.TRANSACTION_TYPE = AT.TRANSACTION_TYPE"+
							  " WHERE AT.ACCT_NO         = "+acct_no+
							  " AND AT.TRANSACTION_TYPE IN (3,12)"
			String q_count = db.executeQueryP2(cquery).toString()
			int row_count = Integer.parseInt(q_count)
			logi.logInfo("value converted to integer : "+row_count)
			int s1;
			for(int i=1;i<=row_count;i++){
				logi.logInfo("entered into the loop")
				 //s1=i-1
				logi.logInfo("counting in md_mpiappliedtrxn_579331_DB_m for account : "+i)
				alltrnx.put("Line Item"+i+"",md_appliedtxnLineItm_m(acct_no,i))
		   }
		   return alltrnx.sort()
		  }
	   
	   public LinkedHashMap md_appliedtxnLineItm_m (String str, int i){
		   
		   
		   LinkedHashMap<String,String> row=new LinkedHashMap<String,String>()
		   ConnectDB db = new ConnectDB()
		   String query1 = "SELECT F.MPIAppOrigTxnNo, F.MPIAppOrigTxnTypeDesc,F.MPIAppOrigTxnDt,F.MPIAppOrigTxnAmt,"+
						  " F.MPIAppliedAmt,F.MPIAppliedDt FROM"+
						  " (SELECT AT.EVENT_NO AS MPIAppOrigTxnNo,TT.DESCRIPTION AS MPIAppOrigTxnTypeDesc,"+
						  " TO_CHAR(TO_DATE(TO_CHAR(CREATE_DATE,'MM/fmDD/YYYY'),'MM/DD/YYYY'),'FMMM/DD/YYYY') AS MPIAppOrigTxnDt,"+
						  " AT.AMOUNT AS MPIAppOrigTxnAmt,AT.APPLIED_AMOUNT AS MPIAppliedAmt,"+
						  " TO_CHAR(TO_DATE(TO_CHAR(FULLY_APPLIED_DATE,'MM/fmDD/YYYY'),'MM/DD/YYYY'),'FMMM/DD/YYYY') AS MPIAppliedDt,"+
						  " ROW_NUMBER() OVER (ORDER BY AT.EVENT_NO) AS SEQ"+
						  " FROM ARIACORE.ACCT_TRANSACTION AT"+
						  " JOIN ARIACORE.PAYMENTS AP"+
						  " ON AT.SOURCE_NO          = AP.PAYMENT_ID"+
						  " AND AT.CLIENT_NO         = AP.CLIENT_NO"+
						  " LEFT JOIN ARIACORE.TRANSACTION_TYPES TT ON"+
						  " TT.TRANSACTION_TYPE = AT.TRANSACTION_TYPE"+
						  " WHERE AT.ACCT_NO         = "+str+
						  " AND AT.TRANSACTION_TYPE IN (3,12))F WHERE F.SEQ = "+i
		   ResultSet rs1 = db.executePlaQuery(query1);
		   ResultSetMetaData md1 = rs1.getMetaData();
		   int columns1 = md1.getColumnCount();
		   while (rs1.next()){
			for(int s=1; s<=columns1; ++s){
					logi.logInfo("Value inserting in hash map")
							  if(rs1.getObject(s).toString().equals("null")){
							   logi.logInfo("No value in "+rs1.getObject(s))
								   }else{logi.logInfo("Value inserting in hash map")
								   row.put(md1.getColumnName(s).toUpperCase(),rs1.getObject(s));}
								   logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))}
						}
		   String query3 = "SELECT AP.VOIDING_EVENT_NO FROM ARIACORE.ACCT_TRANSACTION AT JOIN ARIACORE.PAYMENTS AP ON AT.SOURCE_NO = AP.PAYMENT_ID AND AT.CLIENT_NO = AP.CLIENT_NO LEFT JOIN ARIACORE.TRANSACTION_TYPES TT ON TT.TRANSACTION_TYPE = AT.TRANSACTION_TYPE WHERE AT.ACCT_NO= "+str+" AND AT.TRANSACTION_TYPE IN (3,12)"
		   String voidingevent = db.executeQueryP2(query3).toString()
		  
		   logi.logInfo "we got the value "+voidingevent
		   if(voidingevent.equals("null")){
			  logi.logInfo "we got the value"
			  String query2 = "SELECT F.AMOUNT FROM (SELECT AP.AMOUNT,ROW_NUMBER() OVER (ORDER BY AT.EVENT_NO) AS SEQ FROM ARIACORE.ACCT_TRANSACTION AT JOIN ARIACORE.PAYMENTS AP ON AT.SOURCE_NO = AP.PAYMENT_ID AND AT.CLIENT_NO = AP.CLIENT_NO LEFT JOIN ARIACORE.TRANSACTION_TYPES TT ON TT.TRANSACTION_TYPE = AT.TRANSACTION_TYPE WHERE AT.ACCT_NO= "+str+" AND AT.TRANSACTION_TYPE IN (3,12))F WHERE F.SEQ = "+i
			  String amount = db.executeQueryP2(query2).toString()
			  row.put("MPIAPPLIEDTOTAL",amount)
			  //row.put("MPIAPPLIEDTOTAL",amount)
		   }
			  logi.logInfo "printing inner md_mpiappliedtrxn_579331_DB_m hash "+row.sort()
				  return row.sort()
	   }
	   
	   
	   
	   public LinkedHashMap md_BalXferActivity_579331_m (String str){
		   logi.logInfo("Inside *********md_BalXferActivity_579331_m*******"+str)
		   LinkedHashMap<String,String> row=new LinkedHashMap<String,String>()
		   ConnectDB db = new ConnectDB()
		   String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO2_m(str,null)
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   String cquery = "SELECT TO_CHAR(TO_DATE(TO_CHAR(AT.CREATE_DATE,'MM/fmDD/YYYY'),'MM/DD/YYYY'),'FMMM/DD/YYYY')AS BalXferActivityDate,"+
						  " 'Invoice charge line item xfr (acct '||ACC.CHILD_ACCT_NO||')' AS BalXferActivityDesc,"+
						  " AT.AMOUNT AS BalXferActivityAmount,AT.SOURCE_COMMENTS AS BalXferComment"+
						  "  FROM ARIACORE.ACCT_TRANSACTION AT LEFT JOIN ARIACORE.TRANSACTION_TYPES TT ON AT.TRANSACTION_TYPE = TT.TRANSACTION_TYPE"+
						  " JOIN ARIACORE.ALL_CHILD_ACCTS ACC ON ACC.PARENT_ACCT_NO = AT.ACCT_NO AND ACC.CLIENT_NO = AT.CLIENT_NO"+
						  " WHERE AT.CREATE_USER LIKE '%Transfer to Parent%' AND AT.CLIENT_NO = "+client_no+" AND AT.ACCT_NO = "+acct_no
			  ResultSet rs1 = db.executePlaQuery(cquery);
			  ResultSetMetaData md1 = rs1.getMetaData();
			  int columns1 = md1.getColumnCount();
			  while (rs1.next()){
					  for(int s=1; s<=columns1; ++s){
							  logi.logInfo("Value inserting in hash map")
								if(rs1.getObject(s).toString().equals("null")){
								 logi.logInfo("No value in "+rs1.getObject(s))
								}else{logi.logInfo("Value inserting in hash map")
								row.put(md1.getColumnName(s).toUpperCase(),rs1.getObject(s));}
								logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))}
					  }
			  logi.logInfo "printing md_BalXferActivity_579331_m hash "+row.sort()
			  return row.sort()
	   }
	   
	   
	  
	   
	   public LinkedHashMap md_ICreditActivity_57935_m (String str){
		   logi.logInfo("Inside *********md_ICreditActivity_57935_m*******"+str)
		   //separator acct_no&transaction_types 10,-22,22,-21,3
		   String str1 = str.split("&")[0]
		   String types = str.split("&")[1]
		   logi.logInfo ""+str1+" printing types "+types
		   LinkedHashMap<String,String> row=new LinkedHashMap<String,String>()
		   LinkedHashMap<String,String> row1=new LinkedHashMap<String,String>()
		   ConnectDB db = new ConnectDB()
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(str1,null)
		   
		   String iquery = "SELECT MAX(INVOICE_NO) AS INVOICE_NO FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+" AND CLIENT_NO = "+client_no
		   String inv_no = db.executeQueryP2(iquery).toString()
		   String squery = "SELECT STATEMENT_NO FROM ARIACORE.ACCT_STATEMENT WHERE ACCT_NO = "+acct_no+" AND INVOICE_NO = "+inv_no
		   String statement_no = db.executeQueryP2(squery).toString()
		   String counter = "SELECT COUNT(*) AS COUNTS FROM ARIACORE.ACCT_TRANSACTION AT LEFT JOIN ARIACORE.TRANSACTION_TYPES TT ON AT.TRANSACTION_TYPE= TT.TRANSACTION_TYPE WHERE AT.ACCT_NO= "+acct_no+" AND AT.TRANSACTION_TYPE IN ("+types+") AND AT.STATEMENT_NO = "+statement_no+" ORDER BY AT.EVENT_NO"
		   String counters = db.executeQueryP2(counter).toString()
		   int row_count = Integer.parseInt(counters)
		   logi.logInfo("value converted to integer : "+row_count)
		  
			  
			  for(int lon = 1;lon<=row_count;lon++){
				  logi.logInfo "logging inside for loop "+lon
				  String cquery = "SELECT CC.CreditActivityDesc,CC.CreditActivityAmount,CC.CreditActivityComment FROM (SELECT CASE WHEN AT.TRANSACTION_TYPE IN (22) AND AT.INVOICE_NO IS NOT NULL"+
				  " THEN 'Service Credit (applied '||TO_CHAR(TO_DATE(TO_CHAR(AT.RECORDED_DATE,'MM/fmDD/YYYY'),'MM/DD/YYYY'),'FMMM/DD/YYYY')||')' ELSE TT.DESCRIPTION||' (applied '||TO_CHAR(TO_DATE(TO_CHAR(AT.RECORDED_DATE,'MM/fmDD/YYYY'),'MM/DD/YYYY'),'FMMM/DD/YYYY')||')' END AS CreditActivityDesc,"+
				  " CASE WHEN AT.TRANSACTION_TYPE NOT IN (-22) THEN TO_CHAR((AT.AMOUNT * -1),'fm99990.90')"+
				  " ELSE TO_CHAR(AT.AMOUNT,'fm99990.90') END AS CreditActivityAmount,AT.SOURCE_COMMENTS AS CreditActivityComment,TT.TRANSACTION_TYPE,ROW_NUMBER() OVER (ORDER BY AT.EVENT_NO) AS SEQ"+
				  " FROM ARIACORE.ACCT_TRANSACTION AT LEFT JOIN ARIACORE.TRANSACTION_TYPES TT ON AT.TRANSACTION_TYPE   = TT.TRANSACTION_TYPE"+
				  " WHERE AT.ACCT_NO         = "+acct_no+" AND AT.TRANSACTION_TYPE IN ("+types+")"+
				  " AND AT.STATEMENT_NO = "+statement_no+" ORDER BY AT.EVENT_NO )CC WHERE CC.SEQ =  "+lon
				  ResultSet rs1 = db.executePlaQuery(cquery);
				  ResultSetMetaData md1 = rs1.getMetaData();
				  int columns1 = md1.getColumnCount();
	  
			  while (rs1.next()){
					  for(int s=1; s<=columns1; ++s){
							  logi.logInfo("Value inserting in hash map")
								if(rs1.getObject(s).toString().equals("null")){
								 logi.logInfo("No value in "+rs1.getObject(s))
								}else{logi.logInfo("Value inserting in hash map")
								row.put(md1.getColumnName(s).toUpperCase(),rs1.getObject(s));}
								logi.logInfo(rs1.getObject(s)+" value inserted into "+md1.getColumnName(s))}
					  
					  }
			  row1.put("Line Item"+lon+"",row.sort())
			  }
			  logi.logInfo "printing md_ICreditActivity_57935_m hash "+row1.sort()
			  return row1.sort()
	   }
	  
	   public  LinkedHashMap md_getAcctUnitInstanceAll2_m(String str)
	   {
			logi.logInfo("inside the md_getAcctUnitInstanceAll2_m "+str)
			if(str.equals("n")){
				str = ""
				logi.logInfo " "+str
			}else{
			str = "+"+str
			logi.logInfo " "+str
			}
			ConnectDB db = new ConnectDB()
			ArrayList<String> list = new ArrayList<String>()
			logi.logInfo "SECTOR 150001"
			int i = 1
			def client_no=Constant.mycontext.expand('${Properties#Client_No}')
			logi.logInfo("Client_No" +client_no )
			//1.acct_no
			String acct_no = getValueFromRequest("get_acct_pln_unit_inst_all_m"+str,"//acct_no").toString()
			logi.logInfo("acct_no" +acct_no )
			//2.plan_instance_no --
			String plan_instance_no = getValueFromRequest("get_acct_pln_unit_inst_all_m"+str,"//plan_instance_no").toString()
			logi.logInfo("plan_instance_no" +plan_instance_no )
			//3.fulfillment_only
			String fulfilment = getValueFromRequest("get_acct_pln_unit_inst_all_m"+str,"//fulfillment_only").toString()
			logi.logInfo("fulfillment node : " +fulfilment )
			//4.plan_unit_inst_no --
			String plan_unit_inst_no = getValueFromRequest("get_acct_pln_unit_inst_all_m"+str,"//plan_unit_inst_no").toString()
			logi.logInfo("plan_unit_inst_no" +plan_unit_inst_no )
			//5.status_cd
			String status_cd = getValueFromRequest("get_acct_pln_unit_inst_all_m"+str,"//status_cd").toString()
			logi.logInfo("status cd  : " +status_cd )
			//6.plan_unit_inst_status_cd
			String plan_unit_inst_status_cd = getValueFromRequest("get_acct_pln_unit_inst_all_m"+str,"//plan_unit_inst_status_cd").toString()
			logi.logInfo("plan_unit_inst_no_cd " +plan_unit_inst_status_cd )
			String fulfill,scd,puscd
			//Input checkpoints
		   //fulfillment_only
			if(fulfilment.equals("NoVal")){
				fulfill = "Null"
			}else if(fulfilment.equals("0")){
				fulfill = "Null"
			}else{
			if(fulfilment.equals("1")){ fulfill = "1"  }
			}
			logi.logInfo "SECTOR 654653"
			//status_cd
			if(status_cd.equals("NoVal")){
				scd = "Null"
			}else if(status_cd.equals("0")){
				scd = "Null"
			}else{
			if(status_cd.equals("1")){ scd = "0"  }
			else if(status_cd.equals("2")){scd = "2"   }
			else if(status_cd.equals("3")){ scd = "1"  }
			else {logi.logInfo "USE THE ALLOWED STATUS CODES"}
			}
		   //plan_unit_instance_cd
			if(plan_unit_inst_status_cd.equals("NoVal")){
				puscd = "Null"
			}else if(plan_unit_inst_status_cd.equals("0")){
				puscd = "Null"
			}else{
			if(plan_unit_inst_status_cd.equals("1")){ puscd = "1"  }
			else if(plan_unit_inst_status_cd.equals("2")){puscd = "0" }
			else {logi.logInfo "USE THE ALLOWED STATUS CODES"}
			}
			//process begins
			String cquery = "select plan_instance_no from ariacore.plan_instance where acct_no = "+acct_no+" and client_no = "+client_no+" order by plan_instance_no "
			LinkedHashMap <String, String> inner = new LinkedHashMap<String, String>();
			LinkedHashMap <String, String> outer = new LinkedHashMap<String, String>();
			 if(plan_instance_no == "NoVal" && plan_unit_inst_no == "NoVal")
			{
				  //when acct_no alone
				logi.logInfo "SECTOR 354322"
				logi.logInfo ""+plan_instance_no+" only acct no "+plan_unit_inst_no
				String pui = "Null"
				ResultSet rs=db.executePlaQuery(cquery)
					while(rs.next())
					{
						
						list[i]=rs.getObject(1)
						i++
					}
					logi.logInfo("TotalCount" + i)
					int sizecount = list.size()
					logi.logInfo("sizecount" + sizecount)
					for(int j=1;j<=sizecount-1;j++)
					{
						logi.logInfo("List"+list[j])
						inner = md_planinstance_DT_m(list[j].toString(),pui,fulfill,scd,puscd)
						outer.put("all_plan_instances_row["+j+"]",inner.sort())
					   
						inner = new LinkedHashMap<String, String>();
					}
			}
			else if(plan_instance_no == "NoVal" && plan_unit_inst_no != "NoVal")
			{
				logi.logInfo ""+plan_instance_no+" only acct_no,plan_isntance_no "+plan_unit_inst_no
				//when acct_no and plan_unit_instance_no alone.
				String pui_query = "select PLAN_INSTANCE_NO from ariacore.PLAN_INST_UNIT_INSTANCES where plan_unit_instance_no = "+plan_unit_inst_no
				String plan_instance = db.executeQueryP2(pui_query).toString()
				inner = md_planinstance_DT_m(plan_instance,plan_unit_inst_no,fulfill,scd,puscd)
				outer.put("all_plan_instances_row[1]",inner.sort())
				inner = new LinkedHashMap<String, String>();
				
			}
			else {  //plan_instance_no ! = NoVal & plan_unit_inst_no = NoVal
				
				logi.logInfo " last else "
				String pui = "Null"
				
				if(plan_instance_no != "NoVal"){
				list[i]=plan_instance_no.toString()
				i++
					}
				logi.logInfo("TotalCount" + i)
				int sizecount = list.size()
				logi.logInfo("sizecount" + sizecount)
				for(int j=1;j<=sizecount-1;j++)
				{
					logi.logInfo("List"+list[j])
					inner = md_planinstance_DT_m(list[j].toString(),pui,fulfill,scd,puscd)
					outer.put("all_plan_instances_row["+j+"]",inner.sort())
				   
					inner = new LinkedHashMap<String, String>();
				}
			}
		   
			
			logi.logInfo ""+outer.sort()
			
			return outer.sort()
			
		  //  return null
	   }
	   
	   //plan_instance_details
	   public LinkedHashMap md_planinstance_DT_m(String plan_instance_no,String pui,String fulfillment,String status_cd,String pui_status_cd){
		   
		   ConnectDB db = new ConnectDB()
		   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
		   LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
		   ArrayList<String> list = new ArrayList<String>()
		   int count=1
		   int q=1,r=1,s=1,t=1,z=1,u=1,xx=1
		   String query = "select pi.plan_instance_no,pi.plan_instance_cdid as client_plan_instance_id,pi.PARENT_PLAN_INSTANCE_NO AS PARENT_PLAN_INSTANCE_NO,cp.plan_name as plan_name,decode(supp_plan_ind,0,'MASTER',1,'SUPPLEMENTAL') as plan_type from ariacore.plan_instance pi join ariacore.client_plan cp on pi.plan_no = cp.plan_no where pi.plan_instance_no = "+plan_instance_no+" and pi.client_no =" +client_no
		   ResultSet rs=db.executePlaQuery(query)
		   ResultSetMetaData md = rs.getMetaData();
		   int columns = md.getColumnCount();
		   logi.logInfo "SECTOR 754536"
		   while (rs.next())
		   {
			   for(int i=1; i<=columns; i++)
			   {
				   if((rs.getObject(i))!=null)
				   {
					   //logi.logInfo ""+md.getColumnName(i).toLowerCase()+","+rs.getObject(i)
					   row.put((md.getColumnName(i)).toLowerCase(),(rs.getObject(i)));
				   }
			   }
		   }
		   
		   //plan unit details
		   
		  if(pui.equals("Null")){
			  //retrieving multiple pui's in a plan_instance_no
			  logi.logInfo "SECTOR 54543434"
			  String query2="select plan_unit_instance_no from ariacore.PLAN_INST_UNIT_INSTANCES where client_no = "+client_no+" and plan_instance_no =" +plan_instance_no +" order by plan_unit_instance_no"
			  ResultSet rs1=db.executePlaQuery(query2)
			  ResultSetMetaData md1 = rs1.getMetaData();
			  while (rs1.next())
			  {
					   list[count]=rs1.getObject(1)
					   count++
			  }
			  for( int j=1 ; j <=count-1 ;j++)
			  {
				 logi.logInfo("List"+list[j])
			  }
		  }else{
		  //inserting the single value of the request
		  list[count]=pui.toString()
		  count++
			  for( int j=1 ; j <=count-1 ;j++)
			  {
				 logi.logInfo("List"+list[j])
			  }
		  }
		  
		  
		  //Getting Plan unit instance detail
		  for( int j=1 ; j <=count-1 ;j++)
		  {
			  logi.logInfo " plan instance_no :"+plan_instance_no+" pui : "+list[j]+" fulfill: "+fulfillment+" status_cd : "+status_cd+" pui_status : "+pui_status_cd
			  
			  //fulfillment check
			  if(fulfillment.equals("Null")){
			  
				  //plan_instance_status_cd check
				  if(pui_status_cd.equals("Null")){
					  //plan_unit details
					  String query3="select plan_unit_instance_no as plan_unit_inst_no,plan_unit_instance_cdid as client_plan_unit_inst_id,status as plan_unit_inst_status from ariacore.PLAN_INST_UNIT_INSTANCES where plan_unit_instance_no = "+list[j]
					  ResultSet rs2=db.executePlaQuery(query3)
					  ResultSetMetaData md2 = rs2.getMetaData();
					  int columns2 = md2.getColumnCount();
					  logi.logInfo "SECTOR 566563423"
					  if(!rs2.isBeforeFirst()){
						  logi.logInfo "No rows for the condition pui status "+pui_status_cd
					  }else{
						  while (rs2.next())
						   {
							   for(int k=1; k<=columns2; k++)
							   {
								   if((rs2.getObject(k))!=null)
								   {
									   if(j==1)
									   {
										   row.put((md2.getColumnName(k)).toLowerCase(),(rs2.getObject(k)));
									   }
									   else
									   {
										   row.put((md2.getColumnName(k)).toLowerCase()+"["+j+"]",(rs2.getObject(k)));
									   }
								   }
							   }
						   }
					   }
					  
			  //plan_unit service_details
					  //switch case for service status_cd query
					  String squery,fquery = ""
					  logi.logInfo "SECTOR 534534534"
					  if(status_cd.equals("Null")){
						  squery = "SELECT piuis.service_no, acs.service_name, acs.client_service_id, piuis.fulfillment_status"+
								  " FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis"+
								  " JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI"+
								  " ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO"+
								  " AND PUI.CLIENT_NO = PIUIS.CLIENT_NO "+
								  " LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs"+
								  " ON piuis.service_no               =acs.service_no"+
								  " AND piuis.client_no               =acs.client_no"+
								  " WHERE piuis.plan_unit_instance_no = "+list[j]
								  
					  fquery = "SELECT TO_CHAR(piuis.FULFILLMENT_DATE,'YYYY-MM-DD') AS FULFILLMENT_DATE FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO AND PUI.CLIENT_NO = PIUIS.CLIENT_NO  LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs ON piuis.service_no=acs.service_no AND piuis.client_no=acs.client_no WHERE piuis.plan_unit_instance_no = "+list[j]+" and piuis.FULFILLMENT_DATE is not null"
				  }else{
					  squery = "SELECT piuis.service_no, acs.service_name, acs.client_service_id, piuis.fulfillment_status"+
							  " FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis"+
							  " JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI"+
							  " ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO"+
							  " AND PUI.CLIENT_NO = PIUIS.CLIENT_NO "+
							  " LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs"+
							  " ON piuis.service_no               =acs.service_no"+
							  " AND piuis.client_no               =acs.client_no"+
							  " WHERE piuis.plan_unit_instance_no = "+list[j]+" and piuis.fulfillment_status = "+status_cd
					  
					  fquery = "SELECT TO_CHAR(piuis.FULFILLMENT_DATE,'YYYY-MM-DD') AS FULFILLMENT_DATE FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO AND PUI.CLIENT_NO = PIUIS.CLIENT_NO  LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs ON piuis.service_no=acs.service_no AND piuis.client_no=acs.client_no WHERE piuis.plan_unit_instance_no = "+list[j]+" and piuis.fulfillment_status = "+status_cd+" and piuis.FULFILLMENT_DATE is not null"
					  }
				  ResultSet rs3=db.executePlaQuery(squery)
				  ResultSetMetaData md3 = rs3.getMetaData();
				  int columns3 = md3.getColumnCount();
				  if(!rs3.isBeforeFirst()){
						  logi.logInfo "No rows for the condition pui status "+pui_status_cd
				  }else{
				  while (rs3.next())
				   {
					   for(int l=1; l<=columns3; l++)
					   {
						   if((rs3.getObject(l))!=null)
						   {
							   
							   if(u==1)
							   {
								   row.put((md3.getColumnName(l)).toLowerCase(),(rs3.getObject(l)));
							   }
							   else
							   {
								   row.put((md3.getColumnName(l)).toLowerCase()+"["+u+"]",(rs3.getObject(l)));
							   }
							  
						   }
					   }
					   u++
				   }
				  }
				  //executing fulfillment date query
				  
				  ResultSet rs4=db.executePlaQuery(fquery)
				  ResultSetMetaData md4 = rs4.getMetaData();
				  int columns4 = md4.getColumnCount();
				  if(!rs4.isBeforeFirst()){
						  logi.logInfo "No rows for the condition status_cd "+status_cd
				  }else{
				  while (rs4.next())
				   {
					   for(int l=1; l<=columns4; l++)
					   {
						   if((rs4.getObject(l))!=null)
						   {
							   
							   if(xx==1)
							   {
								   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
							   }
							   else
							   {
								   row.put((md4.getColumnName(l)).toLowerCase()+"["+xx+"]",(rs4.getObject(l)));
							   }
							  
						   }
					   }
					   xx++
				   }
				  }
				  
				  //PRODUCT VALUES
				  logi.logInfo "SECTOR 365324154"
				  String pquery = "SELECT COB.FIELD_NAME,PPVL.VALUE_TEXT AS field_value FROM ARIACORE.PLAN_UNIT_INST_PROD_FIELDS_VAL PPVL JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI ON PUI.PLAN_UNIT_INSTANCE_NO = PPVL.PLAN_UNIT_INSTANCE_NO AND PUI.CLIENT_NO = PPVL.CLIENT_NO JOIN ARIACORE.CLIENT_OBJ_SUPP_FIELDS COB ON COB.FIELD_NO= PPVL.FIELD_NO AND PPVL.CLIENT_NO= COB.CLIENT_NO WHERE PUI.PLAN_UNIT_INSTANCE_NO = "+list[j]
				  ResultSet rs5=db.executePlaQuery(pquery)
				  ResultSetMetaData md5 = rs5.getMetaData();
				  int columns5 = md5.getColumnCount();
				  if(!rs5.isBeforeFirst()){
						  logi.logInfo "No rows for the pquery "
				  }else{
				  while (rs5.next())
				   {
					   for(int l=1; l<=columns5; l++)
					   {
						   if((rs5.getObject(l))!=null)
						   {
							   
							   if(z==1)
							   {
								   row.put((md5.getColumnName(l)).toLowerCase(),(rs5.getObject(l)));
							   }
							   else
							   {
								   row.put((md5.getColumnName(l)).toLowerCase()+"["+z+"]",(rs5.getObject(l)));
							   }
							  
						   }
					   }
					   z++
				   }
				  }
				  logi.logInfo "SECTOR 354874322"
				  }else{
				  //plan_instance_status_cd is 1
				  //plan_unit details
				  logi.logInfo "SECTOR 84758723947878738"
				  String query3="select plan_unit_instance_no as plan_unit_inst_no,plan_unit_instance_cdid as client_plan_unit_inst_id,status as plan_unit_inst_status from ariacore.PLAN_INST_UNIT_INSTANCES where plan_unit_instance_no = "+list[j]+" and STATUS = "+pui_status_cd
				  ResultSet rs2=db.executePlaQuery(query3)
				  ResultSetMetaData md2 = rs2.getMetaData();
				  int columns2 = md2.getColumnCount();
				  logi.logInfo "SECTOR 566563423"
				  if(!rs2.isBeforeFirst()){
					  logi.logInfo "No rows for the condition pui status "+pui_status_cd
				  }else{
					  while (rs2.next())
					   {
						   for(int k=1; k<=columns2; k++)
						   {
							   if((rs2.getObject(k))!=null)
							   {
								   if(j==1)
								   {
									   row.put((md2.getColumnName(k)).toLowerCase(),(rs2.getObject(k)));
								   }
								   else
								   {
									   row.put((md2.getColumnName(k)).toLowerCase()+"["+j+"]",(rs2.getObject(k)));
								   }
							   }
						   }
					   }
				   }
				  
		  //plan_unit service_details
				  //switch case for service status_cd query
				  String squery,fquery = ""
				  logi.logInfo "SECTOR 6873543565"
				  if(status_cd.equals("Null")){
					  squery = "SELECT piuis.service_no, acs.service_name, acs.client_service_id, piuis.fulfillment_status"+
							  " FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis"+
							  " JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI"+
							  " ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO"+
							  " AND PUI.CLIENT_NO = PIUIS.CLIENT_NO "+
							  " LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs"+
							  " ON piuis.service_no               =acs.service_no"+
							  " AND piuis.client_no               =acs.client_no"+
							  " WHERE piuis.plan_unit_instance_no = "+list[j]+" and pui.status = "+pui_status_cd
							  
				  fquery = "SELECT TO_CHAR(piuis.FULFILLMENT_DATE,'YYYY-MM-DD') AS FULFILLMENT_DATE FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO AND PUI.CLIENT_NO = PIUIS.CLIENT_NO  LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs ON piuis.service_no=acs.service_no AND piuis.client_no=acs.client_no WHERE piuis.plan_unit_instance_no = "+list[j]+" and pui.status = "+pui_status_cd+" and piuis.FULFILLMENT_DATE is not null"
			  }else{
				  squery = "SELECT piuis.service_no, acs.service_name, acs.client_service_id, piuis.fulfillment_status"+
						  " FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis"+
						  " JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI"+
						  " ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO"+
						  " AND PUI.CLIENT_NO = PIUIS.CLIENT_NO "+
						  " LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs"+
						  " ON piuis.service_no               =acs.service_no"+
						  " AND piuis.client_no               =acs.client_no"+
						  " WHERE piuis.plan_unit_instance_no = "+list[j]+" and piuis.fulfillment_status = "+status_cd+" and pui.status = "+pui_status_cd
				  
				  fquery = "SELECT TO_CHAR(piuis.FULFILLMENT_DATE,'YYYY-MM-DD') AS FULFILLMENT_DATE FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO AND PUI.CLIENT_NO = PIUIS.CLIENT_NO  LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs ON piuis.service_no=acs.service_no AND piuis.client_no=acs.client_no WHERE piuis.plan_unit_instance_no = "+list[j]+" and piuis.fulfillment_status = "+status_cd+" and pui.status = "+pui_status_cd+"and piuis.FULFILLMENT_DATE is not null"
				  }
			  ResultSet rs3=db.executePlaQuery(squery)
			  ResultSetMetaData md3 = rs3.getMetaData();
			  int columns3 = md3.getColumnCount();
			  if(!rs3.isBeforeFirst()){
					  logi.logInfo "No rows for the condition pui status "+pui_status_cd
			  }else{
			  while (rs3.next())
			   {
				   for(int l=1; l<=columns3; l++)
				   {
					   if((rs3.getObject(l))!=null)
					   {
						   
						   if(u==1)
						   {
							   row.put((md3.getColumnName(l)).toLowerCase(),(rs3.getObject(l)));
						   }
						   else
						   {
							   row.put((md3.getColumnName(l)).toLowerCase()+"["+u+"]",(rs3.getObject(l)));
						   }
						  
					   }
				   }
				   u++
			   }
			  }
			  //executing fulfillment date query
			  
			  ResultSet rs4=db.executePlaQuery(fquery)
			  ResultSetMetaData md4 = rs4.getMetaData();
			  int columns4 = md4.getColumnCount();
			  if(!rs4.isBeforeFirst()){
					  logi.logInfo "No rows for the condition status_cd "+status_cd
			  }else{
			  while (rs4.next())
			   {
				   for(int l=1; l<=columns4; l++)
				   {
					   if((rs4.getObject(l))!=null)
					   {
						   
						   if(xx==1)
						   {
							   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
						   }
						   else
						   {
							   row.put((md4.getColumnName(l)).toLowerCase()+"["+xx+"]",(rs4.getObject(l)));
						   }
						  
					   }
				   }
				   xx++
			   }
			  }
			  
			  //PRODUCT VALUES
			  logi.logInfo "SECTOR 6873218"
			  String pquery = "SELECT COB.FIELD_NAME,PPVL.VALUE_TEXT AS field_value FROM ARIACORE.PLAN_UNIT_INST_PROD_FIELDS_VAL PPVL JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI ON PUI.PLAN_UNIT_INSTANCE_NO = PPVL.PLAN_UNIT_INSTANCE_NO AND PUI.CLIENT_NO = PPVL.CLIENT_NO JOIN ARIACORE.CLIENT_OBJ_SUPP_FIELDS COB ON COB.FIELD_NO= PPVL.FIELD_NO AND PPVL.CLIENT_NO= COB.CLIENT_NO WHERE PUI.PLAN_UNIT_INSTANCE_NO = "+list[j]+" and pui.status = "+pui_status_cd
			  ResultSet rs5=db.executePlaQuery(pquery)
			  ResultSetMetaData md5 = rs5.getMetaData();
			  int columns5 = md5.getColumnCount();
			  if(!rs5.isBeforeFirst()){
					  logi.logInfo "No rows for the pquery "
			  }else{
			  while (rs5.next())
			   {
				   for(int l=1; l<=columns5; l++)
				   {
					   if((rs5.getObject(l))!=null)
					   {
						   
						   if(z==1)
						   {
							   row.put((md5.getColumnName(l)).toLowerCase(),(rs5.getObject(l)));
						   }
						   else
						   {
							   row.put((md5.getColumnName(l)).toLowerCase()+"["+z+"]",(rs5.getObject(l)));
						   }
						  
					   }
				   }
				   z++
			   }
			  }
			  logi.logInfo "SECTOR 6587224"
			  
				  }
					  
			  }else {
			  //for fulfillment is 1
					  
					  //plan_instance_status_cd check
					  if(pui_status_cd.equals("Null")){
						  //plan_unit details
						  String query3="select plan_unit_instance_no as plan_unit_inst_no,plan_unit_instance_cdid as client_plan_unit_inst_id,status as plan_unit_inst_status from ariacore.PLAN_INST_UNIT_INSTANCES where plan_unit_instance_no = "+list[j]
						  ResultSet rs2=db.executePlaQuery(query3)
						  ResultSetMetaData md2 = rs2.getMetaData();
						  int columns2 = md2.getColumnCount();
						  logi.logInfo "SECTOR 6582425612"
						  if(!rs2.isBeforeFirst()){
							  logi.logInfo "No rows for the condition pui status "+pui_status_cd
						  }else{
							  while (rs2.next())
							   {
								   for(int k=1; k<=columns2; k++)
								   {
									   if((rs2.getObject(k))!=null)
									   {
										   if(j==1)
										   {
											   row.put((md2.getColumnName(k)).toLowerCase(),(rs2.getObject(k)));
										   }
										   else
										   {
											   row.put((md2.getColumnName(k)).toLowerCase()+"["+j+"]",(rs2.getObject(k)));
										   }
									   }
								   }
							   }
						   }
						  
				  //plan_unit service_details
						  //switch case for service status_cd query
						  String squery,fquery = ""
						  logi.logInfo "SECTOR 98562421"
						  if(status_cd.equals("Null")){
							  squery = "SELECT piuis.service_no, acs.service_name, acs.client_service_id, piuis.fulfillment_status"+
									  " FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis"+
									  " JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI"+
									  " ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO"+
									  " AND PUI.CLIENT_NO = PIUIS.CLIENT_NO "+
									  " LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs"+
									  " ON piuis.service_no               =acs.service_no"+
									  " AND piuis.client_no               =acs.client_no"+
									  " WHERE piuis.plan_unit_instance_no = "+list[j]+" and piuis.FULFILLMENT_BASED_IND = 1"
									  
						  fquery = "SELECT TO_CHAR(piuis.FULFILLMENT_DATE,'YYYY-MM-DD') AS FULFILLMENT_DATE FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO AND PUI.CLIENT_NO = PIUIS.CLIENT_NO  LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs ON piuis.service_no=acs.service_no AND piuis.client_no=acs.client_no WHERE piuis.plan_unit_instance_no = "+list[j]+" and piuis.FULFILLMENT_BASED_IND = 1 and piuis.FULFILLMENT_DATE is not null"
					  }else{
						  squery = "SELECT piuis.service_no, acs.service_name, acs.client_service_id, piuis.fulfillment_status"+
								  " FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis"+
								  " JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI"+
								  " ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO"+
								  " AND PUI.CLIENT_NO = PIUIS.CLIENT_NO "+
								  " LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs"+
								  " ON piuis.service_no               =acs.service_no"+
								  " AND piuis.client_no               =acs.client_no"+
								  " WHERE piuis.plan_unit_instance_no = "+list[j]+" and piuis.FULFILLMENT_BASED_IND = 1 and piuis.fulfillment_status = "+status_cd
						  
						  fquery = "SELECT TO_CHAR(piuis.FULFILLMENT_DATE,'YYYY-MM-DD') AS FULFILLMENT_DATE FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO AND PUI.CLIENT_NO = PIUIS.CLIENT_NO  LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs ON piuis.service_no=acs.service_no AND piuis.client_no=acs.client_no WHERE piuis.plan_unit_instance_no = "+list[j]+" and piuis.fulfillment_status = "+status_cd+" and piuis.FULFILLMENT_BASED_IND = 1 and piuis.FULFILLMENT_DATE is not null"
						  }
					  ResultSet rs3=db.executePlaQuery(squery)
					  ResultSetMetaData md3 = rs3.getMetaData();
					  int columns3 = md3.getColumnCount();
					  if(!rs3.isBeforeFirst()){
							  logi.logInfo "No rows for the condition pui status "+pui_status_cd
					  }else{
					  while (rs3.next())
					   {
						   for(int l=1; l<=columns3; l++)
						   {
							   if((rs3.getObject(l))!=null)
							   {
								   
								   if(u==1)
								   {
									   row.put((md3.getColumnName(l)).toLowerCase(),(rs3.getObject(l)));
								   }
								   else
								   {
									   row.put((md3.getColumnName(l)).toLowerCase()+"["+u+"]",(rs3.getObject(l)));
								   }
								  
							   }
						   }
						   u++
					   }
					  }
					  //executing fulfillment date query
					  
					  ResultSet rs4=db.executePlaQuery(fquery)
					  ResultSetMetaData md4 = rs4.getMetaData();
					  int columns4 = md4.getColumnCount();
					  if(!rs4.isBeforeFirst()){
							  logi.logInfo "No rows for the condition status_cd "+status_cd
					  }else{
					  while (rs4.next())
					   {
						   for(int l=1; l<=columns4; l++)
						   {
							   if((rs4.getObject(l))!=null)
							   {
								   
								   if(xx==1)
								   {
									   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
								   }
								   else
								   {
									   row.put((md4.getColumnName(l)).toLowerCase()+"["+xx+"]",(rs4.getObject(l)));
								   }
								  
							   }
						   }
						   xx++
					   }
					  }
					  
					  //PRODUCT VALUES
					  logi.logInfo "SECTOR 63254814124"
					  String pquery = "SELECT COB.FIELD_NAME,PPVL.VALUE_TEXT AS field_value FROM ARIACORE.PLAN_UNIT_INST_PROD_FIELDS_VAL PPVL JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI ON PUI.PLAN_UNIT_INSTANCE_NO = PPVL.PLAN_UNIT_INSTANCE_NO AND PUI.CLIENT_NO = PPVL.CLIENT_NO JOIN ARIACORE.CLIENT_OBJ_SUPP_FIELDS COB ON COB.FIELD_NO= PPVL.FIELD_NO AND PPVL.CLIENT_NO= COB.CLIENT_NO WHERE PUI.PLAN_UNIT_INSTANCE_NO = "+list[j]
					  ResultSet rs5=db.executePlaQuery(pquery)
					  ResultSetMetaData md5 = rs5.getMetaData();
					  int columns5 = md5.getColumnCount();
					  if(!rs5.isBeforeFirst()){
							  logi.logInfo "No rows for the pquery "
					  }else{
					  while (rs5.next())
					   {
						   for(int l=1; l<=columns5; l++)
						   {
							   if((rs5.getObject(l))!=null)
							   {
								   
								   if(z==1)
								   {
									   row.put((md5.getColumnName(l)).toLowerCase(),(rs5.getObject(l)));
								   }
								   else
								   {
									   row.put((md5.getColumnName(l)).toLowerCase()+"["+z+"]",(rs5.getObject(l)));
								   }
								  
							   }
						   }
						   z++
					   }
					  }
					  logi.logInfo "SECTOR 468232165"
					  }else{
					  //plan_instance_status_cd is 1
					  //plan_unit details
					  logi.logInfo "SECTOR 455454852233"
					  String query3="select plan_unit_instance_no as plan_unit_inst_no,plan_unit_instance_cdid as client_plan_unit_inst_id,status as plan_unit_inst_status from ariacore.PLAN_INST_UNIT_INSTANCES where plan_unit_instance_no = "+list[j]+" and STATUS = "+pui_status_cd
					  ResultSet rs2=db.executePlaQuery(query3)
					  ResultSetMetaData md2 = rs2.getMetaData();
					  int columns2 = md2.getColumnCount();
					  logi.logInfo "SECTOR 65465465465425"
					  if(!rs2.isBeforeFirst()){
						  logi.logInfo "No rows for the condition pui status "+pui_status_cd
					  }else{
						  while (rs2.next())
						   {
							   for(int k=1; k<=columns2; k++)
							   {
								   if((rs2.getObject(k))!=null)
								   {
									   if(j==1)
									   {
										   row.put((md2.getColumnName(k)).toLowerCase(),(rs2.getObject(k)));
									   }
									   else
									   {
										   row.put((md2.getColumnName(k)).toLowerCase()+"["+j+"]",(rs2.getObject(k)));
									   }
								   }
							   }
						   }
					   }
					  
			  //plan_unit service_details
					  //switch case for service status_cd query
					  String squery,fquery = ""
					  logi.logInfo "SECTOR 9965566545"
					  if(status_cd.equals("Null")){
						  squery = "SELECT piuis.service_no, acs.service_name, acs.client_service_id, piuis.fulfillment_status"+
								  " FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis"+
								  " JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI"+
								  " ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO"+
								  " AND PUI.CLIENT_NO = PIUIS.CLIENT_NO "+
								  " LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs"+
								  " ON piuis.service_no               =acs.service_no"+
								  " AND piuis.client_no               =acs.client_no"+
								  " WHERE piuis.plan_unit_instance_no = "+list[j]+" and piuis.FULFILLMENT_BASED_IND = 1 and pui.status = "+pui_status_cd
								  
					  fquery = "SELECT TO_CHAR(piuis.FULFILLMENT_DATE,'YYYY-MM-DD') AS FULFILLMENT_DATE FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO AND PUI.CLIENT_NO = PIUIS.CLIENT_NO  LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs ON piuis.service_no=acs.service_no AND piuis.client_no=acs.client_no WHERE piuis.plan_unit_instance_no = "+list[j]+" and pui.status = "+pui_status_cd+" and piuis.FULFILLMENT_BASED_IND = 1 and piuis.FULFILLMENT_DATE is not null"
				  }else{
					  squery = "SELECT piuis.service_no, acs.service_name, acs.client_service_id, piuis.fulfillment_status"+
							  " FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis"+
							  " JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI"+
							  " ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO"+
							  " AND PUI.CLIENT_NO = PIUIS.CLIENT_NO "+
							  " LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs"+
							  " ON piuis.service_no               =acs.service_no"+
							  " AND piuis.client_no               =acs.client_no"+
							  " WHERE piuis.plan_unit_instance_no = "+list[j]+" and piuis.FULFILLMENT_BASED_IND = 1 and piuis.fulfillment_status = "+status_cd+" and pui.status = "+pui_status_cd
					  
					  fquery = "SELECT TO_CHAR(piuis.FULFILLMENT_DATE,'YYYY-MM-DD') AS FULFILLMENT_DATE FROM ariacore.PLAN_INST_UNIT_INSTANCE_SRVCS piuis JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI ON PUI.PLAN_UNIT_INSTANCE_NO = PIUIS.PLAN_UNIT_INSTANCE_NO AND PUI.CLIENT_NO = PIUIS.CLIENT_NO  LEFT JOIN ariacore.ALL_CLIENT_SERVICE acs ON piuis.service_no=acs.service_no AND piuis.client_no=acs.client_no WHERE piuis.plan_unit_instance_no = "+list[j]+" and piuis.fulfillment_status = "+status_cd+" and pui.status = "+pui_status_cd+"and piuis.FULFILLMENT_BASED_IND = 1 and piuis.FULFILLMENT_DATE is not null"
					  }
				  ResultSet rs3=db.executePlaQuery(squery)
				  ResultSetMetaData md3 = rs3.getMetaData();
				  int columns3 = md3.getColumnCount();
				  if(!rs3.isBeforeFirst()){
						  logi.logInfo "No rows for the condition pui status "+pui_status_cd
				  }else{
				  while (rs3.next())
				   {
					   for(int l=1; l<=columns3; l++)
					   {
						   if((rs3.getObject(l))!=null)
						   {
							   
							   if(u==1)
							   {
								   row.put((md3.getColumnName(l)).toLowerCase(),(rs3.getObject(l)));
							   }
							   else
							   {
								   row.put((md3.getColumnName(l)).toLowerCase()+"["+u+"]",(rs3.getObject(l)));
							   }
							  
						   }
					   }
					   u++
				   }
				  }
				  //executing fulfillment date query
				  
				  ResultSet rs4=db.executePlaQuery(fquery)
				  ResultSetMetaData md4 = rs4.getMetaData();
				  int columns4 = md4.getColumnCount();
				  if(!rs4.isBeforeFirst()){
						  logi.logInfo "No rows for the condition status_cd "+status_cd
				  }else{
				  while (rs4.next())
				   {
					   for(int l=1; l<=columns4; l++)
					   {
						   if((rs4.getObject(l))!=null)
						   {
							   
							   if(xx==1)
							   {
								   row.put((md4.getColumnName(l)).toLowerCase(),(rs4.getObject(l)));
							   }
							   else
							   {
								   row.put((md4.getColumnName(l)).toLowerCase()+"["+xx+"]",(rs4.getObject(l)));
							   }
							  
						   }
					   }
					   xx++
				   }
				  }
				  
				  //PRODUCT VALUES
				  logi.logInfo "SECTOR 6656561323"
				  String pquery = "SELECT COB.FIELD_NAME,PPVL.VALUE_TEXT AS field_value FROM ARIACORE.PLAN_UNIT_INST_PROD_FIELDS_VAL PPVL JOIN ARIACORE.PLAN_INST_UNIT_INSTANCES PUI ON PUI.PLAN_UNIT_INSTANCE_NO = PPVL.PLAN_UNIT_INSTANCE_NO AND PUI.CLIENT_NO = PPVL.CLIENT_NO JOIN ARIACORE.CLIENT_OBJ_SUPP_FIELDS COB ON COB.FIELD_NO= PPVL.FIELD_NO AND PPVL.CLIENT_NO= COB.CLIENT_NO WHERE PUI.PLAN_UNIT_INSTANCE_NO = "+list[j]+" and pui.status = "+pui_status_cd
				  ResultSet rs5=db.executePlaQuery(pquery)
				  ResultSetMetaData md5 = rs5.getMetaData();
				  int columns5 = md5.getColumnCount();
				  if(!rs5.isBeforeFirst()){
						  logi.logInfo "No rows for the pquery "
				  }else{
				  while (rs5.next())
				   {
					   for(int l=1; l<=columns5; l++)
					   {
						   if((rs5.getObject(l))!=null)
						   {
							   
							   if(z==1)
							   {
								   row.put((md5.getColumnName(l)).toLowerCase(),(rs5.getObject(l)));
							   }
							   else
							   {
								   row.put((md5.getColumnName(l)).toLowerCase()+"["+z+"]",(rs5.getObject(l)));
							   }
							  
						   }
					   }
					   z++
				   }
				  }
				  logi.logInfo "SECTOR 55587455"
				  
					  }
					  
					  
					  }
					  
					  
				  }
				  
		  
		   row.put("queued_ind",'A')
		   return row
	   }
	   
	   /**
		* no input needed
		* @param param
		* @return
		*/
	   public def md_plan_instance_services_row_DB_m(String param)
	   {
		   ConnectDB db = new ConnectDB()
		   LinkedHashMap line_item = new LinkedHashMap()
		   LinkedHashMap row_item = new LinkedHashMap()
		   String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		   String Query="SELECT PISM.service_no,acs.service_name as service_desc,acs.IS_RECURRING as is_recurring_ind,acs.IS_USAGE as is_usage_based_ind,acs.USAGE_TYPE_NO as usage_type,NVL(acs.TAXABLE_IND,0) as taxable_ind,acs.IS_TAX as is_tax_ind,acs.IS_ARREARS as is_arrears_ind,acs.IS_SETUP as is_setup_ind,acs.IS_MISC as is_misc_ind,acs.IS_DONATION as is_donation_ind,acs.IS_ORDER_BASED as is_order_based_ind,acs.IS_CANCELLATION as is_cancellation_ind,acs.coa_id,acs.coa_id as ledger_code,coa.coa_code as client_coa_code,case when acs.NO_DISPLAY_IND = 0 then 1 else 0 end as display_ind,ps.TIERED_PRICING_RULE_NO as tiered_pricing_rule,acs.IS_MIN_FEE as is_min_fee_ind,acs.CLIENT_SERVICE_ID,acut.USAGE_TYPE_CD,ps.FULFILLMENT_BASED_IND,acut.SUMMARY_DESC as usage_type_name,acut.DESCRIPTION as usage_type_desc,acut.USAGE_TYPE_CD as usage_type_code,acuut.DESCRIPTION as usage_unit_label,PS.APPLY_DAILY_RATES_IND AS APPLY_USAGE_RATES_DAILY,acs.SERVICE_LOCATION_NO as svc_location_no,DEST_ADDRESS_SEQ as dest_contact_no  from ariacore.PLAN_INSTANCE_SERVICE_MAP pism join ariacore.all_client_service acs on acs.client_no = pism.client_no and acs.service_no = pism.service_no join ariacore.CLIENT_CHART_OF_ACCOUNT coa on coa.client_no=acs.client_no and coa.coa_id = acs.coa_id join ariacore.PLAN_SERVICES PS on ps.client_no = acs.client_no and ps.plan_no = pism.plan_no and ps.service_no = acs.service_no left join ariacore.ALL_CLIENT_USAGE_TYPE acut on acut.client_no=acs.client_no and acut.usage_type_no=acs.usage_type_no left join ariacore.ALL_CLIENT_USAGE_UNIT_TYPE acuut on acuut.client_no =acs.client_no and acuut.USAGE_UNIT_TYPE_NO = acut.USAGE_UNIT_TYPE_NO left join ariacore.SERVICE_LOCATION sl on sl.client_no = acs.client_no and sl.SERVICE_LOCATION_NO = acs.SERVICE_LOCATION_NO left join ariacore.plan_instance_master pim on pim.client_no=acs.client_no and pim.plan_instance_no = pism.plan_instance_no where pism.acct_no =  "+acct_no+" order by pim.plan_instance_no,pism.plan_instance_no,pism.service_no";
		   ResultSet resultSet = db.executePlaQuery(Query)
		   ResultSetMetaData md = resultSet.getMetaData()
		   int columns = md.getColumnCount();
		   int loops=1
		   while (resultSet.next())
		   {
		   
				for(int u=1; u<=columns; ++u)
			   {
				   if(resultSet.getObject(u)!=null)
					   line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
			   }
			   
			   line_item.sort();
			   row_item.put("plan_instance_services_row["+(loops-1)+"]",line_item.sort())
			   line_item = new LinkedHashMap();
			   loops+=1;
		   }
		   row_item.sort();
		   return row_item.sort();
		   
	   }
	   def md_get_rate_sched_detailsDB_m (String s)
	   {
						 
						 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
						 LinkedHashMap<String,LinkedHashMap> rate_scheds_row = new LinkedHashMap<String,LinkedHashMap>();
						 LinkedHashMap<String,LinkedHashMap> rate_scheds = new LinkedHashMap<String,LinkedHashMap>();
						 LinkedHashMap rates = new LinkedHashMap();
						 int loops=1;
						 String plan_no= getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/plan_no[1]")
						 ConnectDB db = new ConnectDB()
						 int counts= Integer.parseInt(db.executeQueryP2("SELECT count(*) from ariacore.plan_rate_schedule where client_no="+client_no+" and plan_no="+plan_no))
						 for(int loop2=1;loop2<=counts;loop2++)
									 {
										   String  DB_RATE_SCHEDULE_DETAIL_ROW_m=""
										   ResultSet resultSet =null;
										   ResultSetMetaData md ;
										   logi.logInfo"schedule"
										   //String present=db.executeQueryP2("select plan_no from ariacore.gl_detail where seq_num="+loop2+" and invoice_no="+loopval).toString()
										   //if(present.contains("null"))
										   //{
												 String sched_no=db.executeQueryP2("SELECT A.SCHEDULE_NO FROM(SELECT ROW_NUMBER()OVER (ORDER BY SCHEDULE_NO)AS SEQ,SCHEDULE_NO from ariacore.plan_rate_schedule  where plan_no="+plan_no+")A WHERE SEQ="+loop2).toString()
												 String follow_rate=db.executeQueryP2("SELECT FOLLOWUP_RS_NO from ariacore.NEW_RATE_SCHEDULES where plan_no="+plan_no+" and client_no="+client_no+" AND SCHEDULE_NO="+sched_no).toString()
												 String to_date=db.executeQueryP2("SELECT A.EFFECTIVE_TO_DATE FROM(SELECT ROW_NUMBER()OVER (ORDER BY SCHEDULE_NO)AS SEQ,EFFECTIVE_TO_DATE from ariacore.NEW_RATE_SCHEDULES  where plan_no="+plan_no+")A WHERE SEQ="+loop2).toString()
												 logi.logInfo"came inside"
												 if(follow_rate.contains("null")&&to_date !=null)
												 {
												 DB_RATE_SCHEDULE_DETAIL_ROW_m="SELECT A.SCHEDULE_NO, A.SCHEDULE_NAME, A.SCHEDULE_CURRENCY, A.RECURRING_BILLING_INTERVAl, A.USAGE_BILLING_INTERVAL, A.DEFAULT_IND_CURR, A.DISPLAY_IND,A.DEFAULT_IND, A.CLIENT_RATE_SCHEDULE_ID, A.AVAILABLE_FROM_DT,A.AVAILABLE_TO_DT FROM (SELECT ROW_NUMBER() OVER (ORDER BY NRS.SCHEDULE_NO) AS SEQ,NRS.SCHEDULE_NO AS SCHEDULE_NO, NRS.SCHEDULE_NAME AS SCHEDULE_NAME,CASE WHEN PRS.CURRENCY_DEFAULT_IND='1' AND CP.CURRENCY=NRS.CURRENCY_CD THEN 1 ELSE 0 END AS DEFAULT_IND, NRS.CURRENCY_CD AS SCHEDULE_CURRENCY, NRS.RECUR_BILLING_INTERVAL AS RECURRING_BILLING_INTERVAl, NRS.USAGE_BILLING_INTERVAL AS USAGE_BILLING_INTERVAL, PRS.CURRENCY_DEFAULT_IND AS DEFAULT_IND_CURR, PRS.CLIENT_RATE_SCHEDULE_ID AS CLIENT_RATE_SCHEDULE_ID, TO_CHAR(NRS.EFFECTIVE_FROM_DATE, 'YYYY-MM-DD')   AS AVAILABLE_FROM_DT,TO_CHAR(NRS.EFFECTIVE_TO_DATE, 'YYYY-MM-DD')   AS AVAILABLE_TO_DT, CASE WHEN NRS.NO_DISPLAY_IND IN (0) THEN '1' ELSE '0' END AS DISPLAY_IND FROM ARIACORE.NEW_RATE_SCHEDULES NRS JOIN ariacore.CLIENT_PLAN CP ON CP.PLAN_NO=NRS.PLAN_NO AND CP.CLIENT_NO=NRS.CLIENT_NO JOIN ARIACORE.PLAN_RATE_SCHEDULE PRS ON PRS.PLAN_NO=NRS.PLAN_NO AND PRS.CLIENT_NO=NRS.CLIENT_NO AND PRS.SCHEDULE_NO=NRS.SCHEDULE_NO WHERE NRS.PLAN_NO="+plan_no+" AND PRS.CLIENT_NO="+client_no+")A where seq="+loop2
												 if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_RATE_SCHEDULE_DETAIL_ROW_m+")"))!=0)
												 {
													   resultSet = db.executePlaQuery(DB_RATE_SCHEDULE_DETAIL_ROW_m)
													   md = resultSet.getMetaData()
												 }
												 }
												 else if(follow_rate.contains("null")&&to_date==null)
												 {
													   DB_RATE_SCHEDULE_DETAIL_ROW_m="SELECT A.SCHEDULE_NO, A.SCHEDULE_NAME, A.SCHEDULE_CURRENCY, A.RECURRING_BILLING_INTERVAl, A.USAGE_BILLING_INTERVAL, A.DEFAULT_IND_CURR, A.DISPLAY_IND,A.DEFAULT_IND, A.CLIENT_RATE_SCHEDULE_ID, A.AVAILABLE_FROM_DT, FROM (SELECT ROW_NUMBER() OVER (ORDER BY NRS.SCHEDULE_NO) AS SEQ,NRS.SCHEDULE_NO AS SCHEDULE_NO, NRS.SCHEDULE_NAME AS SCHEDULE_NAME,CASE WHEN PRS.CURRENCY_DEFAULT_IND='1' AND CP.CURRENCY=NRS.CURRENCY_CD THEN 1 ELSE 0 END AS DEFAULT_IND, NRS.CURRENCY_CD AS SCHEDULE_CURRENCY, NRS.RECUR_BILLING_INTERVAL AS RECURRING_BILLING_INTERVAl, NRS.USAGE_BILLING_INTERVAL AS USAGE_BILLING_INTERVAL, PRS.CURRENCY_DEFAULT_IND AS DEFAULT_IND_CURR, PRS.CLIENT_RATE_SCHEDULE_ID AS CLIENT_RATE_SCHEDULE_ID, TO_CHAR(NRS.EFFECTIVE_FROM_DATE, 'YYYY-MM-DD')   AS AVAILABLE_FROM_DT , CASE WHEN NRS.NO_DISPLAY_IND IN (0) THEN '1' ELSE '0' END AS DISPLAY_IND FROM ARIACORE.NEW_RATE_SCHEDULES NRS JOIN ariacore.CLIENT_PLAN CP ON CP.PLAN_NO=NRS.PLAN_NO AND CP.CLIENT_NO=NRS.CLIENT_NO JOIN ARIACORE.PLAN_RATE_SCHEDULE PRS ON PRS.PLAN_NO=NRS.PLAN_NO AND PRS.CLIENT_NO=NRS.CLIENT_NO AND PRS.SCHEDULE_NO=NRS.SCHEDULE_NO WHERE NRS.PLAN_NO="+plan_no+" AND PRS.CLIENT_NO="+client_no+")A where seq="+loop2
													   if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_RATE_SCHEDULE_DETAIL_ROW_m+")"))!=0)
													   {
															 resultSet = db.executePlaQuery(DB_RATE_SCHEDULE_DETAIL_ROW_m)
															 md = resultSet.getMetaData()
													   }
												 }
												 else
												 {
												 DB_RATE_SCHEDULE_DETAIL_ROW_m="SELECT A.SCHEDULE_NO,A.SCHEDULE_NAME,A.SCHEDULE_CURRENCY,A.RECURRING_BILLING_INTERVAl, A.USAGE_BILLING_INTERVAL,A.DEFAULT_IND_CURR,A.DISPLAY_IND,A.DEFAULT_IND, A.CLIENT_RATE_SCHEDULE_ID,A.AVAILABLE_FROM_DT, A.AVAILABLE_TO_DT, A.FOLLOWUP_RS_NO,A.CLIENT_FOLLOWUP_RS_ID FROM (SELECT NRS.SCHEDULE_NO AS SCHEDULE_NO,ROW_NUMBER() OVER (ORDER BY NRS.SCHEDULE_NO) AS SEQ,NRS.SCHEDULE_NAME AS SCHEDULE_NAME,NRS.CURRENCY_CD AS SCHEDULE_CURRENCY, NRS.RECUR_BILLING_INTERVAL AS RECURRING_BILLING_INTERVAl, NRS.USAGE_BILLING_INTERVAL AS USAGE_BILLING_INTERVAL, PRS.CURRENCY_DEFAULT_IND AS DEFAULT_IND_CURR, PRS.CLIENT_RATE_SCHEDULE_ID AS CLIENT_RATE_SCHEDULE_ID, TO_CHAR(NRS.EFFECTIVE_FROM_DATE, 'YYYY-MM-DD')   AS AVAILABLE_FROM_DT, TO_CHAR(NRS.EFFECTIVE_TO_DATE, 'YYYY-MM-DD')   AS AVAILABLE_TO_DT, CASE WHEN PRS.CURRENCY_DEFAULT_IND='1' AND CP.CURRENCY=NRS.CURRENCY_CD THEN 1 ELSE 0 END AS DEFAULT_IND,NRS.FOLLOWUP_RS_NO  AS FOLLOWUP_RS_NO, CASE WHEN FOLLOWUP_RS_NO IS NOT NULL THEN (select CLIENT_RATE_SCHEDULE_ID from ariacore.plan_rate_schedule where SCHEDULE_NO= NRS.FOLLOWUP_RS_NO) ELSE NULL END AS CLIENT_FOLLOWUP_RS_ID,CASE WHEN NRS.NO_DISPLAY_IND IN (0) THEN '1' ELSE '0'  END AS DISPLAY_IND FROM ARIACORE.NEW_RATE_SCHEDULES NRS JOIN ariacore.CLIENT_PLAN CP ON CP.PLAN_NO=NRS.PLAN_NO AND CP.CLIENT_NO=NRS.CLIENT_NO JOIN ARIACORE.PLAN_RATE_SCHEDULE PRS ON PRS.PLAN_NO=NRS.PLAN_NO AND PRS.CLIENT_NO=NRS.CLIENT_NO AND PRS.SCHEDULE_NO=NRS.SCHEDULE_NO WHERE NRS.PLAN_NO="+plan_no+"  AND PRS.CLIENT_NO="+client_no+")A WHERE SEQ="+loop2
												 if(Integer.parseInt(db.executeQueryP2("select count(*) from ("+DB_RATE_SCHEDULE_DETAIL_ROW_m+")"))!=0)
												 {
															 resultSet = db.executePlaQuery(DB_RATE_SCHEDULE_DETAIL_ROW_m)
															 md = resultSet.getMetaData()}
												 }
												 int columns = md.getColumnCount();
												 while (resultSet.next())
												 {
													   for(int u=1; u<=columns; ++u)
															 {
																   if((resultSet.getObject(u))!=null)
																   rates.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
															 }
																   
												 }
												 rates.sort();
												 rate_scheds_row.put("rate_scheds_row["+(loops-1)+"]",rates.sort())
												 rates = new LinkedHashMap();
												 loops++;
									 }
									 rate_scheds_row=rate_scheds_row.sort()
									 return rate_scheds_row
	   }
 
 def md_get_RateScheduleNo_Invoice(String str)
 {
	 ConnectDB db = new ConnectDB()
	 String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null)
	 String query = "select max(invoice_no) as invoice_no from ariacore.gl_detail where CLIENT_NO="+Constant.client_no+" and  master_plan_instance_no = " + plan_instance_no
	 String invoice_no = db.executeQueryP2(query)
	 
	String q= "select distinct RATE_SCHEDULE_NO  from ariacore.all_invoice_details where  invoice_no="+invoice_no+" and plan_instance_no="+plan_instance_no+" and RATE_SCHEDULE_NO is not null "
	def rs=db.executeQueryP2(q)
	return rs
	 
 }
 
 def md_get_RateScheduleNoPlanByIndex(String str)
 {
	 
	 ConnectDB db = new ConnectDB()
	 String index=str.split("#")[1]
	 String param=str.split("#")[0]
	 String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(param,null)
	 String plan_no=db.executeQueryP2("SElect plan_no from ariacore.plan_instance where plan_instance_no="+plan_instance_no)
	String q= "select  schedule_no from (SELECT plan_no,schedule_no, row_number() over (order by schedule_no) as sno  FROM ARIACORE.plan_rate_schedule WHERE CLIENT_NO = 7000211 and plan_no="+plan_no+" ) where sno="+index
	def rs=db.executeQueryP2(q)
	return rs
	 
 }
 
 public LinkedHashMap md_create_cm_m (String param){
	 ConnectDB db = new ConnectDB()
	 LinkedHashMap row1 = new LinkedHashMap();
	 String acct_no =  getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
	 String cmquery = "SELECT MAX(CM_NO) AS CM_NO FROM ARIACORE.CREDIT_MEMO WHERE ACCT_NO ="+acct_no
	 String cm_no = db.executeQueryP2(cmquery).toString()
	 logi.logInfo("cm_no : "+cm_no)
	 String cquery = "SELECT COUNT(*) AS COUNTS FROM ARIACORE.CREDIT_MEMO_DETAIL WHERE CM_NO = "+cm_no
	 int cq = Integer.valueOf(db.executeQueryP2(cquery).toString())
	 logi.logInfo("cm_count : "+cq)
	 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	for(int i=1;i<=cq;i++){
		 LinkedHashMap row = new LinkedHashMap();
		 String squery1 = "SELECT APC.ORIG_TRANSACTION_ID,APC.CM_LINE_NO,APC.SERVICE_NO,APC.CLIENT_SERVICE_ID,APC.CM_TRANSACTION_ID,APC.LINE_AMOUNT FROM"+
		  " (SELECT (SELECT EVENT_NO FROM ARIACORE.ACCT_TRANSACTION WHERE SOURCE_NO IN"+
		  " (SELECT INVOICE_DETAIL_NO FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO = CMD.INVOICE_NO AND SEQ_NUM = CMD.GL_SEQ_NUM) AND CLIENT_NO = CMD.CLIENT_NO) AS ORIG_TRANSACTION_ID,"+
		  " CMD.GL_SEQ_NUM AS CM_LINE_NO, GD.SERVICE_NO, NVL(CS.CLIENT_SERVICE_ID,GD.SERVICE_NO) AS CLIENT_SERVICE_ID,ATN.EVENT_NO AS CM_TRANSACTION_ID,"+
		  " CMD.CREDIT AS LINE_AMOUNT,ROW_NUMBER() OVER (ORDER BY CMD.SEQ_NUM) AS SEQ"+
		  " FROM ARIACORE.CREDIT_MEMO_DETAIL CMD JOIN ARIACORE.ACCT_TRANSACTION ATN ON ATN.SOURCE_NO = CMD.CM_DETAIL_NO AND ATN.CLIENT_NO = CMD.CLIENT_NO"+
		  " LEFT JOIN ARIACORE.GL_DETAIL GD ON GD.SEQ_NUM = CMD.GL_SEQ_NUM AND GD.CLIENT_NO = CMD.CLIENT_NO AND GD.INVOICE_NO = CMD.INVOICE_NO"+
		  " LEFT JOIN ARIACORE.CLIENT_SERVICE CS ON GD.SERVICE_NO = CS.SERVICE_NO AND GD.CLIENT_NO = CS.CLIENT_NO"+
		  " WHERE CMD.CM_NO = "+cm_no+" AND CMD.CLIENT_NO = "+client_no+" AND ATN.TRANSACTION_TYPE = 24)APC WHERE SEQ ="+i
		 ResultSet rs5=db.executePlaQuery(squery1)
		 ResultSetMetaData md5 = rs5.getMetaData();
		 int columns5 = md5.getColumnCount();
		 if(!rs5.isBeforeFirst()){
				 logi.logInfo "No rows for the query "
		 }else{
		 while (rs5.next())
		  {
			  for(int l=1; l<=columns5; l++)
			  {
				  if((rs5.getObject(l))!=null)
				  {
					  row.put((md5.getColumnName(l)).toLowerCase(),(rs5.getObject(l)));
				 }
			  }
		  }
	   }
		 row1.put("applied_cm_details_row["+(i-1)+"]",row.sort())
	 }
	 return row1
 }
 
 public LinkedHashMap md_get_gen_cm_m(String param){
	 ConnectDB db = new ConnectDB()
	 LinkedHashMap row = new LinkedHashMap();
	 String cm_no = null
	 
	def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	String query = null
	if(param.equals("cm")){
		String acct_no =  getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
		String cmquery = "SELECT MAX(CM_NO) AS CM_NO FROM ARIACORE.CREDIT_MEMO WHERE ACCT_NO ="+acct_no
		cm_no = db.executeQueryP2(cmquery).toString()
		logi.logInfo("cm_no : "+cm_no)
	   query = "SELECT CM_NO,CREATE_USER AS created_by,TO_CHAR(create_date,'YYYY-MM-DD') as created_date,CREDIT as cm_total,REASON_CD as cm_reason_code FROM ARIACORE.CREDIT_MEMO WHERE CM_NO="+cm_no+" and client_no = "+client_no
	} else{
	cm_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/cm_no[1]")
	query = "SELECT CM_NO AS CM_NO_OUT, CLIENT_CM_SEQ_ID, CREATE_USER AS created_by,TO_CHAR(create_date,'YYYY-MM-DD') as created_date,REASON_CD as credit_reason_code, COMMENTS as credit_comments,CREDIT as cm_total,INVOICE_NO AS ORIG_INVOICE_NO FROM ARIACORE.CREDIT_MEMO WHERE CM_NO="+cm_no+" and client_no = "+client_no
	logi.logInfo("cm_no : "+cm_no)
	}
	ResultSet rs5=db.executePlaQuery(query)
	ResultSetMetaData md5 = rs5.getMetaData();
	int columns5 = md5.getColumnCount();
	if(!rs5.isBeforeFirst()){
			logi.logInfo "No rows for the query "
	}else{
	while (rs5.next())
	 {
		 for(int l=1; l<=columns5; l++)
		 {
			 if((rs5.getObject(l))!=null)
			 {
				 row.put((md5.getColumnName(l)).toLowerCase(),(rs5.getObject(l)));
			}
		 }
   }
	}
   return row.sort()
 }
 
 public LinkedHashMap md_get_cm_line_det_m(String param){
	 ConnectDB db = new ConnectDB()
	 LinkedHashMap row1 = new LinkedHashMap();
	 String cm_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/cm_no[1]")
	 logi.logInfo("cm_no : "+cm_no)
	def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	String cquery = "SELECT COUNT(*) AS COUNTS FROM ARIACORE.CREDIT_MEMO_DETAIL WHERE CM_NO = "+cm_no+" AND CLIENT_NO = "+client_no
	int cq = Integer.valueOf(db.executeQueryP2(cquery).toString())
	logi.logInfo "converted value of cq: "+cq
	for(int i=1;i<=cq;i++){
		LinkedHashMap row = new LinkedHashMap();
		String squery1 = "SELECT CMD.SEQ_NUM AS CM_LINE_NO,CMD.CREDIT AS CM_LINE_AMOUNT,ATN.EVENT_NO AS CM_TRANSACTION_ID FROM ARIACORE.CREDIT_MEMO_DETAIL CMD JOIN ARIACORE.ACCT_TRANSACTION ATN ON ATN.SOURCE_NO = CMD.CM_DETAIL_NO AND ATN.CLIENT_NO = CMD.CLIENT_NO WHERE CMD.CM_NO = "+cm_no+" AND ATN.CLIENT_NO = "+client_no+" AND ATN.TRANSACTION_TYPE = 24 AND CMD.SEQ_NUM = "+i
		ResultSet rs5=db.executePlaQuery(squery1)
		ResultSetMetaData md5 = rs5.getMetaData();
		int columns5 = md5.getColumnCount();
		if(!rs5.isBeforeFirst()){
				logi.logInfo "No rows for the query "
		}else{
		while (rs5.next())
		 {
			 for(int l=1; l<=columns5; l++)
			 {
				 if((rs5.getObject(l))!=null)
				 {
					 row.put((md5.getColumnName(l)).toLowerCase(),(rs5.getObject(l)));
				}
			 }
	   }
		}
		logi.logInfo "second query execution"
		String squery2 = "SELECT ATN.EVENT_NO AS CM_VOID_TRANSACTION_ID FROM ARIACORE.CREDIT_MEMO_DETAIL CMD JOIN ARIACORE.ACCT_TRANSACTION ATN ON ATN.SOURCE_NO = CMD.CM_DETAIL_NO AND ATN.CLIENT_NO = CMD.CLIENT_NO WHERE CMD.CM_NO = "+cm_no+" AND ATN.CLIENT_NO = "+client_no+" AND ATN.TRANSACTION_TYPE = -24 AND CMD.SEQ_NUM = "+i
		ResultSet rs6=db.executePlaQuery(squery2)
		ResultSetMetaData md6 = rs6.getMetaData();
		int columns6 = md5.getColumnCount();
		if(!rs6.isBeforeFirst()){
				logi.logInfo "No rows for the query "
		}else{
		while (rs6.next())
		 {
			 for(int l=1; l<=columns6; l++)
			 {
				 if((rs6.getObject(l))!=null)
				 {
					 row.put((md6.getColumnName(l)).toLowerCase(),(rs6.getObject(l)));
				}
			 }
		 }
		}
		
		row1.put("cm_line_details_row["+(i-1)+"]",row.sort())
	}
	
	return row1
 }
 
 public LinkedHashMap md_get_cm_taxline_det_m(String param){
	 ConnectDB db = new ConnectDB()
	 LinkedHashMap row1 = new LinkedHashMap();
	 String cm_no = null
	 if(param.equals("cm")){
	  String acct_no =  getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
	  String cmquery = "SELECT MAX(CM_NO) AS CM_NO FROM ARIACORE.CREDIT_MEMO WHERE ACCT_NO ="+acct_no
	  cm_no = db.executeQueryP2(cmquery).toString()
	 }else{
	 cm_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/cm_no[1]")
	 }
	 logi.logInfo("cm_no : "+cm_no)
	def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	String cquery = "SELECT COUNT(*) AS COUNTS FROM ARIACORE.CREDIT_MEMO_TAX_DETAIL WHERE CM_NO = "+cm_no+" AND CLIENT_NO = "+client_no
	int cq = Integer.valueOf(db.executeQueryP2(cquery).toString())
	logi.logInfo "converted value of cq: "+cq
	for(int i=1;i<=cq;i++){
		LinkedHashMap row = new LinkedHashMap();
		String squery1 = "SELECT CMT.TAXED_CM_LINE_NO,CMT.TAX_SRV_TAX_TYPE_ID,CMT.TAX_SRV_TAX_TYPE_DESC,CMT.TAX_SERV_CAT_TEXT,CMT.TAX_DETAIL_LINE,CMT.TAX_CREDIT_AMOUNT,CMT.TAX_AUTHORITY_LEVEL,CMT.ORIG_WAS_TAX_INCLUSIVE FROM "+
							"(SELECT ROW_NUMBER() OVER (ORDER BY CMTD.TAXED_SEQ_NUM,CMTD.SEQ_NUM) AS SEQ,CMTD.TAXED_SEQ_NUM AS taxed_cm_line_no,CMTD.TAX_SRV_TAX_TYPE_ID AS tax_srv_tax_type_id, CMTD.TAX_SRV_TAX_TYPE_DESC AS tax_srv_tax_type_desc,CMTD.TAX_SRV_CAT_TEXT AS tax_serv_cat_text,CMTD.SEQ_NUM AS tax_detail_line,CMTD.CREDIT AS tax_credit_amount,CMTD.TAX_AUTHORITY AS tax_authority_level,CASE WHEN CMTD.IS_EXCLUDED IS NULL THEN 0 ELSE CMTD.IS_EXCLUDED END AS orig_was_tax_inclusive FROM ARIACORE.CREDIT_MEMO_TAX_DETAIL CMTD WHERE CM_NO = "+cm_no+" AND CLIENT_NO = "+client_no+")CMT "+
							"WHERE CMT.SEQ = "+i
		ResultSet rs5=db.executePlaQuery(squery1)
		ResultSetMetaData md5 = rs5.getMetaData();
		int columns5 = md5.getColumnCount();
		if(!rs5.isBeforeFirst()){
				logi.logInfo "No rows for the query "
		}else{
		while (rs5.next())
		 {
			 for(int l=1; l<=columns5; l++)
			 {
				 if((rs5.getObject(l))!=null)
				 {
					 row.put((md5.getColumnName(l)).toLowerCase(),(rs5.getObject(l)));
				}
			 }
		 }
	  }
		row1.put("cm_tax_details_row["+(i-1)+"]",row.sort())
	}
	return row1
 }
 
 
 public LinkedHashMap md_get_invoice_cm_details_m (String str){
	 logi.logInfo ":::::::GET_INVOICE_CM_DETAILS_M:::::::"
	 LinkedHashMap row1 = new LinkedHashMap();
	 ConnectDB db = new ConnectDB()
	 String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
	 logi.logInfo("acct_no : "+acct_no)
	 String inv_no = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/invoice_no[1]")
	 logi.logInfo("invoice_no : "+inv_no)
	 String cquery = "SELECT COUNT(*) AS COUNTS FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO = "+inv_no+" AND ORIG_COUPON_CD IS NULL AND SERVICE_NO NOT IN (10004,400,401,402,405,0)"
	 int cq = Integer.valueOf(db.executeQueryP2(cquery).toString())
	 logi.logInfo "converted value of cq: "+cq
	 for(int i=1;i<=cq;i++){
		 LinkedHashMap row = new LinkedHashMap();
		 String  squery1 = null
		 if(str.equals("sc")){
			 squery1 = "SELECT ICM.LINE_NO,ICM.SERVICE_NO, ICM.SERVICE_NAME,ICM.CLIENT_SKU,ICM.ITEM_NO, ICM.CLIENT_SERVICE_ID, ICM.CLIENT_ITEM_ID,ICM.AMOUNT,ICM.MAX_CREDIT_AMOUNT FROM("+
			 " SELECT GD.SEQ_NUM AS LINE_NO, GD.SERVICE_NO, CS.SERVICE_NAME,GD.ORIG_CLIENT_SKU AS CLIENT_SKU,GD.ITEM_NO AS ITEM_NO, NVL(CS.CLIENT_SERVICE_ID ,GD.SERVICE_NO) AS CLIENT_SERVICE_ID,"+
		  " IT.CLIENT_ITEM_ID AS CLIENT_ITEM_ID,ATN.AMOUNT AS AMOUNT,(ATN.AMOUNT - ATN.APPLIED_AMOUNT) AS MAX_CREDIT_AMOUNT,ROW_NUMBER() OVER (ORDER BY GD.SEQ_NUM) AS SEQ"+
		  " FROM ARIACORE.GL_DETAIL GD"+
		  " JOIN ARIACORE.ACCT_TRANSACTION ATN ON GD.INVOICE_DETAIL_NO = ATN.SOURCE_NO AND GD.CLIENT_NO = ATN.CLIENT_NO"+
		  " LEFT JOIN ARIACORE.ALL_CLIENT_SERVICE CS ON CS.SERVICE_NO = GD.SERVICE_NO AND CS.CLIENT_NO = GD.CLIENT_NO"+
		  " LEFT JOIN ARIACORE.INVENTORY_ITEMS IT ON IT.SERVICE_NO = GD.SERVICE_NO AND IT.CLIENT_NO = GD.CLIENT_NO"+
		  " WHERE GD.INVOICE_NO = "+inv_no+" AND GD.ORIG_COUPON_CD IS NULL AND GD.SERVICE_NO NOT IN (10004,401,400,402,405,0))"+
		  " ICM WHERE ICM.SEQ = "+i
		 }else if(str.equals("cp")){
		 squery1 = "SELECT ICM.LINE_NO,ICM.SERVICE_NO, ICM.SERVICE_NAME,ICM.CLIENT_SKU,ICM.ITEM_NO, ICM.CLIENT_SERVICE_ID, ICM.CLIENT_ITEM_ID,ICM.AMOUNT,ICM.MAX_CREDIT_AMOUNT FROM("+
		 " SELECT GD.SEQ_NUM AS LINE_NO, GD.SERVICE_NO, CS.SERVICE_NAME,GD.ORIG_CLIENT_SKU AS CLIENT_SKU,GD.ITEM_NO AS ITEM_NO, NVL(CS.CLIENT_SERVICE_ID ,GD.SERVICE_NO) AS CLIENT_SERVICE_ID,"+
	  " IT.CLIENT_ITEM_ID AS CLIENT_ITEM_ID,ATN.AMOUNT AS AMOUNT,GDC.AMT AS MAX_CREDIT_AMOUNT,ROW_NUMBER() OVER (ORDER BY GD.SEQ_NUM) AS SEQ"+
	  " FROM ARIACORE.GL_DETAIL GD"+
	  " JOIN ARIACORE.ACCT_TRANSACTION ATN ON GD.INVOICE_DETAIL_NO = ATN.SOURCE_NO AND GD.CLIENT_NO = ATN.CLIENT_NO"+
	  " LEFT JOIN ARIACORE.ALL_CLIENT_SERVICE CS ON CS.SERVICE_NO = GD.SERVICE_NO AND CS.CLIENT_NO = GD.CLIENT_NO"+
	  " LEFT JOIN ARIACORE.INVENTORY_ITEMS IT ON IT.SERVICE_NO = GD.SERVICE_NO AND IT.CLIENT_NO = GD.CLIENT_NO"+
	  " LEFT JOIN ARIACORE.GL_DETAIL_AFTER_CREDIT GDC  ON GDC.INVOICE_NO = GD.INVOICE_NO AND GDC.CLIENT_NO = GD.CLIENT_NO  AND GDC.SEQ_NUM = GD.SEQ_NUM"+
	  " WHERE GD.INVOICE_NO = "+inv_no+" AND GD.ORIG_COUPON_CD IS NULL AND GD.SERVICE_NO NOT IN (10004,401,400,402,405,0))"+
	  " ICM WHERE ICM.SEQ = "+i
		 }
		 else{
		 squery1 = "SELECT ICM.LINE_NO,ICM.SERVICE_NO, ICM.SERVICE_NAME,ICM.CLIENT_SKU,ICM.ITEM_NO, ICM.CLIENT_SERVICE_ID, ICM.CLIENT_ITEM_ID,ICM.AMOUNT,ICM.MAX_CREDIT_AMOUNT FROM("+
					 " SELECT GD.SEQ_NUM AS LINE_NO, GD.SERVICE_NO, CS.SERVICE_NAME,GD.ORIG_CLIENT_SKU AS CLIENT_SKU,GD.ITEM_NO AS ITEM_NO, NVL(CS.CLIENT_SERVICE_ID ,GD.SERVICE_NO) AS CLIENT_SERVICE_ID,"+
				  " IT.CLIENT_ITEM_ID AS CLIENT_ITEM_ID,ATN.AMOUNT AS AMOUNT,CASE WHEN (SELECT MAX(CM_NO) FROM ARIACORE.CREDIT_MEMO WHERE ACCT_NO = ATN.ACCT_NO AND INVOICE_NO = GD.INVOICE_NO ) IS NOT NULL"+
				  " THEN NVL((ATN.AMOUNT - NVL((SELECT SUM(CREDIT) AS CREDIT FROM ARIACORE.CREDIT_MEMO_DETAIL WHERE INVOICE_NO = GD.INVOICE_NO AND GL_SEQ_NUM = GD.SEQ_NUM),0)),0)"+
				  " ELSE  NVL(ATN.AMOUNT,0) END AS MAX_CREDIT_AMOUNT,ROW_NUMBER() OVER (ORDER BY GD.SEQ_NUM) AS SEQ"+
				  " FROM ARIACORE.GL_DETAIL GD"+
				  " JOIN ARIACORE.ACCT_TRANSACTION ATN ON GD.INVOICE_DETAIL_NO = ATN.SOURCE_NO AND GD.CLIENT_NO = ATN.CLIENT_NO"+
				  " LEFT JOIN ARIACORE.ALL_CLIENT_SERVICE CS ON CS.SERVICE_NO = GD.SERVICE_NO AND CS.CLIENT_NO = GD.CLIENT_NO"+
				  " LEFT JOIN ARIACORE.INVENTORY_ITEMS IT ON IT.SERVICE_NO = GD.SERVICE_NO AND IT.CLIENT_NO = GD.CLIENT_NO"+
				  " WHERE GD.INVOICE_NO = "+inv_no+" AND GD.ORIG_COUPON_CD IS NULL AND GD.SERVICE_NO NOT IN (10004,401,400,402,405))"+
				  " ICM WHERE ICM.SEQ = "+i
		 }
		 ResultSet rs5=db.executePlaQuery(squery1)
		 ResultSetMetaData md5 = rs5.getMetaData();
		 int columns5 = md5.getColumnCount();
		 if(!rs5.isBeforeFirst()){
				 logi.logInfo "No rows for the query "
		 }else{
		 while (rs5.next())
		  {
			  for(int l=1; l<=columns5; l++)
			  {
				  if((rs5.getObject(l))!=null)
				  {
					  row.put((md5.getColumnName(l)).toLowerCase(),(rs5.getObject(l)));
				 }
			  }
		  }
	   }
		 row1.put("invoice_cm_line_details_row["+(i-1)+"]",row.sort())
	 }
	 return row1.sort()
 }
 
 
 public LinkedHashMap md_void_cm_m(String str){
	 logi.logInfo ":::::::VOID_CREDIT_MEMO_M:::::::"
	 ConnectDB db = new ConnectDB()
	 LinkedHashMap row = new LinkedHashMap();
	 ArrayList<String> list = new ArrayList<String>()
	 String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
	 logi.logInfo("acct_no : "+acct_no)
	 String cm_no = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/cm_no[1]")
	 logi.logInfo("cm_no : "+cm_no)
	 String cquery = "SELECT count(*) as counts FROM ARIACORE.CREDIT_MEMO_DETAIL WHERE CM_NO = "+cm_no
	 int cq = Integer.valueOf(db.executeQueryP2(cquery).toString())
	 logi.logInfo "converted value of cq: "+cq
	 for(int i=1;i<=cq;i++){
		String sq = "SELECT CDD.CM_DETAIL_NO FROM (SELECT CM_DETAIL_NO, ROW_NUMBER() OVER (ORDER BY GL_SEQ_NUM) AS SEQ FROM ARIACORE.CREDIT_MEMO_DETAIL WHERE CM_NO = "+cm_no+")CDD WHERE CDD.SEQ = "+i
		list[i] = db.executeQueryP2(sq).toString()
		}
	  logi.logInfo "list size : "+list.size()
	  for(int i=1;i<list.size();i++){
		  LinkedHashMap row1 = new LinkedHashMap();
		  String sq1 = "SELECT EVENT_NO FROM ARIACORE.ACCT_TRANSACTION WHERE SOURCE_NO = "+list[i]+" AND TRANSACTION_TYPE = 24"
		  String cm_trnx_id = db.executeQueryP2(sq1).toString()
		  row1.put("cm_transaction_id",cm_trnx_id)
		  String sq2 = "SELECT EVENT_NO FROM ARIACORE.ACCT_TRANSACTION WHERE SOURCE_NO = "+list[i]+" AND TRANSACTION_TYPE = -24"
		  String cm_void_trnx_id = db.executeQueryP2(sq2).toString()
		  row1.put("cm_void_transaction_id",cm_void_trnx_id)
		  row.put("cm_void_transactions_row["+(i-1)+"]",row1.sort())
		  }
	  return row.sort()
 }

 public LinkedHashMap md_void_cm2_m(String param){
	 logi.logInfo ":::::::VOID_CREDIT_MEMO_M:::2:::::::"
	 ConnectDB db = new ConnectDB()
	 LinkedHashMap row = new LinkedHashMap();
	 String cm_no = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/cm_no[1]")
	 logi.logInfo("cm_no : "+cm_no)
	 String cm_det = "SELECT CREATE_USER AS CREATED_BY, TO_CHAR(CREATE_DATE,'YYYY-MM-DD') AS CREATED_DATE FROM ARIACORE.CREDIT_MEMO WHERE CM_NO = "+cm_no
	 ResultSet rs5=db.executePlaQuery(cm_det)
	 ResultSetMetaData md5 = rs5.getMetaData();
	 int columns5 = md5.getColumnCount();
	 while (rs5.next())
	 {
		 for(int l=1; l<=columns5; l++)
		 {
			 if((rs5.getObject(l))!=null)
			 {
				 row.put((md5.getColumnName(l)).toLowerCase(),(rs5.getObject(l)));
			}
		 }
	 }
	 return row.sort()
 }
 
 
 public LinkedHashMap md_gen_rb_m(String param){
	 logi.logInfo ":::::::GEN_RB_M::::::::"
	 ConnectDB db = new ConnectDB()
	 LinkedHashMap row = new LinkedHashMap();
	 LinkedHashMap row1 = new LinkedHashMap();
	 String acct_no = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
	 logi.logInfo("cm_no : "+acct_no)
	 String inv_query = "SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no
	 String invoice_no = db.executeQueryP2(inv_query).toString()
	 String inv_det = "SELECT INVOICE_NO,CREATE_USER as created_by, TO_CHAR(CREATE_DATE,'YYYY-MM-DD') AS CREATED_DATE, DEBIT AS AMOUNT,REBILL_REASON_CD AS REASON_CODE  FROM ARIACORE.GL WHERE INVOICE_NO = "+invoice_no
	 ResultSet rs5=db.executePlaQuery(inv_det)
	 ResultSetMetaData md5 = rs5.getMetaData();
	 int columns5 = md5.getColumnCount();
	 while (rs5.next())
	 {
		 for(int l=1; l<=columns5; l++)
		 {
			 if((rs5.getObject(l))!=null)
			 {
				 row.put((md5.getColumnName(l)).toLowerCase(),(rs5.getObject(l)));
			}
		 }
	 }
	 row1.put("gen_rb_mResponse[0]",row.sort())
	 return row1.sort()
 }
  
 public String md_cm_cash_calc_m (String param){
	 ConnectDB db = new ConnectDB()
	 ArrayList<String> list = new ArrayList<String>()
	 ArrayList<String> list2 = new ArrayList<String>()
	 String tax = "0.06"  //----//
	 String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
	 logi.logInfo("acct_no : "+acct_no)
	 String inv_no = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/orig_invoice_no[1]")
	 logi.logInfo("orig_invoice_no : "+inv_no)
	 boolean tr = true
	 double ds = tax.toDouble()  //----//
	 double temp
	 for(int i=1;i<=4 && tr == true;i++){
	 String line_no = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/*/*:cm_line_items_row["+i+"]/*:line_no[1]")
	 String cm_amt = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/*/*:cm_line_items_row["+i+"]/*:cm_line_amount[1]")
	 if(line_no.equals("NoVal")){
	  tr = false
	 }else{
	 list[i] = line_no
	 list2[i]= cm_amt
	   }
	 }
	/* for(int i=1;i<=cq;i++){
		 LinkedHashMap row = new LinkedHashMap();
		 String squery1 = "SELECT service_no FROM "+
							 "(SELECT ROW_NUMBER() OVER (ORDER BY CMTD.TAXED_SEQ_NUM,CMTD.SEQ_NUM) AS SEQ, FROM ARIACORE.CREDIT_MEMO_TAX_DETAIL CMTD WHERE CM_NO = "+cm_no+" AND CLIENT_NO = "+client_no+")CMT "+
							 "WHERE CMT.SEQ = "+i
							 def rr = rcs_rate.toDouble()
								  def prf = tax_rate.toDouble()
								  def uni = units.toDouble()
								  logi.logInfo "ASDF--"
								  def fv = (rr * prf)
								  def total_amount = (fv * uni)
				  
	 }*/
	 String inv_total = "SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO = "+inv_no
	 def inv_tot = db.executeQueryP2(inv_total).toString()
	 temp = inv_tot.toDouble()
	 String cm_count = "SELECT COUNT(*) AS COUNTS FROM ARIACORE.CREDIT_MEMO WHERE ACCT_NO = "+acct_no
	 int cm = Integer.parseInt(db.executeQueryP2(cm_count).toString())
	 if(cm>1){
		 //cumulation for more previous credit memos
		 for(int k = 1;k<(cm-1);k++){
		String cmone = "SELECT SS.CREDIT FROM (SELECT CREDIT,ROW_NUMBER() OVER (ORDER BY CM_NO ASC) AS SEQ FROM ARIACORE.CREDIT_MEMO WHERE ACCT_NO = "+acct_no+")SS WHERE SS.SEQ="+k
		Double cm_amount = (db.executeQueryP2(cmone).toString()).toDouble()
		temp = temp - cm_amount
		 }
	 }else{
	  logi.logInfo "temp: "+temp
	  temp = temp
	 }
   logi.logInfo "out loopssee "+temp
   //trnx calc with line item only
	 for(int i=1;i<list2.size();i++){
		double val = list2[i].toDouble()
		 temp = temp - val
	   }
	 //trans calc with tax
	 for(int i=1;i<list2.size();i++){
		 double val = list2[i].toDouble()
		  temp = temp - (val*ds)
		}
	 
	 logi.logInfo "inv_total : "+temp
	 
	 return temp.toString()
 }
 
 public String md_get_inv_cm_det_balance(String str){
	 ConnectDB db = new ConnectDB()
	 String acct_no=getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
	 logi.logInfo("acct_no : "+acct_no)
	 String inv_no = getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/invoice_no[1]")
	 logi.logInfo("invoice_no : "+inv_no)
	 if(str.equals("a")){
		 
	 }
	 
 }
 
 public LinkedHashMap md_verify_node_count_m(String s){
			logi.logInfo "inro md_verify_node_count_m***"
			ArrayList<String> list = new ArrayList<String>()
			LinkedHashMap <String,String> row = new LinkedHashMap<String,String>()
			int counter = 0,j = 1
			for( int i=0; i<s.length(); i++ ) {
				if( s.charAt(i) == '#' ) {
					counter++;
				} }
			counter = counter+1
			for(int i=0;i<counter;i++){
				list[j] = s.split("#")[i]
				logi.logInfo "in for "+list[j]
				row.put(list[j].split(",")[0],list[j].split(",")[1])
				j++}
		   logi.logInfo "hash : "+row.sort()
		   return row.sort()
		}
	   
 public   HashMap md_getSpecificnodeAsList02_m(String name1) throws Exception{
	 logi.logInfo "into md_getSpecificnodeAsList02_m"
	 logi.logInfo "param given "+name1+Sindex
	 String xmlRecords = library.Constants.Constant.RESPONSE
		LinkedList<String> temp2 = new LinkedList<String>();
		HashMap<String, String> hm = new HashMap<String, String>();
		HashMap<String, String> hm2 = new HashMap<String, String>();
		String temp = null;
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(xmlRecords));
		Document doc = db.parse(is);
		NodeList nodes = doc.getElementsByTagName(pnode);
		   Element element = (Element) nodes.item(Sindex);
		   
		NodeList name = element.getElementsByTagName(name1);
		int index=name.getLength();
		for(int k=index;k>0;k--)
		{ Element line = (Element) name.item(k-1);
		temp=getCharacterDataFromElement(line);
		
		   if(!temp.equals(null)||temp!=null)
		   {
				 temp2.add(temp);
				 if(k>1)
				 hm.put(name1.replace("ns1:","")+"["+(k)+"]",temp);
				 else
				 
					   hm.put(name1.replace("ns1:",""),temp);
		   }
		}
		hm2.put("Unit_Details",hm.sort())
		   return hm2;
 }
 
 public LinkedHashMap md_get_node_count02_m(String input)
 {
	   logi.logInfo "***into node_count02_m"
	   ArrayList<String> list = new ArrayList<String>()
	   int listlength = Integer.valueOf(input.split("#")[0])
	   String str = input.split("#")[1]
	   logi.logInfo "list length : "+listlength
	   int j = 1
	   for(int i=0;i<listlength;i++){
		   list[j] = str.split(",")[i]
		   logi.logInfo "in for"
		   j++
	   }
	  LinkedHashMap <String,String> row = new LinkedHashMap<String,String>()
	   logi.logInfo("into md_get_node_count_m")
	   
	  LinkedHashMap<String,String> inner = new LinkedHashMap<String,String>()
	  int sizecount = list.size()
	  logi.logInfo("sizecount" + sizecount)
	  for(int k=1;k<=sizecount-1;k++)
	  {
	  int acct_count=md_getSpecificnodeAsList_m(list[k]).size()
	  logi.logInfo(list[k]+" method caught accts "+acct_count)
	  row.put(list[k],acct_count)
	  }
	  //return acct_count.toString()
	  return row.sort()
 }

	 public LinkedHashMap md_plan_inst_det_m(String param){
	 ConnectDB db = new ConnectDB()
	 logi.logInfo "in plan instance details : "+param
	 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	 String str
	 if(param.split(",")[0].equals("n")){
		 str = ""
		 logi.logInfo "space:1_"+str+"2"
	 }else{
	 str = "+"+param.split(",")[0]
	 logi.logInfo " "+param.split(",")[0]
	 }
	 LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
	 LinkedHashMap <String, String> row2 = new LinkedHashMap<String, String>();
	 ArrayList<String> list = new ArrayList<String>()
	 int count=1
	 //1.acct_no
	 String acct_no = getValueFromRequest("get_acct_pln_unit_inst_all_m"+str,"//acct_no").toString()
	 logi.logInfo("acct_no" +acct_no )
	 String query = "select "+param.split(",")[1]+" as "+param.split(",")[2]+" from ariacore.plan_instance pi join ariacore.client_plan cp on pi.plan_no = cp.plan_no where pi.acct_no = "+acct_no+" and pi.client_no =" +client_no+ " order by plan_instance_no asc"
	 ResultSet rs=db.executePlaQuery(query)
	 ResultSetMetaData md = rs.getMetaData();
	 int columns = md.getColumnCount();
	 logi.logInfo "SECTOR 160002"
	 int i = 1
	 while (rs.next())
	 {
		 
			 if((rs.getObject(1))!=null)
			 {
				 //logi.logInfo ""+md.getColumnName(i).toLowerCase()+","+rs.getObject(i)
				 if(i == 1){
				 row.put((md.getColumnName(1)).toLowerCase(),(rs.getObject(1)));}
				 else{
					 row.put(md.getColumnName(1).toLowerCase()+"["+i+"]",(rs.getObject(1)));
				 }
			 }
		 i++
	 }
	 row2.put("Unit_Details",row.sort())
	 return row2
 }
				
 
 //DEV7187
 public LinkedHashMap md_getInvoicePeriod_DB(String input)
 {
  logi.logInfo "Calling md_getInvoicePeriod_DB "+input
  LinkedHashMap <String, String> val = new LinkedHashMap<String, String>();
	 boolean flg=false
	 String inv=im.md_get_invoice_no_m(input,null);
	 String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(input,null)
	//String acct_no=im.md_GET_ACCT_NO_m(input.split (",")[0],null).toString()
	String q="select to_char(bill_from_date,'yyyy-mm-dd') as \"from_date\",to_char(bill_thru_date,'yyyy-mm-dd') as \"to_date\" from ariacore.gl where invoice_no=%s"
	 ConnectDB db=new ConnectDB();
	 val=db.executeQueryP2(String.format(q, inv))
	 
	 return val
 }
 /**
  * Generic method to get value from request of any API
  * @param i (api,xpath,key)
  * @usage md_getValueFromRequest_param_assign_acct_plan_m,//alt_proration_start_date,from_date
  * @return hashmap
  */
public LinkedHashMap md_getValueFromRequest(String i)
{
	logi.logInfo "Calling md_getValueFromRequest "+i
	String apiname=i.split("&")[0]
	String xpath=i.split("&")[1]
	String k=i.split("&")[2]
	  LinkedHashMap <String, String> val = new LinkedHashMap<String, String>();
	  String res=getValueFromRequest(apiname,xpath)
	  logi.logInfo "res  "+res
	   val.put(k, res)
	
	   return val
}
public String md_IsPlanChargedOnInvoice(String input)
{
	boolean flg=false
	logi.logInfo "Calling md_IsPlanChargedOnInvoice "+input
	 String inv=im.md_get_invoice_no_m(input,null);
	 String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(input,null)
	 ConnectDB db=new ConnectDB();
	 String q="select count(*) from ariacore.gl_detail where invoice_no=%s  and plan_instance_no=%s"
	 int val=db.executeQueryP2(String.format(q, inv,plan_instance_no))
	 if(val>0)
	 flg=true
	 
	 
	
return flg.toString().toUpperCase()
	
}
/**
 * Used to calculate the proration factor if used days given as input
 * @param PlanInstanceNo&api
 * @return Proration Factor
 */
def md_ProrationFactorCalculationWithUsedDays_m(String input )
{
	String ins=input.split("&")[0]
	String api=input.split("&")[1]
	logi.logInfo "api "+api
	ClientParamUtils cp = new ClientParamUtils()
	//String usedays=input.split("&")[1]
	VerificationMethods vm =new VerificationMethods()
	SimpleDateFormat simpleFormat1 = new SimpleDateFormat("dd-mm-yyyy", Locale.ENGLISH)
	String proration_startdate_text=vm.getValueFromRequest(api,"//*:alt_proration_start_date")
	SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
	Date proration_startdate=originalFormat.parse(proration_startdate_text)
	String formatted_proration_startdate=simpleFormat1.format(proration_startdate)
	logi.logInfo "proration_startdate "+formatted_proration_startdate

	String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
	String PlanInstanceNo = im.md_GET_ACCT_OR_INSTANCE_NO_m(ins,null)
	
	String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	ConnectDB database = new ConnectDB();
	
	int masterplanInterval=database.executeQueryP2("SELECT  max(RECUR_BILLING_INTERVAL)  from ariacore.plan_rate_schedule  where plan_no=( select plan_no from ariacore.PLAN_INSTANCE where plan_instance_no =( SELECT parent_plan_instance_no from ariacore.PLAN_INSTANCE where plan_instance_no="+PlanInstanceNo+"))").toInteger()
	int suppplanInterval=database.executeQueryP2("SELECT  max(RECUR_BILLING_INTERVAL)  from ariacore.plan_rate_schedule  where plan_no=(SELECT plan_no from ariacore.PLAN_INSTANCE where plan_instance_no="+PlanInstanceNo+")").toInteger()
	
	String Query = "SELECT to_char(ARIACORE.ARIAVIRTUALTIME(" + clientNo + "),'yyyy-mm-dd') from DUAL"
	String currentVirtualTime = database.executeQueryP2(Query);
	
	logi.logInfo("Current VT - " +currentVirtualTime.toString())
	
	
	String to_date = database.executeQueryP2("select case when to_char(min(to_date+1),'YYYY-MM-DD') is null then ' ' else  to_char(min(to_date+1),'YYYY-MM-DD') end  from ariacore.gl where acct_no="+acct_no+" and TO_DATE('"+formatted_proration_startdate+"','DD-MM-YYYY') BETWEEN from_date AND to_date and INVOICE_TYPE_CODE !='P' ")
	//usedays =usedays
	logi.logInfo("to_date - " +to_date)
	int durationOfPlan = database.executeQueryP2("select trunc(to_date)-trunc(from_date)  from ariacore.gl where acct_no="+acct_no+" and TO_DATE('"+formatted_proration_startdate+"','DD-MM-YYYY') BETWEEN from_date AND to_date and rownum=1  and invoice_type_code !='P' order by invoice_no ").toInteger()
	
	def usedays,actualdurationOfPlan
	
	
	if(to_date.trim().length()>0)
	{
		logi.logInfo("Here")
	 usedays= cp.datediff(proration_startdate_text,to_date)
	 actualdurationOfPlan=durationOfPlan+1
	}
	else
	{
		logi.logInfo("No Here")
	 String usedays_temp= cp.datediff(proration_startdate_text,currentVirtualTime)
	 String monthdays=database.executeQueryP2("select substr( (last_day( ARIACORE.ARIAVIRTUALTIME( "+clientNo+")) ),1,2) from dual")
	 usedays=( monthdays.toInteger()-usedays_temp.toInteger() ).toString()
	 actualdurationOfPlan=durationOfPlan
	 
	}
	
	logi.logInfo("actualdurationOfPlan - " +actualdurationOfPlan)
	logi.logInfo("usedays - " +usedays)
	int actualUseDays=0
/*	if(usedays.toInteger()==30)
	 actualUseDays=usedays.toInteger()+1
	else	*/
	 actualUseDays=usedays.toInteger()
	
	 logi.logInfo("actualUseDays - " +actualUseDays)
	
	 if(masterplanInterval!=suppplanInterval)
	 {
		 int temp=database.executeQueryP2("select  (trunc(to_date('"+formatted_proration_startdate+"') ) -trunc(created) ) from ariacore.acct where acct_no="+acct_no).toInteger()
		 int totalDaysinMonth=database.executeQueryP2("select (substr(last_day('"+formatted_proration_startdate+"'),1,2) ) from ariacore.acct where acct_no="+acct_no).toInteger()
		 actualdurationOfPlan=totalDaysinMonth
		 actualUseDays=totalDaysinMonth-temp
		 
	 }
	def temp = (actualUseDays/(actualdurationOfPlan).toInteger())
	//def proration = 1 - temp
	def proration = temp
	DecimalFormat df
	if(api.contains("update"))
	 df= new DecimalFormat("0.##########");
	else
	df = new DecimalFormat("0.##########");
	
	logi.logInfo("proration - " +df.format(proration.toDouble()).toString())
	
	return df.format(proration.toDouble())
	
}
public LinkedHashMap md_getPlanAssignmentdate(String input)
{
	
	logi.logInfo "Calling md_getPlanAssignmentdate "+input
	LinkedHashMap <String, String> val = new LinkedHashMap<String, String>();
	//String i=input.split(",")[0]
	 //String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(i,null)
	 String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(input,null)
	 ConnectDB db=new ConnectDB();
	 String q="select to_char(provision_date,'yyyy-mm-dd') from ariacore.PLAN_INSTANCE where plan_instance_no=%s "
	 String x=db.executeQueryP2(String.format(q,plan_instance_no))
	 val.put("plan_assignment_date", x)
	 logi.logInfo("val"+val.toString());
return val
	
}

public LinkedHashMap md_getplansupdated_m(String input)
{
	
	logi.logInfo "Calling md_getplansupdated_m "+input
	LinkedHashMap <String, String> val = new LinkedHashMap<String, String>();
	String i=input.split(",")[0]
	 String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(i,null)
	 String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(input,null)
	 ConnectDB db=new ConnectDB();
	 String q="select p.plan_instance_no as \"plan_instance_no\" ,p.PLAN_INSTANCE_CDID \"client_plan_instance_id\" , a.plan_name \"plan_name\" ,p.plan_no \"plan_no\" from ariacore.PLAN_INSTANCE  p join ariacore.all_acct_active_plans a  on p.plan_instance_no=a.plan_instance_no where p.plan_instance_no=%s"
	 val=db.executeQueryP2(String.format(q,plan_instance_no))
	
	 
return val
	
}
public def md_get_invoice_billingdays_m(String param) throws Exception
{
//Param should contain MPI instance (for e.g 1,1M)
			logi.logInfo "into md_getShortOrLongInvoice_m"+param
			db=new ConnectDB()
			//String input= param.split("_")[1]
			String invoice_no=im.md_get_invoice_no_m(param,null).toString()
			logi.logInfo "into md_getShortOrLongInvoice_m///////////"+invoice_no
			String invoice_billingdays=db.executeQueryP2("SELECT TO_CHAR(TO_DATE(TO_CHAR(TO_DATE,'YYYY-MM-DD'),'YYYY-MM-DD') - TO_DATE(TO_CHAR(FROM_DATE,'YYYY-MM-DD'),'YYYY-MM-DD'))+1 AS days FROM ariacore.ALL_INVOICES WHERE invoice_no="+invoice_no).toString()
			logi.logInfo "into md_getShortOrLongInvoice_m............"+invoice_billingdays
			return invoice_billingdays
}


public def md_get_short_or_long_invoice_m (String param) throws Exception
{
			int max_days=31
			String invoice_type="Long Cycle Invoice"
			String invoice_type1="Short Cycle Invoice"
			db=new ConnectDB()
			String invoice_no=im.md_get_invoice_no_m(param,null).toString()
			logi.logInfo "into md_getShortOrLongInvoice_m///////////"+invoice_no
			String plan_interval_val=db.executeQueryP2("SELECT MAX(RECUR_BILLING_INTERVAL) from ariacore.plan_rate_schedule where plan_no IN (SELECT DISTINCT(PLAN_NO) from ariacore.GL_DETAIL where INVOICE_no="+invoice_no+")AND PLAN_DEFAULT_IND=1 AND CURRENCY_DEFAULT_IND=1")
			int interval= plan_interval_val.toInteger()
			int plan_interval=max_days * interval
			int invoice_interval=md_get_invoice_billingdays_m(param).toInteger()
			if(invoice_interval > plan_interval)
			{
			return invoice_type
			}
			else
			return invoice_type1
			
}

public def md_WaitforEvents_m(String param){
	Thread.sleep(120000)
//	for(int i=0;i<=1000;i++){
//		logi.logInfo "Delay method"
//	}
	return "Delay function"
}

def md_GetAllNestedChildNodesRnameOrder_m(String parentnode) throws Exception{
	
	logi.logInfo "Enter into md_GetAllNestedChildNodesRnameOrder_m"
	LinkedHashMap<String, String> listing = new LinkedHashMap<String, String>();
	LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
	LinkedHashMap<String, HashMap> outer1 = new LinkedHashMap<String, HashMap>();
	
	listing=md_getSpecificnodeAsList_m(parentnode.split(",")[0])

	int counts=listing.size()
	logi.logInfo "the count is here :"+counts
	for(int i=0;i<counts;i++)
	{

		outer=(md_getspecificchildnodesRname_m(parentnode,(i+1)))
		outer=outer.sort()
		outer1.put(parentnode.split(",")[0].replace("ns1:","")+"["+(i)+"]",outer)
		logi.logInfo "outer1 values :"+outer1
		outer1 = find_order(outer1,"tax_detail_line");
		outer = new LinkedHashMap<String, String>();
	}
	outer1=outer1.sort()
	return outer1
}

public def md_invoice_items_tax_specificDB_m(String param)
{
	ConnectDB db = new ConnectDB()
	LinkedHashMap line_item = new LinkedHashMap()
	LinkedHashMap row_item = new LinkedHashMap()
	HashMap hh=md_getSpecificnodeAsList_m("ns1:invoice_no")
	ArrayList<String> invoice_no = new ArrayList<String>(hh.values());
	Collections.sort(invoice_no)
	int loops=1
	int i = param.toInteger()-1
	String Query="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL  WHERE invoice_no="+invoice_no.get(i)+" order by TAX_DETAIL_LINE";
	//String Query="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL  WHERE invoice_no="+invoice_no.get(i)+" order by DEBIT";
	ResultSet resultSet = db.executePlaQuery(Query)
	if((db.executeQueryP2("select count(*) from ("+Query+")").toString()).equals("0"))
	Query="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL_PENDING  WHERE invoice_no="+invoice_no.get(i)+" order by TAX_DETAIL_LINE";
	//Query="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL_PENDING  WHERE invoice_no="+invoice_no.get(i)+" order by DEBIT";
	resultSet = db.executePlaQuery(Query)
	ResultSetMetaData md = resultSet.getMetaData()
	int columns = md.getColumnCount();
	while (resultSet.next())
	{
	
		 for(int u=1; u<=columns; ++u)
		{
			if(resultSet.getObject(u)!=null)
				line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
		}
		
		line_item.sort();
		row_item.put("tax_details_row["+(loops)+"]",line_item.sort())
		line_item = new LinkedHashMap();
		loops+=1;
	}
	row_item.sort();
	return row_item
	
}

LinkedHashMap md_GetNestedChildNodesOrder_m(String parentnode) throws Exception{
	logi.logInfo(parentnode);
	pnode=parentnode.split(",")[0]
	int indexss = parentnode.split(",")[1].toInteger()-1;
	parentnode=parentnode.split(",")[0]
	ArrayList<String> childnodes = new ArrayList<String>();
	ArrayList<ArrayList> all = new ArrayList<ArrayList>();
	LinkedHashMap<String, String> inner = new LinkedHashMap<String, String>();
	LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
	String xmlRecords = library.Constants.Constant.RESPONSE
	//Getting the first level of child nodes
	childnodes=getnode(xmlRecords,parentnode,indexss);
	logi.logInfo "Child Nodes"+childnodes
	int i=0;
	//for getting the second level of child nodes
	for (String number : childnodes)
	{
			   //pname=childnodes.get(i);
			 // all.add(getnode(xmlRecords,number,i));
		  //Sindex=(i+1)
			   inner=md_getAllnodesAsList_m(number+","+(i+1))
			   logi.logInfo "Inner Value in Main method"+inner
	   outer.put(number.replace("ns1:","")+"["+(i+1)+"]",inner)
		  inner = new LinkedHashMap<String, String>();
		  i++;
	}
	logi.logInfo "Call find_order to sort by tax_detail_line"
	logi.logInfo "Outer Value in Main method"+outer
	outer = find_order_test(outer,"tax_detail_line");
	   return outer
}


public  LinkedHashMap find_order_test (LinkedHashMap<String,LinkedHashMap> outer, String orderBy)
{
 List<String> InKeys = new ArrayList<>(outer.keySet());
 LinkedHashMap<String,LinkedHashMap> outers =  new LinkedHashMap<String,LinkedHashMap>();
 LinkedHashMap<String,String> out =  new LinkedHashMap<String,String>();
 LinkedHashMap<Integer,LinkedHashMap> incomming =  new LinkedHashMap<Integer,LinkedHashMap>();
 LinkedHashMap<Integer,Integer> income =  new LinkedHashMap<Integer,Integer>();
 
 Set<String> keys = outer.keySet();
 int i=1;
 for(String k:keys){
	 incomming.put(i,outer.get(k));
	 out=outer.get(k);
	 
	 logi.logInfo "outer value :" + outer;
	 logi.logInfo "out value :" + out;
	 logi.logInfo "i value :" + i;
	 logi.logInfo "k value :" + k;
	 logi.logInfo "income value :" + income;
	 income.put(i,Integer.parseInt(out.get(orderBy)));
	 
	 i++;
 }
 //System.out.println(income);
 List<Integer> Keys = new ArrayList<>(income.keySet());
 List<Integer> venum = new ArrayList<>();
 List<Integer> original = new ArrayList<>(income.values());
 List<Integer> sorted = new ArrayList<>(income.values());
 List<Integer> sorted2 = new ArrayList<>(income.values());
 Collections.sort(sorted);
 
 if(sorted.get(1)==sorted2.get(1))return outer;
 //System.out.println(original);
 //System.out.println(sorted);
 
 for(int s:sorted)
 {
	 for(int key:Keys)
	 {
		 if(s==income.get(key))venum.add(key);
	 }
	 
 }
 
 //System.out.println(venum);
 
 int z=0;
 for(String a:InKeys)
 {
	 outers.put(a,incomming.get(venum.get(z)));
	 z++;
 }
 return outers;
 }


def md_GetAllNestedChildNodesRnameAPI_m(String parentnode) throws Exception{
	LinkedHashMap<String, String> listing = new LinkedHashMap<String, String>();
	LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
	LinkedHashMap<String, HashMap> outer1 = new LinkedHashMap<String, HashMap>();
	
	listing=md_getSpecificnodeAsList_m(parentnode.split(",")[0])

	int counts=listing.size()
	logi.logInfo "the count is here :"+counts
	for(int i=0;i<counts;i++)
	{

		outer=(md_getspecificchildnodesRname_m(parentnode,(i+1)))
		outer=outer.sort()
		outer1.put(parentnode.split(",")[0].replace("ns1:","")+"["+(i)+"]",outer)
		outer = new LinkedHashMap<String, String>();
	}
	outer1=outer1.sort()
	return outer1
}

public def md_invoice_items_taxDB_sorted_m(String param)
{
	   ConnectDB db = new ConnectDB()
	   LinkedHashMap line_item = new LinkedHashMap()
	   LinkedHashMap row_item = new LinkedHashMap()
	   HashMap hh=md_getSpecificnodeAsList_m("ns1:invoice_no")
	   ArrayList<String> invoice_no = new ArrayList<String>(hh.values());
	   Collections.sort(invoice_no)
	   int loops=1
	   for(int i=0;i<invoice_no.size();i++)
	   {
			  //String Query="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL  WHERE invoice_no="+invoice_no.get(i)+" order by TAX_DETAIL_LINE";
			  String Query_1="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL  WHERE invoice_no="+invoice_no.get(i)+" order by DEBIT desc";
	   //String Query="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL  WHERE invoice_no="+invoice_no.get(i)+" order by DEBIT";
	   
		logi.logInfo "+++++++++++++++ here 1st query "+Query_1
	   ResultSet resultSet = db.executePlaQuery(Query_1)
	   //ResultSet resultSet = db.executePlaQuery(Query)
	   
	   //if((db.executeQueryP2("select count(*) from ("+Query+")").toString()).equals("0"))
	   if((db.executeQueryP2("select count(*) from ("+Query_1+")").toString().equalsIgnoreCase("0")))
	   
	   {
			  //Query="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL_PENDING  WHERE invoice_no="+invoice_no.get(i)+" order by TAX_DETAIL_LINE";
			  Query_1="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL_PENDING  WHERE invoice_no="+invoice_no.get(i)+" order by DEBIT desc";
	   //Query="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL  WHERE invoice_no="+invoice_no.get(i)+" order by DEBIT desc";
	   //Query="select TAX_DETAIL_LINE,SEQ_NUM,TAXED_SEQ_NUM,DEBIT,TAX_RATE,IS_EXCLUDED as orig_was_tax_inclusive,TAX_AUTHORITY as tax_authority_level,TAX_SRV_CAT_TEXT,TAX_SRV_JURIS_NM,TAX_SRV_TAX_TYPE_DESC,TAX_SRV_TAX_TYPE_ID from ariacore.GL_TAX_DETAIL_PENDING  WHERE invoice_no="+invoice_no.get(i)+" order by DEBIT";
	   logi.logInfo "for @@@@@@@@@@@@@@@@2ng query"+Query_1
	   }
	   
	   
	   //resultSet = db.executePlaQuery(Query)
	   resultSet = db.executePlaQuery(Query_1)
	   ResultSetMetaData md = resultSet.getMetaData()
	   int columns = md.getColumnCount();
	   while (resultSet.next())
	   {
	   
			  for(int u=1; u<=columns; ++u)
			  {
					if(resultSet.getObject(u)!=null)
						   line_item.put(md.getColumnName(u).toLowerCase(),resultSet.getObject(u));
			  }
			  
			  line_item.sort();
			  row_item.put("tax_details_row["+(loops-1)+"]",line_item.sort())
			  line_item = new LinkedHashMap();
			  loops+=1;
	   }
	   }
	   row_item.sort();
	   return row_item
	   
}

public  String md_mm_yyyy_m(String input)
{
	   InputMethods im = new InputMethods()
	   logi.logInfo("In md_xml_stmt_no_m ")
	   //client no
	   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
	   logi.logInfo("CLINET "+client_no)
	   //acct no
	   //def acct_no=vm.getValueFromRequest(library.Constants.Constant.SERVICENAME, "//*/acct_no[1]")
	   def acct_no= im.md_GET_ACCT_OR_INSTANCE_NO_m(input,null).toString()

	   //db connetion
	   logi.logInfo("ACCT NOOO "+acct_no)
	   ConnectDB db = new ConnectDB()
	   
	   String xml_statementno="update ariacore.billing_info SET CC_EXPIRE_MM=10 ,CC_EXPIRE_YYYY=2050 where client_no="+client_no+"AND acct_no="+acct_no
	   String statement_no = db.executeQueryP2(xml_statementno)
	   logi.logInfo("statement_no "+statement_no)
	   
	   return statement_no
	   
	   
}
public def md_invoice_amount_in_bg (String param) throws Exception
{
	db=new ConnectDB()
	String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
	logi.logInfo"in invoice_verify"+param
	String amount= db.executeQueryP2("select NVL(SUM(DEBIT),0) AS INVOICE_TOTAL_AMOUNT from ariacore.gl where invoice_no in(select max(invoice_no) from ariacore.gl where acct_no="+ acct_no +" and BILLING_GROUP_NO  in(select BILLING_GROUP_NO from ariacore.ACCT_BILLING_GROUP where acct_no="+acct_no+" and PROFILE_NAME='"+ param +"' ))")
	return amount
}

// Method to verify number of days in invoice
public def md_get_no_of_days_invoice_m (String param) throws Exception
//param should contain Api and MPI (for eg:_param_CAC#1,1m)
{
	  db=new ConnectDB()
	  ClientParamUtils cp = new ClientParamUtils()
	  VerificationMethods vm =new VerificationMethods()
	  String Api=param.split('#')[0]
	  String input=param.split('#')[1]
	  logi.logInfo"coming  "+Api+"what came"+input
	  String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
	  String retro_active_startdate = null
	  switch(Api)
	  {
	  case "CAC":
							  retro_active_startdate=vm.getValueFromRequest("create_acct_complete_m","//*/*:retroactive_start_date[1]")
							  logi.logInfo"comingcac"+retro_active_startdate
							  break;
	  case "ASSIGN":
							  logi.logInfo"check point"
							  retro_active_startdate=vm.getValueFromRequest("assign_acct_plan_m","//*/*:retroactive_start_date[1]")
							  logi.logInfo"comingassign"+retro_active_startdate
							  break;
	  case "UPDATE":
							  retro_active_startdate=vm.getValueFromRequest("update_acct_complete_m","//*/*:retroactive_start_date[1]")
							  logi.logInfo"comingupdate"+retro_active_startdate
							  break;
	  case "UPDATEMULTI":
							  retro_active_startdate=vm.getValueFromRequest("update_acct_plan_multi_m","//*/*:retroactive_start_date[1]")
							  logi.logInfo"comingumulti"+retro_active_startdate
							  break;
	  case "REPLACE":
							  retro_active_startdate=vm.getValueFromRequest("replace_acct_plan_m","//*/*:retroactive_start_date[1]")
							  logi.logInfo"comingreplace"+retro_active_startdate
							  break;
	  }
	  String to_date=db.executeQueryP2("SELECT TO_CHAR(TO_DATE+1,'YYYY-MM-DD') from ariacore.gl where acct_no="+acct_no)
	  logi.logInfo "To date" +to_date
	  String days= cp.datediff(retro_active_startdate,to_date)
	  return days
	  }


public String md_credit_applied_m(String s){
	ConnectDB db = new ConnectDB()
	String str1 = s.split(",")[0]
	String str2 = s.split(",")[1]
	String amount = ""
	String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(str1,null)
	if(str2.equals("T")){
		String query = "SELECT TO_CHAR((("+
		 "SELECT AMOUNT FROM ARIACORE.CREDITS WHERE CREDIT_ID IN (SELECT MAX(CREDIT_ID) FROM ARIACORE.CREDITS WHERE ACCT_NO = "+acct_no+")) -"+
		 " (SELECT SUM(DEBIT) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO IN (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+"))),'fm99990.99') AS AMOUNT FROM DUAL"
		amount = db.executeQueryP2(query).toString()
		logi.logInfo "amount in T: "+amount
	}
	else  if(str2.equals("TR")){
		String query = "SELECT TO_CHAR((("+
	 " SELECT AMOUNT FROM ARIACORE.CREDITS WHERE CREDIT_ID IN (SELECT MAX(CREDIT_ID) FROM ARIACORE.CREDITS WHERE ACCT_NO = "+acct_no+")) -"+
	 " (SELECT SUM(GLD.DEBIT) FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.CLIENT_SERVICE CS ON GLD.SERVICE_NO = CS.SERVICE_NO AND GLD.CLIENT_NO = CS.CLIENT_NO AND CS.SERVICE_TYPE = 'RC'"+
	 " WHERE GLD.INVOICE_NO IN (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+")))) AS AMOUNT FROM DUAL"
		amount = db.executeQueryP2(query).toString()
		logi.logInfo "amount in TR: "+amount
	}
	else  if(str2.equals("TRA")){
		String query = "SELECT TO_CHAR((("+
	 " SELECT AMOUNT FROM ARIACORE.CREDITS WHERE CREDIT_ID IN (SELECT MAX(CREDIT_ID) FROM ARIACORE.CREDITS WHERE ACCT_NO = "+acct_no+")) -"+
	 " (SELECT SUM(GLD.DEBIT) FROM ARIACORE.GL_DETAIL GLD JOIN ARIACORE.CLIENT_SERVICE CS ON GLD.SERVICE_NO = CS.SERVICE_NO AND GLD.CLIENT_NO = CS.CLIENT_NO AND (CS.SERVICE_TYPE = 'RC' OR SERVICE_TYPE = 'AC')"+
	 " WHERE GLD.INVOICE_NO IN (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+")))) AS AMOUNT FROM DUAL"
		amount = db.executeQueryP2(query).toString()
		logi.logInfo "amount in TR: "+amount
	}else  if(str2.equals("TRc")){
		String query = "SELECT TO_CHAR((("+
		 " SELECT AMOUNT FROM ARIACORE.CREDITS WHERE CREDIT_ID IN (SELECT MAX(CREDIT_ID) FROM ARIACORE.CREDITS WHERE ACCT_NO = "+acct_no+")) -"+
		 " (SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE SERVICE_NO NOT IN (401,402,405,0) AND INVOICE_NO IN (SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = "+acct_no+")"+
		 " ))) AS AMOUNT FROM DUAL"
		amount = db.executeQueryP2(query).toString()
		logi.logInfo "amount in TR: "+amount
	}else  if(str2.equals("TRAc")){
		String query = "SELECT TO_CHAR((( SELECT AMOUNT FROM ARIACORE.CREDITS WHERE CREDIT_ID IN (SELECT MAX(CREDIT_ID) FROM ARIACORE.CREDITS WHERE ACCT_NO = "+acct_no+")) + (select sum(debit) from ariacore.gl_detail where invoice_no in (select max(invoice_no) from ariacore.gl where acct_no = "+acct_no+") and service_no in (0) ))) AS AMOUNT FROM DUAL"
		amount = db.executeQueryP2(query).toString()
		logi.logInfo "amount in TR: "+amount
	}
	
	return amount
}

public String md_ToTal_Bal_Fwd_m(String s){
	ConnectDB db = new ConnectDB()
	String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO2_m(s,null)
	String query = "SELECT TO_CHAR(S.AMT,'FM99999.90') AS AMOUNT FROM (SELECT (MAX(AMOUNT)-MIN(AMOUNT))AS AMT FROM ARIACORE.ACCT_TRANSACTION WHERE TRANSACTION_TYPE IN (18,9) AND ACCT_NO = "+acct_no+")S"
	String amount = db.executeQueryP2(query).toString()
	logi.logInfo "amount val: "+amount
	return amount
}

public String md_OverRideBill_Proration_amount_m(String val)
{
	logi.logInfo("Proration Amount Assigning the supplemental plan")
	String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
	DecimalFormat d = new DecimalFormat("0.##");
	String[] plan_no
	logi.logInfo("Client No ====>"+ clientNo)
	def invoiceAmount = 0
	String[] format = val.split('#')
	for(int i=0;i<format.size();i++){
		
	val =  format[i]
	String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
	logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
	ConnectDB database = new ConnectDB();
	SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
	String Query = "SELECT ARIACORE.ARIAVIRTUALTIME(" + clientNo + ") from DUAL"
	String currentVirtualTime = database.executeQueryP2(Query);
	logi.logInfo "Current Virtual Time : "+ currentVirtualTime
	//Getting master plan instance
	String Master_plan_instance_query = "SELECT MASTER_PLAN_INSTANCE_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	String Master_plan_instance = database.executeQueryP2(Master_plan_instance_query);
	logi.logInfo "Master Plan Instance No : "+ Master_plan_instance
	def proration_factor = md_ProrationFactorCalculation_Override_m(plan_instance_no)
	d.format(proration_factor)
	logi.logInfo "Proration Factor: "+ proration_factor
	String plan_query = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	plan_no = database.executeQueryP2(plan_query);
	logi.logInfo "Plan Number: "+ plan_no
	String plan_query1 = "SELECT PLAN_NO FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO ="+plan_instance_no
	String plan_no1 = database.executeQueryP2(plan_query1);
	logi.logInfo "Plan Number1: "+ plan_no1
	Map<String, HashMap<String,String>> hash
	Map<String, Map<String,String>> resultHash = new TreeMap<String, TreeMap<String,String>> ()
	Iterator iterator
	hash = getPlanServiceTiredPricing(plan_no,plan_instance_no)
	resultHash = new TreeMap<String, TreeMap<String,String>> ()
	resultHash.putAll(hash)
	logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	iterator = resultHash.entrySet().iterator()
	logi.logInfo "Before Key setting "
	logi.logInfo "ResultHash Sorted in Method: "+ resultHash.toString()
	def totalAmount = 0
	while (iterator.hasNext())
	{
	
		Map.Entry pairs = (Map.Entry) iterator.next()
		String planNo = pairs.getKey().toString()
		logi.logInfo "Execution for the plan :  "+ planNo
		
		Map<String,String> valueMap = pairs.getValue()
		Map<String,String> value = new TreeMap<String,String> ()
		value.putAll(valueMap)
		Iterator service = value.entrySet().iterator()
		String unitsquery="SELECT PLAN_UNITS FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO=" + plan_instance_no
		int noofUnits =database.executeQueryP2(unitsquery).toInteger()
		logi.logInfo "no_of_unitsssss : "+noofUnits+ " plan : " +planNo
	
		while(service.hasNext())
		{
			int ChargeMultipleAct = 0
			Map.Entry servicePairs = (Map.Entry) service.next()
			String serviceNo = servicePairs.getKey().toString()
			logi.logInfo "Invoice calculation for a service : "+serviceNo
			String srvqry="SELECT SERVICE_TYPE FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO ="+serviceNo+ "AND CLIENT_NO="+clientNo
			String srvctype=database.executeQueryP2(srvqry)
			logi.logInfo "Service type: "+srvctype
			String flag_qry = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT' AND CLIENT_NO="+clientNo
			String fla_val=database.executeQueryP2(flag_qry)
			logi.logInfo "CHARGE_ACTIVATION_FEES_ON_NEW_SUPP_PLAN_ASSIGNMENT : "+fla_val
			String flag_qry1 = "SELECT PARAM_VAL FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME='MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT' AND CLIENT_NO="+clientNo
			String fla_val1=database.executeQueryP2(flag_qry1)
			logi.logInfo "MULTIPLY_ACTIVATION_FEES_BY_PLAN_UNIT : "+fla_val1
			if (srvctype=='AC' && fla_val== 'TRUE')
			{
				logi.logInfo "Entering into if loop"
				if(fla_val1 == 'TRUE')
				{
					ChargeMultipleAct = 1
					logi.logInfo "Entering into inner if loop"
				}
				else if(fla_val1 == 'FALSE')
				{
					ChargeMultipleAct = 2
					logi.logInfo "Entering into inner clerif loop"
				}
			}
			logi.logInfo "ChargeMultipleAct Value" +   ChargeMultipleAct
			
			int seqNo=1;
			String tieredPricing = servicePairs.getValue().toString()
			int tieredPricingInt = Integer.parseInt(tieredPricing)
			logi.logInfo "Invoice calculation for a TP : "+tieredPricingInt
			String altFee = "NoVal"
			double serviceInvoice
			
			String query
			switch(tieredPricingInt)
			{
				case 1:
					if(ChargeMultipleAct==2)
					{
						query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						logi.logInfo "Service Amount Calculation" +   serviceInvoice
						totalAmount = totalAmount + serviceInvoice
						logi.logInfo "Total Amount Calculation" +   totalAmount
						break;
						
					}
					else if(ChargeMultipleAct==1)
					{
						query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo
						serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						logi.logInfo "Service Amount Calculation" +   serviceInvoice
						totalAmount = totalAmount + serviceInvoice
						logi.logInfo "Total Amount Calculation" +   totalAmount
						break;
						
					}
					else if (srvctype != 'MN')
					{
						query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no =" +plan_no1+ "and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and NRSR.RATE_SEQ_NO="+seqNo
						serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						double total = (serviceInvoice * proration_factor)
						logi.logInfo "total amount " + total
						totalAmount = totalAmount + total
						logi.logInfo "Total Amount Calculation" +   totalAmount
						break;
					}
				case 2:
						query = "select rate_per_unit * "+noofUnits+" from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						double total = (serviceInvoice * proration_factor)
						logi.logInfo "total amount " + total
						totalAmount = totalAmount + total
						logi.logInfo "Total Amount Calculation" +   totalAmount
						break;
				case 3:
						query = "select rate_per_unit * 1 from ariacore.new_rate_schedules NRS join ariacore.new_rate_sched_rates NRSR on NRS.schedule_no = NRSR.schedule_no where NRS.plan_no = "+plan_no1+" and nrs.client_no="+clientNo+" and service_no = "+serviceNo+" and ("+noofUnits+" between from_unit and to_unit or to_unit is null) and rownum = 1"
						serviceInvoice = Integer.parseInt(database.executeQueryP2(query))
						double total = (serviceInvoice * proration_factor)
						logi.logInfo "total amount " + total
						totalAmount = totalAmount + total
						logi.logInfo "Total Amount Calculation" +   totalAmount
						break;
			}
		}
		logi.logInfo "Total Amount Calculation Final" +   totalAmount
	}
	invoiceAmount = invoiceAmount + totalAmount
	}
	
	return d.format(invoiceAmount)
	
}




public String md_OverrideExpectedDate(String text)
{
	logi.logInfo("Enter into uk block : "+library.Constants.Constant.SERVICENAME)
	  String overridePlanInstanceNo = getValueFromRequest(library.Constants.Constant.SERVICENAME,"//*/*:override_dates_mp_instance_no[1]").toString()
	  
	  logi.logInfo("override_dates_mp_instance_no "+overridePlanInstanceNo)
	  String planInstanceNo = getValueFromResponse(library.Constants.Constant.SERVICENAME,"//*/plan_instance_no").toString()
	  logi.logInfo("plan_instance_no "+planInstanceNo)
	  ConnectDB database = new ConnectDB();
	  String Query1 ="select rs.RECUR_BILLING_INTERVAL from ariacore.plan_instance pi join "+
					  "ariacore.client_plan cp on pi.plan_no = cp.plan_no and pi.client_no = cp.client_no Join ariacore.NEW_RATE_SCHEDULES rs "+
					 " ON pi.schedule_no  =rs.schedule_no and pi.client_no=rs.client_no where pi.plan_instance_no ="+overridePlanInstanceNo
	  int overrideRecInt= database.executeQueryP2(Query1).toInteger();
	  logi.logInfo("overrideRecurringInterval "+overrideRecInt)
	  String Query2 ="select rs.RECUR_BILLING_INTERVAL from ariacore.plan_instance pi join"+
					 " ariacore.client_plan cp on pi.plan_no = cp.plan_no and pi.client_no = cp.client_no Join ariacore.NEW_RATE_SCHEDULES rs"+
					 " ON pi.schedule_no  =rs.schedule_no and pi.client_no=rs.client_no where pi.plan_instance_no ="+planInstanceNo
	  int newRecInt= database.executeQueryP2(Query2).toInteger();
	  logi.logInfo("NewRecurringInterval "+newRecInt)
	  String Query = "select pi.NEXT_BILL_DATE from ariacore.plan_instance pi join ariacore.client_plan cp"+
					  " on pi.plan_no = cp.plan_no and pi.client_no = cp.client_no Join ariacore.NEW_RATE_SCHEDULES rs ON pi.schedule_no  =rs.schedule_no"+
					  " and pi.client_no=rs.client_no where pi.plan_instance_no ="+overridePlanInstanceNo
	  String nextBillDate= database.executeQueryP2(Query).toString();
	  Date billThruDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(nextBillDate);
	  Calendar cal = Calendar.getInstance();
	  String overrideDate_CAC = getValueFromRequest("create_acct_complete_m","//*/*:override_bill_thru_date[1]").toString()
	  cal.setTime(billThruDate);
	  logi.logInfo("overrideDate_CAC _UK"+overrideDate_CAC)
	  if(text.contains("Over_Proration"))
	  { cal.add(Calendar.DATE,-1);
		  if((newRecInt<overrideRecInt)&&overrideDate_CAC=="NoVal")
		  { logi.logInfo("recInterval shorter")
			while(overrideRecInt - 1>=newRecInt)
			 {
			  overrideRecInt = overrideRecInt - newRecInt
			  logi.logInfo("overrideRecInt_uk "+overrideRecInt)
			  logi.logInfo("newRecInt_UK "+newRecInt)
			  cal.add(Calendar.MONTH,-(newRecInt))
			 }
		  }
	  }
	  if(text.equalsIgnoreCase("Over"))
	  { cal.add(Calendar.DATE,-1);
		  /*if(newRecInt<overrideRecInt)
		  { logi.logInfo("recInterval shorter")
			while(overrideRecInt>newRecInt)
			 {
			  overrideRecInt = overrideRecInt - newRecInt
			  cal.add(Calendar.MONTH,-(newRecInt))
			 }
		  }*/
	  }
	  if(text.contains("Bill")||text.contains("1"))
	  { cal.add(Calendar.DATE,-1);
	  }
	  if(text.contains("LC"))
	  {
		  logi.logInfo("Entered into LC")
	  
	  if(newRecInt<overrideRecInt)
	   { logi.logInfo("recInterval shorter")
		 while(overrideRecInt>newRecInt)
		  {
		   overrideRecInt = overrideRecInt - newRecInt
		   cal.add(Calendar.MONTH,-(newRecInt))
		  }
	   }
	   cal.add(Calendar.DATE,-1);
	   }
   SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
   String expectedDate = format1.format(cal.getTime());
   
	return(expectedDate)
 }


public String md_ActOverRideBill_NextBillDate_m(String val)
{
	logi.logInfo("Actual Next Bill date")
	String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
	DecimalFormat d = new DecimalFormat("0.##");
	String[] plan_no
	logi.logInfo("Client No ====>"+ clientNo)
	def invoiceAmount = 0
	
	String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
	logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
	ConnectDB database = new ConnectDB();
	
	String Query = "select TO_CHAR(pi.NEXT_BILL_DATE,'YYYY-MM-DD') from ariacore.plan_instance pi join ariacore.client_plan cp on pi.plan_no = cp.plan_no and pi.client_no = cp.client_no Join ariacore.NEW_RATE_SCHEDULES rs ON pi.schedule_no  =rs.schedule_no and pi.client_no=rs.client_no where pi.plan_instance_no ="+plan_instance_no
	String nextBillDate = database.executeQueryP2(Query);
	//Date nextBillDate1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(nextBillDate);
	
	//SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
	//Calendar cal = Calendar.getInstance();
	//cal.setTime(nextBillDate1);
   
	//String dateMonthsAdded = format1.format(cal.getTime());
	 //logi.logInfo("nextBillDate1 date - " +dateMonthsAdded)
	 //return dateMonthsAdded.toString()
	 return nextBillDate
 }
 
 
public String md_ExpOverRideBill_NextBillDate_m(String val)
{
	   logi.logInfo("Expected Bill date")
	   String clientNo =Constant.mycontext.expand('${Properties#Client_No}')
	   DecimalFormat d = new DecimalFormat("0.##");
	   String[] plan_no
	   logi.logInfo("Client No ====>"+ clientNo)
	   def invoiceAmount = 0
	   String[] values = val.split('#')
	   val = values[0]
	   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(val,null)
	   logi.logInfo("PlanIntsnaceNo ====>"+ plan_instance_no)
	   ConnectDB database = new ConnectDB();
	   Calendar cal = Calendar.getInstance();
	   String override_bill_thru_date = getValueFromRequest(library.Constants.Constant.SERVICENAME,"//lit:override_bill_thru_date").toString()
	   if (override_bill_thru_date== "NoVal")
	   {
		override_bill_thru_date = getValueFromRequest(library.Constants.Constant.SERVICENAME,"//override_bill_thru_date").toString()
		
	   }
	   logi.logInfo("Test override_bill_thru_date "+override_bill_thru_date)
	   logi.logInfo("Test values[1] "+values[1])
	   if ((override_bill_thru_date == "NoVal"&&(values[1].contains("Ins")))|| (override_bill_thru_date == "NoVal"&& (values[1].contains("UB")))|| (override_bill_thru_date == "NoVal"&& (values[1].contains("1"))))
	   {
		   override_bill_thru_date = md_OverrideExpectedDate("Bill")
	   }
	   if ((override_bill_thru_date == "NoVal"&&(values[1].contains("LC"))))
	   {
		   override_bill_thru_date = md_OverrideExpectedDate("LC")
	   }
	   if(override_bill_thru_date !=null || (override_bill_thru_date != "NoVal")){
	   logi.logInfo("Test override_bill_thru_date "+override_bill_thru_date)
	   Date override_bill_thru_date1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(override_bill_thru_date);
	   logi.logInfo("over ride VT - " +override_bill_thru_date1)
	   cal.setTime(override_bill_thru_date1);
	   }
	   logi.logInfo("values[1] :"+values[1])
	   if(values[1].contains("UB"))
	   {
		   String Query = "select rs.RECUR_BILLING_INTERVAL as RBI,rs.USAGE_BILLING_INTERVAL AS UBI from ariacore.plan_instance pi join ariacore.client_plan cp on pi.plan_no = cp.plan_no and pi.client_no = cp.client_no Join ariacore.NEW_RATE_SCHEDULES rs ON pi.schedule_no  =rs.schedule_no and pi.client_no=rs.client_no where pi.plan_instance_no ="+plan_instance_no
		   LinkedHashMap interval = database.executeQueryP2(Query);
		   logi.logInfo("Recu Interval "+interval.get("RBI"))
		   logi.logInfo("Usage Interval "+interval.get("UBI"))
		   logi.logInfo("test2")
		   cal.add(Calendar.MONTH,interval.get("RBI").toInteger())
		   logi.logInfo("test1")
		   int recuInt = interval.get("RBI").toInteger()
		   int useInt = interval.get("UBI").toInteger()
		   logi.logInfo("Before while loop")
		   if(recuInt>useInt){
		   recuInt = recuInt - useInt
		   logi.logInfo("Inside while loop")
				  if(recuInt == 1){
						cal.add(Calendar.MONTH,-(useInt))
						cal.add(Calendar.MONTH,-1)}
				  else if(recuInt == 3){
						cal.add(Calendar.MONTH,-(useInt))
						cal.add(Calendar.MONTH,-3)}
				  
		   logi.logInfo("Inside while loop1")
		   }
	else if(recuInt == useInt){
		   //recuInt = recuInt - useInt
		   logi.logInfo("Inside while loop_equal")
		   logi.logInfo("Check")
		   cal.add(Calendar.MONTH,-(recuInt))
		   logi.logInfo("Check1")
		   cal.add(Calendar.MONTH,+1)
		   logi.logInfo("Inside while loop1_equal")
		   }

		   
	   }
	   
	   if(values[1].contains("RBI"))
	   {
	   String Query = "select rs.RECUR_BILLING_INTERVAL as RBI,rs.USAGE_BILLING_INTERVAL AS UBI from ariacore.plan_instance pi join ariacore.client_plan cp on pi.plan_no = cp.plan_no and pi.client_no = cp.client_no Join ariacore.NEW_RATE_SCHEDULES rs ON pi.schedule_no  =rs.schedule_no and pi.client_no=rs.client_no where pi.plan_instance_no ="+plan_instance_no
	   LinkedHashMap interval = database.executeQueryP2(Query);
	   logi.logInfo("Recu Interval "+interval.get("RBI"))
	   logi.logInfo("Usage Interval "+interval.get("UBI"))
	   cal.add(Calendar.MONTH,interval.get("RBI").toInteger())
	   }
		   cal.add(Calendar.DATE,1)
		   SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		   String dateMonthsAdded = format1.format(cal.getTime());
	   return dateMonthsAdded.toString()
	}



def md_ProrationFactorCalculation_Override_m(String PlanInstanceNo)
{
	
   String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
   ConnectDB database = new ConnectDB();
   SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
   String Query = "SELECT TRUNC(ARIACORE.ARIAVIRTUALTIME(" + clientNo + ")) from DUAL"
   String currentVirtualTime = database.executeQueryP2(Query);
   Date currentVTDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(currentVirtualTime);
   logi.logInfo("Current VT - " +currentVirtualTime)
   //String LastBillThruDateQry = "SELECT TRUNC(LAST_BILL_THRU_DATE+1) FROM ARIACORE.PLAN_INSTANCE WHERE PLAN_INSTANCE_NO =" + PlanInstanceNo
   String LastBillThruDateStr = getValueFromRequest(library.Constants.Constant.SERVICENAME,"//lit:override_bill_thru_date").toString()
   if (LastBillThruDateStr== "NoVal")
   {
	LastBillThruDateStr = getValueFromRequest(library.Constants.Constant.SERVICENAME,"//override_bill_thru_date").toString()
	
   }
   if (LastBillThruDateStr== "NoVal")
	 {
	  LastBillThruDateStr = md_OverrideExpectedDate("Over_Proration")
	  
	 }
	 
   logi.logInfo("Test LastBillThruDateStr "+LastBillThruDateStr)
   Date LastBillThruDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(LastBillThruDateStr);
   logi.logInfo("over ride VT - " +LastBillThruDate)
   String StartDateStr = null;
   if(library.Constants.Constant.SERVICENAME=="create_acct_complete_m"){
	   StartDateStr = getValueFromRequest(library.Constants.Constant.SERVICENAME,"//lit:retroactive_start_date")
   }else
   StartDateStr = getValueFromRequest(library.Constants.Constant.SERVICENAME,"//retroactive_start_date")
   if(StartDateStr.contains("NoVal")){
		  logi.logInfo("Enter 2- " )
		  StartDateStr = getValueFromRequest(library.Constants.Constant.SERVICENAME,"//alt_start_date")
		  logi.logInfo("Enter 1- "+ StartDateStr)
		  }
		  if(StartDateStr.contains("NoVal"))
		   {
			 logi.logInfo("Enter - " )
			 StartDateStr = currentVirtualTime
			 logi.logInfo("Exit - " +StartDateStr)
		   }
   Date StartDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(StartDateStr);
   logi.logInfo("Start date - " +StartDate)
   int durationOfPlan = getDaysBetween(StartDate,LastBillThruDate )
   durationOfPlan = durationOfPlan+1
   logi.logInfo("durationOfPlan - " +durationOfPlan)
   //String BillingIntervalQry ="select cp.billing_interval from ariacore.plan_instance pi join ariacore.client_plan cp on pi.plan_no = cp.plan_no and pi.client_no = cp.client_no where pi.plan_instance_no ="+PlanInstanceNo
   String BillingIntervalQry ="select rs.recur_billing_interval from ariacore.plan_instance pi join ariacore.client_plan cp on pi.plan_no = cp.plan_no and pi.client_no = cp.client_no Join ariacore.NEW_RATE_SCHEDULES rs ON pi.schedule_no  =rs.schedule_no and pi.client_no=rs.client_no where pi.plan_instance_no ="+PlanInstanceNo
   int BillingInterval = database.executeQueryP2(BillingIntervalQry).toInteger();
   String billdayquery ="select bill_day from ariacore.plan_instance_master where plan_instance_no = (select master_plan_instance_no from ariacore.plan_instance where plan_instance_no = "+PlanInstanceNo+")"

   def Billday = database.executeQueryP2(billdayquery).toInteger();
   //Date virtualstartdate = subractMonthsFromGivenDate1(LastBillThruDate,BillingInterval,Billday)
   Date virtualstartdate = subractMonthsFromGivenDate1(LastBillThruDate,BillingInterval,Billday)
   def usedays = getDaysBetween(virtualstartdate, LastBillThruDate)
   //usedays =usedays+1
   logi.logInfo("usedays - " +usedays)
   logi.logInfo("Current VT - " +virtualstartdate)
   def proration = (durationOfPlan.toInteger()/usedays.toInteger())
   logi.logInfo("proration - " +proration)
   
   return proration
   
}




//end of the class
}
