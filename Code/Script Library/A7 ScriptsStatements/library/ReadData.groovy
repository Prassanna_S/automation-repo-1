package library
import com.lowagie.text.Row;

import org.apache.poi.hssf.record.aggregates.WorksheetProtectionBlock;
import org.apache.poi.hssf.usermodel.*;

import java.util.List;
import java.util.Iterator;
import java.util.HashMap;
import java.awt.geom.Arc2D.Float;
import java.lang.reflect.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import library.Constants.*;
import library.GenericLibrary.*;

import java.sql.ResultSet;

import org.apache.commons.lang.StringUtils;

class ReadData {
	def sourceFile
	InputStream inStr
	HSSFWorkbook workbook
	LogResult logi = new LogResult(Constant.logFilePath)
	InputMethods objInputmethod=new InputMethods()
	VerificationMethods vm =new VerificationMethods()

	public String TD_assignmentdirective = null
	public String TD_cancelSuppPlan = null
	public String TD_doWriteCancel = null
	public String TD_commentsCancel = null
	public String TD_clientReceipentId = null
	public String TD_altProrationRate = null
	public String TD_altCallerIdCancel = null
	public String TD_effectiveDate = null
	public String TD_clientNo = null
	public String TD_testCaseID = null
	public String TD_testCaseDesc = null
	public String TD_authKey = null
	public String TD_serviceOrder = null
	public String TD_serviceOrder1 = null
	public String TD_userid = null
	public String TD_status_cd = null
	public String TD_masterPlanNo =null
	public String TD_masterPlanUnits = null
	public String TD_suppPlan1 = null
	public String TD_suppPlan2 = null
	public String TD_suppPlan3 = null
	public String TD_suppPlanUnits1 = null
	public String TD_suppPlanUnits2 = null
	public String TD_suppPlanUnits3 = null
	public String TD_notifyMethod = null
	public String TD_password = null
	public String TD_email = null
	public String TD_payMethod = null
	public String TD_ccNumber = null
	public String TD_ccExpireMM = null
	public String TD_ccExpireYYYY = null
	public String TD_collection_acct_group1 = null
	public String TD_collection_acct_group2 = null
	public String TD_testAcctMethod = null
	public String TD_doFullInvoicing = null
	public String TD_couponCode1 = null
	public String TD_couponCode2 = null
	public String TD_couponCode3 = null
	public String TD_doWrite = null
	public String TD_retroActiveStartDate = null
	public String TD_altStartDate = null
	public String TD_altBillDay = null
	public String TD_taxExemptionLevel = null
	public String TD_currency_cd = null
	public String TD_contract_plan_no = null
	public String TD_contract_type_no = null
	public String TD_contract_length_months = null

	public String TD_custom_rate_plan_no_1= null
	public String TD_cutom_rate_service_no_1= null
	public String TD_custom_rate_seq_no_1= null
	public String TD_custom_rate_from_unit_1 = null
	public String TD_custom_rate_to_unit_1 = null
	public String TD_custom_rate_per_unit_1 = null

	public String TD_custom_rate_plan_no_2= null
	public String TD_cutom_rate_service_no_2 = null
	public String TD_custom_rate_seq_no_2= null
	public String TD_custom_rate_from_unit_2 = null
	public String TD_custom_rate_to_unit_2 = null
	public String TD_custom_rate_per_unit_2 = null

	public String TD_functional_acct_group1 = null
	public String TD_functional_acct_group2 = null
	public String TD_do_prorated_invoicing = null

	public String TD_bank_acct_no = null
	public String TD_bank_routing_number = null
	public String TD_status_until_alt_start_date = null
	public String TD_contract_alt_recur_fee = null
	public String TD_contract_start_date = null
	public String TD_collAcctGroup1 = null
	public String TD_Advance_VirtualTime = null

	//Address
	public String TD_firstName = null
	public String TD_mi = null
	public String TD_lastName = null
	public String TD_companyName = null
	public String TD_addr1 = null
	public String TD_addr2 = null
	public String TD_city = null
	public String TD_state = null
	public String TD_locality = null
	public String TD_country = null
	public String TD_postalCode = null
	public String TD_phone = null

	//Billing Addres
	public String TD_billFirstName = null
	public String TD_billMi = null
	public String TD_billLastName = null
	public String TD_billCompanyName = null
	public String TD_billAddr1 = null
	public String TD_billAddr2 = null
	public String TD_billCity = null
	public String TD_billState = null
	public String TD_billLocality = null
	public String TD_billCountry = null
	public String TD_billPostalCode = null
	public String TD_billPhone = null
	public String TD_billEmail = null
	public String TD_respLevelCd = null
	public String TD_seniorAcctNo = null
	public String TD_qualifierName1 = null
	public String TD_qualifierName2 = null
	public String TD_qualifierValue1 = null
	public String TD_qualifierValue2 = null
	public String TD_clientAcctId = null

	public String TD_promoCd = null
	public String TD_secretQuestion = null
	public String TD_secretQuestionAanswer = null
	public String TD_phoneExt = null
	public String TD_cellPhone = null
	public String TD_workPhone = null
	public String TD_workPhoneExt = null
	public String TD_birthDate = null
	public String TD_billPhoneExt = null
	public String TD_billCellPhone = null
	public String TD_billWorkPhone = null
	public String TD_billWorkPhoneExt = null
	public String TD_balanceForward = null
	public String TD_masterPlanAltRateSchedNo = null
	public String TD_clientReceiptId = null
	public String TD_cvv = null
	public String TD_taxpayerId = null
	public String TD_billAgreementId = null
	public String TD_altMsgTemplateNo = null
	public String TD_seqFuncGroupNo = null
	public String TD_bankAcctType = null
	public String TD_billDriversLicenseNo = null
	public String TD_billDriversLicenseState = null
	public String TD_billTaxpayerId = null
	public String TD_address3 = null
	public String TD_billAddress3 = null
	public String TD_trackData1 = null
	public String TD_trackData2 = null
	public String TD_cnAltMsgTemplateNo = null
	public String TD_altCallerId = null
	public String TD_acctNo = null
	public String TD_parentPlanNo = null
	public String TD_includeAllRateSchedules = null
	public String TD_includePlanHierarchy = null
	public String TD_VerifyDate = null
	public String TD_assign_supp_plan = null

	public String TD_suppFieldNames = null
	public String TD_suppFieldValues = null
	public String TD_altclientacctgroupid = null
	public String TD_doWriteSP = null

	public String TD_num_plan_units_sp = null
	public String TD_coupon_code_sp = null
	public String TD_comments_sp = null
	public String TD_client_receipt_id_sp = null
	public String TD_contract_type_no_sp = null
	public String TD_contract_alt_recur_fee_no_sp = null
	public String TD_contract_length_months_sp = null
	public String TD_contract_cancel_fee_sp = null
	public String TD_contract_comments_sp = null
	public String TD_contract_start_date_sp = null
	public String TD_offset_months_sp = null
	public String TD_auto_offset_months_options = null
	public String TD_alt_proration_start_date = null
	public String TD_altrateschedulenosp = null

	public String TD_assignSuppPlan = null
	public String TD_doWritemodify = null
	public String TD_commentsmodify = null
	public String TD_altCallerIdmodify = null
	public String TD_modifySuppPlan = null
	public String TD_alternateRateScheduleNo = null
	public String TD_couponCodeModify = null
	public String TD_noOfUnits = null
	public String TD_assign_sp_imm = null
	public String TD_cancel_sp_imm = null
	public String TD_modify_sp_imm = null

	public String TD_replace_sp_imm = null
	public String TD_replaceSuppPlan = null
	public String TD_existingSuppPlan = null
	public String TD_doWritereplace = null
	public String TD_commentsreplace = null
	public String TD_altCallerIdreplace = null
	public String TD_couponCodeReplace = null


	public String TD_update_acct_imm = null


	public String TD_ump_one_day_advance = null
	public String TD_ump_uac_option = null
	public String TD_ump_uac_master_Plan = null
	public String TD_ump_uac_master_plan_units = null
	public String TD_ump_uac_AD = null
	public String TD_ump_uac_collection_group = null



	//Custom Rates - AR

	public String TD_custom_rate_plan_no_1_ASP = null
	public String TD_cutom_rate_service_no_1_ASP = null
	public String TD_custom_rate_seq_no_1_ASP = null
	public String TD_custom_rate_from_unit_1_ASP = null
	public String TD_custom_rate_to_unit_1_ASP = null
	public String TD_custom_rate_per_unit_1_ASP = null

	public String TD_custom_rate_plan_no_2_ASP = null
	public String TD_cutom_rate_service_no_2_ASP = null
	public String TD_custom_rate_seq_no_2_ASP = null
	public String TD_custom_rate_from_unit_2_ASP = null
	public String TD_custom_rate_to_unit_2_ASP = null
	public String TD_custom_rate_per_unit_2_ASP = null


	//Integration 4
	public String TD_set_acct_usg_mtd_threshold_Apply=null
	public String TD_set_acct_usg_mtd_threshold_imm=null
	public String TD_amount=null
	public String TD_alt_caller_id=null
	public String TD_optional_trasaction_qualifier1=null
	public String TD_optional_trasaction_qualifier2=null
	public String TD_set_acct_usg_ptd_threshold_Apply=null
	public String TD_set_acct_usg_ptd_threshold_imm=null
	public String TD_amount_ptd=null
	public String TD_alt_caller_id_ptd=null
	public String TD_optional_trasaction_qualifier1_ptd=null
	public String TD_optional_trasaction_qualifier2_ptd=null
	public String TD_record_usage_apply=null
	public String TD_record_usage_imm=null
	public String TD_used_id_rec=null
	public String TD_usage_type=null
	public String TD_usage_units=null
	public String TD_usage_date=null
	public String TD_billable_units=null
	public String TD_amt=null
	public String TD_rate=null
	public String TD_telco_from=null
	public String TD_telco_to=null
	public String TD_comments=null
	public String TD_exclude_from_billing=null
	public String TD_exclusion_comments=null
	public String TD_qualifier1=null
	public String TD_qualifier2=null
	public String TD_qualifier3=null
	public String TD_qualifier4=null
	public String TD_parent_rec_no=null
	public String TD_usage_type_code=null
	public String TD_client_record_id=null
	public String TD_caller_id=null
	public String TD_alt_caller_id_rec_usg=null
	public String TD_optional_transaction_qualifier_rec_usg_1=null
	public String TD_optional_transaction_qualifier_rec_usg_2=null
	public String TD_gen_invoice_apply=null
	public String TD_gen_invoice_imm=null
	public String TD_force_pending=null
	public String TD_client_receipt_id=null
	public String TD_alt_bill_day=null
	public String TD_alt_caller_id_gen_inv=null
	public String TD_set_client_usg_mtd_threshold_Apply = null
	public String TD_set_client_usg_mtd_threshold_imm = null
	public String TD_set_client_usg_ptd_threshold_Apply = null
	public String TD_set_client_usg_ptd_threshold_imm = null
	public String TD_amount_set_acct = null
	public String TD_alt_caller_id_set_acct = null
	public String TD_amount_set_acct_ptd = null
	public String TD_alt_caller_id_set_acct_ptd = null
	public String TD_reset_usg_mtd_bal_Apply = null
	public String TD_reset_usg_mtd_bal_imm = null
	public String TD_alt_caller_id_resetset_mtd = null
	public String TD_reset_usg_ptd_bal_Apply = null
	public String TD_reset_usg_ptd_bal_imm = null
	public String TD_alt_caller_id_resetset_ptd = null
	public String TD_manage_pending_invoice_Apply = null
	public String TD_manage_pending_invoice_imm = null
	public String TD_action_directive = null
	//Ram update_acct_complete
	public String TD_update_acct_complete_imm = null
	public String TD_uac_first_name = null
	public String TD_uac_last_name = null
	public String TD_uac_middle_initial = null
	public String TD_uac_company_name = null
	public String TD_uac_address1 = null
	public String TD_uac_address2 = null
	public String TD_uac_city = null
	public String TD_uac_locality = null
	public String TD_uac_state_prov = null
	public String TD_uac_country = null
	public String TD_uac_postal_cd = null
	public String TD_uac_phone = null
	public String TD_uac_phone_ext = null
	public String TD_uac_cell_phone = null
	public String TD_uac_work_phone = null
	public String TD_uac_work_phone_ext = null
	public String TD_uac_email = null
	public String TD_uac_birthdate = null
	public String TD_uac_bill_first_name = null
	public String TD_uac_bill_last_name = null
	public String TD_uac_bill_middle_initial = null
	public String TD_uac_bill_company_name = null
	public String TD_uac_bill_address1 = null
	public String TD_uac_bill_address2 = null
	public String TD_uac_bill_city = null
	public String TD_uac_bill_locality = null
	public String TD_uac_bill_state_prov = null
	public String TD_uac_bill_country = null
	public String TD_uac_bill_postal_cd = null
	public String TD_uac_bill_phone = null
	public String TD_uac_bill_phone_ext = null
	public String TD_uac_bill_cell_phone = null
	public String TD_uac_bill_work_phone = null
	public String TD_uac_bill_work_phone_ext = null
	public String TD_uac_bill_email = null
	public String TD_uac_pay_method = null
	public String TD_uac_cc_number = null
	public String TD_uac_cc_expire_mm = null
	public String TD_uac_cc_expire_yyyy = null
	public String TD_uac_bank_routing_num = null
	public String TD_uac_bank_acct_num = null
	public String TD_uac_master_plan_no = null
	public String TD_uac_master_plan_alt_rate_sched_no = null
	public String TD_uac_master_plan_units = null
	public String TD_uac_master_plan_assign_directive = null
	public String TD_uac_functional_acct_groups = null
	public String TD_uac_functional_group_directives = null
	public String TD_uac_client_func_acct_group_ids	 = null
	public String TD_uac_collections_acct_groups = null
	public String TD_uac_collections_group_directives = null
	public String TD_uac_client_coll_acct_group_ids = null
	public String TD_uac_status_cd = null
	public String TD_uac_notify_method = null
	public String TD_uac_password = null
	public String TD_uac_secret_question = null
	public String TD_uac_secret_question_answer = null
	public String TD_uac_pin = null
	public String TD_uac_test_acct_ind = null
	public String TD_uac_resp_level_cd = null
	public String TD_uac_senior_acct_no = null
	public String TD_uac_client_acct_id = null
	public String TD_uac_do_collect = null
	public String TD_uac_change_status_after_coll = null
	public String TD_uac_reset_dates_after_status = null
	public String TD_uac_client_receipt_id = null
	public String TD_uac_alt_do_dunning = null
	public String TD_uac_force_currency_change = null
	public String TD_uac_cvv = null
	public String TD_uac_taxpayer_id = null
	public String TD_uac_bill_agreement_id = null
	public String TD_uac_auto_cancel_supp_plans = null
	public String TD_uac_offset_months = null
	public String TD_uac_alt_proration_start_date = null
	public String TD_uac_alt_msg_template_no = null
	public String TD_uac_seq_func_group_no = null
	public String TD_uac_bank_acct_type = null
	public String TD_uac_bill_drivers_license_no = null
	public String TD_uac_bill_drivers_license_state = null
	public String TD_uac_bill_taxpayer_id = null
	public String TD_uac_address3 = null
	public String TD_uac_bill_address3 = null
	public String TD_uac_usage_accumulation_plan_no = null
	public String TD_uac_usage_accumulation_reset_months = null
	public String TD_uac_enable_usage_pooling_plan_no = null
	public String TD_uac_disable_usage_pooling_plan_no = null
	public String TD_uac_alt_client_acct_group_id = null
	public String TD_uac_track_data1 = null
	public String TD_uac_track_data2 = null
	public String TD_uac_offset_interval = null
	public String TD_uac_tax_exemption_level = null
	public String TD_uac_cn_alt_msg_template_no = null
	public String TD_uac_promo_cd = null
	public String TD_uac_invoice_unbilled_usage = null
	public String TD_uac_coupon_code = null
	public String TD_uac_alt_caller_id = null
	public String TD_uac_qualifier_name = null
	public String TD_uac_qualifier_value = null

	//Ram update_master_plan
	public String TD_update_master_plan_imm = null
	public String TD_ump_master_plan_no = null
	public String TD_ump_alt_rate_schedule_no = null
	public String TD_ump_num_plan_units = null
	public String TD_ump_assignment_directive = null
	public String TD_ump_do_write = null
	public String TD_ump_client_receipt_id = null
	public String TD_ump_force_currency_change = null
	public String TD_ump_auto_cancel_supp_plans = null
	public String TD_ump_offset_months = null
	public String TD_ump_alt_proration_start_date = null
	public String TD_ump_alt_client_acct_group_id = null
	public String TD_ump_custom_rate_plan_no = null
	public String TD_ump_custom_rate_service_no = null
	public String TD_ump_custom_rate_seq_no = null
	public String TD_ump_custom_rate_from_unit = null
	public String TD_ump_custom_rate_to_unit = null
	public String TD_ump_custom_rate_per_unit = null
	public String TD_ump_effective_date = null
	public String TD_ump_offset_interval = null
	public String TD_ump_invoice_unbilled_usage = null
	public String TD_ump_coupon_code = null
	public String TD_ump_alt_caller_id = null
	public String TD_ump_qualifier_name = null
	public String TD_ump_qualifier_value = null
	//Ram update_acct_status
	public String TD_uas_status_cd = null
	public String TD_uas_queue_days = null
	public String TD_uas_queue_date = null
	public String TD_uas_force_bill_date_reset = null
	public String TD_uas_comments = null
	public String TD_uas_client_receipt_id = null
	public String TD_uas_alt_do_dunning = null
	public String TD_uas_alt_caller_id = null
	public String TD_uas_qualifier_name = null
	public String TD_uas_qualifier_value = null
	public String TD_update_acct_status_imm = null

	//Ram assign_supp_plan_multi
	public String TD_TestSet = null
	public String TD_assign_supp_plan_multi_imm = null
	public String TD_aspm_assignment_directive = null
	public String TD_aspm_do_write = null
	public String TD_aspm_comments = null
	public String TD_aspm_client_receipt_id = null
	public String TD_aspm_alt_proration_start_date = null
	public String TD_aspm_coupon_codes = null
	public String TD_aspm_supp_plan_no = null
	public String TD_aspm_alt_rate_schedule_no = null
	public String TD_aspm_num_plan_units = null
	public String TD_aspm_contract_type_no = null

	public String TD_aspm_contract_alt_recur_fee = null
	public String TD_aspm_contract_length_months = null
	public String TD_aspm_contract_cancel_fee = null
	public String TD_aspm_contract_comments = null
	public String TD_aspm_contract_start_date = null
	public String TD_aspm_offset_months = null
	public String TD_aspm_auto_offset_months_option = null
	public String TD_aspm_offset_interval = null
	public String TD_aspm_contract_end_date = null
	public String TD_aspm_effective_date = null

	public String TD_aspm_alt_caller_id = null
	public String TD_aspm_qualifier_name = null
	public String TD_aspm_qualifier_value = null

	//Ram modify_supp_plan
	public String TD_modify_supp_plan_imm = null
	public String TD_msp_supp_plan_no = null
	public String TD_msp_alt_rate_schedule_no = null
	public String TD_msp_num_plan_units = null
	public String TD_msp_coupon_code = null
	public String TD_msp_assignment_directive = null
	public String TD_msp_comments = null
	public String TD_msp_do_write = null
	public String TD_msp_client_receipt_id = null

	public String TD_msp_custom_rate_plan_no = null
	public String TD_msp_custom_rate_service_no = null
	public String TD_msp_custom_rate_seq_no = null
	public String TD_msp_custom_rate_from_unit = null
	public String TD_msp_custom_rate_to_unit = null
	public String TD_msp_custom_rate_per_unit = null

	public String TD_msp_effective_date = null
	public String TD_msp_offset_interval = null
	public String TD_msp_alt_caller_id = null

	public String TD_msp_qualifier_name = null
	public String TD_msp_qualifier_value = null

	//Ram replace_supp_plan
	public String TD_replace_supp_plan_imm = null

	public String TD_rsp_existing_supp_plan_no = null
	public String TD_rsp_new_supp_plan_no = null
	public String TD_rsp_alt_rate_schedule_no = null
	public String TD_rsp_num_plan_units = null
	public String TD_rsp_coupon_code = null
	public String TD_rsp_assignment_directive = null
	public String TD_rsp_comments = null
	public String TD_rsp_do_write = null
	public String TD_rsp_client_receipt_id = null
	public String TD_rsp_offset_months = null

	public String TD_rsp_auto_offset_months_option = null
	public String TD_rsp_alt_client_acct_group_id = null
	public String TD_rsp_custom_rate_plan_no = null
	public String TD_rsp_custom_rate_service_no = null
	public String TD_rsp_custom_rate_seq_no = null
	public String TD_rsp_custom_rate_from_unit = null
	public String TD_rsp_custom_rate_to_unit = null
	public String TD_rsp_custom_rate_per_unit = null
	public String TD_rsp_effective_date = null
	public String TD_rsp_offset_interval = null

	public String TD_rsp_invoice_unbilled_usage = null
	public String TD_rsp_alt_caller_id = null

	public String TD_rsp_qualifier_name = null
	public String TD_rsp_qualifier_value = null

	public String TD_AVT = null


	//Ram cancel_supp_plan
	public String TD_cancel_supp_plan_imm = null
	public String TD_csp_supp_plan_no = null
	public String TD_csp_assignment_directive = null
	public String TD_csp_comments = null
	public String TD_csp_do_write = null
	public String TD_csp_client_receipt_id = null
	public String TD_csp_alt_proration_start_date = null
	public String TD_csp_effective_date = null
	public String TD_csp_offset_interval = null
	public String TD_csp_invoice_unbilled_usage = null
	public String TD_csp_alt_caller_id = null

	//Ram assign_supp_plan
	public String TD_assign_supp_plan_imm = null
	public String TD_asp_supp_plan_no = null
	public String TD_asp_num_plan_units = null
	public String TD_asp_coupon_code = null
	public String TD_asp_alt_rate_schedule_no = null
	public String TD_asp_Do_write = null
	public String TD_asp_comments = null
	public String TD_asp_client_receipt_id = null
	public String TD_asp_contract_type_no = null
	public String TD_asp_contract_alt_recur_fee = null
	public String TD_asp_contract_length_months = null
	public String TD_asp_contract_start_date = null
	public String TD_assign_supp_plan_1 = null
	public String TD_assign_supp_plan_2 = null
	public String TD_assign_supp_plan_3 = null
	public String TD_assign_supp_plan_4 = null

	public String TD_num_plan_units_sp_1 = null
	public String TD_num_plan_units_sp_2 = null
	public String TD_num_plan_units_sp_3 = null
	public String TD_num_plan_units_sp_4 = null

	public String TD_effectiveDateSP = null
	public String TD_ClientSKU = null
	public String TD_OrderUnit = null
	public String TD_BillImmediately = null



	//BUG-2390 ASP
	public String TD_assgin_supp_plan2 = null
	public String TD_assign_Supp_Plan_no2 = null
	public String TD_num_plan_units_sp2 = null

	public String TD_Usage_Type = null;
	public String TD_Usage_Units = null;
	public String TD_Rec_Usage_Amt = null;
	public String TD_Rec_Usage_Rate = null;
	public String TD_OrderAmount = null;
	public String TD_credit_amount = null;
	public String TD_UnitDiscountAmt = null;
	public String TD_apply_coupon = null;
	
	//others
	public String TD_Bug_ID = null;
	public String TD_TestLinkID = null;
	
	public static List tcName = []
	public static List tsName = []
	public static List xPath = []
	public static List expValue = []

	Hashtable<String, Integer> excelHeader = new Hashtable<String, Integer>()
	Hashtable<String, Integer> excelrRowColumnCount = new Hashtable<String, Integer>()


	HSSFSheet initiateExcelConnectionNexia(String workSheetName) {

		String strBasePath = null
		String file = null
		HSSFSheet sheet = null

		POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(Constant.PROJECTLOCATION + 'TestCase_Selection.xls'))
		HSSFWorkbook wb = new HSSFWorkbook(fs)
		sheet = wb.getSheet(workSheetName)

		return sheet
	}

	Hashtable<String, Integer> findRowColumnCount(HSSFSheet sheet, Hashtable<String, Integer> rowColumnCount) {

		HSSFRow row = null
		int rows
		rows = sheet.getPhysicalNumberOfRows()
		int cols = 0
		int tmp = 0
		int counter = 0
		String temp = null

		for(int i=0; i<10 || i<rows ; i++) {
			row = sheet.getRow(i)
			if(row != null) {
				temp = convertHSSFCellToString(row.getCell(0))
				if(!temp.equals("")){
					counter++
				}
				tmp = sheet.getRow(i).getPhysicalNumberOfCells()
				if(tmp > cols) cols = tmp
			}
		}
		rowColumnCount.put("RowCount", counter)
		rowColumnCount.put("ColumnCount", cols)

		return rowColumnCount
	}

	String convertHSSFCellToString(HSSFCell cell){
		String cellValue = null
		if(cell != null){
			cellValue = cell.toString()
			cellValue = cellValue.trim()
		}else {
			cellValue = ""
		}
		return cellValue
	}

	Hashtable<String, Integer> readExcelHeaders(HSSFSheet sheet,Hashtable<String, Integer> excelHeaders,Hashtable<String, Integer> rowColumnCount) {

		HSSFRow row = null
		HSSFCell cell = null

		for(int r=0; r < rowColumnCount.get("RowCount") ; r++) {
			row = sheet.getRow(r)

			if(row != null) {
				for(int c=0 ; c < rowColumnCount.get("ColumnCount") ; c++) {
					cell = row.getCell(c)
					if(cell != null) {
						excelHeaders.put(cell.toString(), c)
					}
				}
				break
			}
		}
		return excelHeaders
	}

	public def testDataModifySuppPlan(HSSFRow row) {

		TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
		TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
		TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
		TD_userid = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Status_Code"))).trim()
		TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
		TD_masterPlanUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Units"))).trim()
		TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
		TD_suppPlan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan2"))).trim()
		TD_suppPlan3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan3"))).trim()
		TD_suppPlanUnits1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units1"))).trim()
		TD_suppPlanUnits2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units2"))).trim()
		TD_suppPlanUnits3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units3"))).trim()
		TD_notifyMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Notify_Medhod"))).trim()
		TD_password = convertHSSFCellToString(row.getCell(excelHeader.get("Password"))).trim()
		TD_email = convertHSSFCellToString(row.getCell(excelHeader.get("Email"))).trim()
		TD_payMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Pay_Method"))).trim()
		TD_bank_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("bank_acct_no"))).trim()
		TD_bank_routing_number = convertHSSFCellToString(row.getCell(excelHeader.get("bank_routing_number"))).trim()
		TD_ccNumber = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Number"))).trim()
		TD_ccExpireMM = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_MM"))).trim()
		TD_ccExpireYYYY = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_YYYY"))).trim()
		TD_collection_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group1"))).trim()
		TD_collection_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group2"))).trim()

		TD_testAcctMethod = convertHSSFCellToString(row.getCell(excelHeader.get("test_acct_ind"))).trim()
		TD_doFullInvoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_full_invoicing"))).trim()
		TD_do_prorated_invoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_prorated_invoicing"))).trim()
		TD_couponCode1 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code1"))).trim()
		TD_couponCode2 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code2"))).trim()
		TD_couponCode3 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code3"))).trim()
		TD_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date"))).trim()
		TD_contract_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_plan_no"))).trim()
		TD_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no"))).trim()
		TD_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months"))).trim()

		TD_doWrite = convertHSSFCellToString(row.getCell(excelHeader.get("do_write"))).trim()
		TD_taxExemptionLevel = convertHSSFCellToString(row.getCell(excelHeader.get("Tax_Exemption_Level"))).trim()
		TD_retroActiveStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Retroactive_Start_Date"))).trim()
		TD_altStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Start_Date"))).trim()
		TD_altBillDay = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Billing_Day"))).trim()
		TD_assignmentdirective = convertHSSFCellToString(row.getCell(excelHeader.get("AD"))).trim()
		TD_currency_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Currency_cd"))).trim()

		TD_custom_rate_plan_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1"))).trim()
		TD_cutom_rate_service_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1"))).trim()
		TD_custom_rate_seq_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1"))).trim()
		TD_custom_rate_from_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1"))).trim()
		TD_custom_rate_to_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1"))).trim()
		TD_custom_rate_per_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1"))).trim()
		TD_custom_rate_plan_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2"))).trim()
		TD_cutom_rate_service_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2"))).trim()
		TD_custom_rate_seq_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2"))).trim()
		TD_custom_rate_from_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2"))).trim()
		TD_custom_rate_to_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2"))).trim()
		TD_custom_rate_per_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2"))).trim()

		TD_functional_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group1"))).trim()
		TD_functional_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group2"))).trim()
		TD_status_until_alt_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("status_until_alt_start_date"))).trim()
		TD_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee"))).trim()

		TD_firstName = convertHSSFCellToString(row.getCell(excelHeader.get("First_Name"))).trim()
		TD_mi = convertHSSFCellToString(row.getCell(excelHeader.get("MI"))).trim()
		TD_lastName = convertHSSFCellToString(row.getCell(excelHeader.get("Last_Name"))).trim()
		TD_companyName = convertHSSFCellToString(row.getCell(excelHeader.get("Company_Name"))).trim()
		TD_addr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr1"))).trim()
		TD_addr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr2"))).trim()
		TD_city = convertHSSFCellToString(row.getCell(excelHeader.get("City"))).trim()
		TD_state = convertHSSFCellToString(row.getCell(excelHeader.get("State"))).trim()
		TD_locality = convertHSSFCellToString(row.getCell(excelHeader.get("Locality"))).trim()
		TD_country = convertHSSFCellToString(row.getCell(excelHeader.get("Country"))).trim()
		TD_postalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Postal_Code"))).trim()
		TD_phone = convertHSSFCellToString(row.getCell(excelHeader.get("Phone"))).trim()

		TD_billFirstName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_First_Name"))).trim()
		TD_billMi = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_MI"))).trim()
		TD_billLastName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Last_Name"))).trim()
		TD_billCompanyName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Company_Name"))).trim()
		TD_billAddr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr1"))).trim()
		TD_billAddr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr2"))).trim()
		TD_billCity = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_City"))).trim()
		TD_billState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_State"))).trim()
		TD_billLocality = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Locality"))).trim()
		TD_billCountry = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Country"))).trim()
		TD_billPostalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Postal_Code"))).trim()
		TD_billPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone"))).trim()
		TD_billEmail = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Email"))).trim()
		TD_respLevelCd = convertHSSFCellToString(row.getCell(excelHeader.get("Resp_Level_Cd"))).trim()
		TD_qualifierName1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name1"))).trim()
		TD_qualifierName2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name2"))).trim()
		TD_qualifierValue1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value1"))).trim()
		TD_qualifierValue2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value2"))).trim()

		TD_clientAcctId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Acct_Id"))).trim()
		TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Cd"))).trim()
		TD_secretQuestion = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question"))).trim()
		TD_secretQuestionAanswer = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question_Answer"))).trim()
		TD_phoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Phone_Ext"))).trim()
		TD_cellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Cell_Phone"))).trim()
		TD_workPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone"))).trim()
		TD_workPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone_Ext"))).trim()
		TD_birthDate = convertHSSFCellToString(row.getCell(excelHeader.get("Birthdate"))).trim()
		TD_billPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone_Ext"))).trim()
		TD_billCellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Cell_Phone"))).trim()
		TD_billWorkPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone"))).trim()
		TD_billWorkPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone_Ext"))).trim()
		TD_balanceForward = convertHSSFCellToString(row.getCell(excelHeader.get("Balance_Forward"))).trim()
		TD_masterPlanAltRateSchedNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Alt_Rate_Sched_No"))).trim()
		TD_clientReceiptId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
		TD_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("Cvv"))).trim()
		TD_taxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Taxpayer_Id"))).trim()
		TD_billAgreementId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Agreement_Id"))).trim()
		TD_altMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Msg_Template_No"))).trim()
		TD_seqFuncGroupNo = convertHSSFCellToString(row.getCell(excelHeader.get("Seq_Func_Group_No"))).trim()
		TD_bankAcctType = convertHSSFCellToString(row.getCell(excelHeader.get("Bank_Acct_Type"))).trim()
		TD_billDriversLicenseNo = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_No"))).trim()
		TD_billDriversLicenseState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_State"))).trim()
		TD_billTaxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Taxpayer_Id"))).trim()
		TD_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("Address3"))).trim()
		TD_billAddress3 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Address3"))).trim()
		TD_trackData1 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data1"))).trim()
		TD_trackData2 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data2"))).trim()
		TD_cnAltMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Cn_Alt_Msg_Template_No"))).trim()
		TD_altCallerId = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()
		TD_effectiveDate = convertHSSFCellToString(row.getCell(excelHeader.get("Effective_Date"))).trim()
		TD_modifySuppPlan = convertHSSFCellToString(row.getCell(excelHeader.get("Modify_Supp_Plan"))).trim()
		TD_doWritemodify = convertHSSFCellToString(row.getCell(excelHeader.get("Do_write_SP"))).trim()
		TD_commentsmodify = convertHSSFCellToString(row.getCell(excelHeader.get("Comments_SP"))).trim()
		TD_clientReceipentId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id_SP"))).trim()
		TD_altProrationRate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Proration_Rate_SP"))).trim()
		TD_altCallerIdmodify = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id_SP"))).trim()
		TD_assignmentdirective = convertHSSFCellToString(row.getCell(excelHeader.get("AD"))).trim()
		TD_alternateRateScheduleNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_rate_schedule_no"))).trim()
		TD_couponCodeModify = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_code_SP"))).trim()
		TD_noOfUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Number_of_units_SP"))).trim()
		TD_Advance_VirtualTime = convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).trim()
		TD_modify_sp_imm = convertHSSFCellToString(row.getCell(excelHeader.get("modify_sp_imm"))).trim()
		TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()
	}

	public def testDataReplaceSuppPlan(HSSFRow row) {

		TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
		TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
		TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
		TD_userid = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Status_Code"))).trim()
		TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
		TD_masterPlanUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Units"))).trim()
		TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
		TD_suppPlan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan2"))).trim()
		TD_suppPlan3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan3"))).trim()
		TD_suppPlanUnits1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units1"))).trim()
		TD_suppPlanUnits2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units2"))).trim()
		TD_suppPlanUnits3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units3"))).trim()
		TD_notifyMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Notify_Medhod"))).trim()
		TD_password = convertHSSFCellToString(row.getCell(excelHeader.get("Password"))).trim()
		TD_email = convertHSSFCellToString(row.getCell(excelHeader.get("Email"))).trim()
		TD_payMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Pay_Method"))).trim()
		TD_bank_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("bank_acct_no"))).trim()
		TD_bank_routing_number = convertHSSFCellToString(row.getCell(excelHeader.get("bank_routing_number"))).trim()
		TD_ccNumber = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Number"))).trim()
		TD_ccExpireMM = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_MM"))).trim()
		TD_ccExpireYYYY = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_YYYY"))).trim()
		TD_collection_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group1"))).trim()
		TD_collection_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group2"))).trim()

		TD_testAcctMethod = convertHSSFCellToString(row.getCell(excelHeader.get("test_acct_ind"))).trim()
		TD_doFullInvoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_full_invoicing"))).trim()
		TD_do_prorated_invoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_prorated_invoicing"))).trim()
		TD_couponCode1 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code1"))).trim()
		TD_couponCode2 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code2"))).trim()
		TD_couponCode3 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code3"))).trim()
		TD_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date"))).trim()
		TD_contract_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_plan_no"))).trim()
		TD_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no"))).trim()
		TD_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months"))).trim()

		TD_doWrite = convertHSSFCellToString(row.getCell(excelHeader.get("do_write"))).trim()
		TD_taxExemptionLevel = convertHSSFCellToString(row.getCell(excelHeader.get("Tax_Exemption_Level"))).trim()
		TD_retroActiveStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Retroactive_Start_Date"))).trim()
		TD_altStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Start_Date"))).trim()
		TD_altBillDay = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Billing_Day"))).trim()
		TD_assignmentdirective = convertHSSFCellToString(row.getCell(excelHeader.get("AD"))).trim()
		TD_currency_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Currency_cd"))).trim()

		TD_custom_rate_plan_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1"))).trim()
		TD_cutom_rate_service_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1"))).trim()
		TD_custom_rate_seq_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1"))).trim()
		TD_custom_rate_from_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1"))).trim()
		TD_custom_rate_to_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1"))).trim()
		TD_custom_rate_per_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1"))).trim()
		TD_custom_rate_plan_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2"))).trim()
		TD_cutom_rate_service_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2"))).trim()
		TD_custom_rate_seq_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2"))).trim()
		TD_custom_rate_from_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2"))).trim()
		TD_custom_rate_to_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2"))).trim()
		TD_custom_rate_per_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2"))).trim()

		TD_functional_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group1"))).trim()
		TD_functional_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group2"))).trim()
		TD_status_until_alt_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("status_until_alt_start_date"))).trim()
		TD_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee"))).trim()

		TD_firstName = convertHSSFCellToString(row.getCell(excelHeader.get("First_Name"))).trim()
		TD_mi = convertHSSFCellToString(row.getCell(excelHeader.get("MI"))).trim()
		TD_lastName = convertHSSFCellToString(row.getCell(excelHeader.get("Last_Name"))).trim()
		TD_companyName = convertHSSFCellToString(row.getCell(excelHeader.get("Company_Name"))).trim()
		TD_addr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr1"))).trim()
		TD_addr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr2"))).trim()
		TD_city = convertHSSFCellToString(row.getCell(excelHeader.get("City"))).trim()
		TD_state = convertHSSFCellToString(row.getCell(excelHeader.get("State"))).trim()
		TD_locality = convertHSSFCellToString(row.getCell(excelHeader.get("Locality"))).trim()
		TD_country = convertHSSFCellToString(row.getCell(excelHeader.get("Country"))).trim()
		TD_postalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Postal_Code"))).trim()
		TD_phone = convertHSSFCellToString(row.getCell(excelHeader.get("Phone"))).trim()

		TD_billFirstName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_First_Name"))).trim()
		TD_billMi = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_MI"))).trim()
		TD_billLastName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Last_Name"))).trim()
		TD_billCompanyName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Company_Name"))).trim()
		TD_billAddr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr1"))).trim()
		TD_billAddr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr2"))).trim()
		TD_billCity = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_City"))).trim()
		TD_billState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_State"))).trim()
		TD_billLocality = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Locality"))).trim()
		TD_billCountry = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Country"))).trim()
		TD_billPostalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Postal_Code"))).trim()
		TD_billPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone"))).trim()
		TD_billEmail = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Email"))).trim()
		TD_respLevelCd = convertHSSFCellToString(row.getCell(excelHeader.get("Resp_Level_Cd"))).trim()
		TD_qualifierName1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name1"))).trim()
		TD_qualifierName2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name2"))).trim()
		TD_qualifierValue1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value1"))).trim()
		TD_qualifierValue2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value2"))).trim()

		TD_clientAcctId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Acct_Id"))).trim()
		TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Cd"))).trim()
		TD_secretQuestion = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question"))).trim()
		TD_secretQuestionAanswer = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question_Answer"))).trim()
		TD_phoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Phone_Ext"))).trim()
		TD_cellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Cell_Phone"))).trim()
		TD_workPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone"))).trim()
		TD_workPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone_Ext"))).trim()
		TD_birthDate = convertHSSFCellToString(row.getCell(excelHeader.get("Birthdate"))).trim()
		TD_billPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone_Ext"))).trim()
		TD_billCellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Cell_Phone"))).trim()
		TD_billWorkPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone"))).trim()
		TD_billWorkPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone_Ext"))).trim()
		TD_balanceForward = convertHSSFCellToString(row.getCell(excelHeader.get("Balance_Forward"))).trim()
		TD_masterPlanAltRateSchedNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Alt_Rate_Sched_No"))).trim()
		TD_clientReceiptId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
		TD_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("Cvv"))).trim()
		TD_taxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Taxpayer_Id"))).trim()
		TD_billAgreementId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Agreement_Id"))).trim()
		TD_altMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Msg_Template_No"))).trim()
		TD_seqFuncGroupNo = convertHSSFCellToString(row.getCell(excelHeader.get("Seq_Func_Group_No"))).trim()
		TD_bankAcctType = convertHSSFCellToString(row.getCell(excelHeader.get("Bank_Acct_Type"))).trim()
		TD_billDriversLicenseNo = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_No"))).trim()
		TD_billDriversLicenseState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_State"))).trim()
		TD_billTaxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Taxpayer_Id"))).trim()
		TD_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("Address3"))).trim()
		TD_billAddress3 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Address3"))).trim()
		TD_trackData1 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data1"))).trim()
		TD_trackData2 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data2"))).trim()
		TD_cnAltMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Cn_Alt_Msg_Template_No"))).trim()
		TD_altCallerId = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()
		TD_effectiveDate = convertHSSFCellToString(row.getCell(excelHeader.get("Effective_Date"))).trim()
		TD_existingSuppPlan = convertHSSFCellToString(row.getCell(excelHeader.get("Existing_Supp_Plan"))).trim()
		TD_replaceSuppPlan = convertHSSFCellToString(row.getCell(excelHeader.get("Replace_Supp_Plan"))).trim()
		TD_doWritereplace = convertHSSFCellToString(row.getCell(excelHeader.get("Do_write_SP"))).trim()
		//TD_commentsreplace = convertHSSFCellToString(row.getCell(excelHeader.get("Comments_SP"))).trim()
		//TD_clientReceipentId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id_SP"))).trim()
		//TD_altProrationRate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Proration_Rate_SP"))).trim()
		//TD_altCallerIdreplace = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id_SP"))).trim()
		TD_assignmentdirective = convertHSSFCellToString(row.getCell(excelHeader.get("AD"))).trim()
		TD_alternateRateScheduleNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_rate_schedule_no"))).trim()
		TD_couponCodeReplace = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_code_SP"))).trim()
		TD_noOfUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Number_of_units_SP"))).trim()
		TD_Advance_VirtualTime = convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).trim()
		TD_replace_sp_imm = convertHSSFCellToString(row.getCell(excelHeader.get("replace_sp_imm"))).trim()
		TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()

		/*TD_custom_rate_plan_no_1_sp = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1_SP"))).trim()
		 TD_cutom_rate_service_no_1_sp = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1_SP"))).trim()
		 TD_custom_rate_seq_no_1_sp = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1_SP"))).trim()
		 TD_custom_rate_from_unit_1_sp = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1_SP"))).trim()
		 TD_custom_rate_to_unit_1_sp = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1_SP"))).trim()
		 TD_custom_rate_per_unit_1_sp = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1_SP"))).trim()
		 TD_custom_rate_plan_no_2_sp = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2_SP"))).trim()
		 TD_cutom_rate_service_no_2_sp = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2_SP"))).trim()
		 TD_custom_rate_seq_no_2_sp = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2_SP"))).trim()
		 TD_custom_rate_from_unit_2_sp = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2_SP"))).trim()
		 TD_custom_rate_to_unit_2_sp = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2_SP"))).trim()
		 TD_custom_rate_per_unit_2_sp = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2_SP"))).trim()*/

	}

	public def testDataIntegration4(HSSFRow row)
	{
		//testd = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No")))
		TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()

		TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
		TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
		//							//testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID"))).trim()
		//							clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
		//masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
		TD_userid = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Status_Code"))).trim()
		TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
		TD_masterPlanUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Units"))).trim()
		TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
		TD_suppPlan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan2"))).trim()
		TD_suppPlan3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan3"))).trim()
		TD_suppPlanUnits1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units1"))).trim()
		TD_suppPlanUnits2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units2"))).trim()
		TD_suppPlanUnits3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units3"))).trim()
		TD_notifyMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Notify_Medhod"))).trim()
		TD_password = convertHSSFCellToString(row.getCell(excelHeader.get("Password"))).trim()
		TD_email = convertHSSFCellToString(row.getCell(excelHeader.get("Email"))).trim()
		TD_payMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Pay_Method"))).trim()
		TD_ccNumber = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Number"))).trim()
		TD_ccExpireMM = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_MM"))).trim()
		TD_ccExpireYYYY = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_YYYY"))).trim()
		TD_collection_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group1"))).trim()
		TD_collection_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group2"))).trim()


		TD_testAcctMethod = convertHSSFCellToString(row.getCell(excelHeader.get("test_acct_ind"))).trim()
		TD_doFullInvoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_full_invoicing"))).trim()
		TD_couponCode1 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code1"))).trim()
		TD_couponCode2 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code2"))).trim()
		TD_couponCode3 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code3"))).trim()

		TD_contract_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_plan_no"))).trim()
		TD_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no"))).trim()
		TD_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months"))).trim()

		TD_doWrite = convertHSSFCellToString(row.getCell(excelHeader.get("do_write"))).trim()
		TD_retroActiveStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Retroactive_Start_Date"))).trim()
		TD_altStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Start_Date"))).trim()
		TD_altBillDay = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Billing_Day"))).trim()
		TD_taxExemptionLevel = convertHSSFCellToString(row.getCell(excelHeader.get("Tax_Exemption_Level"))).trim()
		TD_currency_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Currency_cd"))).trim()

		TD_custom_rate_plan_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1"))).trim()
		TD_cutom_rate_service_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1"))).trim()
		TD_custom_rate_seq_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1"))).trim()
		TD_custom_rate_from_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1"))).trim()
		TD_custom_rate_to_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1"))).trim()
		TD_custom_rate_per_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1"))).trim()

		TD_custom_rate_plan_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2"))).trim()
		TD_cutom_rate_service_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2"))).trim()
		TD_custom_rate_seq_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2"))).trim()
		TD_custom_rate_from_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2"))).trim()
		TD_custom_rate_to_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2"))).trim()
		TD_custom_rate_per_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2"))).trim()

		TD_functional_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group1"))).trim()
		TD_functional_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group2"))).trim()
		TD_do_prorated_invoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_prorated_invoicing"))).trim()

		TD_bank_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("bank_acct_no"))).trim()
		TD_bank_routing_number = convertHSSFCellToString(row.getCell(excelHeader.get("bank_routing_number"))).trim()
		TD_status_until_alt_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("status_until_alt_start_date"))).trim()
		TD_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee"))).trim()

		TD_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date"))).trim()

		TD_firstName = convertHSSFCellToString(row.getCell(excelHeader.get("First_Name"))).trim()
		TD_mi = convertHSSFCellToString(row.getCell(excelHeader.get("MI"))).trim()
		TD_lastName = convertHSSFCellToString(row.getCell(excelHeader.get("Last_Name"))).trim()
		TD_companyName = convertHSSFCellToString(row.getCell(excelHeader.get("Company_Name"))).trim()
		TD_addr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr1"))).trim()
		TD_addr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr2"))).trim()
		TD_city = convertHSSFCellToString(row.getCell(excelHeader.get("City"))).trim()
		TD_state = convertHSSFCellToString(row.getCell(excelHeader.get("State"))).trim()
		TD_locality = convertHSSFCellToString(row.getCell(excelHeader.get("Locality"))).trim()
		TD_country = convertHSSFCellToString(row.getCell(excelHeader.get("Country"))).trim()
		TD_postalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Postal_Code"))).trim()
		TD_phone = convertHSSFCellToString(row.getCell(excelHeader.get("Phone"))).trim()


		TD_billFirstName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_First_Name"))).trim()
		TD_billMi = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_MI"))).trim()
		TD_billLastName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Last_Name"))).trim()
		TD_billCompanyName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Company_Name"))).trim()
		TD_billAddr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr1"))).trim()
		TD_billAddr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr2"))).trim()
		TD_billCity = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_City"))).trim()
		TD_billState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_State"))).trim()
		TD_billLocality = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Locality"))).trim()
		TD_billCountry = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Country"))).trim()
		TD_billPostalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Postal_Code"))).trim()
		TD_billPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone"))).trim()
		TD_billEmail = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Email"))).trim()
		TD_respLevelCd = convertHSSFCellToString(row.getCell(excelHeader.get("Resp_Level_Cd"))).trim()
		TD_qualifierName1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name1"))).trim()
		TD_qualifierName2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name2"))).trim()
		TD_qualifierValue1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value1"))).trim()
		TD_qualifierValue2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value2"))).trim()

		TD_clientAcctId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Acct_Id"))).trim()
		TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Cd"))).trim()
		TD_secretQuestion = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question"))).trim()
		TD_secretQuestionAanswer = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question_Answer"))).trim()
		TD_phoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Phone_Ext"))).trim()
		TD_cellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Cell_Phone"))).trim()
		TD_workPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone"))).trim()
		TD_workPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone_Ext"))).trim()
		TD_birthDate = convertHSSFCellToString(row.getCell(excelHeader.get("Birthdate"))).trim()
		TD_billPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone_Ext"))).trim()
		TD_billCellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Cell_Phone"))).trim()
		TD_billWorkPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone"))).trim()
		TD_billWorkPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone_Ext"))).trim()
		TD_balanceForward = convertHSSFCellToString(row.getCell(excelHeader.get("Balance_Forward"))).trim()
		TD_masterPlanAltRateSchedNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Alt_Rate_Sched_No"))).trim()
		TD_clientReceiptId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
		TD_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("Cvv"))).trim()
		TD_taxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Taxpayer_Id"))).trim()
		TD_billAgreementId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Agreement_Id"))).trim()
		TD_altMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Msg_Template_No"))).trim()
		TD_seqFuncGroupNo = convertHSSFCellToString(row.getCell(excelHeader.get("Seq_Func_Group_No"))).trim()
		TD_bankAcctType = convertHSSFCellToString(row.getCell(excelHeader.get("Bank_Acct_Type"))).trim()
		TD_billDriversLicenseNo = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_No"))).trim()
		TD_billDriversLicenseState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_State"))).trim()
		TD_billTaxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Taxpayer_Id"))).trim()
		TD_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("Address3"))).trim()
		TD_billAddress3 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Address3"))).trim()
		TD_trackData1 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data1"))).trim()
		TD_trackData2 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data2"))).trim()
		TD_cnAltMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Cn_Alt_Msg_Template_No"))).trim()
		TD_altCallerId = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()
		TD_Advance_VirtualTime = convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).trim()
		TD_VerifyDate = convertHSSFCellToString(row.getCell(excelHeader.get("Verify_Date"))).trim()


		TD_set_client_usg_mtd_threshold_Apply = convertHSSFCellToString(row.getCell(excelHeader.get("set_client_usg_mtd_threshold_Apply"))).trim()
		TD_set_client_usg_mtd_threshold_imm = convertHSSFCellToString(row.getCell(excelHeader.get("set_client_usg_mtd_threshold_imm"))).trim()
		TD_amount = convertHSSFCellToString(row.getCell(excelHeader.get("amount"))).trim()
		TD_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("alt_caller_id"))).trim()
		TD_optional_trasaction_qualifier1 = convertHSSFCellToString(row.getCell(excelHeader.get("optional_trasaction_qualifier1"))).trim()
		TD_optional_trasaction_qualifier2 = convertHSSFCellToString(row.getCell(excelHeader.get("optional_trasaction_qualifier2"))).trim()
		TD_set_client_usg_ptd_threshold_Apply = convertHSSFCellToString(row.getCell(excelHeader.get("set_client_usg_ptd_threshold_Apply"))).trim()
		TD_set_client_usg_ptd_threshold_imm = convertHSSFCellToString(row.getCell(excelHeader.get("set_client_usg_ptd_threshold_imm"))).trim()
		TD_amount_ptd = convertHSSFCellToString(row.getCell(excelHeader.get("amount_ptd"))).trim()
		TD_alt_caller_id_ptd = convertHSSFCellToString(row.getCell(excelHeader.get("alt_caller_id_ptd"))).trim()
		TD_optional_trasaction_qualifier1_ptd = convertHSSFCellToString(row.getCell(excelHeader.get("optional_trasaction_qualifier1_ptd"))).trim()
		TD_optional_trasaction_qualifier2_ptd = convertHSSFCellToString(row.getCell(excelHeader.get("optional_trasaction_qualifier2_ptd"))).trim()
		TD_record_usage_apply = convertHSSFCellToString(row.getCell(excelHeader.get("record_usage_apply"))).trim()
		TD_record_usage_imm = convertHSSFCellToString(row.getCell(excelHeader.get("record_usage_imm"))).trim()
		TD_used_id_rec = convertHSSFCellToString(row.getCell(excelHeader.get("used_id_rec"))).trim()
		TD_usage_type = convertHSSFCellToString(row.getCell(excelHeader.get("usage_type"))).trim()
		TD_usage_units = convertHSSFCellToString(row.getCell(excelHeader.get("usage_units"))).trim()
		TD_usage_date = convertHSSFCellToString(row.getCell(excelHeader.get("usage_date"))).trim()
		TD_billable_units = convertHSSFCellToString(row.getCell(excelHeader.get("billable_units"))).trim()
		TD_amt = convertHSSFCellToString(row.getCell(excelHeader.get("amt"))).trim()
		TD_rate = convertHSSFCellToString(row.getCell(excelHeader.get("rate"))).trim()
		TD_telco_from = convertHSSFCellToString(row.getCell(excelHeader.get("telco_from"))).trim()
		TD_telco_to = convertHSSFCellToString(row.getCell(excelHeader.get("telco_to"))).trim()
		TD_comments = convertHSSFCellToString(row.getCell(excelHeader.get("comments"))).trim()
		TD_exclude_from_billing = convertHSSFCellToString(row.getCell(excelHeader.get("exclude_from_billing"))).trim()
		TD_exclusion_comments = convertHSSFCellToString(row.getCell(excelHeader.get("exclusion_comments"))).trim()
		TD_qualifier1 = convertHSSFCellToString(row.getCell(excelHeader.get("qualifier1"))).trim()
		TD_qualifier2 = convertHSSFCellToString(row.getCell(excelHeader.get("qualifier2"))).trim()
		TD_qualifier3 = convertHSSFCellToString(row.getCell(excelHeader.get("qualifier3"))).trim()
		TD_qualifier4 = convertHSSFCellToString(row.getCell(excelHeader.get("qualifier4"))).trim()
		TD_parent_rec_no = convertHSSFCellToString(row.getCell(excelHeader.get("parent_rec_no"))).trim()
		TD_usage_type_code = convertHSSFCellToString(row.getCell(excelHeader.get("usage_type_code"))).trim()
		TD_client_record_id = convertHSSFCellToString(row.getCell(excelHeader.get("client_record_id"))).trim()
		TD_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("caller_id"))).trim()
		TD_alt_caller_id_rec_usg = convertHSSFCellToString(row.getCell(excelHeader.get("alt_caller_id_rec_usg"))).trim()
		TD_optional_transaction_qualifier_rec_usg_1 = convertHSSFCellToString(row.getCell(excelHeader.get("optional_transaction_qualifier_rec_usg_1"))).trim()
		TD_optional_transaction_qualifier_rec_usg_2 = convertHSSFCellToString(row.getCell(excelHeader.get("optional_transaction_qualifier_rec_usg_2"))).trim()
		TD_gen_invoice_apply = convertHSSFCellToString(row.getCell(excelHeader.get("gen_invoice_apply"))).trim()
		TD_gen_invoice_imm = convertHSSFCellToString(row.getCell(excelHeader.get("gen_invoice_imm"))).trim()
		TD_force_pending = convertHSSFCellToString(row.getCell(excelHeader.get("force_pending"))).trim()
		TD_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("client_receipt_id"))).trim()
		TD_alt_bill_day = convertHSSFCellToString(row.getCell(excelHeader.get("alt_bill_day"))).trim()
		TD_alt_caller_id_gen_inv = convertHSSFCellToString(row.getCell(excelHeader.get("alt_caller_id_gen_inv"))).trim()

		TD_set_acct_usg_mtd_threshold_Apply = convertHSSFCellToString(row.getCell(excelHeader.get("set_acct_usg_mtd_threshold_Apply"))).trim()
		TD_set_acct_usg_mtd_threshold_imm = convertHSSFCellToString(row.getCell(excelHeader.get("set_acct_usg_mtd_threshold_imm"))).trim()
		TD_amount_set_acct = convertHSSFCellToString(row.getCell(excelHeader.get("amount_set_acct"))).trim()
		TD_alt_caller_id_set_acct = convertHSSFCellToString(row.getCell(excelHeader.get("alt_caller_id_set_acct"))).trim()

		TD_set_acct_usg_ptd_threshold_Apply = convertHSSFCellToString(row.getCell(excelHeader.get("set_acct_usg_ptd_threshold_Apply"))).trim()
		TD_set_acct_usg_ptd_threshold_imm = convertHSSFCellToString(row.getCell(excelHeader.get("set_acct_usg_ptd_threshold_imm"))).trim()
		TD_amount_set_acct_ptd = convertHSSFCellToString(row.getCell(excelHeader.get("amount_set_acct_ptd"))).trim()
		TD_alt_caller_id_set_acct_ptd = convertHSSFCellToString(row.getCell(excelHeader.get("alt_caller_id_set_acct_ptd"))).trim()

		TD_reset_usg_mtd_bal_Apply = convertHSSFCellToString(row.getCell(excelHeader.get("reset_usg_mtd_bal_Apply"))).trim()
		TD_reset_usg_mtd_bal_imm = convertHSSFCellToString(row.getCell(excelHeader.get("reset_usg_mtd_bal_imm"))).trim()
		TD_alt_caller_id_resetset_mtd = convertHSSFCellToString(row.getCell(excelHeader.get("alt_caller_id_resetset_mtd"))).trim()

		TD_reset_usg_ptd_bal_Apply = convertHSSFCellToString(row.getCell(excelHeader.get("reset_usg_ptd_bal_Apply"))).trim()
		TD_reset_usg_ptd_bal_imm = convertHSSFCellToString(row.getCell(excelHeader.get("reset_usg_ptd_bal_imm"))).trim()
		TD_alt_caller_id_resetset_ptd = convertHSSFCellToString(row.getCell(excelHeader.get("alt_caller_id_resetset_ptd"))).trim()


		TD_manage_pending_invoice_Apply = convertHSSFCellToString(row.getCell(excelHeader.get("manage_pending_invoice_Apply"))).trim()
		TD_manage_pending_invoice_imm = convertHSSFCellToString(row.getCell(excelHeader.get("manage_pending_invoice_imm"))).trim()
		TD_action_directive = convertHSSFCellToString(row.getCell(excelHeader.get("action_directive"))).trim()
		
		TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()


	}

	public def testDataUpdateAcctComplete(HSSFRow row)
	{
		TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
		TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
		TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
		TD_userid = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Status_Code"))).trim()
		TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
		TD_masterPlanUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Units"))).trim()
		TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
		TD_suppPlan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan2"))).trim()
		TD_suppPlan3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan3"))).trim()
		TD_suppPlanUnits1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units1"))).trim()
		TD_suppPlanUnits2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units2"))).trim()
		TD_suppPlanUnits3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units3"))).trim()
		TD_notifyMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Notify_Medhod"))).trim()
		TD_password = convertHSSFCellToString(row.getCell(excelHeader.get("Password"))).trim()
		TD_email = convertHSSFCellToString(row.getCell(excelHeader.get("Email"))).trim()
		TD_payMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Pay_Method"))).trim()
		TD_bank_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("bank_acct_no"))).trim()
		TD_bank_routing_number = convertHSSFCellToString(row.getCell(excelHeader.get("bank_routing_number"))).trim()
		TD_ccNumber = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Number"))).trim()
		TD_ccExpireMM = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_MM"))).trim()
		TD_ccExpireYYYY = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_YYYY"))).trim()
		TD_collection_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group1"))).trim()
		TD_collection_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group2"))).trim()

		TD_testAcctMethod = convertHSSFCellToString(row.getCell(excelHeader.get("test_acct_ind"))).trim()
		TD_doFullInvoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_full_invoicing"))).trim()
		TD_do_prorated_invoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_prorated_invoicing"))).trim()
		TD_couponCode1 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code1"))).trim()
		TD_couponCode2 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code2"))).trim()
		TD_couponCode3 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code3"))).trim()
		TD_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date"))).trim()
		TD_contract_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_plan_no"))).trim()
		TD_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no"))).trim()
		TD_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months"))).trim()

		TD_doWrite = convertHSSFCellToString(row.getCell(excelHeader.get("do_write"))).trim()
		TD_taxExemptionLevel = convertHSSFCellToString(row.getCell(excelHeader.get("Tax_Exemption_Level"))).trim()
		TD_retroActiveStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Retroactive_Start_Date"))).trim()
		TD_altStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Start_Date"))).trim()
		TD_altBillDay = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Billing_Day"))).trim()
		TD_assignmentdirective = convertHSSFCellToString(row.getCell(excelHeader.get("AD"))).trim()
		TD_currency_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Currency_cd"))).trim()

		TD_custom_rate_plan_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1"))).trim()
		TD_cutom_rate_service_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1"))).trim()
		TD_custom_rate_seq_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1"))).trim()
		TD_custom_rate_from_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1"))).trim()
		TD_custom_rate_to_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1"))).trim()
		TD_custom_rate_per_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1"))).trim()
		TD_custom_rate_plan_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2"))).trim()
		TD_cutom_rate_service_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2"))).trim()
		TD_custom_rate_seq_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2"))).trim()
		TD_custom_rate_from_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2"))).trim()
		TD_custom_rate_to_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2"))).trim()
		TD_custom_rate_per_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2"))).trim()

		TD_functional_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group1"))).trim()
		TD_functional_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group2"))).trim()
		TD_status_until_alt_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("status_until_alt_start_date"))).trim()
		TD_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee"))).trim()

		TD_firstName = convertHSSFCellToString(row.getCell(excelHeader.get("First_Name"))).trim()
		TD_mi = convertHSSFCellToString(row.getCell(excelHeader.get("MI"))).trim()
		TD_lastName = convertHSSFCellToString(row.getCell(excelHeader.get("Last_Name"))).trim()
		TD_companyName = convertHSSFCellToString(row.getCell(excelHeader.get("Company_Name"))).trim()
		TD_addr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr1"))).trim()
		TD_addr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr2"))).trim()
		TD_city = convertHSSFCellToString(row.getCell(excelHeader.get("City"))).trim()
		TD_state = convertHSSFCellToString(row.getCell(excelHeader.get("State"))).trim()
		TD_locality = convertHSSFCellToString(row.getCell(excelHeader.get("Locality"))).trim()
		TD_country = convertHSSFCellToString(row.getCell(excelHeader.get("Country"))).trim()
		TD_postalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Postal_Code"))).trim()
		TD_phone = convertHSSFCellToString(row.getCell(excelHeader.get("Phone"))).trim()

		TD_billFirstName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_First_Name"))).trim()
		TD_billMi = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_MI"))).trim()
		TD_billLastName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Last_Name"))).trim()
		TD_billCompanyName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Company_Name"))).trim()
		TD_billAddr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr1"))).trim()
		TD_billAddr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr2"))).trim()
		TD_billCity = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_City"))).trim()
		TD_billState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_State"))).trim()
		TD_billLocality = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Locality"))).trim()
		TD_billCountry = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Country"))).trim()
		TD_billPostalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Postal_Code"))).trim()
		TD_billPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone"))).trim()
		TD_billEmail = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Email"))).trim()
		TD_respLevelCd = convertHSSFCellToString(row.getCell(excelHeader.get("Resp_Level_Cd"))).trim()
		TD_qualifierName1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name1"))).trim()
		TD_qualifierName2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name2"))).trim()
		TD_qualifierValue1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value1"))).trim()
		TD_qualifierValue2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value2"))).trim()

		TD_clientAcctId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Acct_Id"))).trim()
		TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Cd"))).trim()
		TD_secretQuestion = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question"))).trim()
		TD_secretQuestionAanswer = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question_Answer"))).trim()
		TD_phoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Phone_Ext"))).trim()
		TD_cellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Cell_Phone"))).trim()
		TD_workPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone"))).trim()
		TD_workPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone_Ext"))).trim()
		TD_birthDate = convertHSSFCellToString(row.getCell(excelHeader.get("Birthdate"))).trim()
		TD_billPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone_Ext"))).trim()
		TD_billCellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Cell_Phone"))).trim()
		TD_billWorkPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone"))).trim()
		TD_billWorkPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone_Ext"))).trim()
		TD_balanceForward = convertHSSFCellToString(row.getCell(excelHeader.get("Balance_Forward"))).trim()
		TD_masterPlanAltRateSchedNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Alt_Rate_Sched_No"))).trim()
		TD_clientReceiptId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
		TD_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("Cvv"))).trim()
		TD_taxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Taxpayer_Id"))).trim()
		TD_billAgreementId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Agreement_Id"))).trim()
		TD_altMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Msg_Template_No"))).trim()
		TD_seqFuncGroupNo = convertHSSFCellToString(row.getCell(excelHeader.get("Seq_Func_Group_No"))).trim()
		TD_bankAcctType = convertHSSFCellToString(row.getCell(excelHeader.get("Bank_Acct_Type"))).trim()
		TD_billDriversLicenseNo = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_No"))).trim()
		TD_billDriversLicenseState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_State"))).trim()
		TD_billTaxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Taxpayer_Id"))).trim()
		TD_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("Address3"))).trim()
		TD_billAddress3 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Address3"))).trim()
		TD_trackData1 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data1"))).trim()
		TD_trackData2 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data2"))).trim()
		TD_cnAltMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Cn_Alt_Msg_Template_No"))).trim()
		TD_altCallerId = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()

		//UpdateAccountComplete

		TD_update_acct_imm = convertHSSFCellToString(row.getCell(excelHeader.get("update_acct_complete_imm"))).trim()
		TD_uac_first_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_first_name"))).trim()
		TD_uac_last_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_last_name"))).trim()
		TD_uac_middle_initial = convertHSSFCellToString(row.getCell(excelHeader.get("uac_middle_initial"))).trim()
		TD_uac_company_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_company_name"))).trim()
		TD_uac_address1 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_address1"))).trim()
		TD_uac_address2 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_address2"))).trim()
		TD_uac_city = convertHSSFCellToString(row.getCell(excelHeader.get("uac_city"))).trim()
		TD_uac_locality = convertHSSFCellToString(row.getCell(excelHeader.get("uac_locality"))).trim()
		TD_uac_state_prov = convertHSSFCellToString(row.getCell(excelHeader.get("uac_state_prov"))).trim()

		TD_uac_country = convertHSSFCellToString(row.getCell(excelHeader.get("uac_country"))).trim()
		TD_uac_postal_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_postal_cd"))).trim()
		TD_uac_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_phone"))).trim()
		TD_uac_phone_ext = convertHSSFCellToString(row.getCell(excelHeader.get("uac_phone_ext"))).trim()
		TD_uac_cell_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cell_phone"))).trim()
		TD_uac_work_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_work_phone"))).trim()
		TD_uac_work_phone_ext = convertHSSFCellToString(row.getCell(excelHeader.get("uac_work_phone_ext"))).trim()
		TD_uac_email = convertHSSFCellToString(row.getCell(excelHeader.get("uac_email"))).trim()
		TD_uac_birthdate = convertHSSFCellToString(row.getCell(excelHeader.get("uac_birthdate"))).trim()
		TD_uac_bill_first_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_first_name"))).trim()

		TD_uac_bill_last_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_last_name"))).trim()
		TD_uac_bill_middle_initial = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_middle_initial"))).trim()
		TD_uac_bill_company_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_company_name"))).trim()
		TD_uac_bill_address1 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_address1"))).trim()
		TD_uac_bill_address2 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_address2"))).trim()
		TD_uac_bill_city = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_city"))).trim()
		TD_uac_bill_locality = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_locality"))).trim()
		TD_uac_bill_state_prov = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_state_prov"))).trim()
		TD_uac_bill_country = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_country"))).trim()
		TD_uac_bill_postal_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_postal_cd"))).trim()

		TD_uac_bill_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_phone"))).trim()
		TD_uac_bill_phone_ext = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_phone_ext"))).trim()
		TD_uac_bill_cell_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_cell_phone"))).trim()
		TD_uac_bill_work_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_work_phone"))).trim()
		TD_uac_bill_work_phone_ext = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_work_phone_ext"))).trim()
		TD_uac_bill_email = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_email"))).trim()
		TD_uac_pay_method = convertHSSFCellToString(row.getCell(excelHeader.get("uac_pay_method"))).trim()
		TD_uac_cc_number = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cc_number"))).trim()
		TD_uac_cc_expire_mm = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cc_expire_mm"))).trim()
		TD_uac_cc_expire_yyyy = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cc_expire_yyyy"))).trim()

		TD_uac_bank_routing_num = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bank_routing_num"))).trim()
		TD_uac_bank_acct_num = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bank_acct_num"))).trim()
		TD_uac_master_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_plan_no"))).trim()
		TD_uac_master_plan_alt_rate_sched_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_plan_alt_rate_sched_no"))).trim()
		TD_uac_master_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_plan_units"))).trim()
		TD_uac_master_plan_assign_directive = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_plan_assign_directive"))).trim()
		TD_uac_functional_acct_groups = convertHSSFCellToString(row.getCell(excelHeader.get("uac_functional_acct_groups"))).trim()
		TD_uac_functional_group_directives = convertHSSFCellToString(row.getCell(excelHeader.get("uac_functional_group_directives"))).trim()
		TD_uac_client_func_acct_group_ids = convertHSSFCellToString(row.getCell(excelHeader.get("uac_client_func_acct_group_ids"))).trim()
		TD_uac_collections_acct_groups = convertHSSFCellToString(row.getCell(excelHeader.get("uac_collections_acct_groups"))).trim()

		TD_uac_collections_group_directives = convertHSSFCellToString(row.getCell(excelHeader.get("uac_collections_group_directives"))).trim()
		TD_uac_client_coll_acct_group_ids = convertHSSFCellToString(row.getCell(excelHeader.get("uac_client_coll_acct_group_ids"))).trim()
		TD_uac_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_status_cd"))).trim()
		TD_uac_notify_method = convertHSSFCellToString(row.getCell(excelHeader.get("uac_notify_method"))).trim()
		TD_uac_password = convertHSSFCellToString(row.getCell(excelHeader.get("uac_password"))).trim()
		TD_uac_secret_question = convertHSSFCellToString(row.getCell(excelHeader.get("uac_secret_question"))).trim()
		TD_uac_secret_question_answer = convertHSSFCellToString(row.getCell(excelHeader.get("uac_secret_question_answer"))).trim()
		TD_uac_pin = convertHSSFCellToString(row.getCell(excelHeader.get("uac_pin"))).trim()
		TD_uac_test_acct_ind = convertHSSFCellToString(row.getCell(excelHeader.get("uac_test_acct_ind"))).trim()
		TD_uac_resp_level_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_resp_level_cd"))).trim()

		TD_uac_senior_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_senior_acct_no"))).trim()
		TD_uac_client_acct_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_client_acct_id"))).trim()
		TD_uac_do_collect = convertHSSFCellToString(row.getCell(excelHeader.get("uac_do_collect"))).trim()
		TD_uac_change_status_after_coll = convertHSSFCellToString(row.getCell(excelHeader.get("uac_change_status_after_coll"))).trim()
		TD_uac_reset_dates_after_status = convertHSSFCellToString(row.getCell(excelHeader.get("uac_reset_dates_after_status"))).trim()
		TD_uac_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_client_receipt_id"))).trim()
		TD_uac_alt_do_dunning = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_do_dunning"))).trim()
		TD_uac_force_currency_change = convertHSSFCellToString(row.getCell(excelHeader.get("uac_force_currency_change"))).trim()
		TD_uac_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cvv"))).trim()
		TD_uac_taxpayer_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_taxpayer_id"))).trim()
		//
		TD_uac_bill_agreement_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_agreement_id"))).trim()
		TD_uac_auto_cancel_supp_plans = convertHSSFCellToString(row.getCell(excelHeader.get("uac_auto_cancel_supp_plans"))).trim()
		TD_uac_offset_months = convertHSSFCellToString(row.getCell(excelHeader.get("uac_offset_months"))).trim()
		TD_uac_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_proration_start_date"))).trim()
		TD_uac_alt_msg_template_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_msg_template_no"))).trim()
		TD_uac_seq_func_group_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_seq_func_group_no"))).trim()
		TD_uac_bank_acct_type = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bank_acct_type"))).trim()
		TD_uac_bill_drivers_license_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_drivers_license_no"))).trim()
		TD_uac_bill_drivers_license_state = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_drivers_license_state"))).trim()
		TD_uac_bill_taxpayer_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_taxpayer_id"))).trim()

		TD_uac_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_address3"))).trim()
		TD_uac_bill_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_address3"))).trim()
		TD_uac_usage_accumulation_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_usage_accumulation_plan_no"))).trim()
		TD_uac_usage_accumulation_reset_months = convertHSSFCellToString(row.getCell(excelHeader.get("uac_usage_accumulation_reset_months"))).trim()
		TD_uac_enable_usage_pooling_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_enable_usage_pooling_plan_no"))).trim()
		TD_uac_disable_usage_pooling_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_disable_usage_pooling_plan_no"))).trim()
		TD_uac_alt_client_acct_group_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_client_acct_group_id"))).trim()
		TD_uac_track_data1 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_track_data1"))).trim()
		TD_uac_track_data2 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_track_data2"))).trim()
		TD_uac_offset_interval = convertHSSFCellToString(row.getCell(excelHeader.get("uac_offset_interval"))).trim()


		TD_uac_tax_exemption_level = convertHSSFCellToString(row.getCell(excelHeader.get("uac_tax_exemption_level"))).trim()
		TD_uac_cn_alt_msg_template_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cn_alt_msg_template_no"))).trim()
		TD_uac_promo_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_promo_cd"))).trim()
		TD_uac_invoice_unbilled_usage = convertHSSFCellToString(row.getCell(excelHeader.get("uac_invoice_unbilled_usage"))).trim()
		TD_uac_coupon_code = convertHSSFCellToString(row.getCell(excelHeader.get("uac_coupon_code"))).trim()
		TD_uac_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_caller_id"))).trim()
		TD_uac_qualifier_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_qualifier_name"))).trim()
		TD_uac_qualifier_value = convertHSSFCellToString(row.getCell(excelHeader.get("uac_qualifier_value"))).trim()

		TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()
	}




	//Ram Integration2 Case
	public def testDataIntegration2(HSSFRow row)
	{
		String testd = null
		//CreateAccountComplete And AssignSuppPlan
		//testd = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No")))
		TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
		TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
		TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
		//TD_userid = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_AVT = convertHSSFCellToString(row.getCell(excelHeader.get("AVT"))).trim()
		TD_Advance_VirtualTime = convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).trim()
		TD_TestSet = convertHSSFCellToString(row.getCell(excelHeader.get("TestSet"))).trim()
		TD_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Status_Code"))).trim()
		TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
		TD_masterPlanUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Units"))).trim()
		TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
		//TD_suppPlan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan2"))).trim()
		//TD_suppPlan3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan3"))).trim()
		TD_suppPlanUnits1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units1"))).trim()
		//TD_suppPlanUnits2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units2"))).trim()
		//TD_suppPlanUnits3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units3"))).trim()
		TD_notifyMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Notify_Medhod"))).trim()
		TD_password = convertHSSFCellToString(row.getCell(excelHeader.get("Password"))).trim()
		TD_email = convertHSSFCellToString(row.getCell(excelHeader.get("Email"))).trim()
		TD_payMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Pay_Method"))).trim()
		TD_bank_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("bank_acct_no"))).trim()
		TD_bank_routing_number = convertHSSFCellToString(row.getCell(excelHeader.get("bank_routing_number"))).trim()
		TD_ccNumber = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Number"))).trim()
		TD_ccExpireMM = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_MM"))).trim()
		TD_ccExpireYYYY = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_YYYY"))).trim()
		TD_collection_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group1"))).trim()
		TD_collection_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group2"))).trim()


		TD_testAcctMethod = convertHSSFCellToString(row.getCell(excelHeader.get("test_acct_ind"))).trim()
		TD_doFullInvoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_full_invoicing"))).trim()
		TD_do_prorated_invoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_prorated_invoicing"))).trim()

		TD_couponCode1 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code1"))).trim()
		TD_couponCode2 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code2"))).trim()
		TD_couponCode3 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code3"))).trim()
		TD_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date"))).trim()
		TD_contract_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_plan_no"))).trim()
		TD_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no"))).trim()
		TD_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months"))).trim()

		TD_doWrite = convertHSSFCellToString(row.getCell(excelHeader.get("do_write"))).trim()
		TD_taxExemptionLevel = convertHSSFCellToString(row.getCell(excelHeader.get("Tax_Exemption_Level"))).trim()
		TD_retroActiveStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Retroactive_Start_Date"))).trim()
		TD_altStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Start_Date"))).trim()
		TD_altBillDay = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Billing_Day"))).trim()
		TD_assignmentdirective = convertHSSFCellToString(row.getCell(excelHeader.get("AD"))).trim()
		TD_currency_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Currency_cd"))).trim()

		TD_custom_rate_plan_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1"))).trim()
		TD_cutom_rate_service_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1"))).trim()
		TD_custom_rate_seq_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1"))).trim()
		TD_custom_rate_from_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1"))).trim()
		TD_custom_rate_to_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1"))).trim()
		TD_custom_rate_per_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1"))).trim()
		TD_custom_rate_plan_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2"))).trim()
		TD_cutom_rate_service_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2"))).trim()
		TD_custom_rate_seq_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2"))).trim()
		TD_custom_rate_from_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2"))).trim()
		TD_custom_rate_to_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2"))).trim()
		TD_custom_rate_per_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2"))).trim()

		TD_functional_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group1"))).trim()
		TD_functional_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group2"))).trim()
		TD_status_until_alt_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("status_until_alt_start_date"))).trim()
		TD_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee"))).trim()

		TD_firstName = convertHSSFCellToString(row.getCell(excelHeader.get("First_Name"))).trim()
		TD_mi = convertHSSFCellToString(row.getCell(excelHeader.get("MI"))).trim()
		TD_lastName = convertHSSFCellToString(row.getCell(excelHeader.get("Last_Name"))).trim()
		TD_companyName = convertHSSFCellToString(row.getCell(excelHeader.get("Company_Name"))).trim()
		TD_addr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr1"))).trim()
		TD_addr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr2"))).trim()
		TD_city = convertHSSFCellToString(row.getCell(excelHeader.get("City"))).trim()
		TD_state = convertHSSFCellToString(row.getCell(excelHeader.get("State"))).trim()
		TD_locality = convertHSSFCellToString(row.getCell(excelHeader.get("Locality"))).trim()
		TD_country = convertHSSFCellToString(row.getCell(excelHeader.get("Country"))).trim()
		TD_postalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Postal_Code"))).trim()
		TD_phone = convertHSSFCellToString(row.getCell(excelHeader.get("Phone"))).trim()

		TD_billFirstName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_First_Name"))).trim()
		TD_billMi = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_MI"))).trim()
		TD_billLastName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Last_Name"))).trim()
		TD_billCompanyName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Company_Name"))).trim()
		TD_billAddr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr1"))).trim()
		TD_billAddr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr2"))).trim()
		TD_billCity = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_City"))).trim()
		TD_billState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_State"))).trim()
		TD_billLocality = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Locality"))).trim()
		TD_billCountry = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Country"))).trim()
		TD_billPostalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Postal_Code"))).trim()
		TD_billPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone"))).trim()
		TD_billEmail = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Email"))).trim()
		TD_respLevelCd = convertHSSFCellToString(row.getCell(excelHeader.get("Resp_Level_Cd"))).trim()
		TD_qualifierName1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name1"))).trim()
		TD_qualifierName2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name2"))).trim()
		TD_qualifierValue1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value1"))).trim()
		TD_qualifierValue2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value2"))).trim()

		TD_clientAcctId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Acct_Id"))).trim()
		TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Cd"))).trim()
		TD_secretQuestion = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question"))).trim()
		TD_secretQuestionAanswer = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question_Answer"))).trim()
		TD_phoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Phone_Ext"))).trim()
		TD_cellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Cell_Phone"))).trim()
		TD_workPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone"))).trim()
		TD_workPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone_Ext"))).trim()
		TD_birthDate = convertHSSFCellToString(row.getCell(excelHeader.get("Birthdate"))).trim()
		TD_billPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone_Ext"))).trim()
		TD_billCellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Cell_Phone"))).trim()
		TD_billWorkPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone"))).trim()
		TD_billWorkPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone_Ext"))).trim()
		TD_balanceForward = convertHSSFCellToString(row.getCell(excelHeader.get("Balance_Forward"))).trim()
		TD_masterPlanAltRateSchedNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Alt_Rate_Sched_No"))).trim()
		TD_clientReceiptId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
		TD_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("Cvv"))).trim()
		/*TD_taxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Taxpayer_Id"))).trim()
		 TD_billAgreementId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Agreement_Id"))).trim()
		 TD_altMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Msg_Template_No"))).trim()
		 TD_seqFuncGroupNo = convertHSSFCellToString(row.getCell(excelHeader.get("Seq_Func_Group_No"))).trim()
		 TD_bankAcctType = convertHSSFCellToString(row.getCell(excelHeader.get("Bank_Acct_Type"))).trim()
		 TD_billDriversLicenseNo = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_No"))).trim()
		 TD_billDriversLicenseState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_State"))).trim()
		 TD_billTaxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Taxpayer_Id"))).trim()
		 TD_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("Address3"))).trim()
		 TD_billAddress3 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Address3"))).trim()
		 TD_trackData1 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data1"))).trim()
		 TD_trackData2 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data2"))).trim()
		 TD_cnAltMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Cn_Alt_Msg_Template_No"))).trim()
		 TD_altCallerId = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()
		 */

		//assign_supp_plam

		/*TD_assign_supp_plan = convertHSSFCellToString(row.getCell(excelHeader.get("Assign_Supp_Plan"))).trim()
		 TD_Advance_VirtualTime = convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).trim()
		 TD_VerifyDate = convertHSSFCellToString(row.getCell(excelHeader.get("Verify_Date"))).trim()
		 TD_altrateschedulenosp = convertHSSFCellToString(row.getCell(excelHeader.get("alt_rate_schedule_no_SP"))).trim()
		 TD_altclientacctgroupid = convertHSSFCellToString(row.getCell(excelHeader.get("alt_client_acct_group_id"))).trim()
		 TD_doWriteSP = convertHSSFCellToString(row.getCell(excelHeader.get("Do_write_SP"))).trim()
		 TD_num_plan_units_sp = convertHSSFCellToString(row.getCell(excelHeader.get("num_plan_units_sp"))).trim()
		 TD_coupon_code_sp = convertHSSFCellToString(row.getCell(excelHeader.get("coupon_code_sp"))).trim()
		 TD_comments_sp = convertHSSFCellToString(row.getCell(excelHeader.get("comments_sp"))).trim()
		 TD_client_receipt_id_sp = convertHSSFCellToString(row.getCell(excelHeader.get("client_receipt_id_sp"))).trim()
		 TD_contract_type_no_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no_sp"))).trim()
		 TD_contract_alt_recur_fee_no_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee_no_sp"))).trim()
		 TD_contract_length_months_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months_sp"))).trim()
		 TD_contract_cancel_fee_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_cancel_fee_sp"))).trim()
		 TD_contract_comments_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_comments_sp"))).trim()
		 TD_contract_start_date_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date_sp"))).trim()
		 TD_offset_months_sp = convertHSSFCellToString(row.getCell(excelHeader.get("offset_months_sp"))).trim()
		 TD_auto_offset_months_options = convertHSSFCellToString(row.getCell(excelHeader.get("auto_offset_months_options_sp"))).trim()
		 TD_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("alt_proration_start_date_sp"))).trim()
		 */
		TD_effectiveDate = convertHSSFCellToString(row.getCell(excelHeader.get("Effective_Date"))).trim()
		//TD_assign_sp_imm = convertHSSFCellToString(row.getCell(excelHeader.get("assign_sp_imm"))).trim()


		//update_acct_complete

		TD_update_acct_complete_imm = convertHSSFCellToString(row.getCell(excelHeader.get("update_acct_complete_imm"))).trim()
		TD_uac_first_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_first_name"))).trim()
		TD_uac_last_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_last_name"))).trim()
		TD_uac_middle_initial = convertHSSFCellToString(row.getCell(excelHeader.get("uac_middle_initial"))).trim()
		TD_uac_company_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_company_name"))).trim()
		TD_uac_address1 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_address1"))).trim()
		TD_uac_address2 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_address2"))).trim()
		TD_uac_city = convertHSSFCellToString(row.getCell(excelHeader.get("uac_city"))).trim()
		TD_uac_locality = convertHSSFCellToString(row.getCell(excelHeader.get("uac_locality"))).trim()
		TD_uac_state_prov = convertHSSFCellToString(row.getCell(excelHeader.get("uac_state_prov"))).trim()

		TD_uac_country = convertHSSFCellToString(row.getCell(excelHeader.get("uac_country"))).trim()
		TD_uac_postal_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_postal_cd"))).trim()
		TD_uac_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_phone"))).trim()
		TD_uac_phone_ext = convertHSSFCellToString(row.getCell(excelHeader.get("uac_phone_ext"))).trim()
		TD_uac_cell_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cell_phone"))).trim()
		TD_uac_work_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_work_phone"))).trim()
		TD_uac_work_phone_ext = convertHSSFCellToString(row.getCell(excelHeader.get("uac_work_phone_ext"))).trim()
		TD_uac_email = convertHSSFCellToString(row.getCell(excelHeader.get("uac_email"))).trim()
		TD_uac_birthdate = convertHSSFCellToString(row.getCell(excelHeader.get("uac_birthdate"))).trim()
		TD_uac_bill_first_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_first_name"))).trim()

		TD_uac_bill_last_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_last_name"))).trim()
		TD_uac_bill_middle_initial = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_middle_initial"))).trim()
		TD_uac_bill_company_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_company_name"))).trim()
		TD_uac_bill_address1 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_address1"))).trim()
		TD_uac_bill_address2 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_address2"))).trim()
		TD_uac_bill_city = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_city"))).trim()
		TD_uac_bill_locality = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_locality"))).trim()
		TD_uac_bill_state_prov = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_state_prov"))).trim()
		TD_uac_bill_country = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_country"))).trim()
		TD_uac_bill_postal_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_postal_cd"))).trim()

		TD_uac_bill_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_phone"))).trim()
		TD_uac_bill_phone_ext = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_phone_ext"))).trim()
		TD_uac_bill_cell_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_cell_phone"))).trim()
		TD_uac_bill_work_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_work_phone"))).trim()
		TD_uac_bill_work_phone_ext = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_work_phone_ext"))).trim()
		TD_uac_bill_email = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_email"))).trim()
		TD_uac_pay_method = convertHSSFCellToString(row.getCell(excelHeader.get("uac_pay_method"))).trim()
		TD_uac_cc_number = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cc_number"))).trim()
		TD_uac_cc_expire_mm = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cc_expire_mm"))).trim()
		TD_uac_cc_expire_yyyy = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cc_expire_yyyy"))).trim()

		TD_uac_bank_routing_num = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bank_routing_num"))).trim()
		TD_uac_bank_acct_num = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bank_acct_num"))).trim()
		TD_uac_master_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_plan_no"))).trim()
		TD_uac_master_plan_alt_rate_sched_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_plan_alt_rate_sched_no"))).trim()
		TD_uac_master_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_plan_units"))).trim()
		//TD_uac_master_plan_assign_directive = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_plan_assign_directive"))).trim()

		/*TD_uac_functional_acct_groups = convertHSSFCellToString(row.getCell(excelHeader.get("uac_functional_acct_groups"))).trim()
		 TD_uac_functional_group_directives = convertHSSFCellToString(row.getCell(excelHeader.get("uac_functional_group_directives"))).trim()
		 TD_uac_client_func_acct_group_ids = convertHSSFCellToString(row.getCell(excelHeader.get("uac_client_func_acct_group_ids"))).trim()
		 TD_uac_collections_acct_groups = convertHSSFCellToString(row.getCell(excelHeader.get("uac_collections_acct_groups"))).trim()
		 TD_uac_collections_group_directives = convertHSSFCellToString(row.getCell(excelHeader.get("uac_collections_group_directives"))).trim()
		 TD_uac_client_coll_acct_group_ids = convertHSSFCellToString(row.getCell(excelHeader.get("uac_client_coll_acct_group_ids"))).trim()
		 TD_uac_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_status_cd"))).trim()
		 TD_uac_notify_method = convertHSSFCellToString(row.getCell(excelHeader.get("uac_notify_method"))).trim()
		 TD_uac_password = convertHSSFCellToString(row.getCell(excelHeader.get("uac_password"))).trim()
		 TD_uac_secret_question = convertHSSFCellToString(row.getCell(excelHeader.get("uac_secret_question"))).trim()
		 TD_uac_secret_question_answer = convertHSSFCellToString(row.getCell(excelHeader.get("uac_secret_question_answer"))).trim()
		 TD_uac_pin = convertHSSFCellToString(row.getCell(excelHeader.get("uac_pin"))).trim()
		 TD_uac_test_acct_ind = convertHSSFCellToString(row.getCell(excelHeader.get("uac_test_acct_ind"))).trim()
		 TD_uac_resp_level_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_resp_level_cd"))).trim()
		 TD_uac_senior_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_senior_acct_no"))).trim()
		 TD_uac_client_acct_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_client_acct_id"))).trim()
		 TD_uac_do_collect = convertHSSFCellToString(row.getCell(excelHeader.get("uac_do_collect"))).trim()
		 TD_uac_change_status_after_coll = convertHSSFCellToString(row.getCell(excelHeader.get("uac_change_status_after_coll"))).trim()
		 TD_uac_reset_dates_after_status = convertHSSFCellToString(row.getCell(excelHeader.get("uac_reset_dates_after_status"))).trim()
		 TD_uac_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_client_receipt_id"))).trim()
		 TD_uac_alt_do_dunning = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_do_dunning"))).trim()
		 TD_uac_force_currency_change = convertHSSFCellToString(row.getCell(excelHeader.get("uac_force_currency_change"))).trim()
		 TD_uac_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cvv"))).trim()
		 TD_uac_taxpayer_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_taxpayer_id"))).trim()
		 TD_uac_bill_agreement_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_agreement_id"))).trim()
		 TD_uac_auto_cancel_supp_plans = convertHSSFCellToString(row.getCell(excelHeader.get("uac_auto_cancel_supp_plans"))).trim()
		 TD_uac_offset_months = convertHSSFCellToString(row.getCell(excelHeader.get("uac_offset_months"))).trim()
		 TD_uac_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_proration_start_date"))).trim()
		 TD_uac_alt_msg_template_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_msg_template_no"))).trim()
		 TD_uac_seq_func_group_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_seq_func_group_no"))).trim()
		 TD_uac_bank_acct_type = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bank_acct_type"))).trim()
		 TD_uac_bill_drivers_license_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_drivers_license_no"))).trim()
		 TD_uac_bill_drivers_license_state = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_drivers_license_state"))).trim()
		 TD_uac_bill_taxpayer_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_taxpayer_id"))).trim()
		 TD_uac_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_address3"))).trim()
		 TD_uac_bill_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_address3"))).trim()
		 TD_uac_usage_accumulation_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_usage_accumulation_plan_no"))).trim()
		 TD_uac_usage_accumulation_reset_months = convertHSSFCellToString(row.getCell(excelHeader.get("uac_usage_accumulation_reset_months"))).trim()
		 TD_uac_enable_usage_pooling_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_enable_usage_pooling_plan_no"))).trim()
		 TD_uac_disable_usage_pooling_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_disable_usage_pooling_plan_no"))).trim()
		 TD_uac_alt_client_acct_group_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_client_acct_group_id"))).trim()
		 TD_uac_track_data1 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_track_data1"))).trim()
		 TD_uac_track_data2 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_track_data2"))).trim()
		 TD_uac_offset_interval = convertHSSFCellToString(row.getCell(excelHeader.get("uac_offset_interval"))).trim()
		 TD_uac_tax_exemption_level = convertHSSFCellToString(row.getCell(excelHeader.get("uac_tax_exemption_level"))).trim()
		 TD_uac_cn_alt_msg_template_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cn_alt_msg_template_no"))).trim()
		 TD_uac_promo_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_promo_cd"))).trim()
		 TD_uac_invoice_unbilled_usage = convertHSSFCellToString(row.getCell(excelHeader.get("uac_invoice_unbilled_usage"))).trim()
		 */
		TD_uac_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_status_cd"))).trim()
		TD_uac_coupon_code = convertHSSFCellToString(row.getCell(excelHeader.get("uac_coupon_code"))).trim()
		/*TD_uac_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_caller_id"))).trim()
		 TD_uac_qualifier_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_qualifier_name"))).trim()
		 TD_uac_qualifier_value = convertHSSFCellToString(row.getCell(excelHeader.get("uac_qualifier_value"))).trim()
		 */

		//modify_supp_plan

		TD_modify_supp_plan_imm = convertHSSFCellToString(row.getCell(excelHeader.get("modify_supp_plan_imm"))).trim()
		TD_msp_supp_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("msp_supp_plan_no"))).trim()
		TD_msp_alt_rate_schedule_no = convertHSSFCellToString(row.getCell(excelHeader.get("msp_alt_rate_schedule_no"))).trim()
		TD_msp_num_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("msp_num_plan_units"))).trim()
		TD_msp_coupon_code = convertHSSFCellToString(row.getCell(excelHeader.get("msp_coupon_code"))).trim()
		//TD_msp_assignment_directive = convertHSSFCellToString(row.getCell(excelHeader.get("msp_assignment_directive"))).trim()
		TD_msp_comments = convertHSSFCellToString(row.getCell(excelHeader.get("msp_comments"))).trim()
		TD_msp_do_write = convertHSSFCellToString(row.getCell(excelHeader.get("msp_do_write"))).trim()
		TD_msp_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("msp_client_receipt_id"))).trim()

		TD_msp_custom_rate_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("msp_custom_rate_plan_no"))).trim()
		TD_msp_custom_rate_service_no = convertHSSFCellToString(row.getCell(excelHeader.get("msp_custom_rate_service_no"))).trim()
		TD_msp_custom_rate_seq_no = convertHSSFCellToString(row.getCell(excelHeader.get("msp_custom_rate_seq_no"))).trim()
		TD_msp_custom_rate_from_unit = convertHSSFCellToString(row.getCell(excelHeader.get("msp_custom_rate_from_unit"))).trim()
		TD_msp_custom_rate_to_unit = convertHSSFCellToString(row.getCell(excelHeader.get("msp_custom_rate_to_unit"))).trim()
		TD_msp_custom_rate_per_unit = convertHSSFCellToString(row.getCell(excelHeader.get("msp_custom_rate_per_unit"))).trim()

		TD_msp_effective_date = convertHSSFCellToString(row.getCell(excelHeader.get("msp_effective_date"))).trim()
		TD_msp_offset_interval = convertHSSFCellToString(row.getCell(excelHeader.get("msp_offset_interval"))).trim()
		TD_msp_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("msp_alt_caller_id"))).trim()

		/*TD_msp_qualifier_name = convertHSSFCellToString(row.getCell(excelHeader.get("msp_qualifier_name"))).trim()
		 TD_msp_qualifier_value = convertHSSFCellToString(row.getCell(excelHeader.get("msp_qualifier_value"))).trim()
		 */

		//replace_supp_plan
		TD_replace_supp_plan_imm = convertHSSFCellToString(row.getCell(excelHeader.get("replace_supp_plan_imm"))).trim()

		TD_rsp_existing_supp_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_existing_supp_plan_no"))).trim()
		TD_rsp_new_supp_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_new_supp_plan_no"))).trim()
		TD_rsp_alt_rate_schedule_no = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_alt_rate_schedule_no"))).trim()
		TD_rsp_num_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_num_plan_units"))).trim()
		TD_rsp_coupon_code = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_coupon_code"))).trim()
		//TD_rsp_assignment_directive = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_assignment_directive"))).trim()
		TD_rsp_comments = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_comments"))).trim()
		TD_rsp_do_write = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_do_write"))).trim()
		TD_rsp_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_client_receipt_id"))).trim()
		TD_rsp_offset_months = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_offset_months"))).trim()

		TD_rsp_auto_offset_months_option = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_auto_offset_months_option"))).trim()
		TD_rsp_alt_client_acct_group_id = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_alt_client_acct_group_id"))).trim()
		TD_rsp_custom_rate_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_custom_rate_plan_no"))).trim()
		TD_rsp_custom_rate_service_no = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_custom_rate_service_no"))).trim()
		TD_rsp_custom_rate_seq_no = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_custom_rate_seq_no"))).trim()
		TD_rsp_custom_rate_from_unit = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_custom_rate_from_unit"))).trim()
		TD_rsp_custom_rate_to_unit = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_custom_rate_to_unit"))).trim()
		TD_rsp_custom_rate_per_unit = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_custom_rate_per_unit"))).trim()
		TD_rsp_effective_date = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_effective_date"))).trim()
		TD_rsp_offset_interval = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_offset_interval"))).trim()

		TD_rsp_invoice_unbilled_usage = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_invoice_unbilled_usage"))).trim()
		TD_rsp_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_alt_caller_id"))).trim()

		/*TD_rsp_qualifier_name = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_qualifier_name"))).trim()
		 TD_rsp_qualifier_value = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_qualifier_value"))).trim()
		 */

		//cancel_supp_plan
		TD_cancel_supp_plan_imm = convertHSSFCellToString(row.getCell(excelHeader.get("cancel_supp_plan_imm"))).trim()
		TD_csp_supp_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("csp_supp_plan_no"))).trim()
		TD_csp_assignment_directive = convertHSSFCellToString(row.getCell(excelHeader.get("csp_assignment_directive"))).trim()
		TD_csp_comments = convertHSSFCellToString(row.getCell(excelHeader.get("csp_comments"))).trim()
		TD_csp_do_write = convertHSSFCellToString(row.getCell(excelHeader.get("csp_do_write"))).trim()
		TD_csp_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("csp_alt_proration_start_date"))).trim()
		TD_csp_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("csp_alt_proration_start_date"))).trim()
		TD_csp_effective_date = convertHSSFCellToString(row.getCell(excelHeader.get("csp_effective_date"))).trim()
		TD_csp_offset_interval = convertHSSFCellToString(row.getCell(excelHeader.get("csp_offset_interval"))).trim()
		TD_csp_invoice_unbilled_usage = convertHSSFCellToString(row.getCell(excelHeader.get("csp_invoice_unbilled_usage"))).trim()
		TD_csp_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("csp_alt_caller_id"))).trim()

		//update_acct_status
		TD_update_acct_status_imm = convertHSSFCellToString(row.getCell(excelHeader.get("update_acct_status_imm"))).trim()
		TD_uas_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uas_status_cd"))).trim()
		TD_uas_queue_days = convertHSSFCellToString(row.getCell(excelHeader.get("uas_queue_days"))).trim()
		TD_uas_queue_date = convertHSSFCellToString(row.getCell(excelHeader.get("uas_queue_date"))).trim()

		//assign_supp_plan
		TD_assign_supp_plan_imm= convertHSSFCellToString(row.getCell(excelHeader.get("assign_supp_plan_imm"))).trim()
		TD_asp_supp_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("asp_supp_plan_no"))).trim()
		TD_asp_num_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("asp_num_plan_units"))).trim()
		TD_asp_coupon_code = convertHSSFCellToString(row.getCell(excelHeader.get("asp_coupon_code"))).trim()
		TD_asp_alt_rate_schedule_no = convertHSSFCellToString(row.getCell(excelHeader.get("asp_alt_rate_schedule_no"))).trim()
		TD_asp_Do_write = convertHSSFCellToString(row.getCell(excelHeader.get("asp_Do_write"))).trim()
		TD_asp_comments = convertHSSFCellToString(row.getCell(excelHeader.get("asp_comments"))).trim()
		TD_asp_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("asp_client_receipt_id"))).trim()
		TD_asp_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("asp_contract_type_no"))).trim()
		TD_asp_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("asp_contract_alt_recur_fee"))).trim()
		TD_asp_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("asp_contract_length_months"))).trim()
		TD_asp_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("asp_contract_start_date"))).trim()

		//assign_supp_plan_multi
		TD_assign_supp_plan_multi_imm = convertHSSFCellToString(row.getCell(excelHeader.get("assign_supp_plan_multi_imm"))).trim()
		TD_aspm_do_write = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_do_write"))).trim()
		TD_aspm_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_alt_proration_start_date"))).trim()
		TD_aspm_coupon_codes = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_coupon_codes"))).trim()
		TD_aspm_supp_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_supp_plan_no"))).trim()
		TD_aspm_alt_rate_schedule_no = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_alt_rate_schedule_no"))).trim()
		TD_aspm_num_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_num_plan_units"))).trim()
		TD_aspm_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_type_no"))).trim()
		TD_aspm_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_alt_recur_fee"))).trim()
		TD_aspm_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_length_months"))).trim()
		TD_aspm_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_start_date"))).trim()
		TD_aspm_contract_end_date = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_end_date"))).trim()
		TD_aspm_effective_date = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_effective_date"))).trim()

		//update_master_plan
		TD_update_master_plan_imm = convertHSSFCellToString(row.getCell(excelHeader.get("update_master_plan_imm"))).trim()
		TD_ump_master_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_master_plan_no"))).trim()
		TD_ump_alt_rate_schedule_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_alt_rate_schedule_no"))).trim()
		TD_ump_num_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("ump_num_plan_units"))).trim()
		//TD_ump_assignment_directive = convertHSSFCellToString(row.getCell(excelHeader.get("ump_assignment_directive"))).trim()
		TD_ump_do_write = convertHSSFCellToString(row.getCell(excelHeader.get("ump_do_write"))).trim()
		TD_ump_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("ump_client_receipt_id"))).trim()
		TD_ump_force_currency_change = convertHSSFCellToString(row.getCell(excelHeader.get("ump_force_currency_change"))).trim()
		TD_ump_auto_cancel_supp_plans = convertHSSFCellToString(row.getCell(excelHeader.get("ump_auto_cancel_supp_plans"))).trim()
		TD_ump_offset_months = convertHSSFCellToString(row.getCell(excelHeader.get("ump_offset_months"))).trim()
		TD_ump_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("ump_alt_proration_start_date"))).trim()

		TD_ump_alt_client_acct_group_id = convertHSSFCellToString(row.getCell(excelHeader.get("ump_alt_client_acct_group_id"))).trim()
		//TD_ump_custom_rate_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_plan_no"))).trim()
		//TD_ump_custom_rate_service_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_service_no"))).trim()
		//TD_ump_custom_rate_seq_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_seq_no"))).trim()
		//TD_ump_custom_rate_from_unit = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_from_unit"))).trim()
		//TD_ump_custom_rate_to_unit = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_to_unit"))).trim()
		//TD_ump_custom_rate_per_unit = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_per_unit"))).trim()
		TD_ump_effective_date = convertHSSFCellToString(row.getCell(excelHeader.get("ump_effective_date"))).trim()
		TD_ump_offset_interval = convertHSSFCellToString(row.getCell(excelHeader.get("ump_offset_interval"))).trim()
		TD_ump_invoice_unbilled_usage = convertHSSFCellToString(row.getCell(excelHeader.get("ump_invoice_unbilled_usage"))).trim()

		TD_ump_coupon_code = convertHSSFCellToString(row.getCell(excelHeader.get("ump_coupon_code"))).trim()
		TD_ump_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("ump_alt_caller_id"))).trim()
		//TD_ump_qualifier_name = convertHSSFCellToString(row.getCell(excelHeader.get("ump_qualifier_name"))).trim()
		//TD_ump_qualifier_value = convertHSSFCellToString(row.getCell(excelHeader.get("ump_qualifier_value"))).trim()
		TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()


	}

	//Ram Integration3 Case
	public def testDataIntegration3(HSSFRow row)
	{
		String testd = null
		//CreateAccountComplete And AssignSuppPlan
		//testd = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No")))
		TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
		TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
		TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
		//TD_userid = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_AVT = convertHSSFCellToString(row.getCell(excelHeader.get("AVT"))).trim()
		TD_Advance_VirtualTime = convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).trim()
		TD_TestSet = convertHSSFCellToString(row.getCell(excelHeader.get("TestSet"))).trim()
		TD_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Status_Code"))).trim()
		TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
		TD_masterPlanUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Units"))).trim()
		TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
		//TD_suppPlan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan2"))).trim()
		//TD_suppPlan3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan3"))).trim()
		TD_suppPlanUnits1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units1"))).trim()
		//TD_suppPlanUnits2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units2"))).trim()
		//TD_suppPlanUnits3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units3"))).trim()
		TD_notifyMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Notify_Medhod"))).trim()
		TD_password = convertHSSFCellToString(row.getCell(excelHeader.get("Password"))).trim()
		TD_email = convertHSSFCellToString(row.getCell(excelHeader.get("Email"))).trim()
		TD_payMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Pay_Method"))).trim()
		TD_bank_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("bank_acct_no"))).trim()
		TD_bank_routing_number = convertHSSFCellToString(row.getCell(excelHeader.get("bank_routing_number"))).trim()
		TD_ccNumber = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Number"))).trim()
		TD_ccExpireMM = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_MM"))).trim()
		TD_ccExpireYYYY = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_YYYY"))).trim()
		TD_collection_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group1"))).trim()
		TD_collection_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group2"))).trim()


		TD_testAcctMethod = convertHSSFCellToString(row.getCell(excelHeader.get("test_acct_ind"))).trim()
		TD_doFullInvoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_full_invoicing"))).trim()
		TD_do_prorated_invoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_prorated_invoicing"))).trim()

		TD_couponCode1 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code1"))).trim()
		TD_couponCode2 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code2"))).trim()
		TD_couponCode3 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code3"))).trim()
		TD_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date"))).trim()
		TD_contract_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_plan_no"))).trim()
		TD_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no"))).trim()
		TD_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months"))).trim()

		TD_doWrite = convertHSSFCellToString(row.getCell(excelHeader.get("do_write"))).trim()
		TD_taxExemptionLevel = convertHSSFCellToString(row.getCell(excelHeader.get("Tax_Exemption_Level"))).trim()
		TD_retroActiveStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Retroactive_Start_Date"))).trim()
		TD_altStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Start_Date"))).trim()
		TD_altBillDay = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Billing_Day"))).trim()
		TD_assignmentdirective = convertHSSFCellToString(row.getCell(excelHeader.get("AD"))).trim()
		TD_currency_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Currency_cd"))).trim()

		TD_custom_rate_plan_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1"))).trim()
		TD_cutom_rate_service_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1"))).trim()
		TD_custom_rate_seq_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1"))).trim()
		TD_custom_rate_from_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1"))).trim()
		TD_custom_rate_to_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1"))).trim()
		TD_custom_rate_per_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1"))).trim()
		TD_custom_rate_plan_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2"))).trim()
		TD_cutom_rate_service_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2"))).trim()
		TD_custom_rate_seq_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2"))).trim()
		TD_custom_rate_from_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2"))).trim()
		TD_custom_rate_to_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2"))).trim()
		TD_custom_rate_per_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2"))).trim()

		TD_functional_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group1"))).trim()
		TD_functional_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group2"))).trim()
		TD_status_until_alt_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("status_until_alt_start_date"))).trim()
		TD_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee"))).trim()

		TD_firstName = convertHSSFCellToString(row.getCell(excelHeader.get("First_Name"))).trim()
		TD_mi = convertHSSFCellToString(row.getCell(excelHeader.get("MI"))).trim()
		TD_lastName = convertHSSFCellToString(row.getCell(excelHeader.get("Last_Name"))).trim()
		TD_companyName = convertHSSFCellToString(row.getCell(excelHeader.get("Company_Name"))).trim()
		TD_addr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr1"))).trim()
		TD_addr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr2"))).trim()
		TD_city = convertHSSFCellToString(row.getCell(excelHeader.get("City"))).trim()
		TD_state = convertHSSFCellToString(row.getCell(excelHeader.get("State"))).trim()
		TD_locality = convertHSSFCellToString(row.getCell(excelHeader.get("Locality"))).trim()
		TD_country = convertHSSFCellToString(row.getCell(excelHeader.get("Country"))).trim()
		TD_postalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Postal_Code"))).trim()
		TD_phone = convertHSSFCellToString(row.getCell(excelHeader.get("Phone"))).trim()

		TD_billFirstName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_First_Name"))).trim()
		TD_billMi = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_MI"))).trim()
		TD_billLastName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Last_Name"))).trim()
		TD_billCompanyName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Company_Name"))).trim()
		TD_billAddr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr1"))).trim()
		TD_billAddr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr2"))).trim()
		TD_billCity = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_City"))).trim()
		TD_billState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_State"))).trim()
		TD_billLocality = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Locality"))).trim()
		TD_billCountry = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Country"))).trim()
		TD_billPostalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Postal_Code"))).trim()
		TD_billPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone"))).trim()
		TD_billEmail = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Email"))).trim()
		TD_respLevelCd = convertHSSFCellToString(row.getCell(excelHeader.get("Resp_Level_Cd"))).trim()
		TD_qualifierName1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name1"))).trim()
		TD_qualifierName2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name2"))).trim()
		TD_qualifierValue1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value1"))).trim()
		TD_qualifierValue2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value2"))).trim()

		TD_clientAcctId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Acct_Id"))).trim()
		TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Cd"))).trim()
		TD_secretQuestion = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question"))).trim()
		TD_secretQuestionAanswer = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question_Answer"))).trim()
		TD_phoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Phone_Ext"))).trim()
		TD_cellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Cell_Phone"))).trim()
		TD_workPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone"))).trim()
		TD_workPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone_Ext"))).trim()
		TD_birthDate = convertHSSFCellToString(row.getCell(excelHeader.get("Birthdate"))).trim()
		TD_billPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone_Ext"))).trim()
		TD_billCellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Cell_Phone"))).trim()
		TD_billWorkPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone"))).trim()
		TD_billWorkPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone_Ext"))).trim()
		TD_balanceForward = convertHSSFCellToString(row.getCell(excelHeader.get("Balance_Forward"))).trim()
		TD_masterPlanAltRateSchedNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Alt_Rate_Sched_No"))).trim()
		TD_clientReceiptId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
		TD_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("Cvv"))).trim()
		/*TD_taxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Taxpayer_Id"))).trim()
		 TD_billAgreementId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Agreement_Id"))).trim()
		 TD_altMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Msg_Template_No"))).trim()
		 TD_seqFuncGroupNo = convertHSSFCellToString(row.getCell(excelHeader.get("Seq_Func_Group_No"))).trim()
		 TD_bankAcctType = convertHSSFCellToString(row.getCell(excelHeader.get("Bank_Acct_Type"))).trim()
		 TD_billDriversLicenseNo = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_No"))).trim()
		 TD_billDriversLicenseState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_State"))).trim()
		 TD_billTaxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Taxpayer_Id"))).trim()
		 TD_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("Address3"))).trim()
		 TD_billAddress3 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Address3"))).trim()
		 TD_trackData1 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data1"))).trim()
		 TD_trackData2 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data2"))).trim()
		 TD_cnAltMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Cn_Alt_Msg_Template_No"))).trim()
		 TD_altCallerId = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()
		 */

		//assign_supp_plam

		/*TD_assign_supp_plan = convertHSSFCellToString(row.getCell(excelHeader.get("Assign_Supp_Plan"))).trim()
		 TD_Advance_VirtualTime = convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).trim()
		 TD_VerifyDate = convertHSSFCellToString(row.getCell(excelHeader.get("Verify_Date"))).trim()
		 TD_altrateschedulenosp = convertHSSFCellToString(row.getCell(excelHeader.get("alt_rate_schedule_no_SP"))).trim()
		 TD_altclientacctgroupid = convertHSSFCellToString(row.getCell(excelHeader.get("alt_client_acct_group_id"))).trim()
		 TD_doWriteSP = convertHSSFCellToString(row.getCell(excelHeader.get("Do_write_SP"))).trim()
		 TD_num_plan_units_sp = convertHSSFCellToString(row.getCell(excelHeader.get("num_plan_units_sp"))).trim()
		 TD_coupon_code_sp = convertHSSFCellToString(row.getCell(excelHeader.get("coupon_code_sp"))).trim()
		 TD_comments_sp = convertHSSFCellToString(row.getCell(excelHeader.get("comments_sp"))).trim()
		 TD_client_receipt_id_sp = convertHSSFCellToString(row.getCell(excelHeader.get("client_receipt_id_sp"))).trim()
		 TD_contract_type_no_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no_sp"))).trim()
		 TD_contract_alt_recur_fee_no_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee_no_sp"))).trim()
		 TD_contract_length_months_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months_sp"))).trim()
		 TD_contract_cancel_fee_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_cancel_fee_sp"))).trim()
		 TD_contract_comments_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_comments_sp"))).trim()
		 TD_contract_start_date_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date_sp"))).trim()
		 TD_offset_months_sp = convertHSSFCellToString(row.getCell(excelHeader.get("offset_months_sp"))).trim()
		 TD_auto_offset_months_options = convertHSSFCellToString(row.getCell(excelHeader.get("auto_offset_months_options_sp"))).trim()
		 TD_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("alt_proration_start_date_sp"))).trim()
		 */
		TD_effectiveDate = convertHSSFCellToString(row.getCell(excelHeader.get("Effective_Date"))).trim()
		//TD_assign_sp_imm = convertHSSFCellToString(row.getCell(excelHeader.get("assign_sp_imm"))).trim()


		//update_acct_complete

		TD_update_acct_complete_imm = convertHSSFCellToString(row.getCell(excelHeader.get("update_acct_complete_imm"))).trim()
		TD_uac_first_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_first_name"))).trim()
		TD_uac_last_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_last_name"))).trim()
		TD_uac_middle_initial = convertHSSFCellToString(row.getCell(excelHeader.get("uac_middle_initial"))).trim()
		TD_uac_company_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_company_name"))).trim()
		TD_uac_address1 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_address1"))).trim()
		TD_uac_address2 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_address2"))).trim()
		TD_uac_city = convertHSSFCellToString(row.getCell(excelHeader.get("uac_city"))).trim()
		TD_uac_locality = convertHSSFCellToString(row.getCell(excelHeader.get("uac_locality"))).trim()
		TD_uac_state_prov = convertHSSFCellToString(row.getCell(excelHeader.get("uac_state_prov"))).trim()

		TD_uac_country = convertHSSFCellToString(row.getCell(excelHeader.get("uac_country"))).trim()
		TD_uac_postal_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_postal_cd"))).trim()
		TD_uac_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_phone"))).trim()
		TD_uac_phone_ext = convertHSSFCellToString(row.getCell(excelHeader.get("uac_phone_ext"))).trim()
		TD_uac_cell_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cell_phone"))).trim()
		TD_uac_work_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_work_phone"))).trim()
		TD_uac_work_phone_ext = convertHSSFCellToString(row.getCell(excelHeader.get("uac_work_phone_ext"))).trim()
		TD_uac_email = convertHSSFCellToString(row.getCell(excelHeader.get("uac_email"))).trim()
		TD_uac_birthdate = convertHSSFCellToString(row.getCell(excelHeader.get("uac_birthdate"))).trim()
		TD_uac_bill_first_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_first_name"))).trim()

		TD_uac_bill_last_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_last_name"))).trim()
		TD_uac_bill_middle_initial = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_middle_initial"))).trim()
		TD_uac_bill_company_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_company_name"))).trim()
		TD_uac_bill_address1 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_address1"))).trim()
		TD_uac_bill_address2 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_address2"))).trim()
		TD_uac_bill_city = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_city"))).trim()
		TD_uac_bill_locality = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_locality"))).trim()
		TD_uac_bill_state_prov = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_state_prov"))).trim()
		TD_uac_bill_country = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_country"))).trim()
		TD_uac_bill_postal_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_postal_cd"))).trim()

		TD_uac_bill_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_phone"))).trim()
		TD_uac_bill_phone_ext = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_phone_ext"))).trim()
		TD_uac_bill_cell_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_cell_phone"))).trim()
		TD_uac_bill_work_phone = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_work_phone"))).trim()
		TD_uac_bill_work_phone_ext = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_work_phone_ext"))).trim()
		TD_uac_bill_email = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_email"))).trim()
		TD_uac_pay_method = convertHSSFCellToString(row.getCell(excelHeader.get("uac_pay_method"))).trim()
		TD_uac_cc_number = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cc_number"))).trim()
		TD_uac_cc_expire_mm = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cc_expire_mm"))).trim()
		TD_uac_cc_expire_yyyy = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cc_expire_yyyy"))).trim()

		TD_uac_bank_routing_num = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bank_routing_num"))).trim()
		TD_uac_bank_acct_num = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bank_acct_num"))).trim()
		TD_uac_master_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_plan_no"))).trim()
		TD_uac_master_plan_alt_rate_sched_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_plan_alt_rate_sched_no"))).trim()
		TD_uac_master_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_plan_units"))).trim()
		//TD_uac_master_plan_assign_directive = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_plan_assign_directive"))).trim()

		/*TD_uac_functional_acct_groups = convertHSSFCellToString(row.getCell(excelHeader.get("uac_functional_acct_groups"))).trim()
		 TD_uac_functional_group_directives = convertHSSFCellToString(row.getCell(excelHeader.get("uac_functional_group_directives"))).trim()
		 TD_uac_client_func_acct_group_ids = convertHSSFCellToString(row.getCell(excelHeader.get("uac_client_func_acct_group_ids"))).trim()
		 TD_uac_collections_acct_groups = convertHSSFCellToString(row.getCell(excelHeader.get("uac_collections_acct_groups"))).trim()
		 TD_uac_collections_group_directives = convertHSSFCellToString(row.getCell(excelHeader.get("uac_collections_group_directives"))).trim()
		 TD_uac_client_coll_acct_group_ids = convertHSSFCellToString(row.getCell(excelHeader.get("uac_client_coll_acct_group_ids"))).trim()
		 TD_uac_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_status_cd"))).trim()
		 TD_uac_notify_method = convertHSSFCellToString(row.getCell(excelHeader.get("uac_notify_method"))).trim()
		 TD_uac_password = convertHSSFCellToString(row.getCell(excelHeader.get("uac_password"))).trim()
		 TD_uac_secret_question = convertHSSFCellToString(row.getCell(excelHeader.get("uac_secret_question"))).trim()
		 TD_uac_secret_question_answer = convertHSSFCellToString(row.getCell(excelHeader.get("uac_secret_question_answer"))).trim()
		 TD_uac_pin = convertHSSFCellToString(row.getCell(excelHeader.get("uac_pin"))).trim()
		 TD_uac_test_acct_ind = convertHSSFCellToString(row.getCell(excelHeader.get("uac_test_acct_ind"))).trim()
		 TD_uac_resp_level_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_resp_level_cd"))).trim()
		 TD_uac_senior_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_senior_acct_no"))).trim()
		 TD_uac_client_acct_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_client_acct_id"))).trim()
		 TD_uac_do_collect = convertHSSFCellToString(row.getCell(excelHeader.get("uac_do_collect"))).trim()
		 TD_uac_change_status_after_coll = convertHSSFCellToString(row.getCell(excelHeader.get("uac_change_status_after_coll"))).trim()
		 TD_uac_reset_dates_after_status = convertHSSFCellToString(row.getCell(excelHeader.get("uac_reset_dates_after_status"))).trim()
		 TD_uac_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_client_receipt_id"))).trim()
		 TD_uac_alt_do_dunning = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_do_dunning"))).trim()
		 TD_uac_force_currency_change = convertHSSFCellToString(row.getCell(excelHeader.get("uac_force_currency_change"))).trim()
		 TD_uac_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cvv"))).trim()
		 TD_uac_taxpayer_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_taxpayer_id"))).trim()
		 TD_uac_bill_agreement_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_agreement_id"))).trim()
		 TD_uac_auto_cancel_supp_plans = convertHSSFCellToString(row.getCell(excelHeader.get("uac_auto_cancel_supp_plans"))).trim()
		 TD_uac_offset_months = convertHSSFCellToString(row.getCell(excelHeader.get("uac_offset_months"))).trim()
		 TD_uac_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_proration_start_date"))).trim()
		 TD_uac_alt_msg_template_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_msg_template_no"))).trim()
		 TD_uac_seq_func_group_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_seq_func_group_no"))).trim()
		 TD_uac_bank_acct_type = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bank_acct_type"))).trim()
		 TD_uac_bill_drivers_license_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_drivers_license_no"))).trim()
		 TD_uac_bill_drivers_license_state = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_drivers_license_state"))).trim()
		 TD_uac_bill_taxpayer_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_taxpayer_id"))).trim()
		 TD_uac_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_address3"))).trim()
		 TD_uac_bill_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_bill_address3"))).trim()
		 TD_uac_usage_accumulation_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_usage_accumulation_plan_no"))).trim()
		 TD_uac_usage_accumulation_reset_months = convertHSSFCellToString(row.getCell(excelHeader.get("uac_usage_accumulation_reset_months"))).trim()
		 TD_uac_enable_usage_pooling_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_enable_usage_pooling_plan_no"))).trim()
		 TD_uac_disable_usage_pooling_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_disable_usage_pooling_plan_no"))).trim()
		 TD_uac_alt_client_acct_group_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_client_acct_group_id"))).trim()
		 TD_uac_track_data1 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_track_data1"))).trim()
		 TD_uac_track_data2 = convertHSSFCellToString(row.getCell(excelHeader.get("uac_track_data2"))).trim()
		 TD_uac_offset_interval = convertHSSFCellToString(row.getCell(excelHeader.get("uac_offset_interval"))).trim()
		 TD_uac_tax_exemption_level = convertHSSFCellToString(row.getCell(excelHeader.get("uac_tax_exemption_level"))).trim()
		 TD_uac_cn_alt_msg_template_no = convertHSSFCellToString(row.getCell(excelHeader.get("uac_cn_alt_msg_template_no"))).trim()
		 TD_uac_promo_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_promo_cd"))).trim()
		 TD_uac_invoice_unbilled_usage = convertHSSFCellToString(row.getCell(excelHeader.get("uac_invoice_unbilled_usage"))).trim()
		 */
		TD_uac_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uac_status_cd"))).trim()
		TD_uac_coupon_code = convertHSSFCellToString(row.getCell(excelHeader.get("uac_coupon_code"))).trim()
		/*TD_uac_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("uac_alt_caller_id"))).trim()
		 TD_uac_qualifier_name = convertHSSFCellToString(row.getCell(excelHeader.get("uac_qualifier_name"))).trim()
		 TD_uac_qualifier_value = convertHSSFCellToString(row.getCell(excelHeader.get("uac_qualifier_value"))).trim()
		 */

		//modify_supp_plan

		TD_modify_supp_plan_imm = convertHSSFCellToString(row.getCell(excelHeader.get("modify_supp_plan_imm"))).trim()
		TD_msp_supp_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("msp_supp_plan_no"))).trim()
		TD_msp_alt_rate_schedule_no = convertHSSFCellToString(row.getCell(excelHeader.get("msp_alt_rate_schedule_no"))).trim()
		TD_msp_num_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("msp_num_plan_units"))).trim()
		TD_msp_coupon_code = convertHSSFCellToString(row.getCell(excelHeader.get("msp_coupon_code"))).trim()
		//TD_msp_assignment_directive = convertHSSFCellToString(row.getCell(excelHeader.get("msp_assignment_directive"))).trim()
		TD_msp_comments = convertHSSFCellToString(row.getCell(excelHeader.get("msp_comments"))).trim()
		TD_msp_do_write = convertHSSFCellToString(row.getCell(excelHeader.get("msp_do_write"))).trim()
		TD_msp_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("msp_client_receipt_id"))).trim()

		TD_msp_custom_rate_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("msp_custom_rate_plan_no"))).trim()
		TD_msp_custom_rate_service_no = convertHSSFCellToString(row.getCell(excelHeader.get("msp_custom_rate_service_no"))).trim()
		TD_msp_custom_rate_seq_no = convertHSSFCellToString(row.getCell(excelHeader.get("msp_custom_rate_seq_no"))).trim()
		TD_msp_custom_rate_from_unit = convertHSSFCellToString(row.getCell(excelHeader.get("msp_custom_rate_from_unit"))).trim()
		TD_msp_custom_rate_to_unit = convertHSSFCellToString(row.getCell(excelHeader.get("msp_custom_rate_to_unit"))).trim()
		TD_msp_custom_rate_per_unit = convertHSSFCellToString(row.getCell(excelHeader.get("msp_custom_rate_per_unit"))).trim()

		TD_msp_effective_date = convertHSSFCellToString(row.getCell(excelHeader.get("msp_effective_date"))).trim()
		TD_msp_offset_interval = convertHSSFCellToString(row.getCell(excelHeader.get("msp_offset_interval"))).trim()
		TD_msp_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("msp_alt_caller_id"))).trim()

		/*TD_msp_qualifier_name = convertHSSFCellToString(row.getCell(excelHeader.get("msp_qualifier_name"))).trim()
		 TD_msp_qualifier_value = convertHSSFCellToString(row.getCell(excelHeader.get("msp_qualifier_value"))).trim()
		 */

		//replace_supp_plan
		TD_replace_supp_plan_imm = convertHSSFCellToString(row.getCell(excelHeader.get("replace_supp_plan_imm"))).trim()

		TD_rsp_existing_supp_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_existing_supp_plan_no"))).trim()
		TD_rsp_new_supp_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_new_supp_plan_no"))).trim()
		TD_rsp_alt_rate_schedule_no = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_alt_rate_schedule_no"))).trim()
		TD_rsp_num_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_num_plan_units"))).trim()
		TD_rsp_coupon_code = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_coupon_code"))).trim()
		//TD_rsp_assignment_directive = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_assignment_directive"))).trim()
		TD_rsp_comments = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_comments"))).trim()
		TD_rsp_do_write = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_do_write"))).trim()
		TD_rsp_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_client_receipt_id"))).trim()
		TD_rsp_offset_months = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_offset_months"))).trim()

		TD_rsp_auto_offset_months_option = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_auto_offset_months_option"))).trim()
		TD_rsp_alt_client_acct_group_id = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_alt_client_acct_group_id"))).trim()
		TD_rsp_custom_rate_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_custom_rate_plan_no"))).trim()
		TD_rsp_custom_rate_service_no = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_custom_rate_service_no"))).trim()
		TD_rsp_custom_rate_seq_no = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_custom_rate_seq_no"))).trim()
		TD_rsp_custom_rate_from_unit = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_custom_rate_from_unit"))).trim()
		TD_rsp_custom_rate_to_unit = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_custom_rate_to_unit"))).trim()
		TD_rsp_custom_rate_per_unit = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_custom_rate_per_unit"))).trim()
		TD_rsp_effective_date = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_effective_date"))).trim()
		TD_rsp_offset_interval = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_offset_interval"))).trim()

		TD_rsp_invoice_unbilled_usage = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_invoice_unbilled_usage"))).trim()
		TD_rsp_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_alt_caller_id"))).trim()

		/*TD_rsp_qualifier_name = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_qualifier_name"))).trim()
		 TD_rsp_qualifier_value = convertHSSFCellToString(row.getCell(excelHeader.get("rsp_qualifier_value"))).trim()
		 */

		//cancel_supp_plan
		TD_cancel_supp_plan_imm = convertHSSFCellToString(row.getCell(excelHeader.get("cancel_supp_plan_imm"))).trim()
		TD_csp_supp_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("csp_supp_plan_no"))).trim()
		TD_csp_assignment_directive = convertHSSFCellToString(row.getCell(excelHeader.get("csp_assignment_directive"))).trim()
		TD_csp_comments = convertHSSFCellToString(row.getCell(excelHeader.get("csp_comments"))).trim()
		TD_csp_do_write = convertHSSFCellToString(row.getCell(excelHeader.get("csp_do_write"))).trim()
		TD_csp_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("csp_alt_proration_start_date"))).trim()
		TD_csp_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("csp_alt_proration_start_date"))).trim()
		TD_csp_effective_date = convertHSSFCellToString(row.getCell(excelHeader.get("csp_effective_date"))).trim()
		TD_csp_offset_interval = convertHSSFCellToString(row.getCell(excelHeader.get("csp_offset_interval"))).trim()
		TD_csp_invoice_unbilled_usage = convertHSSFCellToString(row.getCell(excelHeader.get("csp_invoice_unbilled_usage"))).trim()
		TD_csp_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("csp_alt_caller_id"))).trim()

		//update_acct_status
		TD_update_acct_status_imm = convertHSSFCellToString(row.getCell(excelHeader.get("update_acct_status_imm"))).trim()
		TD_uas_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uas_status_cd"))).trim()
		TD_uas_queue_days = convertHSSFCellToString(row.getCell(excelHeader.get("uas_queue_days"))).trim()
		TD_uas_queue_date = convertHSSFCellToString(row.getCell(excelHeader.get("uas_queue_date"))).trim()

		//assign_supp_plan
		TD_assign_supp_plan_imm= convertHSSFCellToString(row.getCell(excelHeader.get("assign_supp_plan_imm"))).trim()
		TD_asp_supp_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("asp_supp_plan_no"))).trim()
		TD_asp_num_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("asp_num_plan_units"))).trim()
		TD_asp_coupon_code = convertHSSFCellToString(row.getCell(excelHeader.get("asp_coupon_code"))).trim()
		TD_asp_alt_rate_schedule_no = convertHSSFCellToString(row.getCell(excelHeader.get("asp_alt_rate_schedule_no"))).trim()
		TD_asp_Do_write = convertHSSFCellToString(row.getCell(excelHeader.get("asp_Do_write"))).trim()
		TD_asp_comments = convertHSSFCellToString(row.getCell(excelHeader.get("asp_comments"))).trim()
		TD_asp_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("asp_client_receipt_id"))).trim()
		TD_asp_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("asp_contract_type_no"))).trim()
		TD_asp_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("asp_contract_alt_recur_fee"))).trim()
		TD_asp_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("asp_contract_length_months"))).trim()
		TD_asp_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("asp_contract_start_date"))).trim()

		//assign_supp_plan_multi
		TD_assign_supp_plan_multi_imm = convertHSSFCellToString(row.getCell(excelHeader.get("assign_supp_plan_multi_imm"))).trim()
		TD_aspm_do_write = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_do_write"))).trim()
		TD_aspm_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_alt_proration_start_date"))).trim()
		TD_aspm_coupon_codes = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_coupon_codes"))).trim()
		TD_aspm_supp_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_supp_plan_no"))).trim()
		TD_aspm_alt_rate_schedule_no = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_alt_rate_schedule_no"))).trim()
		TD_aspm_num_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_num_plan_units"))).trim()
		TD_aspm_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_type_no"))).trim()
		TD_aspm_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_alt_recur_fee"))).trim()
		TD_aspm_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_length_months"))).trim()
		TD_aspm_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_start_date"))).trim()
		TD_aspm_contract_end_date = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_end_date"))).trim()
		TD_aspm_effective_date = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_effective_date"))).trim()

		//update_master_plan
		TD_update_master_plan_imm = convertHSSFCellToString(row.getCell(excelHeader.get("update_master_plan_imm"))).trim()
		TD_ump_master_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_master_plan_no"))).trim()
		TD_ump_alt_rate_schedule_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_alt_rate_schedule_no"))).trim()
		TD_ump_num_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("ump_num_plan_units"))).trim()
		//TD_ump_assignment_directive = convertHSSFCellToString(row.getCell(excelHeader.get("ump_assignment_directive"))).trim()
		TD_ump_do_write = convertHSSFCellToString(row.getCell(excelHeader.get("ump_do_write"))).trim()
		TD_ump_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("ump_client_receipt_id"))).trim()
		TD_ump_force_currency_change = convertHSSFCellToString(row.getCell(excelHeader.get("ump_force_currency_change"))).trim()
		TD_ump_auto_cancel_supp_plans = convertHSSFCellToString(row.getCell(excelHeader.get("ump_auto_cancel_supp_plans"))).trim()
		TD_ump_offset_months = convertHSSFCellToString(row.getCell(excelHeader.get("ump_offset_months"))).trim()
		TD_ump_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("ump_alt_proration_start_date"))).trim()

		TD_ump_alt_client_acct_group_id = convertHSSFCellToString(row.getCell(excelHeader.get("ump_alt_client_acct_group_id"))).trim()
		//TD_ump_custom_rate_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_plan_no"))).trim()
		//TD_ump_custom_rate_service_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_service_no"))).trim()
		//TD_ump_custom_rate_seq_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_seq_no"))).trim()
		//TD_ump_custom_rate_from_unit = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_from_unit"))).trim()
		//TD_ump_custom_rate_to_unit = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_to_unit"))).trim()
		//TD_ump_custom_rate_per_unit = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_per_unit"))).trim()
		TD_ump_effective_date = convertHSSFCellToString(row.getCell(excelHeader.get("ump_effective_date"))).trim()
		TD_ump_offset_interval = convertHSSFCellToString(row.getCell(excelHeader.get("ump_offset_interval"))).trim()
		TD_ump_invoice_unbilled_usage = convertHSSFCellToString(row.getCell(excelHeader.get("ump_invoice_unbilled_usage"))).trim()

		TD_ump_coupon_code = convertHSSFCellToString(row.getCell(excelHeader.get("ump_coupon_code"))).trim()
		TD_ump_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("ump_alt_caller_id"))).trim()
		//TD_ump_qualifier_name = convertHSSFCellToString(row.getCell(excelHeader.get("ump_qualifier_name"))).trim()
		//TD_ump_qualifier_value = convertHSSFCellToString(row.getCell(excelHeader.get("ump_qualifier_value"))).trim()
		TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()
	}

	//Ram Integration Case
	public def testDataIntegration(HSSFRow row)
	{
		String testd = null
		//CreateAccountComplete And AssignSuppPlan
		//testd = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No")))
		TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
		TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
		TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
		TD_AVT = convertHSSFCellToString(row.getCell(excelHeader.get("AVT"))).trim()
		TD_TestSet = convertHSSFCellToString(row.getCell(excelHeader.get("TestSet"))).trim()
		//TD_userid = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Status_Code"))).trim()
		TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
		TD_masterPlanUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Units"))).trim()
		TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
		TD_suppPlan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan2"))).trim()
		TD_suppPlan3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan3"))).trim()
		TD_suppPlanUnits1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units1"))).trim()
		TD_suppPlanUnits2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units2"))).trim()
		TD_suppPlanUnits3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units3"))).trim()
		TD_notifyMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Notify_Medhod"))).trim()
		TD_password = convertHSSFCellToString(row.getCell(excelHeader.get("Password"))).trim()
		TD_email = convertHSSFCellToString(row.getCell(excelHeader.get("Email"))).trim()
		TD_payMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Pay_Method"))).trim()
		TD_bank_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("bank_acct_no"))).trim()
		TD_bank_routing_number = convertHSSFCellToString(row.getCell(excelHeader.get("bank_routing_number"))).trim()
		TD_ccNumber = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Number"))).trim()
		TD_ccExpireMM = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_MM"))).trim()
		TD_ccExpireYYYY = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_YYYY"))).trim()
		TD_collection_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group1"))).trim()
		TD_collection_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group2"))).trim()


		TD_testAcctMethod = convertHSSFCellToString(row.getCell(excelHeader.get("test_acct_ind"))).trim()
		TD_doFullInvoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_full_invoicing"))).trim()
		TD_do_prorated_invoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_prorated_invoicing"))).trim()

		TD_couponCode1 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code1"))).trim()
		TD_couponCode2 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code2"))).trim()
		TD_couponCode3 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code3"))).trim()
		TD_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date"))).trim()
		TD_contract_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_plan_no"))).trim()
		TD_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no"))).trim()
		TD_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months"))).trim()

		TD_doWrite = convertHSSFCellToString(row.getCell(excelHeader.get("do_write"))).trim()
		TD_taxExemptionLevel = convertHSSFCellToString(row.getCell(excelHeader.get("Tax_Exemption_Level"))).trim()
		TD_retroActiveStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Retroactive_Start_Date"))).trim()
		TD_altStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Start_Date"))).trim()
		TD_altBillDay = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Billing_Day"))).trim()
		TD_assignmentdirective = convertHSSFCellToString(row.getCell(excelHeader.get("AD"))).trim()
		TD_currency_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Currency_cd"))).trim()

		TD_custom_rate_plan_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1"))).trim()
		TD_cutom_rate_service_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1"))).trim()
		TD_custom_rate_seq_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1"))).trim()
		TD_custom_rate_from_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1"))).trim()
		TD_custom_rate_to_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1"))).trim()
		TD_custom_rate_per_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1"))).trim()
		TD_custom_rate_plan_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2"))).trim()
		TD_cutom_rate_service_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2"))).trim()
		TD_custom_rate_seq_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2"))).trim()
		TD_custom_rate_from_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2"))).trim()
		TD_custom_rate_to_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2"))).trim()
		TD_custom_rate_per_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2"))).trim()

		TD_functional_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group1"))).trim()
		TD_functional_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group2"))).trim()
		TD_status_until_alt_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("status_until_alt_start_date"))).trim()
		TD_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee"))).trim()

		TD_firstName = convertHSSFCellToString(row.getCell(excelHeader.get("First_Name"))).trim()
		TD_mi = convertHSSFCellToString(row.getCell(excelHeader.get("MI"))).trim()
		TD_lastName = convertHSSFCellToString(row.getCell(excelHeader.get("Last_Name"))).trim()
		TD_companyName = convertHSSFCellToString(row.getCell(excelHeader.get("Company_Name"))).trim()
		TD_addr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr1"))).trim()
		TD_addr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr2"))).trim()
		TD_city = convertHSSFCellToString(row.getCell(excelHeader.get("City"))).trim()
		TD_state = convertHSSFCellToString(row.getCell(excelHeader.get("State"))).trim()
		TD_locality = convertHSSFCellToString(row.getCell(excelHeader.get("Locality"))).trim()
		TD_country = convertHSSFCellToString(row.getCell(excelHeader.get("Country"))).trim()
		TD_postalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Postal_Code"))).trim()
		TD_phone = convertHSSFCellToString(row.getCell(excelHeader.get("Phone"))).trim()

		TD_billFirstName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_First_Name"))).trim()
		TD_billMi = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_MI"))).trim()
		TD_billLastName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Last_Name"))).trim()
		TD_billCompanyName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Company_Name"))).trim()
		TD_billAddr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr1"))).trim()
		TD_billAddr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr2"))).trim()
		TD_billCity = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_City"))).trim()
		TD_billState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_State"))).trim()
		TD_billLocality = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Locality"))).trim()
		TD_billCountry = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Country"))).trim()
		TD_billPostalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Postal_Code"))).trim()
		TD_billPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone"))).trim()
		TD_billEmail = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Email"))).trim()
		TD_respLevelCd = convertHSSFCellToString(row.getCell(excelHeader.get("Resp_Level_Cd"))).trim()
		TD_qualifierName1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name1"))).trim()
		TD_qualifierName2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name2"))).trim()
		TD_qualifierValue1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value1"))).trim()
		TD_qualifierValue2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value2"))).trim()

		TD_clientAcctId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Acct_Id"))).trim()
		TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Cd"))).trim()
		TD_secretQuestion = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question"))).trim()
		TD_secretQuestionAanswer = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question_Answer"))).trim()
		TD_phoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Phone_Ext"))).trim()
		TD_cellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Cell_Phone"))).trim()
		TD_workPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone"))).trim()
		TD_workPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone_Ext"))).trim()
		TD_birthDate = convertHSSFCellToString(row.getCell(excelHeader.get("Birthdate"))).trim()
		TD_billPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone_Ext"))).trim()
		TD_billCellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Cell_Phone"))).trim()
		TD_billWorkPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone"))).trim()
		TD_billWorkPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone_Ext"))).trim()
		TD_balanceForward = convertHSSFCellToString(row.getCell(excelHeader.get("Balance_Forward"))).trim()
		TD_masterPlanAltRateSchedNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Alt_Rate_Sched_No"))).trim()
		TD_clientReceiptId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
		TD_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("Cvv"))).trim()
		TD_taxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Taxpayer_Id"))).trim()
		TD_billAgreementId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Agreement_Id"))).trim()
		TD_altMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Msg_Template_No"))).trim()
		TD_seqFuncGroupNo = convertHSSFCellToString(row.getCell(excelHeader.get("Seq_Func_Group_No"))).trim()
		TD_bankAcctType = convertHSSFCellToString(row.getCell(excelHeader.get("Bank_Acct_Type"))).trim()
		TD_billDriversLicenseNo = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_No"))).trim()
		TD_billDriversLicenseState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_State"))).trim()
		TD_billTaxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Taxpayer_Id"))).trim()
		TD_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("Address3"))).trim()
		TD_billAddress3 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Address3"))).trim()
		TD_trackData1 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data1"))).trim()
		TD_trackData2 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data2"))).trim()
		TD_cnAltMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Cn_Alt_Msg_Template_No"))).trim()
		TD_altCallerId = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()
		TD_assign_supp_plan = convertHSSFCellToString(row.getCell(excelHeader.get("Assign_Supp_Plan"))).trim()
		TD_Advance_VirtualTime = convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).trim()
		TD_VerifyDate = convertHSSFCellToString(row.getCell(excelHeader.get("Verify_Date"))).trim()

		//assign_supp_plan

		TD_altrateschedulenosp = convertHSSFCellToString(row.getCell(excelHeader.get("alt_rate_schedule_no_SP"))).trim()
		TD_altclientacctgroupid = convertHSSFCellToString(row.getCell(excelHeader.get("alt_client_acct_group_id"))).trim()
		TD_doWriteSP = convertHSSFCellToString(row.getCell(excelHeader.get("Do_write_SP"))).trim()
		TD_num_plan_units_sp = convertHSSFCellToString(row.getCell(excelHeader.get("num_plan_units_sp"))).trim()
		TD_coupon_code_sp = convertHSSFCellToString(row.getCell(excelHeader.get("coupon_code_sp"))).trim()
		TD_comments_sp = convertHSSFCellToString(row.getCell(excelHeader.get("comments_sp"))).trim()
		TD_client_receipt_id_sp = convertHSSFCellToString(row.getCell(excelHeader.get("client_receipt_id_sp"))).trim()
		TD_contract_type_no_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no_sp"))).trim()
		TD_contract_alt_recur_fee_no_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee_no_sp"))).trim()
		TD_contract_length_months_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months_sp"))).trim()
		TD_contract_cancel_fee_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_cancel_fee_sp"))).trim()
		TD_contract_comments_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_comments_sp"))).trim()
		TD_contract_start_date_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date_sp"))).trim()
		TD_offset_months_sp = convertHSSFCellToString(row.getCell(excelHeader.get("offset_months_sp"))).trim()
		TD_auto_offset_months_options = convertHSSFCellToString(row.getCell(excelHeader.get("auto_offset_months_options_sp"))).trim()
		TD_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("alt_proration_start_date_sp"))).trim()
		TD_effectiveDate = convertHSSFCellToString(row.getCell(excelHeader.get("Effective_Date"))).trim()
		TD_assign_sp_imm = convertHSSFCellToString(row.getCell(excelHeader.get("assign_sp_imm"))).trim()

		//Ram update_acct_status
		TD_uas_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("uas_status_cd"))).trim()
		TD_uas_queue_days = convertHSSFCellToString(row.getCell(excelHeader.get("uas_queue_days"))).trim()
		TD_uas_queue_date = convertHSSFCellToString(row.getCell(excelHeader.get("uas_queue_date"))).trim()
		TD_uas_force_bill_date_reset = convertHSSFCellToString(row.getCell(excelHeader.get("uas_force_bill_date_reset"))).trim()
		TD_uas_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("uas_client_receipt_id"))).trim()
		TD_uas_comments = convertHSSFCellToString(row.getCell(excelHeader.get("uas_comments"))).trim()
		TD_uas_alt_do_dunning = convertHSSFCellToString(row.getCell(excelHeader.get("uas_alt_do_dunning"))).trim()
		TD_uas_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("uas_alt_caller_id"))).trim()
		TD_uas_qualifier_name = convertHSSFCellToString(row.getCell(excelHeader.get("uas_qualifier_name"))).trim()
		TD_uas_qualifier_value = convertHSSFCellToString(row.getCell(excelHeader.get("uas_qualifier_value"))).trim()
		TD_update_acct_status_imm = convertHSSFCellToString(row.getCell(excelHeader.get("update_acct_status_imm"))).trim()

		//Ram assign_supp_plan_multi

		TD_assign_supp_plan_multi_imm = convertHSSFCellToString(row.getCell(excelHeader.get("assign_supp_plan_multi_imm"))).trim()

		TD_aspm_assignment_directive = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_assignment_directive"))).trim()
		TD_aspm_do_write = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_do_write"))).trim()
		TD_aspm_comments = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_comments"))).trim()
		TD_aspm_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_client_receipt_id"))).trim()
		TD_aspm_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_alt_proration_start_date"))).trim()
		TD_aspm_coupon_codes = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_coupon_codes"))).trim()
		TD_aspm_supp_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_supp_plan_no"))).trim()
		TD_aspm_alt_rate_schedule_no = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_alt_rate_schedule_no"))).trim()
		TD_aspm_num_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_num_plan_units"))).trim()
		TD_aspm_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_type_no"))).trim()

		TD_aspm_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_alt_recur_fee"))).trim()
		TD_aspm_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_length_months"))).trim()
		TD_aspm_contract_cancel_fee = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_cancel_fee"))).trim()
		TD_aspm_contract_comments = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_comments"))).trim()
		TD_aspm_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_start_date"))).trim()
		TD_aspm_offset_months = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_offset_months"))).trim()
		TD_aspm_auto_offset_months_option = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_auto_offset_months_option"))).trim()
		TD_aspm_offset_interval = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_offset_interval"))).trim()
		TD_aspm_contract_end_date = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_contract_end_date"))).trim()
		TD_aspm_effective_date = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_effective_date"))).trim()

		TD_aspm_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_alt_caller_id"))).trim()
		TD_aspm_qualifier_name = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_qualifier_name"))).trim()
		TD_aspm_qualifier_value = convertHSSFCellToString(row.getCell(excelHeader.get("aspm_qualifier_value"))).trim()
		TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()

	}

	//Integration Case
	public def testDataUpdateMasterPlan(HSSFRow row)
	{
		String testd = null
		//CreateAccountComplete And UpdateMaster	Plan
		//testd = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No")))
		TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
		TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
		TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
		//TD_userid = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Status_Code"))).trim()
		TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
		TD_masterPlanUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Units"))).trim()
		TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
		TD_suppPlan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan2"))).trim()
		TD_suppPlan3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan3"))).trim()
		TD_suppPlanUnits1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units1"))).trim()
		TD_suppPlanUnits2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units2"))).trim()
		TD_suppPlanUnits3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units3"))).trim()
		TD_notifyMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Notify_Medhod"))).trim()
		TD_password = convertHSSFCellToString(row.getCell(excelHeader.get("Password"))).trim()
		TD_email = convertHSSFCellToString(row.getCell(excelHeader.get("Email"))).trim()
		TD_payMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Pay_Method"))).trim()
		TD_bank_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("bank_acct_no"))).trim()
		TD_bank_routing_number = convertHSSFCellToString(row.getCell(excelHeader.get("bank_routing_number"))).trim()
		TD_ccNumber = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Number"))).trim()
		TD_ccExpireMM = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_MM"))).trim()
		TD_ccExpireYYYY = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_YYYY"))).trim()
		TD_collection_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group1"))).trim()
		TD_collection_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group2"))).trim()


		TD_testAcctMethod = convertHSSFCellToString(row.getCell(excelHeader.get("test_acct_ind"))).trim()
		TD_doFullInvoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_full_invoicing"))).trim()
		TD_do_prorated_invoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_prorated_invoicing"))).trim()

		TD_couponCode1 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code1"))).trim()
		TD_couponCode2 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code2"))).trim()
		TD_couponCode3 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code3"))).trim()
		TD_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date"))).trim()
		TD_contract_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_plan_no"))).trim()
		TD_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no"))).trim()
		TD_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months"))).trim()

		TD_doWrite = convertHSSFCellToString(row.getCell(excelHeader.get("do_write"))).trim()
		TD_taxExemptionLevel = convertHSSFCellToString(row.getCell(excelHeader.get("Tax_Exemption_Level"))).trim()
		TD_retroActiveStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Retroactive_Start_Date"))).trim()
		TD_altStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Start_Date"))).trim()
		TD_altBillDay = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Billing_Day"))).trim()
		TD_assignmentdirective = convertHSSFCellToString(row.getCell(excelHeader.get("AD"))).trim()
		TD_currency_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Currency_cd"))).trim()

		TD_custom_rate_plan_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1"))).trim()
		TD_cutom_rate_service_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1"))).trim()
		TD_custom_rate_seq_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1"))).trim()
		TD_custom_rate_from_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1"))).trim()
		TD_custom_rate_to_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1"))).trim()
		TD_custom_rate_per_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1"))).trim()
		TD_custom_rate_plan_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2"))).trim()
		TD_cutom_rate_service_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2"))).trim()
		TD_custom_rate_seq_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2"))).trim()
		TD_custom_rate_from_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2"))).trim()
		TD_custom_rate_to_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2"))).trim()
		TD_custom_rate_per_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2"))).trim()

		TD_functional_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group1"))).trim()
		TD_functional_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group2"))).trim()
		TD_status_until_alt_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("status_until_alt_start_date"))).trim()
		TD_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee"))).trim()

		TD_firstName = convertHSSFCellToString(row.getCell(excelHeader.get("First_Name"))).trim()
		TD_mi = convertHSSFCellToString(row.getCell(excelHeader.get("MI"))).trim()
		TD_lastName = convertHSSFCellToString(row.getCell(excelHeader.get("Last_Name"))).trim()
		TD_companyName = convertHSSFCellToString(row.getCell(excelHeader.get("Company_Name"))).trim()
		TD_addr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr1"))).trim()
		TD_addr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr2"))).trim()
		TD_city = convertHSSFCellToString(row.getCell(excelHeader.get("City"))).trim()
		TD_state = convertHSSFCellToString(row.getCell(excelHeader.get("State"))).trim()
		TD_locality = convertHSSFCellToString(row.getCell(excelHeader.get("Locality"))).trim()
		TD_country = convertHSSFCellToString(row.getCell(excelHeader.get("Country"))).trim()
		TD_postalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Postal_Code"))).trim()
		TD_phone = convertHSSFCellToString(row.getCell(excelHeader.get("Phone"))).trim()

		TD_billFirstName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_First_Name"))).trim()
		TD_billMi = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_MI"))).trim()
		TD_billLastName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Last_Name"))).trim()
		TD_billCompanyName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Company_Name"))).trim()
		TD_billAddr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr1"))).trim()
		TD_billAddr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr2"))).trim()
		TD_billCity = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_City"))).trim()
		TD_billState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_State"))).trim()
		TD_billLocality = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Locality"))).trim()
		TD_billCountry = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Country"))).trim()
		TD_billPostalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Postal_Code"))).trim()
		TD_billPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone"))).trim()
		TD_billEmail = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Email"))).trim()
		TD_respLevelCd = convertHSSFCellToString(row.getCell(excelHeader.get("Resp_Level_Cd"))).trim()
		TD_qualifierName1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name1"))).trim()
		TD_qualifierName2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name2"))).trim()
		TD_qualifierValue1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value1"))).trim()
		TD_qualifierValue2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value2"))).trim()

		TD_clientAcctId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Acct_Id"))).trim()
		TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Cd"))).trim()
		TD_secretQuestion = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question"))).trim()
		TD_secretQuestionAanswer = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question_Answer"))).trim()
		TD_phoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Phone_Ext"))).trim()
		TD_cellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Cell_Phone"))).trim()
		TD_workPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone"))).trim()
		TD_workPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone_Ext"))).trim()
		TD_birthDate = convertHSSFCellToString(row.getCell(excelHeader.get("Birthdate"))).trim()
		TD_billPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone_Ext"))).trim()
		TD_billCellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Cell_Phone"))).trim()
		TD_billWorkPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone"))).trim()
		TD_billWorkPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone_Ext"))).trim()
		TD_balanceForward = convertHSSFCellToString(row.getCell(excelHeader.get("Balance_Forward"))).trim()
		TD_masterPlanAltRateSchedNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Alt_Rate_Sched_No"))).trim()
		TD_clientReceiptId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
		TD_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("Cvv"))).trim()
		TD_taxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Taxpayer_Id"))).trim()
		TD_billAgreementId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Agreement_Id"))).trim()
		TD_altMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Msg_Template_No"))).trim()
		TD_seqFuncGroupNo = convertHSSFCellToString(row.getCell(excelHeader.get("Seq_Func_Group_No"))).trim()
		TD_bankAcctType = convertHSSFCellToString(row.getCell(excelHeader.get("Bank_Acct_Type"))).trim()
		TD_billDriversLicenseNo = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_No"))).trim()
		TD_billDriversLicenseState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_State"))).trim()
		TD_billTaxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Taxpayer_Id"))).trim()
		TD_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("Address3"))).trim()
		TD_billAddress3 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Address3"))).trim()
		TD_trackData1 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data1"))).trim()
		TD_trackData2 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data2"))).trim()
		TD_cnAltMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Cn_Alt_Msg_Template_No"))).trim()
		TD_altCallerId = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()

		TD_effectiveDate = convertHSSFCellToString(row.getCell(excelHeader.get("Effective_Date"))).trim()
		// Update Master Plan

		TD_update_master_plan_imm = convertHSSFCellToString(row.getCell(excelHeader.get("update_master_plan_imm"))).trim()
		TD_ump_master_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_master_plan_no"))).trim()
		TD_ump_alt_rate_schedule_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_alt_rate_schedule_no"))).trim()
		TD_ump_num_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("ump_num_plan_units"))).trim()
		TD_ump_assignment_directive = convertHSSFCellToString(row.getCell(excelHeader.get("ump_assignment_directive"))).trim()
		TD_ump_do_write = convertHSSFCellToString(row.getCell(excelHeader.get("ump_do_write"))).trim()
		TD_ump_client_receipt_id = convertHSSFCellToString(row.getCell(excelHeader.get("ump_client_receipt_id"))).trim()
		TD_ump_force_currency_change = convertHSSFCellToString(row.getCell(excelHeader.get("ump_force_currency_change"))).trim()
		TD_ump_auto_cancel_supp_plans = convertHSSFCellToString(row.getCell(excelHeader.get("ump_auto_cancel_supp_plans"))).trim()
		TD_ump_offset_months = convertHSSFCellToString(row.getCell(excelHeader.get("ump_offset_months"))).trim()
		TD_ump_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("ump_alt_proration_start_date"))).trim()

		TD_ump_alt_client_acct_group_id = convertHSSFCellToString(row.getCell(excelHeader.get("ump_alt_client_acct_group_id"))).trim()
		TD_ump_custom_rate_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_plan_no"))).trim()
		TD_ump_custom_rate_service_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_service_no"))).trim()
		TD_ump_custom_rate_seq_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_seq_no"))).trim()
		TD_ump_custom_rate_from_unit = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_from_unit"))).trim()
		TD_ump_custom_rate_to_unit = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_to_unit"))).trim()
		TD_ump_custom_rate_per_unit = convertHSSFCellToString(row.getCell(excelHeader.get("ump_custom_rate_per_unit"))).trim()
		TD_ump_effective_date = convertHSSFCellToString(row.getCell(excelHeader.get("ump_effective_date"))).trim()
		TD_ump_offset_interval = convertHSSFCellToString(row.getCell(excelHeader.get("ump_offset_interval"))).trim()
		TD_ump_invoice_unbilled_usage = convertHSSFCellToString(row.getCell(excelHeader.get("ump_invoice_unbilled_usage"))).trim()

		TD_ump_coupon_code = convertHSSFCellToString(row.getCell(excelHeader.get("ump_coupon_code"))).trim()
		TD_ump_alt_caller_id = convertHSSFCellToString(row.getCell(excelHeader.get("ump_alt_caller_id"))).trim()
		TD_ump_qualifier_name = convertHSSFCellToString(row.getCell(excelHeader.get("ump_qualifier_name"))).trim()
		TD_ump_qualifier_value = convertHSSFCellToString(row.getCell(excelHeader.get("ump_qualifier_value"))).trim()
		TD_ump_one_day_advance = convertHSSFCellToString(row.getCell(excelHeader.get("ump_one_day_advance"))).trim()
		TD_ump_uac_option = convertHSSFCellToString(row.getCell(excelHeader.get("uac_option"))).trim()
		TD_ump_uac_master_Plan = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_Plan"))).trim()
		TD_ump_uac_master_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("uac_master_plan_units"))).trim()
		TD_ump_uac_AD = convertHSSFCellToString(row.getCell(excelHeader.get("uac_AD"))).trim()
		TD_ump_uac_collection_group = convertHSSFCellToString(row.getCell(excelHeader.get("uac_collection_group"))).trim()
		TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()
	}


	public def testDataAssignSuppPlanMulti(HSSFRow row) {

		TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
		TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
		TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
		TD_userid = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Status_Code"))).trim()
		TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
		TD_masterPlanUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Units"))).trim()
		TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
		TD_suppPlan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan2"))).trim()
		TD_suppPlan3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan3"))).trim()
		TD_suppPlanUnits1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units1"))).trim()
		TD_suppPlanUnits2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units2"))).trim()
		TD_suppPlanUnits3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units3"))).trim()
		TD_notifyMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Notify_Medhod"))).trim()
		TD_password = convertHSSFCellToString(row.getCell(excelHeader.get("Password"))).trim()
		TD_email = convertHSSFCellToString(row.getCell(excelHeader.get("Email"))).trim()
		TD_payMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Pay_Method"))).trim()
		TD_bank_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("bank_acct_no"))).trim()
		TD_bank_routing_number = convertHSSFCellToString(row.getCell(excelHeader.get("bank_routing_number"))).trim()
		TD_ccNumber = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Number"))).trim()
		TD_ccExpireMM = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_MM"))).trim()
		TD_ccExpireYYYY = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_YYYY"))).trim()
		TD_collection_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group1"))).trim()
		TD_collection_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group2"))).trim()

logi.logInfo "Test log read data 1"
		TD_testAcctMethod = convertHSSFCellToString(row.getCell(excelHeader.get("test_acct_ind"))).trim()
		TD_doFullInvoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_full_invoicing"))).trim()
		TD_do_prorated_invoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_prorated_invoicing"))).trim()
		logi.logInfo "Test log read data 2"
		TD_couponCode1 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code1"))).trim()
		TD_couponCode2 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code2"))).trim()
		TD_couponCode3 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code3"))).trim()
		TD_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date"))).trim()
		TD_contract_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_plan_no"))).trim()
		TD_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no"))).trim()
		TD_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months"))).trim()
		logi.logInfo "Test log read data 3"
		TD_doWrite = convertHSSFCellToString(row.getCell(excelHeader.get("do_write"))).trim()
		TD_taxExemptionLevel = convertHSSFCellToString(row.getCell(excelHeader.get("Tax_Exemption_Level"))).trim()
		TD_retroActiveStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Retroactive_Start_Date"))).trim()
		TD_altStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Start_Date"))).trim()
		TD_altBillDay = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Billing_Day"))).trim()
		TD_assignmentdirective = convertHSSFCellToString(row.getCell(excelHeader.get("AD"))).trim()
		TD_currency_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Currency_cd"))).trim()
logi.logInfo "Test log read data 4"
		TD_custom_rate_plan_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1"))).trim()
		TD_cutom_rate_service_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1"))).trim()
		TD_custom_rate_seq_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1"))).trim()
		TD_custom_rate_from_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1"))).trim()
		TD_custom_rate_to_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1"))).trim()
		TD_custom_rate_per_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1"))).trim()
		TD_custom_rate_plan_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2"))).trim()
		TD_cutom_rate_service_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2"))).trim()
		TD_custom_rate_seq_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2"))).trim()
		TD_custom_rate_from_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2"))).trim()
		TD_custom_rate_to_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2"))).trim()
		TD_custom_rate_per_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2"))).trim()

		TD_functional_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group1"))).trim()
		TD_functional_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group2"))).trim()
		TD_status_until_alt_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("status_until_alt_start_date"))).trim()
		TD_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee"))).trim()
		logi.logInfo "Test log read data 5"
		TD_firstName = convertHSSFCellToString(row.getCell(excelHeader.get("First_Name"))).trim()
		TD_mi = convertHSSFCellToString(row.getCell(excelHeader.get("MI"))).trim()
		TD_lastName = convertHSSFCellToString(row.getCell(excelHeader.get("Last_Name"))).trim()
		TD_companyName = convertHSSFCellToString(row.getCell(excelHeader.get("Company_Name"))).trim()
		TD_addr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr1"))).trim()
		TD_addr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr2"))).trim()
		TD_city = convertHSSFCellToString(row.getCell(excelHeader.get("City"))).trim()
		TD_state = convertHSSFCellToString(row.getCell(excelHeader.get("State"))).trim()
		TD_locality = convertHSSFCellToString(row.getCell(excelHeader.get("Locality"))).trim()
		TD_country = convertHSSFCellToString(row.getCell(excelHeader.get("Country"))).trim()
		TD_postalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Postal_Code"))).trim()
		TD_phone = convertHSSFCellToString(row.getCell(excelHeader.get("Phone"))).trim()
logi.logInfo "Test log read data 6"
		TD_billFirstName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_First_Name"))).trim()
		TD_billMi = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_MI"))).trim()
		TD_billLastName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Last_Name"))).trim()
		TD_billCompanyName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Company_Name"))).trim()
		TD_billAddr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr1"))).trim()
		TD_billAddr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr2"))).trim()
		TD_billCity = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_City"))).trim()
		TD_billState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_State"))).trim()
		TD_billLocality = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Locality"))).trim()
		TD_billCountry = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Country"))).trim()
		TD_billPostalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Postal_Code"))).trim()
		TD_billPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone"))).trim()
		TD_billEmail = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Email"))).trim()
		TD_respLevelCd = convertHSSFCellToString(row.getCell(excelHeader.get("Resp_Level_Cd"))).trim()
		TD_qualifierName1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name1"))).trim()
		TD_qualifierName2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name2"))).trim()
		TD_qualifierValue1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value1"))).trim()
		TD_qualifierValue2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value2"))).trim()
		logi.logInfo "Test log read data 7"
		TD_clientAcctId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Acct_Id"))).trim()
		TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Cd"))).trim()
		TD_secretQuestion = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question"))).trim()
		TD_secretQuestionAanswer = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question_Answer"))).trim()
		TD_phoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Phone_Ext"))).trim()
		TD_cellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Cell_Phone"))).trim()
		TD_workPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone"))).trim()
		TD_workPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone_Ext"))).trim()
		TD_birthDate = convertHSSFCellToString(row.getCell(excelHeader.get("Birthdate"))).trim()
		TD_billPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone_Ext"))).trim()
		TD_billCellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Cell_Phone"))).trim()
		TD_billWorkPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone"))).trim()
		TD_billWorkPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone_Ext"))).trim()
		TD_balanceForward = convertHSSFCellToString(row.getCell(excelHeader.get("Balance_Forward"))).trim()
		TD_masterPlanAltRateSchedNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Alt_Rate_Sched_No"))).trim()
		TD_clientReceiptId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
		TD_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("Cvv"))).trim()
		TD_taxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Taxpayer_Id"))).trim()
		TD_billAgreementId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Agreement_Id"))).trim()
		TD_altMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Msg_Template_No"))).trim()
		TD_seqFuncGroupNo = convertHSSFCellToString(row.getCell(excelHeader.get("Seq_Func_Group_No"))).trim()
		TD_bankAcctType = convertHSSFCellToString(row.getCell(excelHeader.get("Bank_Acct_Type"))).trim()
		TD_billDriversLicenseNo = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_No"))).trim()
		TD_billDriversLicenseState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_State"))).trim()
		TD_billTaxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Taxpayer_Id"))).trim()
		TD_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("Address3"))).trim()
		TD_billAddress3 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Address3"))).trim()
		TD_trackData1 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data1"))).trim()
		TD_trackData2 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data2"))).trim()
		TD_cnAltMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Cn_Alt_Msg_Template_No"))).trim()
		TD_altCallerId = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()

		TD_assign_supp_plan_1 = convertHSSFCellToString(row.getCell(excelHeader.get("Assign_Supp_Plan_1"))).trim()
		TD_assign_supp_plan_2 = convertHSSFCellToString(row.getCell(excelHeader.get("Assign_Supp_Plan_2"))).trim()
		TD_assign_supp_plan_3 = convertHSSFCellToString(row.getCell(excelHeader.get("Assign_Supp_Plan_3"))).trim()
		TD_assign_supp_plan_4 = convertHSSFCellToString(row.getCell(excelHeader.get("Assign_Supp_Plan_4"))).trim()

		TD_num_plan_units_sp_1 = convertHSSFCellToString(row.getCell(excelHeader.get("num_plan_units_sp_1"))).trim()
		TD_num_plan_units_sp_2 = convertHSSFCellToString(row.getCell(excelHeader.get("num_plan_units_sp_2"))).trim()
		TD_num_plan_units_sp_3 = convertHSSFCellToString(row.getCell(excelHeader.get("num_plan_units_sp_3"))).trim()
		TD_num_plan_units_sp_4 = convertHSSFCellToString(row.getCell(excelHeader.get("num_plan_units_sp_4"))).trim()
		logi.logInfo "Test log read data 8"
		TD_Advance_VirtualTime = convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).trim()
		TD_VerifyDate = convertHSSFCellToString(row.getCell(excelHeader.get("Verify_Date"))).trim()
		TD_altrateschedulenosp = convertHSSFCellToString(row.getCell(excelHeader.get("alt_rate_schedule_no_SP"))).trim()
		TD_altclientacctgroupid = convertHSSFCellToString(row.getCell(excelHeader.get("alt_client_acct_group_id"))).trim()
		TD_doWriteSP = convertHSSFCellToString(row.getCell(excelHeader.get("Do_write_SP"))).trim()

		TD_coupon_code_sp = convertHSSFCellToString(row.getCell(excelHeader.get("coupon_code_sp"))).trim()
		TD_comments_sp = convertHSSFCellToString(row.getCell(excelHeader.get("comments_sp"))).trim()
		TD_client_receipt_id_sp = convertHSSFCellToString(row.getCell(excelHeader.get("client_receipt_id_sp"))).trim()
		TD_contract_type_no_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no_sp"))).trim()
		TD_contract_alt_recur_fee_no_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee_no_sp"))).trim()
		TD_contract_length_months_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months_sp"))).trim()
		TD_contract_cancel_fee_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_cancel_fee_sp"))).trim()
		TD_contract_comments_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_comments_sp"))).trim()
		TD_contract_start_date_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date_sp"))).trim()
		TD_offset_months_sp = convertHSSFCellToString(row.getCell(excelHeader.get("offset_months_sp"))).trim()
		TD_auto_offset_months_options = convertHSSFCellToString(row.getCell(excelHeader.get("auto_offset_months_options_sp"))).trim()
		TD_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("alt_proration_start_date_sp"))).trim()
		TD_effectiveDate = convertHSSFCellToString(row.getCell(excelHeader.get("Effective_Date"))).trim()
		TD_assign_sp_imm = convertHSSFCellToString(row.getCell(excelHeader.get("assign_sp_imm"))).trim()
		TD_effectiveDateSP = convertHSSFCellToString(row.getCell(excelHeader.get("Effective_Date_sp"))).trim()
		TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()

	}

	public def testDataIntegrationBug1664(HSSFRow row) {

		TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
		TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
		TD_serviceOrder = convertHSSFCellToString(row.getCell(excelHeader.get("Service_Order")))
		TD_serviceOrder1 = convertHSSFCellToString(row.getCell(excelHeader.get("Service_Order1")))
		//							//testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID"))).trim()
		//							clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
		//masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
		TD_userid = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Status_Code"))).trim()
		TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
		TD_masterPlanUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Units"))).trim()
		TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
		TD_suppPlan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan2"))).trim()
		TD_suppPlan3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan3"))).trim()
		TD_suppPlanUnits1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units1"))).trim()
		TD_suppPlanUnits2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units2"))).trim()
		TD_suppPlanUnits3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units3"))).trim()
		TD_notifyMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Notify_Medhod"))).trim()
		TD_password = convertHSSFCellToString(row.getCell(excelHeader.get("Password"))).trim()
		TD_email = convertHSSFCellToString(row.getCell(excelHeader.get("Email"))).trim()
		TD_payMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Pay_Method"))).trim()
		TD_ccNumber = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Number"))).trim()
		TD_ccExpireMM = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_MM"))).trim()
		TD_ccExpireYYYY = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_YYYY"))).trim()
		TD_collection_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group1"))).trim()
		TD_collection_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group2"))).trim()


		TD_testAcctMethod = convertHSSFCellToString(row.getCell(excelHeader.get("test_acct_ind"))).trim()
		TD_doFullInvoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_full_invoicing"))).trim()
		TD_couponCode1 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code1"))).trim()
		TD_couponCode2 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code2"))).trim()
		TD_couponCode3 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code3"))).trim()

		TD_contract_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_plan_no"))).trim()
		TD_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no"))).trim()
		TD_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months"))).trim()

		TD_doWrite = convertHSSFCellToString(row.getCell(excelHeader.get("do_write"))).trim()
		TD_retroActiveStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Retroactive_Start_Date"))).trim()
		TD_altStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Start_Date"))).trim()
		TD_altBillDay = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Billing_Day"))).trim()
		TD_taxExemptionLevel = convertHSSFCellToString(row.getCell(excelHeader.get("Tax_Exemption_Level"))).trim()
		TD_currency_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Currency_cd"))).trim()

		TD_custom_rate_plan_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1"))).trim()
		TD_cutom_rate_service_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1"))).trim()
		TD_custom_rate_seq_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1"))).trim()
		TD_custom_rate_from_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1"))).trim()
		TD_custom_rate_to_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1"))).trim()
		TD_custom_rate_per_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1"))).trim()

		TD_custom_rate_plan_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2"))).trim()
		TD_cutom_rate_service_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2"))).trim()
		TD_custom_rate_seq_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2"))).trim()
		TD_custom_rate_from_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2"))).trim()
		TD_custom_rate_to_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2"))).trim()
		TD_custom_rate_per_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2"))).trim()

		TD_functional_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group1"))).trim()
		TD_functional_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group2"))).trim()
		TD_do_prorated_invoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_prorated_invoicing"))).trim()

		TD_bank_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("bank_acct_no"))).trim()
		TD_bank_routing_number = convertHSSFCellToString(row.getCell(excelHeader.get("bank_routing_number"))).trim()
		TD_status_until_alt_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("status_until_alt_start_date"))).trim()
		TD_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee"))).trim()

		TD_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date"))).trim()

		TD_firstName = convertHSSFCellToString(row.getCell(excelHeader.get("First_Name"))).trim()
		TD_mi = convertHSSFCellToString(row.getCell(excelHeader.get("MI"))).trim()
		TD_lastName = convertHSSFCellToString(row.getCell(excelHeader.get("Last_Name"))).trim()
		TD_companyName = convertHSSFCellToString(row.getCell(excelHeader.get("Company_Name"))).trim()
		TD_addr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr1"))).trim()
		TD_addr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr2"))).trim()
		TD_city = convertHSSFCellToString(row.getCell(excelHeader.get("City"))).trim()
		TD_state = convertHSSFCellToString(row.getCell(excelHeader.get("State"))).trim()
		TD_locality = convertHSSFCellToString(row.getCell(excelHeader.get("Locality"))).trim()
		TD_country = convertHSSFCellToString(row.getCell(excelHeader.get("Country"))).trim()
		TD_postalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Postal_Code"))).trim()
		TD_phone = convertHSSFCellToString(row.getCell(excelHeader.get("Phone"))).trim()


		TD_billFirstName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_First_Name"))).trim()
		TD_billMi = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_MI"))).trim()
		TD_billLastName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Last_Name"))).trim()
		TD_billCompanyName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Company_Name"))).trim()
		TD_billAddr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr1"))).trim()
		TD_billAddr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr2"))).trim()
		TD_billCity = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_City"))).trim()
		TD_billState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_State"))).trim()
		TD_billLocality = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Locality"))).trim()
		TD_billCountry = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Country"))).trim()
		TD_billPostalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Postal_Code"))).trim()
		TD_billPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone"))).trim()
		TD_billEmail = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Email"))).trim()
		TD_respLevelCd = convertHSSFCellToString(row.getCell(excelHeader.get("Resp_Level_Cd"))).trim()
		TD_qualifierName1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name1"))).trim()
		TD_qualifierName2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name2"))).trim()
		TD_qualifierValue1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value1"))).trim()
		TD_qualifierValue2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value2"))).trim()

		TD_clientAcctId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Acct_Id"))).trim()
		TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Cd"))).trim()
		TD_secretQuestion = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question"))).trim()
		TD_secretQuestionAanswer = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question_Answer"))).trim()
		TD_phoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Phone_Ext"))).trim()
		TD_cellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Cell_Phone"))).trim()
		TD_workPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone"))).trim()
		TD_workPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone_Ext"))).trim()
		TD_birthDate = convertHSSFCellToString(row.getCell(excelHeader.get("Birthdate"))).trim()
		TD_billPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone_Ext"))).trim()
		TD_billCellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Cell_Phone"))).trim()
		TD_billWorkPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone"))).trim()
		TD_billWorkPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone_Ext"))).trim()
		TD_balanceForward = convertHSSFCellToString(row.getCell(excelHeader.get("Balance_Forward"))).trim()
		TD_masterPlanAltRateSchedNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Alt_Rate_Sched_No"))).trim()
		TD_clientReceiptId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
		TD_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("Cvv"))).trim()
		TD_taxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Taxpayer_Id"))).trim()
		TD_billAgreementId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Agreement_Id"))).trim()
		TD_altMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Msg_Template_No"))).trim()
		TD_seqFuncGroupNo = convertHSSFCellToString(row.getCell(excelHeader.get("Seq_Func_Group_No"))).trim()
		TD_bankAcctType = convertHSSFCellToString(row.getCell(excelHeader.get("Bank_Acct_Type"))).trim()
		TD_billDriversLicenseNo = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_No"))).trim()
		TD_billDriversLicenseState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_State"))).trim()
		TD_billTaxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Taxpayer_Id"))).trim()
		TD_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("Address3"))).trim()
		//		TD_billAddress3 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Address3"))).trim()
		//		TD_trackData1 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data1"))).trim()
		//		TD_trackData2 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data2"))).trim()
		//		TD_cnAltMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Cn_Alt_Msg_Template_No"))).trim()
		//		TD_altCallerId = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()
		TD_Advance_VirtualTime = convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).trim()
		TD_VerifyDate = convertHSSFCellToString(row.getCell(excelHeader.get("Verify_Date"))).trim()
		TD_ClientSKU = convertHSSFCellToString(row.getCell(excelHeader.get("Client_SKU"))).trim()
		TD_OrderUnit = convertHSSFCellToString(row.getCell(excelHeader.get("Order_Unit"))).trim()
		TD_OrderAmount = convertHSSFCellToString(row.getCell(excelHeader.get("Order_Amount"))).trim()
		TD_BillImmediately = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Immediately"))).trim()
		TD_Usage_Type = convertHSSFCellToString(row.getCell(excelHeader.get("Usage_Type"))).trim()
		TD_Usage_Units = convertHSSFCellToString(row.getCell(excelHeader.get("Usage_Units"))).trim()
		TD_Rec_Usage_Amt = convertHSSFCellToString(row.getCell(excelHeader.get("Rec_Usage_Amt"))).trim()
		TD_Rec_Usage_Rate = convertHSSFCellToString(row.getCell(excelHeader.get("Rec_Usage_Rate"))).trim()
		TD_assignmentdirective = convertHSSFCellToString(row.getCell(excelHeader.get("AD"))).trim()
		TD_assign_supp_plan = convertHSSFCellToString(row.getCell(excelHeader.get("Assign_Supp_Plan"))).trim()
		TD_doWriteSP = convertHSSFCellToString(row.getCell(excelHeader.get("Do_write_SP"))).trim()
		TD_num_plan_units_sp = convertHSSFCellToString(row.getCell(excelHeader.get("num_plan_units_sp"))).trim()
		TD_cancelSuppPlan = convertHSSFCellToString(row.getCell(excelHeader.get("Cancel_Supp_Plan"))).trim()
		TD_ump_master_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("ump_master_plan_no"))).trim()
		TD_ump_num_plan_units = convertHSSFCellToString(row.getCell(excelHeader.get("ump_num_plan_units"))).trim()
		TD_credit_amount = convertHSSFCellToString(row.getCell(excelHeader.get("credit_amount"))).trim()
		TD_UnitDiscountAmt = convertHSSFCellToString(row.getCell(excelHeader.get("Unit_Discount_Amt"))).trim()
		TD_apply_coupon = convertHSSFCellToString(row.getCell(excelHeader.get("apply_coupon"))).trim()
		TD_assign_supp_plan_1 = convertHSSFCellToString(row.getCell(excelHeader.get("Assign_Supp_Plan_1"))).trim()
		TD_assign_supp_plan_2 = convertHSSFCellToString(row.getCell(excelHeader.get("Assign_Supp_Plan_2"))).trim()
		TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()
	}


	public def testDataAssignSuppPlan(HSSFRow row){
		String testd = null
		testd = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No")))
		TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
		TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
		TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
		TD_userid = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
		TD_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Status_Code"))).trim()
		TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
		TD_masterPlanUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Units"))).trim()
		TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
		TD_suppPlan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan2"))).trim()
		TD_suppPlan3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan3"))).trim()
		TD_suppPlanUnits1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units1"))).trim()
		TD_suppPlanUnits2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units2"))).trim()
		TD_suppPlanUnits3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units3"))).trim()
		TD_notifyMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Notify_Medhod"))).trim()
		TD_password = convertHSSFCellToString(row.getCell(excelHeader.get("Password"))).trim()
		TD_email = convertHSSFCellToString(row.getCell(excelHeader.get("Email"))).trim()
		TD_payMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Pay_Method"))).trim()
		TD_bank_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("bank_acct_no"))).trim()
		TD_bank_routing_number = convertHSSFCellToString(row.getCell(excelHeader.get("bank_routing_number"))).trim()
		TD_ccNumber = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Number"))).trim()
		TD_ccExpireMM = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_MM"))).trim()
		TD_ccExpireYYYY = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_YYYY"))).trim()
		TD_collection_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group1"))).trim()
		TD_collection_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group2"))).trim()


		TD_testAcctMethod = convertHSSFCellToString(row.getCell(excelHeader.get("test_acct_ind"))).trim()
		TD_doFullInvoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_full_invoicing"))).trim()
		TD_do_prorated_invoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_prorated_invoicing"))).trim()

		TD_couponCode1 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code1"))).trim()
		TD_couponCode2 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code2"))).trim()
		TD_couponCode3 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code3"))).trim()
		TD_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date"))).trim()
		TD_contract_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_plan_no"))).trim()
		TD_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no"))).trim()
		TD_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months"))).trim()

		TD_doWrite = convertHSSFCellToString(row.getCell(excelHeader.get("do_write"))).trim()
		TD_taxExemptionLevel = convertHSSFCellToString(row.getCell(excelHeader.get("Tax_Exemption_Level"))).trim()
		TD_retroActiveStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Retroactive_Start_Date"))).trim()
		TD_altStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Start_Date"))).trim()
		TD_altBillDay = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Billing_Day"))).trim()
		TD_assignmentdirective = convertHSSFCellToString(row.getCell(excelHeader.get("AD"))).trim()
		TD_currency_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Currency_cd"))).trim()

		TD_custom_rate_plan_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1"))).trim()
		TD_cutom_rate_service_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1"))).trim()
		TD_custom_rate_seq_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1"))).trim()
		TD_custom_rate_from_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1"))).trim()
		TD_custom_rate_to_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1"))).trim()
		TD_custom_rate_per_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1"))).trim()
		TD_custom_rate_plan_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2"))).trim()
		TD_cutom_rate_service_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2"))).trim()
		TD_custom_rate_seq_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2"))).trim()
		TD_custom_rate_from_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2"))).trim()
		TD_custom_rate_to_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2"))).trim()
		TD_custom_rate_per_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2"))).trim()

		TD_functional_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group1"))).trim()
		TD_functional_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group2"))).trim()
		TD_status_until_alt_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("status_until_alt_start_date"))).trim()
		TD_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee"))).trim()

		TD_firstName = convertHSSFCellToString(row.getCell(excelHeader.get("First_Name"))).trim()
		TD_mi = convertHSSFCellToString(row.getCell(excelHeader.get("MI"))).trim()
		TD_lastName = convertHSSFCellToString(row.getCell(excelHeader.get("Last_Name"))).trim()
		TD_companyName = convertHSSFCellToString(row.getCell(excelHeader.get("Company_Name"))).trim()
		TD_addr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr1"))).trim()
		TD_addr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr2"))).trim()
		TD_city = convertHSSFCellToString(row.getCell(excelHeader.get("City"))).trim()
		TD_state = convertHSSFCellToString(row.getCell(excelHeader.get("State"))).trim()
		TD_locality = convertHSSFCellToString(row.getCell(excelHeader.get("Locality"))).trim()
		TD_country = convertHSSFCellToString(row.getCell(excelHeader.get("Country"))).trim()
		TD_postalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Postal_Code"))).trim()
		TD_phone = convertHSSFCellToString(row.getCell(excelHeader.get("Phone"))).trim()

		TD_billFirstName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_First_Name"))).trim()
		TD_billMi = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_MI"))).trim()
		TD_billLastName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Last_Name"))).trim()
		TD_billCompanyName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Company_Name"))).trim()
		TD_billAddr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr1"))).trim()
		TD_billAddr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr2"))).trim()
		TD_billCity = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_City"))).trim()
		TD_billState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_State"))).trim()
		TD_billLocality = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Locality"))).trim()
		TD_billCountry = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Country"))).trim()
		TD_billPostalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Postal_Code"))).trim()
		TD_billPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone"))).trim()
		TD_billEmail = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Email"))).trim()
		TD_respLevelCd = convertHSSFCellToString(row.getCell(excelHeader.get("Resp_Level_Cd"))).trim()
		TD_qualifierName1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name1"))).trim()
		TD_qualifierName2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name2"))).trim()
		TD_qualifierValue1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value1"))).trim()
		TD_qualifierValue2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value2"))).trim()

		TD_clientAcctId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Acct_Id"))).trim()
		TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Cd"))).trim()
		TD_secretQuestion = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question"))).trim()
		TD_secretQuestionAanswer = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question_Answer"))).trim()
		TD_phoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Phone_Ext"))).trim()
		TD_cellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Cell_Phone"))).trim()
		TD_workPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone"))).trim()
		TD_workPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone_Ext"))).trim()
		TD_birthDate = convertHSSFCellToString(row.getCell(excelHeader.get("Birthdate"))).trim()
		TD_billPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone_Ext"))).trim()
		TD_billCellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Cell_Phone"))).trim()
		TD_billWorkPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone"))).trim()
		TD_billWorkPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone_Ext"))).trim()
		TD_balanceForward = convertHSSFCellToString(row.getCell(excelHeader.get("Balance_Forward"))).trim()
		TD_masterPlanAltRateSchedNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Alt_Rate_Sched_No"))).trim()
		TD_clientReceiptId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
		TD_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("Cvv"))).trim()
		TD_taxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Taxpayer_Id"))).trim()
		TD_billAgreementId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Agreement_Id"))).trim()
		TD_altMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Msg_Template_No"))).trim()
		TD_seqFuncGroupNo = convertHSSFCellToString(row.getCell(excelHeader.get("Seq_Func_Group_No"))).trim()
		TD_bankAcctType = convertHSSFCellToString(row.getCell(excelHeader.get("Bank_Acct_Type"))).trim()
		TD_billDriversLicenseNo = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_No"))).trim()
		TD_billDriversLicenseState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_State"))).trim()
		TD_billTaxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Taxpayer_Id"))).trim()
		TD_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("Address3"))).trim()
		TD_billAddress3 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Address3"))).trim()
		TD_trackData1 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data1"))).trim()
		TD_trackData2 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data2"))).trim()
		TD_cnAltMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Cn_Alt_Msg_Template_No"))).trim()
		TD_altCallerId = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()
		TD_assign_supp_plan = convertHSSFCellToString(row.getCell(excelHeader.get("Assign_Supp_Plan"))).trim()
		TD_Advance_VirtualTime = convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).trim()
		TD_VerifyDate = convertHSSFCellToString(row.getCell(excelHeader.get("Verify_Date"))).trim()
		TD_altrateschedulenosp = convertHSSFCellToString(row.getCell(excelHeader.get("alt_rate_schedule_no_SP"))).trim()
		TD_altclientacctgroupid = convertHSSFCellToString(row.getCell(excelHeader.get("alt_client_acct_group_id"))).trim()
		TD_doWriteSP = convertHSSFCellToString(row.getCell(excelHeader.get("Do_write_SP"))).trim()
		TD_num_plan_units_sp = convertHSSFCellToString(row.getCell(excelHeader.get("num_plan_units_sp"))).trim()
		TD_coupon_code_sp = convertHSSFCellToString(row.getCell(excelHeader.get("coupon_code_sp"))).trim()
		TD_comments_sp = convertHSSFCellToString(row.getCell(excelHeader.get("comments_sp"))).trim()
		TD_client_receipt_id_sp = convertHSSFCellToString(row.getCell(excelHeader.get("client_receipt_id_sp"))).trim()
		TD_contract_type_no_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no_sp"))).trim()
		TD_contract_alt_recur_fee_no_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee_no_sp"))).trim()
		TD_contract_length_months_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months_sp"))).trim()
		TD_contract_cancel_fee_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_cancel_fee_sp"))).trim()
		TD_contract_comments_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_comments_sp"))).trim()
		TD_contract_start_date_sp = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date_sp"))).trim()
		TD_offset_months_sp = convertHSSFCellToString(row.getCell(excelHeader.get("offset_months_sp"))).trim()
		TD_auto_offset_months_options = convertHSSFCellToString(row.getCell(excelHeader.get("auto_offset_months_options_sp"))).trim()
		TD_alt_proration_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("alt_proration_start_date_sp"))).trim()
		TD_effectiveDate = convertHSSFCellToString(row.getCell(excelHeader.get("Effective_Date"))).trim()
		TD_assign_sp_imm = convertHSSFCellToString(row.getCell(excelHeader.get("assign_sp_imm"))).trim()

		TD_custom_rate_plan_no_1_ASP = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1_SP"))).trim()
		TD_cutom_rate_service_no_1_ASP = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1_SP"))).trim()
		TD_custom_rate_seq_no_1_ASP = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1_SP"))).trim()
		TD_custom_rate_from_unit_1_ASP = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1_SP"))).trim()
		TD_custom_rate_to_unit_1_ASP = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1_SP"))).trim()
		TD_custom_rate_per_unit_1_ASP = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1_SP"))).trim()

		TD_custom_rate_plan_no_2_ASP = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2_SP"))).trim()
		TD_cutom_rate_service_no_2_ASP = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2_SP"))).trim()
		TD_custom_rate_seq_no_2_ASP = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2_SP"))).trim()
		TD_custom_rate_from_unit_2_ASP = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2_SP"))).trim()
		TD_custom_rate_to_unit_2_ASP = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2_SP"))).trim()
		TD_custom_rate_per_unit_2_ASP = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2_SP"))).trim()


		TD_assgin_supp_plan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Assgin_supp_plan2"))).trim()
		TD_assign_Supp_Plan_no2 = convertHSSFCellToString(row.getCell(excelHeader.get("Assign_Supp_Plan_no2"))).trim()
		TD_num_plan_units_sp2 = convertHSSFCellToString(row.getCell(excelHeader.get("num_plan_units_sp2"))).trim()
		TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()
	}

	String fetchTestData(String testCaseId) {

		boolean isDataFound = false
		HSSFSheet sheet = null
		String testd = null
		sheet = initiateExcelConnectionNexia(Constant.TESTCASENAME)

		excelrRowColumnCount = findRowColumnCount(sheet, excelrRowColumnCount)
		int tmp = excelrRowColumnCount.get("RowCount")

		excelHeader = readExcelHeaders(sheet, excelHeader, excelrRowColumnCount)

		HSSFRow row = null
		HSSFCell cell = null
		String temptestCaseId = null;

		for(int r=0 ; r < excelrRowColumnCount.get("RowCount"); r++) {
			row = sheet.getRow(r)
			if(row != null) {
				for(int c=0 ; c < excelrRowColumnCount.get("ColumnCount"); c++) {
					cell = row.getCell(excelHeader.get("TestCaseID"))
					if(cell != null) {
						temptestCaseId = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
						if(temptestCaseId.equals(testCaseId)) {
							isDataFound = true
							if(Constant.TESTCASENAME.toString().equalsIgnoreCase("CreateAccountComplete")) {
								testd = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No")))
								TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()

								TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
								TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
								//							//testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID"))).trim()
								//							clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
								TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
								//masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
								TD_userid = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
								TD_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Status_Code"))).trim()
								TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
								TD_masterPlanUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Units"))).trim()
								TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
								TD_suppPlan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan2"))).trim()
								TD_suppPlan3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan3"))).trim()
								TD_suppPlanUnits1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units1"))).trim()
								TD_suppPlanUnits2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units2"))).trim()
								TD_suppPlanUnits3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units3"))).trim()
								TD_notifyMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Notify_Medhod"))).trim()
								TD_password = convertHSSFCellToString(row.getCell(excelHeader.get("Password"))).trim()
								TD_email = convertHSSFCellToString(row.getCell(excelHeader.get("Email"))).trim()
								TD_payMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Pay_Method"))).trim()
								TD_ccNumber = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Number"))).trim()
								TD_ccExpireMM = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_MM"))).trim()
								TD_ccExpireYYYY = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_YYYY"))).trim()
								TD_collection_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group1"))).trim()
								TD_collection_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group2"))).trim()


								TD_testAcctMethod = convertHSSFCellToString(row.getCell(excelHeader.get("test_acct_ind"))).trim()
								TD_doFullInvoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_full_invoicing"))).trim()
								TD_couponCode1 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code1"))).trim()
								TD_couponCode2 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code2"))).trim()
								TD_couponCode3 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code3"))).trim()

								TD_contract_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_plan_no"))).trim()
								TD_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no"))).trim()
								TD_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months"))).trim()

								TD_doWrite = convertHSSFCellToString(row.getCell(excelHeader.get("do_write"))).trim()
								TD_retroActiveStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Retroactive_Start_Date"))).trim()
								TD_altStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Start_Date"))).trim()
								TD_altBillDay = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Billing_Day"))).trim()
								TD_taxExemptionLevel = convertHSSFCellToString(row.getCell(excelHeader.get("Tax_Exemption_Level"))).trim()
								TD_currency_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Currency_cd"))).trim()

								TD_custom_rate_plan_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1"))).trim()
								TD_cutom_rate_service_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1"))).trim()
								TD_custom_rate_seq_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1"))).trim()
								TD_custom_rate_from_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1"))).trim()
								TD_custom_rate_to_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1"))).trim()
								TD_custom_rate_per_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1"))).trim()

								TD_custom_rate_plan_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2"))).trim()
								TD_cutom_rate_service_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2"))).trim()
								TD_custom_rate_seq_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2"))).trim()
								TD_custom_rate_from_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2"))).trim()
								TD_custom_rate_to_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2"))).trim()
								TD_custom_rate_per_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2"))).trim()

								TD_functional_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group1"))).trim()
								TD_functional_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group2"))).trim()
								TD_do_prorated_invoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_prorated_invoicing"))).trim()

								TD_bank_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("bank_acct_no"))).trim()
								TD_bank_routing_number = convertHSSFCellToString(row.getCell(excelHeader.get("bank_routing_number"))).trim()
								TD_status_until_alt_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("status_until_alt_start_date"))).trim()
								TD_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee"))).trim()

								TD_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date"))).trim()

								TD_firstName = convertHSSFCellToString(row.getCell(excelHeader.get("First_Name"))).trim()
								TD_mi = convertHSSFCellToString(row.getCell(excelHeader.get("MI"))).trim()
								TD_lastName = convertHSSFCellToString(row.getCell(excelHeader.get("Last_Name"))).trim()
								TD_companyName = convertHSSFCellToString(row.getCell(excelHeader.get("Company_Name"))).trim()
								TD_addr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr1"))).trim()
								TD_addr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr2"))).trim()
								TD_city = convertHSSFCellToString(row.getCell(excelHeader.get("City"))).trim()
								TD_state = convertHSSFCellToString(row.getCell(excelHeader.get("State"))).trim()
								TD_locality = convertHSSFCellToString(row.getCell(excelHeader.get("Locality"))).trim()
								TD_country = convertHSSFCellToString(row.getCell(excelHeader.get("Country"))).trim()
								TD_postalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Postal_Code"))).trim()
								TD_phone = convertHSSFCellToString(row.getCell(excelHeader.get("Phone"))).trim()


								TD_billFirstName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_First_Name"))).trim()
								TD_billMi = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_MI"))).trim()
								TD_billLastName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Last_Name"))).trim()
								TD_billCompanyName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Company_Name"))).trim()
								TD_billAddr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr1"))).trim()
								TD_billAddr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr2"))).trim()
								TD_billCity = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_City"))).trim()
								TD_billState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_State"))).trim()
								TD_billLocality = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Locality"))).trim()
								TD_billCountry = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Country"))).trim()
								TD_billPostalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Postal_Code"))).trim()
								TD_billPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone"))).trim()
								TD_billEmail = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Email"))).trim()
								TD_respLevelCd = convertHSSFCellToString(row.getCell(excelHeader.get("Resp_Level_Cd"))).trim()
								TD_qualifierName1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name1"))).trim()
								TD_qualifierName2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name2"))).trim()
								TD_qualifierValue1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value1"))).trim()
								TD_qualifierValue2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value2"))).trim()

								TD_clientAcctId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Acct_Id"))).trim()
								TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Cd"))).trim()
								TD_secretQuestion = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question"))).trim()
								TD_secretQuestionAanswer = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question_Answer"))).trim()
								TD_phoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Phone_Ext"))).trim()
								TD_cellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Cell_Phone"))).trim()
								TD_workPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone"))).trim()
								TD_workPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone_Ext"))).trim()
								TD_birthDate = convertHSSFCellToString(row.getCell(excelHeader.get("Birthdate"))).trim()
								TD_billPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone_Ext"))).trim()
								TD_billCellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Cell_Phone"))).trim()
								TD_billWorkPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone"))).trim()
								TD_billWorkPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone_Ext"))).trim()
								TD_balanceForward = convertHSSFCellToString(row.getCell(excelHeader.get("Balance_Forward"))).trim()
								TD_masterPlanAltRateSchedNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Alt_Rate_Sched_No"))).trim()
								TD_clientReceiptId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
								TD_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("Cvv"))).trim()
								TD_taxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Taxpayer_Id"))).trim()
								TD_billAgreementId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Agreement_Id"))).trim()
								TD_altMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Msg_Template_No"))).trim()
								TD_seqFuncGroupNo = convertHSSFCellToString(row.getCell(excelHeader.get("Seq_Func_Group_No"))).trim()
								TD_bankAcctType = convertHSSFCellToString(row.getCell(excelHeader.get("Bank_Acct_Type"))).trim()
								TD_billDriversLicenseNo = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_No"))).trim()
								TD_billDriversLicenseState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_State"))).trim()
								TD_billTaxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Taxpayer_Id"))).trim()
								TD_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("Address3"))).trim()
								TD_billAddress3 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Address3"))).trim()
								TD_trackData1 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data1"))).trim()
								TD_trackData2 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data2"))).trim()
								TD_cnAltMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Cn_Alt_Msg_Template_No"))).trim()
								TD_altCallerId = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()
								TD_Advance_VirtualTime = convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).trim()
								TD_VerifyDate = convertHSSFCellToString(row.getCell(excelHeader.get("Verify_Date"))).trim()
								//TD_Bug_ID = convertHSSFCellToString(row.getCell(excelHeader.get("Bug_ID"))).trim()
								TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()
								break;

							} else if(Constant.TESTCASENAME.toString().equalsIgnoreCase("CancelSuppPlan")) {

								testd = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No")))
								TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()

								TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
								TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
								TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
								TD_userid = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
								TD_status_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Status_Code"))).trim()
								TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan"))).trim()
								TD_masterPlanUnits = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Units"))).trim()
								TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
								TD_suppPlan2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan2"))).trim()
								TD_suppPlan3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan3"))).trim()
								TD_suppPlanUnits1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units1"))).trim()
								TD_suppPlanUnits2 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units2"))).trim()
								TD_suppPlanUnits3 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan_Units3"))).trim()
								TD_notifyMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Notify_Medhod"))).trim()
								TD_password = convertHSSFCellToString(row.getCell(excelHeader.get("Password"))).trim()
								TD_email = convertHSSFCellToString(row.getCell(excelHeader.get("Email"))).trim()
								TD_payMethod = convertHSSFCellToString(row.getCell(excelHeader.get("Pay_Method"))).trim()
								TD_ccNumber = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Number"))).trim()
								TD_ccExpireMM = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_MM"))).trim()
								TD_ccExpireYYYY = convertHSSFCellToString(row.getCell(excelHeader.get("CC_Expire_YYYY"))).trim()
								TD_collection_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group1"))).trim()
								TD_collection_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("collection_acct_group2"))).trim()

								TD_serviceOrder = convertHSSFCellToString(row.getCell(excelHeader.get("Service_Order"))).trim()

								TD_testAcctMethod = convertHSSFCellToString(row.getCell(excelHeader.get("test_acct_ind"))).trim()
								TD_doFullInvoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_full_invoicing"))).trim()
								TD_couponCode1 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code1"))).trim()
								TD_couponCode2 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code2"))).trim()
								TD_couponCode3 = convertHSSFCellToString(row.getCell(excelHeader.get("Coupon_Code3"))).trim()

								TD_contract_plan_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_plan_no"))).trim()
								TD_contract_type_no = convertHSSFCellToString(row.getCell(excelHeader.get("contract_type_no"))).trim()
								TD_contract_length_months = convertHSSFCellToString(row.getCell(excelHeader.get("contract_length_months"))).trim()

								TD_doWrite = convertHSSFCellToString(row.getCell(excelHeader.get("do_write"))).trim()
								TD_retroActiveStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Retroactive_Start_Date"))).trim()
								TD_altStartDate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Start_Date"))).trim()
								TD_altBillDay = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Billing_Day"))).trim()
								TD_taxExemptionLevel = convertHSSFCellToString(row.getCell(excelHeader.get("Tax_Exemption_Level"))).trim()
								TD_currency_cd = convertHSSFCellToString(row.getCell(excelHeader.get("Currency_cd"))).trim()

								TD_custom_rate_plan_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_1"))).trim()
								TD_cutom_rate_service_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_1"))).trim()
								TD_custom_rate_seq_no_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_1"))).trim()
								TD_custom_rate_from_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_1"))).trim()
								TD_custom_rate_to_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_1"))).trim()
								TD_custom_rate_per_unit_1 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_1"))).trim()

								TD_custom_rate_plan_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_plan_no_2"))).trim()
								TD_cutom_rate_service_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("cutom_rate_service_no_2"))).trim()
								TD_custom_rate_seq_no_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_seq_no_2"))).trim()
								TD_custom_rate_from_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_from_unit_2"))).trim()
								TD_custom_rate_to_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_to_unit_2"))).trim()
								TD_custom_rate_per_unit_2 = convertHSSFCellToString(row.getCell(excelHeader.get("custom_rate_per_unit_2"))).trim()

								TD_functional_acct_group1 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group1"))).trim()
								TD_functional_acct_group2 = convertHSSFCellToString(row.getCell(excelHeader.get("functional_acct_group2"))).trim()
								TD_do_prorated_invoicing = convertHSSFCellToString(row.getCell(excelHeader.get("do_prorated_invoicing"))).trim()

								TD_bank_acct_no = convertHSSFCellToString(row.getCell(excelHeader.get("bank_acct_no"))).trim()
								TD_bank_routing_number = convertHSSFCellToString(row.getCell(excelHeader.get("bank_routing_number"))).trim()
								TD_status_until_alt_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("status_until_alt_start_date"))).trim()
								TD_contract_alt_recur_fee = convertHSSFCellToString(row.getCell(excelHeader.get("contract_alt_recur_fee"))).trim()

								TD_contract_start_date = convertHSSFCellToString(row.getCell(excelHeader.get("contract_start_date"))).trim()

								TD_firstName = convertHSSFCellToString(row.getCell(excelHeader.get("First_Name"))).trim()
								TD_mi = convertHSSFCellToString(row.getCell(excelHeader.get("MI"))).trim()
								TD_lastName = convertHSSFCellToString(row.getCell(excelHeader.get("Last_Name"))).trim()
								TD_companyName = convertHSSFCellToString(row.getCell(excelHeader.get("Company_Name"))).trim()
								TD_addr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr1"))).trim()
								TD_addr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Addr2"))).trim()
								TD_city = convertHSSFCellToString(row.getCell(excelHeader.get("City"))).trim()
								TD_state = convertHSSFCellToString(row.getCell(excelHeader.get("State"))).trim()
								TD_locality = convertHSSFCellToString(row.getCell(excelHeader.get("Locality"))).trim()
								TD_country = convertHSSFCellToString(row.getCell(excelHeader.get("Country"))).trim()
								TD_postalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Postal_Code"))).trim()
								TD_phone = convertHSSFCellToString(row.getCell(excelHeader.get("Phone"))).trim()

								TD_billFirstName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_First_Name"))).trim()
								TD_billMi = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_MI"))).trim()
								TD_billLastName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Last_Name"))).trim()
								TD_billCompanyName = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Company_Name"))).trim()
								TD_billAddr1 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr1"))).trim()
								TD_billAddr2 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Addr2"))).trim()
								TD_billCity = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_City"))).trim()
								TD_billState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_State"))).trim()
								TD_billLocality = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Locality"))).trim()
								TD_billCountry = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Country"))).trim()
								TD_billPostalCode = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Postal_Code"))).trim()
								TD_billPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone"))).trim()
								TD_billEmail = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Email"))).trim()
								TD_respLevelCd = convertHSSFCellToString(row.getCell(excelHeader.get("Resp_Level_Cd"))).trim()
								TD_qualifierName1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name1"))).trim()
								TD_qualifierName2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Name2"))).trim()
								TD_qualifierValue1 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value1"))).trim()
								TD_qualifierValue2 = convertHSSFCellToString(row.getCell(excelHeader.get("Qualifier_Value2"))).trim()

								TD_clientAcctId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Acct_Id"))).trim()
								TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Cd"))).trim()
								TD_secretQuestion = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question"))).trim()
								TD_secretQuestionAanswer = convertHSSFCellToString(row.getCell(excelHeader.get("Secret_Question_Answer"))).trim()
								TD_phoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Phone_Ext"))).trim()
								TD_cellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Cell_Phone"))).trim()
								TD_workPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone"))).trim()
								TD_workPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Work_Phone_Ext"))).trim()
								TD_birthDate = convertHSSFCellToString(row.getCell(excelHeader.get("Birthdate"))).trim()
								TD_billPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Phone_Ext"))).trim()
								TD_billCellPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Cell_Phone"))).trim()
								TD_billWorkPhone = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone"))).trim()
								TD_billWorkPhoneExt = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Work_Phone_Ext"))).trim()
								TD_balanceForward = convertHSSFCellToString(row.getCell(excelHeader.get("Balance_Forward"))).trim()
								TD_masterPlanAltRateSchedNo = convertHSSFCellToString(row.getCell(excelHeader.get("Master_Plan_Alt_Rate_Sched_No"))).trim()
								TD_clientReceiptId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
								TD_cvv = convertHSSFCellToString(row.getCell(excelHeader.get("Cvv"))).trim()
								TD_taxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Taxpayer_Id"))).trim()
								TD_billAgreementId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Agreement_Id"))).trim()
								TD_altMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Msg_Template_No"))).trim()
								TD_seqFuncGroupNo = convertHSSFCellToString(row.getCell(excelHeader.get("Seq_Func_Group_No"))).trim()
								TD_bankAcctType = convertHSSFCellToString(row.getCell(excelHeader.get("Bank_Acct_Type"))).trim()
								TD_billDriversLicenseNo = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_No"))).trim()
								TD_billDriversLicenseState = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Drivers_License_State"))).trim()
								TD_billTaxpayerId = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Taxpayer_Id"))).trim()
								TD_address3 = convertHSSFCellToString(row.getCell(excelHeader.get("Address3"))).trim()
								TD_billAddress3 = convertHSSFCellToString(row.getCell(excelHeader.get("Bill_Address3"))).trim()
								TD_trackData1 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data1"))).trim()
								TD_trackData2 = convertHSSFCellToString(row.getCell(excelHeader.get("Track_Data2"))).trim()
								TD_cnAltMsgTemplateNo = convertHSSFCellToString(row.getCell(excelHeader.get("Cn_Alt_Msg_Template_No"))).trim()
								TD_altCallerId = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()
								TD_effectiveDate = convertHSSFCellToString(row.getCell(excelHeader.get("Effective_Date"))).trim()
								TD_cancelSuppPlan = convertHSSFCellToString(row.getCell(excelHeader.get("Cancel_Supp_Plan"))).trim()
								TD_doWriteCancel = convertHSSFCellToString(row.getCell(excelHeader.get("Do_write"))).trim()
								TD_commentsCancel = convertHSSFCellToString(row.getCell(excelHeader.get("Comments"))).trim()
								TD_clientReceipentId = convertHSSFCellToString(row.getCell(excelHeader.get("Client_Receipt_Id"))).trim()
								TD_altProrationRate = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Proration_Rate"))).trim()
								TD_altCallerIdCancel = convertHSSFCellToString(row.getCell(excelHeader.get("Alt_Caller_Id"))).trim()
								TD_altCallerIdCancel = convertHSSFCellToString(row.getCell(excelHeader.get("Effective_Date"))).trim()
								TD_effectiveDate = convertHSSFCellToString(row.getCell(excelHeader.get("Effective_Date"))).trim()
								TD_assignmentdirective = convertHSSFCellToString(row.getCell(excelHeader.get("AD"))).trim()
								TD_cancel_sp_imm = convertHSSFCellToString(row.getCell(excelHeader.get("cancel_sp_imm"))).trim()
								TD_VerifyDate = convertHSSFCellToString(row.getCell(excelHeader.get("Verify_Date"))).trim()
								TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()
								break;

							} else if(Constant.TESTCASENAME.toString().equalsIgnoreCase("GetClientPlansAll")) {

								testd = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No")))
								TD_clientNo = convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).trim()
								TD_testCaseID = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseID")))
								TD_testCaseDesc = convertHSSFCellToString(row.getCell(excelHeader.get("TestCaseDesc")))
								TD_authKey = convertHSSFCellToString(row.getCell(excelHeader.get("Auth_Key"))).trim()
								TD_acctNo = convertHSSFCellToString(row.getCell(excelHeader.get("Account_No"))).trim()
								TD_promoCd = convertHSSFCellToString(row.getCell(excelHeader.get("Promo_Code"))).trim()
								TD_masterPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Plan_No"))).trim()
								TD_parentPlanNo = convertHSSFCellToString(row.getCell(excelHeader.get("Parent_Plan_No"))).trim()
								TD_includeAllRateSchedules = convertHSSFCellToString(row.getCell(excelHeader.get("Include_all_Rate_Schedules"))).trim()
								TD_includePlanHierarchy = convertHSSFCellToString(row.getCell(excelHeader.get("Include_Plan_Hierarchy"))).trim()
								TD_suppFieldNames = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Field_Names"))).trim()
								TD_suppFieldValues = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Field_Values"))).trim()
								TD_suppPlan1 = convertHSSFCellToString(row.getCell(excelHeader.get("Supp_Plan1"))).trim()
								TD_TestLinkID = convertHSSFCellToString(row.getCell(excelHeader.get("TestLinkID"))).trim()
								break;
							} else if(Constant.TESTCASENAME.toString().equalsIgnoreCase("AssignSuppPlan")) {
								testDataAssignSuppPlan(row)
								break;
							} else if (Constant.TESTCASENAME.toString().equalsIgnoreCase("ModifySuppPlan")) {
								testDataModifySuppPlan(row)
								break
							} else if (Constant.TESTCASENAME.toString().equalsIgnoreCase("AssignSuppPlanMulti")) {
								testDataAssignSuppPlanMulti(row)
								break
							} //Integration Case
							else if (Constant.TESTCASENAME.toString().equalsIgnoreCase("Integration"))
							{
								testDataIntegration(row)
								break;
							}

							//update_master_Plan
							else if (Constant.TESTCASENAME.toString().equalsIgnoreCase("UpdateMasterPlan"))
							{
								testDataUpdateMasterPlan(row)
								break;
							}
							else if (Constant.TESTCASENAME.toString().equalsIgnoreCase("ModifySuppPlan")) {
								testDataModifySuppPlan(row)
								break
							}

							else if (Constant.TESTCASENAME.toString().equalsIgnoreCase("ReplaceSuppPlan")) {
								testDataReplaceSuppPlan(row)
								break
							} else if (Constant.TESTCASENAME.toString().equalsIgnoreCase("IntegrationBug1664")) {
								testDataIntegrationBug1664(row)
							} else if (Constant.TESTCASENAME.toString().equalsIgnoreCase("UpdateAccountComplete"))
							{
								testDataUpdateAcctComplete(row)
								break;
							} else if(Constant.TESTCASENAME.toString().equalsIgnoreCase("Integration4"))
							{
								testDataIntegration4(row)
							}//create_acct_complete, update_acct_status,assign_supp_plan
							else if (Constant.TESTCASENAME.toString().equalsIgnoreCase("Integration"))
							{
								testDataIntegration(row)
								break;
							}

							//Ram Integration2 Case
							else if (Constant.TESTCASENAME.toString().equalsIgnoreCase("Integration2"))
							{
								testDataIntegration2(row)
								break;
							}
							//Ram Integration3 Case
							else if (Constant.TESTCASENAME.toString().equalsIgnoreCase("Integration3"))
							{
								testDataIntegration2(row)
								break;
							}
							else if (Constant.TESTCASENAME.toString().equalsIgnoreCase("IntegrationBug1664")) {
								testDataIntegrationBug1664(row)
							}
						}
					}
				}
			}
		}
		return testd
	}


	/**
	 * Constructors for the class
	 */
	public def ReadData(){
		logi.logInfo("PROJECTLOCATION: "+Constant.PROJECTLOCATION)
		sourceFile = Constant.PROJECTLOCATION + 'ResponseVerification.xls'
		logi.logInfo("sourceFile: "+sourceFile.toString())
		inStr = new FileInputStream(sourceFile)
		workbook = new HSSFWorkbook(inStr)
		library.Constants.Constant.paramActual.clear();
		library.Constants.Constant.paramExpected.clear();
		library.Constants.Constant.seq.clear();
		library.Constants.Constant.seq2.clear();
		library.Constants.Constant.UniqueID.clear();
	}

	//public def ReadData (def file) {
	//sourceFile = file
	//inStr = new FileInputStream(file)
	//workbook = new HSSFWorkbook(inStr)
	//}

	public def ReadData (def file) {
		sourceFile = file
		inStr = new FileInputStream(Constant.PROJECTLOCATION+ file+'.xls')
		workbook = new HSSFWorkbook(inStr)
	}


	/**
	 * Get the list of value of a single column(from response verification file of a test suite)
	 * @param columnName name of the column from which the data is to be retrieved
	 * @return list of column data
	 * @author vasanth.manickam
	 */
	List getColumnList(String columnName) {
		String dataName = 'null'
		List dataList = []
		int colIndex=-1
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)

		// Identify the column index
		HSSFRow row1 = sheet.getRow(0)
		Iterator<HSSFCell> cells = row1.cellIterator()
		while(cells.hasNext()) {
			HSSFCell cell = cells.next()
			if(getCellValue(cell).equalsIgnoreCase(columnName)){
				colIndex = cell.getColumnIndex()
			}
		}

		// Iterate through all the rows to fetch data
		if (colIndex != -1)
		{
		Iterator<HSSFRow> rows = sheet.rowIterator();
		HSSFRow row = rows.next ();
		row = rows.next ();
		dataName = row.getCell(colIndex).toString()
		dataList.add(dataName)
		while (rows.hasNext ())	{
			row = rows.next ();
			String rowData = row.getCell(colIndex).toString()
			// if the cell is empty with no data, have the last know value as the data and append to the list
			if(rowData.toString().equals("")) {
				dataList.add(dataName)
			} else {
				dataName = rowData
				dataList.add(dataName)
			}
		}
		}
		return dataList
	}
	
	List getColumnVerificationList(String columnName) {
		String dataName = 'null'
		List dataList = []
		int colIndex=-1
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)

		// Identify the column index
		HSSFRow row1 = sheet.getRow(0)
		Iterator<HSSFCell> cells = row1.cellIterator()
		while(cells.hasNext()) {
			HSSFCell cell = cells.next()
			if(getCellValue(cell).equalsIgnoreCase(columnName)){
				colIndex = cell.getColumnIndex()
			}
		}

		// Iterate through all the rows to fetch data
		if (colIndex != -1)
		{
		Iterator<HSSFRow> rows = sheet.rowIterator();
		HSSFRow row = rows.next ();
		row = rows.next ();
		dataName = row.getCell(colIndex).toString()
		dataList.add(dataName)
		while (rows.hasNext ())	{
			row = rows.next ();
			String rowData = row.getCell(colIndex).toString()
			// if the cell is empty with no data, have the last know value as the data and append to the list
			if(rowData.toString().equals("")) {
				dataList.add("")
			} else {
				dataName = rowData
				dataList.add("<font color=\"RoyalBlue\"><i><b>" + dataName + "</b></i><br></font>")
			}
		}
		}
		return dataList
	}

	/**
	 * Get the verification data for the particular test step of the test case
	 * @param testCaseName Name of the test case
	 * @param testStepName Name of the test step under the test case
	 * @return Response data map(xPath as keys and the expected data as value of the map)
	 * @author vasanth.manickam
	 */
	LinkedHashMap getVerificationData(String testCaseName, String testStepName) {
		logi.logInfo "Calling getVerificationData => "+testCaseName+", " +testStepName

		if (Constant.tcNameConstant.size() == 0)
		{
		tcName = getColumnList('TestCase')
		tsName = getColumnList('TestStep')
		xPath = getColumnList('Node_XPath')
		expValue = getColumnList('Expected_Value')
		Constant.tcNameConstant = tcName
		}
		
		LinkedHashMap responseMap = new LinkedHashMap();
		def objxPath = new ReadData()
		objxPath.fetchTestData(testCaseName);
		
		for(int i=0; i<tcName.size(); i++) {
			if(tcName.get(i).toString().equalsIgnoreCase(testCaseName) && tsName.get(i).toString().equalsIgnoreCase(testStepName)){

				// Get the equivalent xPath from the constant groovy file
				String xPathData
				logi.logInfo "xpath data before if" + xPath
				if(xPath.get(i).toString().contains("DB") || xPath.get(i).toString().contains("md")) {
				xPathData = getExpValue(xPath.get(i).toString())
					
				} else if (xPath.get(i).toString().contains("TD")) {
					
					for(Field field : objxPath.getClass().getFields()) {
						if(field.getName().toString().equalsIgnoreCase(xPath.get(i).toString())) {
							Field fieldName = objxPath.getClass().getDeclaredField(field.getName().toString());
							xPathData = "TD" + i + "_" + fieldName.get(objxPath)
							break
						}
					}
				} else {
					xPathData = getXPath(xPath.get(i).toString())
				}

				// Get the expected value
				if(expValue.get(i).toString().contains("DB") || expValue.get(i).toString().contains("md")) {
					String query = getExpValue(expValue.get(i).toString())
					responseMap.put xPathData, query
				} else if (xPath.get(i).toString().contains("TD")) {

					for(Field field : objxPath.getClass().getFields()) {
						if(field.getName().toString().equalsIgnoreCase(xPath.get(i).toString())) {
							Field fieldName = objxPath.getClass().getDeclaredField(field.getName().toString());
							xPathData = fieldName.get(objxPath)
							break
						}
					}
				} else {
					responseMap.put xPathData, expValue.get(i).toString()
				}
			}
		}

		LinkedHashMap<String, com.eviware.soapui.support.XmlHolder> tmp = new LinkedHashMap()
		logi.logInfo("::: RESPONSE VERIFICATION DATA MAP :::")
		logi.logInfo("Test Case: " + testCaseName)
		logi.logInfo(responseMap.toString());
		return responseMap
	}

	LinkedHashMap getHttpVerificationData(String testCaseName, String testStepName) {
		List tcName = getColumnList('TestCase')
		List tsName = getColumnList('TestStep')
		List xPath = getColumnList('Node_XPath')
		List expValue = getColumnList('Expected_Value')
		LinkedHashMap responseMap = new LinkedHashMap();
		for(int i=0; i<tcName.size(); i++) {
			if(tcName.get(i).toString().equalsIgnoreCase(testCaseName) && tsName.get(i).toString().equalsIgnoreCase(testStepName)){

				// Get the equivalent xPath from the constant groovy file
				String xPathData = getHttpXPath(xPath.get(i).toString())
				responseMap.put xPathData, expValue.get(i).toString()
			}
		}
		return responseMap
	}

	LinkedHashMap getClientNoVirtualTimes() {

		LinkedHashMap clientNoAndVirtualTime = new LinkedHashMap()
		List uniqueClientNo = getClientList()

		for(int i=0 ; i<uniqueClientNo.size() ; i++) {
			String clientNo = uniqueClientNo.getAt(i)
			clientNoAndVirtualTime.put clientNo, getHighestAndLowestVirtualTime(clientNo)
		}

		return clientNoAndVirtualTime
	}



	void replaceXpathNameSpace() {
		String dataName = 'null'
		List dataList = []
		int colIndex
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)

		// Identify the column index
		HSSFRow row1 = sheet.getRow(0)
		Iterator<HSSFCell> cells = row1.cellIterator()
		while(cells.hasNext()) {
			HSSFCell cell = cells.next()
			if(getCellValue(cell).equalsIgnoreCase("Expected_Value")){
				colIndex = cell.getColumnIndex()
			}
		}

		// Iterate through all the rows to fetch data
		Iterator<HSSFRow> rows = sheet.rowIterator();
		HSSFRow row = rows.next ();

		while (rows.hasNext ())	{
			row = rows.next ();
			String rowData = row.getCell(colIndex).toString()
			if(rowData.contains("~namespace~")) {
				rowData = rowData.replace("~namespace~", Constant.NAMESPACE)
				row.getCell(colIndex).setCellValue(rowData)
			}
		}

		FileOutputStream fileOut = new FileOutputStream(sourceFile);
		workbook.write(fileOut);
		fileOut.close();
	}

	/**
	 * Get the cell value based on the cell type
	 * @param cell HSSFCell cell for which value is to be retrieved 
	 * @return cell value
	 * @author vasanth.manickam
	 */
	String getCellValue(HSSFCell cell) {
		switch ( cell.getCellType() ) {
			case HSSFCell.CELL_TYPE_NUMERIC:
				cell.getNumericCellValue().toString()
				break;
			case HSSFCell.CELL_TYPE_STRING:
				cell.getStringCellValue().toString()
				break;
		}
	}

	/**
	 * Get all the test cases that has to be executed
	 * @return List of test case name
	 * @author vasanth.manickam
	 */
	List getTCSelection() {
		def inFile = Constant.PROJECTLOCATION + 'TestCase_Selection.xls'

		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listTestCaseName = []
		while (rows.hasNext ())	{

			row = rows.next ();
			if(row.getCell(1).toString().toUpperCase() != "NO"){
				listTestCaseName.add(row.getCell(0).toString())
			}
		}
		inStr.close()
		return listTestCaseName
	}

	List getTCSelectionVT() {
		def inFile = Constant.PROJECTLOCATION + 'TestCase_Selection.xls'

		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listTestCaseName = []
		while (rows.hasNext ())	{

			row = rows.next ();
			if(row.getCell(1).toString().toUpperCase() != "NO" && row.getCell(excelHeader.get("VirtualTime_In_Months")).toString().trim() != "0" ){
				listTestCaseName.add(row.getCell(0).toString())
			}
		}
		inStr.close()
		return listTestCaseName
	}



	List getTCSelectionEffectiveVT() {
		def inFile = Constant.PROJECTLOCATION + 'TestCase_Selection.xls'

		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listTestCaseName = []
		while (rows.hasNext ())	{

			row = rows.next ();
			if(row.getCell(1).toString().toUpperCase() != "NO" && row.getCell(excelHeader.get("ump_effective_date")).toString().trim() != "" ){
				listTestCaseName.add(row.getCell(0).toString())
			}
		}
		inStr.close()
		return listTestCaseName
	}

	List getTCSelectionOnedayVT() {
		def inFile = Constant.PROJECTLOCATION + 'TestCase_Selection.xls'

		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listTestCaseName = []
		while (rows.hasNext ())	{

			row = rows.next ();
			if(row.getCell(1).toString().toUpperCase() != "NO" && row.getCell(excelHeader.get("ump_one_day_advance")).toString().trim().toUpperCase() == "YES" ){
				listTestCaseName.add(row.getCell(0).toString())
			}
		}
		inStr.close()
		return listTestCaseName
	}

	List getTCVT() {
		def inFile = Constant.PROJECTLOCATION + 'TestCase_Selection.xls'

		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listTestCaseName = []
		while (rows.hasNext ())	{

			row = rows.next ();
			if((row.getCell(excelHeader.get("set_acct_usg_mtd_threshold_Apply")).toString().toUpperCase() == "YES" && row.getCell(excelHeader.get("set_acct_usg_mtd_threshold_imm")).toString().toUpperCase() == "NO")||(row.getCell(excelHeader.get("set_acct_usg_ptd_threshold_Apply")).toString().toUpperCase() == "YES" && row.getCell(excelHeader.get("set_acct_usg_ptd_threshold_imm")).toString().toUpperCase() == "NO")||(row.getCell(excelHeader.get("record_usage_apply")).toString().toUpperCase() == "YES" && row.getCell(excelHeader.get("record_usage_imm")).toString().toUpperCase() == "NO")||(row.getCell(excelHeader.get("gen_invoice_apply")).toString().toUpperCase() == "YES" && row.getCell(excelHeader.get("gen_invoice_imm")).toString().toUpperCase() == "NO") ){
				listTestCaseName.add(row.getCell(0).toString())
			}
		}
		inStr.close()
		return listTestCaseName
	}



	String getTCSkippedCount() {
		def inFile = Constant.PROJECTLOCATION + 'TestCase_Selection.xls'

		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listTestCaseName = []
		while (rows.hasNext ())	{

			row = rows.next ();
			if(row.getCell(1).toString().toUpperCase() != "YES"){
				listTestCaseName.add(row.getCell(0).toString())
			}
		}
		inStr.close()
		return listTestCaseName.size()
	}

	/**
	 * Get all the test cases that has to be executed
	 * @return List of test case name
	 * @author vasanth.manickam
	 */
	List getClientList() {
		def inFile = Constant.PROJECTLOCATION + 'TestCase_Selection.xls'

		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)

		excelrRowColumnCount = findRowColumnCount(sheet, excelrRowColumnCount)
		int tmp = excelrRowColumnCount.get("RowCount")

		excelHeader = readExcelHeaders(sheet, excelHeader, excelrRowColumnCount)

		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listClientName = []
		while (rows.hasNext ())	{

			row = rows.next ();
			if(row.getCell(1).toString().toUpperCase() != "NO"){
				listClientName.add(convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).toString())
			}
		}

		Set s = new HashSet(listClientName);
		List listClientNames = new ArrayList(s);
		inStr.close()
		return listClientNames
	}

	def getHighestAndLowestVirtualTime(String clientNo) {
		def inFile = Constant.PROJECTLOCATION + 'TestCase_Selection.xls'

		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)

		excelrRowColumnCount = findRowColumnCount(sheet, excelrRowColumnCount)
		int tmp = excelrRowColumnCount.get("RowCount")

		excelHeader = readExcelHeaders(sheet, excelHeader, excelrRowColumnCount)

		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def virtualTime = []
		while (rows.hasNext ())	{

			row = rows.next ();
			if(row.getCell(1).toString().toUpperCase() != "NO" && convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).toString() == clientNo){
				String value = convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).toString()
				if(value != "0") {
					virtualTime.add(convertHSSFCellToString(row.getCell(excelHeader.get("VirtualTime_In_Months"))).toString())
				}
			}
		}

		def highestVirtualTime = ""
		def smallestVirtualTime = ""

		if(virtualTime.size() != 0) {
			highestVirtualTime = Common.findLargest(virtualTime)
			
			smallestVirtualTime = Common.findSmallest(virtualTime)
		}
		def virTime = smallestVirtualTime + ":" + highestVirtualTime

		return virTime
	}

	List getTestCaseList() {

		int tcCount = 0
		def inFile = Constant.PROJECTLOCATION + 'TestCase_Selection.xls'

		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listTestCaseName = []
		while (rows.hasNext ())	{
			row = rows.next ();
			listTestCaseName.add(row.getCell(0).toString())
		}
		inStr.close()
		return listTestCaseName
	}

	String getTestData() {
		def inFile = Constant.PROJECTLOCATION + 'TestCase_Selection.xls'

		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listTestCaseName = []
		while (rows.hasNext ())	{

			row = rows.next ();
			if(row.getCell(1).toString().toUpperCase() != "NO"){
				listTestCaseName.add(row.getCell(0).toString())
			}
		}
		inStr.close()
		return listTestCaseName
	}

	/**
	 * Get the respective xPath from the constants defined in 'ExPath.groovy'
	 * @param constNameReference reference name for which the respective xPath should be returned
	 * @return equivalent xPath as per defined in 'ExPath.groovy'
	 * @author vasanth.manickam GET_SERVICE_ERRORCODE1
	 */
	String getXPath(String constNameReference) {
		def objxPath
		String xPath

		if(Constant.WSDLDEFINITION.contains(Constant.WSDLTYPE_DOCLITERAL) || (Constant.WSDLDEFINITION.contains(Constant.WSDLTYPE_DOCLITERALWRAPPED))) {
			objxPath = new ExPathDoc()
		} else if (Constant.WSDLDEFINITION.contains(Constant.WSDL_RPCLITERAL)) {
			objxPath = new ExPathRpc()
		} else if (Constant.WSDLDEFINITION.contains(Constant.WSDLTYPE_RPCENCODING)) {
			objxPath = new ExPathRpcEnc()
		} 

		//		if(constNameReference.contains('DB')) {
		//			objxPath = new DBQueries()
		//		} else {
		//			xPath = 'Noqueryfound'
		//		}

		for(Field field : objxPath.getClass().getFields()) {
			if(field.getName().toString().equalsIgnoreCase(constNameReference)) {
				Field fieldName = objxPath.getClass().getDeclaredField(field.getName().toString());
				xPath = fieldName.get(objxPath)
				break
			}
		}
		return xPath
	}
	
	String getXPath_UI(String constNameReference) {
		def objxPath
		String xPath

		objxPath = new ExPathUI()

		for(Field field : objxPath.getClass().getFields()) {
			if(field.getName().toString().equalsIgnoreCase(constNameReference)) {
				Field fieldName = objxPath.getClass().getDeclaredField(field.getName().toString());
				xPath = fieldName.get(objxPath)
				break
			}
		}
		return xPath
	}

	String getExpValue(String constNameReference) {
		def objxPath
		String query

		if(constNameReference.contains('DB') || constNameReference.contains('md') || constNameReference.contains('uv') || constNameReference.contains('auth_')) {
			objxPath = new ExpValueConstants()
		} else {
			query = 'Noqueryfound'
		}

		for(Field field : objxPath.getClass().getFields()) {
			if(field.getName().toString().equalsIgnoreCase(constNameReference)) {
				Field fieldName = objxPath.getClass().getDeclaredField(field.getName().toString());
				query = fieldName.get(objxPath)
				break
			}
		}
		return query
	}
	/**
	 * Get the respective xPath from the constants defined in 'ExPathAdminToolsApi.groovy'
	 * @param constNameReference reference name for which the respective xPath should be returned
	 * @return equivalent xPath as per defined in 'ExPathAdminToolsApi.groovy'
	 * @author Shital Hapani
	 */
	String getHttpXPath(String constNameReference) {
		def objxPath
		String xPath

		objxPath = new ExPathAdminToolsApi()

		for(Field field : objxPath.getClass().getFields()) {
			if(field.getName().toString().equalsIgnoreCase(constNameReference)) {
				Field fieldName = objxPath.getClass().getDeclaredField(field.getName().toString());
				xPath = fieldName.get(objxPath)
				break
			}
		}
		return xPath
	}

	/*String connectTest() {
	 ConnectDBTest db = new ConnectDBTest()
	 db.createConnectionTest()
	 return "One"
	 }*/

	//********************//

	List getClientListP2() {
		def inFile = Constant.PROJECTLOCATION + Constant.TESTDATABOOK + '.xls'
		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)

		excelrRowColumnCount = findRowColumnCount(sheet, excelrRowColumnCount)
		int tmp = excelrRowColumnCount.get("RowCount")

		excelHeader = readExcelHeaders(sheet, excelHeader, excelrRowColumnCount)

		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listClientName = []
		while (rows.hasNext ()) {

			row = rows.next ();
			Constant.lhmClientList.put(row.getCell(0).toString(),convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).toString())
			if(row.getCell(1).toString().toUpperCase() != "NO"){
				listClientName.add(convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).toString())
			}
		}

		Set s = new HashSet(listClientName);
		List listClientNames = new ArrayList(s);
		inStr.close()
		if (listClientName.size() == 0) throw new Exception("Enable the TestCase(Yes/No) with client details.... :(")
		return listClientNames
	}

	List getClientListBySetNoP2(String setNo) {
		def inFile = Constant.PROJECTLOCATION + Constant.TESTDATABOOK + '.xls'
		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)

		excelrRowColumnCount = findRowColumnCount(sheet, excelrRowColumnCount)
		int tmp = excelrRowColumnCount.get("RowCount")

		excelHeader = readExcelHeaders(sheet, excelHeader, excelrRowColumnCount)

		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listClientName = []
		while (rows.hasNext ()) {

			row = rows.next ();
			if((row.getCell(1).toString().toUpperCase() != "NO")&&(row.getCell(6).toString().equalsIgnoreCase(setNo))){
				listClientName.add(convertHSSFCellToString(row.getCell(excelHeader.get("Client_No"))).toString())
			}
		}

		Set s = new HashSet(listClientName);
		List listClientNames = new ArrayList(s);
		inStr.close()
		return listClientNames
	}
	
	List getSetListP2() {
		def inFile = Constant.PROJECTLOCATION + Constant.TESTDATABOOK + '.xls'
		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)

		excelrRowColumnCount = findRowColumnCount(sheet, excelrRowColumnCount)
		int tmp = excelrRowColumnCount.get("RowCount")

		excelHeader = readExcelHeaders(sheet, excelHeader, excelrRowColumnCount)

		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def setNos = []
		while (rows.hasNext ()) {

			row = rows.next ();
			if(row.getCell(1).toString().toUpperCase() != "NO"){
				setNos.add(convertHSSFCellToString(row.getCell(excelHeader.get("Set_No"))).toString())
			}
		}

		Set s = new HashSet(setNos);
		List setNosList = new ArrayList(s);
		inStr.close()
		return setNosList
	}

	List getTCSelectionP2() {
		def inFile = Constant.PROJECTLOCATION + Constant.TESTDATABOOK + '.xls'
		logi.logInfo(inFile);
		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		logi.logInfo("Processing Sheet/API: "+sheet.getSheetName());
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listTestCaseName = []
		while (rows.hasNext ())                {

			row = rows.next ();
			if(row.getCell(1).toString().toUpperCase() == "YES"){
				listTestCaseName.add(row.getCell(0).toString())
			}
		}
		inStr.close()
		if (listTestCaseName.size() == 0) throw new Exception("Enable the TestCase(Yes/No).... :(")
		return listTestCaseName
	}
	

	List getTCSelectionByClientP2(String clientNo) {

		def inFile = Constant.PROJECTLOCATION + Constant.TESTDATABOOK + '.xls'
		logi.logInfo(inFile);
		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		logi.logInfo("Processing Sheet/API: "+sheet.getSheetName());
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listTestCaseName = []
		while (rows.hasNext ()) {

			row = rows.next ();
			if((row.getCell(1).toString().toUpperCase() == "YES")&& (row.getCell(2).toString().equalsIgnoreCase(clientNo))){
				listTestCaseName.add(row.getCell(0).toString())
			}
		}
		inStr.close()
		if (listTestCaseName.size() == 0) throw new Exception("No Client Items Present in the Enabled cases.... :(")
		return listTestCaseName
	}

	List getTCSelectionByClientAndSetP2(String clientNo, String setNo) {
		
				def inFile = Constant.PROJECTLOCATION + Constant.TESTDATABOOK + '.xls'
				logi.logInfo(inFile);
				InputStream inStr = new FileInputStream(inFile)
				HSSFWorkbook workbook = new HSSFWorkbook(inStr)
				HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
				logi.logInfo("Processing Sheet/API: "+sheet.getSheetName());
				HSSFRow headerRow = sheet.getRow(0)
				Iterator<HSSFRow> rows = sheet.rowIterator ();
				HSSFRow row = rows.next();
				def listTestCaseName = []
				while (rows.hasNext ()) {
		
					row = rows.next ();
					if((row.getCell(1).toString().toUpperCase() == "YES")&& (row.getCell(2).toString().equalsIgnoreCase(clientNo))&&(row.getCell(6).toString().equalsIgnoreCase(setNo))){
						listTestCaseName.add(row.getCell(0).toString())
					}
				}
				inStr.close()
				logi.logInfo("TestCase selected :  " + listTestCaseName.toString() +" for Client : "+clientNo + ", Set :"+ setNo);
				return listTestCaseName
			}
	
	HashMap getTCSelectionHashP2() {

		def inFile = Constant.PROJECTLOCATION + Constant.TESTDATABOOK + '.xls'
		logi.logInfo(inFile);
		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		logi.logInfo("Processing Sheet/API: "+sheet.getSheetName());
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listTestCaseName = []
		while (rows.hasNext ())                {

			row = rows.next ();
			if(row.getCell(1).toString().toUpperCase() == "YES"){
				listTestCaseName.add(row.getCell(0).toString())
			}
		}
		inStr.close()
		return listTestCaseName
	}
	
	


	int getTCSelectedCountP2() {
		def inFile = Constant.PROJECTLOCATION + Constant.TESTDATABOOK + '.xls'
		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		int tcSelection =0
		while (rows.hasNext ()) {

			row = rows.next ();
			if(row.getCell(1).toString().toUpperCase() == "YES"){
				tcSelection++
			}
		}
		inStr.close()		
		return tcSelection
	}

	int getTCSkippedCountByClientP2(String clientNo) {

		def inFile = Constant.PROJECTLOCATION + Constant.TESTDATABOOK + '.xls'
		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		logi.logInfo("Processing Sheet/API: "+sheet.getSheetName());
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		int tcSelection =0
		while (rows.hasNext ()) {
			row = rows.next ();
			if((row.getCell(1).toString().toUpperCase() != "YES")&& (row.getCell(2).toString().equalsIgnoreCase(clientNo)))
				tcSelection++
		}
		inStr.close()
		logi.logInfo("Final TC :  " + tcSelection);
		
		return tcSelection
	}

	int getTCSkippedCountByClienAndSettP2(String clientNo, String setNo) {
		
				def inFile = Constant.PROJECTLOCATION + Constant.TESTDATABOOK + '.xls'
				InputStream inStr = new FileInputStream(inFile)
				HSSFWorkbook workbook = new HSSFWorkbook(inStr)
				HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
				logi.logInfo("Processing Sheet/API: "+sheet.getSheetName());
				HSSFRow headerRow = sheet.getRow(0)
				Iterator<HSSFRow> rows = sheet.rowIterator ();
				HSSFRow row = rows.next();
				int tcSkippedCount =0
				while (rows.hasNext ()) {
					row = rows.next ();
					if((row.getCell(1).toString().toUpperCase() != "YES")&& (row.getCell(2).toString().equalsIgnoreCase(clientNo))&&(row.getCell(6).toString().equalsIgnoreCase(setNo)))
						tcSkippedCount++
				}
				inStr.close()
				logi.logInfo("TestCase count skipped :  " + tcSkippedCount+" for Client : "+clientNo + ", Set :"+ setNo);
				return tcSkippedCount
			}
	
	String getTCSkippedCountP2() {
		def inFile = Constant.PROJECTLOCATION + Constant.TESTDATABOOK + '.xls'

		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTCASENAME)
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listTestCaseName = []
		while (rows.hasNext ()) {

			row = rows.next ();
			if(row.getCell(1).toString().toUpperCase() != "YES"){
				listTestCaseName.add(row.getCell(0).toString())
			}
		}
		inStr.close()
		return listTestCaseName.size()
	}

	LinkedHashMap<String,String>  getTestDataMap(String sheetname,String testcaseid)
	{

		LinkedHashMap<String,String> rowvalue = new LinkedHashMap();
		LinkedHashMap<Map,LinkedHashMap<String,String>> allRows = new LinkedHashMap<Map,LinkedHashMap<String,String>>();
		List  headers = []


		String caseId = null;
		HSSFSheet sheet = null


		//logi.logInfo("Calling method getTestDataMap => "+sheetname+", Test Case ID=> "+testcaseid)

		sheet = initiateExcelConnectionP2(sheetname)

		HSSFRow header = sheet.getRow(0)
		String to_pay=null
		String return_val=null
		String return_val_acc=null

		int rows = sheet.getPhysicalNumberOfRows();
		int totalCells = header.getPhysicalNumberOfCells();


		//Adding headers in list
		for(int i=0; i<header.getPhysicalNumberOfCells();i++)
		{
			headers.add(header.getCell(i).toString())
		}


		rowvalue = new HashMap<String, String>();
		//rowvalue.clear();
		Boolean dataRead=false
		for (int i = 1; i < rows; i++) {
			for (int val = 0; val < totalCells; val++) {
				HSSFRow rowNum = sheet.getRow(i);
				caseId = rowNum.getCell((int) 0).toString();

				if(caseId==testcaseid)
				{
					HSSFCell cellA1 = rowNum.getCell((int) val);
					if(cellA1.toString().contains('md_'))
					{
						String[] to_call_array=	cellA1.toString().split("\\|")
						return_val_acc = ""
						for(int callindex=0; callindex<to_call_array.length;callindex++)
						{
							String md_to_call
							if (to_call_array[callindex].contains("md_GET_INV_LINE_REV_AMT"))
								md_to_call = to_call_array[callindex] + ":line" + callindex.toString()
							else
								md_to_call = to_call_array[callindex]
							
							if (md_to_call.contains("md_"))
								return_val=objInputmethod.invokeInputMethod(md_to_call,rowvalue)
							else
								return_val=md_to_call

							if(callindex>0)
								return_val_acc=return_val_acc+(char)124+return_val
							else
								return_val_acc=return_val



						}

						rowvalue.put(headers.get(val), return_val_acc);
					}
					else
					{
						rowvalue.put(headers.get(val),cellA1.toString());
					}


					dataRead=true
				}
				//allRows.put(caseId, rowvalue);
			}            
			rowvalue.put("acct_no",vm.getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1))
						
			if(rowvalue.get("acct_no")=="NoVal")
			{
				rowvalue.put("acct_no",vm.getValueFromResponse('create_acct_hierarchy',ExPathRpcEnc.CAH_ACCT_NO1))
			}

			if (dataRead)
				break
		}
		if (rowvalue.toString().contains("TestCaseDesc") == false)
		{
			logi.logInfo("::: INPUT DATA :::")
			logi.logInfoWithNoFormat(rowvalue.toString());
		}
		return rowvalue;
	}
	HSSFSheet initiateExcelConnectionP2(String workSheetName) {

		String strBasePath = null
		String file = null
		HSSFSheet sheet = null

		POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(Constant.PROJECTLOCATION + Constant.TESTDATABOOK+'.xls'))
		HSSFWorkbook wb = new HSSFWorkbook(fs)
		sheet = wb.getSheet(workSheetName)

		return sheet
	}

	LinkedHashMap getVerificationDataP2(String testCaseName, String testStepName) {

		library.Constants.Constant.paramActual =new HashMap<Integer, String>()
		library.Constants.Constant.paramExpected =new HashMap<Integer, String>()
		library.Constants.Constant.seq =new HashMap<Integer, String>()
		library.Constants.Constant.seq2 =new HashMap<Integer, String>()
              logi.logInfo "Calling getVerificationDataP2 => "+testCaseName+", " +testStepName
              workbook = new HSSFWorkbook(new FileInputStream(Constant.PROJECTLOCATION+ Constant.RESPONSEDATABOOK+'.xls'))

              List tcName = getColumnList('TestCase')
              List tsName = getColumnList('TestStep')
              List xPath = getColumnList('Node_XPath')
              List expValue = getColumnList('Expected_Value')
              List verificationDescFull = getColumnVerificationList('VERIFICATION_DESCRIPTION')
              List verificationDescAPI = []
			  LinkedList key=new LinkedList();
			  LinkedList Value=new LinkedList();

              LinkedHashMap responseMap = new LinkedHashMap();
              LinkedHashMap<String,String> rowvalue = new LinkedHashMap();
              int z=0;
              for(int i=0; i<tcName.size(); i++) {
                     if(tcName.get(i).toString().trim().equalsIgnoreCase(testCaseName.trim()) && tsName.get(i).toString().trim().equalsIgnoreCase(testStepName.trim())){
						 	
                           // Get the equivalent xPath from the constant groovy file
                           if (verificationDescFull.size() > 0)
                                  verificationDescAPI.add verificationDescFull.get(i)
                           String xPathData
						   try{
						   if(xPath.get(i).toString().contains("DB_") || xPath.get(i).toString().contains("md_") || xPath.get(i).toString().contains("uv_") || xPath.get(i).toString().contains("manageauth_")) {
                                  if(xPath.get(i).toString().contains("DB_")&&xPath.get(i).toString().contains("_m")){library.Constants.Constant.DB_flag=true}
                                  if(xPath.get(i).toString().contains("_param_"))
                                  {
									  int e=0;
									  String Val=xPath.get(i).toString().split("_param_")[0]
									  for(int z1=50;z1>0;z1--)
									  {
										  
										  if(Val.contains("_m"+z1))
										  {e=z1}
										 // logi.logInfo " val before replace" +Val
										  Val=Val.replace("_m"+z1,"_m")
										//logi.logInfo " val after replace" +Val
										 
									  }
									  if(e!=0)
									  {xPathData = getExpValue(Val)+e
										 
									  }
									  else{xPathData = getExpValue(Val)
										
									  }
									  
                                  //      xPathData = getExpValue(xPath.get(i).toString().split("_param_")[0])
                                         library.Constants.Constant.paramActual.put(z,xPath.get(i).toString().split("_param_")[1])
										 if(library.Constants.Constant.paramActual.get(z).toString().contains("_seq_"))
										 {
											 library.Constants.Constant.seq.put(z,library.Constants.Constant.paramActual.get(z).toString().split("_seq_")[1])
											 library.Constants.Constant.paramActual.put(z,library.Constants.Constant.paramActual.get(z).toString().split("_seq_")[0])
										 }
										 logi.logInfo "putting into hash"+library.Constants.Constant.paramActual
										 
                                  }      
                                  else {
									  logi.logInfo "into the else since no param"
									  int e=0;
									  String Val=xPath.get(i).toString()
									  for(int z1=50;z1>0;z1--)
									  {
										  
										  if(Val.contains("_m"+z1))
										  {e=z1}
										//  logi.logInfo " val before replace" +Val
										  Val=Val.replace("_m"+z1,"_m")
										//logi.logInfo " val after replace" +Val
										 
									  }
									  if(e!=0)
									  {xPathData = getExpValue(Val)+e
										 
									  }
									  else{xPathData = getExpValue(Val)
										
									  }
									  
                                         xPathData = getExpValue(xPath.get(i).toString())
                                  }
                                  
                                                
                                  
                           } 
                           else if (xPath.get(i).toString().contains("TD_")) {
                                  String fieldname=xPath.get(i).split("TD_")[1]
                                  String temp1,location
                                  if(fieldname.contains(":")) {
                                         String fld=fieldname.split(":")[0]
                                         String param=fieldname.split(":")[1]
                                         int lineno=param.toInteger()                                         
                                         temp1 =  Constant.lhmAllServcieDataAsXML_Global.get(testStepName).get(fld)
                                         String temp2=temp1.split("\\|")[lineno-1]
                                         xPathData = xPath.get(i) + '-' + temp2
                                  }
                                  else if(fieldname.contains("[")) {
                                         location = (((fieldname.split("\\[")[1]).toString()).split("\\]")[0].toString())
                                         fieldname = fieldname.split("\\[")[0]
                                         temp1 = (Constant.lhmAllServcieDataAsXML_Global.get(testStepName).get(fieldname)).split("\\|")[Integer.parseInt(location)-1]
                                         xPathData = xPath.get(i) + '-' + temp1
                                  }
                                  else
                                  {
                                         temp1 =  Constant.lhmAllServcieDataAsXML_Global.get(testStepName).get(fieldname)
                                      temp1.isEmpty()? temp1='NO NODE':temp1;
                                         xPathData = xPath.get(i) + '-' + temp1
                                  }
                           }else if (xPath.get(i).toString().contains("\$")) {
                                  xPathData = xPath.get(i).toString()
                           } else if(xPath.get(i).toString().contains("ui_")){
                                  xPathData = getXPath_UI(xPath.get(i).toString())
                           }
                           else {
                                  xPathData = getXPath(xPath.get(i).toString())
                           }
						   }catch(Exception e){xPathData =xPath.get(i).toString()}

                           // Get the expected value
                           String expData
						   try{
                           if(expValue.get(i).toString().contains("DB_") || expValue.get(i).toString().contains("md_")|| expValue.get(i).toString().contains("manageauth_")) {
                                  if(expValue.get(i).toString().contains("DB_")&&expValue.get(i).toString().contains("_m")){library.Constants.Constant.DB_flag=true}
                                  if(expValue.get(i).toString().contains("_param_"))
                                  {
									  int e=0;
									  String Val=expValue.get(i).toString().split("_param_")[0]
									  for(int z1=1;z1<20;z1++)
									  {
										  if(Val.contains("_m"+z1)){e=z1}
										  Val=Val.replace("_m"+z1,"_m")
										
									  }
									  if(e!=0)
									  { expData = getExpValue(Val)+e
										   logi.logInfo " expected if" +expData
									  }
									  else{expData = getExpValue(Val)
										    logi.logInfo " expected else" +expData
											}
									  
                                       //  expData = getExpValue(expValue.get(i).toString().split("_param_")[0])
                                      	 library.Constants.Constant.paramExpected.put(z,expValue.get(i).toString().split("_param_")[1])
										   if(library.Constants.Constant.paramExpected.get(z).toString().contains("_seq_"))
										   {
											   library.Constants.Constant.seq2.put(z,library.Constants.Constant.paramExpected.get(z).toString().split("_seq_")[1])
											   library.Constants.Constant.paramExpected.put(z,library.Constants.Constant.paramExpected.get(z).toString().split("_seq_")[0])
										   }
										   logi.logInfo "putting into hash exp"+library.Constants.Constant.paramExpected
								 }
                                  else
                                  {
									  logi.logInfo "into the else since no param"
									  int e=0;
									  String Val=expValue.get(i).toString()
									  for(int z1=50;z1>0;z1--)
									  {
										  if(Val.contains("_m"+z1)){e=z1}
										  Val=Val.replace("_m"+z1,"_m")
										
									  }
									  if(e!=0)
									  { expData = getExpValue(Val)+e
										   logi.logInfo " expected if" +expData
									  }
									  else{expData = getExpValue(Val)
											logi.logInfo " expected else" +expData
											}
                                         expData = getExpValue(expValue.get(i).toString())
                                  }
                           
                           } else if (expValue.get(i).toString().contains("TD_")) {
                                  String fieldname=expValue.get(i).split("TD_")[1]
                                  String temp1,location
                                  if(fieldname.contains(":")) {
                                         String fld=fieldname.split(":")[0]
                                         String param=fieldname.split(":")[1]
                                         int lineno=param.toInteger()
                                         temp1 =  Constant.lhmAllServcieDataAsXML_Global.get(testStepName).get(fld)
                                         String temp2=temp1.split("\\|")[lineno-1]
                                         expData = expValue.get(i) + '-' + temp2
                                  }
                                  else if(fieldname.contains("[")) {
                                         location = (((fieldname.split("\\[")[1]).toString()).split("\\]")[0].toString())
                                         fieldname = fieldname.split("\\[")[0]
                                         temp1 = (Constant.lhmAllServcieDataAsXML_Global.get(testStepName).get(fieldname)).split("\\|")[Integer.parseInt(location)-1]
                                         expData = expValue.get(i) + '-' + temp1
                                  }
                                  else
                                  {
                                         temp1 =  Constant.lhmAllServcieDataAsXML_Global.get(testStepName).get(fieldname)
                                         expData = expValue.get(i) + '-' + temp1
                                  }
                           } else {
                                  expData = expValue.get(i).toString()
                           }
						   }catch(Exception e){expData = expValue.get(i).toString()}
                          // responseMap.put xPathData, expData
						   key.add(xPathData)
						   Value.add(expData)
						  ++z;
                     }
					 if(key.size>0)
					responseMap.put(key,Value);
					else responseMap = new LinkedHashMap();
              }

              
              Constant.verificationDescription =  verificationDescAPI

              LinkedHashMap<String, com.eviware.soapui.support.XmlHolder> tmp = new LinkedHashMap()
              logi.logInfo("::: RESPONSE VERIFICATION DATA MAP :::")
              logi.logInfo("Test Case: " + testCaseName)
              logi.logInfo(responseMap.toString());
              return responseMap
       }
	LinkedHashMap<String,String>  getServiceOrder(List selectedCase) {
		Iterator iterator = selectedCase.iterator()
		LinkedHashMap<String,String> serviceOrder = new LinkedHashMap<String,String>()
		while (iterator.hasNext()) {
			String testcaseId = iterator.next()
			LinkedHashMap<String,String> testcaseDetails =getTestDataMap(library.Constants.Constant.TESTCASENAME, testcaseId)
			serviceOrder.put( testcaseId ,testcaseDetails.get('Service_Order'))
		}
		return serviceOrder
	}

	int getMaxVTOccurance(HashMap<String, String> serviceOrder) {
		Iterator iterator = serviceOrder.entrySet().iterator()
		int maxTime = 0
		while (iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) iterator.next()
			String value = pairs.getValue().toString()
			value = StringUtils.lowerCase(value)
			int count = StringUtils.countMatches(value, "|")
			if(count>maxTime)
				maxTime = count
			iterator.remove() // avoids a ConcurrentModificationException
		}
		return maxTime
	}
	
	LinkedHashMap<String,String>  setIndexFromValue(List selectedCase, int fromValue) {
		Iterator iterator = selectedCase.iterator()
		LinkedHashMap<String,String> tcIndex = new LinkedHashMap<String,String>()
		int count =fromValue ;
		while (iterator.hasNext()) {
			String testcaseId = iterator.next()
			tcIndex.put(testcaseId ,count)
			count++
		}
		return tcIndex
	}	

	LinkedHashMap<String,String>  setIndex(List selectedCase) {
		Iterator iterator = selectedCase.iterator()
		LinkedHashMap<String,String> serviceOrder = new LinkedHashMap<String,String>()
		int count =0 ;
		while (iterator.hasNext()) {
			String testcaseId = iterator.next()
			serviceOrder.put(testcaseId ,count)
			count++
		}
		return serviceOrder
	}
	
	String randomNumber()
	{
		//Random randomno = new Random();
		//String number ='receiptId' + randomno.nextInt(10000)
		long number = (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
		String client_receipt_id= 'clientreceiptid'+number
		return client_receipt_id
	}
	
	//For AdminTools
	LinkedHashMap getHttpVerificationDataP2(String testCaseName, String testStepName) {
		
				logi.logInfo "Calling getVerificationDataP2 => "+testCaseName+", " +testStepName
				workbook = new HSSFWorkbook(new FileInputStream(Constant.PROJECTLOCATION+ Constant.RESPONSEDATABOOK+'.xls'))
		
				List tcName = getColumnList('TestCase')
				List tsName = getColumnList('TestStep')
				List xPath = getColumnList('Node_XPath')
				List expValue = getColumnList('Expected_Value')
				List verificationDescFull = getColumnVerificationList('VERIFICATION_DESCRIPTION')
				List verificationDescAPI = []
		
				LinkedHashMap responseMap = new LinkedHashMap();
				LinkedHashMap<String,String> rowvalue = new LinkedHashMap();
				
				for(int i=0; i<tcName.size(); i++) {
					if(tcName.get(i).toString().equalsIgnoreCase(testCaseName) && tsName.get(i).toString().equalsIgnoreCase(testStepName)){
		
						// Get the equivalent xPath from the constant groovy file
						if (verificationDescFull.size() > 0)
							verificationDescAPI.add verificationDescFull.get(i)
						String xPathData
						if(xPath.get(i).toString().contains("DB") || xPath.get(i).toString().contains("md")) {
							
								xPathData = getExpValue(xPath.get(i).toString())
							
							
						} else if (xPath.get(i).toString().contains("TD")) {
							String fieldname=xPath.get(i).split("TD_")[1]
							String temp1,location
							if(fieldname.contains(":")) {
								String fld=fieldname.split(":")[0]
								String param=fieldname.split(":")[1]
								int lineno=param.toInteger()
								temp1 =  Constant.lhmAllServcieDataAsXML_Global.get(testStepName).get(fld)
								String temp2=temp1.split("\\|")[lineno-1]
								xPathData = xPath.get(i) + '-' + temp2
							}
							else if(fieldname.contains("[")) {
								location = (((fieldname.split("\\[")[1]).toString()).split("\\]")[0].toString())
								fieldname = fieldname.split("\\[")[0]
								temp1 = (Constant.lhmAllServcieDataAsXML_Global.get(testStepName).get(fieldname)).split("\\|")[Integer.parseInt(location)-1]
								xPathData = xPath.get(i) + '-' + temp1
							}
							else
							{
								temp1 =  Constant.lhmAllServcieDataAsXML_Global.get(testStepName).get(fieldname)
								xPathData = xPath.get(i) + '-' + temp1
							}
						} else if (xPath.get(i).toString().contains("http") || xPath.get(i).toString().contains("rs")) {
							xPathData = xPath.get(i).toString()
						}
						else if (xPath.get(i).toString().contains("\$")) {
							xPathData = xPath.get(i).toString()
						}
						else {
							xPathData = getHttpXPath(xPath.get(i).toString())
						}
		
						// Get the expected value
						String expData
						if(expValue.get(i).toString().contains("DB") || expValue.get(i).toString().contains("md")) {
							expData = getExpValue(expValue.get(i).toString())
						} else if (expValue.get(i).toString().contains("TD")) {
							String fieldname=expValue.get(i).split("TD_")[1]
							String temp1,location
							if(fieldname.contains(":")) {
								String fld=fieldname.split(":")[0]
								String param=fieldname.split(":")[1]
								int lineno=param.toInteger()
								temp1 =  Constant.lhmAllServcieDataAsXML_Global.get(testStepName).get(fld)
								String temp2=temp1.split("\\|")[lineno-1]
								expData = expValue.get(i) + '-' + temp2
							}
							else if(fieldname.contains("[")) {
								location = (((fieldname.split("\\[")[1]).toString()).split("\\]")[0].toString())
								fieldname = fieldname.split("\\[")[0]
								temp1 = (Constant.lhmAllServcieDataAsXML_Global.get(testStepName).get(fieldname)).split("\\|")[Integer.parseInt(location)-1]
								expData = expValue.get(i) + '-' + temp1
							}
							else
							{
								temp1 =  Constant.lhmAllServcieDataAsXML_Global.get(testStepName).get(fieldname)
								expData = expValue.get(i) + '-' + temp1
							}
						} else {
							expData = expValue.get(i).toString()
						}
						responseMap.put xPathData, expData
					}
				}
				
				Constant.verificationDescription =  verificationDescAPI
		
				LinkedHashMap<String, com.eviware.soapui.support.XmlHolder> tmp = new LinkedHashMap()
				logi.logInfo("::: RESPONSE VERIFICATION DATA MAP :::")
				logi.logInfo("Test Case: " + testCaseName)
				logi.logInfo(responseMap.toString());
				return responseMap
			}
	
	
	public void AddNewNode(String sheetname, String filepath, String service)
	{

		LinkedHashMap<String,String> rowvalue = new LinkedHashMap();
		LinkedHashMap<Map,LinkedHashMap<String,String>> allRows = new LinkedHashMap<Map,LinkedHashMap<String,String>>();
		List  headers = []
		HSSFSheet sheet = null
		sheet = initiateExcelConnectionP2(sheetname)
		HSSFRow header = sheet.getRow(0)
		int rows = sheet.getPhysicalNumberOfRows();
		int totalCells = header.getPhysicalNumberOfCells();
		//Adding headers in list
		for(int i=0; i<header.getPhysicalNumberOfCells();i++)
		{
			headers.add(header.getCell(i).toString())
		}
		rowvalue = new HashMap<String, String>();
		//rowvalue.clear();
		Boolean dataRead=false
		for (int i = 1; i < rows; i++) 
		{
			for (int val = 0; val < totalCells; val++) 
			{
				HSSFRow rowNum = sheet.getRow(i);
					HSSFCell cellA1 = rowNum.getCell((int) val);
					rowvalue.put(headers.get(val),cellA1.toString());
			}
						ClientParamUtils objClientParam= new ClientParamUtils()
						if(service==rowvalue.get('API Name').toString()&&rowvalue.get('Add').toString().equalsIgnoreCase('yes'))
						objClientParam.AddNode(filepath,rowvalue.get('Parent_node').toString(),rowvalue.get('Index').toString(),rowvalue.get('New_node').toString(),rowvalue.get('New_node_value').toString())
		 }
		 	
	}
	/*please dont delete the below function
	 
	List getNodeSheetList() {
		def inFile = Constant.PROJECTLOCATION + Constant.TESTDATABOOK + '.xls'
		logi.logInfo(inFile);
		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.ANYSHEET)
		logi.logInfo("Processing Sheet/API: "+sheet.getSheetName());
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listNodeDetails = []
		//while (rows.hasNext ())                {

			int totalNoOfCols = sheet.getRow(0).getPhysicalNumberOfCells()
			for(int i=0;i<totalNoOfCols;i++)
			{
					listNodeDetails.add(row.getCell(i).toString())
			//}
			
		}
		inStr.close()
		if (listNodeDetails.size() == 0) throw new Exception("Enable the TestCase(Yes/No).... :(")
		return listNodeDetails
	}  */
}


