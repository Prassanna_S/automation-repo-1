package library


import library.Constants.Constant;
import org.w3c.dom.Document

import com.eviware.soapui.impl.wsdl.submit.transports.http.support.methods.ExtendedGetMethod;

import System;
import java.util.Date.*
import java.text.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.CallableStatement;
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory

class ConnectDB 
{
	public static String username,password;
	public static Connection connection=null
	public static Statement statement=null
		
	String connSatus = 'Initialized'
		LogResult logi = new LogResult(Constant.logFilePath)
		
		public ConnectDB() throws SQLException {
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db;
			try {
				db = dbf.newDocumentBuilder();
				Document document ;
				try
				{
					File file = new File("C://Users//ariaqa//Desktop//Config.xml");
					document = db.parse(file);
				}catch(FileNotFoundException e)
				{
					File file = new File("Config.xml");
					document = db.parse(file);
				}
				username = document.getElementsByTagName("db_username").item(0).getTextContent();
				password = document.getElementsByTagName("db_password").item(0).getTextContent();
				} catch (Exception e1) {
				e1.printStackTrace();
			}						
				logi.logInfo("username: "+username+"\npassword: "+password)						
		boolean connectionClosed = false
		if (connection != null)
			if (connection.closed == true)
				connectionClosed = true
				
		if (connection == null || connectionClosed)		
		{			
			statement = null
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			connSatus = "Where is your Oracle JDBC Driver?"
			e.printStackTrace();
			//return;
		}
 
		System.out.println("Oracle JDBC Driver Registered!");
 
		try {

			// FULL QU

			
			//connection = DriverManager.getConnection("jdbc:oracle:thin:@oqn-qa-rac-scan.oqn.ariasystems.net:1521/ec01qub_default","gchtlur", "Aspire!456");
			connection = DriverManager.getConnection("jdbc:oracle:thin:@oqn-qa-rac-scan.oqn.ariasystems.net:1521/ec01qub_default",username, password);
			//connection = DriverManager.getConnection("jdbc:oracle:thin:@oqn-qa-rac-scan.oqn.ariasystems.net:1521/ec01qub_default","pmariappan", "Aspire!456");
			//connection = DriverManager.getConnection("jdbc:oracle:thin:@oqn-qa-rac-scan.oqn.ariasystems.net:1521/ec01qub_default","urahmathulla", "Aspire!456");
			// DF
			//connection = DriverManager.getConnection("jdbc:oracle:thin:@oqn-dv-rac-scan.oqn.ariasystems.net:1521/ec05dfb_default","kmani", "Aspire!456");
			
			String test = "test"
			
		} catch (SQLException e) {
			connSatus = "Connection Failed! Check output console"
			e.printStackTrace();
			//return;
		}
 
		if (connection != null) {
			connSatus = "You made it, take control your database now!"
			logi.logInfo("DB CONNECTION ESTABLISHED!!!")
		} else {
			connSatus =  "Failed to make connection!"
		}
		}
	}
	
	public ResultSet executeQuery(String query) throws SQLException
	{
		logi.logInfo "::: Executing Query :::" + query
		if (statement == null)
		 statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
		String QueryString = query;
		ResultSet resultSet = statement.executeQuery(QueryString);
		return resultSet;
	}
	
	public ResultSet executePlaQuery(String query) throws SQLException
	{
		String QueryString = query;
		int eventTimeout = 0
		ResultSet resultSet		
		//logi.logInfo statement.toString()
		//statement.close()
		
		boolean connectionStatementClosed
		connectionStatementClosed=false
		//logi.logInfo statement.toString()

		
		logi.logInfo connectionStatementClosed.toString()
			if (statement == null || connectionStatementClosed)
			statement = connection.createStatement();
		
		 
		logi.logInfo "::: Executing Query :::"
		//logi.logInfo statement.closed.toString()
		logi.logInfo query
		if(QueryString.contains("NoVal")) {
			logi.logInfo ":If condition:"
			QueryString = "SELECT ARC_TYPE FROM ARIACORE.ARC_TYPES WHERE ARC_TYPE='NULL'"
			resultSet = statement.executeQuery(QueryString);
			
		} else if(QueryString.contains("341")) {
		logi.logInfo ":Else Part:"
			resultSet = statement.executeQuery(QueryString);
			while(!resultSet.isBeforeFirst() && eventTimeout < 60) {
				logi.logInfo ":While loop:"
				Thread.sleep(1000)
				resultSet = statement.executeQuery(QueryString);
				eventTimeout++
			}
		} else {
		logi.logInfo ": Final Else Part started:"+QueryString
			resultSet = statement.executeQuery(QueryString);
			logi.logInfo ": Completed Final Else Part:"
			
		}
		
		return resultSet;
	}
	
	public def executeQueryP2(String query) throws SQLException {
		LinkedHashMap tempValues
		ResultSet resultSet
		
		boolean connectionStatementClosed
		connectionStatementClosed=false
		
			if (statement == null || connectionStatementClosed)
			statement = connection.createStatement();
		
		logi.logInfo "::: Executing Query :::"
		logi.logInfo query
		logi.logInfo ""+statement.executeQuery(query);
		resultSet = statement.executeQuery(query);
		
		ResultSetMetaData metaData = resultSet.getMetaData();
		int columns = metaData.getColumnCount();
		//logi.logInfo("Column count is  : "+columns)
		
		if(resultSet == null){
			String emptyValue = 0;
			logi.logInfo("Query executed and the return emptyValue:"+emptyValue)
			return emptyValue
			
		}
		
		while (resultSet !=null && resultSet.next()) {
			   if(columns>1){
					// logi.logInfo("Return type should be hash")
					 tempValues = new LinkedHashMap<String,String>()
					 for (int col = 1; col <= columns; col++) {
							String columnValue = resultSet.getString(col);
							tempValues.put(metaData.getColumnName(col),columnValue)
					 }
					logi.logInfo("Query executed and the return value is "+tempValues.toString())
					 return tempValues.sort()
			   } else{
					 logi.logInfo("Query executed and the return value is "+resultSet.getString(1))
					 return resultSet.getString(1)
			   }
		}
		
 }
	
	
	
	public def executeQueryP3(String query) throws SQLException {
		LinkedHashMap tempValues
		ResultSet resultSet
		
		boolean connectionStatementClosed
		connectionStatementClosed=false
		
			if (statement == null || connectionStatementClosed)
			statement = connection.createStatement();
		
		logi.logInfo "::: Executing Query :::"
		logi.logInfo query
		//logi.logInfo ""+statement.executeQuery(query);
		statement.executeQuery(query)
		logi.logInfo "::: done"
		return "true"
		//ResultSetMetaData metaData = resultSet.getMetaData();
		//int columns = metaData.getColumnCount();
		//logi.logInfo("Column count is  : "+columns)
		
		if(resultSet == null){
			String emptyValue = 0;
			logi.logInfo("Query executed and the return emptyValue:"+emptyValue)
			return emptyValue
			
		}
		
		return resultSet
 }
	
	

	public String[] executeQueryReturnValuesAsArray(String query) throws SQLException
	{
		def valueArray = []
		int i = 0;
		if (statement == null)
		 statement = connection.createStatement();
		 logi.logInfo "::: Executing Query :::"
		 logi.logInfo query
		ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			valueArray[i] = resultSet.getString(1);
			i++;
		}

		resultSet.close()
		logi.logInfo "Query executed and the return array is : "+valueArray
		return valueArray;
	}
	public void closeConnection() throws SQLException
	{		
		statement.close()
		connection.close();
		logi.logInfo "DB CONNECTION CLOSED"
	}
	
	/**
	 * @deprecated
	 */
//	public String buildQuery(String query, String clientNumber, String searchParam1, String searchParam2)
//	{
//		query = query.replaceAll(Constant.Database.QUERY_CLIENT_NO_TAG, clientNumber);
//		
//		if(query.contains(Constant.Database.QUERY_SEARCH_PARAM_1_TAG))
//			query = query.replaceAll(Constant.Database.QUERY_SEARCH_PARAM_1_TAG, searchParam1);
//		
//		if(query.contains(Constant.Database.QUERY_SEARCH_PARAM_2_TAG))
//			query = query.replaceAll(Constant.Database.QUERY_SEARCH_PARAM_2_TAG, searchParam2);
//		
//		if(query.contains(Constant.Database.QUERY_SEARCH_PARAM_3_TAG))
//			query = query.replaceAll(Constant.Database.QUERY_SEARCH_PARAM_2_TAG, searchParam2);
//		
//		if(query.contains(Constant.Database.QUERY_CURRENCY))
//			query = query.replaceAll(Constant.Database.QUERY_CURRENCY, searchParam1);
//		
//		if(query.contains(Constant.Database.INVENTORY_NAME))
//			query = query.replaceAll(Constants.Constant.Database.INVENTORY_NAME, searchParam1);
//		return query;
//	}
	
	public String buildQuery(String query, String clientNo, ArrayList<String> searchParamValues)
	{
		String[] sp = query.split("<");
		ArrayList<String> arrValues = new ArrayList<>();
		
		for(int i=1 ; i<sp.length ; i++)
		{
			arrValues.add(sp[i].substring(0, sp[i].indexOf(">")));
		}
		String newquery = query;
		for(int i=0 ; i<arrValues.size() ; i++)
		{
			newquery = newquery.replace(arrValues.get(i), "SearchParam" + (i+1));
		}
		
		if(newquery.contains("TD_ClientNo")) {
			newquery = newquery.replaceAll("TD_ClientNo", clientNo);
		}
		for(int i=1 ; i <= searchParamValues.size() ; i++) {
			if(newquery.contains("<SearchParam" + i + ">"))
				newquery = newquery.replaceAll("<SearchParam" + i + ">", searchParamValues.get(i-1));
		}
		return newquery;
	}
	
	public String turnAutoCommit(boolean setCommit) {
		connection.commit()
		connection.setAutoCommit(setCommit);
	}
	
	public String executeSP(String queryInFormat, String clinetNo)
	{
		String status = "";
		CallableStatement proc;
		try {
			
			if (statement == null)
				statement = connection.createStatement();
			
			proc  = connection.prepareCall(queryInFormat);
			proc.setString(1, clinetNo);	
			try{proc.setString(2, clinetNo);}catch(Exception e){logi.logInfo "This could be for bulk_record_usage"}
			proc.executeQuery();
			
			status= "success";
		}
		/*catch (�xception e)
		{
			a=e.toString();
		}*/
		finally
		{
			  proc.close();
		}
		return status
	}
}
